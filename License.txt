UDP-Manager
Copyright (c) 2014-2017 Freie und Hansestadt Hamburg, Landesbetrieb Geoinformation und Vermessung

Open Source License
---------------------------------------------------------------------------------------------------
This version of UDP-Manager is licensed under the terms of the Open Source GPL 3.0 license.

http://www.gnu.org/licenses/gpl.html
---------------------------------------------------------------------------------------------------

UDP-Manager is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

UDP-Manager is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Der UDP-Manager ist Freie Software: Sie können es unter den Bedingungen
der GNU General Public License, wie von der Free Software Foundation,
Version 3 der Lizenz oder (nach Ihrer Wahl) jeder neueren
veröffentlichten Version, weiterverbreiten und/oder modifizieren.

Der UDP-Manager wird in der Hoffnung, dass es nützlich sein wird, aber
OHNE JEDE GEWÄHRLEISTUNG, bereitgestellt; sogar ohne die implizite
Gewährleistung der MARKTFÄHIGKEIT oder EIGNUNG FÜR EINEN BESTIMMTEN ZWECK.
Siehe die GNU General Public License fpr weitere Details.
