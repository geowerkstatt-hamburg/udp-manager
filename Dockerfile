# -- build image --
FROM node:20.9.0-bullseye-slim  AS build

ARG http_proxy
ARG https_proxy

RUN apt-get update && apt-get install -y --no-install-recommends dumb-init openjdk-11-jdk

WORKDIR /usr/src/app

COPY package.json /usr/src/app/package.json
COPY backend /usr/src/app/backend
COPY frontend /usr/src/app/frontend_src

RUN if [ -n "$https_proxy" ] ; then npm config set https-proxy ${https_proxy} ; fi
RUN if [ -n "$http_proxy" ] ; then npm config set proxy ${http_proxy} ; fi
RUN npm install
RUN npm install -g @sencha/cmd
RUN chmod -R a+rwx /usr/local/lib/node_modules/@sencha/cmd/dist/sencha
RUN mkdir /usr/src/prod_build

WORKDIR /usr/src/app/frontend_src

ENV OPENSSL_CONF=/etc/ssl/

RUN sencha app build production

# -- runtime image --
FROM node:20.9.0-bullseye-slim

RUN apt-get update && apt-get -y install -y git

COPY --from=build /usr/src/app/node_modules /usr/src/app/node_modules
COPY --from=build /usr/src/app/backend /usr/src/app/backend
COPY --from=build /usr/src/prod_build/frontend /usr/src/app/frontend
COPY --from=build /usr/src/app/frontend_src/views /usr/src/app/frontend/views
COPY --from=build /usr/src/app/frontend_src/bootstrap.js /usr/src/app/frontend/bootstrap.js
COPY --from=build /usr/bin/dumb-init /usr/bin/dumb-init

WORKDIR /usr/src/app

USER node

ENV NODE_ENV prod
ENV NODE_HOST 0.0.0.0
ENV NODE_PORT 3000
ENV DEV_AD_USER MusterMax
ENV LOG_LEVEL debug
ENV SECRET 01234567890123456789012345678901
ENV API_TOKEN 01234567890123456789012345678901
ENV CONFIG_OUT_PATH C:/configs
ENV AUTH_METHOD ldap
ENV KC_ISSUER_URL https://keycloak.host/auth/realms/udp-manager
ENV KC_CLIENT_ID udp-manager
ENV KC_CLIENT_SECRET 123
ENV KC_REDIRECT_URL http://localhost:9020/auth/callback
ENV KC_POST_LOGOUT_REDIRECT_URL http://localhost:9020/logout/callback
ENV LDAP_URL ldaps://ldap.de
ENV LDAP_BASE_DN base_dn
ENV LDAP_USER username
ENV LDAP_PASSWORD password
ENV LDAP_ADMIN_GROUP Admin-Group
ENV PROXY_HOST proxy.host
ENV PROXY_PORT 80
ENV DB_HOST localhost
ENV DB_PORT 5432
ENV DB_NAME udp_manager
ENV DB_USER udp_manager
ENV DB_PASSWORD password
ENV DB_DIAGNOSTICS true
ENV SMTP_HOST mail.de
ENV SMTP_PORT 25
ENV GIT_REPO_URL https://token:password@git.de/repo.git
ENV JIRA_USERNAME username@mail.de
ENV JIRA_PASSWORD password

EXPOSE 3000
CMD ["dumb-init", "node", "./backend/server.js"]