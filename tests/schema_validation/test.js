const schemaValidator = require("../../backend/utils/configuration_data_validate.util");

const validGroupCollection = {
    title: "title",
    table: null,
    schema: null,
    attributeMap: null,
    style: null,
    isGroup: true,
    mainGeomProjection: "EPSG:Dings",
    namespace: null,
    legendDataUrl: null,
    legendContentType: null,
    legendFileName: null,
    custom_bbox: null ,
    primaryKey: null,
    mainGeom: null
}

schemaValidator.validate("collection", validGroupCollection)