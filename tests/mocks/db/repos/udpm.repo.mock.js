class UDPMRepoMock {
    getLayers () {
        return [
            {id: 1, dataset_id: 1, collection_id: 1, service_id: 1}
        ];
    }

    addLayer () {
        return [
            {id: 2}
        ];
    }

    updateLayer () {
        return [
        ];
    }

    deleteLayer () {
        return [];
    }

    addLayerDeleteRequest () {
        return [];
    }

    deleteLayerDeleteRequest () {
        return [];
    }

    getCompleteLayerParams () {
        return [];
    }

    getMetadataCatalogExtRepl () {
        return [];
    }

    getCollections () {
        return [
            {id: 1, dataset_id: 1, name: "test_collection"}
        ];
    }

    getCollectionsDataset () {
        return [
            {id: 1, dataset_id: 1, name: "test_collection"}
        ];
    }

    addCollectionsDataset () {
        return [
            {id: 2}
        ];
    }

    updateCollectionsDataset () {
        return [];
    }

    deleteCollectionsDataset () {
        return [];
    }

    deleteCollectionsDataset () {
        return [];
    }

    updateAttributeConfig () {
        return [];
    }

    updateAttributeConfigAllCollections () {
        return [];
    }

    updateAttributeConfigAllCollections () {
        return [];
    }

    getServices () {
        return [
            {id: 1, dataset_id: 1, name: "test_service"}
        ];
    }

    getDatasets () {
        return [
            {id: 1, name: "test_dataset"}
        ];
    }

    addDataset () {
        return [
            {id: 2}
        ];
    }

    updateDataset () {
        return [];
    }

    deleteDataset () {
        return [];
    }
}

module.exports = UDPMRepoMock;
