const {UDPMRepoMock} = require("./repos");
const db_udpm = {};

db_udpm.exec = new UDPMRepoMock();

module.exports = {db_udpm};
