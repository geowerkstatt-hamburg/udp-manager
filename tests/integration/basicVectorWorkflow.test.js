require("dotenv").config();
const express = require("express");
const app = express();
const axios = require("axios");
const expressLoader = require("../../backend/loaders/express.loader");
const {db_udpm, db_ext} = require("../../backend/db");
const httpClient = require("supertest");
const assert = require("assert");
const {ConfiguratorFactory} = require("../../backend/configurator");
const testData = require("./basicVectorWorkflow/testData");
const fs = require("fs").promises;
const {hashElement} = require("folder-hash");

describe("Basic Workflow - vector data with deegree and ldproxy services", async () => {

    expressLoader(app, db_udpm, db_ext, axios, null, ConfiguratorFactory, null);
    const request = httpClient(app);

    it("clear out path", async () => {
        await fs.rm(process.env.CONFIG_OUT_PATH, {recursive: true, force: true});
    });

    it("clear DB", async () => {
        await request
            .get("/backend/cleardb")
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success response message");
            });
    });

    it("init DB Schema", async () => {
        await request
            .get("/backend/initdbschema")
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, "Initialisierung erfolgreich!", "should get success response message");
            });
    });

    it("add DB Connection", async () => {
        await request
            .post("/backend/adddbconnection")
            .send(testData.adddbconnection)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {"id": 1}, "should get db connection id 1");
            });
    });

    it("add Metadata Catalog", async () => {
        await request
            .post("/backend/addconfigmetadatacatalog")
            .send(testData.addconfigmetadatacatalog)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, [], "should get empty array");
            });
    });

    it("add Dataset", async () => {
        await request
            .post("/backend/savedataset")
            .send(testData.savedataset)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get dataset id 1");
            });
    });

    it("add visual Collection", async () => {
        await request
            .post("/backend/savecollectionsdataset")
            .send(testData.savecollectionsdataset.visual)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get collection id 1");
            });
    });

    it("add download Collection", async () => {
        await request
            .post("/backend/savecollectionsdataset")
            .send(testData.savecollectionsdataset.download)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 2}, "should get collection id 2");
            });
    });

    it("add visual and download Collection", async () => {
        await request
            .post("/backend/savecollectionsdataset")
            .send(testData.savecollectionsdataset.visual_download)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 3}, "should get collection id 3");
            });
    });

    it("add group Collection", async () => {
        await request
            .post("/backend/savecollectionsdataset")
            .send(testData.savecollectionsdataset.group)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 4}, "should get collection id 4");
            });
    });

    it("update attribute config 1", async () => {
        await request
            .post("/backend/updateattributeconfig")
            .send(testData.updateattributeconfig.collection_1)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("update attribute config 2", async () => {
        await request
            .post("/backend/updateattributeconfig")
            .send(testData.updateattributeconfig.collection_2)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("update attribute config 3", async () => {
        await request
            .post("/backend/updateattributeconfig")
            .send(testData.updateattributeconfig.collection_3)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("update attribute config 4", async () => {
        await request
            .post("/backend/updateattributeconfig")
            .send(testData.updateattributeconfig.collection_4)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("update style config 1", async () => {
        await request
            .post("/backend/updatestyleconfig")
            .send(testData.updatestyleconfig.collection_1)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("update style config 3", async () => {
        await request
            .post("/backend/updatestyleconfig")
            .send(testData.updatestyleconfig.collection_3)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("add deegree wms service", async () => {
        await request
            .post("/backend/saveservice")
            .send(testData.saveservice.wms_deegree)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get service id 1");
            });
    });

    it("add deegree wfs service", async () => {
        await request
            .post("/backend/saveservice")
            .send(testData.saveservice.wfs_deegree)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 2}, "should get service id 2");
            });
    });

    it("add ldproxy oaf service", async () => {
        await request
            .post("/backend/saveservice")
            .send(testData.saveservice.oaf_ldproxy)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 3}, "should get service id 3");
            });
    });

    it("add layer deegree wms collection 1", async () => {
        await request
            .post("/backend/savelayer")
            .send(testData.savelayer.service1_collection1)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get layer id 1");
            });
    });

    it("add layer deegree wms collection 3", async () => {
        await request
            .post("/backend/savelayer")
            .send(testData.savelayer.service1_collection3)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 2}, "should get layer id 2");
            });
    });

    it("add layer deegree wms collection 4", async () => {
        await request
            .post("/backend/savelayer")
            .send(testData.savelayer.service1_collection4)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 3}, "should get layer id 3");
            });
    });

    it("add layer deegree wfs collection 2", async () => {
        await request
            .post("/backend/savelayer")
            .send(testData.savelayer.service2_collection2)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 4}, "should get layer id 4");
            });
    });

    it("add layer deegree wfs collection 3", async () => {
        await request
            .post("/backend/savelayer")
            .send(testData.savelayer.service2_collection3)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 5}, "should get layer id 5");
            });
    });

    it("add layer ldproxy oaf collection 2", async () => {
        await request
            .post("/backend/savelayer")
            .send(testData.savelayer.service3_collection2)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 6}, "should get layer id 6");
            });
    });

    it("add layer ldproxy oaf collection 3", async () => {
        await request
            .post("/backend/savelayer")
            .send(testData.savelayer.service3_collection3)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 7}, "should get layer id 7");
            });
    });

    it("generate config deegree wms", async () => {
        await request
            .post("/backend/generateconfig")
            .send(testData.generateconfig)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {newStatus: "in Bearbeitung", services: {1: {configsEdited: false, status: "success"}, 2: {configsEdited: false, status: "success"}, 3: {configsEdited: false, status: "success"}}}, "should get success message");
            });
    });

    it("validate out-path check-sum", async () => {
        const options = {
            folders: {
                ignoreRootName: true
            }
        };

        const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
        const hashref = await hashElement("./basicVectorWorkflow/api-configs-ref-dev", options);

        assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    });

    it("deploy stage config deegree wms", async () => {
        await request
            .post("/backend/deployserviceconfig")
            .send(testData.deployserviceconfig.stage)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {newStatus: "vorveröffentlicht", "services": {
                    "1": {
                        "serviceStatus": {
                            "folder": "fachdaten",
                            "id": 1,
                            "server": "server2",
                            "status_dev": false,
                            "status_prod": false,
                            "status_stage": true,
                            "url_ext": "https://api.de/wms_stadtgruen",
                            "url_int": "http://server2.de/fachdaten/services/wms_stadtgruen"
                        },
                        "status": "success"
                    },
                    "2": {
                        "serviceStatus": {
                            "folder": "fachdaten",
                            "id": 2,
                            "server": "server2",
                            "status_dev": false,
                            "status_prod": false,
                            "status_stage": true,
                            "url_ext": "https://api.de/wfs_stadtgruen",
                            "url_int": "http://server2.de/fachdaten/services/wfs_stadtgruen"
                        },
                        "status": "success"
                    },
                    "3": {
                        "serviceStatus": {
                            "folder": "ldproxy",
                            "id": 3,
                            "server": "server2",
                            "status_dev": false,
                            "status_prod": false,
                            "status_stage": true,
                            "url_ext": "https://api.de/datasets/v1/stadtgruen",
                            "url_int": "http://server2.de/ldproxy/stadtgruen"
                        },
                        "status": "success"
                    }
                }}, "should get success message");
            });
    });

    it("validate stage out-path check-sum", async () => {
        const options = {
            folders: {
                ignoreRootName: true
            }
        };

        const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
        const hashref = await hashElement("./basicVectorWorkflow/api-configs-ref-stage", options);

        assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    });

    it("check url_int replacement", async () => {
        await request
            .get("/backend/getservicesdataset?dataset_id=1")
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                for (const service of res.body.results) {
                    if (service.type === "WMS") {
                        assert.deepStrictEqual(service.url_int, "http://server2.de/fachdaten/services/wms_stadtgruen", "should get success message");
                    }
                    else if (service.type === "WFS") {
                        assert.deepStrictEqual(service.url_int, "http://server2.de/fachdaten/services/wfs_stadtgruen", "should get success message");
                    }
                    else if (service.type === "OAF") {
                        assert.deepStrictEqual(service.url_int, "http://server2.de/ldproxy/stadtgruen", "should get success message");
                    }
                }
            });
    });

    it("deploy prod config deegree wms", async () => {
        await request
            .post("/backend/deployserviceconfig")
            .send(testData.deployserviceconfig.prod)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {newStatus: "veröffentlicht", "services": {
                    "1": {
                        "serviceStatus": {
                            "folder": "fachdaten",
                            "id": 1,
                            "server": "server3",
                            "status_dev": false,
                            "status_prod": true,
                            "status_stage": true,
                            "url_ext": "https://api.de/wms_stadtgruen",
                            "url_int": "http://server3.de/fachdaten/services/wms_stadtgruen"
                        },
                        "status": "success"
                    },
                    "2": {
                        "serviceStatus": {
                            "folder": "fachdaten",
                            "id": 2,
                            "server": "server3",
                            "status_dev": false,
                            "status_prod": true,
                            "status_stage": true,
                            "url_ext": "https://api.de/wfs_stadtgruen",
                            "url_int": "http://server3.de/fachdaten/services/wfs_stadtgruen"
                        },
                        "status": "success"
                    },
                    "3": {
                        "serviceStatus": {
                            "folder": "ldproxy",
                            "id": 3,
                            "server": "server3",
                            "status_dev": false,
                            "status_prod": true,
                            "status_stage": true,
                            "url_ext": "https://api.de/datasets/v1/stadtgruen",
                            "url_int": "http://server3.de/ldproxy/stadtgruen"
                        },
                        "status": "success"
                    }
                }}, "should get success message");
            });
    });

    it("validate prod out-path check-sum", async () => {
        const options = {
            folders: {
                ignoreRootName: true
            }
        };

        const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
        const hashref = await hashElement("./basicVectorWorkflow/api-configs-ref-prod", options);

        assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    });

    it("delete layer ldproxy oaf collection 2", async () => {
        await request
            .post("/backend/deletelayer")
            .send(testData.deletelayer.oaf_layer_1)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("delete layer ldproxy oaf collection 3", async () => {
        await request
            .post("/backend/deletelayer")
            .send(testData.deletelayer.oaf_layer_2)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("delete layer deegree wfs collection 2", async () => {
        await request
            .post("/backend/deletelayer")
            .send(testData.deletelayer.wfs_layer_1)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("delete service ldproxy oaf", async () => {
        await request
            .post("/backend/deleteservice")
            .send(testData.deleteservice.ldproxy_oaf)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("validate after oaf delete check-sum", async () => {
        const options = {
            folders: {
                ignoreRootName: true
            }
        };

        const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
        const hashref = await hashElement("./basicVectorWorkflow/api-configs-ref-delete-oaf", options);

        assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    });

    it("delete dataset", async () => {
        await request
            .post("/backend/deletedataset")
            .send(testData.deletedataset)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", noServices: false}, "should get success message");
            });
    });

    it("validate after dataset delete check-sum", async () => {
        const options = {
            folders: {
                ignoreRootName: true
            }
        };

        const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
        const hashref = await hashElement("./basicVectorWorkflow/api-configs-ref-delete-dataset", options);

        assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    });
});
