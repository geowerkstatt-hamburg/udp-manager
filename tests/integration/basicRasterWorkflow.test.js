require("dotenv").config();
const express = require("express");
const app = express();
const axios = require("axios");
const expressLoader = require("../../backend/loaders/express.loader");
const {db_udpm, db_ext} = require("../../backend/db");
const httpClient = require("supertest");
const assert = require("assert");
const {ConfiguratorFactory} = require("../../backend/configurator");
const testData = require("./basicRasterWorkflow/testData");
const fs = require("fs").promises;
const {hashElement} = require("folder-hash");

describe("Basic Workflow - raster data with mapserver services", async () => {

    expressLoader(app, db_udpm, db_ext, axios, null, ConfiguratorFactory, null);
    const request = httpClient(app);

    it("clear out path", async () => {
        await fs.rm(process.env.CONFIG_OUT_PATH, {recursive: true, force: true});
    });

    it("clear DB", async () => {
        await request
            .get("/backend/cleardb")
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success response message");
            });
    });

    it("init DB Schema", async () => {
        await request
            .get("/backend/initdbschema")
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, "Initialisierung erfolgreich!", "should get success response message");
            });
    });

    it("add Metadata Catalog", async () => {
        await request
            .post("/backend/addconfigmetadatacatalog")
            .send(testData.addconfigmetadatacatalog)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, [], "should get empty array");
            });
    });

    it("add Dataset", async () => {
        await request
            .post("/backend/savedataset")
            .send(testData.savedataset)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get dataset id 1");
            });
    });

    it("add visual Collection", async () => {
        await request
            .post("/backend/savecollectionsdataset")
            .send(testData.savecollectionsdataset.visual)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get collection id 1");
            });
    });

    it("add group Collection", async () => {
        await request
            .post("/backend/savecollectionsdataset")
            .send(testData.savecollectionsdataset.group)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 2}, "should get collection id 2");
            });
    });

    it("add mapserver wms service", async () => {
        await request
            .post("/backend/saveservice")
            .send(testData.saveservice.wms_mapserver)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get service id 1");
            });
    });

    it("add layer mapserver wms collection 1", async () => {
        await request
            .post("/backend/savelayer")
            .send(testData.savelayer.service1_collection1)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get layer id 1");
            });
    });

    it("add layer mapserver wms collection 2", async () => {
        await request
            .post("/backend/savelayer")
            .send(testData.savelayer.service1_collection2)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 2}, "should get layer id 2");
            });
    });

    it("generate config mapserver wms", async () => {
        await request
            .post("/backend/generateconfig")
            .send(testData.generateconfig)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {newStatus: "in Bearbeitung", services: {1: {configsEdited: false, status: "success"}}}, "should get success message");
            });
    });

    it("validate out-path check-sum", async () => {
        const options = {
            folders: {
                ignoreRootName: true
            }
        };

        const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
        const hashref = await hashElement("./basicRasterWorkflow/api-configs-ref-dev", options);

        assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    });

    it("deploy stage config mapseerver wms", async () => {
        await request
            .post("/backend/deployserviceconfig")
            .send(testData.deployserviceconfig.stage)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {newStatus: "vorveröffentlicht", "services": {
                    "1": {
                        "serviceStatus": {
                            "folder": "",
                            "id": 1,
                            "server": "server2",
                            "status_dev": false,
                            "status_prod": false,
                            "status_stage": true,
                            "url_ext": "https://api.de/wms_stadtgruen",
                            "url_int": "http://server2.de/wms_stadtgruen"
                        },
                        "status": "success"
                    }
                }}, "should get success message");
            });
    });

    it("validate stage out-path check-sum", async () => {
        const options = {
            folders: {
                ignoreRootName: true
            }
        };

        const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
        const hashref = await hashElement("./basicRasterWorkflow/api-configs-ref-stage", options);

        assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    });

    it("check url_int replacement", async () => {
        await request
            .get("/backend/getservicesdataset?dataset_id=1")
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body.results[0].url_int, "http://server2.de/wms_stadtgruen", "should get success message");
            });
    });

    it("deploy prod config mapserver wms", async () => {
        await request
            .post("/backend/deployserviceconfig")
            .send(testData.deployserviceconfig.prod)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {newStatus: "veröffentlicht", "services": {
                    "1": {
                        "serviceStatus": {
                            "folder": "",
                            "id": 1,
                            "server": "server3",
                            "status_dev": false,
                            "status_prod": true,
                            "status_stage": true,
                            "url_ext": "https://api.de/wms_stadtgruen",
                            "url_int": "http://server3.de/wms_stadtgruen"
                        },
                        "status": "success"
                    }
                }}, "should get success message");
            });
    });

    it("validate prod out-path check-sum", async () => {
        const options = {
            folders: {
                ignoreRootName: true
            }
        };

        const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
        const hashref = await hashElement("./basicRasterWorkflow/api-configs-ref-prod", options);

        assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    });

    it("delete layer mapserver wms collection 1", async () => {
        await request
            .post("/backend/deletelayer")
            .send(testData.deletelayer.wms_layer_1)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("delete layer mapserver wms collection 2", async () => {
        await request
            .post("/backend/deletelayer")
            .send(testData.deletelayer.wms_layer_2)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("delete service mapserver wms", async () => {
        await request
            .post("/backend/deleteservice")
            .send(testData.deleteservice.mapserver_wms)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success message");
            });
    });

    it("validate after wms delete check-sum", async () => {
        const options = {
            folders: {
                ignoreRootName: true
            }
        };

        const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
        const hashref = await hashElement("./basicRasterWorkflow/api-configs-ref-delete-wms", options);

        assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    });

    // it("delete dataset", async () => {
    //     await request
    //         .post("/backend/deletedataset")
    //         .send(testData.deletedataset)
    //         .timeout(5000)
    //         .expect(200)
    //         .expect((res) => {
    //             assert.deepStrictEqual(res.body, {status: "success", noServices: false}, "should get success message");
    //         });
    // });

    // it("validate after dataset delete check-sum", async () => {
    //     const options = {
    //         folders: {
    //             ignoreRootName: true
    //         }
    //     };

    //     const hash = await hashElement(process.env.CONFIG_OUT_PATH, options);
    //     const hashref = await hashElement("./basicRasterWorkflow/api-configs-ref-delete-dataset", options);

    //     assert.deepStrictEqual(hash.hash, hashref.hash, "should get correct check-sum");
    // });
});
