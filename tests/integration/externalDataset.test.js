require("dotenv").config();
const express = require("express");
const app = express();
const axios = require("axios");
const expressLoader = require("../../backend/loaders/express.loader");
const {db_udpm, db_ext} = require("../../backend/db");
const httpClient = require("supertest");
const assert = require("assert");
const testData = require("./externalDataset/testData");

describe("External Dataset - import collections", async () => {

    expressLoader(app, db_udpm, db_ext, axios, null, null, null);
    const request = httpClient(app);
    const {url} = request.get("/");

    testData.getservicecollections.wms_external.wms.url = url + "testresources/wms.xml";
    testData.getservicecollections.wfs_external.wfs.url = url + "testresources/wfs.xml";

    it("clear DB", async () => {
        await request
            .get("/backend/cleardb")
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success"}, "should get success response message");
            });
    });

    it("init DB Schema", async () => {
        await request
            .get("/backend/initdbschema")
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, "Initialisierung erfolgreich!", "should get success response message");
            });
    });

    it("add Metadata Catalog", async () => {
        await request
            .post("/backend/addconfigmetadatacatalog")
            .send(testData.addconfigmetadatacatalog)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, [], "should get empty array");
            });
    });

    it("add Dataset", async () => {
        await request
            .post("/backend/savedataset")
            .send(testData.savedataset)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get dataset id 1");
            });
    });

    it("add external wms service", async () => {
        await request
            .post("/backend/saveservice")
            .send(testData.saveservice.wms_external)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 1}, "should get service id 1");
            });
    });

    it("add external wfs service", async () => {
        await request
            .post("/backend/saveservice")
            .send(testData.saveservice.wfs_external)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {status: "success", id: 2}, "should get service id 2");
            });
    });

    it("import wms collections", async () => {
        await request
            .post("/backend/getservicecollections")
            .send(testData.getservicecollections.wms_external)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {
                    status: "success",
                    result_data: {
                        wms: {
                            title: "WMS 100 Jahre Stadtgruen",
                            fees: "Datenlizenz Deutschland Namensnennung 2.0, Quellenvermerk: Freie und Hansestadt Hamburg, Behörde für Stadtentwicklung und Wohnen",
                            access_constraints: "Es gelten keine Zugriffsbeschränkungen, Hamburgisches Geodateninfrastrukturgesetz (HmbGDIG)",
                            inspire_ext_cap: true,
                            collection_list: [
                                {
                                    authority_url: true,
                                    group_layer: true,
                                    identifier: true,
                                    metadata_uuid: "",
                                    name: "theme_hh_stadtgruen",
                                    namespace: "",
                                    output_format_wms: "image/png",
                                    scale_max: "2500000",
                                    scale_min: "0",
                                    service_id_wms: 1,
                                    service_name_wms: "WMS 100 Jahre Stadtgrün - Stadtpark und Volkspark",
                                    service_type: "WMS",
                                    title: "wms_hh_stadtgruen",
                                    type: "groupLayer"
                                },
                                {
                                    name: "poi",
                                    title: "100 Jahre Stadtgruen Icons",
                                    metadata_uuid: "4E67DF32-AAC0-4410-A215-4110F8D50BBD",
                                    authority_url: true,
                                    identifier: true,
                                    group_layer: false,
                                    namespace: "",
                                    output_format_wms: "image/png",
                                    scale_max: "2500000",
                                    scale_min: "0",
                                    type: "layer",
                                    service_id_wms: 1,
                                    service_name_wms: "WMS 100 Jahre Stadtgrün - Stadtpark und Volkspark",
                                    service_type: "WMS"
                                }
                            ],
                            epsg_list: [
                                "CRS:84",
                                "EPSG:4326",
                                "EPSG:4258",
                                "EPSG:25832",
                                "EPSG:25833",
                                "EPSG:31467",
                                "EPSG:3857",
                                "EPSG:3044",
                                "EPSG:3034",
                                "EPSG:3035"
                            ],
                            version_check: true
                        }
                    }
                }, "should get collection list");
            });
    });

    it("import wfs collections", async () => {
        await request
            .post("/backend/getservicecollections")
            .send(testData.getservicecollections.wfs_external)
            .timeout(5000)
            .expect(200)
            .expect((res) => {
                assert.deepStrictEqual(res.body, {
                    "status": "success",
                    "result_data": {
                        "wfs": {
                            "title": "WFS 100 Jahre Stadtgruen",
                            "fees": "Datenlizenz Deutschland Namensnennung 2.0, Quellenvermerk: Freie und Hansestadt Hamburg, Behörde für Umwelt, Klima, Energie und Agrarwirtschaft (BUKEA)",
                            "accessconstraints_cap": "Es gelten keine Zugriffsbeschränkungen",
                            "inspire_ext_cap": false,
                            "collection_list": [
                                {
                                    "name": "strecken",
                                    "title": "strecken",
                                    "metadata_uuid": "4E67DF32-AAC0-4410-A215-4110F8D50BBD",
                                    "authority_url": false,
                                    "identifier": false,
                                    "group_layer": false,
                                    "namespace": "app=http://www.deegree.org/app",
                                    "output_format_wfs": "XML",
                                    "scale_max": "2500000",
                                    "scale_min": "0",
                                    "type": "layer",
                                    "service_id_wfs": 2,
                                    "service_name_wfs": "WFS 100 Jahre Stadtgrün - Stadtpark und Volkspark",
                                    "service_type": "WFS"
                                },
                                {
                                    "name": "umring",
                                    "title": "umring",
                                    "metadata_uuid": "4E67DF32-AAC0-4410-A215-4110F8D50BBD",
                                    "authority_url": false,
                                    "identifier": false,
                                    "group_layer": false,
                                    "namespace": "app=http://www.deegree.org/app",
                                    "output_format_wfs": "XML",
                                    "scale_max": "2500000",
                                    "scale_min": "0",
                                    "type": "layer",
                                    "service_id_wfs": 2,
                                    "service_name_wfs": "WFS 100 Jahre Stadtgrün - Stadtpark und Volkspark",
                                    "service_type": "WFS"
                                }
                            ],
                            "epsg_list": [
                                "EPSG:25832",
                                "EPSG:25833",
                                "EPSG:4326",
                                "EPSG:4258",
                                "EPSG:31467",
                                "EPSG:3857",
                                "EPSG:3044",
                                "EPSG:3034",
                                "EPSG:3035"
                            ],
                            "version_check": true
                        }
                    }
                }, "should get service id 2");
            });
    });
});
