const axios = require("axios");
const {db_udpm, db_ext} = require("../../backend/db/index.js");
const ConfigurationDataGen = require("../../backend/utils/configuration_data_gen.util.js");
const managementService = require("../../backend/services/management.service.js")({axios, db_udpm});
const {ConfiguratorFactory} = require("../../backend/configurator/index.js");
const servicesService = require("../../backend/services/services.service.js")({db_udpm, db_ext, managementService, ConfiguratorFactory});
const ConfigurationDataValidator = require("../../backend/utils/configuration_data_validate.util.js");
const Logger = require("../../backend/utils/logger.util.js");

async function retrieveConfigData (id) {
    
    // get data from db
    const db_data = await db_udpm.exec.getConfigDataFromDb({service_id: id}); //  stadtgrün wfs=258 und wms=136

    // look up source data types
    const db_data_types = await servicesService._getDataTypesFromDb(db_data);

    // get list of related service config files that were manually edited (won't be overwritten)
    const configsEdited = await db_udpm.exec.getServiceConfigsEdited({service_id: id})

    // assemble generic data object from above db responses
    const config_data_object = ConfigurationDataGen.generateConfigurationDataObject(db_data, configsEdited, db_data_types);
    // check implementation exceptions and validate schemas
    // NOTE: try catch block is required in an async function to ensure Exceptions are properly raised
    try {
        
        // UNCOMMENT any of below data lines to trigger exceptions
        //config_data_object.service.type = "WFS-T";

        // UNCOMMENT any of below lines to trigger various validatio errors
        //config_data_object.dataset.primaryKey = null; 
        //config_data_object.dataset.unerwartetesAttribut = 'hey';
        // config_data_object.service.type = undefined;

        // UNCOMMENT below section to trigger validation error when attribute map is missing
        // const {collections} = config_data_object
        // for (const collectionName in collections) {
        //     const collection = collections[collectionName];
        //     collection.attributeMap = null;
        // }
   
        ConfigurationDataGen.checkImplementationExceptions(config_data_object) 

        const firstCollection = db_data[0]
        if (firstCollection.dataset_store_type === "Vektor") {
            //ConfigurationDataValidator.validate(config_data_object);
        }

    } catch (error) {
        Logger.error("services.service - generateConfig:");
        Logger.error(error);
        return {error: true, message: error.message};
    }

    // store configs
    const configurator = ConfiguratorFactory.create(config_data_object.software.name);
    configurator.storeService(config_data_object);
}

// Generate configs, e.g. abfall recycling service IDs in QS - oaf=4770 ; wms=1598; wfs: 1660
const serviceIds = [5230] // 136, 258, 4792
for (let i = 0; i < serviceIds.length; i++ ) {
    retrieveConfigData(serviceIds[i]);
}



