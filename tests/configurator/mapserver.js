const axios = require("axios");
const {db_udpm, db_ext} = require("../../backend/db/index.js");
const ConfigurationDataGen = require("../../backend/utils/configuration_data_gen.util");
const managementService = require("../../backend/services/management.service")({axios, db_udpm});
const {ConfiguratorFactory} = require("../../backend/configurator/index.js");
const servicesService = require("../../backend/services/services.service")({db_udpm, db_ext, managementService, ConfiguratorFactory});
// const ConfigurationDataValidator = require("../../backend/utils/configuration_data_validate.util");
// const Logger = require("../../backend/utils/logger.util");

async function retrieveConfigData (id) {

    // assemble generic data object from above db responses
    const db_data = await db_udpm.exec.getConfigDataFromDb({service_id: id});
    const db_data_types = await servicesService._getDataTypesFromDb(db_data);
    const configsEdited = await db_udpm.exec.getServiceConfigsEdited({service_id: id});
    const data = ConfigurationDataGen.generateConfigurationDataObject(db_data, configsEdited, db_data_types);

    // store configs
    const configurator = ConfiguratorFactory.create(data.software.name);

    configurator.storeService(data);
}

// Digitale Orthophotos (belaubt) Hamburg: WMS Service ID
const serviceIds = [3021];

for (let i = 0; i < serviceIds.length; i++) {
    retrieveConfigData(serviceIds[i]);
}
