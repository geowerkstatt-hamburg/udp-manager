const axios = require("axios");
const {db_udpm, db_ext} = require("../../backend/db/index.js");
const ConfigurationDataGen = require("../../backend/utils/configuration_data_gen.util");
const managementService = require("../../backend/services/management.service")({axios, db_udpm});
const {ConfiguratorFactory} = require("../../backend/configurator/index.js");
const servicesService = require("../../backend/services/services.service")({db_udpm, db_ext, managementService, ConfiguratorFactory});

//Todo remove redundancy from other test ldproxy.js
async function retrieveConfigData (id) {
    // get data from db
    console.log('Processing', id)
    try{
        const db_data = await db_udpm.exec.getConfigDataFromDb({service_id: id}); //  stadtgrün wfs=258 und wms=136
            //console.log('I am db data', db_data)

        // look up source data types
        //const me = this;
        const db_data_types = await servicesService._getDataTypesFromDb(db_data);
        // assemble generic data object from above db responses
        //console.log('I am db_data_types for', id, db_data_types)

        const configsEdited = await db_udpm.exec.getServiceConfigsEdited({service_id: id})

        const data = ConfigurationDataGen.generateConfigurationDataObject(db_data, configsEdited, db_data_types);
        // store configs
        //console.log('I am data', data)

        const configurator = ConfiguratorFactory.create(data.software.name);
        //console.log('I am data', data)
        configurator.storeService(data);
    } catch (error){
        console.log('Error!', id, error)
    }

}

//Generate for all services
//test_services.forEach(service => retrieveConfigData(service.id))

//Generate for one service
async function getOafServices () {
    // Datasets to be migrated
    // Defined by SQL Query found here:
    // backend\db\sql\data\get_datasets_for_oaf.sql
    const services = await db_udpm.exec.getServicesForOaf();
    return services
}
getOafServices()
.then((services) => {
    services.forEach((service => {
        
        try{
            //retrieveConfigData(service.service_id)
            console.log(service.service_id)
        } catch (errror){
            console.log('I am an error!', error, service.service_id)
        }
        }));
    })

