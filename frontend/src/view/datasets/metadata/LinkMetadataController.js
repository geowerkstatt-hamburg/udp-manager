Ext.define("UDPManager.view.datasets.metadata.LinkMetadataController", {
    extend: "Ext.app.ViewController",
    alias: "controller.linkmetadata",

    onSaveMetadataLink: function () {
        const scope = Ext.getCmp("linkmetadata-window").scope;
        const datasetsgrid = Ext.getCmp("linkmetadata-grid");

        if (datasetsgrid.getSelectionModel().getSelection()[0] !== undefined) {
            if (scope === "dataset") {
                const selectedDataset = datasetsgrid.getSelectionModel().getSelection()[0].data;

                if (selectedDataset.md_id !== "") {
                    const selection = Ext.getCmp("datasetsourcecswname").getSelection();

                    Ext.getCmp("datasetmetadatalink").setValue("<a href=\"" + selection.data.show_doc_url + selectedDataset.md_id + "\" target=\"_blank\">Metadaten anzeigen</a>");
                    Ext.getCmp("deletedatasetmetadatacoupling").setDisabled(false);
                    Ext.getCmp("deletedatasetmetadatacoupling").setHidden(false);
                }

                const mb = Ext.MessageBox;

                mb.buttonText.yes = "ok";
                mb.buttonText.no = "abbrechen";

                mb.confirm("Hinweis", "Die Angaben zu Name, Beschreibung, Nutzungsbedingungen und Zugriffsbeschränkungen werden mit den Informationen aus dem Metadatenkatalog überschrieben.", function (btn) {
                    if (btn === "yes") {
                        Ext.getCmp("datasetdetailsform").getForm().setValues({
                            datasettitle: selectedDataset.title,
                            datasetdescription: selectedDataset.description,
                            datasetfees: selectedDataset.fees,
                            datasetfeesjson: selectedDataset.fees_json,
                            datasetaccessconstraints: selectedDataset.access_constraints,
                            datasetbbox: selectedDataset.bbox,
                            datasetrsid: selectedDataset.rs_id,
                            datasetmdid: selectedDataset.md_id,
                            datasetkeywords: selectedDataset.keywords,
                            datasetcatopendata: selectedDataset.cat_opendata,
                            datasetcatinspire: selectedDataset.cat_inspire,
                            datasetcathmbtg: selectedDataset.cat_hmbtg,
                            datasetcatorg: selectedDataset.cat_org,
                            datasetinspire: selectedDataset.inspire,
                            datasetcontactmail: selectedDataset.md_contact_mail
                        });
                    }
                });
            }
            else if (scope === "service") {
                const selectedDataset = datasetsgrid.getSelectionModel().getSelection()[0].data;

                if (selectedDataset.md_id !== "") {
                    const selection = Ext.getCmp("servicesourcecswname").getSelection();

                    Ext.getCmp("servicemetadatalink").setValue("<a href=\"" + selection.data.show_doc_url + selectedDataset.md_id + "\" target=\"_blank\">Metadaten anzeigen</a>");
                    Ext.getCmp("deleteservicemetadatacoupling").setDisabled(false);
                    Ext.getCmp("deleteservicemetadatacoupling").setHidden(false);
                }

                const mb = Ext.MessageBox;

                mb.buttonText.yes = "ok";
                mb.buttonText.no = "abbrechen";

                mb.confirm("Hinweis", "Die Angaben zu Titel, Beschreibung, Nutzungsbedingungen und Zugriffsbeschränkungen werden mit den Informationen aus dem Metadatenkatalog überschrieben.", function (btn) {
                    if (btn === "yes") {
                        Ext.getCmp("servicedetailsform").getForm().setValues({
                            servicetitle: selectedDataset.title,
                            servicedescription: selectedDataset.description,
                            servicefees: selectedDataset.fees,
                            servicefeesjson: selectedDataset.fees_json,
                            serviceaccessconstraints: selectedDataset.access_constraints,
                            servicemdid: selectedDataset.md_id,
                            servicekeywords: selectedDataset.keywords
                        });
                    }
                });
            }
        }

        Ext.getCmp("linkmetadata-window").close();
    }
});
