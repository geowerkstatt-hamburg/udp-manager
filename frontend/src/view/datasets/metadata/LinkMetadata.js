Ext.define("UDPManager.view.datasets.metadata.LinkMetadata", {
    extend: "Ext.window.Window",

    alias: "datasets.metadata.linkmetadata",

    id: "linkmetadata-window",
    height: 400,
    width: 400,
    title: "Metadatensatz Wählen",
    resizable: false,
    controller: "linkmetadata",
    scope: null,
    items: [
        {
            xtype: "grid",
            store: "MetadataSets",
            id: "linkmetadata-grid",
            height: 314,
            autoScroll: true,
            viewConfig: {
                enableTextSelection: true
            },
            columns: [{
                header: "Titel",
                dataIndex: "title",
                align: "left",
                flex: 1
            }],
            columnLines: true,
            selModel: {
                type: "checkboxmodel",
                mode: "SINGLE"
            }
        }
    ],
    buttons: [{
        text: "OK",
        tooltip: "Die UUID des gewählten Metadatensatzes wird eingefügt.",
        listeners: {
            click: "onSaveMetadataLink"
        }
    }],
    showWindow: function (scope) {
        this.scope = scope;

        this.show();
    }
});
