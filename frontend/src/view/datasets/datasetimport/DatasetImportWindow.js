Ext.define("UDPManager.view.datasets.datasetimport.DatasetImportWindow", {
    extend: "Ext.window.Window",
    id: "datasetimport-window",
    alias: "datasetimport.DatasetImportWindow",
    controller: "datasetimport",
    padding: "5 0 0 0",
    layout: "fit",
    width: 800,
    height: 300,
    modal: true,
    title: "Datensatz importieren",
    listeners: {
        close: "onWindowClose"
    },
    tbar: [
        {
            xtype: "combo",
            fieldLabel: "ext. UDP-Manager-Instanz:",
            store: "ConfigExtInstances",
            displayField: "name",
            allowBlank: false,
            id: "extinstancesourcecombo",
            editable: false,
            margin: "0 10 0 0",
            labelWidth: 200,
            width: 400
        },
        {
            xtype: "textfield",
            id: "datasetsearchfield",
            width: 200,
            tooltip: "Nach externem Datensatz suchen",
            enableKeyEvents: true,
            listeners: {
                "keypress": "onKeyPress"
            }
        },
        {
            text: "Datensatz suchen",
            handler: "onSearchDatasetClicked"
        }
    ],
    items: [
        {
            xtype: "grid",
            id: "found-datasets-grid",
            autoScroll: true,
            selectable: {
                columns: false,
                extensible: true
            },
            columnLines: true,
            columns: [
                {
                    dataIndex: "status",
                    width: 30,
                    align: "center",
                    sortable: false,
                    menuDisabled: true,
                    resizeable: false,
                    renderer: function (value) {
                        if (value === "prod") {
                            return "<div class=\"x-fa fa-circle greenIcon\"></div>";
                        }
                        else if (value === "stage") {
                            return "<div class=\"x-fa fa-circle yellowIcon\"></div>";
                        }
                        else if (value === "dev") {
                            return "<div class=\"x-fa fa-circle orangeIcon\"></div>";
                        }
                        else if (value === "test") {
                            return "<div class=\"x-fa fa-circle redIcon\"></div>";
                        }
                    }
                },
                {
                    text: "Datensatz ID",
                    dataIndex: "id",
                    flex: 0.2,
                    align: "left"
                },
                {
                    text: "Titel",
                    dataIndex: "title",
                    flex: 0.5,
                    align: "left"
                },
                {
                    text: "Bearbeiter",
                    dataIndex: "last_edited_by",
                    flex: 0.3,
                    align: "left"
                }
            ],
            store: Ext.create("Ext.data.Store", {
            })
        }
    ],
    buttons: [
        {
            text: "Ausgewählten Datensatz importieren",
            tooltip: "Der ausgewählte Datensatz wird importiert.",
            listeners: {
                click: "onImportSelectedDataset"
            }
        }
    ]
});
