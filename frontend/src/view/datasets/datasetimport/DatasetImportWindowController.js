Ext.define("UDPManager.view.datasets.datasetimport.DatasetImportWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.datasetimport",

    onKeyPress: function (field, event) {
        if (event.getKey() === event.ENTER) {
            this.onSearchDatasetClicked();
        }
    },

    onWindowClose: function () {
        Ext.getCmp("found-datasets-grid").getStore().removeAll();
    },

    onSearchDatasetClicked: function () {
        const searchStr = Ext.getCmp("datasetsearchfield").getSubmitValue().toLowerCase();
        const datasetSourceName = Ext.getCmp("extinstancesourcecombo").getSubmitValue();
        const me = this;

        if (datasetSourceName !== "") {
            if (searchStr !== "") {
                me.searchDatasets(datasetSourceName, searchStr);
            }
            else {
                Ext.Msg.alert("Suche", "Suchfeld muss ausgefüllt werden!", Ext.emptyFn);
            }
        }
        else {
            Ext.Msg.alert("Datensatz Import", "Externe UDP-Manager-Instanz auswählen!", Ext.emptyFn);
        }
    },

    searchDatasets: function (datasetSourceName, searchStr) {
        const me = this;
        const extInstanceSourceCombo = Ext.getCmp("extinstancesourcecombo");
        const index = extInstanceSourceCombo.getStore().findExact("name", datasetSourceName);
        const record = extInstanceSourceCombo.getStore().getAt(index);

        Ext.Ajax.request({
            url: "backend/searchdatasetinextinstance",
            headers: {token: window.apiToken},
            jsonData: {
                searchStr: searchStr,
                connectionId: record.data.id
            },
            success: function (response) {
                if (response.responseText) {
                    me.addFoundDatasetsToGrid(response);
                }
                else {
                    Ext.Msg.alert("Suche fehlgeschlagen", "Suche fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Suche fehlgeschlagen", "Suche fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    addFoundDatasetsToGrid: function (response) {
        const res = Ext.decode(response.responseText);
        const grid = Ext.getCmp("found-datasets-grid");
        const foundDatasetsStore = grid.getStore();

        foundDatasetsStore.removeAll();

        if (Array.isArray(res)) {
            if (res.length > 0) {
                const datasets = [];

                for (const dataset of res) {
                    const double_ds = res.filter((ds) => ds.id === dataset.id);

                    if (double_ds.length === 1 && dataset.service_status_dev !== true) {
                        datasets.push(dataset);
                    }
                }

                if (datasets.length > 0) {
                    foundDatasetsStore.add(datasets);
                }
                else {
                    Ext.Msg.alert("Datensatzsuche", "Keine verknüpfte Schnittstellen darf den Status 'dev' haben!", Ext.emptyFn);
                }
            }
            else {
                Ext.Msg.alert("Datensatzsuche", "Kein Datensatz gefunden", Ext.emptyFn);
            }
        }
        else {
            Ext.Msg.alert("Datensatzsuche", "Kein Datensatz gefunden", Ext.emptyFn);
        }
    },

    onImportSelectedDataset: function () {
        const extInstanceSourceCombo = Ext.getCmp("extinstancesourcecombo");
        const datasetSourceName = extInstanceSourceCombo.getSubmitValue();
        const index = extInstanceSourceCombo.getStore().findExact("name", datasetSourceName);
        const connectionRecord = extInstanceSourceCombo.getStore().getAt(index);
        const grid = Ext.getCmp("found-datasets-grid");
        const selectedDataset = grid.getSelectionModel().getSelection();
        const me = this;

        selectedDataset.forEach(function (dataset) {
            const datasetIndex = Ext.getStore("Datasets").findExact("id", dataset.data.id);

            if (datasetIndex === -1) {
                me.importDataset(dataset, connectionRecord.data.id, false, false);
            }
            else {
                const mb = Ext.MessageBox;

                if (datasetIndex !== -1) {
                    const record = Ext.getStore("Datasets").getAt(datasetIndex);

                    mb.buttonText.yes = "Neue Datensatz ID erzeugen";
                    mb.buttonText.no = "Datensatz aktualisieren";
                    mb.confirm("Datensatz Importieren", "Ein Datensatz mit der ID " + dataset.data.id + " existiert bereits für " + record.data.title + ". Neue IDs für erzeugen oder den bestehenden Datensatz aktualisieren?", function (btn) {
                        if (btn === "yes") {
                            me.importDataset(dataset, connectionRecord.data.id, true, false);
                        }
                        if (btn === "no") {
                            me.importDataset(dataset, connectionRecord.data.id, false, true);
                        }
                    });
                }
            }
        });
    },

    importDataset: function (dataset, connectionId, createNewIds, update) {
        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("datasetimport-window");

        Ext.Ajax.request({
            url: "backend/importdataset",
            headers: {token: window.apiToken},
            jsonData: {
                id: dataset.data.id,
                last_edited_by: window.auth_user,
                createNewIds: createNewIds,
                update: update,
                connectionId: connectionId
            },
            timeout: 60000,
            success: function (response) {
                const jsonobj = Ext.decode(response.responseText);

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();

                if (jsonobj.status === "error") {
                    Ext.Msg.alert("Import fehlgeschlagen", "Import des Datensatzes fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                }
                else {
                    if (jsonobj.hasOwnProperty("id")) {
                        Ext.getStore("Datasets").load({
                            callback: function () {
                                const new_dataset = Ext.getStore("Datasets").findRecord("id", jsonobj.id, 0, false, false, true);
                                const dataset_grid = Ext.getCmp("grid_dataset");
                                const row_dataset = dataset_grid.store.indexOf(new_dataset);

                                dataset_grid.getSelectionModel().deselect(row_dataset);
                                dataset_grid.getSelectionModel().select(row_dataset);
                                dataset_grid.ensureVisible(row_dataset);
                            }
                        });

                        Ext.getStore("ConfigDbConnections").load();

                        Ext.Msg.alert("Import erfolgreich", "Import des Datensatzes " + dataset.data.title + " (ID: " + jsonobj.id + ") erfolgreich abgeschlossen!", Ext.emptyFn);
                    }
                    else {
                        Ext.Msg.alert("Import fehlgeschlagen", "Import fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                    }
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Import fehlgeschlagen", "Import des Datensatzes fehlgeschlagen. Logs prüfen.", Ext.emptyFn);
                console.log(Ext.decode(response.responseText));
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                Ext.getCmp("datasetimport-window").unmask();
            }
        });
    }
});
