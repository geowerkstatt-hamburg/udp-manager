Ext.define("UDPManager.view.datasets.DatasetListController", {
    extend: "Ext.app.ViewController",

    alias: "controller.datasetlist-controller",

    onGridItemSelected: function (sender, record) {
        const editMode = (record.data.status === "in Bearbeitung" || record.data.status === "veröffentlicht - in Bearbeitung" || record.data.external) && !window.read_only;

        if (editMode || (record.data.external && !window.read_only)) {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsWritable();
            Ext.getCmp("dataseteditnotes").setHtml("<span style='color:#666'>Es können alle Felder bearbeitet werden</span>");
            Ext.getCmp("collectioneditnotes").setHtml("<span style='color:#666'>Es können alle Felder bearbeitet werden</span>");
            Ext.getCmp("serviceeditnotes").setHtml("<span style='color:#666'>Es können alle Felder bearbeitet werden</span>");
            Ext.getCmp("servicelayereditnotes").setHtml("<span style='color:#666'>Es können alle Felder bearbeitet werden</span>");
            Ext.getCmp("layereditnotes").setHtml("<span style='color:#666'>Es können alle Felder bearbeitet werden</span>");
        }
        else if (!record.data.external && !window.read_only) {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsReadOnlyEditorial();
            Ext.getCmp("dataseteditnotes").setHtml("<span style='color:#666'>Es können nur Felder ohne hervorgehobenem Label bearbeitet werden</span>");
            Ext.getCmp("collectioneditnotes").setHtml("<span style='color:#666'>Es können nur Felder ohne hervorgehobenem Label bearbeitet werden</span>");
            Ext.getCmp("serviceeditnotes").setHtml("<span style='color:#666'>Es können keine Felder bearbeitet werden</span>");
            Ext.getCmp("servicelayereditnotes").setHtml("<span style='color:#666'>Es können alle Felder bearbeitet werden</span>");
            Ext.getCmp("layereditnotes").setHtml("<span style='color:#666'>Es können alle Felder bearbeitet werden</span>");
        }
        else {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsReadOnly();
            Ext.getCmp("dataseteditnotes").setHtml("<span style='color:#666'>Es können kein Felder bearbeitet werden</span>");
            Ext.getCmp("collectioneditnotes").setHtml("<span style='color:#666'>Es können kein Felder bearbeitet werden</span>");
            Ext.getCmp("serviceeditnotes").setHtml("<span style='color:#666'>Es können kein Felder bearbeitet werden</span>");
            Ext.getCmp("servicelayereditnotes").setHtml("<span style='color:#666'>Es können keine Felder bearbeitet werden</span>");
            Ext.getCmp("layereditnotes").setHtml("<span style='color:#666'>Es können keine Felder bearbeitet werden</span>");
        }

        this.getViewModel().set("form_datasets", record);

        if (record.data.status === "veröffentlicht") {
            Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetStatusIcon\" data-qtip=\"veröffentlicht\"></div>");
        }
        else if (record.data.status === "veröffentlicht - vorveröffentlicht") {
            Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetYellowIconGradient\" data-qtip=\"veröffentlicht - vorveröffentlicht\"></div>");
        }
        else if (record.data.status === "veröffentlicht - in Bearbeitung") {
            Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetOrangeIconGradient\" data-qtip=\"veröffentlicht - in Bearbeitung\"></div>");
        }
        else if (record.data.status === "vorveröffentlicht") {
            Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetYellowIcon\" data-qtip=\"vorveröffentlicht\"></div>");
        }
        else {
            Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetOrangeIcon\" data-qtip=\"in Bearbeitung\"></div>");
        }

        if (!record.data.md_id) {
            Ext.getCmp("datasetmdid").setValue("");
        }

        Ext.getCmp("datasetcollectiondetailsform").getForm().reset();
        Ext.getCmp("layerdetailsform").getForm().reset();
        Ext.getCmp("servicedetailsform").getForm().reset();

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllLayerDetailsFieldsetsHidden();
        UDPManager.app.getController("UDPManager.controller.PublicFunctions").emptyLayerJsonField();

        Ext.getStore("ETLProcesses").removeAll();
        Ext.getStore("ETLProcessCollections").removeAll();
        Ext.getStore("LayersCollection").removeAll();
        Ext.getStore("LayerPortals").removeAll();
        Ext.getStore("ChangeLog").removeAll();
        Ext.getCmp("etlprocessmapcollection").setValue("");
        Ext.getCmp("etlprocessmapcollection").setDisabled(true);
        Ext.getCmp("etlprocessmapallcollections").setDisabled(true);

        Ext.getStore("ETLProcesses").load({
            params: {dataset_id: record.get("id")},
            callback: function (records) {
                if (records.length === 0) {
                    Ext.getCmp("delete_etlprocess").setDisabled(true);
                    Ext.getCmp("etlprocessdescription").setValue("");
                    Ext.getCmp("delete_etlprocess").setDisabled(true);
                    Ext.getCmp("save_etlprocess").setDisabled(true);
                }
            }
        });

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").resetStoreLoadState();

        if (parseInt(record.get("id")) !== 0) {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("datasetstab");
        }

        Ext.getStore("CollectionsDataset").load({
            params: {dataset_id: record.get("id"), dataset_status: record.get("status")},
            callback: function (records) {
                if (records.length === 0) {
                    Ext.getCmp("editattributeconfig").setDisabled(true);
                    Ext.getCmp("editstyle").setDisabled(true);
                    Ext.getCmp("editstylesldfile").setDisabled(true);

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").setStoreLoadState("layerscollections", true);
                }
                else {
                    if (record.data.store_type !== "Raster") {
                        Ext.getCmp("editattributeconfig").setDisabled(false);
                        Ext.getCmp("editstyle").setDisabled(false);
                        Ext.getCmp("editstylesldfile").setDisabled(false);
                        Ext.getCmp("etlprocessmapcollection").bindStore("CollectionsDataset");
                    }
                    else {
                        Ext.getCmp("etlprocessmapcollection").bindStore("CollectionsDataset");
                    }

                    let params = {};

                    if (window.location.href.indexOf("?") > 0) {
                        params = Ext.Object.fromQueryString(window.location.href.split("?")[1]);
                    }

                    if (params.collection_id) {
                        Ext.getCmp("dataset_main_tabs").setActiveTab(1);

                        const collection = Ext.getStore("CollectionsDataset").findRecord("id", params.collection_id, 0, false, false, true);

                        if (collection) {
                            const row_collection = Ext.getStore("CollectionsDataset").indexOf(collection);
                            const dataset_collections_grid = Ext.getCmp("grid_datasetcollections");

                            dataset_collections_grid.getSelectionModel().select(row_collection);
                            dataset_collections_grid.ensureVisible(row_collection);
                        }
                    }

                    if (record.data.status === "in Bearbeitung" || record.data.status === "veröffentlicht - in Bearbeitung") {
                        Ext.getCmp("datasetcollectionchangestab").setDisabled(false);

                        Ext.Ajax.request({
                            url: "backend/getcollectionschangelog",
                            method: "GET",
                            headers: {token: window.apiToken},
                            params: {
                                dataset_id: record.get("id")
                            },
                            success: function (response) {
                                const response_json = Ext.decode(response.responseText, true);

                                if (response_json.status === "change detected") {
                                    Ext.getCmp("datasetcollectionchanges").setHtml(response_json.collection_diff);
                                    Ext.getCmp("datasetcollectionchangeslabel").setHtml(`Letzter geloggter Stand: ${response_json.timestamp}, editiert von: ${response_json.editor}`);

                                    Ext.getStore("CollectionsDataset").each((collection) => {
                                        for (let i = 0; i < response_json.collection_diff_json.length; i++) {
                                            if (Ext.getStore("CollectionsDataset").indexOf(collection) === i && ((typeof Object.values(response_json.collection_diff_json[i])[0] === "object" || Object.values(response_json.collection_diff_json[i])[0] === "Collection neu hinzugefügt"))) {
                                                collection.data.collection_changed = true;
                                            }
                                        }
                                    });
                                }
                                else {
                                    Ext.getCmp("datasetcollectionchanges").setHtml("");
                                    Ext.getCmp("datasetcollectionchangeslabel").setHtml("Kein Vergleich möglich - Keine alte Version der Attributtabelle vorhanden!");
                                }
                            },
                            failure: function (response) {
                                console.log(Ext.decode(response.responseText));
                            }
                        });
                    }
                    else {
                        Ext.getCmp("datasetcollectionchangestab").setDisabled(true);
                    }
                }
            }
        });

        Ext.getStore("JiraTickets").load({
            params: {dataset_id: record.get("id")}
        });

        Ext.getStore("ServicesDataset").load({
            params: {dataset_id: record.get("id")},
            callback: function (records) {
                if (records.length === 0) {
                    Ext.getCmp("importcollection").setDisabled(true);
                    Ext.getCmp("importcollectionexternal").setDisabled(true);

                    if (editMode) {
                        Ext.getCmp("addnewservice").setDisabled(false);
                        Ext.getCmp("linkservice").setDisabled(false);
                    }

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").setStoreLoadState("layersservices", true);
                }
                else {
                    if (!window.read_only) {
                        if (UDPManager.Configs.getModules().externalCollectionImport && !record.data.external && editMode) {
                            Ext.getCmp("importcollection").setHidden(false);
                            Ext.getCmp("importcollection").setDisabled(false);
                            Ext.getCmp("importcollectionexternal").setHidden(true);
                            Ext.getCmp("importcollectionexternal").setDisabled(true);
                        }
                        else if (UDPManager.Configs.getModules().externalCollectionImport && record.data.external && editMode) {
                            Ext.getCmp("importcollection").setHidden(true);
                            Ext.getCmp("importcollectionexternal").setDisabled(false);
                            Ext.getCmp("importcollectionexternal").setHidden(false);
                        }
                        else if (!UDPManager.Configs.getModules().externalCollectionImport && editMode) {
                            Ext.getCmp("importcollection").setHidden(false);
                            Ext.getCmp("importcollection").setDisabled(false);
                            Ext.getCmp("importcollectionexternal").setHidden(true);
                        }
                        else {
                            Ext.getCmp("importcollection").setDisabled(true);
                            Ext.getCmp("importcollection").setHidden(false);
                            Ext.getCmp("importcollectionexternal").setHidden(true);
                        }

                        // const allServicesExternal = records.every((service) => service.data.external === true);

                        // if (allServicesExternal) {
                        //     UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsWritable();
                        // }
                    }
                    else {
                        Ext.getCmp("importcollection").setDisabled(true);
                        Ext.getCmp("importcollectionexternal").setHidden(true);
                    }

                    let params = {};

                    if (window.location.href.indexOf("?") > 0) {
                        params = Ext.Object.fromQueryString(window.location.href.split("?")[1]);
                    }

                    if (params.service_id) {
                        Ext.getCmp("dataset_main_tabs").setActiveTab(2);

                        const service = Ext.getStore("ServicesDataset").findRecord("id", params.service_id, 0, false, false, true);

                        if (service) {
                            const row_service = Ext.getStore("ServicesDataset").indexOf(service);
                            const dataset_services_grid = Ext.getCmp("grid_datasetservices");

                            dataset_services_grid.getSelectionModel().select(row_service);
                            dataset_services_grid.ensureVisible(row_service);
                        }
                    }
                }
            }
        });

        Ext.getStore("Comments").load({
            params: {dataset_id: record.get("id")},
            callback: function (records) {
                if (records.length > 0) {
                    const result = records.find((comment) => comment.data.important === true);

                    if (result) {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Wichtige Kommentare beachten!", true);
                    }
                }
            }
        });

        Ext.getStore("ChangeLog").load({
            params: {dataset_id: record.get("id")}
        });

        Ext.getStore("Contacts").clearFilter();

        // for (let i = 0; i < record.get("keywords").length; i++) {
        //     Ext.getStore("Keywords").add({text: record.get("keywords")[i]});
        // }

        if (record.data.md_id && record.data.metadata_catalog_id) {
            const csw = Ext.getStore("ConfigMetadataCatalogs").findRecord("id", record.data.metadata_catalog_id, 0, false, false, true);

            if (csw) {
                Ext.getCmp("datasetmdid").setValue(record.data.md_id);
                Ext.getCmp("datasetsourcecswname").setValue(csw.data.name);
                Ext.getCmp("datasetmetadatalink").setValue("<a href=\"" + csw.data.show_doc_url + record.data.md_id + "\" target=\"_blank\">Metadaten aufrufen</a>");
                Ext.getCmp("deletedatasetmetadatacoupling").setDisabled(!editMode);
                Ext.getCmp("deletedatasetmetadatacoupling").setHidden(!editMode);
            }
        }
        else {
            Ext.getCmp("datasetsourcecswname").setValue();
            Ext.getCmp("datasetmetadatalink").setValue("<a>keine Metadaten vorhanden</a>");
            Ext.getCmp("deletedatasetmetadatacoupling").setDisabled(true);
            Ext.getCmp("deletedatasetmetadatacoupling").setHidden(true);
        }

        Ext.getCmp("datasetcollectionstab").setDisabled(false);
        Ext.getCmp("datasetservicestab").setDisabled(false);
        Ext.getCmp("datasetetltab").setDisabled(false);
        Ext.getCmp("datasetcommentstab").setDisabled(false);
        if (UDPManager.Configs.getModules().jiraTickets) {
            Ext.getCmp("datasetjiratab").setDisabled(false);
        }

        Ext.getCmp("collections_main_tabs").setActiveTab(0);

        if (record.data.metadata_catalog_id !== null) {
            Ext.getCmp("datasetrsid").setReadOnly(true);
            Ext.getCmp("datasetdescription").setReadOnly(true);
            Ext.getCmp("datasetkeywords").setReadOnly(true);
            Ext.getCmp("datasetaccessconstraints").setReadOnly(true);
            Ext.getCmp("datasetfees").setReadOnly(true);
            Ext.getCmp("datasetfeesjson").setReadOnly(true);
            Ext.getCmp("datasetcathmbtg").setReadOnly(true);
            Ext.getCmp("datasetcatinspire").setReadOnly(true);
            Ext.getCmp("datasetcatopendata").setReadOnly(true);
            Ext.getCmp("datasetcatorg").setReadOnly(true);
            Ext.getCmp("datasetinspire").setReadOnly(true);
            Ext.getCmp("datasetcontactmail").setReadOnly(true);
        }
        else {
            Ext.getCmp("datasetrsid").setReadOnly(false);
            Ext.getCmp("datasetdescription").setReadOnly(false);
            Ext.getCmp("datasetkeywords").setReadOnly(false);
            Ext.getCmp("datasetaccessconstraints").setReadOnly(false);
            Ext.getCmp("datasetfees").setReadOnly(false);
            Ext.getCmp("datasetfeesjson").setReadOnly(false);
            Ext.getCmp("datasetcathmbtg").setReadOnly(false);
            Ext.getCmp("datasetcatinspire").setReadOnly(false);
            Ext.getCmp("datasetcatopendata").setReadOnly(false);
            Ext.getCmp("datasetcatorg").setReadOnly(false);
            Ext.getCmp("datasetinspire").setReadOnly(false);
            Ext.getCmp("datasetcontactmail").setReadOnly(false);
        }

        Ext.getCmp("datasetdetailsform").setDisabled(false);
        Ext.getCmp("etldetailsform").setDisabled(true);
        Ext.getCmp("datasetcollectiondetailsform").setDisabled(true);
        Ext.getCmp("servicedetailsform").setDisabled(true);
        Ext.getCmp("layerdetailsform").setDisabled(true);
        Ext.getCmp("servicelayerdetailsform").setDisabled(true);

        Ext.getCmp("generateconfigbutton").setDisabled(true);
        Ext.getCmp("deploystageconfigbutton").setDisabled(true);
        Ext.getCmp("deployprodconfigbutton").setDisabled(true);
        Ext.getCmp("undeployprodconfigbutton").setDisabled(true);
        Ext.getCmp("duplicatecollection").setDisabled(true);
        Ext.getCmp("duplicateservice").setDisabled(true);
        Ext.getCmp("deleteservice").setDisabled(true);
        Ext.getCmp("deletelayerbutton").setDisabled(true);
        Ext.getCmp("layerjsonupdate").setDisabled(true);
        Ext.getCmp("layerjsonupdateall").setDisabled(true);
        Ext.getCmp("servicelayerjsonupdate").setDisabled(true);
        Ext.getCmp("servicelayerjsonupdateall").setDisabled(true);

        if (record.data.publish_date) {
            Ext.getCmp("datasetpublishdate").setHidden(false);
        }
        else {
            Ext.getCmp("datasetpublishdate").setHidden(true);
        }

        Ext.getCmp("shareservice").setDisabled(true);
        Ext.getCmp("sharecollection").setDisabled(true);
    },

    switchTab: function () {
        Ext.getCmp("dataset_main_tabs").setActiveTab(3);
        Ext.getCmp("dataset_main_tabs").setActiveTab(2);
        Ext.getCmp("dataset_main_tabs").setActiveTab(1);
        Ext.getCmp("dataset_main_tabs").setActiveTab(0);
        Ext.getCmp("collections_main_tabs").setActiveTab(1);
        Ext.getCmp("collections_main_tabs").setActiveTab(0);
        window.mainViewRendered = true;
    },

    onAddNewDataset: function () {
        Ext.getCmp("dataset_main_tabs").setActiveTab(0);

        const dataset = Ext.getStore("Datasets").findRecord("id", 0, 0, false, false, true);
        const db_connection_default_r = Ext.getStore("ConfigDbConnections").findRecord("use_as_default_r", true, 0, false, false, true);
        const db_connection_default_w = Ext.getStore("ConfigDbConnections").findRecord("use_as_default_w", true, 0, false, false, true);

        if (dataset) {
            Ext.getCmp("grid_dataset").getSelectionModel().select(dataset);
        }
        else {
            const rec = new UDPManager.model.UDPManagerModel({
                id: 0,
                title: "*NEU*",
                status: "in Bearbeitung",
                srs: UDPManager.Configs.getDefaultDatasetSrs(),
                store_type: "Vektor",
                last_edit_date: moment().format("YYYY-MM-DD HH:mm"),
                last_edited_by: window.auth_user,
                create_date: moment().format("YYYY-MM-DD HH:mm"),
                created_by: window.auth_user,
                db_connection_r_id: db_connection_default_r ? db_connection_default_r.data.id : null,
                db_connection_w_id: db_connection_default_w ? db_connection_default_w.data.id : null
            });
            const count = Ext.getStore("Datasets").count();

            Ext.getStore("Datasets").insert(count, rec);

            Ext.getCmp("grid_dataset").getSelectionModel().select(rec);
            Ext.getCmp("grid_dataset").ensureVisible(0);
            Ext.getCmp("datasetcollectionstab").setDisabled(true);
            Ext.getCmp("datasetservicestab").setDisabled(true);
            Ext.getCmp("datasetetltab").setDisabled(true);
            Ext.getCmp("datasetcommentstab").setDisabled(true);
            Ext.getCmp("datasetjiratab").setDisabled(true);
        }
    },

    onDeleteDataset: function () {
        Ext.getCmp("deletedataset").setDisabled(true);

        const mb = Ext.MessageBox;

        mb.buttonText.yes = "ja";
        mb.buttonText.no = "nein";

        mb.confirm("Löschen", "ACHTUNG: es werden alle verknüpften Collections, Layer und Schnittstellen gelöscht! <br> Kopplung zwischen Layer und Portalen geprüft? Wirklich ausführen?", function (btn) {
            if (btn === "yes") {

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("main-nav-tab-panel");

                Ext.Ajax.request({
                    url: "backend/deletedataset",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        id: Ext.getCmp("datasetid").getSubmitValue(),
                        last_edited_by: window.auth_user
                    },
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);

                        if (response_json.status === "success") {
                            if (response_json.noServices) {
                                Ext.MessageBox.alert("Hinweis", "Schnittstellen konnten auf Grund Verknüpfung mit mehreren Datensätzen nicht gelöscht werden!");
                            }

                            Ext.getStore("Datasets").load();
                            Ext.getCmp("datasetdetailsform").getForm().reset();
                            Ext.getCmp("datasetmetadatalink").setValue("Keine Metadaten verknüpft");
                            Ext.getCmp("deletedatasetmetadatacoupling").setDisabled(true);
                            Ext.getCmp("deletedatasetmetadatacoupling").setHidden(true);

                            if (UDPManager.Configs.getWebsocket()) {
                                window.socket.emit("dataset_details_changed");
                            }
                        }
                        else {
                            Ext.MessageBox.alert("Fehler", response_json.message);
                        }
                        Ext.getCmp("deletedataset").setDisabled(false);

                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Datensatz konnte nicht gelöscht werden");
                        Ext.getCmp("deletedataset").setDisabled(false);
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                    }
                });
            }
            else {
                Ext.getCmp("deletedataset").setDisabled(false);
            }
        });
    },

    onDuplicateDataset: function () {
        const formfieldvalues = Ext.getCmp("datasetdetailsform").getForm().getFieldValues();

        formfieldvalues.id = 0;
        formfieldvalues.title = formfieldvalues.title + " KOPIE";
        formfieldvalues.shortname = formfieldvalues.shortname + "_duplicate";

        const rec = new UDPManager.model.UDPManagerModel(formfieldvalues);
        const count = Ext.getStore("Datasets").count();

        Ext.getStore("Datasets").insert(count, rec);

        Ext.getCmp("grid_dataset").getSelectionModel().select(rec);
        Ext.getCmp("grid_dataset").ensureVisible(rec);
    },

    onFilter: function (btn, menuitem) {
        let type = "";

        if (menuitem.type.indexOf("responsible_party") > -1) {
            type = "responsible_party";
        }

        if (menuitem.type.indexOf("status") > -1) {
            type = "status";
        }

        if (menuitem.type.indexOf("alle") > -1) {
            Ext.getStore("Datasets").removeFilter("filter_" + type);
        }
        else {
            Ext.getStore("Datasets").addFilter(new Ext.util.Filter({
                id: "filter_" + type,
                filterFn: function (item) {
                    return item.get(type) === menuitem.text;
                }
            }));
        }
    },

    onChangeSort: function (checkbox, newValue) {
        const sorters = Ext.getStore("Datasets").getSorters();

        if (newValue) {
            sorters.remove("title");

            sorters.add({
                property: "last_edit_date",
                direction: "DESC"
            });

            sorters.add({
                property: "title",
                direction: "ASC"
            });
        }
        else {
            sorters.remove("last_edit_date");
        }
    },

    onShowMyDatasets: function (checkbox, newValue) {
        if (newValue) {
            Ext.getStore("Datasets").addFilter(new Ext.util.Filter({
                id: "filter_mydatasets",
                filterFn: function (item) {
                    return item.get("last_edited_by") === window.auth_user;
                }
            }));
        }
        else {
            Ext.getStore("Datasets").removeFilter("filter_mydatasets");
        }
    },

    onShowDatasetsWithCoupledMetadata: function (checkbox, newValue) {
        if (newValue) {
            Ext.getStore("Datasets").addFilter(new Ext.util.Filter({
                id: "filter_coupledmetadata",
                filterFn: function (item) {
                    return item.get("md_id");
                }
            }));
        }
        else {
            Ext.getStore("Datasets").removeFilter("filter_coupledmetadata");
        }
    },

    onShowExternalDatasets: function (checkbox, newValue) {
        if (newValue) {
            Ext.getStore("Datasets").addFilter(new Ext.util.Filter({
                id: "filter_externaldatasets",
                filterFn: function (item) {
                    return item.get("external");
                }
            }));
        }
        else {
            Ext.getStore("Datasets").removeFilter("filter_externaldatasets");
        }
    },

    onImportDataset: function () {
        const configExtInstancesStore = Ext.getStore("ConfigExtInstances");

        if (configExtInstancesStore.getCount() > 0) {
            let datasetImportWindow = Ext.getCmp("datasetimport-window");

            if (!datasetImportWindow) {
                datasetImportWindow = Ext.create("datasetimport.DatasetImportWindow");
            }
            datasetImportWindow.show();
        }
        else {
            Ext.Msg.alert("Datensatz Import", "Bisher keine externe UDP-Manager-Instanz konfiguriert (Verwaltung > Konfiguration > Externe UDP-Manager-Instanzen konfigurieren).", Ext.emptyFn);
        }
    },

    onShareDataset: function () {
        let shareWindow = Ext.getCmp("share-window");

        if (!shareWindow) {
            shareWindow = Ext.create("datasets.ShareWindow");
        }
        shareWindow.show();

        const baseUrl = window.location.href;
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();

        Ext.getCmp("shareurlfield").setValue(`${baseUrl}?dataset_id=${dataset_id}`);
    }
});
