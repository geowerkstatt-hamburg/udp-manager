Ext.define("UDPManager.view.datasets.share.ShareWindow", {
    extend: "Ext.window.Window",
    xtype: "sharewindow",
    id: "share-window",
    alias: "datasets.ShareWindow",
    height: 110,
    width: 500,
    title: "Link teilen",
    controller: "share",
    layout: "hbox",
    items: [
        {
            xtype: "textfield",
            id: "shareurlfield",
            width: 430,
            margin: "20 10 20 10"
        },
        {
            xtype: "button",
            id: "copytoclipboard",
            width: 40,
            iconCls: "x-fa fa-clipboard",
            cls: "custom-btn-icon",
            margin: "20 10 10 0",
            listeners: {
                click: "copyToClipboard"
            }
        }
    ]
});
