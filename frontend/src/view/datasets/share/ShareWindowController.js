Ext.define("UDPManager.view.datasets.share.ShareWindowController", {
    extend: "Ext.app.ViewController",

    alias: "controller.share",

    copyToClipboard: function () {
        const url = Ext.getCmp("shareurlfield").getValue();

        navigator.clipboard.writeText(url);

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("URL in Zwischenablage kopiert");
    }
});
