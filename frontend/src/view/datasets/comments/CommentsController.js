Ext.define("UDPManager.view.datasets.comments.CommentsController", {
    extend: "Ext.app.ViewController",
    alias: "controller.comments",

    onAddComment: function () {
        const grid = Ext.getCmp("comments-grid");
        let commentEditorWindow = Ext.getCmp("commenteditor-window");

        grid.getSelectionModel().deselectAll();

        if (commentEditorWindow) {
            commentEditorWindow.destroy();
        }

        commentEditorWindow = Ext.create("comments.commenteditor");

        commentEditorWindow.show();
    },

    onDeleteComment: function () {
        const grid = Ext.getCmp("comments-grid");
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();

        if (grid.getSelectionModel().hasSelection()) {
            const row = grid.getSelectionModel().getSelection()[0];

            Ext.Ajax.request({
                url: "backend/deletecomment",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    id: row.data.id,
                    dataset_id: dataset_id,
                    last_edited_by: window.auth_user
                },
                success: function () {
                    Ext.getStore("Comments").load({
                        params: {dataset_id: dataset_id}
                    });
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    onEditComment: function () {
        const grid = Ext.getCmp("comments-grid");

        if (grid.getSelectionModel().hasSelection()) {
            const row = grid.getSelectionModel().getSelection()[0];
            let commentEditorWindow = Ext.getCmp("commenteditor-window");

            if (commentEditorWindow) {
                commentEditorWindow.destroy();
            }

            commentEditorWindow = Ext.create("comments.commenteditor");

            commentEditorWindow.show();

            Ext.getCmp("commenteditortext").setValue(row.data.comment);
            Ext.getCmp("commenteditorimportant").setValue(row.data.important);
        }
    }
});
