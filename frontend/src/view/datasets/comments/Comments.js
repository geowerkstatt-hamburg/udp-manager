Ext.define("UDPManager.view.datasets.comments.Comments", {
    extend: "Ext.container.Container",
    xtype: "dataset_comments",
    controller: "comments",
    bodyPadding: "5 5 5 5",
    style: "border-top: 1px solid #cfcfcf !important;",
    items: [
        {
            xtype: "grid",
            cls: "commentsCls",
            store: "Comments",
            name: "commentsGrid",
            autoScroll: true,
            height: Ext.Element.getViewportHeight() - 108,
            id: "comments-grid",
            hideHeaders: true,
            border: false,
            viewConfig: {
                enableTextSelection: true,
                stripeRows: false
            },
            columnLines: true,
            selModel: "rowmodel",
            columns: [
                {
                    xtype: "gridcolumn",
                    align: "left",
                    renderer: function (value, metaData, record) {
                        let comment = "";

                        if (record.data.important) {
                            comment += "<div class=\"commentsImportantCls\">Wichtig!</div>";
                        }

                        comment += "<div><div class=\"commentsAuthorCls\">" + record.data.username + " - " + record.data.post_date + "</div><div>" + record.data.comment + "</div></div>";

                        return comment;
                    },
                    dataIndex: "text",
                    cellWrap: true,
                    flex: 1
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "top",
                padding: "20 20 5 20",
                items: [{
                    id: "addcommentbutton",
                    disabled: window.read_only,
                    iconCls: "x-fa fa-plus",
                    cls: "custom-btn-icon",
                    scale: "medium",
                    handler: "onAddComment"
                }, {
                    id: "editcommentbutton",
                    disabled: window.read_only,
                    iconCls: "x-fa fa-pen",
                    cls: "custom-btn-icon",
                    scale: "medium",
                    handler: "onEditComment"
                }, {
                    id: "deletecommentbutton",
                    disabled: window.read_only,
                    iconCls: "x-fa fa-trash",
                    cls: "custom-btn-icon",
                    scale: "medium",
                    handler: "onDeleteComment"
                }]
            }]
        }
    ]
});
