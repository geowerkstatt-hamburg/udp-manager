Ext.define("UDPManager.view.datasets.comments.CommentEditor", {
    extend: "Ext.window.Window",
    id: "commenteditor-window",
    alias: "comments.commenteditor",
    height: 410,
    width: 730,
    title: "Kommentar bearbeiten",

    controller: "commenteditor",
    items: [
        {
            xtype: "form",
            id: "commenteditorform",
            items: [
                {
                    xtype: "htmleditor",
                    id: "commenteditortext",
                    grow: true,
                    labelWidth: 0,
                    height: 327,
                    width: 730,
                    autoscroll: true
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        border: true,
        style: "border-top: 1px solid #cfcfcf !important;",
        items: [
            {
                fieldLabel: "Als wichtig markieren",
                id: "commenteditorimportant",
                name: "important",
                xtype: "checkboxfield",
                labelWidth: 160,
                margin: "0 7 10 10"
            },
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                text: "Speichern",
                scale: "medium",
                cls: "btn-save",
                margin: "0 7 0 0",
                listeners: {
                    click: "onSaveComment"
                }
            }
        ]
    }]
});
