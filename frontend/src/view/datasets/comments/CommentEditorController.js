Ext.define("UDPManager.view.datasets.comments.CommentEditorController", {
    extend: "Ext.app.ViewController",
    alias: "controller.commenteditor",

    onSaveComment: function () {
        const grid = Ext.getCmp("comments-grid");
        const comment = Ext.getCmp("commenteditortext").getValue();
        const important = Ext.getCmp("commenteditorimportant").getValue();
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        let comment_id = null;

        if (grid.getSelectionModel().hasSelection()) {
            comment_id = grid.getSelectionModel().getSelection()[0].data.id;
        }

        Ext.Ajax.request({
            url: "backend/savecomment",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                id: comment_id,
                dataset_id: dataset_id,
                username: window.auth_user,
                post_date: moment().format("YYYY-MM-DD"),
                comment: comment,
                important: important
            },
            success: function () {
                Ext.getStore("Comments").load({
                    params: {dataset_id: dataset_id}
                });
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });

        Ext.getCmp("commenteditor-window").close();
    }
});
