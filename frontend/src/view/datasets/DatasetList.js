/**
 * This view is an example list of datasets.
 */
Ext.define("UDPManager.view.datasets.DatasetList", {
    extend: "Ext.form.Panel",
    xtype: "datasetlist",
    alias: "widget.datasetlist",
    controller: "datasetlist-controller",
    viewModel: {
        data: {
            form_datasets: null
        },
        stores: {
            gridstore_datasets: {
                type: "datasets"
            }
        }
    },
    layout: "border",
    initComponent: function () {
        this.items = [
            {
                xtype: "panel",
                id: "datasetlistcollapsible",
                layout: "vbox",
                height: Ext.Element.getViewportHeight() - 62,
                width: "35%",
                collapsible: true,
                header: false,
                region: "west",
                split: true,
                items: [
                    {
                        xtype: "container",
                        style: "background-color: #ebebeb",
                        width: "100%",
                        padding: "5 5 0 5",
                        minHeight: 36,
                        items: [
                            {
                                xtype: "fieldset",
                                id: "filterfieldset",
                                layout: "vbox",
                                width: "100%",
                                title: "Filter",
                                style: "background-color: #ffffff",
                                collapsible: true,
                                collapsed: true,
                                items: [
                                    {
                                        xtype: "fieldcontainer",
                                        layout: "hbox",
                                        style: "background-color: #ffffff",
                                        width: "100%",
                                        margin: "0 0 0 0",
                                        items: [
                                            {
                                                prependText: "Stelle: ",
                                                xtype: "cycle",
                                                id: "responsiblepartyfilterbutton",
                                                showText: true,
                                                width: 195,
                                                margin: "10px 5px 5px 10px",
                                                textAlign: "left",
                                                listeners: {
                                                    change: "onFilter"
                                                },
                                                menu: {
                                                    items: UDPManager.Configs.responsiblePartyFilterListGen()
                                                }
                                            },
                                            {
                                                prependText: "Status: ",
                                                xtype: "cycle",
                                                id: "statusfilterbutton",
                                                showText: true,
                                                width: 195,
                                                margin: "10px 0px 5px 5px",
                                                textAlign: "left",
                                                listeners: {
                                                    change: "onFilter"
                                                },
                                                menu: {
                                                    items: [
                                                        {
                                                            text: "Alle",
                                                            type: "status_alle",
                                                            itemId: "status_alle",
                                                            checked: true
                                                        },
                                                        {
                                                            text: "veröffentlicht",
                                                            type: "status_prod",
                                                            itemId: "status_prod",
                                                            checked: false
                                                        },
                                                        {
                                                            text: "veröffentlicht - vorveröffentlicht",
                                                            type: "status_stage",
                                                            itemId: "status_stage",
                                                            checked: false
                                                        },
                                                        {
                                                            text: "vorveröffentlicht",
                                                            type: "status_stage",
                                                            itemId: "status_stage",
                                                            checked: false
                                                        },
                                                        {
                                                            text: "veröffentlicht - in Bearbeitung",
                                                            type: "status_stage",
                                                            itemId: "status_stage",
                                                            checked: false
                                                        },
                                                        {
                                                            text: "in Bearbeitung",
                                                            type: "status_dev",
                                                            itemId: "status_dev",
                                                            checked: false
                                                        }
                                                    ]
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "checkboxfield",
                                        id: "sortdatasetsbydate",
                                        fieldLabel: "Nach 'zuletzt bearbeitet' sortieren",
                                        checked: true,
                                        margin: "5 0 5 10",
                                        labelWidth: 195,
                                        listeners: {
                                            change: "onChangeSort"
                                        }
                                    },
                                    {
                                        xtype: "fieldcontainer",
                                        layout: "hbox",
                                        style: "background-color: #ffffff",
                                        width: "100%",
                                        margin: "0 0 0 0",
                                        items: [
                                            {
                                                xtype: "checkboxfield",
                                                id: "filterdatasetsbyeditor",
                                                fieldLabel: "Meine Datensätze",
                                                checked: false,
                                                margin: "5 0 10 10",
                                                labelWidth: 115,
                                                listeners: {
                                                    change: "onShowMyDatasets"
                                                }
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "filterdatasetswithmetadatacoupling",
                                                fieldLabel: "Metadatenkopplung:",
                                                checked: false,
                                                margin: "5 0 10 10",
                                                labelWidth: 115,
                                                listeners: {
                                                    change: "onShowDatasetsWithCoupledMetadata"
                                                }
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "filterdatasetsexternal",
                                                fieldLabel: "Externe Datensätze:",
                                                checked: false,
                                                margin: "5 0 10 10",
                                                labelWidth: 115,
                                                listeners: {
                                                    change: "onShowExternalDatasets"
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "tagfield",
                                        id: "filterkeywords",
                                        fieldLabel: "Schlagwortfilter",
                                        labelWidth: 90,
                                        width: 355,
                                        store: "DatasetFilterKeywords",
                                        margin: "0 0 10 10",
                                        displayField: "text",
                                        valueField: "text",
                                        forceSelection: false,
                                        autocomplete: "off",
                                        filterPickList: true,
                                        matchFieldWidth: false,
                                        grow: false,
                                        height: 49,
                                        listeners: {
                                            change: "onKeywordFilter"
                                        }
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: "gridpanel",
                        id: "grid_dataset",
                        bind: "{gridstore_datasets}",
                        //height: Ext.Element.getViewportHeight() - 172,
                        width: "100%",
                        flex: 1,
                        headerBorders: true,
                        autoscroll: true,
                        rowLines: false,
                        style: "border-top: 1px solid #cfcfcf !important;",
                        viewConfig: {
                            enableTextSelection: true,
                            preserveScrollOnRefresh: true,
                            preserveScrollOnReload: true,
                            //stripeRows: true,
                            getRowClass: function () {
                                return "multiline-row";
                            }
                        },
                        columns: [
                            {
                                dataIndex: "status",
                                width: 40,
                                align: "center",
                                sortable: false,
                                menuDisabled: true,
                                resizeable: false,
                                renderer: function (value) {
                                    if (value === "veröffentlicht") {
                                        return "<div class=\"x-fa fa-globe-africa statusIcon\" data-qtip=\"veröffentlicht\"></div>";
                                    }
                                    else if (value === "veröffentlicht - vorveröffentlicht") {
                                        return "<div class=\"x-fa fa-globe-africa yellowIconGradient\" data-qtip=\"veröffentlicht - vorveröffentlicht\"></div>";
                                    }
                                    else if (value === "veröffentlicht - in Bearbeitung") {
                                        return "<div class=\"x-fa fa-globe-africa orangeIconGradient\" data-qtip=\"veröffentlicht - in Bearbeitung\"></div>";
                                    }
                                    else if (value === "vorveröffentlicht") {
                                        return "<div class=\"x-fa fa-globe-africa yellowIcon\" data-qtip=\"vorveröffentlicht\"></div>";
                                    }
                                    else {
                                        return "<div class=\"x-fa fa-globe-africa orangeIcon\" data-qtip=\"in Bearbeitung\"></div>";
                                    }
                                }
                            },
                            {
                                text: "ID",
                                dataIndex: "id",
                                width: 70,
                                resizeable: false
                            },
                            {
                                text: "Titel",
                                dataIndex: "title",
                                flex: 1,
                                resizeable: false
                            },
                            {
                                dataIndex: "external",
                                width: 30,
                                align: "center",
                                sortable: false,
                                menuDisabled: true,
                                resizeable: false,
                                renderer: function (value) {
                                    if (value) {
                                        return "<div class=\"x-fa fa-sign-in-alt\" style=\"color:#00000066\" data-qtip=\"externer Datensatz\"></div>";
                                    }
                                    else {
                                        return "<div></div>";
                                    }
                                }
                            }
                        ],
                        listeners: {
                            select: "onGridItemSelected",
                            viewready: "switchTab"
                        },
                        dockedItems: [{
                            xtype: "toolbar",
                            dock: "bottom",
                            items: [
                                {
                                    id: "addnewdataset",
                                    iconCls: "x-fa fa-plus",
                                    cls: "custom-btn-icon",
                                    disabled: window.read_only,
                                    scale: "medium",
                                    tooltip: "Neuen Datensatz anlegen (Blanko)",
                                    listeners: {
                                        click: "onAddNewDataset"
                                    }
                                },
                                {
                                    id: "importdataset",
                                    iconCls: "x-fa fa-file-import",
                                    cls: "custom-btn-icon",
                                    disabled: window.read_only,
                                    hidden: true,
                                    scale: "medium",
                                    tooltip: "Datensatz aus externer Instanz importieren",
                                    listeners: {
                                        click: "onImportDataset"
                                    }
                                },
                                {
                                    id: "copydatasettest",
                                    iconCls: "x-fa fa-copy",
                                    cls: "custom-btn-icon",
                                    disabled: window.read_only,
                                    scale: "medium",
                                    tooltip: "Ausgewählten Datensatz duplizieren",
                                    listeners: {
                                        click: "onDuplicateDataset"
                                    }
                                },
                                {
                                    id: "deletedataset",
                                    iconCls: "x-fa fa-trash",
                                    cls: "custom-btn-icon",
                                    disabled: window.read_only,
                                    scale: "medium",
                                    tooltip: "Ausgewählten Datensatz löschen",
                                    listeners: {
                                        click: "onDeleteDataset"
                                    }
                                },
                                {
                                    id: "sharedataset",
                                    iconCls: "x-fa fa-share-alt",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    tooltip: "Link auf diesen Datensatz teilen",
                                    listeners: {
                                        click: "onShareDataset"
                                    }
                                },
                                {
                                    xtype: "component",
                                    flex: 1,
                                    html: ""
                                },
                                {
                                    xtype: "component",
                                    id: "datasetscounter",
                                    cls: "x-form-item-label-default",
                                    html: ""
                                }
                            ]
                        }]
                    }
                ]
            },
            {
                xtype: "datasettabpanel",
                id: "dataset_main_tabs",
                region: "center",
                split: true,
                width: "65%"
            }
        ];

        this.callParent(arguments);
    }
});
