Ext.define("UDPManager.view.datasets.etl.ETLDetails", {
    extend: "Ext.panel.Panel",
    xtype: "dataset_etldetails",
    controller: "etldetails",
    layout: "column",
    height: Ext.Element.getViewportHeight() - 108,
    items: [
        {
            xtype: "gridpanel",
            id: "etlprocessesgrid",
            autoScroll: true,
            height: Ext.Element.getViewportHeight() - 108,
            style: "border-right: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
            columnWidth: 0.35,
            bind: {
                selection: "{ETLProcess}"
            },
            store: "ETLProcesses",
            columns: [
                {
                    dataIndex: "name",
                    text: "Name",
                    align: "center",
                    flex: 1
                }
            ],
            viewConfig: {
                //stripeRows: true,
                getRowClass: function() {
                    return "multiline-row";
                }
            },
            listeners: {
                select: "onGridItemSelected"
            },
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    {
                        id: "addnewetlprocess",
                        iconCls: "x-fa fa-plus",
                        disabled: window.read_only,
                        tooltip: "Neuen ETL Prozess anlegen (Blanko)",
                        cls: "custom-btn-icon",
                        scale: "medium",
                        listeners: {
                            click: "onAddNewEtlProcess"
                        }
                    },
                    {
                        id: "delete_etlprocess",
                        tooltip: "ETL-Prozess löschen",
                        scale: "medium",
                        iconCls: "x-fa fa-trash",
                        cls: "custom-btn-icon",
                        disabled: true,
                        listeners: {
                            click: "onDeleteEtlProcess"
                        }
                    }
                ]
            }]
        },
        {
            xtype: "form",
            id: "etldetailsform",
            columnWidth: 0.65,
            bodyPadding: 20,
            layout: "anchor",
            defaultType: "textfield",
            frame: true,
            height: Ext.Element.getViewportHeight() - 107,
            style: "border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
            autoScroll: true,
            disabled: true,
            items: [
                {
                    xtype: "numberfield",
                    id: "etlprocessid",
                    hidden: true,
                    name: "id",
                    width: "100%",
                    bind: "{ETLProcess.id}"
                },
                {
                    xtype: "combo",
                    fieldLabel: "Typ",
                    id: "etlprocesstype",
                    store: ["FME Workspace", "FME Automation", "Container-Services", "Sonstiges"],
                    name: "type",
                    labelWidth: 160,
                    width: "100%",
                    bind: "{ETLProcess.type}",
                    editable: false,
                    listeners: {
                        change: "onSelectType"
                    }
                },
                {
                    xtype: "textfield",
                    fieldLabel: "Host",
                    id: "etlprocesshost",
                    name: "host",
                    labelWidth: 160,
                    width: "100%",
                    bind: "{ETLProcess.host}"
                },
                {
                    xtype: "combo",
                    fieldLabel: "Host",
                    id: "etlprocessfmehost",
                    name: "fmehost",
                    labelWidth: 160,
                    width: "100%",
                    hidden: true,
                    editable: false,
                    bind: "{ETLProcess.host}",
                    listeners: {
                        change: "onSelectFMEHost"
                    }
                },
                {
                    xtype: "textfield",
                    fieldLabel: "Ordner",
                    id: "etlprocessfolder",
                    name: "folder",
                    labelWidth: 160,
                    width: "100%",
                    bind: "{ETLProcess.folder}"
                },
                {
                    xtype: "combo",
                    fieldLabel: "Repository",
                    id: "etlprocessfmefolder",
                    name: "fmefolder",
                    labelWidth: 160,
                    width: "100%",
                    hidden: true,
                    editable: false,
                    bind: "{ETLProcess.folder}",
                    listeners: {
                        change: "onSelectFMERepository"
                    }
                },
                {
                    xtype: "textfield",
                    fieldLabel: "Repository",
                    id: "etlprocessrepo",
                    name: "repo",
                    labelWidth: 160,
                    width: "100%",
                    bind: "{ETLProcess.repo}",
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Verweis zum Bitbucket-Main-Branch"
                    }
                },
                {
                    xtype: "textfield",
                    fieldLabel: "Service Definition",
                    id: "etlprocessservicedefinition",
                    name: "service_definition",
                    labelWidth: 160,
                    width: "100%",
                    bind: "{ETLProcess.service_definition}",
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Helm-Chart oder dc.yaml"
                    }
                },
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    width: "100%",
                    scrollable: true,
                    items: [
                        {
                            id: "etlprocessdoclink",
                            name: "doclink",
                            fieldLabel: "Dokumentation Datenquelle",
                            xtype: "textfield",
                            margin: "0 10 0 0",
                            labelWidth: 160,
                            flex: 14,
                            bind: "{ETLProcess.doclink}",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Link zu einer API-Dokumentation"
                            }
                        },
                        {
                            xtype: "button",
                            id: "doclinkcall",
                            disabled: window.read_only,
                            margin: "0 0 0 0",
                            flex: 1,
                            autoEl: {
                                tag: "div",
                                "data-qtip": "API-Dokumentation aufrufen"
                            },
                            iconCls: "x-fa fa-external-link-alt",
                            listeners: {
                                click: "onCallDocLink"
                            }
                        }
                    ]
                },
                {
                    xtype: "textfield",
                    fieldLabel: "Name",
                    id: "etlprocessname",
                    name: "name",
                    labelWidth: 160,
                    width: "100%",
                    bind: "{ETLProcess.name}"
                },
                {
                    xtype: "combo",
                    fieldLabel: "Name",
                    id: "etlprocessfmename",
                    name: "fmename",
                    labelWidth: 160,
                    width: "100%",
                    hidden: true,
                    editable: false,
                    bind: "{ETLProcess.name}",
                    listeners: {
                        select: "onSelectFMEProcessName"
                    }
                },
                {
                    xtype: "fieldcontainer",
                    id: "etlprocessintervalcontainer",
                    layout: "hbox",
                    labelWidth: 160,
                    fieldLabel: "Intervall",
                    width: "100%",
                    items: [
                        {
                            xtype: "numberfield",
                            fieldLabel: "alle",
                            id: "etlprocessintervalnumber",
                            minValue: 0,
                            step: 1,
                            labelWidth: 40,
                            width: 120,
                            name: "interval_number",
                            bind: "{ETLProcess.interval_number}"
                        },
                        {
                            xtype: "combo",
                            store: ["Minuten", "Stunden", "Tag", "Woche", "Monate", "Jahre", "unregelmäßig", "custom"],
                            allowBlank: false,
                            editable: false,
                            id: "etlprocessintervalunit",
                            width: 160,
                            name: "interval_unit",
                            listeners: {
                                change: "onIntervalUnitChange"
                            },
                            bind: "{ETLProcess.interval_unit}"
                        }
                    ]
                },
                {
                    xtype: "textfield",
                    fieldLabel: "Intervall (custom)",
                    id: "etlprocessintervalcustom",
                    name: "interval_custom",
                    hidden: true,
                    labelWidth: 160,
                    width: "100%",
                    bind: "{ETLProcess.interval_custom}"
                },
                {
                    fieldLabel: "Letzte Ausführung",
                    id: "etlprocesslastrun",
                    name: "last_run",
                    xtype: "datefield",
                    format: "Y-m-d H:i",
                    labelWidth: 160,
                    width: "100%",
                    bind: "{ETLProcess.last_run}"
                },
                {
                    xtype: "component",
                    cls: "x-form-item-label-default",
                    html: "Beschreibung"
                },
                {
                    xtype: "htmleditor",
                    id: "etlprocessdescription",
                    name: "description",
                    labelWidth: 0,
                    height: 200,
                    width: "100%"
                },
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    labelWidth: 0,
                    width: "100%",
                    items: [
                        {
                            xtype: "combo",
                            fieldLabel: "Collection verknüpfen",
                            id: "etlprocessmapcollection",
                            labelWidth: 160,
                            width: 500,
                            queryMode: "local",
                            displayField: "name",
                            disabled: true,
                            editable: false,
                            listeners: {
                                select: "onSelectCollection"
                            }
                        },
                        {
                            xtype: "button",
                            text: "Alle verknüpfen",
                            id: "etlprocessmapallcollections",
                            disabled: true,
                            margin: "0 0 0 10",
                            width: 120,
                            listeners: {
                                click: "onMapAllCollections"
                            }
                        }
                    ]
                },
                {
                    xtype: "component",
                    cls: "x-form-item-label-default",
                    html: "über diesen Prozess aktualisierte Collections"
                },
                {
                    xtype: "gridpanel",
                    id: "etlprocessescollectionsgrid",
                    autoScroll: true,
                    height: 160,
                    width: "100%",
                    store: "ETLProcessCollections",
                    border: true,
                    columns: [
                        {
                            dataIndex: "id",
                            text: "Collection-ID",
                            align: "center",
                            width: 100
                        },
                        {
                            dataIndex: "name",
                            text: "Name",
                            align: "center",
                            flex: 1
                        },
                        {
                            xtype: "actioncolumn",
                            width: 26,
                            sortable: false,
                            menuDisabled: true,
                            align: "center",
                            handler: "onDeleteRow",
                            hidden: window.read_only,
                            iconCls: "x-fa fa-ban deleteIcon"
                        }
                    ]
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                border: true,
                style: "border-top: 1px solid #cfcfcf !important;",
                items: [
                    {
                        xtype: "component",
                        html: "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Pflichtfeld</span>",
                        hidden: true
                    },
                    {
                        xtype: "component",
                        flex: 1,
                        html: ""
                    },
                    {
                        text: "Speichern",
                        id: "save_etlprocess",
                        scale: "medium",
                        cls: "btn-save",
                        disabled: true,
                        margin: "0 7 0 0",
                        listeners: {
                            click: "onSaveEtlProcess"
                        }
                    }
                ]
            }]
        }
    ]
});
