Ext.define("UDPManager.view.datasets.etl.ETLDetailsController", {
    extend: "Ext.app.ViewController",
    alias: "controller.etldetails",

    onGridItemSelected: function (sender, record) {
        const selectedETLProcess = record.data;
        const etlProcessCollectionsStore = Ext.getStore("ETLProcessCollections");

        etlProcessCollectionsStore.removeAll();
        if (selectedETLProcess.mapped_collections) {
            etlProcessCollectionsStore.loadData(selectedETLProcess.mapped_collections);
        }

        Ext.getCmp("etlprocessdescription").setValue(selectedETLProcess.description);
        Ext.getCmp("etlprocessmapcollection").setDisabled(window.read_only);
        Ext.getCmp("etlprocessmapallcollections").setDisabled(window.read_only);
        Ext.getCmp("delete_etlprocess").setDisabled(window.read_only);
        Ext.getCmp("save_etlprocess").setDisabled(window.read_only);
        Ext.getCmp("etldetailsform").setDisabled(false);
    },

    onIntervalUnitChange: function () {
        const unit = Ext.getCmp("etlprocessintervalunit").getSubmitValue();
        const number = Ext.getCmp("etlprocessintervalnumber");
        const custom = Ext.getCmp("etlprocessintervalcustom");

        if (unit === "unregelmäßig") {
            number.setValue("");
            number.setDisabled(true);
            custom.setHidden(true);
        }
        else if (unit === "custom") {
            number.setValue("");
            number.setDisabled(true);
            custom.setHidden(false);
        }
        else {
            number.setDisabled(false);
            custom.setHidden(true);
        }
    },

    onSelectType: function (combo) {
        if (combo.getValue() === "FME Workspace") {
            Ext.getCmp("etlprocesshost").setHidden(true);
            Ext.getCmp("etlprocessfolder").setHidden(true);
            Ext.getCmp("etlprocessname").setHidden(true);
            Ext.getCmp("etlprocessfmehost").setHidden(false);
            Ext.getCmp("etlprocessfmefolder").setHidden(false);
            Ext.getCmp("etlprocessfmename").setHidden(false);
            Ext.getCmp("etlprocessfmehost").setStore(UDPManager.Configs.getFmeServer());
            Ext.getCmp("etlprocesslastrun").setHidden(false);
            Ext.getCmp("etlprocessrepo").setHidden(true);
            Ext.getCmp("etlprocessservicedefinition").setHidden(true);
            Ext.getCmp("etlprocessdoclink").setHidden(true);
            Ext.getCmp("doclinkcall").setHidden(true);
        }
        else if (combo.getValue() === "FME Automation") {
            Ext.getCmp("etlprocesshost").setHidden(true);
            Ext.getCmp("etlprocessfolder").setHidden(true);
            Ext.getCmp("etlprocessname").setHidden(true);
            Ext.getCmp("etlprocessfmehost").setHidden(false);
            Ext.getCmp("etlprocessfmefolder").setHidden(true);
            Ext.getCmp("etlprocessfmename").setHidden(false);
            Ext.getCmp("etlprocessfmehost").setStore(UDPManager.Configs.getFmeServer());
            Ext.getCmp("etlprocesslastrun").setHidden(false);
            Ext.getCmp("etlprocessrepo").setHidden(true);
            Ext.getCmp("etlprocessservicedefinition").setHidden(true);
            Ext.getCmp("etlprocessdoclink").setHidden(true);
            Ext.getCmp("doclinkcall").setHidden(true);
        }
        else {
            Ext.getCmp("etlprocesshost").setHidden(false);
            Ext.getCmp("etlprocessfolder").setHidden(false);
            Ext.getCmp("etlprocessname").setHidden(false);
            Ext.getCmp("etlprocessfmehost").setHidden(true);
            Ext.getCmp("etlprocessfmefolder").setHidden(true);
            Ext.getCmp("etlprocessfmename").setHidden(true);
            if (combo.getValue() === "Container-Services") {
                Ext.getCmp("etlprocesslastrun").setHidden(true);
            }
            else {
                Ext.getCmp("etlprocesslastrun").setHidden(false);
            }
            Ext.getCmp("etlprocessrepo").setHidden(false);
            Ext.getCmp("etlprocessservicedefinition").setHidden(false);
            Ext.getCmp("etlprocessdoclink").setHidden(false);
            Ext.getCmp("doclinkcall").setHidden(false);
        }
    },

    onSelectFMEHost: function () {
        if (Ext.getCmp("etlprocessfmehost").getValue()) {
            if (Ext.getCmp("etlprocesstype").getSubmitValue() === "FME Automation") {
                Ext.getCmp("etlprocessfmename").setDisabled(true);

                Ext.Ajax.request({
                    url: "backend/getfmeautomations",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        host: Ext.getCmp("etlprocessfmehost").getValue()
                    },
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);

                        if (response_json.status === "success" && response_json.automations.length > 0) {
                            Ext.getCmp("etlprocessfmename").setStore(response_json.automations);
                        }
                        Ext.getCmp("etlprocessfmename").setDisabled(false);
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht abgerufen werden");
                        Ext.getCmp("etlprocessfmename").setDisabled(false);
                    }
                });
            }
            else {
                Ext.getCmp("etlprocessfmefolder").setDisabled(true);

                Ext.Ajax.request({
                    url: "backend/getfmerepositories",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        host: Ext.getCmp("etlprocessfmehost").getValue()
                    },
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);

                        if (response_json.status === "success" && response_json.repositories.length > 0) {
                            Ext.getCmp("etlprocessfmefolder").setStore(response_json.repositories);
                        }
                        Ext.getCmp("etlprocessfmefolder").setDisabled(false);
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht abgerufen werden");
                        Ext.getCmp("etlprocessfmefolder").setDisabled(false);
                    }
                });
            }
        }
    },

    onSelectFMERepository: function () {
        if (Ext.getCmp("etlprocesshost").getValue() && Ext.getCmp("etlprocessfmefolder").getValue()) {
            Ext.getCmp("etlprocessfmename").setDisabled(true);

            Ext.Ajax.request({
                url: "backend/getfmeworkspaces",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    host: Ext.getCmp("etlprocessfmehost").getValue(),
                    repository: Ext.getCmp("etlprocessfmefolder").getValue()
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success" && response_json.workspaces) {
                        if (response_json.workspaces.length > 0) {
                            Ext.getCmp("etlprocessfmename").setStore(response_json.workspaces);
                        }
                    }
                    Ext.getCmp("etlprocessfmename").setDisabled(false);
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht abgerufen werden");
                    Ext.getCmp("etlprocessfmename").setDisabled(false);
                }
            });
        }
    },

    onSelectFMEProcessName: function (combo) {
        if (Ext.getCmp("etlprocesshost").getValue() && Ext.getCmp("etlprocessfmefolder").getValue()) {
            Ext.Ajax.request({
                url: "backend/getfmeworkspacedetails",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    host: Ext.getCmp("etlprocessfmehost").getValue(),
                    repository: Ext.getCmp("etlprocessfmefolder").getValue(),
                    workspace: combo.getValue()
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success" && response_json.workspace) {
                        Ext.getCmp("etlprocessdescription").setValue(response_json.workspace.description);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht abgerufen werden");
                }
            });
        }
    },

    onAddNewEtlProcess: function () {
        const rec = new UDPManager.model.UDPManagerModel({
            id: 0,
            name: "*NEU*"
        });
        const count = Ext.getStore("ETLProcesses").count();

        Ext.getStore("ETLProcesses").insert(count, rec);

        Ext.getCmp("etlprocessmapcollection").setValue("");
        Ext.getCmp("etlprocessdescription").setValue("");
        Ext.getStore("ETLProcessCollections").removeAll();

        Ext.getCmp("etlprocessesgrid").getSelectionModel().select(rec);
    },

    onDeleteEtlProcess: function () {
        const selection = Ext.getCmp("etlprocessesgrid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            Ext.getCmp("delete_etlprocess").setDisabled(true);

            const mb = Ext.MessageBox;

            mb.buttonText.yes = "Ja";
            mb.buttonText.no = "Nein";

            mb.confirm("ETL-Prozess", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    const data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deleteetlprocess",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            id: data.id
                        },
                        success: function () {
                            Ext.getStore("ETLProcesses").load({
                                params: {dataset_id: Ext.getCmp("datasetid").getSubmitValue()}
                            });
                            Ext.getCmp("delete_etlprocess").setDisabled(false);
                            Ext.getStore("ETLProcessCollections").removeAll();
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("ETL Prozess gelöscht!");
                        },
                        failure: function (response) {
                            Ext.getCmp("delete_etlprocess").setDisabled(false);
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
                else {
                    Ext.getCmp("delete_etlprocess").setDisabled(false);
                }
            });
        }
    },

    onSaveEtlProcess: function () {
        const fieldvalues = Ext.getCmp("etldetailsform").getForm().getFieldValues();

        fieldvalues.dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        fieldvalues.last_edited_by = window.auth_user;

        if (fieldvalues.type.indexOf("FME") > -1) {
            fieldvalues.host = fieldvalues.fmehost;
            fieldvalues.folder = fieldvalues.fmefolder;
            fieldvalues.name = fieldvalues.fmename;
        }

        let etlprocess_collections = "[]";

        if (Ext.getStore("ETLProcessCollections").count() > 0) {
            const etlprocess_collections_array = [];

            Ext.getStore("ETLProcessCollections").each(function (record) {
                const etlprocess_collections_json = {
                    id: record.data.id,
                    name: record.data.name
                };

                etlprocess_collections_array.push(etlprocess_collections_json);
            });

            etlprocess_collections = JSON.stringify(etlprocess_collections_array);
        }

        fieldvalues.mapped_collections = etlprocess_collections;

        if (!fieldvalues.interval_number) {
            fieldvalues.interval_number = null;
        }

        if (fieldvalues.id !== 0) {
            Ext.Ajax.request({
                url: "backend/updateetlprocess",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: fieldvalues,
                success: function (response) {
                    if (Ext.decode(response.responseText).error) {
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    }
                    else {
                        Ext.getStore("ETLProcesses").load({
                            params: {dataset_id: Ext.getCmp("datasetid").getSubmitValue()}
                        });
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Datensatz erfolgreich gespeichert!");
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
        else if (fieldvalues.name !== "" && fieldvalues.type !== "") {
            Ext.Ajax.request({
                url: "backend/addetlprocess",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: fieldvalues,
                success: function (response) {
                    if (Ext.decode(response.responseText).error) {
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    }
                    else {
                        Ext.getStore("ETLProcesses").load({
                            params: {dataset_id: Ext.getCmp("datasetid").getSubmitValue()}
                        });
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Datensatz erfolgreich gespeichert!");
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
    },

    onSelectCollection: function (combo) {
        const selected_collection = combo.getSelection().data;

        const rec = new UDPManager.model.UDPManagerModel({
            id: selected_collection.id,
            name: selected_collection.name
        });

        const collection = Ext.getStore("ETLProcessCollections").findRecord("id", selected_collection.id, 0, false, false, true);

        if (!collection) {
            Ext.getStore("ETLProcessCollections").add(rec);
        }
    },

    onDeleteRow: function (grid, rowIndex) {
        grid.getStore().removeAt(rowIndex);
    },

    onMapAllCollections: function () {
        Ext.getStore("CollectionsDataset").each((record) => {
            const rec = new UDPManager.model.UDPManagerModel({
                id: record.data.id,
                name: record.data.name
            });

            Ext.getStore("ETLProcessCollections").add(rec);
        });
    },

    onCallDocLink: function () {
        const doc_link = Ext.getCmp("etlprocessdoclink").getValue();

        window.open(doc_link, "_blank");
    }
});
