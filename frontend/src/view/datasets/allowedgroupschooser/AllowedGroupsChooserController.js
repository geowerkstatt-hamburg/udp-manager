Ext.define("UDPManager.view.datasets.allowedgroupschooser.AllowedGroupsChooserController", {
    extend: "Ext.app.ViewController",
    alias: "controller.allowedgroupschooser",

    onSearchADGroups: function () {
        const groupname = Ext.getCmp("adgroup_search_field").getSubmitValue();

        Ext.getStore("ADGroups").load({
            params: {groupname: groupname}
        });
    },

    onAddToAllowedGroups: function (grid, rowIndex) {
        const data = grid.getStore().getAt(rowIndex).data;
        const allowed_groups_store = Ext.getStore("AllowedADGroups");

        allowed_groups_store.add(data);
    },

    onDeleteAllowedGroup: function (grid, rowIndex) {
        const record = grid.getStore().getAt(rowIndex);
        const allowed_groups_store = Ext.getStore("AllowedADGroups");

        allowed_groups_store.remove(record);
    },

    onSaveDatasetAllowedGroups: function () {
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const allowed_groups_store = Ext.getStore("AllowedADGroups");
        const allowed_groups = [];

        allowed_groups_store.each(function (allowed_group) {
            allowed_groups.push(allowed_group.get("name"));
        });

        Ext.Ajax.request({
            url: "backend/updatedatasetallowedgroups",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                dataset_id: dataset_id,
                allowed_groups: JSON.stringify(allowed_groups),
                last_edited_by: window.auth_user
            },
            success: function () {
                Ext.getStore("Datasets").load();
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Berechtigung gespeichert!");
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onSaveServiceAllowedGroups: function () {
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const allowed_groups_store = Ext.getStore("AllowedADGroups");
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const url_sec = Ext.getCmp("serviceurlsec").getSubmitValue();
        const allowed_groups = [];

        allowed_groups_store.each(function (allowed_group) {
            allowed_groups.push(allowed_group.get("name"));
        });

        Ext.Ajax.request({
            url: "backend/updateserviceallowedgroups",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                dataset_id: dataset_id,
                service_id: service_id,
                allowed_groups: JSON.stringify(allowed_groups),
                url_sec: url_sec,
                security_type: "AD SSO",
                last_edited_by: window.auth_user
            },
            success: function () {
                Ext.getStore("ServicesDataset").load({
                    params: {dataset_id: dataset_id}
                });
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Berechtigung gespeichert!");
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    }
});
