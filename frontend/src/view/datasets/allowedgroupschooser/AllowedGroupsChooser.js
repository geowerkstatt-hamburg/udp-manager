Ext.define("UDPManager.view.datasets.allowedgroupschooser.AllowedGroupsChooser", {
    extend: "Ext.window.Window",
    id: "allowedgroupschooser-window",
    alias: "allowedgroupschooser.AllowedGroupsChooser",
    height: 455,
    width: 727,

    title: "Gruppen im AD Suchen und hinzufügen",

    controller: "allowedgroupschooser",

    initComponent: function () {

        this.items = [
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                labelWidth: 0,
                width: "100%",
                items: [
                    {
                        xtype: "textfield",
                        id: "adgroup_search_field",
                        margin: "5 20 5 5",
                        width: 200,
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Nach Gruppen im AD suchen"
                        }
                    }, {
                        xtype: "button",
                        id: "search_adgroup",
                        margin: "5 0 5 0",
                        tooltip: "Suche nach Ad Gruppen starten",
                        iconCls: "x-fa fa-search",
                        listeners: {
                            click: "onSearchADGroups"
                        }
                    }
                ]
            },
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                labelWidth: 0,
                width: "100%",
                items: [
                    {
                        xtype: "grid",
                        store: "ADGroups",
                        id: "adgroups-grid",
                        height: 320,
                        width: 355,
                        border: true,
                        margin: "0 0 0 5",
                        autoScroll: true,
                        columns: [
                            {
                                header: "Suchergebnisse",
                                dataIndex: "name",
                                align: "left",
                                flex: 1
                            }, {
                                xtype: "actioncolumn",
                                header: "+",
                                name: "allowgroup",
                                tooltip: "Die Gruppe wird in die Liste der erlaubten Gruppen hinzugefügt",
                                align: "left",
                                handler: "onAddToAllowedGroups",
                                hidden: window.read_only,
                                width: 40,
                                items: [
                                    {
                                        iconCls: "black-icon x-fa fa-plus"
                                    }
                                ]
                            }
                        ],
                        columnLines: true
                    },
                    {
                        xtype: "grid",
                        store: "AllowedADGroups",
                        id: "allowedadgroups-grid",
                        height: 320,
                        width: 355,
                        border: true,
                        margin: "0 0 0 5",
                        autoScroll: true,
                        columns: [
                            {
                                header: "berechtigte Gruppen",
                                dataIndex: "name",
                                align: "left",
                                flex: 1
                            }, {
                                xtype: "actioncolumn",
                                header: "-",
                                name: "disallowgroup",
                                tooltip: "Die Gruppe wird aus der Liste der erlaubten Gruppen gelöscht",
                                align: "left",
                                handler: "onDeleteAllowedGroup",
                                hidden: window.read_only,
                                width: 40,
                                items: [
                                    {
                                        iconCls: "black-icon x-fa fa-minus"
                                    }
                                ]
                            }
                        ],
                        columnLines: true
                    }
                ]
            }
        ];
        this.buttons = [
            {
                id: "saveserviceallowedgroupsbutton",
                text: "Speichern",
                tooltip: "Die Liste der erlaubten AD Gruppen wird gespeichert.",
                disabled: window.read_only,
                hidden: true,
                listeners: {
                    click: "onSaveServiceAllowedGroups"
                }
            },
            {
                id: "savedatasetallowedgroupsbutton",
                text: "Speichern",
                tooltip: "Die Liste der erlaubten AD Gruppen wird gespeichert.",
                disabled: window.read_only,
                hidden: true,
                listeners: {
                    click: "onSaveDatasetAllowedGroups"
                }
            }
        ];

        this.callParent(arguments);
    }
});
