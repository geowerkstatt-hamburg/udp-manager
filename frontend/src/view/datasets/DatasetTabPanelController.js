Ext.define("UDPManager.view.datasets.jira.DatasetTabPanelController", {
    extend: "Ext.app.ViewController",
    alias: "controller.datasettabpanel",

    checkJiraTicketsModuleStatus: function () {
        if (!UDPManager.Configs.getModules().jiraTickets) {
            Ext.getCmp("datasetjiratab").setDisabled(true);
        }
    },

    onTabChange: function (tabs, newTab) {
        if (window.mainViewRendered) {
            if (newTab.xtype === "dataset_details") {
                Ext.getCmp("datasetlistcollapsible").setCollapsed(false);
                Ext.getCmp("dataset_main_tabs").setTitle("");
            }
            else if (newTab.xtype === "dataset_changelog") {
                Ext.getCmp("datasetlistcollapsible").setCollapsed(true);
                Ext.getCmp("dataset_main_tabs").setTitle("<b>Datensatz: \"" + Ext.getCmp("datasettitle").getValue() + "\"</b>");

                Ext.getStore("ChangeLog").load({
                    params: {dataset_id: Ext.getCmp("datasetid").getSubmitValue()}
                });
            }
            else {
                Ext.getCmp("datasetlistcollapsible").setCollapsed(true);
                Ext.getCmp("dataset_main_tabs").setTitle("<b>Datensatz: \"" + Ext.getCmp("datasettitle").getValue() + "\"</b>");
            }
        }
    }
});
