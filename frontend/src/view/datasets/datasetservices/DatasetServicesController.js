Ext.define("UDPManager.view.datasets.datasetservices.DatasetServicesController", {
    extend: "Ext.app.ViewController",

    alias: "controller.dataset_services",

    loadedWmsThemeId: null,

    onGridServiceItemSelected: function (sender, record) {
        const editMode = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditMode();
        const externalDataset = Ext.getCmp("datasetexternal").getValue();
        const datasetStatus = Ext.getCmp("datasetstatus").getValue();

        if (editMode) {
            Ext.getCmp("servicedetailsform").getForm().getFields().each(function (field) {
                field.setReadOnly(false);
            });

            // only if check for all service configs is successfull enable deploy stage button
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").checkServiceConfig();

            Ext.getCmp("saveservicedetails").setDisabled(false);
            Ext.getCmp("servicelayerjsonupdate").setDisabled(false);
            Ext.getCmp("servicelayerjsonupdateall").setDisabled(false);
            Ext.getCmp("deletewmsthemeconfig").setDisabled(false);
            Ext.getCmp("savewmsthemeconfig").setDisabled(false);
            Ext.getCmp("serviceurlinteditbutton").setDisabled(false);
            Ext.getCmp("serviceurlexteditbutton").setDisabled(false);
            Ext.getCmp("serviceurlseceditbutton").setDisabled(false);
            Ext.getCmp("addnewservice").setDisabled(false);
            Ext.getCmp("linkservice").setDisabled(false);
            Ext.getCmp("duplicateservice").setDisabled(false);
            Ext.getCmp("deleteservice").setDisabled(false);
            Ext.getCmp("generateconfigbutton").setDisabled(true);
            Ext.getCmp("deployprodconfigbutton").setDisabled(true);
            Ext.getCmp("deploystageconfigbutton").setDisabled(true);
            Ext.getCmp("undeployprodconfigbutton").setDisabled(true);

            let disable_gen_button = true;
            let allServicesExternal = true;

            Ext.getStore("ServicesDataset").each(function (service) {
                if (!service.data.disable_config_module && !service.data.disable_config_management) {
                    disable_gen_button = false;
                }
                if (!service.data.external) {
                    allServicesExternal = false;
                }
            });

            if (record.data.status_prod && !record.data.external) {
                Ext.getCmp("undeployprodconfigbutton").setDisabled(false);
            }
            if ((record.data.status_stage && datasetStatus !== "veröffentlicht") || (record.data.status_prod && record.data.status_stage && datasetStatus !== "veröffentlicht") || (!record.data.status_dev && record.data.status_prod && !record.data.external && datasetStatus !== "veröffentlicht") || (allServicesExternal && !externalDataset)) {
                Ext.getCmp("deployprodconfigbutton").setDisabled(false);
            }
            if (record.data.status_prod && !record.data.status_dev && !record.data.external) {
                Ext.getCmp("deployprodconfigbutton").setDisabled(true);
            }
            if (record.data.status_stage && !record.data.status_prod) {
                Ext.getCmp("deployprodconfigbutton").setDisabled(false);
                Ext.getCmp("undeployprodconfigbutton").setDisabled(true);
            }
            if (record.data.status_dev && record.data.id !== 0 && !disable_gen_button) {
                Ext.getCmp("generateconfigbutton").setDisabled(false);
            }
        }
        else {
            Ext.getCmp("servicedetailsform").getForm().getFields().each(function (field) {
                field.setReadOnly(true);
            });

            Ext.getCmp("saveservicedetails").setDisabled(true);
            Ext.getCmp("servicelayerjsonupdate").setDisabled(window.read_only);
            Ext.getCmp("servicelayerjsonupdateall").setDisabled(window.read_only);
            Ext.getCmp("deletewmsthemeconfig").setDisabled(true);
            Ext.getCmp("savewmsthemeconfig").setDisabled(true);
            Ext.getCmp("serviceurlinteditbutton").setDisabled(true);
            Ext.getCmp("serviceurlexteditbutton").setDisabled(true);
            Ext.getCmp("serviceurlseceditbutton").setDisabled(true);
            Ext.getCmp("addnewservice").setDisabled(true);
            Ext.getCmp("linkservice").setDisabled(true);
            Ext.getCmp("duplicateservice").setDisabled(true);
            Ext.getCmp("deleteservice").setDisabled(true);
            Ext.getCmp("newservicelayerbutton").setDisabled(true);
            Ext.getCmp("deleteservicelayerbutton").setDisabled(true);
            Ext.getCmp("generateconfigbutton").setDisabled(true);
            Ext.getCmp("deployprodconfigbutton").setDisabled(true);
            Ext.getCmp("deploystageconfigbutton").setDisabled(true);
            Ext.getCmp("undeployprodconfigbutton").setDisabled(true);
            Ext.getCmp("servicesearchmetadata").setDisabled(true);

            if (record.data.status_prod && !window.read_only && !record.data.external) {
                Ext.getCmp("undeployprodconfigbutton").setDisabled(false);
                if (datasetStatus !== "veröffentlicht") {
                    Ext.getCmp("deployprodconfigbutton").setDisabled(false);
                }
            }
            else if (record.data.status_stage && !record.data.status_prod && !window.read_only && !record.data.external) {
                Ext.getCmp("deployprodconfigbutton").setDisabled(false);
            }
        }

        if (window.read_only) {
            Ext.getCmp("saveservicelayerdetailsbutton").setDisabled(true);
        }
        else {
            Ext.getCmp("saveservicelayerdetailsbutton").setDisabled(false);
        }

        if (record.data.external || record.data.security_type === "keine") {
            Ext.getCmp("serviceurlsec").setHidden(true);
            Ext.getCmp("serviceurlseceditbutton").setHidden(true);
            Ext.getCmp("opensetallowedgroupsbutton").setHidden(true);
        }

        // empty service dependend stores
        Ext.getStore("ServiceKeywords").removeAll();
        Ext.getStore("ServiceConfig").removeAll();
        Ext.getStore("FeatureTypes").removeAll();
        Ext.getStore("FeatureTypesGeoserver").removeAll();

        // reset service conf gui elements
        Ext.getCmp("details_theme").setText("");
        Ext.getCmp("details_endpoint").setText("");
        Ext.getCmp("details_metadata").setText("");
        Ext.getCmp("details_service").setText("");
        Ext.getCmp("details_provider").setText("");
        Ext.getCmp("details_workspace").setText("");
        Ext.getCmp("details_namespace").setText("");
        Ext.getCmp("details_endpoint").setDisabled(true);
        Ext.getCmp("details_metadata").setDisabled(true);
        Ext.getCmp("details_theme").setDisabled(true);
        Ext.getCmp("details_service").setDisabled(true);
        Ext.getCmp("details_provider").setDisabled(true);
        Ext.getCmp("details_workspace").setDisabled(true);
        Ext.getCmp("details_namespace").setDisabled(true);
        Ext.getCmp("featuretypeconfpanel").setHidden(true);
        Ext.getCmp("featuretypeconfpanelGeoserver").setHidden(true);
        Ext.getCmp("wmsthemeconfigurationfieldset").setHidden(true);
        Ext.getCmp("deegreeconfcontainer").setHidden(true);
        Ext.getCmp("geoserverconfcontainer").setHidden(true);
        Ext.getCmp("ldproxyconfcontainer").setHidden(true);
        Ext.getCmp("mapserverconfcontainer").setHidden(true);
        Ext.getCmp("geoserverconfcontainer").setHidden(true);
        Ext.getCmp("conferrorstatus").setHidden(true);

        Ext.getCmp("serviceurlint").setEditable(false);
        Ext.getCmp("serviceurlext").setEditable(false);
        Ext.getCmp("serviceurlsec").setEditable(false);

        // if service metadata is coupled, set data to relevant fields
        if (record.data.md_id) {
            const csw = Ext.getStore("ConfigMetadataCatalogs").findRecord("id", record.data.metadata_catalog_id, 0, false, false, true);

            if (csw) {
                Ext.getCmp("servicesourcecswname").setValue(csw.data.name);
                Ext.getCmp("servicemetadatalink").setValue("<a href=\"" + csw.data.show_doc_url + record.data.md_id + "\" target=\"_blank\">Metadaten aufrufen</a>");

                Ext.getCmp("deleteservicemetadatacoupling").setDisabled(!editMode);
                Ext.getCmp("deleteservicemetadatacoupling").setHidden(false);
            }
        }

        // for WMS, prepare theme conf trees
        if ((record.data.type === "WMS" || record.data.type === "WMS-Time") && (record.data.software === "MapServer" || record.data.software === "deegree" || record.data.software === "GeoServer")) {
            Ext.getCmp("wmsthemeconfigurationfieldset").setHidden(false);

            // workaround: if same wms config loaded double, grouped layers were not shown
            if (this.lastLoadedWmsId !== record.data.id) {
                Ext.getStore("WMSThemeLayer").getRoot().removeAll();
                Ext.getStore("WMSThemeConfig").getRoot().removeAll();

                if (record.data.wms_theme_config) {
                    if (record.data.wms_theme_config.children) {
                        Ext.getStore("WMSThemeConfig").getRoot().appendChild(record.data.wms_theme_config.children);
                    }
                }

                Ext.getCmp("wmsthemeconfigtree").expandAll();

                this.lastLoadedWmsId = record.data.id;
            }
        }

        if (record.data.software === "MapServer" || record.data.software === "GeoServer") {
            Ext.getCmp("servicefolder").setHidden(true);
            Ext.getCmp("servicefolderprod").setHidden(true);
        }
        else {
            Ext.getCmp("servicefolder").setHidden(false);
            Ext.getCmp("servicefolderprod").setHidden(false);
        }

        if (record.data.id !== 0) {
            // load layers for selected service, when load finished fill theme layer tree
            Ext.getStore("LayersService").load({
                params: {service_id: record.data.id},
                callback: function () {
                    if (record.data.type === "WMS" || record.data.type === "WMS-Time") {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").fillLayerTreeStore();
                    }

                    Ext.getCmp("grid_servicelayers").getSelectionModel().select(0);
                }
            });

            if (!record.data.disable_config_management) {
                Ext.getStore("ServiceConfigsEdited").load({
                    params: {service_id: record.data.id},
                    callback: function (records) {
                        if (records.length > 0) {
                            Ext.getCmp("serviceconfigeditedcontainer").setCollapsed(false);
                        }
                        else {
                            Ext.getCmp("serviceconfigeditedcontainer").setCollapsed(true);
                        }
                    }
                });
            }

            if (UDPManager.Configs.getModules().visitStatistics && record.data.status_prod) {
                Ext.getStore("Visits").load({
                    params: {service_id: record.data.id}
                });
            }

            // if security module is active, prepare relevant fields
            if (UDPManager.Configs.getModules().serviceSecurity) {
                if (["WMS", "WMS-Time", "WFS", "WFS-T", "OAF"].includes(record.data.type)) {
                    Ext.getCmp("servicesecuritycontainer").setHidden(false);
                }
                else {
                    Ext.getCmp("servicesecuritycontainer").setHidden(true);
                }
            }
        }

        if (record.data.type === "WMS" || record.data.type === "WMS-Time") {
            Ext.getCmp("serviceclickradius").setHidden(false);
        }
        else {
            Ext.getCmp("serviceclickradius").setHidden(true);
        }

        if (["WMS", "WMS-Time", "WFS", "WFS-T"].includes(record.data.type)) {
            Ext.getCmp("servicemaxfeatures").setHidden(false);
        }
        else {
            Ext.getCmp("servicemaxfeatures").setHidden(true);
        }

        // always set service details as active tab, when select service
        Ext.getCmp("services_main_tabs").setActiveTab(0);

        // for deegree, MapServer and ldproxy services, load config
        // and activate deployment buttons
        if ((record.data.software === "deegree" || record.data.software === "ldproxy" || record.data.software === "MapServer" || record.data.software === "GeoServer") && record.data.id !== 0 && !record.data.disable_config_management) {
            let status = "dev";

            if (record.data.status_stage) {
                status = "stage";
            }
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").getServiceConfig(record.data, status);
        }

        if (record.data.metadata_catalog_id) {
            Ext.getCmp("servicemdid").setReadOnly(true);
            Ext.getCmp("servicedescription").setReadOnly(true);
            Ext.getCmp("servicekeywords").setReadOnly(true);
            Ext.getCmp("serviceaccessconstraints").setReadOnly(true);
            Ext.getCmp("servicefees").setReadOnly(true);
            Ext.getCmp("servicefeesjson").setReadOnly(true);
        }
        else {
            Ext.getCmp("servicemdid").setReadOnly(false);
            Ext.getCmp("servicedescription").setReadOnly(false);
            Ext.getCmp("servicekeywords").setReadOnly(false);
            Ext.getCmp("serviceaccessconstraints").setReadOnly(false);
            Ext.getCmp("servicefees").setReadOnly(false);
            Ext.getCmp("servicefeesjson").setReadOnly(false);
        }

        Ext.getCmp("servicedetailsform").setDisabled(false);

        Ext.getCmp("servicelayerdetailsform").getForm().reset();

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllServiceLayerDetailsFieldsetsHidden();
        UDPManager.app.getController("UDPManager.controller.PublicFunctions").emptyServiceLayerJsonField();

        // set segment button for service details
        Ext.getCmp("detailssegbtndev").setDisabled(true);
        Ext.getCmp("detailssegbtnstage").setDisabled(true);
        Ext.getCmp("detailssegbtnprod").setDisabled(true);
        // set segment button for api configs
        Ext.getCmp("configsegbtndev").setDisabled(true);
        Ext.getCmp("configsegbtnstage").setDisabled(true);
        Ext.getCmp("configsegbtnprod").setDisabled(true);

        if (UDPManager.Configs.getModules().visitStatistics) {
            Ext.getCmp("datasetservices_statistics").setDisabled(true);
        }

        if (record.data.status_dev) {
            Ext.getCmp("detailssegbtndev").setDisabled(false);
            Ext.getCmp("configsegbtndev").setDisabled(false);
            Ext.getCmp("statusspecificicon").setHtml("<div class='x-fa fa-circle orangeIconSmall'></div>");
        }
        if (record.data.status_stage) {
            Ext.getCmp("detailssegbtnstage").setDisabled(false);
            Ext.getCmp("configsegbtnstage").setDisabled(false);
            Ext.getCmp("statusspecificicon").setHtml("<div class='x-fa fa-circle yellowIconSmall'></div>");
        }
        if (record.data.status_prod) {
            Ext.getCmp("detailssegbtnprod").setDisabled(false);
            Ext.getCmp("configsegbtnprod").setDisabled(false);

            if (UDPManager.Configs.getModules().visitStatistics) {
                Ext.getCmp("datasetservices_statistics").setDisabled(false);
            }
        }

        if (record.data.disable_config_module) {
            Ext.getCmp("confmoduledisabledstatus").setHidden(false);
            Ext.getCmp("servicespecialsettings").setCollapsed(false);
        }
        else {
            Ext.getCmp("confmoduledisabledstatus").setHidden(true);
            Ext.getCmp("servicespecialsettings").setCollapsed(true);
        }

        if (record.data.id !== 0) {
            Ext.getCmp("shareservice").setDisabled(false);
        }
    },

    onSelectServiceDetails: function (btn) {
        if (btn.id === "detailssegbtndev" || btn.id === "detailssegbtnstage") {
            Ext.getCmp("statusspecificfieldsetdevstage").setHidden(false);
            Ext.getCmp("statusspecificfieldsetprod").setHidden(true);
        }
        else {
            Ext.getCmp("statusspecificfieldsetdevstage").setHidden(true);
            Ext.getCmp("statusspecificfieldsetprod").setHidden(false);
        }
    },

    onAddNewService: function () {
        const dataset_shortname = Ext.getCmp("datasetshortname").getValue();

        if (dataset_shortname.includes("change_it_")) {
            Ext.Msg.alert("Achtung", "Der Kurzname des Datensatzes muss angepasst werden!");
        }
        else {
            const external = Ext.getCmp("datasetexternal").getValue();
            const rec = Ext.getStore("ServicesDataset").add({
                id: 0,
                title: "*NEU*",
                name: "",
                type: "",
                server: "",
                folder: "",
                url_int: "",
                url_ext: "",
                status_dev: !external,
                status_stage: false,
                status_prod: external,
                security_type: "keine",
                software: "",
                server_prod: "",
                folder_prod: "",
                external: external,
                version: ""
            })[0];

            Ext.getCmp("grid_datasetservices").getSelectionModel().select(rec);
        }
    },

    onServiceTypeChange: function (obj, service_type) {
        if (Ext.getCmp("grid_datasetservices").selModel.hasSelection()) {
            const service_id = Ext.getCmp("grid_datasetservices").getSelectionModel().getSelection()[0].data.id;

            if (parseInt(service_id) === 0 || service_id === "0") {
                const shortname = Ext.getCmp("datasetshortname").getValue();
                const dataset_title = Ext.getCmp("datasettitle").getValue();

                if (service_type) {
                    Ext.getCmp("servicetitle").setValue(service_type.replace("-Time", "") + " " + dataset_title);
                }

                if (["WMS", "WMS-Time", "WFS", "WFS-T"].includes(service_type)) {

                    Ext.getCmp("servicename").setValue(service_type.replace("-Time", "").toLowerCase() + "_" + shortname);

                    if (service_type === "WMS") {
                        Ext.getCmp("serviceversion").setValue("1.3.0");
                    }
                    else if (service_type === "WMS-Time") {
                        Ext.getCmp("serviceversion").setValue("1.1.1");
                    }
                    else if (service_type === "WFS" || service_type === "WFS-T") {
                        Ext.getCmp("serviceversion").setValue("1.1.0");
                    }
                }
                else if (service_type === "OAF") {
                    Ext.getCmp("servicename").setValue(shortname);
                    Ext.getCmp("serviceversion").setValue("1.0");
                }

                if (service_type === "WMS" || service_type === "WMS-Time") {
                    Ext.getCmp("serviceclickradius").setHidden(false);
                }
                else {
                    Ext.getCmp("serviceclickradius").setHidden(true);
                }

                if (["WMS", "WMS-Time", "WFS", "WFS-T"].includes(service_type)) {
                    Ext.getCmp("servicemaxfeatures").setHidden(false);
                }
                else {
                    Ext.getCmp("servicemaxfeatures").setHidden(true);
                }
            }
        }

        const softwarestore = UDPManager.Configs.getSoftwareByType(service_type);

        Ext.getCmp("servicesoftware").bindStore(softwarestore);

        const versionstore = UDPManager.Configs.getServiceVersions(service_type);

        Ext.getCmp("serviceversion").bindStore(versionstore);
    },

    onFieldValueChanged: function (obj, newValue) {
        if (Ext.getCmp("grid_datasetservices").getSelectionModel().hasSelection()) {
            const selectedService = Ext.getCmp("grid_datasetservices").getSelectionModel().getSelection()[0].data;
            const serverObj = UDPManager.Configs.getServerByName(selectedService.server);
            const software = Ext.getCmp("servicesoftware").getSubmitValue();
            const folder = Ext.getCmp("servicefolder").getSubmitValue();
            const server = Ext.getCmp("serviceserver").getSubmitValue();
            const name = Ext.getCmp("servicename").getSubmitValue();
            const typ = Ext.getCmp("servicetype").getSubmitValue();

            if (selectedService.status_dev) {
                if (server && name && software && serverObj) {
                    const urlInt = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlInt(software, folder, server, name, "dev", typ);

                    Ext.getCmp("serviceurlint").setValue(urlInt);

                    if (!selectedService.url_ext) {
                        const newUrlExt = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlExt(software, folder, server, name, "dev", typ);

                        Ext.getCmp("serviceurlext").setValue(newUrlExt);
                    }
                }

                if (UDPManager.Configs.getModules().serviceSecurity) {
                    const security_type = Ext.getCmp("servicesecuritytype").getSubmitValue();

                    if (security_type === "Basic Auth") {
                        Ext.getCmp("opensetallowedgroupsbutton").setHidden(true);
                        if (server && folder && name) {
                            const url_secured = UDPManager.Configs.getSecurityProxyUrlAuth() + "/" + software.toLowerCase() + "/" + name;

                            Ext.getCmp("serviceurlsec").setValue(url_secured);
                        }
                    }
                }

                if (obj.id === "servicesoftware") {
                    if (newValue) {
                        const config_software = UDPManager.Configs.getSoftwareConfigByName(newValue);

                        if (config_software) {
                            Ext.getCmp("servicefolder").setStore(config_software.dev.instances);

                            if (config_software.dev.instances.length === 1) {
                                Ext.getCmp("servicefolder").setValue(config_software.dev.instances[0]);
                            }
                            else if (config_software.dev.instances.length === 0) {
                                Ext.getCmp("servicefolder").setHidden(true);
                            }
                        }
                    }
                    if (selectedService.id === 0 && newValue) {
                        Ext.getCmp("serviceserver").setValue(UDPManager.Configs.getDefaultServer(newValue).dev);
                    }
                }

                if (obj.id === "serviceserver") {
                    const urlInt = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlInt(software, folder, newValue, name, "dev", typ);

                    Ext.getCmp("serviceurlint").setValue(urlInt);

                    if (!selectedService.url_ext) {
                        const newUrlExt = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlExt(software, folder, newValue, name, "dev", typ);

                        Ext.getCmp("serviceurlext").setValue(newUrlExt);
                    }
                }
            }
        }
    },

    saveWmsThemeConfig: function () {
        const themeConfig = Ext.getStore("WMSThemeConfig").getRoot().serialize();
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        let themeConfigSerial;

        if (themeConfig.children) {
            themeConfigSerial = JSON.stringify(themeConfig);
        }
        else {
            themeConfigSerial = null;
        }

        Ext.Ajax.request({
            url: "backend/updatewmsthemeconfig",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                service_id: Ext.getCmp("serviceid").getSubmitValue(),
                theme_config: themeConfigSerial,
                dataset_id: dataset_id,
                last_edited_by: window.auth_user
            },
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Konfiguration erfolgreich gespeichert!");
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
            },
            failure: function (response) {
                Ext.MessageBox.alert("Status", "Es ist ein Fehler aufgetreten!");
                console.log(response.responseText);
            }
        });
    },

    deleteWmsThemeConfig: function () {
        Ext.getStore("WMSThemeConfig").getRoot().removeAll();

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").fillLayerTreeStore();
    },

    onBeforeDeploy: function (btn) {
        const externalDataset = Ext.getCmp("datasetexternal").getValue();
        let allServicesExternal = true;
        const servicesToDeploy = {};
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();

        Ext.getStore("ServicesDataset").each(function (service) {
            if (!service.data.external) {
                allServicesExternal = false;
            }
            else {
                servicesToDeploy[service.data.id] = {
                    service_id: service.data.id,
                    title: service.data.title,
                    src: "prod",
                    dest: "prod",
                    external: true,
                    status_dev: service.data.status_dev,
                    status_stage: service.data.status_stage,
                    status_prod: service.data.status_prod,
                    dataset_id: parseInt(dataset_id),
                    last_edited_by: window.auth_user
                };
            }
        });

        if (!externalDataset && allServicesExternal) {
            Ext.Ajax.request({
                url: "backend/deployserviceconfig",
                method: "POST",
                headers: {token: window.apiToken},
                timeout: 120000,
                jsonData: servicesToDeploy,
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.newStatus === "veröffentlicht") {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast(`Bearbeitung erfolgreich abgeschlossen!`);
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsReadOnly();
                        Ext.getCmp("deployprodconfigbutton").setDisabled(true);
                    }

                    Ext.getCmp("datasetstatus").setValue(response_json.newStatus);

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                },
                failure: function (response) {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                    Ext.Msg.alert("Fehler", "Die Schnittstellenkonfiguration konnte nicht verschoben werden!");
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").openWorkspaceChooserWindow(btn);
        }
    },

    onBeforeUnDeploy: function (btn) {
        const mb = Ext.MessageBox;

        mb.buttonText.yes = "ja";
        mb.buttonText.no = "nein";

        mb.confirm("Alle Schnittstellen aus Prod löschen", "Veröffentlichung wirklich zurück nehmen?", function (confirmBtn) {
            if (confirmBtn === "yes") {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").openWorkspaceChooserWindow(btn);
            }
        });
    },

    onExternalServiceChanged: function (checkbox, newValue) {
        const service = Ext.getCmp("grid_datasetservices").getSelectionModel().getSelection()[0];

        if (service) {
            const servicesoftwareCombo = Ext.getCmp("servicesoftware");
            const reverseProxyCheckBox = Ext.getCmp("servicereverseproxy");
            const serviceserverCombo = Ext.getCmp("serviceserver");
            const servicefolderTextField = Ext.getCmp("servicefolder");
            const serviceserverprodCombo = Ext.getCmp("serviceserverprod");
            const servicefolderprodTextField = Ext.getCmp("servicefolderprod");
            const servicenameTextField = Ext.getCmp("servicename");
            const serviceurlintTextField = Ext.getCmp("serviceurlint");
            const serviceurlextTextField = Ext.getCmp("serviceurlext");
            const serviceurlintprodTextField = Ext.getCmp("serviceurlintprod");
            const serviceurlextprodTextField = Ext.getCmp("serviceurlextprod");

            const statusspecificfieldsetdevstage = Ext.getCmp("statusspecificfieldsetdevstage");
            const statusspecificfieldsetprod = Ext.getCmp("statusspecificfieldsetprod");
            const servicespecialsettingsFieldset = Ext.getCmp("servicespecialsettings");
            const datasetservicesconfigurationTab = Ext.getCmp("datasetservices_configuration");

            if (newValue) {
                service.set("status_dev", false);
                service.set("status_stage", false);
                service.set("status_prod", true);

                // show prod values
                statusspecificfieldsetdevstage.setHidden(true);
                statusspecificfieldsetprod.setHidden(false);
                // special settings
                servicespecialsettingsFieldset.setHidden(true);
                // configuration
                datasetservicesconfigurationTab.setDisabled(true);
                // ReverseProxy
                reverseProxyCheckBox.show();
                if (servicesoftwareCombo.getSubmitValue() === "ReverseProxy") {
                    reverseProxyCheckBox.setRawValue(true);
                    serviceurlintTextField.setFieldLabel("Quell URL");
                    serviceurlintTextField.setEditable(true);
                    serviceurlintprodTextField.setFieldLabel("Quell URL");
                    serviceurlintprodTextField.setEditable(true);
                }
                else {
                    serviceurlintTextField.setDisabled(true);
                    serviceurlintTextField.setEditable(false);
                    serviceurlintprodTextField.setDisabled(true);
                    serviceurlintprodTextField.setEditable(false);
                }
                servicesoftwareCombo.setDisabled(true);
                // Server
                serviceserverCombo.setDisabled(true);
                serviceserverprodCombo.setDisabled(true);
                // Ordner / Webapp
                servicefolderTextField.setDisabled(true);
                servicefolderprodTextField.setDisabled(true);
                // Dienstname / Endpoint
                servicenameTextField.setDisabled(true);
                // Absicherung
                if (UDPManager.Configs.getModules().serviceSecurity) {
                    Ext.getCmp("servicesecuritytype").setDisabled(true);
                    Ext.getCmp("serviceurlsec").setDisabled(true);
                }
                serviceurlextTextField.setEditable(true);
                serviceurlextprodTextField.setEditable(true);
            }
            else {
                service.set("status_dev", true);
                service.set("status_stage", false);
                service.set("status_prod", false);

                // special settings
                servicespecialsettingsFieldset.setHidden(false);
                // show dev/stage values
                statusspecificfieldsetdevstage.setHidden(false);
                statusspecificfieldsetprod.setHidden(true);
                // configuration
                datasetservicesconfigurationTab.setDisabled(false);
                // ReverseProxy
                reverseProxyCheckBox.hide();
                // Software
                servicesoftwareCombo.setDisabled(false);
                // Server
                serviceserverCombo.setDisabled(false);
                serviceserverCombo.allowBlank = false;
                serviceserverprodCombo.setDisabled(false);
                serviceserverprodCombo.allowBlank = false;
                // Ordner / Webapp
                servicefolderTextField.setDisabled(false);
                servicefolderprodTextField.setDisabled(false);
                // Dienstname / Endpoint
                servicenameTextField.setDisabled(false);
                // interne URL
                serviceurlintTextField.setDisabled(false);
                serviceurlintprodTextField.setDisabled(false);
                // Absicherung
                if (UDPManager.Configs.getModules().serviceSecurity) {
                    Ext.getCmp("servicesecuritytype").setDisabled(false);
                    Ext.getCmp("serviceurlsec").setDisabled(false);
                }
                serviceurlextTextField.setEditable(false);
                serviceurlintTextField.setEditable(false);
                serviceurlextprodTextField.setEditable(false);
                serviceurlintprodTextField.setEditable(false);
            }
        }
    },

    onDisableConfigModuleChanged: function (checkbox, newValue) {
        Ext.getCmp("servicedisableconfigmanagement").setHidden(newValue);

        if (newValue) {
            //Ext.getCmp("grid_datasetservices").getSelectionModel().getSelection()[0].set("disable_config_management", !newValue);
            Ext.getCmp("servicedisableconfigmanagement").setValue(!newValue);
        }
    },

    onDisableConfigManagementChanged: function (checkbox, newValue) {
        const datasetservicesconfigurationTab = Ext.getCmp("datasetservices_configuration");

        datasetservicesconfigurationTab.setDisabled(newValue);

        Ext.getCmp("servicedisableconfigmodule").setHidden(newValue);

        if (newValue) {
            //Ext.getCmp("grid_datasetservices").getSelectionModel().getSelection()[0].set("disable_config_module", !newValue);
            Ext.getCmp("servicedisableconfigmodule").setValue(!newValue);
        }
    },

    onReverseProxyCheckBoxChanged: function (checkbox, newValue) {
        const servicesoftwareCombo = Ext.getCmp("servicesoftware");
        const serviceurlintTextField = Ext.getCmp("serviceurlint");
        const serviceurlintprodTextField = Ext.getCmp("serviceurlintprod");

        if (newValue) {
            servicesoftwareCombo.setValue("ReverseProxy");

            serviceurlintTextField.setDisabled(false);
            serviceurlintTextField.setEditable(true);
            serviceurlintTextField.setFieldLabel("Quell URL");
            serviceurlintprodTextField.setDisabled(false);
            serviceurlintprodTextField.setEditable(true);
            serviceurlintprodTextField.setFieldLabel("Quell URL");
        }
        else {
            serviceurlintTextField.setDisabled(true);
            serviceurlintTextField.setEditable(false);
            serviceurlintTextField.setFieldLabel("interne URL");
            serviceurlintprodTextField.setDisabled(true);
            serviceurlintprodTextField.setEditable(false);
            serviceurlintprodTextField.setFieldLabel("interne URL");
        }
    },

    onSaveServiceDetails: function () {
        const fieldvalues = Ext.getCmp("servicedetailsform").getForm().getFieldValues();
        const source_csw_selection = Ext.getCmp("servicesourcecswname").getSelection();
        const collection_id = Ext.getCmp("datasetcollectionid").getValue();
        const scope_id = Ext.getCmp("saveservicedetails").id;
        const responsible_party_values = Ext.getCmp("datasetresponsibleparty").getSelection().data;
        const mail_from = "\"" + responsible_party_values.inbox_name + "\" <" + responsible_party_values.mail + ">";
        let status = "dev";

        if (fieldvalues.status_stage) {
            status = "stage";
        }

        fieldvalues.metadata_catalog_id = null;
        fieldvalues.last_edited_by = window.auth_user;
        fieldvalues.dataset_id = Ext.getCmp("datasetid").getValue();
        fieldvalues.responsible_party_name = responsible_party_values.name;
        fieldvalues.responsible_party_mail = responsible_party_values.mail;
        fieldvalues.mail_from = mail_from;

        let correct_workspace = false;

        if (!fieldvalues.external) {
            const software_config = UDPManager.Configs.getSoftwareConfigByName(fieldvalues.software);

            if (software_config) {
                if (software_config[status].instances.includes(fieldvalues.folder) || software_config[status].instances.length > 0) {
                    correct_workspace = true;
                }
            }
        }

        if (source_csw_selection) {
            fieldvalues.metadata_catalog_id = source_csw_selection.data.id;
        }
        if (fieldvalues.external) {
            fieldvalues.name = "";
            fieldvalues.software = "";
            fieldvalues.server = "";
            fieldvalues.folder = "";
            fieldvalues.url_int = "";
            fieldvalues.url_ext = "";
            fieldvalues.server_prod = "";
            fieldvalues.folder_prod = "";
            fieldvalues.url_int_prod = "";
            fieldvalues.url_sec = "";
            fieldvalues.security_type = "";
            fieldvalues.disable_config_management = true;
        }

        const unique_service_test = Ext.getStore("ServicesDataset").findRecord("name", fieldvalues.name, 0, false, false, true);

        if (!UDPManager.app.getController("UDPManager.controller.PublicFunctions").isJsonString(fieldvalues.fees_json) && fieldvalues.fees_json !== "") {
            Ext.MessageBox.alert("Fehler", "Nutzungsbedingungen (JSON) kein valider JSON String");
        }
        else if (!correct_workspace && fieldvalues.software === "deegree" && !fieldvalues.external) {
            Ext.MessageBox.alert("Fehler", "bitte Ordner korrekt setzen");
        }
        else if (unique_service_test && unique_service_test.data.id !== fieldvalues.id) {
            console.log(unique_service_test, unique_service_test.data.id, fieldvalues.id);
            Ext.MessageBox.alert("Fehler", "Endpunktname bereits vergeben!");
        }
        else {
            Ext.getCmp("saveservicedetails").setDisabled(true);

            Ext.Ajax.request({
                url: "backend/saveservice",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: fieldvalues,
                success: function (response) {
                    Ext.getCmp("saveservicedetails").setDisabled(window.read_only);

                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        Ext.getStore("ServicesDataset").load({
                            params: {dataset_id: Ext.getCmp("datasetid").getValue()},
                            callback: async function () {
                                if (fieldvalues.id === 0) {
                                    const collectionvalues = Ext.getStore("CollectionsDataset").getData().items;
                                    const layervalues = {
                                        id: 0,
                                        service_id: response_json.id,
                                        dataset_id: fieldvalues.dataset_id,
                                        last_edited_by: window.auth_user
                                    };
                                    const dataset_store_type = Ext.getCmp("datasetstoretype").getValue();

                                    for (const collection of collectionvalues) {
                                        if (collection.data.api_constraint === "only_visual" && fieldvalues.type !== "WMS" && fieldvalues.type !== "WMS-Time") {
                                            continue;
                                        }
                                        if (collection.data.api_constraint === "only_download" && fieldvalues.type !== "WFS" && fieldvalues.type !== "WFS-T" && fieldvalues.type !== "OAF") {
                                            continue;
                                        }
                                        if ((collection.data.store_type === "Raster" || dataset_store_type === "Raster") && fieldvalues.software !== "MapServer") {
                                            continue;
                                        }
                                        if ((collection.data.store_type === "Vektor" || dataset_store_type === "Vektor") && fieldvalues.software === "MapServer") {
                                            continue;
                                        }
                                        layervalues.collection_id = collection.data.id;

                                        const ajax_request_layerdata = UDPManager.app.getController("UDPManager.controller.PublicFunctions").stAttributesSetter(fieldvalues.type, collection.data.group_object, layervalues);

                                        if (!Ext.Object.isEmpty(ajax_request_layerdata.st_attributes)) {
                                            await UDPManager.app.getController("UDPManager.controller.PublicFunctions").autoCreateLayer(ajax_request_layerdata);
                                        }
                                    }
                                }
                                Ext.getStore("LayersCollection").load({
                                    params: {collection_id: collection_id}
                                });
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").onRecreateJson(scope_id, "dev");
                                Ext.getStore("ServiceKeywords").load();

                                if (fieldvalues.metadata_catalog_id !== null) {
                                    Ext.getCmp("servicemdid").setReadOnly(true);
                                    Ext.getCmp("servicedescription").setReadOnly(true);
                                    Ext.getCmp("servicekeywords").setReadOnly(true);
                                    Ext.getCmp("serviceaccessconstraints").setReadOnly(true);
                                    Ext.getCmp("servicefees").setReadOnly(true);
                                    Ext.getCmp("servicefeesjson").setReadOnly(true);
                                }
                                else {
                                    Ext.getCmp("servicemdid").setReadOnly(false);
                                    Ext.getCmp("servicedescription").setReadOnly(false);
                                    Ext.getCmp("servicekeywords").setReadOnly(false);
                                    Ext.getCmp("serviceaccessconstraints").setReadOnly(false);
                                    Ext.getCmp("servicefees").setReadOnly(false);
                                    Ext.getCmp("servicefeesjson").setReadOnly(false);
                                }

                                const service = Ext.getStore("ServicesDataset").findRecord("id", response_json.id, 0, false, false, true);
                                const service_grid = Ext.getCmp("grid_datasetservices");
                                const row_service = service_grid.store.indexOf(service);

                                service_grid.getSelectionModel().select(row_service);
                                service_grid.ensureVisible(row_service);

                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Schnittstelle erfolgreich gespeichert!");
                            }
                        });
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                },
                failure: function (response) {
                    Ext.getCmp("saveservicedetails").setDisabled(window.read_only);
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
    },

    onDeleteService: function () {
        Ext.getCmp("deleteservice").setDisabled(true);

        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);

        // first, get all dataset/service links
        this.getServiceLinks(service_id).then(function (serviceLinks) {
            let delete_service = false;
            let type = "Verknüpfung";

            // if only one datasets is coupled, the service object will be deleted, otherwise only the service link will be deletetd
            if (serviceLinks.length === 1) {
                delete_service = true;
                type = "Schnittstelle";
            }

            const mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Löschen", type + " wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("mainviewport");

                    Ext.Ajax.request({
                        url: "backend/deleteservice",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            id: service_id,
                            dataset_id: Ext.getCmp("datasetid").getSubmitValue(),
                            status: service.data.status,
                            title: service.data.title,
                            software: service.data.software,
                            delete_service: delete_service,
                            external: service.data.external,
                            disable_config_management: service.data.disable_config_management,
                            last_edited_by: window.auth_user
                        },
                        success: function (response) {
                            const response_json = Ext.decode(response.responseText);

                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();

                            if (response_json.status === "success") {
                                Ext.getStore("ServicesDataset").load({
                                    params: {dataset_id: Ext.getCmp("datasetid").getValue()},
                                    callback: function (records) {
                                        if (records.length === 0) {
                                            Ext.getCmp("saveservicedetails").setDisabled(true);
                                            Ext.getCmp("deleteservice").setDisabled(true);
                                            Ext.getCmp("duplicateservice").setDisabled(true);
                                            Ext.getCmp("deleteservice").setDisabled(true);
                                            Ext.getCmp("shareservice").setDisabled(true);
                                            Ext.getCmp("generateconfigbutton").setDisabled(true);
                                            Ext.getCmp("deployprodconfigbutton").setDisabled(true);
                                            Ext.getCmp("deploystageconfigbutton").setDisabled(true);
                                            Ext.getCmp("undeployprodconfigbutton").setDisabled(true);
                                            Ext.getCmp("saveservicedetails").setDisabled(true);
                                        }
                                        else {
                                            Ext.getCmp("deleteservice").setDisabled(false);
                                        }
                                    }
                                });
                                Ext.getCmp("servicedetailsform").getForm().reset();
                                Ext.getCmp("servicemetadatalink").setValue("Keine Metadaten verknüpft");
                                Ext.getCmp("deleteservicemetadatacoupling").setDisabled(true);
                                Ext.getCmp("deleteservicemetadatacoupling").setHidden(true);

                                if (delete_service) {
                                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Schnittstelle erfolgreich gelöscht!");
                                }
                                else {
                                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Verknüpfung erfolgreich gelöscht!");
                                }
                            }
                            else {
                                if (response_json.code === "23503") {
                                    Ext.MessageBox.alert("Fehler", "Schnittstelle kann nicht gelöscht werden, es sind noch verknüpfte Layer Objekte vorhanden!");
                                }
                                else {
                                    Ext.MessageBox.alert("Fehler", response_json.message);
                                }
                            }
                        },
                        failure: function (response) {
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                            console.log(Ext.decode(response.responseText));
                            Ext.MessageBox.alert("Fehler", "Schnittstelle konnte nicht gelöscht werden");
                            Ext.getCmp("deleteservice").setDisabled(false);
                        }
                    });
                }
                else {
                    Ext.getCmp("deleteservice").setDisabled(false);
                }
            });
        });
    },

    onDuplicateService: function () {
        const formfieldvalues = Ext.getCmp("servicedetailsform").getForm().getFieldValues();

        formfieldvalues.id = 0;
        formfieldvalues.title = formfieldvalues.title + " KOPIE";
        formfieldvalues.name = formfieldvalues.name + "_duplicate";

        const rec = new UDPManager.model.UDPManagerModel(formfieldvalues);
        const count = Ext.getStore("ServicesDataset").count();

        Ext.getStore("ServicesDataset").insert(count, rec);

        Ext.getCmp("grid_datasetservices").getSelectionModel().select(rec);
        Ext.getCmp("grid_datasetservices").ensureVisible(rec);
    },

    onSecurityTypeChange: function () {
        const security_type = Ext.getCmp("servicesecuritytype").getSubmitValue();
        const software = Ext.getCmp("servicesoftware").getSubmitValue();
        const folder = Ext.getCmp("servicefolder").getSubmitValue();
        const server = Ext.getCmp("serviceserver").getSubmitValue();
        const service_name = Ext.getCmp("servicename").getSubmitValue();
        const status_dev = Ext.getCmp("servicestatusdev").getValue();
        let url_secured;

        if (security_type !== "keine") {
            if (security_type === "Basic Auth") {
                Ext.getCmp("serviceurlsec").setHidden(false);
                Ext.getCmp("serviceurlseceditbutton").setHidden(window.read_only);
                Ext.getCmp("opensetallowedgroupsbutton").setHidden(true);
                if (software !== "" && service_name !== "") {
                    url_secured = UDPManager.Configs.getSecurityProxyUrlAuth() + "/" + software.toLowerCase() + "/" + service_name;
                }
            }
            else if (security_type === "AD SSO") {
                const url_split = Ext.getCmp("serviceurlext").getSubmitValue().split("/");

                url_secured = "/" + url_split[url_split.length - 1];
                Ext.getCmp("serviceurlsec").setHidden(false);
                Ext.getCmp("serviceurlseceditbutton").setHidden(window.read_only);
                Ext.getCmp("opensetallowedgroupsbutton").setHidden(window.read_only);
            }
            else {
                Ext.getCmp("serviceurlsec").setHidden(true);
                Ext.getCmp("opensetallowedgroupsbutton").setHidden(true);
            }

            if (Ext.getCmp("serviceurlsec").getSubmitValue() === "" || (Ext.getCmp("serviceurlsec").getSubmitValue().indexOf(url_secured) === -1 && security_type === "AD SSO") || (Ext.getCmp("serviceurlsec").getSubmitValue().indexOf(UDPManager.Configs.getSecurityProxyUrlAuth()) === -1 && security_type === "Basic Auth")) {
                Ext.getCmp("serviceurlsec").setValue(url_secured);
            }

            if (security_type === "AD SSO" && (Ext.getCmp("serviceurlext").getSubmitValue() === "" || Ext.getCmp("serviceurlext").getSubmitValue().indexOf(UDPManager.Configs.getSecurityProxyUrlSso().dev) === -1) && status_dev) {
                Ext.getCmp("serviceurlext").setValue(UDPManager.Configs.getSecurityProxyUrlSso().dev + url_secured);
            }
            else if (security_type === "Basic Auth" && (Ext.getCmp("serviceurlext").getSubmitValue() === "") && status_dev) {
                if (software && folder && server && service_name) {
                    const url = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlExt(software, folder, server, service_name, "dev");

                    Ext.getCmp("serviceurlext").setValue(url);
                }
            }
        }
        else {
            Ext.getCmp("opensetallowedgroupsbutton").setHidden(true);
            Ext.getCmp("serviceurlsec").setHidden(true);
            Ext.getCmp("serviceurlseceditbutton").setHidden(true);
            Ext.getCmp("serviceurlsec").setValue("");
            if (Ext.getCmp("serviceurlext").getSubmitValue() === "" || Ext.getCmp("serviceurlext").getSubmitValue().indexOf(UDPManager.Configs.getSecurityProxyUrlAuth()) > -1 || Ext.getCmp("serviceurlext").getSubmitValue().indexOf(UDPManager.Configs.getSecurityProxyUrlSso().dev) > -1) {
                if (software && folder && server && service_name && status_dev) {
                    const url = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlExt(software, folder, server, service_name, "dev");

                    Ext.getCmp("serviceurlext").setValue(url);
                }
            }
        }
    },

    onEditInternalUrlToggle: function () {
        Ext.getCmp("serviceurlint").setEditable(!Ext.getCmp("serviceurlint").getEditable());

        if (Ext.getCmp("serviceurlint").getEditable()) {
            Ext.Msg.alert("Achtung", "URL muss nur bei externen Diensten manuell eingetragen werden!", Ext.emptyFn);
        }
    },

    onEditExternalUrlToggle: function () {
        Ext.getCmp("serviceurlext").setEditable(!Ext.getCmp("serviceurlext").getEditable());

        if (Ext.getCmp("serviceurlext").getEditable()) {
            Ext.Msg.alert("Achtung", "URL muss nur bei externen Diensten manuell eingetragen werden!", Ext.emptyFn);
        }
    },

    onEditSecUrlToggle: function () {
        Ext.getCmp("serviceurlsec").setEditable(!Ext.getCmp("serviceurlsec").getEditable());

        if (Ext.getCmp("serviceurlsec").getEditable()) {
            Ext.Msg.alert("Achtung", "URL muss nur bei externen Diensten manuell eingetragen werden!", Ext.emptyFn);
        }
    },

    onOpenSetAllowedGroups: function () {
        const allowed_groups_store = Ext.getStore("AllowedADGroups");
        const ad_groups_store = Ext.getStore("ADGroups");
        const service = Ext.getStore("ServicesDataset").query("id", Ext.getCmp("serviceid").getSubmitValue(), false, false, true).items[0].data;

        allowed_groups_store.removeAll();
        ad_groups_store.removeAll();

        if (service.allowed_groups !== "" && service.allowed_groups) {
            for (let i = 0; i < service.allowed_groups.length; i++) {
                allowed_groups_store.add({name: service.allowed_groups[i]});
            }
        }

        let allowedGroupsChooserWindow = Ext.getCmp("allowedgroupschooser-window");

        if (!allowedGroupsChooserWindow) {
            allowedGroupsChooserWindow = Ext.create("allowedgroupschooser.AllowedGroupsChooser");
        }
        allowedGroupsChooserWindow.show();

        Ext.getCmp("saveserviceallowedgroupsbutton").setHidden(false);
    },

    getServiceLinks: function (service_id) {
        return new Ext.Promise(function (resolve, reject) {
            Ext.Ajax.request({
                url: "backend/getservicelinks",
                headers: {token: window.apiToken},
                jsonData: {
                    service_id: service_id
                },
                success: function (response) {
                    resolve(Ext.decode(response.responseText));
                },
                failure: function (response) {
                    reject(response.status);
                }
            });
        });
    },

    onSearchMetadata: function (btn) {
        const source_csw_selection = Ext.getCmp("servicesourcecswname").getSelection();
        const servicetitle = Ext.getCmp("servicetitle").getSubmitValue();
        const servicemdid = Ext.getCmp("servicemdid").getSubmitValue();

        if (source_csw_selection) {
            btn.setDisabled(true);

            const loadingsMask = new Ext.LoadMask({
                msg: "Bitte warten...",
                target: Ext.getCmp("services_main_tabs")
            });

            loadingsMask.show();

            Ext.Ajax.request({
                url: "backend/getservicemetadata",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    servicetitle: servicetitle,
                    servicemdid: servicemdid,
                    metadata_catalog_id: source_csw_selection.data.id
                },
                success: function (response) {
                    const results = Ext.decode(response.responseText);

                    btn.setDisabled(false);

                    if (!results.error) {
                        if (results.results.length > 0) {
                            const store = Ext.getStore("MetadataSets");
                            const linkMetadataWindow = Ext.getCmp("linkmetadata-window");

                            store.removeAll();
                            store.add(results.results);

                            if (!linkMetadataWindow) {
                                Ext.create("datasets.metadata.linkmetadata").showWindow("service");
                            }
                            else {
                                linkMetadataWindow.showWindow("service");
                            }
                        }
                        else {
                            Ext.MessageBox.alert("Status", "Keine Treffer im " + source_csw_selection.data.name);
                        }
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", "Fehler beim Abruf des Katalogs " + source_csw_selection.data.name);
                    }

                    loadingsMask.hide();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Fehler beim Abruf des Katalogs " + source_csw_selection.data.name);
                    btn.setDisabled(false);
                    loadingsMask.hide();
                }
            });
        }
    },

    onLinkService: function () {
        Ext.getStore("Services").load();

        let serviceLinkerWindow = Ext.getCmp("servicelinker-window");

        if (serviceLinkerWindow) {
            serviceLinkerWindow.destroy();
        }

        serviceLinkerWindow = Ext.create("servicelinker.ServiceLinker");

        serviceLinkerWindow.show();
    },

    onDeleteMetadataCouplingService: function (btn) {
        UDPManager.app.getController("UDPManager.controller.PublicFunctions").onDeleteMetadataCoupling(btn);
    },

    onApiCall: function (grid, rowIndex, colIndex, item) {
        const status = "status_" + item.itemId.split("status")[1];
        const row_data = grid.getStore().getAt(rowIndex).data;

        if (row_data[status]) {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").onApiCall(grid, rowIndex, status);
        }
    },

    onShareService: function () {
        let shareWindow = Ext.getCmp("share-window");

        if (!shareWindow) {
            shareWindow = Ext.create("datasets.ShareWindow");
        }
        shareWindow.show();

        const baseUrl = window.location.href;
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const service_id = Ext.getCmp("serviceid").getSubmitValue();

        Ext.getCmp("shareurlfield").setValue(`${baseUrl}?dataset_id=${dataset_id}&service_id=${service_id}`);
    }
});
