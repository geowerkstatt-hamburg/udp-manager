Ext.define("UDPManager.view.datasets.datasetservices.layerconf.NewServiceLayerWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.newservicelayer",

    onAddNewLayer: function () {
        const selectedCollection = Ext.getCmp("newservicelayercollection").getSelection().data;
        const service_type = Ext.getCmp("servicetype").getValue();
        const layerName = !selectedCollection.alternative_name ? selectedCollection.name : selectedCollection.alternative_name;

        const rec = new UDPManager.model.UDPManagerModel({
            id: 0,
            name: layerName,
            title: selectedCollection.title,
            service_type: service_type,
            service_id: Ext.getCmp("serviceid").getValue(),
            collection_id: selectedCollection.id,
            service_title: Ext.getCmp("servicetitle").getValue(),
            group_layer_wms: selectedCollection.group_object
        });

        const count = Ext.getStore("LayersService").count();
        const namespace = selectedCollection.namespace;
        const reg_namespace = /(.*)=(.*)/g;

        if (!namespace && (service_type === "WFS" || service_type === "WFS-T")) {
            Ext.MessageBox.alert("Hinweis", "Layer konnte nicht angelegt werden. Das Feld Namespace muss gefüllt sein! <br><br> Anzugeben in der Schreibweise Prefix=URL.");
        }
        else if (!reg_namespace.test(namespace) && (service_type === "WFS" || service_type === "WFS-T")) {
            Ext.MessageBox.alert("Hinweis", "Layer konnte nicht angelegt werden. Schreibweise des Namespace ist ungültig! <br><br> Anzugeben in der Schreibweise Prefix=URL.");
        }
        else {
            const layer = Ext.getStore("LayersService").findRecord("name", layerName, 0, false, false, true);

            if (layer) {
                Ext.MessageBox.alert("Hinweis", "Ein Layer für diese Collection ist bereits vorhanden!");
            }
            else {
                Ext.getStore("LayersService").insert(count, rec);
                Ext.getCmp("grid_servicelayers").getSelectionModel().select(count);
                Ext.getCmp("newservicelayer-window").destroy();
            }
        }
    },

    onClose: function () {
        if (Ext.getStore("CollectionsDataset").getFilters().length > 0) {
            Ext.getStore("CollectionsDataset").removeFilter("groupFilter");
        }
    }
});
