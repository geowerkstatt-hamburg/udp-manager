Ext.define("UDPManager.view.datasets.datasetservices.layerconf.LayerConfController", {
    extend: "Ext.app.ViewController",

    alias: "controller.dataset_services_layerconf",

    onGridLayerItemSelected: function (sender, record) {
        const editMode = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditMode();

        this.getViewModel().set("servicelayerdetailsform", record);

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").updateServiceLayerJsonField(record.data.id);
        Ext.getCmp("servicelayerswitchcollectionqs").setDisabled(false);
        Ext.getCmp("servicelayerswitchcollectionprod").setDisabled(false);
        Ext.getCmp("servicelayerjsonspecificicon").setHtml("<div class='orangeYellowIconSmall'></div>");

        Ext.getStore("LayerPortals").load({
            params: {layer_id: record.data.id},
            callback: function (records) {
                if (records.length === 0) {
                    Ext.getCmp("deleteservicelayerbutton").setDisabled(!editMode);
                    Ext.getCmp("servicelayerdeleterequestbutton").setDisabled(true);
                }
                else {
                    Ext.getCmp("servicelayerdeleterequestbutton").setDisabled(window.read_only);
                    Ext.getCmp("deleteservicelayerbutton").setDisabled(true);
                }
            }
        });

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllLayerDetailsFieldsetsHidden();
        UDPManager.app.getController("UDPManager.controller.PublicFunctions").emptyLayerJsonField();

        if (record.data.service_type !== "WFS-T" && record.data.service_type !== "WMS-Time") {
            if ("servicelayer" + Ext.getCmp(record.data.service_type.toLowerCase() + "attrfieldset")) {
                Ext.getCmp("servicelayer" + record.data.service_type.toLowerCase() + "attrfieldset").setHidden(false);
            }
        }
        else if (record.data.service_type === "WFS-T") {
            Ext.getCmp("servicelayerwfsattrfieldset").setHidden(false);
        }
        else if (record.data.service_type === "WMS-Time") {
            Ext.getCmp("wmsattrfieldset").setHidden(false);
        }

        if (editMode) {
            Ext.getCmp("servicelayerjsonupdate").setDisabled(false);
            Ext.getCmp("servicelayerjsonupdateall").setDisabled(false);
        }

        if (record.data.delete_request) {
            Ext.getCmp("servicelayerdeleterequest").setHidden(false);
        }

        Ext.getCmp("servicelayerdetailsform").setDisabled(false);
    },

    onNewLayer: function () {
        let newLayerWindow = Ext.getCmp("newservicelayer-window");

        if (newLayerWindow) {
            newLayerWindow.destroy();
        }

        if (Ext.getCmp("servicetype").getValue() !== "WMS" && Ext.getCmp("servicetype").getValue() !== "WMS-Time") {
            const groupFilter = new Ext.util.Filter({
                id: "groupFilter",
                filterFn: function (item) {
                    return item.get("group_object") === false;
                }
            });

            Ext.getStore("CollectionsDataset").addFilter(groupFilter);
        }

        newLayerWindow = Ext.create("datasetservices.newservicelayer");

        newLayerWindow.show();
    },

    onGoToCollection: function () {
        Ext.getCmp("dataset_main_tabs").setActiveTab(1);

        const datasetCollectionsGrid = Ext.getCmp("grid_datasetcollections");
        const records = Ext.getStore("CollectionsDataset").getData().items;
        const collection_id = Ext.getCmp("servicelayercollectionid").getSubmitValue();

        for (let i = 0; i < records.length; i++) {
            if (records[i].data.id === collection_id) {
                datasetCollectionsGrid.fireEvent("cellclick", datasetCollectionsGrid, null, 0, records[i]);
                const row = datasetCollectionsGrid.store.indexOf(records[i]);

                datasetCollectionsGrid.getSelectionModel().select(row);
                datasetCollectionsGrid.ensureVisible(row);
                break;
            }
        }
    },

    onSaveLayerDetails: function (btn) {
        btn.setDisabled(true);

        const fieldvalues = Ext.getCmp("servicelayerdetailsform").getForm().getFieldValues();
        const collection_id = Ext.getCmp("servicelayercollectionid").getValue();
        const dataset_id = Ext.getCmp("datasetid").getValue();
        const service_id = Ext.getCmp("serviceid").getValue();
        const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);
        const collection = Ext.getStore("CollectionsDataset").findRecord("id", collection_id, 0, false, false, true);
        const st_attributes = {};

        if (!fieldvalues.limit_oaf || fieldvalues.limit_oaf === "" || fieldvalues.limit_oaf <= 0) {
            fieldvalues.limit_oaf = null;
        }

        if (service.data.type === "WFS-T") {
            service.data.type = "wfs";
        }
        if (service.data.type === "WMS-Time") {
            service.data.type = "wms";
        }

        for (const [key, value] of Object.entries(fieldvalues)) {
            if (key.indexOf("_" + service.data.type.toLowerCase()) > -1) {
                st_attributes[key.replace("_" + service.data.type.toLowerCase(), "")] = value;
            }
        }

        const post_data = {
            id: fieldvalues.id,
            collection_id: collection_id,
            dataset_id: dataset_id,
            service_id: service_id,
            last_edited_by: window.auth_user,
            service_type: service.data.type,
            service_security_type: service.data.security_type,
            service_title: service.data.title,
            service_url_int: service.data.url_int,
            service_url_sec: service.data.url_sec,
            collection_name: collection.data.name,
            collection_title: collection.data.title,
            collection_namespace: collection.data.namespace,
            st_attributes: JSON.stringify(st_attributes)
        };

        Ext.Ajax.request({
            url: "backend/savelayer",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: post_data,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Layer erfolgreich gespeichert!");

                    Ext.getStore("LayersCollection").load({
                        params: {collection_id: collection_id}
                    });

                    // load layers for selected service, when load finished fill theme layer tree
                    Ext.getStore("LayersService").load({
                        params: {service_id: service.data.id},
                        callback: function () {
                            if (service.data.type === "WMS" || service.data.type === "WMS-Time") {
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").fillLayerTreeStore();
                            }

                            const layer = Ext.getStore("LayersService").findRecord("id", response_json.id, 0, false, false, true);
                            const layer_grid = Ext.getCmp("grid_servicelayers");
                            const row_layer = layer_grid.store.indexOf(layer);

                            layer_grid.getSelectionModel().select(row_layer);
                            layer_grid.ensureVisible(row_layer);
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }

                btn.setDisabled(false);
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                btn.setDisabled(false);
            }
        });
    },

    onDeleteLayer: function () {
        Ext.getCmp("deleteservicelayerbutton").setDisabled(true);

        const mb = Ext.MessageBox;

        mb.buttonText.yes = "ja";
        mb.buttonText.no = "nein";

        mb.confirm("Löschen", "Wirklich löschen?", function (btn) {
            if (btn === "yes") {
                const collection_id = Ext.getCmp("servicelayercollectionid").getValue();
                const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
                const service_id = Ext.getCmp("serviceid").getValue();
                const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);
                const collection = Ext.getStore("CollectionsDataset").findRecord("id", collection_id, 0, false, false, true);
                const responsible_party_values = Ext.getCmp("datasetresponsibleparty").getSelection().data;
                const mail_from = "\"" + responsible_party_values.inbox_name + "\" <" + responsible_party_values.mail + ">";

                Ext.Ajax.request({
                    url: "backend/deletelayer",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        id: Ext.getCmp("servicelayerid").getSubmitValue(),
                        service_security_type: service.data.security_type,
                        service_title: service.data.title,
                        service_url_int: service.data.url_int,
                        collection_name: collection.data.name,
                        title: collection.data.title,
                        dataset_id: dataset_id,
                        last_edited_by: window.auth_user,
                        mail_from: mail_from
                    },
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);

                        if (response_json.status === "success") {
                            Ext.getStore("LayersCollection").load({
                                params: {collection_id: collection_id}
                            });

                            // load layers for selected service, when load finished fill theme layer tree
                            Ext.getStore("LayersService").load({
                                params: {service_id: service_id},
                                callback: function () {
                                    if (service.data.type === "WMS" || service.data.type === "WMS-Time") {
                                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").fillLayerTreeStore();
                                    }
                                }
                            });

                            Ext.getCmp("servicelayerdetailsform").getForm().reset();
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllServiceLayerDetailsFieldsetsHidden();
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").emptyServiceLayerJsonField();
                            Ext.getCmp("servicelayerdeleterequestbutton").setDisabled(true);
                        }
                        else {
                            Ext.MessageBox.alert("Fehler", response_json.message);
                        }
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Layer konnte nicht gelöscht werden");
                    }
                });
            }
            else {
                Ext.getCmp("deleteservicelayerbutton").setDisabled(false);
            }
        });
    },

    onLayerDeleteRequest: function () {
        Ext.getCmp("servicelayerdeleterequest").setHidden(false);
        Ext.getCmp("servicelayerdeleterequestcomment").setReadOnly(false);
    },

    onSaveLayerDeleteRequest: function () {
        const linked_portals = [];
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const selectedLayer = Ext.getCmp("grid_servicelayers").getSelectionModel().getSelection()[0].data;
        const responsible_party_values = Ext.getCmp("datasetresponsibleparty").getSelection().data;
        let mail_from = "";

        if (responsible_party_values.inbox_name_alt && responsible_party_values.mail_alt) {
            mail_from = "\"" + responsible_party_values.inbox_name_alt + "\" <" + responsible_party_values.mail_alt + ">";
        }
        else {
            mail_from = "\"" + responsible_party_values.inbox_name + "\" <" + responsible_party_values.mail + ">";
        }

        Ext.getStore("LayerPortals").getRange().forEach(function (record) {
            linked_portals.push(record.data);
        });

        Ext.Ajax.request({
            url: "backend/addlayerdeleterequest",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                id: Ext.getCmp("servicelayerid").getSubmitValue(),
                comment: Ext.getCmp("servicelayerdeleterequestcomment").getSubmitValue(),
                title: selectedLayer.title,
                service_id: selectedLayer.service_id,
                linked_portals: linked_portals,
                dataset_id: dataset_id,
                last_edited_by: window.auth_user,
                mail_from: mail_from
            },
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Löschvermerk eingetragen!");
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Löschvermerk konnte nicht gespeichert werden");
            }
        });
    },

    onDeleteLayerDeleteRequest: function () {
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();

        Ext.Ajax.request({
            url: "backend/deletelayerdeleterequest",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                id: Ext.getCmp("servicelayerid").getSubmitValue(),
                dataset_id: dataset_id,
                last_edited_by: window.auth_user
            },
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    Ext.getCmp("servicelayerdeleterequest").setHidden(true);

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Löschvermerk entfernt!");
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Löschvermerk konnte nicht gespeichert werden");
            }
        });
    },

    onRecreateJson: function (btn) {
        const dataset_status = Ext.getCmp("datasetstatus").getValue();
        let env = "dev";

        if (dataset_status === "veröffentlicht" || dataset_status === "veröffentlicht - vorveröffentlicht" || dataset_status === "vorveröffentlicht") {
            env = "prod";
        }

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").onRecreateJson(btn.id, env);
    },

    onDeletePortalLayerLink: function (grid, rowIndex) {
        const data = grid.getStore().getAt(rowIndex).data;
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const editMode = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditMode();

        if (data.update_mode === "manual") {
            Ext.getCmp("portallinks-grid").getSelectionModel().deselectAll();

            Ext.Ajax.request({
                url: "backend/deleteportallayerlink",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    layer_id: data.layer_id,
                    portal_id: data.id,
                    dataset_id: dataset_id,
                    last_edited_by: window.auth_user
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        grid.getStore().removeAt(rowIndex);

                        if (grid.getStore().count() === 0) {
                            Ext.getCmp("deleteservicelayerbutton").setDisabled(!editMode);
                            Ext.getCmp("servicelayerdeleterequestbutton").setDisabled(true);
                        }
                        else {
                            Ext.getCmp("servicelayerdeleterequestbutton").setDisabled(window.read_only);
                            Ext.getCmp("deleteservicelayerbutton").setDisabled(true);
                        }
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Link konnte nicht gelöscht werden");
                }
            });
        }
    },

    onSelectLayerJson: function (btn) {
        const layerId = Ext.getCmp("grid_servicelayers").getSelectionModel().getSelection()[0].id;
        let selectionEnv;

        if (btn.id === "servicelayerswitchcollectionqs") {
            selectionEnv = "stage";
            Ext.getCmp("servicelayerjsonspecificicon").setHtml("<div class='orangeYellowIconSmall'></div>");
        }
        else if (btn.id === "servicelayerswitchcollectionprod") {
            selectionEnv = "prod";
            Ext.getCmp("servicelayerjsonspecificicon").setHtml("<div class='x-fa fa-circle greenIconSmall'></div>");
        }

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").updateServiceLayerJsonField(layerId, selectionEnv);
    }
});
