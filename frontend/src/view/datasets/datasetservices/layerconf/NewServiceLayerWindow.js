Ext.define("UDPManager.view.datasets.datasetservices.layerconf.NewServiceLayerWindow", {
    extend: "Ext.window.Window",
    id: "newservicelayer-window",
    alias: "datasetservices.newservicelayer",
    height: 125,
    width: 400,
    title: "Neuen Layer Hinzufügen",
    resizable: false,
    closeAction: "hide",
    controller: "newservicelayer",
    listeners: {
        hide: "onClose",
        destroy: "onClose"
    },
    items: [
        {
            xtype: "panel",
            bodyPadding: 10,
            items: [
                {
                    xtype: "combo",
                    fieldLabel: "Collection wählen",
                    allowBlank: false,
                    id: "newservicelayercollection",
                    store: "CollectionsDataset",
                    queryMode: "local",
                    displayField: "title",
                    labelWidth: 120,
                    width: 380
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    "->",
                    {
                        id: "addnewservicelayermanually",
                        text: "Hinzufügen",
                        listeners: {
                            click: "onAddNewLayer"
                        }
                    }
                ]
            }]
        }
    ]
});
