Ext.define("UDPManager.view.datasets.datasetservices.layerconf.LayerConf", {
    extend: "Ext.panel.Panel",
    xtype: "dataset_services_layerconf",
    alias: "widget.dataset_services_layerconf",
    controller: "dataset_services_layerconf",
    layout: "vbox",
    listeners: {
        select: "onItemSelected"
    },
    viewModel: {
        data: {
            servicelayerdetailsform: null
        }
    },

    initComponent: function () {
        this.items = [
            {
                xtype: "panel",
                layout: "column",
                width: "100%",
                title: "Layerkonfiguration",
                header: false,
                items: [
                    {
                        xtype: "gridpanel",
                        id: "grid_servicelayers",
                        bind: "{gridstore_servicelayers}",
                        reference: "grid_servicelayers",
                        style: "border-left: 1px solid #cfcfcf !important;border-right: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                        rowLines: false,
                        columns: [
                            {text: "ID", dataIndex: "id", width: 70},
                            {text: "Referenzcollection", dataIndex: "title", flex: 1}
                        ],
                        viewConfig: {
                            //stripeRows: true,
                            getRowClass: function () {
                                return "multiline-row";
                            }
                        },
                        columnWidth: 0.3,
                        listeners: {
                            select: "onGridLayerItemSelected"
                        },
                        height: Ext.Element.getViewportHeight() - 108,
                        autoScroll: true,
                        buttonAlign: "left",
                        dockedItems: [{
                            xtype: "toolbar",
                            dock: "bottom",
                            items: [
                                {
                                    id: "newservicelayerbutton",
                                    iconCls: "x-fa fa-plus",
                                    tooltip: "Neuen Layer anlegen",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    disabled: window.read_only,
                                    listeners: {
                                        click: "onNewLayer"
                                    }
                                },
                                {
                                    xtype: "button",
                                    iconCls: "x-fa fa-trash",
                                    id: "deleteservicelayerbutton",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    disabled: true,
                                    tooltip: "Ausgewählten Layer löschen",
                                    listeners: {
                                        click: "onDeleteLayer"
                                    }
                                }
                            ]
                        }]
                    },
                    {
                        xtype: "tabpanel",
                        id: "servicelayers_main_tabs",
                        activeTab: 0,
                        tabPosition: "left",
                        tabRotation: 0,
                        columnWidth: 0.699,
                        items: [
                            {
                                title: "Layer-Details",
                                xtype: "form",
                                id: "servicelayerdetailsform",
                                defaultType: "textfield",
                                style: "border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                height: Ext.Element.getViewportHeight() - 108,
                                autoScroll: true,
                                bodyPadding: 20,
                                scrollable: true,
                                disabled: true,
                                items: [
                                    {
                                        xtype: "component",
                                        margin: "0 0 10 165",
                                        id: "servicelayereditnotes",
                                        html: "",
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "Layer-Attribute können auch ohne Bearbeitungsmodus aktualisiert werden"
                                        }
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "LÖSCHVERMERK",
                                        id: "servicelayerdeleterequest",
                                        hidden: true,
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "textareafield",
                                                id: "servicelayerdeleterequestcomment",
                                                labelWidth: 0,
                                                width: "100%",
                                                height: 100,
                                                grow: true,
                                                bind: "{servicelayerdetailsform.delete_request_comment}"
                                            },
                                            {
                                                xtype: "fieldcontainer",
                                                layout: "hbox",
                                                width: "100%",
                                                items: [
                                                    {
                                                        xtype: "button",
                                                        id: "saveservicelayerdeleterequestbutton",
                                                        text: "Speichern",
                                                        width: "120px",
                                                        margin: "10 25 25 0",
                                                        listeners: {
                                                            click: "onSaveLayerDeleteRequest"
                                                        }
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "deleteservicelayerdeleterequestbutton",
                                                        text: "Entfernen",
                                                        width: "120px",
                                                        margin: "10 25 25 0",
                                                        listeners: {
                                                            click: "onDeleteLayerDeleteRequest"
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "numberfield",
                                        fieldLabel: "Layer-ID",
                                        id: "servicelayerid",
                                        name: "id",
                                        readOnly: true,
                                        hidden: true,
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "ID des Layers"
                                        },
                                        labelWidth: 160,
                                        width: 360,
                                        bind: "{servicelayerdetailsform.id}"
                                    },
                                    {
                                        fieldLabel: "Name",
                                        id: "servicelayername",
                                        name: "name",
                                        readOnly: true,
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "Name des Layers"
                                        },
                                        labelWidth: 160,
                                        width: "100%",
                                        bind: "{servicelayerdetailsform.name}"
                                    },
                                    {
                                        xtype: "fieldcontainer",
                                        layout: "hbox",
                                        items: [
                                            {
                                                xtype: "textfield",
                                                id: "servicelayercollectiontitle",
                                                name: "title",
                                                labelWidth: 160,
                                                fieldLabel: "Collection",
                                                flex: 1,
                                                readOnly: true,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Titel der referenzierten Collection"
                                                },
                                                bind: "{servicelayerdetailsform.title}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                id: "servicelayercollectionid",
                                                name: "collection_id",
                                                hidden: true,
                                                bind: "{servicelayerdetailsform.collection_id}"
                                            },
                                            {
                                                xtype: "button",
                                                id: "servicelayergoToCollection",
                                                iconCls: "x-fa fa-share",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Zur Collection springen"
                                                },
                                                listeners: {
                                                    click: "onGoToCollection"
                                                },
                                                width: 50,
                                                margin: "0 0 0 20"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "WMS Parameter",
                                        id: "servicelayerwmsattrfieldset",
                                        hidden: true,
                                        defaultType: "textfield",
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "checkboxfield",
                                                id: "servicelayergrouplayer",
                                                name: "group_layer_wms",
                                                fieldLabel: "Gruppenlayer",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.group_layer_wms}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                fieldLabel: "Gutter (Px)",
                                                id: "servicelayergutter",
                                                name: "gutter_wms",
                                                minValue: 0,
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.gutter_wms}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                id: "servicelayertilesize",
                                                name: "tile_size_wms",
                                                fieldLabel: "Kachelgröße (Px)",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.tile_size_wms}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "servicelayersingletile",
                                                name: "single_tile_wms",
                                                fieldLabel: "Singletile",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.single_tile_wms}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "servicelayertransparent",
                                                name: "transparent_wms",
                                                fieldLabel: "Transparent",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.transparent_wms}"
                                            },
                                            {
                                                xtype: "combo",
                                                fieldLabel: "Ausgabeformat",
                                                store: ["image/png", "image/jpeg"],
                                                name: "output_format_wms",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Standardrückgabeformat bei WMS getMap Requests"
                                                },
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.output_format_wms}"
                                            },
                                            {
                                                xtype: "combo",
                                                fieldLabel: "GFI Format",
                                                store: ["", "text/html"],
                                                name: "gfi_format_wms",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Wenn das Feld nicht ausgefüllt wird, wird text/xml verwendet"
                                                },
                                                labelWidth: 150,
                                                bind: "{servicelayerdetailsform.gfi_format_wms}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                fieldLabel: "Feature Count",
                                                name: "feature_count_wms",
                                                labelWidth: 150,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Anzahl der maximal abfragbaren Feature bei einem Klick."
                                                },
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.feature_count_wms}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                name: "notsupportedin3d_wms",
                                                fieldLabel: "Nicht für 3D Anwendungen",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Häkchen setzen, wenn Layer nicht in 3D Anwendungen benutzbar ist."
                                                },
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.notsupportedin3d_wms}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                name: "time_series_wms",
                                                fieldLabel: "Zeitreihe (WMS-Time)",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Häkchen setzen, wenn der Layer eine zeitliche Komponente für eine Zeitreihe (WMS-Time) enthält."
                                                },
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.time_series_wms}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "WMTS Parameter",
                                        id: "servicelayerwmtsattrfieldset",
                                        hidden: true,
                                        defaultType: "textfield",
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "WFS Parameter",
                                        id: "servicelayerwfsattrfieldset",
                                        hidden: true,
                                        defaultType: "textfield",
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "combo",
                                                fieldLabel: "Ausgabeformat",
                                                store: ["XML"],
                                                name: "output_format_wfs",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Standardrückgabeformat bei WFS getFeature Requests"
                                                },
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.output_format_wfs}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "OAF Parameter",
                                        id: "servicelayeroafattrfieldset",
                                        hidden: true,
                                        defaultType: "textfield",
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "numberfield",
                                                fieldLabel: "Limit",
                                                id: "servicelayeroaflimit",
                                                name: "limit_oaf",
                                                labelWidth: 150,
                                                bind: "{servicelayerdetailsform.limit_oaf}",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Limitiert die Anzahl der zurückgegebenen Elemente. Nur positive Ganzzahl gültig!"
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "Terrain3D Einstellungen",
                                        id: "servicelayerterrain3dattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "checkboxfield",
                                                id: "servicelayerrequestvertexnormals",
                                                name: "request_vertex_normals_terrain3d",
                                                fieldLabel: "requestVertexNormals",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.request_vertex_normals_terrain3d}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "TileSet3D Einstellungen",
                                        id: "servicelayertileset3dattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "numberfield",
                                                fieldLabel: "maxScreenSpaceError",
                                                id: "servicelayermaximumscreenspaceerror",
                                                name: "maximum_screen_space_error_tileset3d",
                                                minValue: 0,
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.maximum_screen_space_error_tileset3d}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "Oblique Einstellungen",
                                        id: "servicelayerobliqueattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "numberfield",
                                                id: "servicelayerhidelevels",
                                                name: "hidelevels_oblique",
                                                fieldLabel: "hideLevels",
                                                minValue: 0,
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.hidelevels_oblique}"
                                            }, {
                                                xtype: "numberfield",
                                                id: "servicelayerminzoom",
                                                name: "minzoom_oblique",
                                                minValue: 0,
                                                fieldLabel: "minZoom",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.minzoom_oblique}"
                                            }, {
                                                xtype: "numberfield",
                                                id: "servicelayerresolution",
                                                name: "resolution_oblique",
                                                fieldLabel: "resolution",
                                                minValue: 0,
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.resolution_oblique}"
                                            }, {
                                                xtype: "textfield",
                                                id: "servicelayerprojection",
                                                name: "projection_oblique",
                                                fieldLabel: "projection",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.projection_oblique}"
                                            }, {
                                                xtype: "textfield",
                                                id: "servicelayerterrainurl",
                                                name: "terrainurl_oblique",
                                                fieldLabel: "terrainUrl",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.terrainurl_oblique}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "STA Einstellungen",
                                        id: "servicelayerstaattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "combo",
                                                fieldLabel: "URL Param. - Root",
                                                store: ["Datastreams", "Things"],
                                                id: "servicelayerURLParamsRootEl",
                                                name: "rootel_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.rootel_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "URL Param. - Filter",
                                                id: "servicelayerURLParamsFilter",
                                                name: "filter_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.filter_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "URL Param. - Expand",
                                                id: "servicelayerURLParamsExpand",
                                                name: "expand_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.expand_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "EPSG",
                                                name: "epsg_sta",
                                                id: "servicelayerEPSG",
                                                labelWidth: 150,
                                                width: "100%",
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.epsg_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "Zu ersetzende LayerIDs",
                                                id: "servicelayerRelatedWMSLayerIDs",
                                                name: "related_wms_layers_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.related_wms_layers_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "Style ID",
                                                id: "servicelayerStyleID",
                                                name: "style_id_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.style_id_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                id: "servicelayerClusterDistance",
                                                name: "cluster_distance_sta",
                                                fieldLabel: "ClusterDistance (Px)",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.cluster_distance_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                id: "servicelayerMouseHoverField",
                                                name: "mouse_hover_field_sta",
                                                fieldLabel: "MouseHoverField",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.mouse_hover_field_sta}"
                                            },
                                            {
                                                xtype: "textareafield",
                                                id: "servicelayerStaTestUrl",
                                                name: "sta_test_url_sta",
                                                fieldLabel: "Test URL",
                                                labelWidth: 150,
                                                width: "100%",
                                                height: 100,
                                                grow: true,
                                                bind: "{servicelayerdetailsform.sta_test_url_sta}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "servicelayerLoadThingsOnlyInCurrentExtent",
                                                name: "load_things_only_in_current_extent_sta",
                                                fieldLabel: "Nur Objeckte in aktuellem Ausschnitt laden",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{servicelayerdetailsform.load_things_only_in_current_extent_sta}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "Vector Tiles Einstellungen",
                                        id: "servicelayervectortilesattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "Extent",
                                                id: "servicelayerextent",
                                                name: "extent_vectortiles",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.extent_vectortiles}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "Origin",
                                                id: "servicelayerorigin",
                                                name: "origin_vectortiles",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{servicelayerdetailsform.origin_vectortiles}"
                                            },
                                            {
                                                xtype: "textareafield",
                                                fieldLabel: "Resolutions",
                                                id: "servicelayerresolutions",
                                                name: "resolutions_vectortiles",
                                                labelWidth: 150,
                                                width: "100%",
                                                height: 100,
                                                grow: true,
                                                bind: "{servicelayerdetailsform.resolutions_vectortiles}"
                                            },
                                            {
                                                xtype: "textareafield",
                                                fieldLabel: "VT Styles",
                                                id: "servicelayervtstyles",
                                                name: "vtstyles_vectortiles",
                                                labelWidth: 150,
                                                width: "100%",
                                                height: 100,
                                                grow: true,
                                                bind: "{servicelayerdetailsform.vtstyles_vectortiles}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                fieldLabel: "Visibility",
                                                id: "servicelayervisibility",
                                                name: "visibility_vectortiles",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                bind: "{servicelayerdetailsform.visibility_vectortiles}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldcontainer",
                                        layout: "hbox",
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "button",
                                                id: "servicelayerdeleterequestbutton",
                                                text: "Löschvermerk",
                                                width: 120,
                                                margin: "0 25 10 0",
                                                disabled: true,
                                                listeners: {
                                                    click: "onLayerDeleteRequest"
                                                }
                                            }
                                        ]
                                    }
                                ],
                                dockedItems: [{
                                    xtype: "toolbar",
                                    dock: "bottom",
                                    border: true,
                                    style: "border-top: 1px solid #cfcfcf !important;",
                                    items: [
                                        {
                                            xtype: "component",
                                            html: "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Pflichtfeld</span>",
                                            hidden: true
                                        },
                                        {
                                            xtype: "component",
                                            html: "<span style='color:grey;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Standardwert</span>"
                                        },
                                        {
                                            xtype: "component",
                                            flex: 1,
                                            html: ""
                                        },
                                        {
                                            id: "saveservicelayerdetailsbutton",
                                            text: "Speichern",
                                            scale: "medium",
                                            cls: "btn-save",
                                            disabled: true,
                                            listeners: {
                                                click: "onSaveLayerDetails"
                                            }
                                        }
                                    ]
                                }]
                            },
                            {
                                title: "Portale",
                                bodyPadding: "0 0 0 0",
                                width: "100%",
                                items: [
                                    {
                                        xtype: "gridpanel",
                                        store: "LayerPortals",
                                        title: "gekoppelte Portale",
                                        id: "servicelayerportallinks-grid",
                                        autoScroll: true,
                                        height: Ext.Element.getViewportHeight() - 149,
                                        style: "border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                        width: "100%",
                                        viewConfig: {
                                            enableTextSelection: true
                                        },
                                        columnLines: true,
                                        selModel: "rowmodel",
                                        features: [{
                                            ftype: "grouping",
                                            startCollapsed: true,
                                            groupHeaderTpl: '{columnName}: {name} ({rows.length} Portal{[values.rows.length > 1 ? "e" : ""]})'
                                        }],
                                        columns: [
                                            {
                                                text: "Titel",
                                                dataIndex: "title",
                                                align: "left",
                                                flex: 1
                                            },
                                            {
                                                text: "URI",
                                                dataIndex: "url",
                                                align: "left",
                                                flex: 1
                                            },
                                            {
                                                text: "Host",
                                                dataIndex: "host",
                                                align: "left",
                                                hidden: true,
                                                flex: 1
                                            },
                                            {
                                                text: "Link",
                                                dataIndex: "url",
                                                align: "left",
                                                width: 70,
                                                renderer: function (value, metaData, record) {
                                                    if (value !== "") {
                                                        return "<a href=\"" + record.data.host + value + "\" target=\"_blank\">Link</a>";
                                                    }
                                                    else {
                                                        return "";
                                                    }
                                                }
                                            },
                                            {
                                                xtype: "actioncolumn",
                                                header: "Löschen",
                                                name: "delete",
                                                align: "center",
                                                sortable: false,
                                                menuDisabled: true,
                                                width: 80,
                                                renderer: function (value, metadata, record) {
                                                    if (record.get("update_mode") === "manual" && !window.read_only) {
                                                        this.items[0].iconCls = "x-fa fa-times";
                                                        this.items[0].tooltip = "Verlinkung löschen";
                                                        this.items[0].handler = "onDeletePortalLayerLink";
                                                    }
                                                    else {
                                                        this.items[0].iconCls = "";
                                                        this.items[0].tooltip = "Keine Funktion verfügbar";
                                                    }
                                                },
                                                items: [
                                                    {
                                                        iconCls: "x-fa fa-times",
                                                        tooltip: "Verlinkung löschen",
                                                        handler: "onDeletePortalLayerLink"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title: "JSON",
                                bodyPadding: "5 5 5 5",
                                height: Ext.Element.getViewportHeight() - 149,
                                style: "border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                items: [
                                    {
                                        xtype: "fieldcontainer",
                                        layout: "hbox",
                                        items: [
                                            {
                                                xtype: "button",
                                                id: "servicelayerjsonupdate",
                                                text: "JSON neu erzeugen",
                                                margin: "15 10 10 15",
                                                width: 230,
                                                disabled: true,
                                                listeners: {
                                                    click: "onRecreateJson"
                                                }
                                            },
                                            {
                                                xtype: "button",
                                                id: "servicelayerjsonupdateall",
                                                text: "JSON neu erzeugen (ganze Collection)",
                                                margin: "15 15 10 10",
                                                width: 230,
                                                disabled: true,
                                                listeners: {
                                                    click: "onRecreateJson"
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldcontainer",
                                        margin: "0 0 10 15",
                                        items: [{
                                            xtype: "segmentedbutton",
                                            items: [
                                                {
                                                    id: "servicelayerswitchcollectionqs",
                                                    text: "dev / stage",
                                                    scale: "medium",
                                                    disabled: true,
                                                    cls: "btn-api-stage-gradient",
                                                    listeners: {
                                                        click: "onSelectLayerJson"
                                                    }
                                                },
                                                {
                                                    id: "servicelayerswitchcollectionprod",
                                                    text: "prod",
                                                    scale: "medium",
                                                    disabled: true,
                                                    cls: "btn-api-prod",
                                                    listeners: {
                                                        click: "onSelectLayerJson"
                                                    }
                                                }
                                            ]
                                        }]
                                    },
                                    {
                                        xtype: "fieldset",
                                        id: "servicelayerjsonspecificfieldset",
                                        padding: "5 5 5 15",
                                        cls: "prio-fieldset",
                                        collapsible: false,
                                        collapsed: false,
                                        border: false,
                                        items: [
                                            {
                                                xtype: "component",
                                                id: "servicelayerjsonspecificicon",
                                                html: "<div class='orangeYellowIconSmall'></div>"
                                            },
                                            {
                                                xtype: "textareafield",
                                                id: "servicelayer_json_field",
                                                labelWidth: 0,
                                                height: Ext.Element.getViewportHeight() - 279,
                                                width: "100%",
                                                autoscroll: true
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }
});
