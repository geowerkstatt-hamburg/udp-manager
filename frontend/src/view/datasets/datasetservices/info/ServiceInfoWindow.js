Ext.define("UDPManager.view.datasets.datasetservices.infoServiceInfoWindow", {
    extend: "Ext.window.Window",
    xtype: "serviceinfowindow",
    id: "serviceinfo-window",
    alias: "datasetservices.ServiceInfoWindow",
    height: 340,
    width: 600,
    title: "Schnittstelle Aufrufen",
    bodyStyle: "margin: 10px;",
    controller: "ServiceInfoWindowController",
    layout: "vbox",
    prod: false,
    initComponent: function () {
        const srs = Ext.getCmp("datasetsrs").getSubmitValue() ? Ext.getCmp("datasetsrs").getSubmitValue() : UDPManager.Configs.getDefaultDatasetSrs();

        this.items = [
            {
                xtype: "component",
                html: "Aufruf von Capabilities bzw. Landing Pages"
            },
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                width: "100%",
                items: [
                    {
                        xtype: "button",
                        text: "intern URL",
                        margin: "0px 10px 10px 0px",
                        handler: "onUrlIntCall"
                    },
                    {
                        xtype: "button",
                        text: "externe URL",
                        margin: "0px 10px 10px 0px",
                        handler: "onUrlExtCall"
                    },
                    {
                        xtype: "button",
                        text: "Vorschauportal",
                        margin: "0px 10px 10px 0px",
                        handler: "onPreviewPortalCall"
                    }
                ]
            },
            {
                xtype: "component",
                html: "Aufruf von getMap / getFeature Requests, sowie Vorschauportal"
            },
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                width: "100%",
                items: [
                    {
                        xtype: "splitbutton",
                        id: "servicetestrequestbutton",
                        width: 120,
                        margin: "5 10 10 0",
                        text: "Testrequest",
                        disabled: true,
                        menu: []
                    },
                    {
                        xtype: "component",
                        id: "servicetestrequestinfo",
                        margin: "10 0 0 0",
                        html: ""
                    }
                ]
            },
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                width: "100%",
                items: [
                    {
                        xtype: "splitbutton",
                        id: "servicetestcollectionbutton",
                        width: 120,
                        margin: "5 10 10 0",
                        text: "Collection",
                        disabled: true,
                        menu: []
                    },
                    {
                        xtype: "component",
                        id: "servicetestcollectioninfo",
                        margin: "10 0 0 0",
                        html: ""
                    }
                ]
            },
            {
                xtype: "fieldcontainer",
                id: "getmaprequestconfcontainer",
                layout: "vbox",
                disabled: true,
                items: [
                    {
                        xtype: "combo",
                        fieldLabel: "Bounding Box",
                        store: UDPManager.Configs.getTestBboxBySrs(srs),
                        displayField: "name",
                        allowBlank: false,
                        id: "servicegetmapbbox",
                        valueField: "bbox",
                        editable: false,
                        margin: "0 10 10 0",
                        labelWidth: 100,
                        width: 300,
                        listeners: {
                            change: "onChangeGetMapBbox"
                        }
                    },
                    {
                        xtype: "combo",
                        fieldLabel: "Bildgröße",
                        store: ["256", "512", "1024"],
                        value: "256",
                        allowBlank: false,
                        id: "servicegetmapsize",
                        margin: "0 10 10 0",
                        labelWidth: 100,
                        width: 300,
                        listeners: {
                            change: "onChangeGetMapBbox"
                        }
                    }
                ]
            },
            {
                xtype: "fieldcontainer",
                id: "testurlfieldcontainer",
                layout: "hbox",
                disabled: true,
                items: [
                    {
                        xtype: "textfield",
                        id: "testrequesttype",
                        hidden: true
                    },
                    {
                        xtype: "textfield",
                        id: "testrequestcollection",
                        hidden: true
                    },
                    {
                        xtype: "textfield",
                        id: "apitestrequesturl",
                        width: 480,
                        readOnly: true,
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Test URL"
                        }
                    },
                    {
                        xtype: "button",
                        id: "calltesturlbutton",
                        iconCls: "x-fa fa-share",
                        width: 40,
                        margin: "0 10 0 10",
                        disabled: true,
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Test URL aufrufen"
                        },
                        handler: "onCallTestRequest"
                    },
                    {
                        xtype: "button",
                        id: "copytesturlbutton",
                        iconCls: "x-fa fa-clipboard",
                        width: 40,
                        margin: "0 10 0 0",
                        disabled: true,
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Test URL in Zwischenablage kopieren"
                        },
                        handler: "onCopyUrlToClipboard"
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }
});
