Ext.define("UDPManager.view.datasets.datasetservices.info.ServiceInfoWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.ServiceInfoWindowController",

    onChangeGetMapBbox: function () {
        const req = Ext.getCmp("testrequesttype").getSubmitValue();
        let layers = Ext.getCmp("testrequestcollection").getSubmitValue();
        const bbox = Ext.getCmp("servicegetmapbbox").getSubmitValue();
        const size = Ext.getCmp("servicegetmapsize").getSubmitValue();
        const service = this.getViewModel().data.service;
        const srs = Ext.getCmp("datasetsrs").getSubmitValue() ? Ext.getCmp("datasetsrs").getSubmitValue() : UDPManager.Configs.getDefaultDatasetSrs();

        if (bbox && size && req) {
            if (layers === "alle Layer") {
                const layersArray = [];

                Ext.getStore("TestLayersService").each(function (record) {
                    if (record.data.alternative_name) {
                        layersArray.push(record.data.alternative_name);
                    }
                    else {
                        layersArray.push(record.data.name);
                    }
                });

                layers = layersArray.toString();
            }

            let proj = "CRS";

            if (service.version === "1.1.1") {
                proj = "SRS";
            }

            let url_int = service.url_int;
            let url_ext = service.url_ext;

            if (Ext.getCmp("serviceinfo-window").prod) {
                url_int = service.url_int_prod;
                url_ext = service.url_ext_prod;
            }

            let url = "";

            if (req === "getMap - intern") {
                url = url_int + "?SERVICE=WMS&VERSION=" + service.version + "&REQUEST=GetMap&FORMAT=image/png&BBOX=" + bbox + "&WIDTH=" + size + "&HEIGHT=" + size + "&STYLES=&" + proj + "=" + srs + "&LAYERS=" + layers;
            }
            else if (req === "getMap - extern") {
                url = url_ext + "?SERVICE=WMS&VERSION=" + service.version + "&REQUEST=GetMap&FORMAT=image/png&BBOX=" + bbox + "&WIDTH=" + size + "&HEIGHT=" + size + "&STYLES=&" + proj + "=" + srs + "&LAYERS=" + layers;
            }
            else if (req === "getLegendGraphic - intern") {
                url = url_int + "?service=WMS&request=GetLegendGraphic&version=" + service.version + "&format=image/png&layer=" + layers;
            }
            else if (req === "getLegendGraphic - extern") {
                url = url_ext + "?service=WMS&request=GetLegendGraphic&version=" + service.version + "&format=image/png&layer=" + layers;
            }

            const getMapRequestWindow = Ext.getCmp("serviceinfo-window");

            getMapRequestWindow.service_int_ext_url = url;

            if (Ext.getCmp("servicegetmapbbox").getValue()) {
                Ext.getCmp("apitestrequesturl").setValue(url);
                Ext.getCmp("calltesturlbutton").enable();
                Ext.getCmp("copytesturlbutton").enable();
            }
            else {
                Ext.getCmp("calltesturlbutton").disable();
                Ext.getCmp("copytesturlbutton").disable();
            }
        }
    },

    onRequestgetMapBbox: function () {
        const getMapRequestWindow = Ext.getCmp("serviceinfo-window");

        window.open(getMapRequestWindow.service_int_ext_url, "_blank");
    },

    onCancelgetMapBbox: function () {
        Ext.getCmp("serviceinfo-window").close();
        return false;
    },

    onCallTestRequest: function () {
        const testRequest = Ext.getCmp("apitestrequesturl").getValue();

        window.open(testRequest, "_blank");
    },

    onPreviewPortalCall: function () {
        this._openPreviewPortal("alle Layer");
    },

    onUrlIntCall: function () {
        const data = this.getViewModel().data.service;

        if (Ext.getCmp("serviceinfo-window").prod) {
            this._onUrlCall(data.url_int_prod, data);
        }
        else {
            this._onUrlCall(data.url_int, data);
        }
    },

    onUrlExtCall: function () {
        const data = this.getViewModel().data.service;

        if (Ext.getCmp("serviceinfo-window").prod) {
            this._onUrlCall(data.url_ext_prod, data);
        }
        else {
            this._onUrlCall(data.url_ext, data);
        }
    },

    _onUrlCall: function (base_url, data) {
        const version = data.version;
        let service_type = data.type;

        if (data.type === "WFS-T") {
            service_type = "WFS";
        }

        if (data.type === "WMS-Time") {
            service_type = "WMS";
        }

        if (service_type === "WMS" || service_type === "WFS" || service_type === "WMTS" || service_type === "WPS") {
            const url = base_url + "?Service=" + service_type + "&Version=" + version + "&Request=GetCapabilities";

            window.open(url, "_blank");
        }
        else if (service_type === "STA") {
            const url = base_url.replace(/\/$/, "") + "/v" + version;

            window.open(url, "_blank");
        }
        else if (service_type === "OAF") {
            window.open(base_url, "_blank");
        }
    },

    onSetTestRequest: function (btn) {
        Ext.getCmp("getmaprequestconfcontainer").setDisabled(true);
        Ext.getCmp("testurlfieldcontainer").setDisabled(true);

        Ext.getCmp("testrequesttype").setValue(btn.text);
        Ext.getCmp("servicetestrequestinfo").setHtml(btn.text);

        const data = this.getViewModel().data.service;
        const menuItems = [];

        if (!btn.text.includes("getLegendGraphic")) {
            menuItems.push({
                text: "alle Layer",
                layerId: 0,
                listeners: {
                    click: "onSetTestCollection"
                }
            });
        }

        Ext.getStore("TestLayersService").each(function (record) {
            let layer = "";

            if (record.data.alternative_name) {
                if (data.type === "WFS" || data.type === "WFS-T") {
                    layer = record.data.namespace.indexOf("=") > 0 ? record.data.namespace.split("=")[0] + ":" + record.data.alternative_name : record.data.alternative_name;
                }
                else {
                    layer = record.data.alternative_name;
                }
            }
            else {
                if (data.type === "WFS" || data.type === "WFS-T") {
                    layer = record.data.namespace.indexOf("=") > 0 ? record.data.namespace.split("=")[0] + ":" + record.data.name : record.data.name;
                }
                else {
                    layer = record.data.name;
                }
            }

            menuItems.push({
                text: layer,
                layerId: record.data.id,
                listeners: {
                    click: "onSetTestCollection"
                }
            });
        });

        Ext.getCmp("servicetestcollectionbutton").setMenu(
            new Ext.menu.Menu({
                items: menuItems
            })
        );

        Ext.getCmp("servicetestcollectionbutton").setDisabled(false);
    },

    onSetTestCollection: function (btn) {
        const req = Ext.getCmp("testrequesttype").getSubmitValue();
        const data = this.getViewModel().data.service;
        const layerId = btn.layerId;
        let layers = btn.text;
        let count = 1;

        Ext.getCmp("testrequestcollection").setValue(btn.text);
        Ext.getCmp("servicetestcollectioninfo").setHtml(btn.text);

        if (req === "Vorschauportal") {
            this._openPreviewPortal(layers, layerId);
        }
        else {
            if (data.type === "WMS" || data.type === "WMS-Time") {
                const srs = Ext.getCmp("datasetsrs").getSubmitValue() ? Ext.getCmp("datasetsrs").getSubmitValue() : UDPManager.Configs.getDefaultDatasetSrs();

                Ext.getCmp("servicegetmapbbox").setStore(UDPManager.Configs.getTestBboxBySrs(srs));

                Ext.getCmp("getmaprequestconfcontainer").setDisabled(false);
                Ext.getCmp("testurlfieldcontainer").setDisabled(false);

                this.onChangeGetMapBbox();
            }
            else if (data.type === "WFS" || data.type === "WFS-T") {
                if (layers === "alle Layer") {
                    const layersArray = [];

                    Ext.getStore("TestLayersService").each(function (record) {
                        let layer = "";

                        if (record.data.alternative_name) {
                            layer = record.data.namespace.indexOf("=") > 0 ? record.data.namespace.split("=")[0] + ":" + record.data.alternative_name : record.data.alternative_name;
                        }
                        else {
                            layer = record.data.namespace.indexOf("=") > 0 ? record.data.namespace.split("=")[0] + ":" + record.data.name : record.data.name;
                        }

                        layersArray.push(layer);
                    });

                    layers = layersArray.toString();
                    count = Ext.getStore("TestLayersService").count();
                }

                let url_int = data.url_int;
                let url_ext = data.url_ext;

                if (Ext.getCmp("serviceinfo-window").prod) {
                    url_int = data.url_int_prod;
                    url_ext = data.url_ext_prod;
                }

                const version = data.version;
                let url = "";
                let maxFeatures = "maxFeatures";

                if (version === "2.0.0") {
                    maxFeatures = "count";
                }

                if (req === "getFeature - intern") {
                    url = url_int + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=GetFeature&typename=" + layers.toString() + "&" + maxFeatures + "=1" + count;
                }
                else if (req === "getFeature - extern") {
                    url = url_ext + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=GetFeature&typename=" + layers.toString() + "&" + maxFeatures + "=" + count;
                }
                else if (req === "describeFeatureType - intern") {
                    url = url_int + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=DescribeFeatureType&typename=" + layers.toString();
                }
                else if (req === "describeFeatureType - extern") {
                    url = url_ext + "?SERVICE=WFS&VERSION=" + version + "&REQUEST=DescribeFeatureType&typename=" + layers.toString();
                }

                if (url) {
                    Ext.getCmp("apitestrequesturl").setValue(url);
                    Ext.getCmp("calltesturlbutton").enable();
                    Ext.getCmp("copytesturlbutton").enable();
                }
                else {
                    Ext.getCmp("calltesturlbutton").disable();
                    Ext.getCmp("copytesturlbutton").disable();
                }

                Ext.getCmp("getmaprequestconfcontainer").setDisabled(true);
                Ext.getCmp("testurlfieldcontainer").setDisabled(false);
            }
        }
    },

    _openPreviewPortal: function (layers, layerId) {
        const MB = Ext.MessageBox;
        let titleList = [];

        if (layers === "alle Layer") {
            Ext.getStore("TestLayersService").each(function (record) {
                titleList.push(record.data.title.toUpperCase().replace("Ö", "O").replace("Ü", "U").replace("Ä", "A") + "*#*" + record.data.id + "*#*");
            });

            titleList = titleList.sort().reverse().toString().split("*#*");

            for (let j = 0; j < titleList.length; j++) {
                titleList.splice(j + 0, 1);
            }
        }
        else {
            titleList.push(layerId);
        }

        let mapPreview = UDPManager.Configs.getMapPreview();

        if (Ext.getCmp("serviceinfo-window").prod) {
            mapPreview = UDPManager.Configs.getMapPreviewProd();
        }

        const previewURL = mapPreview + "?Map/layerids=" + UDPManager.Configs.getMapPreviewBaseLayerId() + "," + titleList.toString();

        window.open(previewURL, "_blank");

        // deactivate komplex view
        // MB.buttonText.yes = "Einfach";
        // MB.buttonText.no = "Komplex";

        // MB.confirm("Vorschauportal", "Einfachen oder komplexen Themenbaum aufrufen?", function (mbbtn) {
        //     if (mbbtn === "yes") {
        //         const previewURL = mapPreview + "?Map/layerids=" + UDPManager.Configs.getMapPreviewBaseLayerId() + "," + titleList.toString() + "&transparency=" + transparencyList.substring(0, transparencyList.length - 1);

        //         window.open(previewURL, "_blank");
        //     }
        //     else if (mbbtn === "no") {
        //         const previewURL_complex = mapPreview + "?Map/layerids=" + UDPManager.Configs.getMapPreviewBaseLayerId() + "," + titleList.toString() + "&transparency=" + transparencyList.substring(0, transparencyList.length - 1) + "&config=" + UDPManager.Configs.getMapPreviewComplexAppend();

        //         window.open(previewURL_complex, "_blank");
        //     }
        // });
    },

    onCopyUrlToClipboard: function () {
        const testRequest = Ext.getCmp("apitestrequesturl").getValue();

        navigator.clipboard.writeText(testRequest);

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("URL in Zwischenablage kopiert");
    }
});
