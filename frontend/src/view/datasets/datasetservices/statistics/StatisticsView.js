Ext.define("UDPManager.view.datasets.datasetservices.statistics.StatisticsView", {
    extend: "Ext.panel.Panel",
    xtype: "dataset_services_statistics",
    alias: "widget.dataset_services_statistics",
    controller: "servicestatistics",
    style: "border-left: 1px solid #cfcfcf !important;",
    items: [
        {
            xtype: "cartesian",
            id: "serviceVisitsChart",
            reference: "chart",
            width: "100%",
            height: 500,
            animation: {
                duration: 200
            },
            store: "Visits",
            insetPadding: 40,
            innerPadding: {
                left: 20,
                right: 20
            },
            legend: {
                type: "dom",
                docked: "bottom"
            },
            sprites: [{
                type: "text",
                text: "Gesamtzugriffe",
                fontSize: 22,
                width: 500,
                height: 30,
                x: 40,
                y: 20
            }],
            axes: [
                {
                    type: "numeric",
                    position: "left",
                    grid: true,
                    minimum: 0
                }, {
                    type: "category",
                    position: "bottom",
                    grid: true,
                    label: {
                        rotate: {
                            degrees: -45
                        }
                    }
                }
            ],
            series: [{
                type: "bar",
                title: ["Internet", "Intranet"],
                xField: "month",
                yField: ["visits_internet", "visits_intranet"],
                stacked: true,
                style: {
                    opacity: 0.80,
                    maxBarWidth: 30
                },
                highlight: {
                    fillStyle: "yellow"
                },
                tooltip: {
                    renderer: "onBarTooltipRender"
                }
            }]
        }
    ],
    dockedItems: [
        {
            xtype: "toolbar",
            id: "visitsStatsToolbar",
            dock: "top",
            padding: "20 0 0 20",
            items: [
                {
                    id: "savevvisitsstats",
                    text: "Grafik speichern",
                    handler: "onSaveVisitsStats"
                }
            ]
        }
    ]
});
