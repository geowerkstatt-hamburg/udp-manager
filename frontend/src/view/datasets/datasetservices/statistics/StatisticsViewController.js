Ext.define("UDPManager.view.datasets.datasetservices.statistics.StatisticsViewController", {
    extend: "Ext.app.ViewController",
    alias: "controller.servicestatistics",

    onBarTooltipRender: function (tooltip, record, item) {
        const fieldIndex = Ext.Array.indexOf(item.series.getYField(), item.field);
        const source = item.series.getTitle()[fieldIndex];

        tooltip.setHtml("Zugriffe - " + source + " " + record.get("month") + ": " + record.get(item.field));
    },

    onLabelRender: function (text, sprite, config, rendererData, index) {
        const store = rendererData.store;
        const storeItems = store.getData().items;
        const record = storeItems[index];

        return "gesamt: " + record.data.visits_total;
    },

    onSaveVisitsStats: function () {
        const serviceTitle = Ext.getCmp("servicetitle").getSubmitValue();
        const dt = Ext.getCmp("serviceVisitsChart").getImage().data.replace(/^data:image\/[^;]*/, "data:application/octet-stream");

        if (dt) {
            const download_link = document.createElement("a");

            download_link.href = dt;
            download_link.target = "_blank";
            download_link.download = "Zugriffe " + serviceTitle + ".png";
            download_link.click();
            download_link.remove();
        }
    }
});
