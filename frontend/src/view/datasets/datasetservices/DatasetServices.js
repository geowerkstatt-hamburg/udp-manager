Ext.define("UDPManager.view.datasets.datasetservices.DatasetServices", {
    extend: "Ext.panel.Panel",
    xtype: "dataset_services",
    alias: "widget.dataset_services",
    controller: "dataset_services",
    title: "Schnittstellen",
    layout: "vbox",
    viewModel: {
        stores: {
            gridstore_services: {
                type: "servicesdataset"
            },
            gridstore_servicelayers: {
                type: "layersservice"
            }
        }
    },
    initComponent: function () {
        this.items = [
            {
                xtype: "panel",
                layout: "column",
                width: "100%",
                height: "100%",
                items: [
                    {
                        xtype: "container",
                        columnWidth: 0.35,
                        height: "100%",
                        style: "border-right: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                        items: [
                            {
                                xtype: "gridpanel",
                                id: "grid_datasetservices",
                                bind: "{gridstore_services}",
                                reference: "grid_datasetservices",
                                listeners: {
                                    select: "onGridServiceItemSelected"
                                },
                                rowLines: false,
                                viewConfig: {
                                    //stripeRows: true,
                                    enableTextSelection: true,
                                    getRowClass: function () {
                                        return "multiline-row";
                                    }
                                },
                                height: Ext.Element.getViewportHeight() - 107,
                                autoScroll: true,
                                buttonAlign: "left",
                                columns: [
                                    {
                                        xtype: "actioncolumn",
                                        text: "Status",
                                        width: 90,
                                        menuDisabled: true,
                                        sortable: false,
                                        align: "center",
                                        renderer: function (value, metadata, record) {
                                            if (record.get("status_dev") === true) {
                                                this.items[0].iconCls = "x-fa fa-info-circle orangeIcon firstIcon";
                                                this.items[0].tooltip = "Schnittstelle in Status dev aufrufen";
                                                this.items[1].iconCls = "x-fa fa-info-circle greyIcon secondIcon";
                                                this.items[1].tooltip = "Schnittstelle in Status stage aufrufen";
                                                this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                                                this.items[2].tooltip = "Schnittstelle in Status prod aufrufen";
                                            }
                                            else {
                                                this.items[0].iconCls = "x-fa fa-info-circle greyIcon firstIcon";
                                                this.items[1].iconCls = "x-fa fa-info-circle greyIcon secondIcon";
                                                this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                                            }
                                            if (record.get("status_stage") === true) {
                                                this.items[1].iconCls = "x-fa fa-info-circle yellowIcon secondIcon";
                                                this.items[1].tooltip = "Schnittstelle in Status stage aufrufen";
                                                this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                                                this.items[2].tooltip = "Schnittstelle in Status prod aufrufen";
                                            }
                                            else {
                                                this.items[1].iconCls = "x-fa fa-info-circle greyIcon secondIcon";
                                                this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                                            }
                                            if (record.get("status_prod") === true) {
                                                this.items[2].iconCls = "x-fa fa-info-circle greenIcon";
                                                this.items[2].tooltip = "Schnittstelle in Status prod aufrufen";
                                            }
                                            else {
                                                this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                                            }
                                        },
                                        items: [
                                            {
                                                itemId: "serviceinfostatusdev",
                                                iconCls: "x-fa fa-info-circle greyIcon firstIcon",
                                                tooltip: "Schnittstelle in Status dev aufrufen",
                                                handler: "onApiCall"
                                            },
                                            {
                                                itemId: "serviceinfostatusstage",
                                                iconCls: "x-fa fa-info-circle greyIcon",
                                                tooltip: "Schnittstelle in Status stage aufrufen",
                                                handler: "onApiCall"
                                            },
                                            {
                                                itemId: "serviceinfostatusprod",
                                                iconCls: "x-fa fa-info-circle greyIcon",
                                                tooltip: "Schnittstelle in Status prod aufrufen",
                                                handler: "onApiCall"
                                            }
                                        ]
                                    },
                                    {
                                        text: "ID",
                                        dataIndex: "id",
                                        width: 70
                                    },
                                    {
                                        text: "Titel",
                                        dataIndex: "title",
                                        flex: 1
                                    },
                                    {
                                        text: "Typ",
                                        dataIndex: "type",
                                        width: 85
                                    },
                                    {
                                        dataIndex: "ds_count",
                                        width: 35,
                                        renderer: function (value) {
                                            if (value > 1) {
                                                return "<div class=\"x-fa fa-link\" data-qtip=\"mit mehreren Datensätzen verknüpft\"></div>";
                                            }
                                            else {
                                                return "";
                                            }
                                        }
                                    }
                                ],
                                dockedItems: [{
                                    xtype: "toolbar",
                                    dock: "bottom",
                                    items: [
                                        {
                                            id: "addnewservice",
                                            iconCls: "x-fa fa-plus",
                                            cls: "custom-btn-icon",
                                            disabled: window.read_only,
                                            scale: "medium",
                                            tooltip: "Neue Schnittstelle anlegen (Blanko)",
                                            listeners: {
                                                click: "onAddNewService"
                                            }
                                        },
                                        {
                                            id: "linkservice",
                                            iconCls: "x-fa fa-link",
                                            cls: "custom-btn-icon",
                                            disabled: true,
                                            scale: "medium",
                                            tooltip: "Schnittstelle mit weiterem Datensatz verknüpfen",
                                            listeners: {
                                                click: "onLinkService"
                                            }
                                        },
                                        {
                                            xtype: "button",
                                            id: "duplicateservice",
                                            disabled: true,
                                            scale: "medium",
                                            iconCls: "x-fa fa-copy",
                                            cls: "custom-btn-icon",
                                            listeners: {
                                                click: "onDuplicateService"
                                            }
                                        },
                                        {
                                            xtype: "button",
                                            id: "deleteservice",
                                            disabled: true,
                                            scale: "medium",
                                            iconCls: "x-fa fa-trash",
                                            cls: "custom-btn-icon",
                                            listeners: {
                                                click: "onDeleteService"
                                            }
                                        },
                                        {
                                            id: "shareservice",
                                            iconCls: "x-fa fa-share-alt",
                                            cls: "custom-btn-icon",
                                            scale: "medium",
                                            disabled: true,
                                            tooltip: "Link auf diese Schnittstelle teilen",
                                            listeners: {
                                                click: "onShareService"
                                            }
                                        },
                                        "-",
                                        {
                                            xtype: "button",
                                            id: "generateconfigbutton",
                                            tooltip: "Konfigurationsdateien generieren",
                                            scale: "medium",
                                            disabled: true,
                                            iconCls: "x-fa fa-wrench",
                                            cls: "custom-btn-icon-dev",
                                            listeners: {
                                                click: "onBeforeDeploy"
                                            }
                                        },
                                        {
                                            xtype: "button",
                                            id: "deploystageconfigbutton",
                                            tooltip: "Konfigurationsdateien werden in die stage verschoben",
                                            scale: "medium",
                                            disabled: true,
                                            iconCls: "x-fa fa-tasks",
                                            cls: "custom-btn-icon-stage",
                                            listeners: {
                                                click: "onBeforeDeploy"
                                            }
                                        },
                                        {
                                            xtype: "button",
                                            id: "deployprodconfigbutton",
                                            tooltip: "Konfigurationsdateien werden in die prod kopiert",
                                            scale: "medium",
                                            disabled: true,
                                            iconCls: "x-fa fa-toggle-on",
                                            cls: "custom-btn-icon-prod",
                                            listeners: {
                                                click: "onBeforeDeploy"
                                            }
                                        },
                                        {
                                            xtype: "button",
                                            id: "undeployprodconfigbutton",
                                            tooltip: "Konfigurationsdateien werden aus der prod gelöscht",
                                            scale: "medium",
                                            disabled: true,
                                            iconCls: "x-fa fa-toggle-off",
                                            cls: "custom-btn-icon-undeploy",
                                            listeners: {
                                                click: "onBeforeUnDeploy"
                                            }
                                        },
                                        {
                                            xtype: "tbspacer",
                                            flex: 1
                                        }
                                    ]
                                }]
                            }
                        ]
                    },
                    {
                        xtype: "tabpanel",
                        id: "services_main_tabs",
                        activeTab: 0,
                        tabPosition: "left",
                        tabRotation: 0,
                        columnWidth: 0.65,
                        items: [
                            {
                                xtype: "container",
                                title: "Schnittstellen-Details",
                                style: "white-space: pre-line;border-left: 1px solid #cfcfcf !important;",
                                items: [
                                    {
                                        xtype: "form",
                                        id: "servicedetailsform",
                                        disabled: true,
                                        defaults: {
                                            xtype: "textfield",
                                            labelWidth: 150,
                                            editModeOnly: true
                                        },
                                        bodyPadding: 20,
                                        height: Ext.Element.getViewportHeight() - 109,
                                        autoScroll: true,
                                        scrollable: true,
                                        style: "border-top: 1px solid #cfcfcf !important;",
                                        items: [
                                            {
                                                xtype: "fieldcontainer",
                                                margin: "0 0 20 0",
                                                items: [{
                                                    xtype: "segmentedbutton",
                                                    items: [
                                                        {
                                                            id: "detailssegbtndev",
                                                            text: "dev",
                                                            pressed: true,
                                                            scale: "medium",
                                                            cls: "btn-api-dev",
                                                            listeners: {
                                                                click: "onSelectServiceDetails"
                                                            }
                                                        },
                                                        {
                                                            id: "detailssegbtnstage",
                                                            text: "stage",
                                                            scale: "medium",
                                                            disabled: true,
                                                            cls: "btn-api-stage",
                                                            listeners: {
                                                                click: "onSelectServiceDetails"
                                                            }
                                                        },
                                                        {
                                                            id: "detailssegbtnprod",
                                                            text: "prod",
                                                            scale: "medium",
                                                            disabled: true,
                                                            cls: "btn-api-prod",
                                                            listeners: {
                                                                click: "onSelectServiceDetails"
                                                            }
                                                        }
                                                    ]
                                                }]
                                            },
                                            {
                                                xtype: "component",
                                                margin: "0 0 10 155",
                                                id: "serviceeditnotes",
                                                html: "",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Schnittstellen-Attribute können nur im Bearbeitungsmodus aktualisiert werden"
                                                }
                                            },
                                            {
                                                xtype: "numberfield",
                                                id: "serviceid",
                                                name: "id",
                                                width: 360,
                                                readOnly: true,
                                                hidden: true,
                                                bind: "{grid_datasetservices.selection.id}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "servicestatusdev",
                                                name: "status_dev",
                                                width: 360,
                                                readOnly: true,
                                                hidden: true,
                                                bind: "{grid_datasetservices.selection.status_dev}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "servicestatusstage",
                                                name: "status_stage",
                                                width: 360,
                                                readOnly: true,
                                                hidden: true,
                                                bind: "{grid_datasetservices.selection.status_stage}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "servicestatusprod",
                                                name: "status_prod",
                                                width: 360,
                                                readOnly: true,
                                                hidden: true,
                                                bind: "{grid_datasetservices.selection.status_prod}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "serviceqa",
                                                name: "qa",
                                                width: 360,
                                                fieldLabel: "QA Prüfung",
                                                bind: "{grid_datasetservices.selection.qa}"
                                            },
                                            {
                                                xtype: "combo",
                                                id: "servicetype",
                                                width: 360,
                                                fieldLabel: "Typ",
                                                name: "type",
                                                store: UDPManager.Configs.serviceTypeFilterListGen(false),
                                                listeners: {
                                                    change: "onServiceTypeChange"
                                                },
                                                bind: "{grid_datasetservices.selection.type}",
                                                requiredField: true
                                            },
                                            {
                                                fieldLabel: "Externe Schnittstelle",
                                                xtype: "checkboxfield",
                                                id: "externalServiceCheckBox",
                                                name: "external",
                                                bind: "{grid_datasetservices.selection.external}",
                                                listeners: {
                                                    change: "onExternalServiceChanged"
                                                }
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                name: "reverseproxy",
                                                id: "servicereverseproxy",
                                                fieldLabel: "Reverse Proxy",
                                                hidden: true,
                                                listeners: {
                                                    change: "onReverseProxyCheckBoxChanged"
                                                }
                                            },
                                            {
                                                xtype: "combo",
                                                width: 360,
                                                fieldLabel: "Software",
                                                id: "servicesoftware",
                                                name: "software",
                                                store: UDPManager.Configs.softwareFilterListGen(false),
                                                bind: "{grid_datasetservices.selection.software}",
                                                listeners: {
                                                    change: "onFieldValueChanged"
                                                },
                                                requiredField: true
                                            },
                                            {
                                                fieldLabel: "Endpoint-Name",
                                                width: "100%",
                                                id: "servicename",
                                                name: "name",
                                                bind: "{grid_datasetservices.selection.name}",
                                                listeners: {
                                                    change: "onFieldValueChanged"
                                                },
                                                requiredField: true,
                                                defaultValue: true,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Default ist der Kurzname des Datensatzes. Anpassungen nur nötig, wenn zum Datensatz ein zweiter WMS oder WFS konfiguriert werden muss. Im Regelfall greift aber die 1 Datensatz : 1 Dienst Konvention."
                                                }
                                            },
                                            {
                                                fieldLabel: "Titel",
                                                id: "servicetitle",
                                                width: "100%",
                                                name: "title",
                                                fieldStyle: "font-weight: bold;",
                                                bind: "{grid_datasetservices.selection.title}",
                                                requiredField: true,
                                                defaultValue: true,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Default setzt sich aus API Typ und Datensatz Titel zusammen."
                                                }
                                            },
                                            {
                                                xtype: "combo",
                                                fieldLabel: "Version",
                                                id: "serviceversion",
                                                store: [],
                                                width: 360,
                                                name: "version",
                                                bind: "{grid_datasetservices.selection.version}",
                                                defaultValue: true,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Default abhängig von API-Typ."
                                                }
                                            },
                                            {
                                                xtype: "fieldset",
                                                id: "statusspecificfieldsetdevstage",
                                                padding: "5 5 5 5",
                                                cls: "prio-fieldset",
                                                collapsible: false,
                                                collapsed: false,
                                                items: [
                                                    {
                                                        xtype: "component",
                                                        id: "statusspecificicon",
                                                        html: "<div class='x-fa fa-circle orangeIconSmall'></div>"
                                                    },
                                                    {
                                                        xtype: "combo",
                                                        fieldLabel: "Server",
                                                        labelWidth: 145,
                                                        width: "100%",
                                                        id: "serviceserver",
                                                        name: "server",
                                                        store: UDPManager.Configs.serverFilterListGen(false),
                                                        bind: "{grid_datasetservices.selection.server}",
                                                        listeners: {
                                                            change: "onFieldValueChanged"
                                                        },
                                                        requiredField: true,
                                                        defaultValue: true
                                                    },
                                                    {
                                                        xtype: "combo",
                                                        fieldLabel: "Instanz / Ordner",
                                                        labelWidth: 145,
                                                        width: "100%",
                                                        id: "servicefolder",
                                                        name: "folder",
                                                        bind: "{grid_datasetservices.selection.folder}",
                                                        listeners: {
                                                            change: "onFieldValueChanged"
                                                        },
                                                        requiredField: true,
                                                        defaultValue: true,
                                                        editable: false,
                                                        emptyText: "Default abhängig vom API-Typ"
                                                    },
                                                    {
                                                        xtype: "container",
                                                        layout: "hbox",
                                                        margin: "5 0 5 0",
                                                        items: [
                                                            {
                                                                xtype: "textfield",
                                                                fieldLabel: "interne URL",
                                                                id: "serviceurlint",
                                                                flex: 1,
                                                                labelWidth: 145,
                                                                name: "url_int",
                                                                bind: "{grid_datasetservices.selection.url_int}",
                                                                editable: false,
                                                                defaultValue: true
                                                            },
                                                            {
                                                                xtype: "button",
                                                                id: "serviceurlinteditbutton",
                                                                margin: "0 5 0 10",
                                                                width: 30,
                                                                disabled: window.read_only,
                                                                autoEl: {
                                                                    tag: "div",
                                                                    "data-qtip": "interne URL bearbeiten"
                                                                },
                                                                iconCls: "x-fa fa-pen",
                                                                listeners: {
                                                                    click: "onEditInternalUrlToggle"
                                                                }
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        xtype: "container",
                                                        layout: "hbox",
                                                        margin: "5 0 5 0",
                                                        items: [
                                                            {
                                                                xtype: "textfield",
                                                                fieldLabel: "externe URL",
                                                                id: "serviceurlext",
                                                                flex: 1,
                                                                labelWidth: 145,
                                                                name: "url_ext",
                                                                bind: "{grid_datasetservices.selection.url_ext}",
                                                                requiredField: true,
                                                                defaultValue: true,
                                                                editable: false
                                                            },
                                                            {
                                                                xtype: "button",
                                                                id: "serviceurlexteditbutton",
                                                                margin: "0 5 0 10",
                                                                width: 30,
                                                                disabled: window.read_only,
                                                                autoEl: {
                                                                    tag: "div",
                                                                    "data-qtip": "externe URL bearbeiten"
                                                                },
                                                                iconCls: "x-fa fa-pen",
                                                                listeners: {
                                                                    click: "onEditExternalUrlToggle"
                                                                }
                                                            }
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldset",
                                                id: "statusspecificfieldsetprod",
                                                padding: "5 5 5 5",
                                                cls: "prio-fieldset",
                                                collapsible: false,
                                                collapsed: false,
                                                hidden: true,
                                                items: [
                                                    {
                                                        xtype: "component",
                                                        id: "statusspecificiconprod",
                                                        html: "<div class='x-fa fa-circle greenIconSmall'></div>"
                                                    },
                                                    {
                                                        xtype: "combo",
                                                        fieldLabel: "Server",
                                                        labelWidth: 145,
                                                        width: "100%",
                                                        id: "serviceserverprod",
                                                        name: "server_prod",
                                                        store: UDPManager.Configs.serverFilterListGen(false),
                                                        bind: "{grid_datasetservices.selection.server_prod}",
                                                        listeners: {
                                                            change: "onFieldValueChanged"
                                                        },
                                                        requiredField: true,
                                                        defaultValue: true
                                                    },
                                                    {
                                                        xtype: "combo",
                                                        fieldLabel: "Instanz / Ordner",
                                                        labelWidth: 145,
                                                        width: "100%",
                                                        id: "servicefolderprod",
                                                        name: "folder_prod",
                                                        bind: "{grid_datasetservices.selection.folder_prod}",
                                                        listeners: {
                                                            change: "onFieldValueChanged"
                                                        },
                                                        requiredField: true,
                                                        defaultValue: true,
                                                        editable: false,
                                                        emptyText: "Default abhängig vom API-Typ"
                                                    },
                                                    {
                                                        xtype: "textfield",
                                                        fieldLabel: "interne URL",
                                                        id: "serviceurlintprod",
                                                        width: "100%",
                                                        labelWidth: 145,
                                                        name: "url_int_prod",
                                                        bind: "{grid_datasetservices.selection.url_int_prod}",
                                                        editable: false,
                                                        defaultValue: true
                                                    },
                                                    {
                                                        xtype: "textfield",
                                                        fieldLabel: "externe URL",
                                                        id: "serviceurlextprod",
                                                        width: "100%",
                                                        labelWidth: 145,
                                                        name: "url_ext_prod",
                                                        bind: "{grid_datasetservices.selection.url_ext_prod}",
                                                        requiredField: true,
                                                        defaultValue: true,
                                                        editable: false
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldcontainer",
                                                id: "servicesecuritycontainer",
                                                layout: "hbox",
                                                fieldLabel: "Absicherung",
                                                hidden: true,
                                                width: "100%",
                                                items: [{
                                                    xtype: "combo",
                                                    store: UDPManager.Configs.securityTypeFilterListGen(),
                                                    value: "keine",
                                                    autoEl: {
                                                        tag: "div",
                                                        "data-qtip": "Auswahl der Absicherung"
                                                    },
                                                    id: "servicesecuritytype",
                                                    name: "security_type",
                                                    disabled: window.read_only,
                                                    bind: "{grid_datasetservices.selection.security_type}",
                                                    labelWidth: 0,
                                                    width: 205,
                                                    editable: false,
                                                    listeners: {
                                                        change: "onSecurityTypeChange"
                                                    }
                                                },
                                                {
                                                    xtype: "button",
                                                    id: "opensetallowedgroupsbutton",
                                                    hidden: true,
                                                    margin: "0 0 0 20",
                                                    tooltip: "AD Gruppen berechtigen",
                                                    text: "AD Gruppen berechtigen",
                                                    listeners: {
                                                        click: "onOpenSetAllowedGroups"
                                                    }
                                                }]
                                            },
                                            {
                                                xtype: "container",
                                                layout: "hbox",
                                                margin: "5 0 5 0",
                                                items: [
                                                    {
                                                        xtype: "textfield",
                                                        fieldLabel: "Abgesicherte URL",
                                                        id: "serviceurlsec",
                                                        flex: 1,
                                                        labelWidth: 150,
                                                        name: "url_sec",
                                                        bind: "{grid_datasetservices.selection.url_sec}",
                                                        hidden: true,
                                                        editable: false
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "serviceurlseceditbutton",
                                                        margin: "0 5 0 10",
                                                        width: 30,
                                                        hidden: true,
                                                        autoEl: {
                                                            tag: "div",
                                                            "data-qtip": "abgesicherte URL bearbeiten"
                                                        },
                                                        iconCls: "x-fa fa-pen",
                                                        listeners: {
                                                            click: "onEditSecUrlToggle"
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                fieldLabel: "Prüfstatus",
                                                store: ["Geprüft", "Ungeprüft", "Hinweis", "Fehler"],
                                                xtype: "combo",
                                                id: "servicecheckstatus",
                                                name: "check_status",
                                                hidden: true,
                                                editable: false,
                                                width: 360,
                                                bind: "{grid_datasetservices.selection.check_status}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                id: "serviceclickradius",
                                                fieldLabel: "Klickradius (Px)",
                                                minValue: 0,
                                                labelWidth: 150,
                                                width: 360,
                                                name: "clickradius",
                                                bind: "{grid_datasetservices.selection.clickradius}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                id: "servicemaxfeatures",
                                                fieldLabel: "Max. Features",
                                                minValue: 0,
                                                labelWidth: 150,
                                                width: 360,
                                                name: "maxfeatures",
                                                bind: "{grid_datasetservices.selection.maxfeatures}"
                                            },
                                            {
                                                xtype: "fieldset",
                                                id: "wmsthemeconfigurationfieldset",
                                                layout: "hbox",
                                                width: "100%",
                                                title: "WMS Themenkonfiguration",
                                                hidden: true,
                                                frame: true,
                                                collapsible: true,
                                                collapsed: true,
                                                cls: "prio-fieldset-only-edit-mode",
                                                items: [
                                                    {
                                                        xtype: "treepanel",
                                                        id: "wmsthemelayertree",
                                                        title: "Nicht zugeordnete Layer",
                                                        autoScroll: true,
                                                        store: "WMSThemeLayer",
                                                        rootVisible: true,
                                                        height: 270,
                                                        width: "50%",
                                                        frame: true,
                                                        margin: "0 5 5 0",
                                                        columns: [
                                                            {
                                                                xtype: "treecolumn",
                                                                text: "Name",
                                                                dataIndex: "name",
                                                                sortable: false,
                                                                flex: 1
                                                            }
                                                        ],
                                                        viewConfig: {
                                                            plugins: {
                                                                treeviewdragdrop: {
                                                                    ddGroup: "two-trees-drag-drop",
                                                                    containerScroll: true,
                                                                    allowContainerDrops: true
                                                                }
                                                            }
                                                        }
                                                    },
                                                    {
                                                        xtype: "treepanel",
                                                        id: "wmsthemeconfigtree",
                                                        title: "Themenbaum",
                                                        autoScroll: true,
                                                        store: "WMSThemeConfig",
                                                        height: 270,
                                                        width: "50%",
                                                        rootVisible: true,
                                                        frame: true,
                                                        columns: [
                                                            {
                                                                xtype: "treecolumn",
                                                                text: "Name",
                                                                dataIndex: "name",
                                                                sortable: false,
                                                                flex: 1
                                                            }
                                                        ],
                                                        viewConfig: {
                                                            plugins: {
                                                                treeviewdragdrop: {
                                                                    ddGroup: "two-trees-drag-drop",
                                                                    containerScroll: true,
                                                                    allowContainerDrops: true
                                                                }
                                                            }
                                                        },
                                                        bbar: [
                                                            "->",
                                                            {
                                                                id: "deletewmsthemeconfig",
                                                                text: "Löschen",
                                                                disabled: window.read_only,
                                                                handler: "deleteWmsThemeConfig"
                                                            },
                                                            {
                                                                id: "savewmsthemeconfig",
                                                                text: "Speichern",
                                                                disabled: window.read_only,
                                                                handler: "saveWmsThemeConfig"
                                                            }
                                                        ]
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldset",
                                                title: "Metadaten",
                                                padding: "0 5 0 5",
                                                cls: "prio-fieldset-only-edit-mode",
                                                collapsible: true,
                                                collapsed: true,
                                                items: [
                                                    {
                                                        xtype: "textfield",
                                                        fieldLabel: "Metadaten UUID",
                                                        name: "md_id",
                                                        labelWidth: 145,
                                                        width: "100%",
                                                        id: "servicemdid",
                                                        bind: "{grid_datasetservices.selection.md_id}",
                                                        readOnly: false
                                                    },
                                                    {
                                                        xtype: "fieldcontainer",
                                                        layout: "hbox",
                                                        width: "100%",
                                                        items: [
                                                            {
                                                                xtype: "combo",
                                                                fieldLabel: "Quellkatalog",
                                                                store: "ConfigMetadataCatalogs",
                                                                displayField: "name",
                                                                id: "servicesourcecswname",
                                                                name: "config_metadata_catalog_name",
                                                                editable: false,
                                                                margin: "0 10 0 0",
                                                                labelWidth: 145,
                                                                flex: 1
                                                            },
                                                            {
                                                                xtype: "button",
                                                                id: "servicesearchmetadata",
                                                                disabled: window.read_only,
                                                                margin: "0 10 0 0",
                                                                width: 50,
                                                                autoEl: {
                                                                    tag: "div",
                                                                    "data-qtip": "Abfrage des Metadatenkatalogs via CSW, es wird nach dem eingegebenen Datensatz-Titel oder der eingegebenen Metadaten-ID gesucht"
                                                                },
                                                                iconCls: "x-fa fa-search",
                                                                listeners: {
                                                                    click: "onSearchMetadata"
                                                                }
                                                            },
                                                            {
                                                                xtype: "button",
                                                                id: "deleteservicemetadatacoupling",
                                                                width: 50,
                                                                autoEl: {
                                                                    tag: "div",
                                                                    "data-qtip": "Löscht die Verknüpfung auf den Metadatensatz."
                                                                },
                                                                hidden: true,
                                                                disabled: true,
                                                                iconCls: "x-fa fa-times",
                                                                listeners: {
                                                                    click: "onDeleteMetadataCouplingService"
                                                                }
                                                            },
                                                            {
                                                                xtype: "displayfield",
                                                                id: "servicemetadatalink",
                                                                name: "metadata_link",
                                                                value: "Keine Metadaten verknüpft",
                                                                labelWidth: 0,
                                                                width: 180,
                                                                margin: "0 10 0 10"
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        title: "Beschreibung",
                                                        xtype: "fieldset",
                                                        padding: "0 5 0 5",
                                                        collapsible: true,
                                                        collapsed: true,
                                                        items: [{
                                                            xtype: "textarea",
                                                            name: "description",
                                                            id: "servicedescription",
                                                            width: "100%",
                                                            height: 120,
                                                            bind: "{grid_datasetservices.selection.description}",
                                                            readOnly: false
                                                        }]
                                                    },
                                                    {
                                                        xtype: "fieldset",
                                                        title: "Schlagworte",
                                                        padding: "5 5 5 5",
                                                        cls: "prio-fieldset",
                                                        collapsible: true,
                                                        collapsed: true,
                                                        layout: "fit",
                                                        items: [{
                                                            xtype: "tagfield",
                                                            id: "servicekeywords",
                                                            name: "keywords",
                                                            autoEl: {
                                                                tag: "div",
                                                                "data-qtip": "Auswahl über Dropddown, oder durch Eingabe, gefolgt von &quot;,&quot;"
                                                            },
                                                            listeners: {
                                                                beforeselect: function (combo, record) {
                                                                    const keyword_array = Ext.getCmp("servicekeywords").getValueRecords();
                                                                    let is_mrh = false;
                                                                    let is_only_mrh = false;

                                                                    for (let i = 0; i < keyword_array.length; i++) {
                                                                        if (keyword_array[i].data.text === "mrh") {
                                                                            is_mrh = true;
                                                                        }
                                                                        if (keyword_array[i].data.text === "only_mrh") {
                                                                            is_only_mrh = true;
                                                                        }
                                                                    }
                                                                    if (record.data.text === "mrh" && is_only_mrh) {
                                                                        return false;
                                                                    }
                                                                    if (record.data.text === "only_mrh" && is_mrh) {
                                                                        return false;
                                                                    }
                                                                }
                                                            },
                                                            store: "ServiceKeywords",
                                                            displayField: "text",
                                                            valueField: "text",
                                                            forceSelection: false,
                                                            queryMode: "local",
                                                            createNewOnEnter: true,
                                                            filterPickList: true,
                                                            bind: "{grid_datasetservices.selection.keywords}",
                                                            readOnly: false
                                                        }]
                                                    },
                                                    {
                                                        title: "Nutzungsbedingungen",
                                                        xtype: "fieldset",
                                                        padding: "0 5 0 5",
                                                        collapsible: true,
                                                        collapsed: true,
                                                        items: [{
                                                            xtype: "textarea",
                                                            name: "fees",
                                                            id: "servicefees",
                                                            width: "100%",
                                                            height: 120,
                                                            bind: "{grid_datasetservices.selection.fees}",
                                                            readOnly: false
                                                        }]
                                                    },
                                                    {
                                                        xtype: "fieldset",
                                                        title: "Nutzungsbedingungen (JSON)",
                                                        padding: "0 5 0 5",
                                                        cls: "prio-fieldset",
                                                        collapsible: true,
                                                        collapsed: true,
                                                        items: [
                                                            {
                                                                xtype: "textarea",
                                                                id: "servicefeesjson",
                                                                name: "fees_json",
                                                                labelWidth: 0,
                                                                height: 120,
                                                                width: "100%",
                                                                bind: "{grid_datasetservices.selection.fees_json}",
                                                                readOnly: false
                                                            }
                                                        ]
                                                    },
                                                    {
                                                        title: "Zugriffsbeschränkungen",
                                                        xtype: "fieldset",
                                                        padding: "0 5 0 5",
                                                        collapsible: true,
                                                        collapsed: true,
                                                        items: [{
                                                            xtype: "textarea",
                                                            name: "access_constraints",
                                                            id: "serviceaccessconstraints",
                                                            width: "100%",
                                                            height: 120,
                                                            bind: "{grid_datasetservices.selection.access_constraints}",
                                                            readOnly: false
                                                        }]
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldset",
                                                title: "zusätzliche Informationen",
                                                padding: "0 5 0 5",
                                                cls: "prio-fieldset-only-edit-mode",
                                                collapsible: true,
                                                collapsed: true,
                                                items: [{
                                                    xtype: "textareafield",
                                                    grow: false,
                                                    readOnly: false,
                                                    allowBlank: true,
                                                    id: "serviceadditionalinfo",
                                                    name: "additional_info",
                                                    height: 100,
                                                    width: "100%",
                                                    bind: "{grid_datasetservices.selection.additional_info}"
                                                }]
                                            },
                                            {
                                                xtype: "fieldset",
                                                title: "Spezialeinstellungen",
                                                id: "servicespecialsettings",
                                                padding: "0 5 0 5",
                                                cls: "prio-fieldset-only-edit-mode",
                                                collapsible: true,
                                                collapsed: true,
                                                items: [
                                                    {
                                                        xtype: "checkboxfield",
                                                        id: "servicedisableconfigmodule",
                                                        name: "disable_config_module",
                                                        labelWidth: 145,
                                                        fieldLabel: "Automatische Config-Generierung deaktivieren",
                                                        bind: "{grid_datasetservices.selection.disable_config_module}",
                                                        listeners: {
                                                            change: "onDisableConfigModuleChanged"
                                                        }
                                                    },
                                                    {
                                                        xtype: "checkboxfield",
                                                        id: "servicedisableconfigmanagement",
                                                        name: "disable_config_management",
                                                        labelWidth: 145,
                                                        fieldLabel: "Konfigurations-Management deaktivieren",
                                                        bind: "{grid_datasetservices.selection.disable_config_management}",
                                                        listeners: {
                                                            change: "onDisableConfigManagementChanged"
                                                        }
                                                    }
                                                ]
                                            }
                                        ],
                                        dockedItems: [{
                                            xtype: "toolbar",
                                            dock: "bottom",
                                            border: true,
                                            style: "border-top: 1px solid #cfcfcf !important;",
                                            items: [
                                                {
                                                    xtype: "component",
                                                    html: "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Pflichtfeld</span>"
                                                },
                                                {
                                                    xtype: "component",
                                                    html: "<span style='color:grey;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Standardwert</span>"
                                                },
                                                {
                                                    xtype: "component",
                                                    flex: 1,
                                                    html: ""
                                                },
                                                {
                                                    text: "Speichern",
                                                    id: "saveservicedetails",
                                                    scale: "medium",
                                                    cls: "btn-save",
                                                    disabled: true,
                                                    margin: "0 7 0 0",
                                                    listeners: {
                                                        click: "onSaveServiceDetails"
                                                    }
                                                }
                                            ]
                                        }]
                                    }
                                ]
                            },
                            {
                                xtype: "dataset_services_layerconf",
                                id: "datasetservices_layers",
                                title: "Layer",
                                height: Ext.Element.getViewportHeight() - 108,
                                scrollable: true
                            },
                            {
                                xtype: "dataset_services_configuration",
                                id: "datasetservices_configuration",
                                title: "Konfiguration",
                                bodyPadding: 20,
                                height: Ext.Element.getViewportHeight() - 108,
                                scrollable: true
                            },
                            {
                                xtype: "panel",
                                title: "Prüfen",
                                bodyPadding: 20,
                                height: Ext.Element.getViewportHeight() - 108,
                                hidden: true,
                                scrollable: true,
                                items: [
                                    {
                                        xtype: "button",
                                        id: "testcheckbutton",
                                        text: "Prüfroutine starten",
                                        tooltip: "",
                                        scale: "medium",
                                        disabled: window.read_only,
                                        width: 200,
                                        margin: "0 25 25 0"
                                    }
                                ]
                            },
                            {
                                xtype: "dataset_services_statistics",
                                id: "datasetservices_statistics",
                                title: "Statistiken",
                                bodyPadding: 20,
                                height: Ext.Element.getViewportHeight() - 108,
                                disabled: true,
                                scrollable: true
                            }
                        ]
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }
});
