Ext.define("UDPManager.view.datasets.datasetservices.configuration.FileNameInputWindow", {
    extend: "Ext.window.Window",
    id: "filenameinput-window",
    alias: "datasetservices.filenameinput",
    height: 125,
    width: 300,
    title: "Dateiname eingeben",
    resizable: false,
    closeAction: "hide",
    controller: "serviceconfiguration",
    items: [
        {
            xtype: "panel",
            bodyPadding: 10,
            items: [
                {
                    xtype: "textfield",
                    allowBlank: false,
                    id: "nametextfield",
                    labelWidth: 0,
                    width: 280
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    "->",
                    {
                        id: "nestedmapfilecreatebutton",
                        text: "Anlegen",
                        hidden: true,
                        listeners: {
                            click: "onCreateFile"
                        }
                    },
                    {
                        id: "mapservertemplatecreatebutton",
                        text: "Anlegen",
                        hidden: true,
                        listeners: {
                            click: "onCreateFile"
                        }
                    }
                ]
            }]
        }
    ]
});
