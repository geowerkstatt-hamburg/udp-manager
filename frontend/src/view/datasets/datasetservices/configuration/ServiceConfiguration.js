Ext.define("UDPManager.view.datasets.datasetservices.configuration.ServiceConfiguration", {
    extend: "Ext.panel.Panel",
    xtype: "dataset_services_configuration",
    alias: "widget.dataset_services_configuration",
    layout: "vbox",
    controller: "serviceconfiguration",
    style: "border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",

    initComponent: function () {
        this.items = [
            {
                xtype: "fieldcontainer",
                margin: "0 0 20 0",
                items: [{
                    xtype: "segmentedbutton",
                    id: "configsegbtns",
                    items: [
                        {
                            id: "configsegbtndev",
                            text: "dev",
                            scale: "medium",
                            cls: "btn-api-dev",
                            listeners: {
                                click: "onSelectServiceConfig"
                            }
                        },
                        {
                            id: "configsegbtnstage",
                            text: "stage",
                            scale: "medium",
                            cls: "btn-api-stage",
                            listeners: {
                                click: "onSelectServiceConfig"
                            }
                        },
                        {
                            id: "configsegbtnprod",
                            text: "prod",
                            scale: "medium",
                            cls: "btn-api-prod",
                            listeners: {
                                click: "onSelectServiceConfig"
                            }
                        }
                    ]
                }]
            },
            {
                xtype: "component",
                id: "conferrorstatus",
                hidden: true,
                html: "<b>Hinweis:</b> Konfiguration nicht gefunden / konnte nicht geladen werden"
            },
            {
                xtype: "component",
                id: "confmoduledisabledstatus",
                hidden: true,
                html: "<b>Hinweis:</b> Für diese Schnittstelle ist das automatische Generieren der Konfigurationsdateien deaktiviert."
            },
            {
                xtype: "fieldset",
                id: "deegreeconfcontainer",
                hidden: true,
                layout: "vbox",
                width: "100%",
                title: "deegree Konfiguration",
                frame: true,
                collapsible: true,
                collapsed: false,
                items: [
                    {
                        xtype: "component",
                        id: "statusspecificconfigicondeegree",
                        env: "dev",
                        html: "<div class='x-fa fa-circle orangeIconSmall'></div>"
                    },
                    {
                        xtype: "fieldcontainer",
                        layout: "hbox",
                        items: [
                            {
                                xtype: "button",
                                id: "validatebutton",
                                text: "Workspace validieren",
                                margin: "10 10 10 5",
                                disabled: true,
                                listeners: {
                                    click: "onWorkspaceValidate"
                                }
                            },
                            {
                                xtype: "button",
                                id: "restartbutton",
                                text: "Workspace neu laden",
                                margin: "10 0 10 5",
                                disabled: true,
                                listeners: {
                                    click: "onWorkspaceRestart"
                                }
                            },
                            {
                                xtype: "component",
                                id: "deegreeworkspacetimestamp",
                                margin: "15 0 10 15",
                                html: ""
                            }
                        ]
                    },
                    {
                        xtype: "container",
                        layout: {
                            type: "table",
                            columns: 2,
                            tdAttrs: {style: "padding: 5px 10px;"}
                        },
                        items: [
                            {
                                xtype: "component",
                                html: "Endpunkt:"
                            },
                            {
                                xtype: "button",
                                id: "details_endpoint",
                                disabled: true,
                                listeners: {
                                    click: "onShowConf"
                                }
                            },
                            {
                                xtype: "component",
                                html: "Metadaten:"
                            },
                            {
                                xtype: "button",
                                id: "details_metadata",
                                disabled: true,
                                listeners: {
                                    click: "onShowConf"
                                }
                            },
                            {
                                xtype: "component",
                                id: "theme_comp",
                                html: "Theme:"
                            },
                            {
                                xtype: "button",
                                id: "details_theme",
                                disabled: true,
                                listeners: {
                                    click: "onShowConf"
                                }
                            },
                            {
                                xtype: "component",
                                id: "processconf_comp",
                                hidden: true,
                                html: "Prozesskonfiguration:"
                            },
                            {
                                xtype: "button",
                                id: "details_processconf",
                                hidden: true,
                                listeners: {
                                    click: "onShowConf"
                                }
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        layout: "column",
                        width: "100%",
                        margin: "10px, 0px, 0px, 0px",
                        items: [
                            {
                                xtype: "gridpanel",
                                id: "endpoint_details_grid",
                                height: 200,
                                columnWidth: 0.35,
                                autoScroll: true,
                                store: "FeatureTypes",
                                headerBorders: true,
                                rowLines: true,
                                frame: true,
                                bind: {
                                    selection: "{FeatureType}"
                                },
                                listeners: {
                                    select: "onFeatureTypeSelect"
                                },
                                margin: "0 0 5 0",
                                columns: [
                                    {
                                        dataIndex: "name",
                                        text: "Layer-/FeatureType-Name",
                                        align: "left",
                                        flex: 1,
                                        renderer: function (value) {
                                            return "<b>" + value + "</b>";
                                        }
                                    }
                                ],
                                viewConfig: {
                                    emptyText: "Keine Layer / FeatureTypes vorhanden",
                                    deferEmptyText: false,
                                    enableTextSelection: true
                                }
                            },
                            {
                                xtype: "panel",
                                id: "featuretypeconfpanel",
                                columnWidth: 0.65,
                                layout: {
                                    type: "table",
                                    columns: 2,
                                    tdAttrs: {style: "padding: 5px 10px;"}
                                },
                                padding: "30px 10x 10px 10px",
                                hidden: true,
                                items: [
                                    {
                                        xtype: "component",
                                        html: "Layer:",
                                        id: "details_layer_component",
                                        hidden: true
                                    },
                                    {
                                        xtype: "button",
                                        margin: "0px, 0px, 5px, 0px",
                                        id: "details_layer",
                                        hidden: true,
                                        bind: {
                                            text: "{FeatureType.layerStoreId}"
                                        },
                                        listeners: {
                                            click: "onShowConf"
                                        }
                                    },
                                    {
                                        xtype: "component",
                                        html: "Datasource:"
                                    },
                                    {
                                        xtype: "button",
                                        margin: "0px, 0px, 5px, 0px",
                                        id: "details_datasource",
                                        bind: {
                                            text: "{FeatureType.datasource}"
                                        },
                                        listeners: {
                                            click: "onShowConf"
                                        }
                                    },
                                    {
                                        xtype: "component",
                                        html: "JDBC:",
                                        hidden: true,
                                        id: "details_jdbc_component"
                                    },
                                    {
                                        xtype: "button",
                                        margin: "0px, 0px, 5px, 0px",
                                        id: "details_jdbc",
                                        bind: {
                                            text: "{FeatureType.jdbcId}"
                                        },
                                        listeners: {
                                            click: "onShowConf"
                                        }
                                    },
                                    {
                                        xtype: "component",
                                        html: "Styles:",
                                        hidden: true,
                                        id: "details_style_component"
                                    },
                                    {
                                        xtype: "panel",
                                        id: "details_styles"
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                xtype: "fieldset",
                id: "geoserverconfcontainer",
                hidden: true,
                layout: "vbox",
                width: "100%",
                title: "GeoServer Konfiguration",
                frame: true,
                collapsible: true,
                collapsed: false,
                items: [
                    {
                        xtype: "component",
                        id: "statusspecificconfigicongeoserver",
                        html: "<div class='x-fa fa-circle orangeIconSmall'></div>"
                    },
                    {
                        xtype: "button",
                        id: "geoserverrestartbutton",
                        text: "Workspace neu laden",
                        margin: "10 0 10 5",
                        disabled: true,
                        listeners: {
                            click: "onWorkspaceRestart"
                        }
                    },
                    {
                        xtype: "container",
                        layout: {
                            type: "table",
                            columns: 2,
                            tdAttrs: {style: "padding: 5px 10px;"}
                        },
                        items: [
                            {
                                xtype: "component",
                                html: "Workspace:"
                            },
                            {
                                xtype: "button",
                                id: "details_workspace",
                                disabled: true,
                                listeners: {
                                    click: "onShowConf"
                                }
                            },
                            {
                                xtype: "component",
                                html: "Namespace:"
                            },
                            {
                                xtype: "button",
                                id: "details_namespace",
                                disabled: true,
                                listeners: {
                                    click: "onShowConf"
                                }
                            }
                        ]
                    },
                    {
                        xtype: "panel",
                        layout: "column",
                        width: "100%",
                        margin: "10px, 0px, 0px, 0px",
                        items: [
                            {
                                xtype: "gridpanel",
                                id: "endpoint_details_grid_Geoserver",
                                height: 200,
                                columnWidth: 0.35,
                                autoScroll: true,
                                store: "FeatureTypesGeoserver",
                                headerBorders: true,
                                rowLines: true,
                                frame: true,
                                bind: {
                                    selection: "{FeatureTypeGeoserver}"
                                },
                                listeners: {
                                    select: "onFeatureTypeSelectGeoserver"
                                },
                                margin: "0 0 5 0",
                                columns: [
                                    {
                                        dataIndex: "name",
                                        text: "Layer-/FeatureType-Name",
                                        align: "left",
                                        flex: 1,
                                        renderer: function (value) {
                                            return "<b>" + value + "</b>";
                                        }
                                    }
                                ],
                                viewConfig: {
                                    emptyText: "Keine Layer / FeatureTypes vorhanden",
                                    deferEmptyText: false,
                                    enableTextSelection: true
                                }
                            },
                            {
                                xtype: "panel",
                                id: "featuretypeconfpanelGeoserver",
                                columnWidth: 0.65,
                                layout: {
                                    type: "table",
                                    columns: 2,
                                    tdAttrs: {style: "padding: 5px 10px;"}
                                },
                                padding: "30px 10x 10px 10px",
                                hidden: true,
                                items: [
                                    {
                                        xtype: "component",
                                        html: "Datastore:",
                                        id: "details_datastore_component_Geoserver",
                                    },
                                    {
                                        xtype: "button",
                                        id: "details_datastore_Geoserver",
                                        listeners: {
                                            click: "onShowConf"
                                        }
                                    },
                                    {
                                        xtype: "component",
                                        html: "Layer:",
                                        id: "details_layer_component_Geoserver",
                                    },
                                    {
                                        xtype: "button",
                                        margin: "0px, 0px, 5px, 0px",
                                        id: "details_layer_Geoserver",
                                        listeners: {
                                            click: "onShowConf"
                                        }
                                    },
                                    {
                                        xtype: "component",
                                        html: "Featuretype:"
                                    },
                                    {
                                        xtype: "button",
                                        margin: "0px, 0px, 5px, 0px",
                                        id: "details_featuretype_Geoserver",
                                        listeners: {
                                            click: "onShowConf"
                                        }
                                    },
                                    {
                                        xtype: "component",
                                        html: "Style:",
                                        id: "details_style_component_Geoserver",
                                    },
                                    {
                                        xtype: "button",
                                        margin: "0px, 0px, 5px, 0px",
                                        id: "details_style_Geoserver",
                                        listeners: {
                                            click: "onShowConf"
                                        }
                                    },
                                    {
                                        xtype: "component",
                                        html: "SLD:",
                                        id: "details_sld_component_Geoserver",
                                    },
                                    {
                                        xtype: "button",
                                        margin: "0px, 0px, 5px, 0px",
                                        id: "details_sld_Geoserver",
                                        listeners: {
                                            click: "onShowConf"
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                xtype: "fieldset",
                id: "ldproxyconfcontainer",
                hidden: true,
                layout: "vbox",
                width: "100%",
                title: "ldproxy Konfiguration",
                frame: true,
                collapsible: true,
                collapsed: false,
                items: [
                    {
                        xtype: "component",
                        id: "statusspecificconfigiconldproxy",
                        html: "<div class='x-fa fa-circle orangeIconSmall'></div>"
                    },
                    {
                        xtype: "button",
                        id: "ldproxyrestartbutton",
                        text: "Workspace neu laden",
                        margin: "10 0 10 5",
                        disabled: true,
                        listeners: {
                            click: "onWorkspaceRestart"
                        }
                    },
                    {
                        xtype: "container",
                        layout: {
                            type: "table",
                            columns: 2,
                            tdAttrs: {style: "padding: 5px 10px;"}
                        },
                        items: [
                            {
                                xtype: "component",
                                html: "Service:"
                            },
                            {
                                xtype: "button",
                                id: "details_service",
                                disabled: true,
                                listeners: {
                                    click: "onShowConf"
                                }
                            },
                            {
                                xtype: "component",
                                html: "Provider:"
                            },
                            {
                                xtype: "button",
                                id: "details_provider",
                                disabled: true,
                                listeners: {
                                    click: "onShowConf"
                                }
                            }
                        ]
                    }
                ]
            },
            {
                xtype: "fieldset",
                id: "mapserverconfcontainer",
                hidden: true,
                layout: "vbox",
                width: "100%",
                title: "MapServer Konfiguration",
                frame: true,
                collapsible: true,
                collapsed: false,
                items: [
                    {
                        xtype: "component",
                        id: "statusspecificconfigiconmapserver",
                        html: "<div class='x-fa fa-circle orangeIconSmall'></div>"
                    },
                    {
                        xtype: "container",
                        layout: {
                            type: "table",
                            columns: 2,
                            tdAttrs: {style: "padding: 5px 10px;"}
                        },
                        items: [
                            {
                                xtype: "component",
                                html: "Mapfile:"
                            },
                            {
                                xtype: "button",
                                id: "details_mapfile",
                                disabled: true,
                                listeners: {
                                    click: "onShowConf"
                                }
                            }
                        ]
                    },
                    {
                        xtype: "container",
                        width: "100%",
                        layout: "hbox",
                        items: [
                            {
                                xtype: "gridpanel",
                                id: "nestedmapfilesgrid",
                                title: "Nested Mapfiles",
                                height: 200,
                                margin: "5px 5px 10px 5px",
                                width: "50%",
                                autoScroll: true,
                                store: "NestedMapfiles",
                                headerBorders: true,
                                rowLines: true,
                                frame: true,
                                columns: [
                                    {
                                        dataIndex: "name",
                                        text: "Name",
                                        align: "left",
                                        flex: 1
                                    },
                                    {
                                        xtype: "actioncolumn",
                                        width: 26,
                                        sortable: false,
                                        menuDisabled: true,
                                        align: "center",
                                        handler: "onShowConf",
                                        items: [
                                            {
                                                iconCls: "x-fa fa-edit faIcon",
                                                tooltip: "Bearbeiten"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "actioncolumn",
                                        width: 26,
                                        sortable: false,
                                        menuDisabled: true,
                                        align: "center",
                                        handler: "onDeleteMapServerFile",
                                        items: [
                                            {
                                                iconCls: "x-fa fa-times deleteIcon",
                                                tooltip: "Eintrag löschen"
                                            }
                                        ]
                                    }
                                ],
                                viewConfig: {
                                    emptyText: "Keine nested Mapfiles vorhanden",
                                    deferEmptyText: false,
                                    enableTextSelection: true
                                },
                                buttonAlign: "left",
                                buttons: [
                                    {
                                        id: "addnewnestedmapfile",
                                        iconCls: "x-fa fa-plus",
                                        disabled: window.read_only,
                                        tooltip: "Neues nested Mapfile (Blanko)",
                                        width: 50,
                                        margin: "0 10 0 0",
                                        listeners: {
                                            click: "onAddNewFile"
                                        }
                                    }
                                ]
                            },
                            {
                                xtype: "gridpanel",
                                id: "mapservertemplatesgrid",
                                title: "Templates",
                                height: 200,
                                width: "50%",
                                margin: "5px 5px 10px 5px",
                                autoScroll: true,
                                store: "MapserverTemplates",
                                headerBorders: true,
                                rowLines: true,
                                frame: true,
                                columns: [
                                    {
                                        dataIndex: "name",
                                        text: "Name",
                                        align: "left",
                                        flex: 1
                                    },
                                    {
                                        xtype: "actioncolumn",
                                        width: 26,
                                        sortable: false,
                                        menuDisabled: true,
                                        align: "center",
                                        handler: "onShowConf",
                                        items: [
                                            {
                                                iconCls: "x-fa fa-edit faIcon",
                                                tooltip: "Bearbeiten"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "actioncolumn",
                                        width: 26,
                                        sortable: false,
                                        menuDisabled: true,
                                        align: "center",
                                        handler: "onDeleteMapServerFile",
                                        items: [
                                            {
                                                iconCls: "x-fa fa-times deleteIcon",
                                                tooltip: "Eintrag löschen"
                                            }
                                        ]
                                    }
                                ],
                                viewConfig: {
                                    emptyText: "Keine MapServer Templates vorhanden",
                                    deferEmptyText: false,
                                    enableTextSelection: true
                                },
                                buttonAlign: "left",
                                buttons: [
                                    {
                                        id: "addnewmapservertemplate",
                                        iconCls: "x-fa fa-plus",
                                        disabled: window.read_only,
                                        tooltip: "Neues MapServer Template (Blanko)",
                                        width: 50,
                                        margin: "0 10 0 0",
                                        listeners: {
                                            click: "onAddNewFile"
                                        }
                                    }
                                ]
                            }
                        ]
                    }
                ]
            },
            {
                xtype: "fieldset",
                id: "serviceconfigeditedcontainer",
                layout: "vbox",
                width: "100%",
                title: "manuell überschriebene Konfigurationen",
                collapsible: true,
                collapsed: false,
                frame: true,
                items: [
                    {
                        xtype: "gridpanel",
                        id: "serviceconfigeditedgrid",
                        height: 250,
                        width: "100%",
                        autoScroll: true,
                        store: "ServiceConfigsEdited",
                        headerBorders: true,
                        rowLines: true,
                        frame: true,
                        columns: [
                            {
                                dataIndex: "uri",
                                text: "Pfad",
                                align: "left",
                                flex: 1
                            },
                            {
                                xtype: "actioncolumn",
                                width: 26,
                                sortable: false,
                                menuDisabled: true,
                                align: "center",
                                handler: "onDeleteServiceConfigEdited",
                                items: [
                                    {
                                        iconCls: "x-fa fa-times deleteIcon",
                                        tooltip: "Eintrag löschen"
                                    }
                                ]
                            }
                        ],
                        viewConfig: {
                            emptyText: "Keine Layer / FeatureTypes vorhanden",
                            deferEmptyText: false,
                            enableTextSelection: true
                        }
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }
});
