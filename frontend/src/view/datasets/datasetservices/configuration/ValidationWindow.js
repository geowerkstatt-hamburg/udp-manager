Ext.define("UDPManager.view.datasets.datasetservices.configuration.ValidationWindow", {
    extend: "Ext.window.Window",
    alias: "configuration.ValidationWindow",
    id: "validation-window",
    height: 600,
    width: 800,
    resizable: false,
    title: "Validierungsergebnisse",
    header: true,
    items: [
        {
            xtype: "gridpanel",
            id: "validation_grid",
            height: 563,
            autoScroll: true,
            store: "ValidationResults",
            loadMask: true,
            headerBorders: true,
            rowLines: false,
            columns: [
                {
                    dataIndex: "file_name",
                    text: "Datei",
                    align: "left",
                    flex: 1,
                    renderer: function (value) {
                        return "<b>" + value + "</b>";
                    }
                }
            ],
            viewConfig: {
                deferEmptyText: false,
                enableTextSelection: true
            },
            plugins: [{
                ptype: "rowexpander",
                rowBodyTpl: new Ext.XTemplate(
                    "<p>{message}</p>")
            }]
        }
    ]
});
