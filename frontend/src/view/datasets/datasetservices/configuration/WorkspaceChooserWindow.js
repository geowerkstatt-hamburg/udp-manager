Ext.define("UDPManager.view.datasets.datasetservices.configuration.WorkspaceChooserWindow", {
    extend: "Ext.window.Window",
    id: "workspacechooser-window",
    alias: "datasetservices.workspacechooser",
    height: 320,
    width: 500,
    title: "Workspace wählen und deployen",
    resizable: false,
    closeAction: "hide",
    controller: "serviceconfiguration",
    items: [
        {
            xtype: "panel",
            bodyPadding: 10,
            height: 280,
            autoScroll: true,
            items: [
                {
                    xtype: "checkboxgroup",
                    id: "generateconfigservices",
                    columns: 2,
                    vertical: false,
                    labelWidth: 160,
                    width: "100%",
                    hidden: false,
                    allowBlank: false
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    "->",
                    {
                        id: "workspacechooserdeploydevbutton",
                        hidden: true,
                        disabled: true,
                        scale: "medium",
                        text: "Deploy",
                        listeners: {
                            click: "onDeployDev"
                        }
                    },
                    {
                        id: "workspacechooserdeploystagebutton",
                        hidden: true,
                        disabled: true,
                        scale: "medium",
                        text: "Deploy",
                        listeners: {
                            click: "onDeployStage"
                        }
                    },
                    {
                        id: "workspacechooserdeployprodbutton",
                        hidden: true,
                        disabled: true,
                        scale: "medium",
                        text: "Deploy",
                        listeners: {
                            click: "onDeployProd"
                        }
                    },
                    {
                        id: "workspacechooserundeployprodbutton",
                        hidden: true,
                        disabled: true,
                        scale: "medium",
                        text: "Deploy",
                        listeners: {
                            click: "onUnDeploy"
                        }
                    },
                    {
                        id: "workspacechoosergeneratebutton",
                        hidden: true,
                        disabled: true,
                        scale: "medium",
                        text: "Deploy",
                        listeners: {
                            click: "onGenerateConfig"
                        }
                    }
                ]
            }]
        }
    ]
});
