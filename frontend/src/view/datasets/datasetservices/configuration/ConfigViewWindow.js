Ext.define("UDPManager.view.datasets.datasetservices.configuration.ConfigViewWindow", {
    extend: "Ext.window.Window",
    alias: "configuration.ConfigViewWindow",
    id: "configview-window",
    height: Ext.Element.getViewportHeight() - 120,
    width: Ext.Element.getViewportWidth() - 120,
    resizable: false,
    title: "Konfiguration",
    header: true,
    controller: "serviceconfiguration",

    initComponent: function () {
        this.items = [
            {
                xtype: "panel",
                id: "configview-panel",
                height: Ext.Element.getViewportHeight() - 200,
                autoScroll: true,
                html: "<div id=\"container\" style=\"width: " + parseInt(Ext.Element.getViewportWidth() - 122) + "px; height: " + parseInt(Ext.Element.getViewportHeight() - 200) + "px; border: 1px solid grey\"></div>"
            },
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                width: Ext.Element.getViewportWidth() - 122,
                items: [
                    {
                        xtype: "component",
                        flex: 1,
                        html: ""
                    },
                    {
                        xtype: "button",
                        id: "overwriteconfig",
                        text: "Speichern",
                        scale: "medium",
                        cls: "btn-save",
                        tooltip: "Konfigurationsdatei wird überschrieben und als manuelle Bearbeitung gekennzeichnet",
                        disabled: true,
                        margin: "5 10 5 5",
                        listeners: {
                            click: "onOverwriteConfig"
                        }
                    }
                ]
            }
        ];
        this.callParent(arguments);
    }
});
