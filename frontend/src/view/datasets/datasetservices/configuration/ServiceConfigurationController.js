Ext.define("UDPManager.view.datasets.datasetservices.configuration.ServiceConfigurationController", {
    extend: "Ext.app.ViewController",
    alias: "controller.serviceconfiguration",

    onShowConf: function (btn, rowIndex) {
        let configView = Ext.getCmp("configview-window");

        if (configView) {
            configView.destroy();
        }

        configView = Ext.create("configuration.ConfigViewWindow");
        configView.show();

        let language = "xml";

        if (btn.id === "details_service" || btn.id === "details_provider") {
            language = "yaml";
        }

        const editor = monaco.editor.create(document.getElementById("container"), {
            language: language
        });

        if (btn.id === "details_mapfile") {
            editor.setValue(Ext.getStore("ServiceConfig").getAt(0).get("mapfileRaw"));
            editor.configUri = Ext.getStore("ServiceConfig").getAt(0).get("mapfileUri");
        }
        else if (btn.id === "details_service") {
            editor.setValue(Ext.getStore("ServiceConfig").getAt(0).get("serviceRaw"));
            editor.configUri = Ext.getStore("ServiceConfig").getAt(0).get("serviceUri");
        }
        else if (btn.id === "details_provider") {
            editor.setValue(Ext.getStore("ServiceConfig").getAt(0).get("providerRaw"));
            editor.configUri = Ext.getStore("ServiceConfig").getAt(0).get("providerUri");
        }
        else if (btn.id === "details_endpoint") {
            editor.setValue(Ext.getStore("ServiceConfig").getAt(0).get("endpointRaw"));
            editor.configUri = Ext.getStore("ServiceConfig").getAt(0).get("endpointUri");
        }
        else if (btn.id === "details_workspace") {
            editor.setValue(Ext.getStore("ServiceConfig").getAt(0).get("workspaceRaw"));
            editor.configUri = Ext.getStore("ServiceConfig").getAt(0).get("workspaceUri");
        }
        else if (btn.id === "details_namespace") {
            editor.setValue(Ext.getStore("ServiceConfig").getAt(0).get("namespaceRaw"));
            editor.configUri = Ext.getStore("ServiceConfig").getAt(0).get("namespaceUri");
        }
        else if (btn.id === "details_metadata") {
            editor.setValue(Ext.getStore("ServiceConfig").getAt(0).get("endpointMetadataRaw"));
            editor.configUri = Ext.getStore("ServiceConfig").getAt(0).get("endpointMetadataUri");
        }
        else if (btn.id === "details_theme") {
            editor.setValue(Ext.getStore("ServiceConfig").getAt(0).get("themeRaw"));
            editor.configUri = Ext.getStore("ServiceConfig").getAt(0).get("themeUri");
        }
        else if (btn.id === "details_datastore_Geoserver") {
            editor.setValue(Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.datastoreRaw);
            editor.configUri = Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.datastoreUri;
        }
        else if (btn.id === "details_layer_Geoserver") {
            editor.setValue(Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.layerRaw);
            editor.configUri = Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.layerUri;
        }
        else if (btn.id === "details_featuretype_Geoserver") {
            editor.setValue(Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.featuretypeRaw);
            editor.configUri = Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.featuretypeUri;
        }
        else if (btn.id === "details_style_Geoserver") {
            editor.setValue(Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.styleXMLRaw);
            editor.configUri = Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.styleXMLUri;
        }
        else if (btn.id === "details_sld_Geoserver") {
            editor.setValue(Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.styleSLDRaw);
            editor.configUri = Ext.getCmp("endpoint_details_grid_Geoserver").getSelectionModel().getSelection()[0].data.styleSLDUri;
        }
        else if (btn.id === "details_layer") {
            editor.setValue(Ext.getCmp("endpoint_details_grid").getSelectionModel().getSelection()[0].data.layerRaw);
            editor.configUri = Ext.getCmp("endpoint_details_grid").getSelectionModel().getSelection()[0].data.layerUri;
        }
        else if (btn.id === "details_datasource") {
            editor.setValue(Ext.getCmp("endpoint_details_grid").getSelectionModel().getSelection()[0].data.featureStoreRaw);
            editor.configUri = Ext.getCmp("endpoint_details_grid").getSelectionModel().getSelection()[0].data.featureStoreUri;
        }
        else if (btn.id === "details_jdbc") {
            editor.setValue(Ext.getCmp("endpoint_details_grid").getSelectionModel().getSelection()[0].data.jdbcRaw);
            editor.configUri = Ext.getCmp("endpoint_details_grid").getSelectionModel().getSelection()[0].data.jdbcUri;
        }
        else if (btn.id.indexOf("details_style") > -1) {
            const index = parseInt(btn.id.replace("details_style_", ""));

            editor.setValue(Ext.getCmp("endpoint_details_grid").getSelectionModel().getSelection()[0].data.styles[index].styleRaw);
            editor.configUri = Ext.getCmp("endpoint_details_grid").getSelectionModel().getSelection()[0].data.styles[index].styleUri;
        }
        else if (btn.grid) {
            if (btn.grid.id) {
                editor.setValue(btn.grid.getStore().getAt(rowIndex).data.raw);
                editor.configUri = btn.grid.getStore().getAt(rowIndex).data.uri;
            }
        }

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").setEditor(editor);

        const service_id = Ext.getCmp("serviceid").getValue();
        const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);


        const status = Ext.getCmp("statusspecificconfigicon" + service.data.software.toLowerCase()).env;

        if (status === "dev") {
            Ext.getCmp("overwriteconfig").setDisabled(window.read_only);
        }
        else {
            Ext.getCmp("overwriteconfig").setDisabled(true);
        }
    },

    onFeatureTypeSelect: function (sender, record) {
        Ext.getCmp("featuretypeconfpanel").setHidden(false);

        Ext.getCmp("details_styles").removeAll();

        if (record.data.type === "FeatureType") {
            Ext.getCmp("details_layer_component").setHidden(true);
            Ext.getCmp("details_layer").setHidden(true);
            Ext.getCmp("details_style_component").setHidden(true);
            Ext.getCmp("details_styles").setHidden(true);
            Ext.getCmp("details_jdbc_component").setHidden(false);
            Ext.getCmp("details_jdbc").setHidden(false);
        }
        else if (record.data.type === "RemoteWMSLayer") {
            Ext.getCmp("details_layer_component").setHidden(false);
            Ext.getCmp("details_layer").setHidden(false);
            Ext.getCmp("details_style_component").setHidden(true);
            Ext.getCmp("details_styles").setHidden(true);
            Ext.getCmp("details_jdbc_component").setHidden(true);
            Ext.getCmp("details_jdbc").setHidden(true);
        }
        else {
            Ext.getCmp("details_layer_component").setHidden(false);
            Ext.getCmp("details_layer").setHidden(false);
            Ext.getCmp("details_style_component").setHidden(false);
            Ext.getCmp("details_styles").setHidden(false);
            Ext.getCmp("details_jdbc_component").setHidden(false);
            Ext.getCmp("details_jdbc").setHidden(false);

            const styleButtons = [];

            for (let i = 0; i < record.data.styles.length; i++) {
                const button = Ext.create({
                    xtype: "button",
                    margin: "0px, 0px, 5px, 0px",
                    id: "details_style_" + i,
                    text: record.data.styles[i].styleId,
                    listeners: {
                        click: "onShowConf"
                    }
                });

                styleButtons.push(button);
            }

            const stylePanel = Ext.create({
                xtype: "panel",
                items: styleButtons
            });

            Ext.getCmp("details_styles").add(stylePanel);
        }
    },

    onFeatureTypeSelectGeoserver: function (sender, record) {
        Ext.getCmp("featuretypeconfpanelGeoserver").setHidden(false);

        if (record.data.type === "Geoserver") {
            Ext.getCmp("details_layer_Geoserver").setHidden(false);
            Ext.getCmp("details_featuretype_Geoserver").setHidden(false);

            if (record.data.styleXMLRaw === null || record.data.styleSLDRaw === null) {
                Ext.getCmp("details_style_component_Geoserver").setHidden(true);
                Ext.getCmp("details_style_Geoserver").setHidden(true);
                Ext.getCmp("details_sld_component_Geoserver").setHidden(true);
                Ext.getCmp("details_sld_Geoserver").setHidden(true);
            }
            else {
                Ext.getCmp("details_style_component_Geoserver").setHidden(false);
                Ext.getCmp("details_style_Geoserver").setHidden(false);
                Ext.getCmp("details_sld_component_Geoserver").setHidden(false);
                Ext.getCmp("details_sld_Geoserver").setHidden(false);
            }
        }
        else {
            Ext.getCmp("details_layer_Geoserver").setHidden(true);
            Ext.getCmp("details_featuretype_Geoserver").setHidden(true);
        }
    },

    onChangeWorkspace: function () {
        const serviceElSelection = Ext.getCmp("generateconfigservices").items.items;
        let emptyCombos = false;

        for (const serviceEl of serviceElSelection) {
            Ext.getCmp("workspacechoosercombo_" + serviceEl.name).getSubmitValue();

            if (Ext.getCmp("workspacechoosercombo_" + serviceEl.name).getSubmitValue() === "") {
                emptyCombos = true;
            }
        }

        Ext.getCmp("workspacechooserdeploydevbutton").setDisabled(emptyCombos);
        Ext.getCmp("workspacechooserdeploystagebutton").setDisabled(emptyCombos);
        Ext.getCmp("workspacechoosergeneratebutton").setDisabled(emptyCombos);
    },

    onBeforeDeploy: function (btn) {
        UDPManager.app.getController("UDPManager.controller.PublicFunctions").openWorkspaceChooserWindow(btn);
    },

    onGenerateConfig: function () {
        if (Ext.getCmp("workspacechooser-window")) {
            Ext.getCmp("workspacechooser-window").hide();
        }

        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const serviceElSelection = Ext.getCmp("generateconfigservices").items.items;
        const servicesToGenerate = {};
        const softwareConfig = UDPManager.Configs.getSoftwareConfig();
        const me = this;

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("mainviewport");

        for (const serviceEl of serviceElSelection) {
            const service = Ext.getStore("ServicesDataset").findRecord("id", parseInt(serviceEl.name), 0, false, false, true);
            const instances = softwareConfig[service.data.software].dev.instances;

            if (serviceEl.id.includes("generateconfigservices") && serviceEl.checked) {
                let targetWorkspace = Ext.getCmp("workspacechoosercombo_" + serviceEl.name).getSubmitValue();

                if (targetWorkspace || instances.length === 0) {
                    const destServer = UDPManager.Configs.getDefaultServer(service.data.software).dev;
                    let srcWorkspace = service.data.folder;

                    if (service.data.software === "MapServer" || targetWorkspace === "keine Auswahl notwendig") {
                        targetWorkspace = null;
                        srcWorkspace = null;
                    }

                    const new_url_int = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlInt(service.data.software, targetWorkspace, destServer, service.data.name, "dev", service.data.type);
                    // let new_url_ext = service.data.url_ext;

                    // if (!service.data.status_prod) {
                    //     new_url_ext = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlExt(service.data.software, targetWorkspace, destServer, service.data.name, "dev");
                    // }

                    servicesToGenerate[service.data.id] = {
                        service_id: service.data.id,
                        title: service.data.title,
                        status_dev: service.data.status_dev,
                        status_stage: service.data.status_stage,
                        status_prod: service.data.status_prod,
                        software: service.data.software,
                        srcWorkspace: srcWorkspace,
                        targetWorkspace: targetWorkspace,
                        newUrlInt: new_url_int,
                        newUrlExt: service.data.url_ext,
                        server: destServer,
                        dataset_id: parseInt(dataset_id),
                        last_edited_by: window.auth_user,
                        disable_config_module: service.data.disable_config_module,
                        disable_config_management: service.data.disable_config_management
                    };
                }
            }
        }

        Ext.Ajax.request({
            url: "backend/generateconfig",
            method: "POST",
            headers: {token: window.apiToken},
            timeout: 120000,
            jsonData: servicesToGenerate,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                for (const id in response_json.services) {
                    const respService = response_json.services[id];

                    if (respService.status === "success") {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast(`Schnittstellenkonfigurationen für ID ${id} erfolgreich generiert!`);

                        if (respService.configsEdited) {
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast(`Schnittstellenkonfigurationen für ID ${id} teilweise manuell editiert!`, true);
                        }
                    }
                    else if (respService.status === "skipped") {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast(`Schnittstellenkonfigurationen für ID ${id} deaktiviert!`, true);
                    }
                    else {
                        Ext.Msg.alert("Fehler", `Die Schnittstellenkonfiguration für ID ${id}  konnte nicht generiert werden! <br> ${respService.message}`);
                    }
                }

                me.reloadServicesAndSelect(dataset_id, service_id);

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            },
            failure: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                Ext.Msg.alert("Fehler", "Die Schnittstellenkonfiguration konnte nicht generiert werden!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onDeployDev: function () {
        Ext.getCmp("workspacechooser-window").hide();

        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const serviceElSelection = Ext.getCmp("generateconfigservices").items.items;
        const servicesToDeploy = {};
        const src = "stage";
        const dest = "dev";
        const me = this;

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("mainviewport");

        for (const serviceEl of serviceElSelection) {
            if (serviceEl.id.includes("generateconfigservices") && serviceEl.checked) {
                const service = Ext.getStore("ServicesDataset").findRecord("id", parseInt(serviceEl.name), 0, false, false, true);
                const softwareConfig = UDPManager.Configs.getSoftwareConfig();
                const destServer = UDPManager.Configs.getDefaultServer(service.data.software).dev;
                const instances = softwareConfig[service.data.software][dest].instances;
                let srcWorkspace = service.data.folder;
                let targetWorkspace = Ext.getCmp("workspacechoosercombo_" + serviceEl.name).getSubmitValue();

                if (instances.length === 0) {
                    targetWorkspace = null;
                    srcWorkspace = null;
                }

                const new_url_int = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlInt(service.data.software, targetWorkspace, destServer, service.data.name, dest, service.data.type);
                let new_url_ext = service.data.url_ext;

                if (softwareConfig[service.data.software].dev.urlExtTemplate !== softwareConfig[service.data.software].stage.urlExtTemplate) {
                    new_url_ext = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlExt(service.data.software, targetWorkspace, destServer, service.data.name, dest, service.data.type);
                }

                servicesToDeploy[service.data.id] = {
                    service_id: service.data.id,
                    title: service.data.title,
                    src: src,
                    dest: dest,
                    status_dev: service.data.status_dev,
                    status_stage: service.data.status_stage,
                    status_prod: service.data.status_prod,
                    software: service.data.software,
                    srcWorkspace: srcWorkspace,
                    targetWorkspace: targetWorkspace,
                    newUrlInt: new_url_int,
                    newUrlExt: new_url_ext,
                    server: destServer,
                    disable_config_management: service.data.disable_config_management,
                    dataset_id: parseInt(dataset_id),
                    last_edited_by: window.auth_user
                };
            }
        }

        Ext.Ajax.request({
            url: "backend/deployserviceconfig",
            method: "POST",
            headers: {token: window.apiToken},
            timeout: 120000,
            jsonData: servicesToDeploy,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                for (const id in response_json.services) {
                    const respService = response_json.services[id];

                    if (respService.status === "success") {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast(`Schnittstellenkonfigurationen für ID ${id} erfolgreich verschoben!`);
                    }
                    else {
                        Ext.Msg.alert("Fehler", `Die Schnittstellenkonfiguration für ID ${id}  konnte nicht verschoben werden! <br> ${respService.message}`);
                    }
                }

                me.reloadServicesAndSelect(dataset_id, service_id);

                Ext.getCmp("deploystageconfigbutton").setDisabled(false);
                Ext.getCmp("deployprodconfigbutton").setDisabled(true);
                Ext.getCmp("datasetstatus").setValue(response_json.newStatus);

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsWritable();

                // set dataset status field, no need for store reload
                Ext.getCmp("datasetstatus").setValue("veröffentlicht - in Bearbeitung");

                Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetOrangeIconGradient\" data-qtip=\"veröffentlicht - in Bearbeitung\"></div>");

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Datensatz bereit zur Bearbeitung!", false);

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            },
            failure: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                Ext.Msg.alert("Fehler", "Die Schnittstellenkonfiguration konnte nicht verschoben werden!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onDeployStage: function () {
        Ext.getCmp("workspacechooser-window").hide();

        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const serviceElSelection = Ext.getCmp("generateconfigservices").items.items;
        const servicesToDeploy = {};
        const src = "dev";
        const dest = "stage";
        const me = this;

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("mainviewport");

        for (const serviceEl of serviceElSelection) {
            if (serviceEl.id.includes("generateconfigservices") && serviceEl.checked) {
                const service = Ext.getStore("ServicesDataset").findRecord("id", parseInt(serviceEl.name), 0, false, false, true);
                const softwareConfig = UDPManager.Configs.getSoftwareConfig();
                const instances = softwareConfig[service.data.software][dest].instances;
                let destServer = UDPManager.Configs.getDefaultServer(service.data.software).stage;
                let targetWorkspace = Ext.getCmp("workspacechoosercombo_" + serviceEl.name).getSubmitValue();
                let srcWorkspace = service.data.folder;

                if (targetWorkspace === "keine Auswahl notwendig") {
                    targetWorkspace = null;
                }

                // LGV Workaround
                // --------------------------------------------------------------------------
                if (destServer === "wfalgqa004" && service.data.software === "deegree") {
                    if (targetWorkspace === "fachdaten_a_b" || targetWorkspace === "fachdaten_c_g" || targetWorkspace === "fachdaten_h_m" || targetWorkspace === "fachdaten_n_s" || targetWorkspace === "fachdaten_t_z" || targetWorkspace === "fachdaten_mrh") {
                        destServer = "lgvfds-container-stage";
                    }
                }
                // --------------------------------------------------------------------------

                if (instances.length === 0) {
                    targetWorkspace = null;
                    srcWorkspace = null;
                }

                const new_url_int = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlInt(service.data.software, targetWorkspace, destServer, service.data.name, dest, service.data.type);
                let new_url_ext = service.data.url_ext;

                if (!service.data.status_prod || (softwareConfig[service.data.software].dev.urlExtTemplate !== softwareConfig[service.data.software].stage.urlExtTemplate)) {
                    if (service.data.security_type === "AD SSO") {
                        new_url_ext = UDPManager.Configs.getSecurityProxyUrlSso().stage + service.data.url_sec;
                    }
                    else {
                        new_url_ext = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlExt(service.data.software, targetWorkspace, destServer, service.data.name, dest, service.data.type);
                    }
                }

                servicesToDeploy[service.data.id] = {
                    service_id: service.data.id,
                    title: service.data.title,
                    src: src,
                    dest: dest,
                    status_dev: service.data.status_dev,
                    status_stage: service.data.status_stage,
                    status_prod: service.data.status_prod,
                    software: service.data.software,
                    srcWorkspace: srcWorkspace,
                    targetWorkspace: targetWorkspace,
                    disable_config_management: service.data.disable_config_management,
                    newUrlInt: new_url_int,
                    newUrlExt: new_url_ext,
                    server: destServer,
                    dataset_id: parseInt(dataset_id),
                    last_edited_by: window.auth_user
                };
            }
        }

        Ext.Ajax.request({
            url: "backend/deployserviceconfig",
            method: "POST",
            headers: {token: window.apiToken},
            timeout: 120000,
            jsonData: servicesToDeploy,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                for (const id in response_json.services) {
                    const respService = response_json.services[id];

                    if (respService.status === "success") {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast(`Schnittstellenkonfigurationen für ID ${id} erfolgreich verschoben!`);
                    }
                    else {
                        Ext.Msg.alert("Fehler", `Die Schnittstellenkonfiguration für ID ${id}  konnte nicht verschoben werden! <br> ${respService.message}`);
                    }
                }

                me.reloadServicesAndSelect(dataset_id, service_id);

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsReadOnly();

                Ext.getCmp("deployprodconfigbutton").setDisabled(false);
                Ext.getCmp("startdatasetediting").setDisabled(false);

                Ext.getCmp("datasetstatus").setValue(response_json.newStatus);

                if (response_json.newStatus === "veröffentlicht") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetStatusIcon\" data-qtip=\"veröffentlicht\"></div>");
                }
                else if (response_json.newStatus === "veröffentlicht - vorveröffentlicht") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetYellowIconGradient\" data-qtip=\"veröffentlicht - vorveröffentlicht\"></div>");
                }
                else if (response_json.newStatus === "veröffentlicht - in Bearbeitung") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetOrangeIconGradient\" data-qtip=\"veröffentlicht - in Bearbeitung\"></div>");
                }
                else if (response_json.newStatus === "vorveröffentlicht") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetYellowIcon\" data-qtip=\"vorveröffentlicht\"></div>");
                }
                else {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetOrangeIcon\" data-qtip=\"in Bearbeitung\"></div>");
                }

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            },
            failure: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                Ext.Msg.alert("Fehler", "Die Schnittstellenkonfiguration konnte nicht verschoben werden!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onDeployProd: function () {
        Ext.getCmp("workspacechooser-window").hide();

        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const serviceElSelection = Ext.getCmp("generateconfigservices").items.items;
        const servicesToDeploy = {};
        const src = "stage";
        const dest = "prod";
        const me = this;

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("mainviewport");

        for (const serviceEl of serviceElSelection) {
            const service = Ext.getStore("ServicesDataset").findRecord("id", parseInt(serviceEl.name), 0, false, false, true);
            const softwareConfig = UDPManager.Configs.getSoftwareConfig();
            const instances = softwareConfig[service.data.software][dest].instances;
            let destServer = UDPManager.Configs.getDefaultServer(service.data.software).prod;
            let srcWorkspace = service.data.folder;
            let targetWorkspace = service.data.folder;

            // LGV Workaround
            // --------------------------------------------------------------------------
            if (destServer === "lgvfds03" && service.data.software === "deegree") {
                if (targetWorkspace === "fachdaten_a_b" || targetWorkspace === "fachdaten_c_g" || targetWorkspace === "fachdaten_h_m" || targetWorkspace === "fachdaten_n_s" || targetWorkspace === "fachdaten_t_z" || targetWorkspace === "fachdaten_mrh") {
                    destServer = "lgvfds-container-prod";
                }
            }
            // --------------------------------------------------------------------------

            if (instances.length === 0) {
                targetWorkspace = null;
                srcWorkspace = null;
            }

            const new_url_int = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlInt(service.data.software, targetWorkspace, destServer, service.data.name, dest, service.data.type);
            let new_url_ext = service.data.url_ext_prod;

            if (!service.data.status_prod) {
                if (service.data.security_type === "AD SSO") {
                    new_url_ext = UDPManager.Configs.getSecurityProxyUrlSso().prod + service.data.url_sec;
                }
                else {
                    new_url_ext = UDPManager.app.getController("UDPManager.controller.PublicFunctions").generateUrlExt(service.data.software, targetWorkspace, destServer, service.data.name, dest, service.data.type);
                }
            }

            servicesToDeploy[service.data.id] = {
                service_id: service.data.id,
                title: service.data.title,
                src: src,
                dest: dest,
                status_dev: service.data.status_dev,
                status_stage: service.data.status_stage,
                status_prod: service.data.status_prod,
                software: service.data.software,
                srcWorkspace: srcWorkspace,
                targetWorkspace: targetWorkspace,
                disable_config_management: service.data.disable_config_management,
                newUrlInt: new_url_int,
                newUrlExt: new_url_ext,
                server: destServer,
                dataset_id: parseInt(dataset_id),
                last_edited_by: window.auth_user
            };
        }

        Ext.Ajax.request({
            url: "backend/deployserviceconfig",
            method: "POST",
            headers: {token: window.apiToken},
            timeout: 120000,
            jsonData: servicesToDeploy,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                for (const id in response_json.services) {
                    const respService = response_json.services[id];

                    if (respService.status === "success") {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast(`Schnittstellenkonfigurationen für ID ${id} erfolgreich verschoben!`);
                    }
                    else {
                        Ext.Msg.alert("Fehler", `Die Schnittstellenkonfiguration für ID ${id}  konnte nicht verschoben werden! <br> ${respService.message}`);
                    }
                }

                me.reloadServicesAndSelect(dataset_id, service_id);

                if (response_json.newStatus === "veröffentlicht") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsReadOnly();
                    Ext.getCmp("deployprodconfigbutton").setDisabled(true);
                    Ext.getCmp("undeployprodconfigbutton").setDisabled(false);
                }

                Ext.getCmp("datasetstatus").setValue(response_json.newStatus);

                if (response_json.newStatus === "veröffentlicht") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetStatusIcon\" data-qtip=\"veröffentlicht\"></div>");
                }
                else if (response_json.newStatus === "veröffentlicht - vorveröffentlicht") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetYellowIconGradient\" data-qtip=\"veröffentlicht - vorveröffentlicht\"></div>");
                }
                else if (response_json.newStatus === "veröffentlicht - in Bearbeitung") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetOrangeIconGradient\" data-qtip=\"veröffentlicht - in Bearbeitung\"></div>");
                }
                else if (response_json.newStatus === "vorveröffentlicht") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetYellowIcon\" data-qtip=\"vorveröffentlicht\"></div>");
                }
                else {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetOrangeIcon\" data-qtip=\"in Bearbeitung\"></div>");
                }

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            },
            failure: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                Ext.Msg.alert("Fehler", "Die Schnittstellenkonfiguration konnte nicht verschoben werden!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onUnDeploy: function () {
        if (Ext.getCmp("workspacechooser-window")) {
            Ext.getCmp("workspacechooser-window").hide();
        }

        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const serviceElSelection = Ext.getCmp("generateconfigservices").items.items;
        const servicesToUnDeploy = {};
        const me = this;

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("mainviewport");

        for (const serviceEl of serviceElSelection) {
            if (serviceEl.id.includes("generateconfigservices") && serviceEl.checked) {
                const service = Ext.getStore("ServicesDataset").findRecord("id", parseInt(serviceEl.name), 0, false, false, true);

                servicesToUnDeploy[service.data.id] = {
                    service_id: service.data.id,
                    title: service.data.title,
                    status_dev: service.data.status_dev,
                    status_stage: service.data.status_stage,
                    status_prod: service.data.status_prod,
                    software: service.data.software,
                    dataset_id: parseInt(dataset_id),
                    last_edited_by: window.auth_user
                };
            }
        }

        Ext.Ajax.request({
            url: "backend/undeployserviceconfig",
            method: "POST",
            headers: {token: window.apiToken},
            timeout: 120000,
            jsonData: servicesToUnDeploy,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                for (const id in response_json.services) {
                    const respService = response_json.services[id];

                    if (respService.status === "success") {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast(`Veröffentlichung der Schnittstellenkonfigurationen für ID ${id} zurück gezogen!`);
                    }
                    else {
                        Ext.Msg.alert("Fehler", `Veröffentlichung der Schnittstellenkonfigurationen für ID ${id}  konnte nicht zurück gezogen werden! <br> ${respService.message}`);
                    }
                }

                me.reloadServicesAndSelect(dataset_id, service_id);

                Ext.getCmp("datasetstatus").setValue(response_json.newStatus);

                if (response_json.newStatus === "veröffentlicht") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetStatusIcon\" data-qtip=\"veröffentlicht\"></div>");
                }
                else if (response_json.newStatus === "veröffentlicht - vorveröffentlicht") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetYellowIconGradient\" data-qtip=\"veröffentlicht - vorveröffentlicht\"></div>");
                }
                else if (response_json.newStatus === "veröffentlicht - in Bearbeitung") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetOrangeIconGradient\" data-qtip=\"veröffentlicht - in Bearbeitung\"></div>");
                }
                else if (response_json.newStatus === "vorveröffentlicht") {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetYellowIcon\" data-qtip=\"vorveröffentlicht\"></div>");
                }
                else {
                    Ext.getCmp("datasetstatusicon").setHtml("<div class=\"x-fa fa-globe-africa datasetOrangeIcon\" data-qtip=\"in Bearbeitung\"></div>");
                }

                Ext.getCmp("undeployprodconfigbutton").setDisabled(true);

                if (response_json.newStatus === "vorveröffentlicht") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsReadOnly();
                    Ext.getCmp("deployprodconfigbutton").setDisabled(false);
                }

                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            },
            failure: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                Ext.Msg.alert("Fehler", "Veröffentlichung der Schnittstellenkonfigurationen Schnittstellenkonfiguration konnte nicht zurück gezogen werden!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onOverwriteConfig: function () {
        const editor = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditor();
        const config = editor.getValue();
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("configview-window");

        Ext.Ajax.request({
            url: "backend/overwriteserviceconfig",
            method: "POST",
            headers: {token: window.apiToken},
            timeout: 120000,
            jsonData: {
                id: service_id,
                title: service.data.title,
                status_dev: service.data.status_dev,
                status_stage: service.data.status_stage,
                status_prod: service.data.status_prod,
                software: service.data.software,
                folder: service.data.folder,
                config: config,
                uri: editor.configUri,
                dataset_id: dataset_id,
                last_edited_by: window.auth_user
            },
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Dienstkonfiguration gespeichert!");

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").getServiceConfig(service.data, "dev");

                    if (service.data.software === "deegree") {
                        Ext.getCmp("featuretypeconfpanel").setHidden(true);
                    }
                    if (service.data.software === "GeoServer") {
                        Ext.getCmp("featuretypeconfpanelGeoserver").setHidden(true);
                    }

                    Ext.getStore("ServiceConfigsEdited").load({
                        params: {service_id: service.data.id},
                        callback: function (records) {
                            if (records.length > 0) {
                                Ext.getCmp("serviceconfigeditedcontainer").setCollapsed(false);
                            }
                            else {
                                Ext.getCmp("serviceconfigeditedcontainer").setCollapsed(true);
                            }
                        }
                    });
                }
                else {
                    Ext.Msg.alert("Fehler", "Die Dienstkonfiguration konnte nicht gespeichert werden! <br>" + response_json.message);
                }
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            },
            failure: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                Ext.Msg.alert("Fehler", "Die Dienstkonfiguration konnte nicht gespeichert werden!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onDeleteServiceConfigEdited: function (view, recIndex, cellIndex, item, e, record) {
        Ext.Ajax.request({
            url: "backend/deleteserviceconfigedited",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                id: record.data.id
            },
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.error) {
                    Ext.Msg.alert("Fehler", "Der Eintrag konnte nicht gelöscht werden! <br>" + response_json.message);
                }
                else {
                    record.drop();
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Fehler", "Der Eintrag konnte nicht gelöscht werden!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onWorkspaceValidate: function () {
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);
        const status = Ext.getCmp("statusspecificconfigicondeegree").env;

        Ext.getStore("ValidationResults").removeAll();

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("mainviewport");

        Ext.Ajax.request({
            url: "backend/startworkspacevalidate",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                url: service.data.url_int,
                url_prod: service.data.url_int_prod,
                folder: service.data.folder,
                folder_prod: service.data.folder_prod,
                status: status
            },
            timeout: 120000,
            success: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "error") {
                    Ext.Msg.alert("Fehler", "Validierung konnte nicht ausgeführt werden! " + response_json.message);
                }
                else {
                    if (response_json.data.length > 0) {
                        let validationResults = Ext.getCmp("validation-window");

                        if (validationResults) {
                            validationResults.destroy();
                        }

                        validationResults = Ext.create("configuration.ValidationWindow");
                        validationResults.show();

                        Ext.getStore("ValidationResults").add(response_json.data);
                    }
                    else {
                        Ext.Msg.alert("Erfolg", "Workspace valide!");
                    }
                }
            },
            failure: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                Ext.Msg.alert("Fehler", "Validierung fehlgeschlagen!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onWorkspaceRestart: function (btn) {
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);
        let url = "backend/startworkspacerestart";
        let status = Ext.getCmp("statusspecificconfigicondeegree").env;

        if (btn.id === "geoserverrestartbutton") {
            url = "backend/startgeoserverworkspacerestart";
            status = Ext.getCmp("statusspecificconfigicongeoserver").env;
        }
        else if (btn.id === "ldproxyrestartbutton") {
            url = "backend/startldproxyworkspacerestart";
            status = Ext.getCmp("statusspecificconfigiconldproxy").env;
        }

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("mainviewport");

        Ext.Ajax.request({
            url: url,
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                name: service.data.name,
                server: service.data.server,
                server_prod: service.data.server_prod,
                folder: service.data.folder,
                folder_prod: service.data.folder_prod,
                status: status
            },
            timeout: 120000,
            success: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "error") {
                    Ext.Msg.alert("Fehler", "Neustart konnte nicht ausgeführt werden! " + response_json.message);
                }
                else {
                    Ext.Msg.alert("Erfolg", "Workspace neu geladen!");
                }
            },
            failure: function (response) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                Ext.Msg.alert("Fehler", "Neustart fehlgeschlagen!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onAddNewFile: function (btn) {
        let fileNameInputWindow = Ext.getCmp("filenameinput-window");

        if (fileNameInputWindow) {
            fileNameInputWindow.destroy();
        }

        fileNameInputWindow = Ext.create("datasetservices.filenameinput");

        if (btn.id === "addnewmapservertemplate") {
            Ext.getCmp("mapservertemplatecreatebutton").setHidden(false);
        }
        else {
            Ext.getCmp("nestedmapfilecreatebutton").setHidden(false);
        }

        fileNameInputWindow.show();
    },

    onDeleteMapServerFile: function (view, recIndex, cellIndex, item, e, record) {
        const mb = Ext.MessageBox;

        mb.buttonText.yes = "ja";
        mb.buttonText.no = "nein";

        mb.confirm("Löschen", "Datei wirklich löschen?", function (btn) {
            if (btn === "yes") {
                const service_id = Ext.getCmp("serviceid").getSubmitValue();
                const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);

                Ext.Ajax.request({
                    url: "backend/deletemapserverfile",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        name: record.data.name,
                        uri: record.data.uri,
                        service_id: service_id,
                        status_dev: service.data.status_dev,
                        status_stage: service.data.status_stage,
                        status_prod: service.data.status_prod
                    },
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);

                        if (response_json.status === "error") {
                            Ext.Msg.alert("Fehler", "Die Datei konnte nicht gelöscht werden! <br>" + response_json.message);
                        }
                        else {
                            record.drop();

                            Ext.getStore("ServiceConfigsEdited").load({
                                params: {service_id: service_id},
                                callback: function (records) {
                                    if (records.length > 0) {
                                        Ext.getCmp("serviceconfigeditedcontainer").setCollapsed(false);
                                    }
                                    else {
                                        Ext.getCmp("serviceconfigeditedcontainer").setCollapsed(true);
                                    }
                                }
                            });
                        }
                    },
                    failure: function (response) {
                        Ext.Msg.alert("Fehler", "Die Datei konnte nicht gelöscht werden!");
                        console.log(Ext.decode(response.responseText));
                    }
                });
            }
        });
    },

    onCreateFile: function (btn) {
        const service_id = Ext.getCmp("serviceid").getSubmitValue();
        const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);
        const filename = Ext.getCmp("nametextfield").getSubmitValue();

        if (filename.indexOf(".") === -1) {
            Ext.Msg.alert("Hinweis", "Der Dateiname muss mit Dateiendung eingegeben werden!");
        }
        else {
            Ext.getCmp("filenameinput-window").close();

            let path = "";

            if (btn.id === "nestedmapfilecreatebutton") {
                path = `/${service.data.name}/nested_mapfiles`;
            }
            else {
                path = `/${service.data.name}/templates`;
            }

            Ext.Ajax.request({
                url: "backend/createconfigfile",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    service_id: service_id,
                    filename: filename,
                    path: path,
                    software: "MapServer"
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "error") {
                        Ext.Msg.alert("Fehler", "Datei konnte nicht erzeugt werden! " + response_json.message);
                    }
                    else {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").getServiceConfig(service.data, "dev");
                    }
                },
                failure: function (response) {
                    Ext.Msg.alert("Fehler", "Datei konnte nicht erzeugt werden!");
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    onSelectServiceConfig: function (btn) {
        Ext.getCmp("featuretypeconfpanel").setHidden(true);

        const service = Ext.getCmp("grid_datasetservices").getSelectionModel().getSelection()[0];
        const status = btn.text;

        if ((service.data.software === "deegree" || service.data.software === "GeoServer" || service.data.software === "ldproxy" || service.data.software === "MapServer") && service.data.id !== 0) {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").getServiceConfig(service.data, status);
        }

        if (status === "dev") {
            Ext.getCmp("serviceconfigeditedcontainer").setHidden(false);
        }
        else {
            Ext.getCmp("serviceconfigeditedcontainer").setHidden(true);
        }
    },

    reloadServicesAndSelect: function (dataset_id, service_id) {
        Ext.getStore("ServicesDataset").load({
            params: {dataset_id: dataset_id},
            callback: function () {
                const service = Ext.getStore("ServicesDataset").findRecord("id", parseInt(service_id), 0, false, false, true);

                Ext.getCmp("grid_datasetservices").getSelectionModel().deselect(service);
                Ext.getCmp("grid_datasetservices").getSelectionModel().select(service);
            }
        });
    }
});
