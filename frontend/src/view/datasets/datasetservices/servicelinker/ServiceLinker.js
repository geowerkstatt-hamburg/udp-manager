Ext.define("UDPManager.view.datasets.datasetservices.servicelinker.ServiceLinker", {
    extend: "Ext.window.Window",
    id: "servicelinker-window",
    alias: "servicelinker.ServiceLinker",
    height: 125,
    width: 480,
    title: "Schnittstellen verknüpfen",
    resizable: false,
    closeAction: "hide",
    controller: "servicelinker",
    items: [
        {
            xtype: "panel",
            bodyPadding: 10,
            items: [
                {
                    xtype: "combo",
                    allowBlank: false,
                    id: "linkservicecombo",
                    store: "Services",
                    queryMode: "local",
                    displayField: "title",
                    valueField: "id",
                    width: 460
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    "->",
                    {
                        id: "saveservicelink",
                        text: "Verknüpfen",
                        listeners: {
                            click: "onSaveServiceLink"
                        }
                    }
                ]
            }]
        }
    ]
});

