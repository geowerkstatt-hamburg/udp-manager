Ext.define("UDPManager.view.datasets.datasetservices.servicelinker.ServiceLinkerController", {
    extend: "Ext.app.ViewController",

    alias: "controller.servicelinker",

    onSaveServiceLink: function (btn) {
        const service_id = Ext.getCmp("linkservicecombo").getValue();
        const dataset_id = Ext.getCmp("datasetid").getValue();

        btn.setDisabled(true);

        Ext.Ajax.request({
            url: "backend/addservicelink",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                service_id: service_id,
                dataset_id: dataset_id,
                last_edited_by: window.auth_user
            },
            success: function (response) {
                const results = Ext.decode(response.responseText);

                btn.setDisabled(false);

                if (!results.error) {
                    Ext.getStore("ServicesDataset").load({
                        params: {dataset_id: dataset_id}
                    });
                }
                else {
                    Ext.MessageBox.alert("Fehler", "Schnittstelle konnte nicht verknüpft werden");
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Schnittstelle konnte nicht verknüpft werden");
                btn.setDisabled(false);
            }
        });
    }
});
