Ext.define("UDPManager.view.datasets.DatasetTabPanel", {
    extend: "Ext.tab.Panel",
    xtype: "datasettabpanel",
    alias: "widget.datasettabpanel",
    controller: "datasettabpanel",
    tabBarHeaderPosition: 1,
    activeTab: 0,
    tabBar: {
        defaults: {
            padding: "9 12 9 12"
        }
    },
    listeners: {
        added: "checkJiraTicketsModuleStatus",
        tabchange: "onTabChange"
    },
    items: [
        {
            xtype: "dataset_details",
            title: "Datensatz-Details",
            id: "datasetdetailstab"
        },
        {
            xtype: "dataset_collections",
            title: "Collections",
            id: "datasetcollectionstab"
        },
        {
            xtype: "dataset_services",
            title: "Schnittstellen",
            id: "datasetservicestab"
        },
        {
            xtype: "dataset_etldetails",
            title: "ETL Prozesse",
            id: "datasetetltab"
        },
        {
            xtype: "dataset_comments",
            title: "Kommentare",
            id: "datasetcommentstab"
        },
        {
            xtype: "dataset_jira",
            title: "JIRA",
            id: "datasetjiratab"
        },
        {
            xtype: "dataset_changelog",
            title: "Changelog"
        }
    ]
});
