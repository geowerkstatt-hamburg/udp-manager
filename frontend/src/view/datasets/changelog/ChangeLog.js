Ext.define("UDPManager.view.datasets.changelog.ChangeLog", {
    extend: "Ext.container.Container",
    xtype: "dataset_changelog",
    bodyPadding: "5 5 5 5",
    style: "border-top: 1px solid #cfcfcf !important;",
    items: [
        {
            xtype: "grid",
            store: "ChangeLog",
            autoScroll: true,
            height: Ext.Element.getViewportHeight() - 108,
            id: "changelog-grid",
            border: false,
            viewConfig: {
                enableTextSelection: true,
                stripeRows: false
            },
            columnLines: true,
            selModel: "rowmodel",
            columns: [
                {
                    text: "Bereich",
                    dataIndex: "entity",
                    width: 80
                },
                {
                    text: "Bearbeiter",
                    dataIndex: "username",
                    width: 170
                },
                {
                    text: "Zeitstempel",
                    dataIndex: "change_date",
                    width: 150
                },
                {
                    text: "Details",
                    dataIndex: "change_message",
                    flex: 1
                }
            ]
        }
    ]
});
