Ext.define("UDPManager.view.datasets.jira.SearchJiraTicketWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.searchjiraticket-controller",

    onAddIssue: function (grid, rowIndex) {
        const record = grid.getStore().getAt(rowIndex);
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();

        let create_date = new Date(record.data.create_date.split("T")[0]);

        create_date = Ext.Date.format(create_date, "d.m.Y");

        Ext.Ajax.request({
            url: "backend/addjiraticketsdataset",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                id: record.data.id,
                dataset_id: dataset_id,
                key: record.data.key,
                creator: record.data.creator,
                summary: record.data.summary,
                assignee: record.data.assignee,
                project: record.data.project,
                last_edited_by: window.auth_user,
                create_date: create_date
            },
            success: function (response) {
                if (response.responseText === "") {
                    Ext.Msg.alert("Jira Ticket", "Fehler: Jira Ticket bereits verknüpft?", Ext.emptyFn);
                }
                else {
                    Ext.getCmp("grid_searchjiraticket").getStore().remove(record);
                    Ext.Msg.alert("Jira Ticket", "Das Jira Ticket wurde erfolgreich verknüpft.", Ext.emptyFn);
                    Ext.getStore("JiraTickets").load({
                        params: {dataset_id: dataset_id}
                    });
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Jira Ticket", "Leider ist ein Fehler aufgetreten: " + response.responseText, Ext.emptyFn);
                console.log(Ext.decode(response.responseText));
            }
        });
    }
});
