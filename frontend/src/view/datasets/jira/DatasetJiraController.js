Ext.define("UDPManager.view.datasets.jira.DatasetList_JiraController", {
    extend: "Ext.app.ViewController",
    alias: "controller.dataset_jira",

    onDeleteIssue: function (grid, rowIndex) {
        const record = grid.getStore().getAt(rowIndex);
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();

        Ext.Ajax.request({
            url: "backend/deletejiraticketdataset",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                key: record.data.key,
                dataset_id: dataset_id,
                last_edited_by: window.auth_user
            },
            success: function (response) {
                if (response.responseText === "") {
                    Ext.Msg.alert("Jira Ticket", "Fehler: Ist die Verknüpfung bereits entfernt?", Ext.emptyFn);
                }
                else {
                    Ext.Msg.alert("Jira Ticket", "Die Verknüpfung zwischen Datensatz und Jira Ticket wurde erfolgreich entfernt.", Ext.emptyFn);
                    Ext.getStore("JiraTickets").load({
                        params: {dataset_id: dataset_id}
                    });
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Jira Ticket", "Leider ist ein Fehler aufgetreten: " + response.responseText, Ext.emptyFn);
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onKeyPress: function (field, event) {
        if (event.getKey() === event.ENTER) {
            this.onSearchIssue();
        }
    },

    /**
     * Function for searching Jira tickets. The response from the backend contains the issues of the searched Jira projects.
     * If a keyword is passed, the tickets are filtered and displayed listed in a new window.
     * @returns {void}
     */
    onSearchJiraTickets: function () {
        const issueKey = Ext.getCmp("jiraTicketSearchField").getSubmitValue().toLowerCase();
        const me = this;
        const loadingMask = new Ext.LoadMask({target: Ext.getCmp("grid_linkedjiratickets"), msg: "Bitte warten..."});

        loadingMask.show();

        Ext.Ajax.request({
            url: "backend/searchjiratickets",
            headers: {token: window.apiToken},
            jsonData: {
                key: issueKey
            },
            success: function (response) {
                const issues = Ext.decode(response.responseText);

                let searchJiraTicketWindow = Ext.getCmp("searchjiraticket-window");

                if (issues.error) {
                    loadingMask.hide();
                    Ext.MessageBox.alert("Fehler", "Es konnten keine JIRA Tickets geladen werden!");
                }
                else {
                    if (searchJiraTicketWindow) {
                        searchJiraTicketWindow.destroy();
                    }

                    searchJiraTicketWindow = Ext.create("jira.SearchJiraTicketWindow");

                    const grid = Ext.getCmp("grid_searchjiraticket");
                    const jiraticketStore = grid.getStore();

                    jiraticketStore.removeAll();

                    if (issueKey === "") {
                        issues.forEach(function (issue) {
                            me._addIssueToStore(jiraticketStore, issue, issue.fields.project.key);
                        });
                    }
                    else {
                        issues.forEach(function (issue) {
                            if (issue.key.toLowerCase().includes(issueKey) || issue.fields.creator.displayName.toLowerCase().includes(issueKey) || issue.fields.summary.toLowerCase().includes(issueKey)) {
                                me._addIssueToStore(jiraticketStore, issue, issue.fields.project.key);
                            }
                            else {
                                if (issue.fields.assignee) {
                                    if (issue.fields.assignee.displayName.toLowerCase().includes(issueKey)) {
                                        me._addIssueToStore(jiraticketStore, issue, issue.fields.project.key);
                                    }
                                }
                            }
                        });
                    }

                    loadingMask.hide();

                    if (jiraticketStore.getCount() > 0) {
                        searchJiraTicketWindow.show();
                    }
                    else {
                        Ext.Msg.alert("Jira Ticket", "Keine passenden Jira Tickets für die Suchanfrage " + Ext.getCmp("jiraTicketSearchField").getSubmitValue() + " gefunden.", Ext.emptyFn);
                    }
                }
            },
            failure: function (response) {
                loadingMask.hide();
                console.log(response);
                Ext.MessageBox.alert("Fehler", "Es konnten keine JIRA Tickets geladen werden!");
            }
        });
    },

    /**
     * Auxiliary function to populate the store with the searched jira issues.
     * @param {Object} jiraticketStore - store to be filled with the searched issues
     * @param {Object} issue - one of the searched jira issues
     * @param {String} project - jira project the issue belongs
     * @returns {void}
     */
    _addIssueToStore: function (jiraticketStore, issue, project) {
        if (issue.fields.assignee) {
            jiraticketStore.add({id: issue.id, key: issue.key, creator: issue.fields.creator.displayName, summary: issue.fields.summary, assignee: issue.fields.assignee.displayName, project: project, create_date: issue.fields.created});
        }
        else {
            jiraticketStore.add({id: issue.id, key: issue.key, creator: issue.fields.creator.displayName, summary: issue.fields.summary, assignee: issue.fields.assignee, project: project, create_date: issue.fields.created});
        }
    },

    onOpenJiraTicketWindow: function () {
        const service_types = [];
        const service_status = [];
        const dataset_metadata = Ext.getCmp("datasetdetailsform").getForm().getFieldValues().rs_id;
        const no_metadata_link = Ext.getCmp("datasetdetailsform").getForm().getFieldValues().no_catalogue_link;

        let createJiraTicketWindow = Ext.getCmp("createjiraticket-window");

        Ext.getStore("ServicesDataset").data.items.forEach(function (service) {
            service_types.push(service.data.type);
            if (service.data.status !== "stage") {
                service_status.push(service.data.status);
            }
        });

        if (createJiraTicketWindow) {
            createJiraTicketWindow.destroy();
        }

        if (service_types.length === 0) {
            Ext.Msg.show({
                title: "Jira Ticket",
                message: "Keine Schnittstelle mit Datensatz verknüpft <br> Sicher, das ein Ticket erstellt werden soll?",
                buttons: Ext.Msg.YESNO,
                buttonText: {
                    yes: "Ja",
                    no: "Nein"
                },
                icon: Ext.Msg.QUESTION,
                fn: btn => {
                    if (btn === "yes") {
                        createJiraTicketWindow = Ext.create("jira.CreateJiraTicketWindow").show();
                    }
                    else if (btn === "no") {
                        Ext.getCmp("dataset_main_tabs").setActiveTab(2);
                    }
                }
            });
        }
        else if (service_status.length > 0) {
            Ext.Msg.show({
                title: "Jira Ticket",
                message: "Nicht alle Schnittstellen weisen den Status \"stage\" auf. Jira Ticket darf nicht angelegt werden.",
                buttons: Ext.Msg.OK,
                buttonText: {
                    ok: "OK"
                },
                fn: btn => {
                    if (btn === "ok" || btn === "cancel") {
                        Ext.getCmp("dataset_main_tabs").setActiveTab(2);
                    }
                }
            });
        }
        else if ((dataset_metadata === "" || dataset_metadata === undefined) && !no_metadata_link) {
            Ext.Msg.show({
                title: "Jira Ticket",
                message: "Es liegt keine Datensatz-Metadaten-Kopplung vor. Jira Ticket darf nicht angelegt werden.",
                buttons: Ext.Msg.OK,
                buttonText: {
                    ok: "OK"
                },
                fn: btn => {
                    if (btn === "ok" || btn === "cancel") {
                        Ext.getCmp("dataset_main_tabs").setActiveTab(0);
                    }
                }
            });
        }
        else {
            createJiraTicketWindow = Ext.create("jira.CreateJiraTicketWindow").show();
        }
    }
});
