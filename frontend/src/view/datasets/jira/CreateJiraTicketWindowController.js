Ext.define("UDPManager.view.datasets.jira.CreateJiraTicketWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.createjiraticket-controller",

    /**
     * Function to generate the sercivetype checkboxes automatically according, which servicetypes are in the service store of the dataset.
     * @returns {void}
     */
    onGetServiceTypes: function () {
        const service_types = [];
        const service_types_checkbox = Ext.getCmp("createjiraticketservicetype");

        Ext.getStore("ServicesDataset").data.items.forEach(function (service) {
            service_types.push(service.data.type);
        });

        service_types_checkbox.removeAll();
        service_types.forEach(function (servicetype) {
            service_types_checkbox.add({
                boxLabel: servicetype,
                itemId: servicetype.toLowerCase() + "_createjiraticketservicetype",
                name: servicetype.toLowerCase(),
                inputValue: servicetype,
                checked: true
            });
        });
    },

    linkedticketShowHide: function () {
        const jiraproject_records = Ext.getCmp("createjiraticketproject").getSelection();
        const linkedticket = Ext.getCmp("createjiraticketlinkedticket");
        const linkedticket_element = Ext.getCmp("createjiraticketlinkedticket").getEl();
        const jiraproject_labels_element = Ext.getCmp("createjiraticketlabel").getEl();
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();

        linkedticket.setValue("");

        if (jiraproject_records.data.new_as_subtask) {
            linkedticket_element.show();
            jiraproject_labels_element.show();

            Ext.getStore("JiraLinkedTickets").load({
                params: {
                    dataset_id: dataset_id,
                    project: jiraproject_records.data.name
                }
            });
        }
        else {
            linkedticket.setValue("");
            linkedticket_element.hide();
            jiraproject_labels_element.show();
        }
    },

    collapseLinkedTicket: function () {
        Ext.getCmp("createjiraticketlinkedticket").collapse();
    },

    /**
     * Function to control which elements should be displayed in the "Create Jira Ticket" window. Depending on the ticket type.
     * @returns {void}
     */
    tickettypeShowHideElements: function () {
        const me = this;
        const tickettype = Ext.getCmp("createjiratickettype").getValue();
        const components_to_update_element = Ext.getCmp("createjiraticketcomponents").getEl();
        const fme_component_element = Ext.getCmp("createjiraticketfmecomponent").getEl();
        const servicetype_element = Ext.getCmp("createjiraticketservicetype").getEl();
        // const security_element = Ext.getCmp("createjiraticketsecurity").getEl();

        servicetype_element.show();

        if (tickettype === "Daten Aktualisierung") {
            components_to_update_element.show();
            fme_component_element.hide();
            // security_element.show();
            // Ext.getCmp("createjiraticketsecurity").setFieldLabel("Absicherung anpassen");
            me._clearCheckboxesRadioButtons();
        }
        else if (tickettype === "Neue Daten") {
            components_to_update_element.hide();
            fme_component_element.show();
            // security_element.show();
            // Ext.getCmp("createjiraticketsecurity").setFieldLabel("Absicherung einrichten");
            me._clearCheckboxesRadioButtons();
        }
        else if (tickettype === "Löschen") {
            components_to_update_element.hide();
            fme_component_element.hide();
            // security_element.hide();
            me._clearCheckboxesRadioButtons();
        }
    },

    opencollectionspicker: function () {
        let createJiraTicketWindowCollections = Ext.getCmp("createjiraticketcollectionpick-window");

        if (createJiraTicketWindowCollections) {
            createJiraTicketWindowCollections.destroy();
        }

        createJiraTicketWindowCollections = Ext.create("jira.CreateJiraTicketCollectionPickWindow");

        createJiraTicketWindowCollections.show();

        const collection_store = Ext.getStore("CollectionsDataset");

        Ext.getCmp("select-collection-grid").setStore(collection_store);
        Ext.getCmp("select-collection-grid").getView().getSelectionModel().selectAll(true);
    },

    settextstyle: function () {
        var textarea_value = Ext.getCmp("createjiraticketdescription").getValue();

        if (textarea_value === "" || textarea_value) {
            Ext.getCmp("createjiraticketdescription").setFieldStyle({
                "color": "#404040",
                "font-weight": "normal"
            });
        }
    },

    _clearCheckboxesRadioButtons: function () {
        const radio_button_scheduler = Ext.getCmp("createjiraticketfmecomponent").getComponent("createjiraticketfmescheduler");
        const radio_button_automation = Ext.getCmp("createjiraticketfmecomponent").getComponent("createjiraticketfmeautomation");
        const radio_button_nocomponent = Ext.getCmp("createjiraticketfmecomponent").getComponent("createjiraticketnofme");
        const checkboxes_components_to_update = Ext.getCmp("createjiraticketcomponents");

        radio_button_scheduler.setValue(false);
        radio_button_automation.setValue(false);
        radio_button_nocomponent.setValue(false);
        checkboxes_components_to_update.setValue(false);
    },

    /**
     * Function to create a new Jira ticket in a given Jira project. The required informations are collected in the frontend and passed to the backend.
     * After a successful creation of the Jira ticket, informations about it are automatically added to a list in the frontend.
     * @returns {void}
     */
    onCreateJiraTicket: function () {
        const me = this;
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const responsible_party = Ext.getCmp("datasetresponsibleparty").getSubmitValue();
        const linkedticket = Ext.getCmp("createjiraticketlinkedticket").getSubmitValue();
        const tickettype = Ext.getCmp("createjiratickettype").getSubmitValue();
        const duedate = Ext.getCmp("createjiraticketduedate").getSubmitValue();
        const description = Ext.getCmp("createjiraticketdescription").getValue();
        const update_components = Ext.getCmp("createjiraticketcomponents").getValue();
        const fme_component_activate = Ext.getCmp("createjiraticketfmecomponent").getValue();
        const fme_processname = Ext.getCmp("etlprocessfmename").getSubmitValue();
        const fme_host = Ext.getCmp("etlprocessfmehost").getSubmitValue();
        const fme_repo = Ext.getCmp("etlprocessfmefolder").getSubmitValue();
        const fme_location = "Host: *" + fme_host + "*, Repository: *" + fme_repo + "*";
        const jiraproject_records = Ext.getCmp("createjiraticketproject").getSelection();
        const jiraproject_name_to_write = jiraproject_records.data.name;
        const servicetype_checkbox = Ext.getCmp("createjiraticketservicetype").getValue();
        const jiraproject_label = [Ext.getCmp("createjiraticketlabel").getSubmitValue()];
        const update_components_text = me._prepareUpdateComponentsText(update_components, tickettype);
        const fme_text = me._prepareFmeText(fme_component_activate, update_components.fme, tickettype, fme_processname, fme_location);
        const servicetype_text = me._prepareServiceTypeText(servicetype_checkbox, tickettype, update_components);
        const datasource_text = me._prepareDatasourceText();

        let issuetype = "";
        let story_points_value;
        let project_components;
        let update_components_values = [];

        if (jiraproject_records.data.new_as_subtask) {
            issuetype = "Sub-task";
            story_points_value = jiraproject_records.data.story_points_value;
            project_components = jiraproject_records.data.components;
        }
        else {
            issuetype = "Task";
            story_points_value = jiraproject_records.data.story_points_value;
            project_components = jiraproject_records.data.components;
        }

        Object.values(update_components).forEach(array => {
            update_components_values = update_components_values.concat(array);
        });

        if (tickettype.length === 0 || duedate.length === 0) {
            Ext.Msg.alert("Hinweis", "Bitte Felder füllen.");
        }
        else if (tickettype === "Neue Daten" && Ext.getCmp("createjiraticketfmecomponent").allowBlank === false) {
            Ext.Msg.alert("Hinweis", "Bitte einen der Radio Buttons bei 'FME Komponenten aktivieren' auswählen.");
        }
        else if (tickettype === "Neue Daten" && (!fme_processname || !fme_host || !fme_repo) && fme_component_activate.fmecomponent !== "Keine") {
            Ext.Msg.alert("Hinweis", "Felder 'Host', 'Repository' und 'Name' im Reiter ETL-Prozesse sind nicht gefüllt. Bitte diese Felder füllen.");
        }
        else if (tickettype === "Daten Aktualisierung" && (!fme_processname || !fme_location) && (update_components_values.includes("FME Schedule") || update_components_values.includes("FME Automation"))) {
            Ext.Msg.alert("Hinweis", "Felder 'Host', 'Repository' und 'Name' im Reiter ETL-Prozesse sind nicht gefüllt. Bitte diese Felder füllen.");
        }
        else if (tickettype === "Daten Aktualisierung" && (update_components_values.includes("FME Schedule") && update_components_values.includes("FME Automation"))) {
            Ext.Msg.alert("Hinweis", "Bitte nur eine oder keine FME Checkbox auswählen!");
        }
        else if (datasource_text === "") {
            Ext.Msg.alert("Hinweis", "Das Feld 'DB-Verbindung (lesend)' im Reiter 'Datensatz-Details' ist nicht gefüllt. Bitte eine lesende Datenbankverbindung eintragen.");
        }
        else {
            Ext.Ajax.request({
                url: "backend/createjiraticket",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    tickettype: tickettype,
                    dataset_id: dataset_id,
                    duedate: duedate,
                    description: description,
                    user: window.auth_user,
                    datasource: datasource_text,
                    responsible_party: responsible_party,
                    linkedticket: linkedticket,
                    update_components_text: update_components_text,
                    fme_text: fme_text,
                    jiraproject_name_to_write: jiraproject_name_to_write,
                    issuetype: issuetype,
                    story_points_value: story_points_value,
                    project_components: project_components,
                    servicetype_text: servicetype_text,
                    jiraproject_label: jiraproject_label,
                    last_edited_by: window.auth_user
                },
                success: function (response) {
                    var jsonResponseText = JSON.parse(response.responseText);

                    Ext.getStore("JiraTickets").load({
                        params: {dataset_id: dataset_id}
                    });

                    if (jsonResponseText.error === true && jsonResponseText.errorType === "transition") {
                        if (jsonResponseText.errorStatus === 500) {
                            Ext.Msg.alert("Jira Ticket", "Jira Ticket wurde im Backlog angelegt, konnte aber nicht verschoben werden!<br>Transition-ID aus dem Backlog fehlerhaft.");
                        }
                        else if (jsonResponseText.errorStatus === 400) {
                            Ext.Msg.alert("Jira Ticket", "Jira Ticket wurde angelegt, aber beim Verschieben ist ein Problem aufgetreten!<br>Ticket konnte nicht in Zielspalte verschoben werden. Transition-ID an einer Stelle fehlerhaft.");
                        }
                    }
                    else if (jsonResponseText.error === true && jsonResponseText.errorType === "add_to_database") {
                        Ext.Msg.alert("Jira Ticket", "Jira Ticket wurde erstellt, konnte aber nicht in der UDP Manager DB hinterlegt werden.", Ext.emptyFn);
                    }
                    else if (jsonResponseText.error === true) {
                        if (jsonResponseText.errorMessages && jsonResponseText.errorMessages.length > 0) {
                            Ext.Msg.alert("Jira Ticket", "Jira Ticket konnte nicht erzeugt werden. Fehler: " + jsonResponseText.errorMessages[0], Ext.emptyFn);
                        }
                        else if (jsonResponseText.errors) {
                            Ext.Msg.alert("Jira Ticket", "Jira Ticket konnte nicht erzeugt werden. Fehler: " + JSON.stringify(jsonResponseText.errors), Ext.emptyFn);
                        }
                        else if (jsonResponseText.errorCode === "23505") {
                            Ext.Msg.alert("Jira Ticket", "Jira Ticket wurde erfolgreich angelegt!<br>Aufgrund eines ID-Fehlers konnte das Ticket nicht in die Datenbank geschrieben werden und taucht nicht in der Liste auf. Bitte manuell eintragen. Ticket: " + jsonResponseText.jiraTicketKey + ".");
                        }
                        else {
                            Ext.Msg.alert("Jira Ticket", "Jira Ticket konnte nicht erzeugt werden.", Ext.emptyFn);
                        }
                    }
                    else {
                        Ext.getCmp("createjiraticket-window").close();
                        Ext.Msg.alert("Jira Ticket", "Das Jira Ticket wurde erfolgreich angelegt.", Ext.emptyFn);
                    }
                },
                failure: function (response) {
                    Ext.Msg.alert("Jira Ticket", "Leider ist ein Fehler aufgetreten: " + response.responseText, Ext.emptyFn);
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    /**
     * Auxiliary function to generate a text for the Jira ticket description regarding the components to be updated.
     * @param {Array} update_components - Components to be updated
     * @param {String} tickettype - Type of task of the ticket
     * @returns {String} - Text as an enumerated list with the components to be updated
     */
    _prepareUpdateComponentsText: function (update_components, tickettype) {
        let update_components_text = "*Komponenten, die aktualisiert werden müssen:* \r\n";

        if (tickettype === "Daten Aktualisierung") {
            if (Object.keys(update_components).length === 0 && update_components.constructor === Object) {
                update_components_text = "*Komponenten, die aktualisiert werden müssen:* siehe oben unter manuelle Angaben!";
            }
            else {
                if (update_components.data) {
                    update_components_text += "* Daten: \r\n";
                    if (typeof update_components.data === "object") {
                        for (let i = 0; i < update_components.data.length; i++) {
                            update_components_text += "** " + update_components.data[i] + "\r\n";
                        }
                    }
                    else {
                        update_components_text += "** " + update_components.data + "\r\n";
                    }
                }
                if (update_components.fme) {
                    update_components_text += "* FME: \r\n";
                    if (typeof update_components.fme === "object") {
                        for (let i = 0; i < update_components.fme.length; i++) {
                            update_components_text += "** " + update_components.fme[i] + "\r\n";
                        }
                    }
                    else {
                        update_components_text += "** " + update_components.fme + "\r\n";
                    }
                }
            }
        }
        else {
            update_components_text = "";
        }

        return update_components_text;
    },

    /**
     * Auxiliary function to generate a text for the Jira ticket description regarding FME.
     * @param {Object} fme_cmp_activate - FME component to be activated. Depending on the ticket type
     * @param {String} fme_cmp_adjust - FME component to be adjusted. Depending on the ticket type
     * @param {String} tickettype - Type of task of the ticket
     * @param {String} fme_processname - Name of the FME schedule or automation
     * @param {String} fme_location - Location of the FME schedule or automation
     * @returns {String} - Text with instructions for the FME components
     */
    _prepareFmeText: function (fme_cmp_activate, fme_cmp_adjust, tickettype, fme_processname, fme_location) {
        let fme_text = {schedule: "FME Scheduler: *", automation: "FME Automation: *"};

        if (tickettype === "Neue Daten") {
            if (fme_cmp_activate.fmecomponent === "FME Scheduler") {
                fme_text = fme_text.schedule += "Bitte aktivieren.* Prozessname: *" + fme_processname + ".* Hier zu finden: " + fme_location;
            }
            else if (fme_cmp_activate.fmecomponent === "FME Automation") {
                fme_text = fme_text.automation += "Bitte starten.* Prozessname: *" + fme_processname + ".* Hier zu finden: " + fme_location;
            }
            else if (fme_cmp_activate.fmecomponent === "Keine") {
                fme_text = "*Es muss kein FME Scheduler oder FME Automation aktiviert/gestartet werden!*";
            }
        }
        else if (tickettype === "Daten Aktualisierung") {
            if (!fme_cmp_adjust) {
                fme_text = "*Es muss kein FME Scheduler oder FME Automation angepasst/gestartet werden!*";
            }
            else if (fme_cmp_adjust.includes("FME Schedule")) {
                fme_text = fme_text.schedule += "Bitte anpassen.* Prozessname: *" + fme_processname + ".* Hier zu finden: " + fme_location;
            }
            else if (fme_cmp_adjust.includes("FME Automation")) {
                fme_text = fme_text.automation += "Bitte starten.* Name: *" + fme_processname + ".* Link zur Automation: " + fme_location;
            }
        }

        return fme_text;
    },

    /**
     * Auxiliary function to generate a text for the Jira ticket description regarding the services to be created or updated.
     * @param {Object} servicetypes - All services to be created or updated
     * @param {String} tickettype - Type of task of the ticket
     * @param {String} update_components - Components to update
     * @returns {String} - Text as an enumerated list with all the service types to be created or updated
     */
    _prepareServiceTypeText: function (servicetypes, tickettype, update_components) {
        const servicetype_values = Object.values(servicetypes);
        const header_text = "|*Typ*|*ID*|*Dienstname/Endpoint*|*Instanz/Ordner Dienstkonfig*|*Absicherung*|\r\n";
        const header_text_update = "|*Typ*|*ID*|*Dienstname/Endpoint*|*Instanz/Ordner Dienstkonfig*|\r\n";

        let servicetype_text = "Bitte folgende Schnittstellen ";
        let each_servicetype_text = "";
        let each_servicetype_text_update = "";
        let security_text = "";

        servicetype_values.forEach(servicetype => {
            const service_data = Ext.getStore("ServicesDataset").findRecord("type", servicetype).data;
            const service_id = service_data.id;
            const service_name = service_data.name;
            const service_folder = service_data.folder ? service_data.folder : "nicht vorhanden";

            let service_security_type = "";

            if (!service_data.security_type) {
                service_security_type = "keine Absicherung";
            }
            else {
                service_security_type = service_data.security_type;
            }

            each_servicetype_text += "|" + servicetype + "|" + service_id + "|" + service_name + "|" + service_folder + "|" + service_security_type + "\r\n";
            each_servicetype_text_update += "|" + servicetype + "|" + service_id + "|" + service_name + "|" + service_folder + "\r\n";
        });

        if (update_components.security) {
            security_text = "\r\n*Absicherungen der Schnittstellen ändern sich. Nähere Informationen dazu in den manuellen Angaben!*";
        }

        if (tickettype === "Neue Daten") {
            servicetype_text += "anlegen:\r\n" + header_text + each_servicetype_text;
        }
        else if (tickettype === "Daten Aktualisierung") {
            servicetype_text += "anpassen:\r\n" + header_text_update + each_servicetype_text_update + security_text;
        }
        else if (tickettype === "Löschen") {
            servicetype_text += "löschen:\r\n" + header_text + each_servicetype_text;
        }

        return servicetype_text;
    },

    /**
     * Auxiliary function to generate a text for the Jira ticket description regarding the collections to be created or updated.
     * @returns {String} - Text as an enumerated list with all the collections and their db, db schema and db table.
     */
    _prepareDatasourceText: function () {
        const service_store_data = Ext.getStore("ServicesDataset").data.items;
        const service_store_types = [];
        const header_text = "|*ID*|*DB*|*Schema*|*Tabelle*|\r\n";

        service_store_data.forEach(service => {
            service_store_types.push(service.data.type);
        });

        // prepare informations for the datasource
        const external_dataset = Ext.getCmp("datasetexternal").getValue();
        const dataset_store_type = Ext.getCmp("datasetstoretype").getValue();
        const raster_data_filestore = Ext.getCmp("datasetfilestore").getValue();
        let datasource_text = "";

        if (external_dataset === true) {
            datasource_text = "nicht vorhanden - externer Datensatz";
        }
        else if (dataset_store_type === "Raster") {
            if (raster_data_filestore) {
                datasource_text = raster_data_filestore;
            }
            else {
                datasource_text = "Speicherort der Rasterdaten nicht angegeben.";
            }
        }
        else {
            const datasource_db_connection_id = Ext.getCmp("datasetdbconnectionrid").getSubmitValue();
            const all_collection_data = Ext.getStore("CollectionsDataset").data.items;
            const collecetion_selection_data = Ext.getStore("JiraTicketsCollection").data.items;

            if (datasource_db_connection_id) {
                const datasource_db_name = Ext.getStore("ConfigDbConnections").getById(datasource_db_connection_id).data.database;

                datasource_text += header_text;

                if (collecetion_selection_data.length > 0) {
                    collecetion_selection_data.forEach(collection => {
                        datasource_text += "|" + collection.data.id + "|" + datasource_db_name + "|" + (collection.data.db_schema !== null ? collection.data.db_schema : "keine Angabe") + "|" + (collection.data.db_table !== null ? collection.data.db_table : "keine Angabe") + "\r\n";
                    });
                    Ext.getStore("JiraTicketsCollection").removeAll();
                }
                else {
                    all_collection_data.forEach(collection => {
                        datasource_text += "|" + collection.data.id + "|" + datasource_db_name + "|" + (collection.data.db_schema !== null ? collection.data.db_schema : "keine Angabe") + "|" + (collection.data.db_table !== null ? collection.data.db_table : "keine Angabe") + "\r\n";
                    });
                }
            }
            else {
                if (service_store_types.length === 1 && service_store_types[0] === "STA") {
                    datasource_text = "keine Angaben";
                }
            }
        }

        return datasource_text;
    },

    /**
     * Auxiliary function to fill a store with a selection of collections.
     * @returns {void}
     */
    onJiraTicketCollectionSelectionDone: function () {
        const collectionSelection = Ext.getCmp("select-collection-grid").getSelectionModel().getSelected().items;
        const createJiraTicketWindowCollections = Ext.getCmp("createjiraticketcollectionpick-window");
        const collecetionStore = Ext.getStore("JiraTicketsCollection");

        if (collectionSelection.length === 0) {
            Ext.MessageBox.alert("Hinweis", "Bitte mindestens eine Collection auswählen.");
        }
        else {
            collecetionStore.add(collectionSelection);

            if (createJiraTicketWindowCollections) {
                createJiraTicketWindowCollections.destroy();
            }
        }
    }
});
