Ext.define("UDPManager.view.datasets.jira.CreateJiraTicketCollectionPickWindow", {
    extend: "Ext.window.Window",
    id: "createjiraticketcollectionpick-window",
    alias: "jira.CreateJiraTicketCollectionPickWindow",
    controller: "createjiraticket-controller",
    padding: "5 5 5 5", // (top, right, bottom, left)
    width: 500,
    height: 500,
    autoScroll: true,
    modal: true,
    title: "Auswahl Collections - Aktualisierung DB Tabellen",
    scrollable: true,

    layout: {
        type: "vbox",
        align: "stretch"
    },

    buttons: [{
        text: "Übernehmen",
        handler: "onJiraTicketCollectionSelectionDone"
    }],

    items: [
        {
            xtype: "grid",
            id: "select-collection-grid",
            columns: [{
                text: "Collection-ID",
                dataIndex: "id",
                flex: 0.8,
                align: "left"
            }, {
                text: "Collection-Titel",
                dataIndex: "title",
                flex: 2,
                align: "left"
            }],
            selModel: {
                selType: "checkboxmodel"
            }
        }
    ]
});
