Ext.define("UDPManager.view.datasets.jira.SearchJiraTicketWindow", {
    extend: "Ext.window.Window",
    id: "searchjiraticket-window",
    alias: "jira.SearchJiraTicketWindow",
    controller: "searchjiraticket-controller",
    padding: "5 0 0 0", // (top, right, bottom, left)
    layout: "fit",
    width: 1000,
    height: 700,
    x: 770,
    y: 200,
    modal: true,
    title: "Jira Tickets hinzufügen",
    items: [
        {
            xtype: "grid",
            id: "grid_searchjiraticket",
            autoScroll: true,
            columns: [
                {
                    text: "Key",
                    dataIndex: "key",
                    width: 90,
                    align: "left"
                }, {
                    text: "Autor",
                    dataIndex: "creator",
                    width: 200,
                    align: "left"
                }, {
                    text: "Bearbeiter",
                    dataIndex: "assignee",
                    width: 200,
                    align: "left"
                }, {
                    text: "Zusammenfassung",
                    dataIndex: "summary",
                    flex: 1,
                    // width: 200,
                    align: "left"
                }, {
                    text: "URL",
                    width: 85,
                    align: "left",
                    renderer: function (value, metadata, record) {
                        return "<a href=\"" + UDPManager.Configs.getJira() + "/" + record.get("key") + "\" target=\"_blank\">Jira Link</a>";
                    }
                }, {
                    xtype: "actioncolumn",
                    width: 24,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onAddIssue",
                    items: [{
                        iconCls: "x-fa fa-plus-circle",
                        tooltip: "Jira Ticket verknüpfen"
                    }]
                }
            ],
            store: Ext.create("Ext.data.Store", {
                fields: [],
                data: []
            })
        }
    ]
});
