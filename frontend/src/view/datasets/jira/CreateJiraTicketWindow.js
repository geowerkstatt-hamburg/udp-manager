Ext.define("UDPManager.view.datasets.jira.CreateJiraTicketWindow", {
    extend: "Ext.window.Window",
    id: "createjiraticket-window",
    alias: "jira.CreateJiraTicketWindow",
    controller: "createjiraticket-controller",
    padding: "5 5 5 5", // (top, right, bottom, left)
    width: 540,
    height: 620,
    autoScroll: true,
    modal: true,
    title: "Jira Ticket anlegen",
    listeners: {
        beforeshow: "onGetServiceTypes"
    },
    initComponent: function () {
        this.items = [
            {
                xtype: "combo",
                fieldLabel: "Jira-Projekt",
                store: Ext.create("Ext.data.Store", {
                    fields: ["name", "name_alias", "new_as_subtask", "use_in_ticketsearch"],
                    data: UDPManager.Configs.getJiraProjects()
                }),
                displayField: "name_alias",
                valueField: "name",
                editable: false,
                allowBlank: false,
                id: "createjiraticketproject",
                labelWidth: 160,
                width: "100%",
                emptyText: "Bitte Jira-Projekt auswählen...",
                listeners: {
                    "select": "linkedticketShowHide",
                    "expand": "collapseLinkedTicket"
                }
            }, {
                xtype: "combo",
                fieldLabel: "Ticketverknüpfung",
                store: "JiraLinkedTickets",
                displayField: "key",
                valueField: "key",
                editable: false,
                allowBlank: false,
                id: "createjiraticketlinkedticket",
                labelWidth: 160,
                width: "100%",
                queryMode: "local",
                hidden: true
            }, {
                xtype: "combo",
                fieldLabel: "Label",
                store: UDPManager.Configs.getJiraLabels(),
                editable: false,
                allowBlank: false,
                id: "createjiraticketlabel",
                labelWidth: 160,
                width: "100%",
                hidden: true
            }, {
                xtype: "combo",
                fieldLabel: "Ticketart",
                store: ["Neue Daten", "Daten Aktualisierung", "Löschen"],
                editable: false,
                allowBlank: false,
                id: "createjiratickettype",
                labelWidth: 160,
                width: "100%",
                listeners: {
                    "select": "tickettypeShowHideElements"
                }
            }, {
                xtype: "checkboxgroup",
                fieldLabel: "Schnittstellen",
                id: "createjiraticketservicetype",
                columns: 2,
                labelWidth: 160,
                width: "100%",
                hidden: true,
                allowBlank: false
            }, {
                xtype: "radiogroup",
                fieldLabel: "FME Komponente aktivieren",
                columns: 3,
                id: "createjiraticketfmecomponent",
                labelWidth: 160,
                width: "100%",
                items: [
                    {boxLabel: "FME Scheduler", itemId: "createjiraticketfmescheduler", name: "fmecomponent", inputValue: "FME Scheduler"},
                    {boxLabel: "FME Automation", itemId: "createjiraticketfmeautomation", name: "fmecomponent", inputValue: "FME Automation"},
                    {boxLabel: "Keine", itemId: "createjiraticketnofme", name: "fmecomponent", inputValue: "Keine"}
                ],
                hidden: true,
                allowBlank: false,
                listeners: {
                    change: function () {
                        this.allowBlank = true;
                    }
                }
            }, {
                xtype: "checkboxgroup",
                fieldLabel: "Komponenten zu aktualisieren",
                layout: "column",
                defaultType: "container",
                items: [{
                    items: [
                        {xtype: "component", html: "Daten", cls: "x-form-check-group-label", width: 158.5},
                        {xtype: "checkboxfield", boxLabel: "Database", name: "data", inputValue: "Database", margin: 0, listeners: {change: "opencollectionspicker"}},
                        {xtype: "checkboxfield", boxLabel: "Filesystem", name: "data", inputValue: "Filesystem", margin: 0},
                        {xtype: "component", html: "Absicherung", cls: "x-form-check-group-label"},
                        {xtype: "checkboxfield", boxLabel: "Schnittstellen absichern", name: "security", inputValue: "Absicherung", margin: 0, autoEl: {tag: "div", "data-qtip": "Änderungen zur Absicherung der Schnittstellen bitte unter den Anmerkungen ausführen."}}
                    ]
                }, {
                    items: [
                        {xtype: "component", html: "FME", cls: "x-form-check-group-label"},
                        {xtype: "checkboxfield", boxLabel: "Schedule", name: "fme", inputValue: "FME Schedule", margin: 0},
                        {xtype: "checkboxfield", boxLabel: "Automation", name: "fme", inputValue: "FME Automation", margin: 0}
                    ]
                }],
                id: "createjiraticketcomponents",
                labelWidth: 160,
                width: "100%",
                allowBlank: false,
                hidden: true
            },
            // {
            //     xtype: "combo",
            //     fieldLabel: "Absicherung",
            //     store: ["Nein", "Basic Auth", "AD SSO", "IP Schutz"],
            //     editable: false,
            //     allowBlank: false,
            //     id: "createjiraticketsecurity",
            //     labelWidth: 160,
            //     width: "100%",
            //     hidden: true
            // },
            {
                xtype: "datefield",
                fieldLabel: "Fälligkeitsdatum",
                id: "createjiraticketduedate",
                labelWidth: 160,
                width: "100%",
                allowBlank: false,
                minValue: new Date(),
                format: "Y-m-d",
                value: moment().add(7, "days").format("YYYY-MM-DD")
            }, {
                xtype: "textarea",
                id: "createjiraticketdescription",
                hideLabel: true,
                flex: 1,
                emptyText: "Anmerkungen",
                width: "100%",
                height: 200,
                fieldStyle: {
                    "color": "#404040",
                    "font-weight": "normal"
                },
                listeners: {
                    change: "settextstyle"
                }
            }
        ];
        this.buttons = [
            {
                text: "Abschicken",
                handler: "onCreateJiraTicket"
            }
        ];
        this.callParent(arguments);
    }
});
