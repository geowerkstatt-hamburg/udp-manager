Ext.define("UDPManager.view.datasets.jira.DatasetJira", {
    extend: "Ext.panel.Panel",
    xtype: "dataset_jira",
    alias: "widget.dataset_jira",
    controller: "dataset_jira",

    title: "JIRA",
    layout: "vbox",
    bodyPadding: "0",
    style: "border-top: 1px solid #cfcfcf !important;",
    items: [
        {
            xtype: "grid",
            id: "grid_linkedjiratickets",
            store: "JiraTickets",
            autoScroll: true,
            height: Ext.Element.getViewportHeight() - 108,
            width: "100%",
            viewConfig: {
                enableTextSelection: true,
                stripRow: false
            },
            columnLines: true,
            columns: [
                {
                    text: "Projekt",
                    dataIndex: "project",
                    minWidth: 90,
                    autoSizeColumn: true,
                    align: "left"
                }, {
                    text: "Key",
                    dataIndex: "key",
                    minWidth: 120,
                    autoSizeColumn: true,
                    align: "left"
                }, {
                    text: "Autor",
                    dataIndex: "creator",
                    minWidth: 150,
                    autoSizeColumn: true,
                    align: "left"
                }, {
                    text: "Bearbeiter",
                    dataIndex: "assignee",
                    minWidth: 150,
                    autoSizeColumn: true,
                    align: "left"
                }, {
                    text: "Erstellungsdatum",
                    dataIndex: "create_date",
                    minWidth: 125,
                    autoSizeColumn: true,
                    align: "left"
                }, {
                    text: "URL",
                    width: 85,
                    align: "center",
                    renderer: function (value, metadata, record) {
                        return "<a href=\"" + UDPManager.Configs.getJira() + "/" + record.get("key") + "\" target=\"_blank\">Jira Link</a>";
                    }
                }, {
                    text: "Zusammenfassung",
                    dataIndex: "summary",
                    flex: 1,
                    align: "left"
                }, {
                    xtype: "actioncolumn",
                    text: "Verknüpfung entfernen",
                    minWidth: 160,
                    autoSizeColumn: true,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onDeleteIssue",
                    items: [{
                        iconCls: "x-fa fa-minus-circle",
                        tooltip: "An Datensatz verknüpftes Jira Ticket entfernen"
                    }]
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "top",
                padding: "20 20 20 20",
                items: [{
                    xtype: "textfield",
                    id: "jiraTicketSearchField",
                    width: 200,
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Nach Jira Ticket suchen"
                    },
                    enableKeyEvents: true,
                    listeners: {
                        "keypress": "onKeyPress"
                    }
                }, {
                    id: "searchJiraTicketButton",
                    disabled: window.read_only,
                    text: "Jira Tickets Suchen",
                    handler: "onSearchJiraTickets"
                }, {
                    xtype: "tbspacer",
                    flex: 1
                }, {
                    id: "openJiraTicketWindow",
                    disabled: window.read_only,
                    text: "Jira Ticket anlegen",
                    handler: "onOpenJiraTicketWindow"
                }]
            }]
        }
    ]
});
