Ext.define("UDPManager.view.datasets.datasetcollections.DatasetCollectionsController", {
    extend: "Ext.app.ViewController",

    alias: "controller.dataset_collections",

    onTabChange: function (tabs, newTab) {
        // if (window.mainViewRendered) {
        //     if (newTab.xtype === "dataset_collections_layerconf") {
        //         Ext.getCmp("grid_datasetcollections").setCollapsed(true);
        //     }
        //     else {
        //         Ext.getCmp("grid_datasetcollections").setCollapsed(false);
        //     }
        // }
    },

    onGridCollectionItemSelected: function (sender, record) {
        this.getViewModel().set("form_collections", record);

        const db_connection_r_id = Ext.getCmp("datasetdbconnectionrid").getValue();
        const dataset_store_type = Ext.getCmp("datasetstoretype").getValue();
        const me = this;

        Ext.getCmp("datasetcollectiondbtable").bindStore([]);

        if (db_connection_r_id && !record.get("group_object")) {
            Ext.Ajax.request({
                url: "backend/getdbschemas",
                method: "GET",
                headers: {token: window.apiToken},
                params: {
                    db_connection_r_id: db_connection_r_id
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText, true);
                    const schema_arr = [];

                    response_json.forEach(element => schema_arr.push(element.schema_name));

                    Ext.getCmp("datasetcollectiondbschema").bindStore(schema_arr.sort());

                    if (!schema_arr.find(el => el === Ext.getCmp("datasetcollectiondbschema").getValue()) && Ext.getCmp("datasetcollectiondbschema").getValue() && Ext.getCmp("datasetdbconnectionwid").getSubmitValue() && parseInt(Ext.getCmp("datasetcollectionid").getSubmitValue()) !== 0) {
                        Ext.getCmp("datasetcollectionadddbschema").setDisabled(window.read_only);
                    }
                    else {
                        Ext.getCmp("datasetcollectionadddbschema").setDisabled(true);
                        me.onDbSchemaSelect(Ext.getCmp("datasetcollectiondbschema"));
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }

        Ext.getCmp("layerdetailsform").getForm().reset();

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllLayerDetailsFieldsetsHidden();
        UDPManager.app.getController("UDPManager.controller.PublicFunctions").emptyLayerJsonField();

        const editMode = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditMode();

        if (editMode) {
            Ext.getCmp("duplicatecollection").setDisabled(false);
        }

        Ext.getStore("LayersCollection").load({
            params: {collection_id: record.get("id")},
            callback: function (records) {
                if (records.length === 0 && editMode) {
                    Ext.getCmp("deletecollection").setDisabled(false);
                }
                else {
                    Ext.getCmp("deletecollection").setDisabled(true);
                }
            }
        });

        const selectedCollection = record.data;
        const attributeConfigStore = Ext.getStore("AttributeConfigs");

        Ext.getCmp("datasetcollectionadddbtable").setDisabled(true);
        Ext.getCmp("datasetcollectionadddbschema").setDisabled(true);

        if (parseInt(selectedCollection.id) !== 0) {
            Ext.getCmp("editattributeconfig").setDisabled(false);
            Ext.getCmp("editstyle").setDisabled(false);
            Ext.getCmp("editstylesldfile").setDisabled(false);
        }
        else {
            Ext.getCmp("editattributeconfig").setDisabled(true);
            Ext.getCmp("editstyle").setDisabled(true);
            Ext.getCmp("editstylesldfile").setDisabled(true);
        }

        attributeConfigStore.removeAll();

        if (selectedCollection.attribute_config) {
            if (Object.keys(selectedCollection.attribute_config).length > 0) {
                for (let i = 0; i < selectedCollection.attribute_config.length; i++) {
                    const config = selectedCollection.attribute_config[i];

                    if (typeof config.attr_name_masterportal === "object" && config.attr_name_masterportal !== null) {
                        config.attr_name_masterportal = JSON.stringify(config.attr_name_masterportal);
                    }
                    if (selectedCollection.attr_log) {
                        if (selectedCollection.attr_log.attr_diff_json && (typeof Object.values(selectedCollection.attr_log.attr_diff_json[i])[0] === "object" || Object.values(selectedCollection.attr_log.attr_diff_json[i])[0] === "Attribut neu hinzugefügt")) {
                            config.attr_changed = true;
                        }
                    }
                }
            }

            attributeConfigStore.loadData(selectedCollection.attribute_config);

            if (selectedCollection.id > 0 && dataset_store_type !== "Raster") {
                Ext.getCmp("editattributeconfig").setDisabled(false);
            }
        }

        const styleStore = Ext.getStore("DatasetCollectionStyle");

        styleStore.removeAll();

        if (selectedCollection.style) {
            styleStore.loadData(selectedCollection.style);
        }

        if (selectedCollection.legend_file_name) {
            Ext.getCmp("datasetcollectiondownloadlegend").setHidden(false);
            Ext.getCmp("datasetcollectiondeletelegend").setHidden(false);
        }
        else {
            Ext.getCmp("datasetcollectiondownloadlegend").setHidden(true);
            Ext.getCmp("datasetcollectiondeletelegend").setHidden(true);
        }

        const external_dataset = Ext.getCmp("datasetexternal").getValue();

        if (external_dataset) {
            if (record.data.id === 0) {
                Ext.getCmp("datasetcollectionname").setEditable(true);
                Ext.getCmp("datasetcollectionapiconstraint").setDisabled(false);
            }
            else {
                Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(true);
                Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(true);
                Ext.getCmp("datasetcollectionname").setEditable(false);
                if (UDPManager.Configs.getModules().externalCollectionImport) {
                    Ext.getCmp("datasetcollectionapiconstraint").setDisabled(true);
                }
            }
        }
        else {
            Ext.getCmp("datasetcollectionapiconstraint").setDisabled(false);
            Ext.getCmp("datasetcollectionname").setEditable(true);
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(false);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(false);
        }

        if (dataset_store_type === "Raster") {
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(true);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(true);
        }
        else  {
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(false);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(false);
        }

        Ext.getCmp("datasetcollectiondetailsform").setDisabled(false);

        if (record.data.id !== 0) {
            Ext.getCmp("sharecollection").setDisabled(false);
        }
    },

    onStoreTypeChange: function () {
        const store_type = Ext.getCmp("datasetcollectionstoretype").getSubmitValue();
        const dataset_store_type = Ext.getCmp("datasetstoretype").getSubmitValue();

        if (store_type === "Vektor") {
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(false);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(false);
            Ext.getCmp("datasetcollectiontileindex").setHidden(true);
            Ext.getCmp("datasetcollectiontileindex").setValue("");
            Ext.getCmp("datasetcollectionnamespace").setHidden(false);
            Ext.getCmp("datasetcollectioncomplexscheme").setHidden(false);
            Ext.getCmp("editattributeconfig").setDisabled(false);
            Ext.getCmp("editstyle").setDisabled(false);
            Ext.getCmp("editstylesldfile").setDisabled(false);
        }
        else if (store_type === "Raster") {
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(true);
            Ext.getCmp("datasetcollectiondbschema").setValue(null);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(true);
            Ext.getCmp("datasetcollectiondbtable").setValue(null);
            Ext.getCmp("datasetcollectiontileindex").setHidden(false);
            Ext.getCmp("datasetcollectionnamespace").setHidden(true);
            Ext.getCmp("datasetcollectioncomplexscheme").setHidden(true);
            Ext.getCmp("editattributeconfig").setDisabled(true);
            Ext.getCmp("editstyle").setDisabled(true);
            Ext.getCmp("editstylesldfile").setDisabled(true);
        }
    },

    onDbSchemaSelect: function (combo) {
        const db_connection_r_id = Ext.getCmp("datasetdbconnectionrid").getValue();

        if (db_connection_r_id) {
            Ext.Ajax.request({
                url: "backend/getdbtables",
                method: "GET",
                headers: {token: window.apiToken},
                params: {
                    db_connection_r_id: db_connection_r_id,
                    schema: combo.getSubmitValue()
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText, true);
                    const table_arr = [];

                    response_json.forEach(element => table_arr.push(element.table_name));
                    Ext.getCmp("datasetcollectiondbtable").bindStore(table_arr.sort());

                    if (!table_arr.find(el => el === Ext.getCmp("datasetcollectiondbtable").getValue()) && Ext.getCmp("datasetcollectiondbtable").getValue() && Ext.getCmp("datasetdbconnectionwid").getSubmitValue() && Ext.getStore("AttributeConfigs").count() > 0) {
                        Ext.getCmp("datasetcollectionadddbtable").setDisabled(window.read_only);
                    }
                    else {
                        Ext.getCmp("datasetcollectionadddbtable").setDisabled(true);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
    },

    onDbSchemaChange: function (combo) {
        const schemaStore = combo.getStore().findExact("field1", combo.getSubmitValue());

        if (schemaStore === -1 && combo.getSubmitValue() && Ext.getCmp("datasetdbconnectionwid").getSubmitValue() && parseInt(Ext.getCmp("datasetcollectionid").getSubmitValue()) !== 0) {
            Ext.getCmp("datasetcollectionadddbschema").setDisabled(window.read_only);
        }
        else {
            Ext.getCmp("datasetcollectionadddbschema").setDisabled(true);
        }
    },

    onDbTableChange: function (combo) {
        const tableStore = combo.getStore().findExact("field1", combo.getSubmitValue());
        const schemaStore = Ext.getCmp("datasetcollectiondbschema").getStore().findExact("field1", Ext.getCmp("datasetcollectiondbschema").getSubmitValue());

        if (tableStore === -1 && combo.getSubmitValue() && schemaStore > -1 && Ext.getCmp("datasetcollectiondbschema").getSubmitValue() && Ext.getCmp("datasetdbconnectionwid").getSubmitValue() && Ext.getStore("AttributeConfigs").count() > 0 && Ext.getCmp("datasetcollectionid").getSubmitValue() !== 0) {
            Ext.getCmp("datasetcollectionadddbtable").setDisabled(window.read_only);
        }
        else {
            Ext.getCmp("datasetcollectionadddbtable").setDisabled(true);
        }
    },

    onGenerateBbox: function (combo) {
        const db_schema = Ext.getCmp("datasetcollectiondbschema").getValue();
        const db_table = Ext.getCmp("datasetcollectiondbtable").getValue();
        const db_connection_r_id = Ext.getCmp("datasetdbconnectionrid").getSubmitValue();
        const crs = Ext.getCmp("datasetsrs").getValue();

        Ext.Ajax.request({
            url: "backend/generateBbox",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                db_schema: db_schema,
                db_table: db_table,
                db_connection_r_id: db_connection_r_id,
                crs: crs,
            },
            success: function (response) {
                if (response?.responseText) {
                    const bbox = JSON.parse(response.responseText),
                        bboxAsString = Object.values(bbox).join(",");

                    Ext.getCmp("datasetcollectioncumstombbox").setValue(bboxAsString);

                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Die BBox konnte nicht erzeugt werden");
            }
        });
    },

    onAddDbSchema: function () {
        const db_connection_w_id = Ext.getCmp("datasetdbconnectionwid").getValue();
        const db_connection_r_id = Ext.getCmp("datasetdbconnectionrid").getValue();
        const schema = Ext.getCmp("datasetcollectiondbschema").getSubmitValue();

        if (db_connection_w_id && schema) {
            Ext.Ajax.request({
                url: "backend/adddbschema",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    db_connection_w_id: db_connection_w_id,
                    db_connection_r_id: db_connection_r_id,
                    schema: schema
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText, true);

                    if (response_json.error) {
                        Ext.MessageBox.alert("Fehler", "Schema konnte nicht angelegt werden");
                    }
                    else {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Schema angelegt!");
                        Ext.getCmp("datasetcollectiondbschema").getStore().add({field1: schema});
                        Ext.getCmp("datasetcollectionadddbschema").setDisabled(true);
                        Ext.getCmp("datasetcollectionadddbtable").setDisabled(false);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
    },

    onAddDbTable: function () {
        Ext.getStore("TableSchema").removeAll();

        if (Ext.getStore("AttributeConfigs").count() > 0) {
            Ext.getStore("AttributeConfigs").each(function (record) {
                if (record.data.attr_name_db !== "ignore") {
                    Ext.getStore("TableSchema").add({
                        attr_name_db: record.data.attr_name_db,
                        attr_datatype: record.data.attr_datatype,
                        attr_datatype_orig: record.data.attr_datatype,
                        pk: record.data.pk
                    });
                }
            });

            let tableSchemaEditor = Ext.getCmp("tableschemaeditor-window");

            if (tableSchemaEditor) {
                tableSchemaEditor.destroy();
            }

            tableSchemaEditor = Ext.create("tableschema.TableSchemaEditor");
            tableSchemaEditor.show();
        }
        else {
            Ext.MessageBox.alert("Hinweis", "Attributkonfiguration nicht vorhanden!");
        }
    },

    onCreateDbTable: function () {
        const db_connection_w_id = Ext.getCmp("datasetdbconnectionwid").getValue();
        const db_connection_r_id = Ext.getCmp("datasetdbconnectionrid").getValue();
        const schema = Ext.getCmp("datasetcollectiondbschema").getSubmitValue();
        const table = Ext.getCmp("datasetcollectiondbtable").getSubmitValue();
        const table_schema_array = [];
        let valid = true;

        Ext.getStore("TableSchema").each(function (record) {
            if (record.data.attr_datatype_orig === "number" && (record.data.attr_datatype === "text" || record.data.attr_datatype === "date" || record.data.attr_datatype.indexOf("time") > -1 || record.data.attr_datatype === "boolean" || record.data.attr_datatype.indexOf("geometry") > -1)) {
                valid = false;
            }
            if (record.data.attr_datatype_orig === "text" && (record.data.attr_datatype === "double precision" || record.data.attr_datatype === "integer" || record.data.attr_datatype === "numeric(10,2)" || record.data.attr_datatype === "number" || record.data.attr_datatype === "date" || record.data.attr_datatype.indexOf("time") > -1 || record.data.attr_datatype === "boolean" || record.data.attr_datatype.indexOf("geometry") > -1)) {
                valid = false;
            }
            if (record.data.attr_datatype_orig.indexOf("geometry") > -1 && (record.data.attr_datatype === "double precision" || record.data.attr_datatype === "integer" || record.data.attr_datatype === "numeric(10,2)" || record.data.attr_datatype === "number" || record.data.attr_datatype === "date" || record.data.attr_datatype.indexOf("time") > -1 || record.data.attr_datatype === "boolean")) {
                valid = false;
            }
            if (record.data.attr_datatype_orig === "date" && (record.data.attr_datatype === "double precision" || record.data.attr_datatype === "integer" || record.data.attr_datatype === "numeric(10,2)" || record.data.attr_datatype === "number" || record.data.attr_datatype === "boolean" || record.data.attr_datatype.indexOf("geometry") > -1)) {
                valid = false;
            }

            table_schema_array.push(record.data);
        });

        if (!valid) {
            Ext.MessageBox.alert("Fehler", "Datentypzuordnung nicht valide");
        }
        else if (db_connection_w_id && schema && table && Ext.getStore("TableSchema").count() > 0) {
            Ext.Ajax.request({
                url: "backend/adddbtable",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    db_connection_w_id: db_connection_w_id,
                    db_connection_r_id: db_connection_r_id,
                    schema: schema,
                    table: table,
                    data_schema: table_schema_array,
                    srs: Ext.getCmp("datasetsrs").getSubmitValue().split(":")[1]
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText, true);

                    if (response_json.error) {
                        Ext.MessageBox.alert("Fehler", "Tabelle konnte nicht angelegt werden");
                    }
                    else {
                        Ext.getCmp("tableschemaeditor-window").hide();
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Tabelle angelegt!");
                        Ext.getCmp("datasetcollectiondbtable").getStore().add({field1: table});
                        Ext.getCmp("datasetcollectionadddbtable").setDisabled(true);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
    },

    onGfiAsNewWindow: function () {
        if (Ext.getCmp("datasetcollectiongfiasnewwindow").checked) {
            Ext.getCmp("datasetcollectiongfiwindowspecs").setHidden(false);
        }
        else {
            Ext.getCmp("datasetcollectiongfiwindowspecs").setHidden(true);
            Ext.getCmp("datasetcollectiongfiwindowspecs").setValue("");
        }
    },

    onEditAttributeConfig: function () {
        const group = Ext.getCmp("datasetcollectiongroupobject").getValue();

        let attributeEditorSimple = Ext.getCmp("attributeeditorsimple-window");

        if (attributeEditorSimple) {
            attributeEditorSimple.destroy();
        }

        let attributeEditor = Ext.getCmp("attributeeditor-window");

        if (attributeEditor) {
            attributeEditor.destroy();
        }

        if (group) {
            attributeEditorSimple = Ext.create("datasetcollections.AttributeEditorSimple");
            attributeEditorSimple.show();
            attributeEditorSimple.setTitle("Attribute konfigurieren - " + Ext.getCmp("datasetcollectiontitle").getSubmitValue());
        }
        else {
            attributeEditor = Ext.create("datasetcollections.AttributeEditor");
            attributeEditor.show();
            attributeEditor.setTitle("Attribute konfigurieren - " + Ext.getCmp("datasetcollectiontitle").getSubmitValue());
        }

        const editMode = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditMode();

        Ext.getCmp("attributessave").setDisabled(!editMode);
        Ext.getCmp("getattributesservice").setDisabled(!editMode);
        Ext.getCmp("getattributesdb").setDisabled(!editMode);
        Ext.getCmp("addattributerow").setDisabled(!editMode);
        Ext.getCmp("deleteattributerow").setDisabled(!editMode);
        Ext.getCmp("attributeeditorchanges").setDisabled(!editMode);

        Ext.getCmp("attributeeditorform").setDisabled(true);

        const collection = Ext.getStore("CollectionsDataset").findRecord("id", Ext.getCmp("datasetcollectionid").getValue(), 0, false, false, true);

        if (collection.data.attribute_config) {
            if (collection.data.attribute_config.length > 0) {
                Ext.getCmp("attributeeditor-grid").selModel.select(0);
            }
        }

        if (collection.data.attr_log) {
            if (collection.data.attr_log.status === "change detected") {
                Ext.getCmp("attrchanges").setHtml(collection.data.attr_log.attr_diff);
                Ext.getCmp("attrchangeslabel").setHtml(`Letzter geloggter Stand der Attributtabelle: ${collection.data.attr_log.timestamp}, editiert von: ${collection.data.attr_log.editor}`);
            }
            else {
                Ext.getCmp("attrchanges").setHtml("");
                Ext.getCmp("attrchangeslabel").setHtml("Kein Vergleich möglich - Keine alte Version der Attributtabelle vorhanden!");
            }
        }
    },

    onClickChangelog: function (grid, rowIndex) {
        const row_data = grid.getStore().getAt(rowIndex).data;

        if (row_data.collection_changed) {
            Ext.getCmp("collections_main_tabs").setActiveTab(1);
        }
    },

    onAddNewCollection: function () {
        Ext.getCmp("collections_main_tabs").setActiveTab(0);

        const rec = new UDPManager.model.UDPManagerModel({
            id: 0,
            title: "*NEU*",
            attribute_config: {},
            namespace: UDPManager.Configs.getDefaultCollectionNamespace(),
            transparency: 0,
            scale_min: 0,
            scale_max: 2500000,
            gfi_theme: "default",
            db_schema: Ext.getCmp("datasetshortname").getValue(),
            service_url_visible: true
        });
        const count = Ext.getStore("CollectionsDataset").count();

        Ext.getStore("CollectionsDataset").insert(count, rec);

        Ext.getCmp("grid_datasetcollections").getSelectionModel().select(0);
    },

    /**
    * Sub-function of collection import functionality. Collects required informations from GUI fields the service-capabilities-request and send it to the backend.
    * Gets all required information from the backend to fill a selection window, which shows all the collections to import of the dataset.
    * @param {Object} btn - information about the button with which the function was triggered.
    * @returns {void}
    */
    onImportCollection: function (btn) {
        const service_store_types = [];
        const service_request_data = {};
        let service_store_data;

        // get information about a selected service
        if (btn.id === "retrievecollections") {
            const serviceChoiceWindow = Ext.getCmp("collectionimportchoice-window");

            service_store_data = [Ext.getCmp("collectionsimportservicetype").getSelection()];

            if (serviceChoiceWindow) {
                serviceChoiceWindow.destroy();
            }
        }
        // get information about all services attached to the dataset
        else {
            service_store_data = Ext.getStore("ServicesDataset").data.items;
        }

        service_store_data.forEach(service => {
            service_store_types.push(service.data.type);
        });

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showLoadMask("grid_datasetcollections");

        // collects all required inforamtion from wms and/or wfs and/or wmts services connected to the dataset.
        service_store_data.forEach(service => {
            if (service) {
                const service_data = {};

                service_data.is_mrh = false;
                service_data.service_id = service.data.id;
                service_data.service_title = service.data.title;
                service_data.service_type = service.data.type;
                service_data.version = service.data.version;
                service_data.software = service.data.software;
                service_data.url_ext = service.data.url_ext;
                service_data.url = service.data.url_int;
                service_data.external_service = service.data.external;

                if (service_data.external_service) {
                    service_data.url = service.data.url_ext_prod;
                }
                if (service_data.url.slice(-1) === "&") {
                    service_data.url = service_data.url.slice(0, -1);
                }
                if (service_data.service_title.indexOf("MRH") > -1) {
                    service_data.is_mrh = true;
                }

                if (service_store_types.length > 1) {
                    if (service.data.type === "WMS" || service.data.type === "WMS-Time") {
                        service_request_data.wms = service_data;
                    }
                    else if (service.data.type === "WFS" || service.data.type === "WFS-T") {
                        service_request_data.wfs = service_data;
                    }
                    else if ((!service_store_types.includes("WMS") && !service_store_types.includes("WFS")) && service.data.type === "WMTS") {
                        service_request_data.wmts = service_data;
                    }
                }
                else {
                    service_request_data[service.data.type.toLowerCase()] = service_data;
                }
            }
            else {
                Ext.MessageBox.alert("Hinweis", "Keine Schnittstellen mit dem Datensatz verknüpft!");
            }
        });

        if (Object.entries(service_request_data).length > 0) {
            Ext.Ajax.request({
                url: "backend/getservicecollections",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: service_request_data,
                success: function (response) {
                    const responseText = Ext.decode(response.responseText);
                    const storeFields = [
                        {name: "type"},
                        {name: "name"},
                        {name: "title"},
                        {name: "service_id"},
                        {name: "service_type"},
                        {name: "namespace"},
                        {name: "scale_max"},
                        {name: "scale_min"},
                        {name: "output_format_wms"},
                        {name: "output_format_wfs"},
                        {name: "service_name_wms"},
                        {name: "service_name_wfs"}
                    ];
                    const groupLayerStore = new Ext.data.ArrayStore({
                        fields: storeFields,
                        filters: [{
                            property: "type",
                            value: /groupLayer/
                        }]
                    });
                    const layerStore = new Ext.data.ArrayStore({
                        fields: storeFields,
                        filters: [{
                            property: "type",
                            value: /layer/
                        }]
                    });

                    let collection_list = [];

                    if (responseText.status === "success") {

                        // result data ist splitted in wms and wfs. Required information to import the collections are stored in an array.
                        // if the result data only includes wms the array are filled with the wms data.
                        if (responseText.result_data.wms) {
                            collection_list = responseText.result_data.wms.collection_list;

                            // if the result data also includes wfs data the wfs specific data is append to the array.
                            if (responseText.result_data.wfs) {
                                collection_list.forEach(collection_element => {
                                    collection_element.namespace = responseText.result_data.wfs.collection_list[0].namespace;
                                    collection_element.service_name_wfs = responseText.result_data.wfs.collection_list[0].service_name_wfs;
                                    collection_element.output_format_wfs = responseText.result_data.wfs.collection_list[0].output_format_wfs;
                                    collection_element.service_id_wfs = responseText.result_data.wfs.collection_list[0].service_id_wfs;
                                });
                            }
                            // if the result data does not include wfs data for the namespace a default is setted.
                            else {
                                collection_list.forEach(collection_element => {
                                    collection_element.namespace = UDPManager.Configs.getDefaultCollectionNamespace();
                                });
                            }
                        }
                        // if the result data only includes wfs the array are filled with the wfs data.
                        else if (responseText.result_data.wfs) {
                            collection_list = responseText.result_data.wfs.collection_list;
                        }
                        else if (responseText.result_data.wmts) {
                            collection_list = responseText.result_data.wmts.collection_list;
                        }

                        // removes the namespace of the collections if the external collection import is used
                        if (UDPManager.Configs.getModules().externalCollectionImport) {
                            collection_list.forEach(collection_element => {
                                collection_element.namespace = null;
                            });
                        }

                        groupLayerStore.loadData(collection_list);

                        // to avoid duplicates of the collections, it is checked whether collections already exists for the dataset and if so, they are deleted from the array to import.
                        Ext.getStore("CollectionsDataset").getData().items.forEach(dataItem => {
                            collection_list.forEach(collectionItem => {
                                if (!UDPManager.Configs.getModules().externalCollectionImport) {
                                    if (collectionItem.name === dataItem.data.name || collectionItem.name === dataItem.data.alternative_name) {
                                        collection_list.splice(collection_list.indexOf(collectionItem), 1);
                                    }
                                }
                                else {
                                    if (collectionItem.service_type + " " + collectionItem.title === dataItem.data.title || collectionItem.name === dataItem.data.name) {
                                        collection_list.splice(collection_list.indexOf(collectionItem), 1);
                                    }
                                }
                            });
                        });

                        layerStore.loadData(collection_list);

                        let collectionImportWindow = Ext.getCmp("collectionimport-window");

                        if (collectionImportWindow) {
                            collectionImportWindow.destroy();
                        }

                        collectionImportWindow = Ext.create("datasetcollections.DatasetCollectionsImport");

                        if (btn.id === "retrievecollections") {
                            collectionImportWindow.setTitle("Collection Auswahl - Schnittstelle: " + service_store_data[0].data.title);
                        }

                        // auskommentiert 04.07.22 - Funktionalität für Gruppenlayer noch nicht vorhanden
                        // if (groupLayerStore.getCount() > 0) {
                        //     Ext.getCmp("select-grouplayer_grid").setStore(groupLayerStore);
                        // }
                        // else {
                        //     Ext.getCmp("select-grouplayer-grid").hide();
                        // }

                        Ext.getCmp("select-layer-grid").setStore(layerStore);
                        Ext.getCmp("select-layer-grid").getView().getSelectionModel().selectAll(true);

                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                        collectionImportWindow.show();

                        if (layerStore.data.items.length === 0) {
                            Ext.MessageBox.alert("Hinweis", "Keine neuen Collections in Schnittstelle vorhanden!");
                        }
                    }
                    else {
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                        Ext.MessageBox.alert("Status", "Schnittstelle konnte nicht abgerufen werden: " + responseText.message + ". </br>" + responseText.response);
                    }
                },
                failure: function (response) {
                    console.log(response);
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                    Ext.MessageBox.alert("Status", "Schnittstelle konnte nicht abgerufen werden!");
                }
            });
        }
        else {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            Ext.MessageBox.alert("Hinweis", "Keine WMS oder WFS Schnittstelle mit Datensatz verknüpft");
        }
    },

    /**
    * Sub-function of collection import functionality. Saves the selected Collections from the import window in the database.
    * Also generates a layer for every service in the collection and saves it in the database.
    * @returns {void}
    */
    onCollectionSelectionDone: function () {
        const collectionSelection = Ext.getCmp("select-layer-grid").getSelectionModel().getSelected().items;
        const dataset_id = Ext.getCmp("datasetid").getValue();
        const dataset_status = Ext.getCmp("datasetstatus").getValue();
        const me = this;

        let collectionvalues = {};

        collectionSelection.forEach(collection => {
            collection.data.id = 0;
            collection.data.dataset_id = dataset_id;
            collection.data.title_alt = null;
            collection.data.attribution = null;
            collection.data.additional_categories = null;
            collection.data.legend_url_intranet = null;
            collection.data.legend_url_internet = null;
            collection.data.transparency = 0;
            collection.data.gfi_disabled = false;
            collection.data.gfi_theme = "default";
            collection.data.gfi_theme_params = null;
            collection.data.attribute_config = null;
            collection.data.gfi_as_new_window = false;
            collection.data.gfi_window_specs = null;
            collection.data.service_url_visible = true;
            collection.data.db_table = null;
            collection.data.additional_info = null;
            collection.data.description = null;
            collection.data.group_object = false;
            collection.data.custom_bbox = null;
            collection.data.legend_data_url = null;
            collection.data.legend_content_type = null;
            collection.data.legend_file_name = null;
            collection.data.complex_scheme = false;
            collection.data.tileindex = null;
            collection.data.db_schema = null;
            collection.data.use_style_sld = false;
            collection.data.number_features = null;
            collection.data.store_type = null;
            collection.data.source_data_type = null;
            collection.data.source_data_location = null;
            collection.data.source_data_name = null;
            collection.data.source_data_table = null;
            collection.data.source_data_schema = null;
            collection.data.source_data_query = null;
            collection.data.source_db_connection_id = null;
            collection.data.last_edited_by = window.auth_user;

            // set certain collection values differently depending on whether the module "externalCollectionImport" is switched on or off.
            if (UDPManager.Configs.getModules().externalCollectionImport) {
                if (collection.data.service_type === "WFS" || collection.data.service_type === "WFS-T") {
                    collection.data.api_constraint = "only_download";
                }
                else if (collection.data.service_type === "WMS" || collection.data.service_type === "WMS-Time" || collection.data.service_type === "WMTS") {
                    collection.data.api_constraint = "only_visual";
                }
                else {
                    collection.data.api_constraint = null;
                }
                collection.data.alternative_name = collection.data.name;
                collection.data.name = "extern_";
                collection.data.title = collection.data.service_type + " " + collection.data.title;
            }
            else {
                collection.data.api_constraint = null;
                collection.data.alternative_name = null;
            }

            collectionvalues = collection;

            Ext.Ajax.request({
                url: "backend/savecollectionsdataset",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: collectionvalues.data,
                success: async function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        Ext.getStore("CollectionsDataset").load({
                            params: {dataset_id: dataset_id, dataset_status: dataset_status}
                        });

                        const layervalues = {
                            id: 0,
                            collection_id: response_json.id,
                            collection_name: collectionvalues.data.name,
                            collection_title: collectionvalues.data.title,
                            collection_namespace: collectionvalues.data.namespace,
                            dataset_id: dataset_id,
                            last_edited_by: window.auth_user
                        };

                        if (collectionvalues.data.service_id_wms) {
                            const service_wms = Ext.getStore("ServicesDataset").findRecord("id", collectionvalues.data.service_id_wms, 0, false, false, true);

                            layervalues.service_id = collectionvalues.data.service_id_wms;
                            layervalues.st_attributes = {output_format: collectionvalues.data.output_format_wms};
                            layervalues.service_type = service_wms.data.type;
                            layervalues.service_security_type = service_wms.data.security_type;
                            layervalues.service_title = service_wms.data.title;
                            layervalues.service_url_int = service_wms.data.url_int;
                            layervalues.service_url_sec = service_wms.data.url_sec;

                            const ajax_request_layerdata = await UDPManager.app.getController("UDPManager.controller.PublicFunctions").stAttributesSetter(layervalues.service_type, false, layervalues);

                            me._saveLayerAjax(ajax_request_layerdata, layervalues.collection_id);
                        }

                        if (collectionvalues.data.service_id_wfs) {
                            const service_wfs = Ext.getStore("ServicesDataset").findRecord("id", collectionvalues.data.service_id_wfs, 0, false, false, true);

                            layervalues.service_id = collectionvalues.data.service_id_wfs;
                            layervalues.st_attributes = {output_format: collectionvalues.data.output_format_wfs};
                            layervalues.service_type = service_wfs.data.type;
                            layervalues.service_security_type = service_wfs.data.security_type;
                            layervalues.service_title = service_wfs.data.title;
                            layervalues.service_url_int = service_wfs.data.url_int;
                            layervalues.service_url_sec = service_wfs.data.url_sec;

                            const ajax_request_layerdata = await UDPManager.app.getController("UDPManager.controller.PublicFunctions").stAttributesSetter(layervalues.service_type, false, layervalues);

                            me._saveLayerAjax(ajax_request_layerdata, layervalues.collection_id);
                        }

                        if (collectionvalues.data.service_id_wmts) {
                            const service_wmts = Ext.getStore("ServicesDataset").findRecord("id", collectionvalues.data.service_id_wmts, 0, false, false, true);

                            layervalues.service_id = collectionvalues.data.service_id_wmts;
                            layervalues.st_attributes = {output_format: collectionvalues.data.output_format_wmts};
                            layervalues.service_type = service_wmts.data.type;
                            layervalues.service_security_type = service_wmts.data.security_type;
                            layervalues.service_title = service_wmts.data.title;
                            layervalues.service_url_int = service_wmts.data.url_int;
                            layervalues.service_url_sec = service_wmts.data.url_sec;

                            const ajax_request_layerdata = await UDPManager.app.getController("UDPManager.controller.PublicFunctions").stAttributesSetter(layervalues.service_type, false, layervalues);

                            me._saveLayerAjax(ajax_request_layerdata, layervalues.collection_id);
                        }

                        const selected_service = Ext.getCmp("grid_datasetservices").getSelectionModel().getSelection()[0];

                        if (selected_service.data.type === "WMS" || selected_service.data.type === "WMS-Time") {
                            // load layers for selected service, when load finished fill theme layer tree
                            Ext.getStore("LayersService").load({
                                params: {service_id: selected_service.data.id},
                                callback: function () {
                                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").fillLayerTreeStore();
                                }
                            });
                        }

                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Collection und Layer erfolgreich gespeichert!");
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        });
        Ext.getCmp("collectionimport-window").destroy();
    },

    /**
     * Helper function to save the layer of the imported collection
     * @param {Object} post_data - informations about the layer to be created
     * @param {Number} collection_id - collection ID to reload the layers store for the right collection
     * @returns {void}
     */
    _saveLayerAjax: function (post_data, collection_id) {
        Ext.Ajax.request({
            url: "backend/savelayer",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: post_data,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    Ext.getStore("LayersCollection").load({
                        params: {collection_id: collection_id}
                    });
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").updateLayerJsonField(response_json.id);
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
            }
        });
    },

    onSaveCollectionDetails: function () {
        const fieldvalues = Ext.getCmp("datasetcollectiondetailsform").getForm().getFieldValues();
        const dataset_status = Ext.getCmp("datasetstatus").getValue();

        if (dataset_status === "veröffentlicht" || dataset_status === "veröffentlicht - vorveröffentlicht" || dataset_status === "vorveröffentlicht") {
            this.saveEditorial();
        }
        else {
            const dataset_id = Ext.getCmp("datasetid").getValue();
            const external_dataset = Ext.getCmp("datasetexternal").getValue();
            const dataset_store_type = Ext.getCmp("datasetstoretype").getValue();
    
            fieldvalues.dataset_id = dataset_id;
            fieldvalues.last_edited_by = window.auth_user;
    
            if (fieldvalues.custom_bbox === "") {
                fieldvalues.custom_bbox = null;
            }
    
            fieldvalues.name = fieldvalues.name.replace(/ü/gi, "ue");
            fieldvalues.name = fieldvalues.name.replace(/ö/gi, "oe");
            fieldvalues.name = fieldvalues.name.replace(/ä/gi, "ae");
            fieldvalues.name = fieldvalues.name.replace(/ß/gi, "ss");
            fieldvalues.name = fieldvalues.name.replace(/ /gi, "_");
    
            fieldvalues.db_table = fieldvalues.db_table.replace(/ü/gi, "ue");
            fieldvalues.db_table = fieldvalues.db_table.replace(/ö/gi, "oe");
            fieldvalues.db_table = fieldvalues.db_table.replace(/ä/gi, "ae");
            fieldvalues.db_table = fieldvalues.db_table.replace(/ß/gi, "ss");
            fieldvalues.db_table = fieldvalues.db_table.replace(/ /gi, "_");
    
            Ext.getCmp("datasetcollectionname").setValue(fieldvalues.name);
    
            // for external datasets, the value of api_constraint must be requested explicitly. Fieldvalues of disabled fields cannot be requested via the form.
            if (external_dataset) {
                fieldvalues.api_constraint = Ext.getCmp("datasetcollectionapiconstraint").getValue();
            }
    
            const unique_collection_test = Ext.getStore("CollectionsDataset").findRecord("name", fieldvalues.name, 0, false, false, true);
    
            if (fieldvalues.name.match(/^\d/)) {
                Ext.MessageBox.alert("Fehler", "Der technische Name der Collection darf nicht mit einer Zahl beginnen!");
            }
            else if (fieldvalues.name.match(/[^a-z0-9_]/)) {
                Ext.MessageBox.alert("Fehler", "Mit Ausnahme von UNDERSCORE sind Sonderzeichen und Großschreibung für den technischen Namen der Collection nicht zulässig. Um mehrere Wörter zu verbinden bitte Snakecase benutzen!");
            }
            else if (fieldvalues.name.length > 50) {
                Ext.MessageBox.alert("Fehler", "Der technische Name der Collection darf nicht länger als 50 Zeichen sein!");
            }
            else if (fieldvalues.name[0] === "_") {
                Ext.MessageBox.alert("Fehler", "Der technische Name der Collection darf nicht mit einem UNDERSCORE beginnen!");
            }
            else if (!fieldvalues.api_constraint) {
                Ext.MessageBox.alert("Fehler", "Für die Collection muss der API Zugriff eingetragen sein!");
            }
            else if (fieldvalues.api_constraint !== "only_visual" && fieldvalues.group_object) {
                Ext.MessageBox.alert("Fehler", "Für eine Gruppen-Collection muss der API Zugriff &quot;nur Visualisierung&quot; eingetragen sein!");
            }
            else if (!fieldvalues.store_type && dataset_store_type === "Beides") {
                Ext.MessageBox.alert("Fehler", "Der Datentyp muss festgelegt werden!");
            }
            else if (unique_collection_test && unique_collection_test.data.id !== fieldvalues.id) {
                Ext.MessageBox.alert("Fehler", "Der technische Name der Collection ist bereits vergeben!");
            }
            else {
                Ext.Ajax.request({
                    url: "backend/savecollectionsdataset",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: fieldvalues,
                    success: async function (response) {
                        const response_json = Ext.decode(response.responseText);
                        const scope_id = "layerjsonupdateall";
    
                        if (response_json.status === "success") {
                            Ext.getStore("CollectionsDataset").load({
                                params: {dataset_id: dataset_id, dataset_status: dataset_status},
                                callback: function () {
                                    const collection = Ext.getStore("CollectionsDataset").findRecord("id", response_json.id, 0, false, false, true);
                                    const collection_grid = Ext.getCmp("grid_datasetcollections");
                                    const row_collection = collection_grid.store.indexOf(collection);
    
                                    collection_grid.getSelectionModel().deselectAll();
                                    collection_grid.getSelectionModel().select(row_collection);
                                    collection_grid.ensureVisible(row_collection);
    
                                    const selected_service = Ext.getCmp("grid_datasetservices").getSelectionModel().getSelection()[0];
    
                                    if (selected_service && selected_service.data.type === "WMS" || selected_service && selected_service.data.type === "WMS-Time") {
                                        // load layers for selected service, when load finished fill theme layer tree
                                        Ext.getStore("LayersService").load({
                                            params: {service_id: selected_service.data.id},
                                            callback: function () {
                                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").fillLayerTreeStore();
                                            }
                                        });
                                    }
                                }
                            });
    
                            if (fieldvalues.id === 0) {
                                const servicevalues = Ext.getStore("ServicesDataset").getData().items;
                                const layervalues = {
                                    id: 0,
                                    dataset_id: fieldvalues.dataset_id,
                                    collection_id: response_json.id,
                                    collection_name: fieldvalues.name,
                                    collection_title: fieldvalues.title,
                                    collection_namespace: fieldvalues.namespace,
                                    last_edited_by: window.auth_user
                                };
    
                                for (const service of servicevalues) {
                                    if (fieldvalues.api_constraint === "only_visual" && service.data.type !== "WMS" && service.data.type !== "WMS-Time") {
                                        continue;
                                    }
                                    if (fieldvalues.api_constraint === "only_download" && service.data.type !== "WFS" && service.data.type !== "WFS-T" && service.data.type !== "OAF") {
                                        continue;
                                    }
                                    if ((fieldvalues.store_type === "Raster" || dataset_store_type === "Raster" || dataset_store_type === "Beides") && service.data.software !== "MapServer") {
                                        continue;
                                    }
                                    if ((fieldvalues.store_type === "Vektor" || dataset_store_type === "Vektor" || dataset_store_type === "Beides") && service.data.software === "MapServer") {
                                        continue;
                                    }
                                    layervalues.service_id = service.id;
                                    layervalues.service_type = service.data.type;
                                    layervalues.service_security_type = service.data.security_type;
                                    layervalues.service_title = service.data.title;
                                    layervalues.service_url_int = service.data.url_int;
                                    layervalues.service_url_sec = service.data.url_sec;
    
                                    const ajax_request_layerdata = await UDPManager.app.getController("UDPManager.controller.PublicFunctions").stAttributesSetter(service.data.type, fieldvalues.group_object, layervalues);
    
                                    if (!Ext.Object.isEmpty(ajax_request_layerdata.st_attributes)) {
                                        await UDPManager.app.getController("UDPManager.controller.PublicFunctions").autoCreateLayer(ajax_request_layerdata);
                                    }
                                }
                            }
    
                            if (response_json.id > 0 && (dataset_store_type !== "Raster" || (dataset_store_type !== "Beides" && dataset_store_type !== "Raster"))) {
                                Ext.getCmp("editattributeconfig").setDisabled(false);
                            }
    
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").onRecreateJson(scope_id, "dev");
    
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Collection erfolgreich gespeichert!");
                        }
                        else {
                            Ext.MessageBox.alert("Fehler", response_json.message);
                        }
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    }
                });
            }
        }
    },

    saveEditorial: function () {
        const fieldvalues = Ext.getCmp("datasetcollectiondetailsform").getForm().getFieldValues();
        const dataset_id = Ext.getCmp("datasetid").getValue();
        const dataset_status = Ext.getCmp("datasetstatus").getValue();

        fieldvalues.dataset_id = dataset_id;
        fieldvalues.last_edited_by = window.auth_user;

        Ext.Ajax.request({
            url: "backend/savecollectionsdataseteditorial",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: fieldvalues,
            success: async function (response) {
                const response_json = Ext.decode(response.responseText);
                const scope_id = "layerjsonupdateall";

                if (response_json.status === "success") {
                    Ext.getStore("CollectionsDataset").load({
                        params: {dataset_id: dataset_id, dataset_status: dataset_status},
                        callback: function () {
                            const collection = Ext.getStore("CollectionsDataset").findRecord("id", response_json.id, 0, false, false, true);
                            const collection_grid = Ext.getCmp("grid_datasetcollections");
                            const row_collection = collection_grid.store.indexOf(collection);

                            collection_grid.getSelectionModel().deselectAll();
                            collection_grid.getSelectionModel().select(row_collection);
                            collection_grid.ensureVisible(row_collection);
                        }
                    });

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").onRecreateJson(scope_id, "prod");

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Collection erfolgreich gespeichert!");
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
            }
        });
    },

    onDeleteCollection: function () {
        Ext.getCmp("deletecollection").setDisabled(true);

        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const dataset_status = Ext.getCmp("datasetstatus").getValue();
        const mb = Ext.MessageBox;

        mb.buttonText.yes = "ja";
        mb.buttonText.no = "nein";

        mb.confirm("Löschen", "Wirklich löschen?", function (btn) {
            if (btn === "yes") {
                Ext.Ajax.request({
                    url: "backend/deletecollection",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        id: Ext.getCmp("datasetcollectionid").getSubmitValue(),
                        dataset_id: dataset_id,
                        last_edited_by: window.auth_user
                    },
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);

                        if (response_json.status === "success") {
                            Ext.getStore("CollectionsDataset").load(
                                {
                                    params: {dataset_id: dataset_id, dataset_status: dataset_status}
                                }
                            );
                            Ext.getCmp("datasetcollectiondetailsform").getForm().reset();
                        }
                        else {
                            Ext.MessageBox.alert("Fehler", response_json.message);
                        }
                        Ext.getCmp("deletecollection").setDisabled(window.read_only);

                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Collection gelöscht!");
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Collection konnte nicht gelöscht werden");
                        Ext.getCmp("deletecollection").setDisabled(window.read_only);
                    }
                });
            }
            else {
                Ext.getCmp("deletecollection").setDisabled(window.read_only);
            }
        });
    },

    onDuplicateCollection: function () {
        Ext.getCmp("collections_main_tabs").setActiveTab(0);

        const formfieldvalues = Ext.getCmp("datasetcollectiondetailsform").getForm().getFieldValues();

        formfieldvalues.id = 0;
        formfieldvalues.title = formfieldvalues.title + " KOPIE";
        formfieldvalues.name = formfieldvalues.name + "_duplicate";

        let attribute_config = "";

        if (Ext.getStore("AttributeConfigs").count() > 0) {
            const attribute_config_array = [];

            Ext.getStore("AttributeConfigs").each(function (record) {
                const attribute_config_json = {};

                attribute_config_json.attr_name_source = record.data.attr_name_source;
                attribute_config_json.attr_name_db = record.data.attr_name_db;
                attribute_config_json.attr_name_service = record.data.attr_name_service;
                if (record.data.attr_name_masterportal.trim().startsWith("{")) {
                    attribute_config_json.attr_name_masterportal = JSON.parse(record.data.attr_name_masterportal);
                }
                else {
                    attribute_config_json.attr_name_masterportal = record.data.attr_name_masterportal;
                }
                attribute_config_json.attr_datatype = record.data.attr_datatype;
                attribute_config_json.description = record.data.description;
                attribute_config_json.pk = record.data.pk;
                attribute_config_json.primary_geom = record.data.primary_geom;
                attribute_config_json.primary_date = record.data.primary_date;
                attribute_config_json.unit = record.data.unit;
                attribute_config_json.rule_mandatory = record.data.rule_mandatory;
                attribute_config_json.rule_filter_vis = record.data.rule_filter_vis;
                attribute_config_json.rule_filter_data = record.data.rule_filter_data;
                attribute_config_json.rule_order = record.data.rule_order;
                attribute_config_json.rule_date = record.data.rule_date;
                attribute_config_json.rule_georef = record.data.rule_georef;
                attribute_config_json.rule_note = record.data.rule_note;
                attribute_config_array.push(attribute_config_json);
            });

            attribute_config = attribute_config_array;
        }
        else {
            // attribute_config = null;
            attribute_config = {};
        }

        formfieldvalues.attribute_config = attribute_config;

        const rec = new UDPManager.model.UDPManagerModel(formfieldvalues);

        const count = Ext.getStore("CollectionsDataset").count();

        Ext.getStore("CollectionsDataset").insert(count, rec);

        Ext.getCmp("grid_datasetcollections").getSelectionModel().select(rec);
        Ext.getCmp("grid_datasetcollections").ensureVisible(rec);
    },

    onOpenStyleEditor: function () {
        const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getPrimaryGeomType();

        if (geomType === "point" || geomType === "line" || geomType === "polygon") {
            let styleEditorWindow = Ext.getCmp("styleeditor-window");

            if (styleEditorWindow) {
                styleEditorWindow.destroy();
            }

            styleEditorWindow = Ext.create("styleeditor.StyleEditorWindow");
            styleEditorWindow.show();
            styleEditorWindow.setTitle("Style Editor - " + Ext.getCmp("datasetcollectiontitle").getSubmitValue());

            Ext.getCmp("add_style_rule_geom").setStore(UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomTypes());

            if (Ext.getStore("DatasetCollectionStyle").count() > 0) {
                Ext.getCmp("styleeditor-grid").getSelectionModel().select(0);
                Ext.getCmp("savestyle").setDisabled(window.read_only);
            }
            else {
                Ext.getCmp("styles_useforallcollections").setHidden(true);
                Ext.getCmp("rule_name_col_id").setHidden(true);
                Ext.getCmp("rule_name_id").setHidden(true);
            }

            Ext.getCmp("styleresetfilter").setDisabled(true);

            const editMode = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditMode();

            Ext.getCmp("savestyle").setDisabled(!editMode);
        }
        else {
            Ext.Msg.alert("Achtung", "Es muss ein primäres Geometrieattribut vom Typ Punkt, Linie oder Polygon definiert sein!");
        }
    },

    onOpenSldFileEditor: function () {
        const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getPrimaryGeomType();

        if (geomType) {
            let styleSldView = Ext.getCmp("stylesldeditor-window");

            if (styleSldView) {
                styleSldView.destroy();
            }

            styleSldView = Ext.create("styleeditor.StyleSldEditorWindow");
            styleSldView.show();
            styleSldView.setTitle("Style bearbeiten - " + Ext.getCmp("datasetcollectiontitle").getSubmitValue());

            const editor = monaco.editor.create(document.getElementById("sldcontainer"), {
                language: "xml"
            });

            const collection_id = Ext.getCmp("datasetcollectionid").getSubmitValue();
            const collection = Ext.getStore("CollectionsDataset").findRecord("id", collection_id, 0, false, false, true);

            if (collection.data.style_sld) {
                editor.setValue(collection.data.style_sld);
            }

            const styleSldIconsStore = Ext.getStore("StyleSldIcons");

            styleSldIconsStore.removeAll();

            if (collection.data.style_sld_icons) {
                styleSldIconsStore.loadData(collection.data.style_sld_icons);
            }

            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setSldEditor(editor);

            const editMode = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditMode();

            Ext.getCmp("savestylesld").setDisabled(!editMode);
        }
        else {
            Ext.Msg.alert("Achtung", "Es muss ein primäres Geometrieattribut definiert sein!");
        }
    },

    onUploadStyleSldIcon: function (input) {
        if (input.getEl().down("input[type=file]") !== null) {
            const file = input.getEl().down("input[type=file]").dom.files[0];

            if (file.type === "image/jpeg" || file.type === "image/png" || file.type === "image/gif" || file.type === "image/svg+xml" || file.name.indexOf(".ttf") > -1) {
                if (file.size <= 25000) {
                    const reader = new FileReader();

                    reader.onload = function (e) {
                        const style_sld_icons_store = Ext.getStore("StyleSldIcons");

                        style_sld_icons_store.add({file_name: file.name, data_url: e.target.result, isImage: file.type ? true : false});
                    };

                    reader.readAsDataURL(file);
                    reader.onerror = function (error) {
                        console.log("Error: ", error);
                    };
                }
                else {
                    Ext.Msg.alert("Fehler", "Datei darf nicht größer als 25kb sein!");
                }
            }
            else {
                Ext.Msg.alert("Fehler", "Erlaubte Dateiformate: png, jpeg, gif, svg, ttf!");
            }
        }
    },

    onDeleteSldIcon: function (view, recIndex, cellIndex, item, e, record) {
        record.drop();
    },

    onSaveStyleSld: function () {
        const editor = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getSldEditor();
        const style_sld = editor.getValue();
        const collection_id = Ext.getCmp("datasetcollectionid").getSubmitValue();
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const dataset_status = Ext.getCmp("datasetstatus").getValue();
        let style_sld_icons = null;

        if (Ext.getStore("StyleSldIcons").count() > 0) {
            const style_sld_icons_array = [];

            Ext.getStore("StyleSldIcons").each(function (record) {
                style_sld_icons_array.push({file_name: record.data.file_name, data_url: record.data.data_url, isImage: record.data.isImage});
            });

            style_sld_icons = JSON.stringify(style_sld_icons_array);
        }

        Ext.Ajax.request({
            url: "backend/updatestylesld",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                id: collection_id,
                style_sld: style_sld,
                style_sld_icons: style_sld_icons,
                dataset_id: dataset_id,
                last_edited_by: window.auth_user
            },
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Style SLD erfolgreich gespeichert!");

                    Ext.getStore("CollectionsDataset").load({
                        params: {dataset_id: dataset_id, dataset_status: dataset_status},
                        callback: function () {
                            const collection = Ext.getStore("CollectionsDataset").findRecord("id", collection_id, 0, false, false, true);
                            const collection_grid = Ext.getCmp("grid_datasetcollections");
                            const row_collection = collection_grid.store.indexOf(collection);

                            collection_grid.getSelectionModel().deselectAll();
                            collection_grid.getSelectionModel().select(row_collection);
                            collection_grid.ensureVisible(row_collection);
                        }
                    });
                }
                else {
                    Ext.Msg.alert("Fehler", "Style SLD konnte nicht gespeichert werden! <br>" + response_json.message);
                }
            },
            failure: function (response) {
                Ext.Msg.alert("Fehler", "Style SLD konnte nicht gespeichert werden!");
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    onCollectionNameChange: function () {
        if (Ext.getCmp("datasetcollectionid").getSubmitValue() === "0") {
            Ext.getCmp("datasetcollectiondbtable").setValue(Ext.getCmp("datasetcollectionname").getValue());
        }
    },

    onGroupCheck: function (checkbox) {
        if (checkbox.checked) {
            Ext.getCmp("datasetcollectionnamespace").setHidden(true);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(true);
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(true);
            Ext.getCmp("datasetcollectionlegendcontainer").setHidden(true);
            Ext.getCmp("editstyle").setHidden(true);
            Ext.getCmp("editstylesldfile").setHidden(true);
            Ext.getCmp("datasetcollectionapiconstraint").setValue("only_visual");
            Ext.getCmp("datasetcollectioncomplexscheme").setHidden(true);
        }
        else {
            Ext.getCmp("datasetcollectionnamespace").setHidden(false);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(false);
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(false);
            Ext.getCmp("datasetcollectionlegendcontainer").setHidden(false);
            Ext.getCmp("editstyle").setHidden(Ext.getCmp("datasetcollectionusestylesld").getValue());
            Ext.getCmp("editstylesldfile").setHidden(!Ext.getCmp("datasetcollectionusestylesld").getValue());
            Ext.getCmp("datasetcollectioncomplexscheme").setHidden(false);
        }
    },

    onUploadLegend: function (input) {
        if (input.getEl().down("input[type=file]") !== null) {
            const file = input.getEl().down("input[type=file]").dom.files[0];

            if (file.type === "image/jpeg" || file.type === "image/png" || file.type === "image/gif") {
                const reader = new FileReader();

                reader.onload = function (e) {
                    Ext.getCmp("datasetcollectionlegenddataurl").setValue(e.target.result);
                    Ext.getCmp("datasetcollectionlegendcontenttype").setValue(file.type);
                    Ext.getCmp("datasetcollectionlegendfilename").setValue(file.name);
                    Ext.getCmp("datasetcollectiondownloadlegend").setHidden(false);
                    Ext.getCmp("datasetcollectiondeletelegend").setHidden(false);
                };

                reader.readAsDataURL(file);
                reader.onerror = function (error) {
                    console.log("Error: ", error);
                };

                if (input.getEl().down("input[type=file]").dom.files[0] !== undefined) {
                    // https://stackoverflow.com/questions/11118949/extjs-reset-filefield-input
                    const fileField = document.getElementById("datasetcollectionuploadlegend");
                    const parentNod = fileField.parentNode;
                    const tmpForm = document.createElement("form");

                    parentNod.replaceChild(tmpForm, fileField);
                    tmpForm.appendChild(fileField);
                    tmpForm.reset();
                    parentNod.replaceChild(fileField, tmpForm);
                }
            }
            else {
                Ext.Msg.alert("Fehler", "Erlaubte Dateiformate: png, jpeg, gif!");
            }
        }
    },

    onDownloadLegend: function () {
        const download_link = document.createElement("a");

        download_link.href = Ext.getCmp("datasetcollectionlegenddataurl").getSubmitValue().replace(Ext.getCmp("datasetcollectionlegendcontenttype").getSubmitValue(), "image/octet-stream");
        download_link.target = "_blank";
        download_link.download = Ext.getCmp("datasetcollectionlegendfilename").getSubmitValue();
        download_link.click();
        download_link.remove();
    },

    onDeleteLegend: function () {
        Ext.getCmp("datasetcollectionlegenddataurl").setValue("");
        Ext.getCmp("datasetcollectionlegendcontenttype").setValue("");
        Ext.getCmp("datasetcollectionlegendfilename").setValue("");
        Ext.getCmp("datasetcollectiondownloadlegend").setHidden(true);
        Ext.getCmp("datasetcollectiondeletelegend").setHidden(true);
    },

    onIconChange: function (value, metaData, record) {
        if (record.data.group_object === true) {
            return "<div class=\"x-fa fa-folder-open\" style=\"color:#00000066\" data-qtip=\"Gruppenobjekt\"></div>";
        }
        else if (record.data.api_constraint === "only_visual") {
            return "<div class=\"x-fa fa-map\" style=\"color:#00000066\" data-qtip=\"Nur Visualisierung\"></div>";
        }
        else if (record.data.api_constraint === "only_download") {
            return "<div class=\"x-fa fa-download\" style=\"color:#00000066\" data-qtip=\"Nur Download\"></div>";
        }
        else {
            return "<div class=\"x-fa fa-file\" style=\"color:#00000066\" data-qtip=\"Visualisierung & Download\"></div>";
        }
    },

    onServiceChoice: function () {
        let serviceChoiceWindow = Ext.getCmp("collectionimportchoice-window");

        if (serviceChoiceWindow) {
            serviceChoiceWindow.destroy();
        }

        const collectionImportFilter = new Ext.util.Filter({
            id: "collectionImportFilter",
            filterFn: function (item) {
                if (["WMS", "WFS", "WMTS"].includes(item.get("type"))) {
                    return item.get("type");
                }
            }
        });

        Ext.getStore("ServicesDataset").addFilter(collectionImportFilter);


        serviceChoiceWindow = Ext.create("datasetcollections.collectionimportchoice");

        serviceChoiceWindow.show();
    },

    onClose: function () {
        if (Ext.getStore("ServicesDataset").getFilters().length > 0) {
            Ext.getStore("ServicesDataset").removeFilter("collectionImportFilter");
        }
    },

    onShareCollection: function () {
        let shareWindow = Ext.getCmp("share-window");

        if (!shareWindow) {
            shareWindow = Ext.create("datasets.ShareWindow");
        }
        shareWindow.show();

        const baseUrl = window.location.href;
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const collection_id = Ext.getCmp("datasetcollectionid").getSubmitValue();

        Ext.getCmp("shareurlfield").setValue(`${baseUrl}?dataset_id=${dataset_id}&collection_id=${collection_id}`);
    }
});
