Ext.define("UDPManager.view.datasets.datasetcollections.attributeeditor.GetAttributesWindowController", {
    extend: "Ext.app.ViewController",

    alias: "controller.getattributes",

    afterRender: function () {
        const tips = [{
            target: this.lookup("WMSToolTip").el,
            html: "Funktioniert nur mit WMS, wenn der Request GetFeatureInfoSchema unterstützt wird!"
        }];

        this.tips = Ext.Array.map(tips, function (cfg) {
            cfg.showOnTap = true;

            return new Ext.tip.ToolTip(cfg);
        });
    },

    onFilterStore: function () {
        this.store = Ext.getStore("ServicesDataset");

        const customFilter = new Ext.util.Filter({
            id: "ServiceTypeSelectionFilter",
            filterFn: function (item) {
                if (item) {
                    return item.data.type === "WMS" || item.data.type === "WMS-Time" || item.data.type === "WFS" || item.data.type === "WFS-T";
                }
            }
        });

        this.store.addFilter(customFilter);
    },

    onDeleteFilter: function () {
        this.store = Ext.getStore("ServicesDataset");
        this.store.removeFilter("ServiceTypeSelectionFilter");
        Ext.getCmp("getattributesservice").setDisabled(false);
    },

    onGetAttributesService: function () {
        const combobox_selection = Ext.getCmp("getattributesservicetype").getSelection();
        const mb = Ext.MessageBox;

        mb.buttonText.yes = "Ja";
        mb.buttonText.no = "Nein";

        if (combobox_selection) {
            const getAttributesWindow = Ext.getCmp("getattributes-window");
            const layername = Ext.getCmp("layername").getSubmitValue();
            const service_type = combobox_selection.data.type;
            const service_software = combobox_selection.data.software;
            const version = combobox_selection.data.version;
            const externalService = combobox_selection.data.external;

            let url_int = combobox_selection.data.url_int;

            if (((service_type === "WMS" || service_type === "WMS-Time") && service_software === "deegree") || service_type === "WFS" || service_type === "WFS-T") {
                Ext.getCmp("getattributesservice").setDisabled(true);

                if (url_int.slice(-1) === "&") {
                    url_int = url_int.slice(0, -1);
                }

                mb.confirm("Vorhandene Attribute werden überschrieben!", "Attribute trotzdem eintragen?", function (btn) {
                    if (btn === "yes") {
                        Ext.Ajax.request({
                            url: "backend/getattributesservice",
                            method: "POST",
                            headers: {token: window.apiToken},
                            jsonData: {
                                url: url_int,
                                version: version,
                                type: service_type,
                                layername: layername,
                                externalService: externalService
                            },
                            success: function (response) {
                                const response_json = Ext.decode(response.responseText);

                                if (response_json.status === "success" && response_json.result_data.length > 0) {
                                    Ext.getStore("AttributeConfigs").removeAll();
                                    Ext.getStore("AttributeConfigs").add(response_json.result_data);
                                }
                                else if (response_json.status === "success" && response_json.result_data.length === 0) {
                                    Ext.MessageBox.alert("Hinweis", "Keine Attribute abrufbar!");
                                }
                                else if (response_json.length !== 0 && response_json.status === "error") {
                                    Ext.MessageBox.alert("Fehler", response_json.message + "<br>" + response_json.response);
                                }

                                Ext.getCmp("getattributesservice").setDisabled(false);
                                getAttributesWindow.hide();
                                Ext.getStore("ServicesDataset").removeFilter("ServiceTypeSelectionFilter");
                            },
                            failure: function (response) {
                                console.log(Ext.decode(response.responseText));
                                Ext.MessageBox.alert("Fehler", "Abruf fehlgeschlagen!");
                                Ext.getCmp("getattributesservice").setDisabled(false);
                            }
                        });
                    }
                    else {
                        Ext.getCmp("getattributesservice").setDisabled(false);
                        Ext.getCmp("getattributes-window").close();
                    }
                });
            }
            else if ((service_type === "WMS" || service_type === "WMS-Time") && service_software !== "deegree") {
                Ext.MessageBox.alert("Achtung", "GFI Abfrage nur mit deegree Schnittstellen möglich! <br><br> Wenn vorhanden WFS benutzen.");
            }
            else if (!service_type) {
                Ext.MessageBox.alert("Achtung", "Bitte Schnittstellentyp auswählen.");
            }
        }
        else {
            Ext.MessageBox.alert("Achtung", "Bitte Schnittstellentyp auswählen.");
        }
    }
});
