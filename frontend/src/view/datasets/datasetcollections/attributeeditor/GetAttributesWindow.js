Ext.define("UDPManager.view.datasets.datasetcollections.attributeeditor.GetAttributesWindow", {
    extend: "Ext.window.Window",
    id: "getattributes-window",
    alias: "datasetcollections.getattributes",
    height: 150,
    width: 400,
    title: "Attribute abfragen",
    resizable: false,
    closeAction: "hide",
    controller: "getattributes",
    listeners: {
        close: "onDeleteFilter"
    },
    items: [
        {
            xtype: "panel",
            bodyPadding: 10,
            items: [
                {
                    xtype: "component",
                    cls: "x-form-item-label-default",
                    id: "getattributestext",
                    html: "Welcher Schnittstellentyp soll für die Abfrage genutzt werden?"
                },
                {
                    xtype: "combo",
                    allowBlank: false,
                    id: "getattributesservicetype",
                    store: "ServicesDataset",
                    queryMode: "local",
                    displayField: "type",
                    labelWidth: 100,
                    width: 260,
                    reference: "WMSToolTip",
                    listeners: {
                        expand: "onFilterStore"
                    }
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    "->",
                    {
                        id: "getattributemanually",
                        text: "Auswählen",
                        listeners: {
                            click: "onGetAttributesService"
                        }
                    }
                ]
            }]
        }
    ]
});
