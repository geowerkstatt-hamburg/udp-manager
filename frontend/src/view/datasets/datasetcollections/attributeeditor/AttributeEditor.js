Ext.define("UDPManager.view.datasets.datasetcollections.attributeeditor.AttributeEditor", {
    extend: "Ext.window.Window",
    id: "attributeeditor-window",
    alias: "datasetcollections.AttributeEditor",
    width: "96%",
    height: Ext.Element.getViewportHeight(),
    title: "Attribute konfigurieren",
    resizable: false,
    scrollable: true,
    closeAction: "hide",
    controller: "attributeeditor",
    listeners: {
        close: {
            fn: "onWindowClose"
        },
        beforerender: "onBeforeRender"
    },
    viewModel: true,
    modelValidation: true,
    layout: "hbox",
    initComponent: function () {
        this.items = [
            {
                xtype: "panel",
                layout: "column",
                width: "100%",
                height: "100%",
                items: [
                    {
                        xtype: "gridpanel",
                        store: "AttributeConfigs",
                        id: "attributeeditor-grid",
                        reference: "form_attr_conf",
                        style: "border-right: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                        scrollable: true,
                        columnLines: true,
                        columnWidth: 0.5,
                        height: Ext.Element.getViewportHeight() - 38,
                        autoScroll: true,
                        buttonAlign: "left",
                        header: false,
                        viewConfig: {
                            plugins: {
                                ptype: "gridviewdragdrop",
                                dragText: "Drag & Drop zum Umsortieren"
                            },
                            markDirty: false
                        },
                        listeners: {
                            select: "onGridAttributeSelected"
                        },
                        columns: [
                            {
                                id: "gridrownumberer",
                                renderer: "onRenderCell",
                                width: 45
                            },
                            {
                                header: "Name (Quelle)",
                                dataIndex: "attr_name_source",
                                align: "left",
                                flex: 1,
                                sortable: false,
                                cls: "multiline",
                                renderer: "onRenderCell"
                            },
                            {
                                header: "Name (DB)",
                                dataIndex: "attr_name_db",
                                align: "left",
                                flex: 1,
                                sortable: false,
                                cls: "multiline",
                                renderer: "onRenderCell"
                            },
                            {
                                header: "Name (API)",
                                dataIndex: "attr_name_service",
                                align: "left",
                                flex: 1,
                                sortable: false,
                                cls: "multiline",
                                renderer: "onRenderCell"
                            }, {
                                header: "Label",
                                dataIndex: "attr_name_masterportal",
                                flex: 1,
                                sortable: false,
                                align: "left",
                                cls: "multiline",
                                renderer: "onRenderCell"
                            }, {
                                header: "Datentyp",
                                dataIndex: "attr_datatype",
                                width: 115,
                                sortable: false,
                                align: "left",
                                renderer: "onRenderCell"
                            },
                            {
                                xtype: "checkcolumn",
                                dataIndex: "pk",
                                text: "PS",
                                tooltip: "Primärschlüssel",
                                align: "center",
                                headerCheckbox: false,
                                sortable: false,
                                width: 40,
                                processEvent: Ext.emptyFn,
                                listeners: {
                                    checkchange: "onlyCheckOneCheckbox"
                                }
                            }, {
                                xtype: "checkcolumn",
                                dataIndex: "primary_geom",
                                text: "PG",
                                tooltip: "Primäre Geometrie",
                                align: "center",
                                headerCheckbox: false,
                                sortable: false,
                                width: 40,
                                cls: "multiline",
                                processEvent: Ext.emptyFn,
                                listeners: {
                                    checkchange: "onlyCheckOneCheckbox"
                                }
                            }, {
                                xtype: "checkcolumn",
                                dataIndex: "primary_date",
                                text: "PZ",
                                tooltip: "Primärer Zeitindex",
                                align: "center",
                                headerCheckbox: false,
                                sortable: false,
                                width: 40,
                                cls: "multiline",
                                processEvent: Ext.emptyFn,
                                listeners: {
                                    checkchange: "onlyCheckOneCheckbox"
                                }
                            },
                            {
                                id: "attrchangeicon",
                                xtype: "actioncolumn",
                                width: 40,
                                menuDisabled: true,
                                sortable: false,
                                resizeable: false,
                                align: "left",
                                renderer: function (value, metadata, record) {
                                    if (record.get("attr_changed") === true) {
                                        this.items[0].iconCls = "x-fa fa-wrench";
                                        this.items[0].tooltip = "Attribut bearbeitet<br>Für mehr Informationen siehe Änderungslog";
                                    }
                                    else {
                                        this.items[0].iconCls = "";
                                        this.items[0].tooltip = "";
                                    }
                                },
                                items: [
                                    {
                                        iconCls: "x-fa fa-wrench",
                                        tooltip: "Änderungen sichtbar im Änderungslog",
                                        handler: "onClickAttrChangelog"
                                    }
                                ]
                            }
                        ],
                        dockedItems: [{
                            xtype: "toolbar",
                            dock: "bottom",
                            border: true,
                            style: "border-top: 1px solid #cfcfcf !important;",
                            items: [
                                {
                                    id: "addattributerow",
                                    iconCls: "x-fa fa-plus",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    tooltip: "Fügt eine leere Zeile am Ende der Tabelle ein.",
                                    hidden: window.read_only,
                                    handler: "onAddRow"
                                },
                                {
                                    id: "deleteattributerow",
                                    iconCls: "x-fa fa-trash",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    tooltip: "Löscht die ausgewählte Zeile.",
                                    hidden: window.read_only,
                                    handler: "onDeleteRow"
                                },
                                {
                                    id: "saveattributesascsv",
                                    iconCls: "x-fa fa-file-csv",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    tooltip: "Tabelle im CSV Format exportieren",
                                    handler: "onCsvExport"
                                },
                                {
                                    id: "extendedfn",
                                    iconCls: "x-fa fa-bars",
                                    tooltip: "weitere Funktionen",
                                    arrowVisible: false,
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    menu: [
                                        {
                                            text: "Attribute abfragen (Schnittstelle)",
                                            id: "getattributesservice",
                                            tooltip: "Attribute über Schnittstelle abfragen <br> Z. B. bei externen Diensten nutzbar, um die Attribute per WMS oder WFS Schnittestelle einzutragen.",
                                            hidden: window.read_only,
                                            disabled: true,
                                            handler: "onOpenGetAttributesWindow"
                                        }, {
                                            text: "Attribute abfragen (DB)",
                                            id: "getattributesdb",
                                            tooltip: "Attribute aus Datenbank abfragen <br> Für die Abfrage wird die am Datensatz hinterlegte Datenbankverbindung genutzt. Es wird auf das Schema und die Tabelle der Collection zugegriffen.",
                                            hidden: window.read_only,
                                            disabled: true,
                                            handler: "onGetAttributesDb"
                                        }
                                    ]
                                },
                                {
                                    xtype: "tbspacer",
                                    width: "1%"
                                },
                                {
                                    xtype: "component",
                                    id: "attrdragdropinfo",
                                    cls: "x-form-item-label-default",
                                    html: "Drag & Drop zum Umsortieren"
                                }
                            ]
                        }]
                    },
                    {
                        xtype: "tabpanel",
                        id: "attr_window_tabs",
                        activeTab: 0,
                        tabPosition: "left",
                        tabRotation: 0,
                        columnWidth: 0.5,
                        items: [
                            {
                                xtype: "container",
                                style: "white-space: pre-line;border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                title: "Attribut-Details",
                                items: [
                                    {
                                        xtype: "form",
                                        id: "attributeeditorform",
                                        disabled: true,
                                        defaultType: "textfield",
                                        scrollable: true,
                                        autoScroll: true,
                                        bodyPadding: 20,
                                        flex: 1,
                                        height: Ext.Element.getViewportHeight() - 38,
                                        items: [
                                            {
                                                fieldLabel: "Name (Quelle)",
                                                store: ["ignore"],
                                                editable: true,
                                                labelWidth: 170,
                                                width: 500,
                                                id: "attr_name_source",
                                                name: "attr_name_source",
                                                bind: "{form_attr_conf.selection.attr_name_source}",
                                                listeners: {
                                                    blur: "onEditField"
                                                }
                                            },
                                            {
                                                xtype: "fieldcontainer",
                                                layout: "hbox",
                                                width: "100%",
                                                items: [
                                                    {
                                                        xtype: "textfield",
                                                        fieldLabel: "Name (DB)",
                                                        editable: true,
                                                        labelWidth: 170,
                                                        width: 500,
                                                        requiredField: true,
                                                        id: "attr_name_db",
                                                        name: "attr_name_db",
                                                        bind: "{form_attr_conf.selection.attr_name_db}",
                                                        listeners: {
                                                            blur: "onEditField",
                                                            focus: "getOldValue"
                                                        }
                                                    },
                                                    {
                                                        boxLabel: "Nicht relevant für Datenintegration",
                                                        id: "attr_name_db_ignore",
                                                        xtype: "checkboxfield",
                                                        labelWidth: 100,
                                                        name: "attr_name_db_ignore",
                                                        flex: 1,
                                                        margin: "0 0 0 20",
                                                        bind: "{form_attr_conf.selection.attr_name_db_ignore}",
                                                        listeners: {
                                                            change: "changeIgnore"
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldcontainer",
                                                layout: "hbox",
                                                width: "100%",
                                                items: [
                                                    {
                                                        xtype: "textfield",
                                                        fieldLabel: "Name (API)",
                                                        editable: true,
                                                        labelWidth: 170,
                                                        width: 500,
                                                        requiredField: true,
                                                        id: "attr_name_service",
                                                        name: "attr_name_service",
                                                        bind: "{form_attr_conf.selection.attr_name_service}",
                                                        listeners: {
                                                            blur: "onEditField",
                                                            focus: "getOldValue"
                                                        }
                                                    },
                                                    {
                                                        boxLabel: "Nicht relevant für die API",
                                                        id: "attr_name_service_ignore",
                                                        xtype: "checkboxfield",
                                                        labelWidth: 100,
                                                        name: "attr_name_service_ignore",
                                                        flex: 1,
                                                        margin: "0 0 0 20",
                                                        bind: "{form_attr_conf.selection.attr_name_service_ignore}",
                                                        listeners: {
                                                            change: "changeIgnore"
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldcontainer",
                                                layout: "hbox",
                                                width: "100%",
                                                items: [
                                                    {
                                                        xtype: "textfield",
                                                        fieldLabel: "Label",
                                                        editable: true,
                                                        labelWidth: 170,
                                                        width: 500,
                                                        requiredField: true,
                                                        id: "attr_name_masterportal",
                                                        name: "attr_name_masterportal",
                                                        bind: "{form_attr_conf.selection.attr_name_masterportal}",
                                                        listeners: {
                                                            blur: "onEditField",
                                                            focus: "getOldValue"
                                                        }
                                                    },
                                                    {
                                                        boxLabel: "Nicht relevant für GFI",
                                                        id: "attr_name_masterportal_ignore",
                                                        xtype: "checkboxfield",
                                                        labelWidth: 100,
                                                        name: "attr_name_masterportal_ignore",
                                                        flex: 1,
                                                        margin: "0 0 0 20",
                                                        bind: "{form_attr_conf.selection.attr_name_masterportal_ignore}"
                                                    }
                                                ]
                                            },
                                            {
                                                fieldLabel: "Datentyp",
                                                xtype: "combobox",
                                                store: ["text", "number", "date", "boolean", "geometry [point]", "geometry [line]", "geometry [polygon]"],
                                                editable: false,
                                                labelWidth: 170,
                                                width: 360,
                                                requiredField: true,
                                                id: "attr_datatype",
                                                name: "attr_datatype",
                                                bind: "{form_attr_conf.selection.attr_datatype}"
                                            },
                                            {
                                                fieldLabel: "PS - Primärschlüssel",
                                                xtype: "checkbox",
                                                labelWidth: 170,
                                                width: 360,
                                                id: "attr_pk",
                                                name: "pk",
                                                bind: "{form_attr_conf.selection.pk}",
                                                listeners: {
                                                    focusenter: "fireCheckedChangeEvent"
                                                }
                                            },
                                            {
                                                fieldLabel: "PG - Primäre Geometrie",
                                                xtype: "checkbox",
                                                labelWidth: 170,
                                                width: 360,
                                                id: "attr_primary_geom",
                                                name: "primary_geom",
                                                bind: "{form_attr_conf.selection.primary_geom}",
                                                listeners: {
                                                    focusenter: "fireCheckedChangeEvent"
                                                }
                                            },
                                            {
                                                fieldLabel: "PZ - Primärer Zeitindex",
                                                xtype: "checkbox",
                                                labelWidth: 170,
                                                width: 360,
                                                id: "attr_primary_date",
                                                name: "primary_date",
                                                bind: "{form_attr_conf.selection.primary_date}",
                                                listeners: {
                                                    focusenter: "fireCheckedChangeEvent"
                                                }
                                            },
                                            {
                                                xtype: "fieldset",
                                                title: "Regeln zur Datenintegration",
                                                padding: "0 5 0 5",
                                                cls: "prio-fieldset",
                                                width: "100%",
                                                collapsible: true,
                                                collapsed: false,
                                                items: [
                                                    {
                                                        fieldLabel: "Pflicht",
                                                        xtype: "checkbox",
                                                        labelWidth: 165,
                                                        width: "100%",
                                                        id: "rule_mandatory",
                                                        name: "rule_mandatory",
                                                        bind: "{form_attr_conf.selection.rule_mandatory}"
                                                    },
                                                    {
                                                        fieldLabel: "Filter für Visualisierung",
                                                        xtype: "textarea",
                                                        labelWidth: 165,
                                                        width: "100%",
                                                        id: "rule_filter_vis",
                                                        name: "rule_filter_vis",
                                                        bind: "{form_attr_conf.selection.rule_filter_vis}"
                                                    },
                                                    {
                                                        fieldLabel: "Filter für Daten",
                                                        xtype: "textarea",
                                                        labelWidth: 165,
                                                        width: "100%",
                                                        id: "rule_filter_data",
                                                        name: "rule_filter_data",
                                                        bind: "{form_attr_conf.selection.rule_filter_data}",
                                                        autoEl: {
                                                            tag: "div",
                                                            "data-qtip": "Werte mit Pipe | trennen"
                                                        }
                                                    },
                                                    {
                                                        fieldLabel: "Attributreihenfolge",
                                                        xtype: "combobox",
                                                        store: ["Adresse_Str", "Adresse_Hausnr", "Adresse_Zusatz", "Ort_PLZ", "Ort"],
                                                        editable: true,
                                                        labelWidth: 165,
                                                        width: 360,
                                                        id: "rule_order",
                                                        name: "rule_order",
                                                        bind: "{form_attr_conf.selection.rule_order}",
                                                        listeners: {
                                                            blur: "onEditField"
                                                        }
                                                    },
                                                    {
                                                        fieldLabel: "Datum",
                                                        xtype: "combobox",
                                                        store: ["jjjjmmtt", "jjjj-mm-tt", "jjjj.mm.tt", "tt.mm.jjjj", "tt-mm-jjjj"],
                                                        editable: false,
                                                        labelWidth: 165,
                                                        width: 360,
                                                        id: "rule_date",
                                                        name: "rule_date",
                                                        bind: "{form_attr_conf.selection.rule_date}"
                                                    },
                                                    {
                                                        fieldLabel: "Georeferenzierung",
                                                        xtype: "combobox",
                                                        store: ["Straßenname", "Hausnummer", "Hausnr_Zusatz", "PLZ", "Ort", "Rechtswert", "Hochwert"],
                                                        editable: false,
                                                        labelWidth: 165,
                                                        width: 360,
                                                        id: "rule_georef",
                                                        name: "rule_georef",
                                                        bind: "{form_attr_conf.selection.rule_georef}"
                                                    },
                                                    {
                                                        fieldLabel: "Anmerkungen",
                                                        xtype: "textarea",
                                                        labelWidth: 165,
                                                        width: "100%",
                                                        id: "rule_note",
                                                        name: "rule_note",
                                                        bind: "{form_attr_conf.selection.rule_note}",
                                                        autoEl: {
                                                            tag: "div",
                                                            "data-qtip": "Anmerkungen für die Bearbeitung bei der Datenintegration"
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                fieldLabel: "Einheit",
                                                labelWidth: 170,
                                                width: 360,
                                                id: "attr_unit",
                                                name: "unit",
                                                bind: "{form_attr_conf.selection.unit}"
                                            },
                                            {
                                                fieldLabel: "Beschreibung",
                                                xtype: "textareafield",
                                                grow: false,
                                                labelWidth: 170,
                                                width: "100%",
                                                id: "attr_description",
                                                name: "description",
                                                bind: "{form_attr_conf.selection.description}",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Beschreibung des Attributs - wird für die Ausgabe des Datenschemas genutzt"
                                                }
                                            }
                                        ],
                                        dockedItems: [{
                                            xtype: "toolbar",
                                            dock: "bottom",
                                            border: true,
                                            style: "border-top: 1px solid #cfcfcf !important;",
                                            items: [
                                                {
                                                    xtype: "component",
                                                    html: "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Pflichtfeld</span>"
                                                },
                                                {
                                                    xtype: "tbspacer",
                                                    flex: 1
                                                },
                                                {
                                                    xtype: "checkboxfield",
                                                    id: "useforallcollections",
                                                    fieldLabel: "Konfiguration für alle Collections übernehmen",
                                                    hidden: window.read_only,
                                                    labelWidth: 260
                                                },
                                                {
                                                    text: "Speichern",
                                                    id: "attributessave",
                                                    tooltip: "Speichert die Attributzuordnung",
                                                    scale: "medium",
                                                    cls: "btn-save",
                                                    hidden: window.read_only,
                                                    handler: "onSaveAttributes"
                                                }
                                            ]
                                        }]
                                    }
                                ]
                            },
                            {
                                xtype: "panel",
                                style: "white-space: pre-line;border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                title: "Änderungslog",
                                id: "attributeeditorchanges",
                                height: Ext.Element.getViewportHeight() - 38,
                                bodyPadding: 20,
                                items: [
                                    {
                                        xtype: "component",
                                        id: "attrchangeslabel",
                                        width: "100%",
                                        html: ""
                                    },
                                    {
                                        xtype: "component",
                                        id: "attrchanges",
                                        width: "100%",
                                        height: "100%",
                                        html: "",
                                        scrollable: true
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }
});
