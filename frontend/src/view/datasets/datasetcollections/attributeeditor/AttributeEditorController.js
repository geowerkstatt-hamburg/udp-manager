Ext.define("UDPManager.view.datasets.datasetcollections.attributeeditor.AttributeEditorController", {
    extend: "Ext.app.ViewController",
    alias: "controller.attributeeditor",

    onWindowClose: function (attributeEditor) {
        const selectedCollection = Ext.getCmp("grid_datasetcollections").getSelectionModel().getSelection()[0].data;
        const attributeConfigStore = Ext.getStore("AttributeConfigs");

        attributeConfigStore.removeAll();
        if (selectedCollection.attribute_config) {
            selectedCollection.attribute_config.forEach(config => {
                if (typeof config.attr_name_masterportal === "object" && config.attr_name_masterportal !== null) {
                    config.attr_name_masterportal = JSON.stringify(config.attr_name_masterportal);
                }
            });
            attributeConfigStore.loadData(selectedCollection.attribute_config);
        }
        attributeEditor.hide();
    },

    onAddRow: function () {
        const rec = new UDPManager.model.UDPManagerModel({
            attr_name_source: "",
            attr_name_db: "",
            attr_name_service: "",
            attr_name_masterportal: "",
            attr_datatype: "",
            description: "",
            rule_mandatory: false,
            rule_filter_vis: "",
            rule_filter_data: "",
            rule_order: "",
            rule_date: "",
            rule_georef: "",
            rule_note: "",
            unit: "",
            pk: false,
            primary_geom: false,
            primary_date: false
        });
        const count = Ext.getStore("AttributeConfigs").count();

        Ext.getStore("AttributeConfigs").insert(count, rec);
        Ext.getCmp("attributeeditor-grid").getSelectionModel().select(count);
    },

    onOpenGetAttributesWindow: function () {
        Ext.getCmp("getattributesservice").setDisabled(true);
        let getAttributesWindow = Ext.getCmp("getattributes-window");

        if (getAttributesWindow) {
            getAttributesWindow.destroy();
        }

        getAttributesWindow = Ext.create("datasetcollections.getattributes");

        getAttributesWindow.show();
    },

    onGetAttributesDb: function () {
        Ext.getCmp("getattributesdb").setDisabled(true);

        const db_schema = Ext.getCmp("datasetcollectiondbschema").getValue();
        const db_table = Ext.getCmp("datasetcollectiondbtable").getValue();
        const db_connection_r_id = Ext.getCmp("datasetdbconnectionrid").getSubmitValue();
        const mb = Ext.MessageBox;

        mb.buttonText.yes = "Ja";
        mb.buttonText.no = "Nein";

        mb.confirm("Vorhandene Attribute werden überschrieben!", "Attribute trotzdem eintragen?", function (btn) {
            if (btn === "yes") {
                Ext.Ajax.request({
                    url: "backend/getattributesdb",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        db_schema: db_schema,
                        db_table: db_table,
                        db_connection_r_id: db_connection_r_id
                    },
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);

                        if (response_json.status === "success" && response_json.result_data.length > 0) {
                            Ext.getStore("AttributeConfigs").removeAll();
                            Ext.getStore("AttributeConfigs").add(response_json.result_data);
                        }
                        else if (response_json.status === "success" && response_json.result_data.length === 0) {
                            Ext.MessageBox.alert("Hinweis", "Keine Attribute abrufbar! <br> Bitte Namen der Tabelle und des Schemas prüfen.");
                        }
                        else if (response_json.length !== 0 && response_json.status === "error") {
                            Ext.MessageBox.alert("Fehler", response_json.message + "<br>" + response_json.response);
                        }
                        Ext.getCmp("getattributesdb").setDisabled(false);
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Abruf fehlgeschlagen!");
                        Ext.getCmp("getattributesdb").setDisabled(false);
                    }
                });
            }
            else {
                Ext.getCmp("getattributesdb").setDisabled(false);
            }
        });
    },

    onSaveAttributes: function () {
        let attribute_config = "";
        const useforallcollections = Ext.getCmp("useforallcollections").getValue();
        const me = this;
        const scope_id = "layerjsonupdateall";

        if (Ext.getStore("AttributeConfigs").count() > 0) {
            const attribute_config_array = [];

            Ext.getStore("AttributeConfigs").each(function (record) {
                const attribute_config_json = {};

                if (!record.data.attr_name_source) {
                    record.data.attr_name_source = null;
                }
                if (!record.data.attr_name_db) {
                    record.data.attr_name_db = "ignore";
                }
                if (!record.data.attr_name_db_ignore) {
                    record.data.attr_name_db_ignore = false;
                }
                if (!record.data.attr_name_service) {
                    record.data.attr_name_service = "ignore";
                }
                if (!record.data.attr_name_service_ignore) {
                    record.data.attr_name_service_ignore = false;
                }
                if (!record.data.attr_name_masterportal) {
                    record.data.attr_name_masterportal = null;
                }
                if (!record.data.attr_name_masterportal_ignore) {
                    record.data.attr_name_masterportal_ignore = false;
                }
                if (!record.data.attr_datatype) {
                    record.data.attr_datatype = null;
                }
                if (!record.data.description) {
                    record.data.description = null;
                }
                if (!record.data.pk) {
                    record.data.pk = false;
                }
                if (!record.data.primary_geom) {
                    record.data.primary_geom = false;
                }
                if (!record.data.primary_date) {
                    record.data.primary_date = false;
                }
                if (!record.data.unit) {
                    record.data.unit = null;
                }
                if (!record.data.rule_mandatory) {
                    record.data.rule_mandatory = false;
                }

                if (!record.data.rule_filter_vis) {
                    record.data.rule_filter_vis = null;
                }

                if (!record.data.rule_filter_data) {
                    record.data.rule_filter_data = null;
                }

                if (!record.data.rule_order) {
                    record.data.rule_order = null;
                }

                if (!record.data.rule_date) {
                    record.data.rule_date = null;
                }

                if (!record.data.rule_georef) {
                    record.data.rule_georef = null;
                }

                if (!record.data.rule_note) {
                    record.data.rule_note = null;
                }
                if (record.data.attr_name_db === "ignore") {
                    record.data.attr_name_db_ignore = true;
                    record.data.attr_name_service_ignore = true;
                    record.data.attr_name_masterportal_ignore = true;
                    record.data.attr_name_service === "ignore";
                    record.data.attr_name_masterportal === "ignore";
                }
                else {
                    record.data.attr_name_db_ignore = false;
                }
                if (record.data.attr_name_service === "ignore") {
                    record.data.attr_name_service_ignore = true;
                    record.data.attr_name_masterportal_ignore = true;
                    record.data.attr_name_masterportal === "ignore";
                }
                else {
                    record.data.attr_name_service_ignore = false;
                }
                if (record.data.attr_name_masterportal === "ignore") {
                    record.data.attr_name_masterportal_ignore = true;
                }

                attribute_config_json.attr_name_source = record.data.attr_name_source;
                attribute_config_json.attr_name_db = record.data.attr_name_db;
                attribute_config_json.attr_name_db_ignore = record.data.attr_name_db_ignore;
                attribute_config_json.attr_name_service = record.data.attr_name_service;
                attribute_config_json.attr_name_service_ignore = record.data.attr_name_service_ignore;
                if (record.data.attr_name_masterportal && record.data.attr_name_masterportal.trim().startsWith("{")) {
                    attribute_config_json.attr_name_masterportal = JSON.parse(record.data.attr_name_masterportal);
                }
                attribute_config_json.attr_name_masterportal_ignore = record.data.attr_name_masterportal_ignore;
                attribute_config_json.attr_name_masterportal = record.data.attr_name_masterportal;
                attribute_config_json.attr_datatype = record.data.attr_datatype;
                attribute_config_json.description = record.data.description;
                attribute_config_json.pk = record.data.pk;
                attribute_config_json.primary_geom = record.data.primary_geom;
                attribute_config_json.primary_date = record.data.primary_date;
                attribute_config_json.unit = record.data.unit;
                attribute_config_json.rule_mandatory = record.data.rule_mandatory;
                attribute_config_json.rule_filter_vis = record.data.rule_filter_vis;
                attribute_config_json.rule_filter_data = record.data.rule_filter_data;
                attribute_config_json.rule_order = record.data.rule_order;
                attribute_config_json.rule_date = record.data.rule_date;
                attribute_config_json.rule_georef = record.data.rule_georef;
                attribute_config_json.rule_note = record.data.rule_note;
                attribute_config_array.push(attribute_config_json);
            });

            attribute_config = me._removeTrailingWhitespacesInAttributeMap(attribute_config_array);
        }
        else {
            attribute_config = null;
        }

        // collects the data types for all attributes to check if a data type is set for all attributes
        const attr_config_datatypes = Ext.getStore("AttributeConfigs").data.items.map((a) => a.data.attr_datatype);
        const attr_config_name_service = Ext.getStore("AttributeConfigs").data.items.map((a) => a.data.attr_name_service).filter((b) => b !== "ignore");
        const attr_config_name_masterportal = Ext.getStore("AttributeConfigs").data.items.map((a) => a.data.attr_name_masterportal);
        const attr_config_pk = Ext.getStore("AttributeConfigs").data.items.map((a) => a.data.pk).filter((b) => b);
        const attr_config_pg = Ext.getStore("AttributeConfigs").data.items.map((a) => a.data.primary_geom).filter((b) => b);
        const attr_config_pz = Ext.getStore("AttributeConfigs").data.items.map((a) => a.data.primary_date).filter((b) => b);

        // input validation tests
        if (attr_config_name_service.some((e, i, arr) => arr.indexOf(e) !== i)) {
            Ext.MessageBox.alert("Fehler", "Die Attributliste der API Namen enthält Duplikate. Bitte entfernen!");
        }
        else if (attr_config_name_service.some((e) => e.match(/[^a-z0-9_]/))) {
            Ext.MessageBox.alert("Fehler", "Mit Ausnahme von UNDERSCORE sind Sonderzeichen und Großschreibung für 'Name (API)' nicht zulässig. Um mehrere Wörter zu verbinden bitte Snakecase benutzen!");
        }
        else if (attr_config_datatypes.includes(null)) {
            Ext.MessageBox.alert("Fehler", "Der Datentyp muss für jedes Attribut gesetzt sein!");
        }
        else if (attr_config_name_masterportal.includes(null)) {
            Ext.MessageBox.alert("Fehler", "Das Label muss für jedes Attribut gesetzt sein!");
        }
        else if (attr_config_pk.length === 0) {
            Ext.MessageBox.alert("Fehler", "Ein Attribut muss als Primärschlüssel markiert sein!");
        }
        else if (attr_config_pg.length === 0 && attr_config_datatypes.some(e => e.includes("geometry"))) {
            Ext.MessageBox.alert("Fehler", "Mindestens ein Geometrieattribut ist vorhanden. Mindestens ein Attribut muss als primäre Geometrie markiert sein!");
        }
        else if (attr_config_pz.length === 0 && attr_config_datatypes.includes("date")) {
            Ext.MessageBox.alert("Fehler", "Mindestens ein Zeitattribut ist vorhanden. Mindestens ein Attribut muss als primärer Zeitindex markiert sein!");
        }
        else {
            Ext.Ajax.request({
                url: "backend/updateattributeconfig",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    collection_id: Ext.getCmp("datasetcollectionid").getSubmitValue(),
                    dataset_id: Ext.getCmp("datasetid").getValue(),
                    last_edited_by: window.auth_user,
                    attribute_config: attribute_config,
                    useforallcollections: useforallcollections
                },
                success: function (response) {
                    const collectiongrid = Ext.getCmp("grid_datasetcollections");
                    const scroll_x = collectiongrid.getView().getScrollX();
                    const scroll_y = collectiongrid.getView().getScrollY();
                    const selectedCollection = collectiongrid.getSelectionModel().getSelection()[0].data;
                    const dataset_status = Ext.getCmp("datasetstatus").getValue();
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        Ext.getStore("CollectionsDataset").load({
                            params: {dataset_id: selectedCollection.dataset_id, dataset_status: dataset_status},
                            callback: function (records) {
                                collectiongrid.getView().setScrollX(scroll_x);
                                collectiongrid.getView().setScrollY(scroll_y);

                                if (records.length > 0) {
                                    for (let i = 0; i < records.length; i++) {
                                        if (records[i].data.id === selectedCollection.id) {
                                            const row = collectiongrid.store.indexOf(records[i]);

                                            collectiongrid.getSelectionModel().select(row);
                                            collectiongrid.ensureVisible(row);
                                            Ext.getCmp("attributeeditor-grid").getView().refresh();
                                            break;
                                        }
                                    }
                                    Ext.getCmp("attributeeditor-window").destroy();
                                }
                            }
                        });

                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").onRecreateJson(scope_id, "dev");
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Attribute erfolgreich gespeichert!");
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                },
                failure: function (response) {
                    Ext.MessageBox.alert("Status", "Es ist ein Fehler aufgetreten!");
                    console.log(response.responseText);
                }
            });
        }
    },

    onMoveDown: function (grid, rowIndex) {
        const record = grid.getStore().getAt(rowIndex);

        if (!record) {
            return;
        }

        let index = rowIndex;

        if (index < grid.getStore().getCount()) {
            index++;
            grid.getStore().removeAt(rowIndex);
            grid.getStore().insert(index, record);
            grid.getSelectionModel().select(index, true);
        }
    },

    onMoveUp: function (grid, rowIndex) {
        const record = grid.getStore().getAt(rowIndex);

        if (!record) {
            return;
        }

        let index = rowIndex;

        if (index > 0) {
            index--;
            grid.getStore().removeAt(rowIndex);
            grid.getStore().insert(index, record);
            grid.getSelectionModel().select(index, true);
        }
    },

    onDeleteRow: function () {
        const gridrow = Ext.getCmp("attributeeditor-grid").getSelectionModel().getSelection()[0];

        gridrow.drop();
    },

    fireCheckedChangeEvent: function (me) {
        const gridstore = Ext.getStore("AttributeConfigs");
        const gridrow = Ext.getCmp("attributeeditor-grid").getSelectionModel().getSelection()[0];
        const gridcolumnname = me.name;

        gridstore.each(function (grid_record) {
            if (grid_record.data[gridcolumnname] && gridrow.internalId !== grid_record.internalId) {
                grid_record.data[gridcolumnname] = false;
            }
        });

        Ext.getCmp("attributeeditor-grid").getView().refresh();
    },

    onlyCheckOneCheckbox: function (me, rowIndex, checked, record) {
        const gridstore = Ext.getStore("AttributeConfigs");
        const gridcolumn = me.dataIndex;

        gridstore.each(function (grid_record) {
            if (grid_record.data[gridcolumn] && grid_record.internalId !== record.internalId) {
                grid_record.data[gridcolumn] = false;
            }
        });

        Ext.getCmp("attributeeditor-grid").getView().refresh();
    },

    /**
     * Retrieves the old value of the specified field in the Attribute Config grid.
     * As well as all values for the specified attribute name.
     * Stores it in the specified attribute for the blur event of the textfield.
     *
     * @param {Ext.form.field.Base} field - The field for which to retrieve old values.
     * @returns {void}
     */
    getOldValue: function (field) {
        const gridstore = Ext.getStore("AttributeConfigs");
        const gridvalues = [];

        gridstore.each(function (grid_record) {
            if (grid_record.data[field.id] !== field.value) {
                gridvalues.push(grid_record.data[field.id]);
            }
        });

        field.oldGridValues = gridvalues;
        field.oldValue = field.value;
    },

    /**
     * Handles editing of attribute names (service, db) in the attribute editor.
     * Checks for duplicate values and invalid characters.
     *
     * @param {Ext.form.field.Base} field - The field being edited.
     * @returns {void}
     */
    onEditField: function (field) {
        const oldGridValues = field.oldGridValues;

        // switch cells "name (db, api, portal)" to ignore if the cell "rule order" has changed
        if (field.id === "rule_order") {
            const rule_order_values = Ext.getCmp("rule_order").getStore().config.data.flat();

            if (rule_order_values.includes(field.value)) {
                Ext.getCmp("attr_name_db").setValue("ignore");
                Ext.getCmp("attr_name_service").setValue("ignore");
                Ext.getCmp("attr_name_masterportal").setValue("ignore");
                Ext.getCmp("attr_name_db_ignore").setValue(true);
                Ext.getCmp("attr_name_service_ignore").setValue(true);
                Ext.getCmp("attr_name_masterportal_ignore").setValue(true);
                Ext.getCmp("attr_name_db_ignore").setReadOnly(true);
                Ext.getCmp("attr_name_service_ignore").setReadOnly(true);
                Ext.getCmp("attr_name_masterportal_ignore").setReadOnly(true);
            }
        }

        if ((field.id === "attr_name_service" || field.id === "attr_name_db") && oldGridValues) {
            // Check for duplicate values in the grid
            oldGridValues.forEach(function (grid_value) {
                if (grid_value === field.value) {
                    Ext.MessageBox.alert("Fehler", `${field.fieldLabel} bereits eingetragen!`);
                    Ext.getCmp(field.id).setValue(field.oldValue);
                }
            });

            // Replace umlauts with their corresponding ASCII characters
            Ext.getCmp(field.id).setValue(field.value.replace(/ü/gi, "ue"));
            Ext.getCmp(field.id).setValue(field.value.replace(/ö/gi, "oe"));
            Ext.getCmp(field.id).setValue(field.value.replace(/ä/gi, "ae"));
            Ext.getCmp(field.id).setValue(field.value.replace(/ß/gi, "ss"));

            // Check for invalid characters in the edited value
            if (!field.value.match(/^(?![0-9])[a-z0-9_]*$/)) {
                Ext.MessageBox.alert("Fehler", `Mit Ausnahme von UNDERSCORE sind Sonderzeichen und Großschreibung für  ${field.fieldLabel} nicht zulässig. Um mehrere Wörter zu verbinden bitte Snakecase benutzen!`);
                Ext.getCmp(field.id).setValue(field.oldValue);
            }
        }

        if (field.id === "attr_name_source" || field.id === "attr_name_db") {
            // if other name fields are empty, fill them with content of source name field
            const gridstore = Ext.getStore("AttributeConfigs");
            const gridvaluesdb = [];
            const gridvaluesservice = [];

            gridstore.each(function (grid_record) {
                gridvaluesdb.push(grid_record.data.attr_name_db);
                gridvaluesservice.push(grid_record.data.attr_name_service);
            });

            const matcher = !field.value.toLowerCase().match(/^(?![0-9])[a-z0-9_]*$/);
            const duplicatesdb = gridvaluesdb.includes(field.value.toLowerCase());
            const duplicatesservice = gridvaluesservice.includes(field.value.toLowerCase());

            if (field.id === "attr_name_source" && (Ext.getCmp("attr_name_db").getValue() === "" && Ext.getCmp("attr_name_service").getValue() === "")) {
                if (!matcher && !duplicatesdb && !duplicatesservice) {
                    Ext.getCmp("attr_name_db").setValue(field.value.toLowerCase());
                    Ext.getCmp("attr_name_service").setValue(field.value.toLowerCase());
                }
            }
            else if (field.id === "attr_name_db" && (Ext.getCmp("attr_name_service").getValue() === "")) {
                if (!matcher && !duplicatesservice) {
                    Ext.getCmp("attr_name_service").setValue(field.value.toLowerCase());
                }
            }
        }

        if (field.id === "attr_name_masterportal") {
            if (!field.value) {
                Ext.MessageBox.alert("Fehler", `Das ${field.fieldLabel} muss immer gefüllt sein!`);
            }
        }
    },

    onEditCells: function (editor, context) {
        const attribute_source_name = context.value;
        const gridstore = Ext.getStore("AttributeConfigs");

        // edit multiple cells "name (db, api, portal)" if the cell "name (source)" has changed
        gridstore.each(function (grid_record) {
            if (context.field === "attr_name_source") {
                if (grid_record.internalId === context.record.internalId) {
                    if (grid_record.data.attr_name_db === "") {
                        grid_record.data.attr_name_db = attribute_source_name.toLowerCase();
                    }
                    if (grid_record.data.attr_name_service === "") {
                        grid_record.data.attr_name_service = attribute_source_name.toLowerCase();
                    }
                    if (grid_record.data.attr_name_masterportal === "") {
                        grid_record.data.attr_name_masterportal = attribute_source_name.toLowerCase();
                    }
                }
            }
        });

        // switch cells "name (db, api, portal)" to ignore if the cell "rule order" has changed
        if (context.field === "rule_order") {
            const rule_order_values = Ext.getCmp("ruleordervalues").getStore().config.data.flat();

            if (rule_order_values.includes(context.value)) {
                gridstore.each(function (grid_record) {
                    if (grid_record.internalId === context.record.internalId) {
                        grid_record.data.attr_name_db = "ignore";
                        grid_record.data.attr_name_service = "ignore";
                        grid_record.data.attr_name_masterportal = "ignore";
                    }
                });
            }
        }

        if (context.field === "attr_name_db") {
            gridstore.each(function (grid_record) {
                if (grid_record.internalId === context.record.internalId) {
                    grid_record.data.attr_name_db = grid_record.data.attr_name_db.replace(/ü/gi, "ue");
                    grid_record.data.attr_name_db = grid_record.data.attr_name_db.replace(/ö/gi, "oe");
                    grid_record.data.attr_name_db = grid_record.data.attr_name_db.replace(/ä/gi, "ae");
                    grid_record.data.attr_name_db = grid_record.data.attr_name_db.replace(/ß/gi, "ss");
                    grid_record.data.attr_name_db = grid_record.data.attr_name_db.replace(/ /gi, "_");

                    if (grid_record.data.attr_name_db.match(/[^a-z0-9_]/)) {
                        Ext.MessageBox.alert("Fehler", "Mit Ausnahme von UNDERSCORE sind Sonderzeichen und Großschreibung nicht zulässig. Um mehrere Wörter zu verbinden bitte Snakecase benutzen!").setAlwaysOnTop(true);
                    }
                }

                if (context.value.toLowerCase() === grid_record.data.attr_name_db && context.value !== "ignore" && grid_record.internalId !== context.record.internalId) {
                    Ext.MessageBox.alert("Fehler", "Name (DB) bereits eingetragen!").setAlwaysOnTop(true);
                    context.record.data.attr_name_db = context.originalValue;
                }
            });
        }

        Ext.getCmp("attributeeditor-grid").getView().refresh();
    },

    onBeforeRender: function () {
        // save old grid values for each attribute
        const attribute_store_items = Ext.getStore("AttributeConfigs").getData().items;

        attribute_store_items.forEach(item => {
            Object.keys(item.data).forEach(key => {
                item.data[key + "_old"] = item.data[key];
            });
        });

        // manage deactivation of "get attributes from service" button
        const serviceTypeArr = Ext.getStore("ServicesDataset").data.items.map((object) => {
            const service_types = object.data.type;

            return service_types;
        });

        if (serviceTypeArr.length > 0 && (serviceTypeArr.includes("WMS") || serviceTypeArr.includes("WFS"))) {
            Ext.getCmp("getattributesservice").setDisabled(false);
        }

        // manage deactivation of "get attributes from database" button
        const db_connection_r_id = Ext.getCmp("datasetdbconnectionrid").getSubmitValue();
        const db_schema = Ext.getCmp("datasetcollectiondbschema").getValue();
        const db_table = Ext.getCmp("datasetcollectiondbtable").getValue();

        if (db_connection_r_id && db_schema && db_table) {
            Ext.getCmp("getattributesdb").setDisabled(false);
        }

        // funcionality for mandatory fields and default values
        const mandatory_form_fields = Ext.ComponentQuery.query("form component[requiredField/='true']");

        mandatory_form_fields.forEach(mandatory_field => {
            Ext.getCmp(mandatory_field.id).afterLabelTextTpl = "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px' data-qtip='Pflichtfeld'>*</span>";
        });
    },

    onCsvExport: function () {
        const store = Ext.getStore("AttributeConfigs");
        const csv_name = "attributes_" + Ext.getCmp("datasetcollectionname").getSubmitValue() + ".csv";

        let csvContent = "data:text/csv;charset=utf-8,Attributname (Quelle);Attributname (DB);Attributname (Schnittstelle);Anzeigename;Datentyp;Primärschlüssel;Primäre Geometrie;Primärer Zeitindex;Regel (Pflicht);Regel (Filter Visualisierung);Regel (Filter Daten);Regel (Attributreihenfolge);Regel (Datum);Regel (Georeferenzierung);Regel (Anmerkungen);Einheit;Beschreibung\r\n";

        store.each(function (record) {
            csvContent += record.data.attr_name_source + ";"
            + record.data.attr_name_db + ";"
            + record.data.attr_name_service + ";"
            + record.data.attr_name_masterportal + ";"
            + record.data.attr_datatype + ";"
            + record.data.pk + ";"
            + record.data.primary_geom + ";"
            + record.data.primary_date + ";"
            + record.data.rule_mandatory + ";"
            + record.data.rule_filter_data + ";"
            + record.data.rule_filter_vis + ";"
            + record.data.rule_order + ";"
            + record.data.rule_date + ";"
            + record.data.rule_georef + ";"
            + record.data.rule_note + ";"
            + record.data.unit + ";"
            + record.data.description + "\r\n";
        });

        const encodedUri = encodeURI(csvContent);
        const link = document.createElement("a");

        link.setAttribute("href", encodedUri);
        link.setAttribute("download", csv_name);
        document.body.appendChild(link);

        link.click();

        document.body.removeChild(link);
    },

    _removeTrailingWhitespacesInAttributeMap: function (array) {
        array.forEach(obj => {
            for (const key of Object.keys(obj)) {
                const value = obj[key];

                if (typeof value === "string") {
                    obj[key] = value.trim();
                }
            }
        });
        return array;
    },

    onRenderCell: function (value, meta, record, rowIndex, colIndex, store, view) {

        const headerCt = view.getHeaderCt();
        const column = headerCt.getHeaderAtIndex(colIndex);

        if (column.id === "gridrownumberer") {
            return rowIndex + 1;
        }

        if (value === "ignore") {
            meta.tdCls = "ignore-cell";
            meta.tdAttr = "data-qtip='" + value + "'";
            return value;
        }
        else if (column.dataIndex === "attr_name_masterportal" && record.data.attr_name_masterportal_ignore) {
            meta.tdCls = "ignore-cell";
            meta.tdAttr = "data-qtip='" + value + "'";
            return value;
        }
        else {
            if (value) {
                meta.tdAttr = "data-qtip='" + value + "'";
            }
            return value;
        }
    },

    onGridAttributeSelected: function (me, record) {
        // switch to details tab
        Ext.getCmp("attr_window_tabs").setActiveTab(0);

        // enable form fields
        Ext.getCmp("attributeeditorform").setDisabled(false);

        // set read only for ignored attributes
        Object.keys(record.data).forEach(key => {
            if (key === "attr_name_db_ignore" || key === "attr_name_service_ignore") {
                if (record.data[key]) {
                    Ext.getCmp(key.replace("_ignore", "")).setReadOnly(true);
                }
                else {
                    Ext.getCmp(key.replace("_ignore", "")).setReadOnly(false);
                }
            }
        });
    },

    changeIgnore: function (checkbox) {
        let oldValue = null;
        let oldValueApi = null;
        let oldValuePortal = null;
        let oldValueApiIgnore = null;
        let oldValuePortalIgnore = null;

        if (Ext.getCmp("attributeeditor-grid").getSelectionModel().getSelection()[0]) {
            oldValue = Ext.getCmp("attributeeditor-grid").getSelectionModel().getSelection()[0].data[checkbox.name.replace("_ignore", "") + "_old"];
            oldValueApi = Ext.getCmp("attributeeditor-grid").getSelectionModel().getSelection()[0].data.attr_name_service_old;
            oldValuePortal = Ext.getCmp("attributeeditor-grid").getSelectionModel().getSelection()[0].data.attr_name_masterportal_old;
            oldValueApiIgnore = Ext.getCmp("attributeeditor-grid").getSelectionModel().getSelection()[0].data.attr_name_service_ignore_old;
            oldValuePortalIgnore = Ext.getCmp("attributeeditor-grid").getSelectionModel().getSelection()[0].data.attr_name_masterportal_ignore_old;
        }

        if (checkbox.checked) {
            Ext.getCmp(checkbox.name.replace("_ignore", "")).setValue("ignore");
            Ext.getCmp(checkbox.name.replace("_ignore", "")).setReadOnly(true);
            if (checkbox.id === "attr_name_db_ignore") {
                Ext.getCmp("attr_name_service").setValue("ignore");
                Ext.getCmp("attr_name_masterportal").setValue("ignore");
                Ext.getCmp("attr_name_service_ignore").setValue(true);
                Ext.getCmp("attr_name_masterportal_ignore").setValue(true);
                Ext.getCmp("attr_name_service_ignore").setReadOnly(true);
                Ext.getCmp("attr_name_masterportal_ignore").setReadOnly(true);
                Ext.getCmp("attr_name_masterportal").setReadOnly(true);
            }
            if (checkbox.id === "attr_name_service_ignore") {
                Ext.getCmp("attr_name_masterportal").setValue("ignore");
                Ext.getCmp("attr_name_masterportal_ignore").setValue(true);
                Ext.getCmp("attr_name_masterportal_ignore").setReadOnly(true);
                Ext.getCmp("attr_name_masterportal").setReadOnly(true);
            }
        }
        else {
            if (checkbox.id === "attr_name_db_ignore") {
                if (oldValue === "ignore") {
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setValue("");
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setReadOnly(false);
                }
                else {
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setValue(oldValue);
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setReadOnly(false);
                }
                Ext.getCmp("attr_name_service").setValue(oldValueApi);
                Ext.getCmp("attr_name_masterportal").setValue(oldValuePortal);
                Ext.getCmp("attr_name_masterportal").setReadOnly(false);
                Ext.getCmp("attr_name_service_ignore").setValue(oldValueApiIgnore);
                Ext.getCmp("attr_name_masterportal_ignore").setValue(oldValuePortalIgnore);
                Ext.getCmp("attr_name_service_ignore").setReadOnly(false);
                Ext.getCmp("attr_name_masterportal_ignore").setReadOnly(false);
            }
            else if (checkbox.id === "attr_name_service_ignore") {
                if (oldValue === "ignore") {
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setValue("");
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setReadOnly(false);
                }
                else {
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setValue(oldValue);
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setReadOnly(false);
                }
                Ext.getCmp("attr_name_masterportal").setValue(oldValuePortal);
                Ext.getCmp("attr_name_masterportal").setReadOnly(false);
                Ext.getCmp("attr_name_masterportal_ignore").setValue(oldValuePortalIgnore);
                Ext.getCmp("attr_name_masterportal_ignore").setReadOnly(false);
            }
            else {
                if (oldValue === "ignore") {
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setValue("");
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setReadOnly(false);
                }
                else {
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setValue(oldValue);
                    Ext.getCmp(checkbox.name.replace("_ignore", "")).setReadOnly(false);
                }
            }
        }
    },

    onClickAttrChangelog: function (grid, rowIndex) {
        const row_data = grid.getStore().getAt(rowIndex).data;

        if (row_data.attr_changed) {
            Ext.getCmp("attr_window_tabs").setActiveTab(1);
        }
    }
});
