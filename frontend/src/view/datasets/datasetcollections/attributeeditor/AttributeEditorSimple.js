Ext.define("UDPManager.view.datasets.datasetcollections.attributeeditor.AttributeEditorSimple", {
    extend: "Ext.window.Window",
    id: "attributeeditorsimple-window",
    alias: "datasetcollections.AttributeEditorSimple",
    width: 500,
    height: 600,
    title: "Attribute konfigurieren",
    resizable: false,
    scrollable: true,
    closeAction: "hide",
    controller: "attributeeditor",
    listeners: {
        close: {
            fn: "onWindowClose"
        },
        beforerender: "onBeforeRender"
    },
    initComponent: function () {
        this.items = [
            {
                xtype: "grid",
                store: "AttributeConfigs",
                id: "attributeeditor-grid",
                height: 550,
                scrollable: true,
                columnLines: true,
                plugins: {
                    cellediting: {
                        clicksToEdit: 1
                    }
                },
                selModel: {
                    type: "cellmodel"
                },
                viewConfig: {
                    enableTextSelection: true
                },
                listeners: {
                    edit: "onEditMultipleCells"
                },
                columns: [{
                    xtype: "rownumberer"
                },
                {
                    header: "Name (API)",
                    dataIndex: "attr_name_service",
                    align: "left",
                    flex: 1,
                    sortable: false,
                    editor: {
                        allowBlank: false
                    },
                    cls: "multiline",
                    renderer: function (value, meta) {
                        if (value === "ignore") {
                            meta.tdCls = "ignore-cell";
                            return value;
                        }
                        else {
                            return value;
                        }
                    }
                }, {
                    header: "Label",
                    dataIndex: "attr_name_masterportal",
                    flex: 1,
                    sortable: false,
                    align: "left",
                    editor: {
                        allowBlank: false
                    },
                    cls: "multiline",
                    renderer: function (value, meta) {
                        if (value === "ignore") {
                            meta.tdCls = "ignore-cell";
                            return value;
                        }
                        else {
                            return value;
                        }
                    }
                }, {
                    xtype: "actioncolumn",
                    width: 26,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onMoveUp",
                    hidden: window.read_only,
                    iconCls: "x-fa fa-angle-up faIcon"
                }, {
                    xtype: "actioncolumn",
                    width: 26,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onMoveDown",
                    hidden: window.read_only,
                    iconCls: "x-fa fa-angle-down faIcon"
                }, {
                    xtype: "actioncolumn",
                    width: 26,
                    sortable: false,
                    menuDisabled: true,
                    align: "center",
                    handler: "onDeleteRow",
                    hidden: window.read_only,
                    iconCls: "x-fa fa-trash deleteIcon"
                }],
                tbar: [{
                    text: "Zeile hinzufügen",
                    tooltip: "Fügt eine leere Zeile am Ende der Tabelle ein",
                    hidden: window.read_only,
                    handler: "onAddRow"
                },
                {
                    text: "weitere Funktionen",
                    hidden: true,
                    menu: [
                        {
                            text: "Attribute abfragen (Schnittstelle)",
                            id: "getattributesservice",
                            tooltip: "Attribute über Schnittstelle abfragen <br> Z. B. bei externen Diensten nutzbar, um die Attribute per WMS oder WFS Schnittestelle einzutragen.",
                            hidden: true,
                            disabled: true,
                            handler: "onOpenGetAttributesWindow"
                        }, {
                            text: "Attribute abfragen (DB)",
                            id: "getattributesdb",
                            tooltip: "Attribute aus Datenbank abfragen <br> Für die Abfrage wird die am Datensatz hinterlegte Datenbankverbindung genutzt. Es wird auf das Schema und die Tabelle der Collection zugegriffen.",
                            hidden: true,
                            disabled: true,
                            handler: "onGetAttributesDb"
                        }
                    ]
                },
                {
                    xtype: "tbspacer",
                    flex: 1
                },
                {
                    xtype: "checkboxfield",
                    id: "useforallcollections",
                    fieldLabel: "Konfiguration für alle Collections übernehmen",
                    hidden: true,
                    margin: "0 0 0 10",
                    labelWidth: 260
                }, {
                    text: "speichern",
                    id: "attributessave",
                    tooltip: "speichert die Attributzuordnung",
                    margin: "0 10 0 10",
                    hidden: window.read_only,
                    handler: "onSaveAttributes"
                }]
            }
        ];

        this.callParent(arguments);
    }
});
