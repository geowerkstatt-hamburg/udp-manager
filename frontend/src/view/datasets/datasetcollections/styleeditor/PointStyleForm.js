Ext.define("UDPManager.view.datasets.datasetcollections.styleeditor.PointStyleForm", {
    extend: "Ext.form.Panel",
    xtype: "pointstyle-form",
    alias: "styleeditor.PointStyleForm",
    bodyPadding: "10 10 10 10",
    defaultType: "textfield",
    layout: "anchor",
    defaults: {
        anchor: "0",
        labelWidth: 60,
        layout: {
            type: "hbox",
            align: "center"
        }
    },
    items: [
        {
            id: "pointstylename",
            labelWidth: 150,
            width: "100%",
            fieldLabel: "Bezeichnung",
            allowBlank: false,
            bind: "{grid_style.selection.rule_name}"
        },
        {
            id: "pointstyletitle",
            labelWidth: 150,
            width: "100%",
            fieldLabel: "Titel",
            allowBlank: false,
            bind: "{grid_style.selection.title}"
        },
        {
            xtype: "combo",
            fieldLabel: "Symbolauswahl",
            id: "pointstylesymbol",
            editable: false,
            labelWidth: 150,
            flex: 1,
            margin: "10 0 10 0",
            store: ["Kreis", "Dreieck", "Quadrat", "Stern", "Externes Icon"],
            name: "point_style",
            bind: "{grid_style.selection.point_style}",
            listeners: {
                change: "onDrawPoint"
            }
        },
        {
            xtype: "fieldset",
            id: "pointstylestrokecolorfieldset",
            title: "Umringfarbe definieren",
            defaultType: "textfield",
            layout: "anchor",
            width: "100%",
            defaults: {
                anchor: "0",
                labelWidth: 60,
                layout: {
                    type: "hbox",
                    align: "center"
                }
            },
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "vbox",
                    defaults: {
                        height: 35,
                        flex: 1
                    },
                    items: [
                        {
                            xtype: "checkbox",
                            boxLabel: "Farbwert aus Attribut",
                            labelWidth: 150,
                            checked: false,
                            disabled: true,
                            id: "pointstylestrokecolordynamiccheckbox",
                            bind: "{grid_style.selection.stroke_color_dynamic}",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Hex-Farbwerte aus Datenbank, Format: #946edc"
                            },
                            listeners: {
                                change: "onStrokeDynamic"
                            }
                        },
                        {
                            xtype: "combobox",
                            id: "pointstylestrokecolordynamicattribute",
                            bind: "{grid_style.selection.stroke_color_dynamic_attribute}",
                            fieldLabel: "Attribut",
                            labelWidth: 150,
                            store: [],
                            editable: false,
                            allowBlank: false,
                            sortable: false,
                            hideable: false,
                            hidden: true,
                            listeners: {
                                change: "onDrawPoint"
                            }
                        }
                    ]
                },
                {
                    xtype: "colorbutton",
                    id: "pointstylestrokecolorhex",
                    bind: "{grid_style.selection.stroke_color_hex}",
                    width: "100%",
                    height: 25,
                    margin: "0, 0, 5, 0",
                    listeners: {
                        change: "onDrawPoint"
                    }
                },
                {
                    xtype: "numberfield",
                    fieldLabel: "Deckkraft",
                    labelWidth: 150,
                    id: "pointstylestrokeopacity",
                    width: "100%",
                    bind: "{grid_style.selection.stroke_opacity}",
                    maxValue: 1,
                    minValue: 0,
                    allowDecimals: true,
                    decimalPrecision: 2,
                    step: 0.01,
                    listeners: {
                        change: "onDrawPoint"
                    }
                }
            ]
        },
        {
            xtype: "numberfield",
            fieldLabel: "Größe",
            id: "pointstylesize",
            labelWidth: 150,
            bind: "{grid_style.selection.size}",
            minValue: 0,
            maxValue: 50,
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawPoint"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "Umring Strichbreite",
            id: "pointstylestrokewidth",
            labelWidth: 150,
            bind: "{grid_style.selection.stroke_width}",
            minValue: 0,
            maxValue: 10,
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawPoint"
            }
        },
        {
            xtype: "combo",
            fieldLabel: "Rotation",
            id: "pointstyleiconrotationtriangle",
            labelWidth: 150,
            hidden: true,
            editable: false,
            allowBlank: false,
            store: [0, 90, 180, 270],
            bind: "{grid_style.selection.icon_rotation}",
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawPoint"
            }
        },
        {
            xtype: "combo",
            fieldLabel: "Rotation",
            id: "pointstyleiconrotationsquare",
            labelWidth: 150,
            hidden: true,
            editable: false,
            store: [0, 45],
            bind: "{grid_style.selection.icon_rotation}",
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawPoint"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "Rotation",
            id: "pointstyleiconrotationexticon",
            labelWidth: 150,
            hidden: true,
            minValue: 0,
            maxValue: 359,
            bind: "{grid_style.selection.icon_rotation}",
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawPoint"
            }
        },
        {
            xtype: "fieldset",
            id: "pointstylefillcolorfieldset",
            title: "Füllfarbe definieren",
            defaultType: "textfield",
            layout: "anchor",
            defaults: {
                anchor: "0",
                labelWidth: 60,
                layout: {
                    type: "hbox",
                    align: "center"
                }
            },
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "vbox",
                    defaults: {
                        height: 35,
                        flex: 1
                    },
                    items: [
                        {
                            xtype: "checkbox",
                            boxLabel: "Farbwert aus Attribut",
                            labelWidth: 150,
                            checked: false,
                            disabled: true,
                            id: "pointstylefillcolordynamiccheckbox",
                            bind: "{grid_style.selection.fill_color_dynamic}",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Hex-Farbwerte aus Datenbank, Format: #946edc"
                            },
                            listeners: {
                                change: "onFillDynamic"
                            }
                        },
                        {
                            xtype: "combobox",
                            id: "pointstylefillcolordynamicattribute",
                            fieldLabel: "Attribut",
                            labelWidth: 150,
                            store: [],
                            editable: false,
                            allowBlank: false,
                            sortable: false,
                            hideable: false,
                            bind: "{grid_style.selection.fill_color_dynamic_attribute}",
                            hidden: true,
                            listeners: {
                                change: "onDrawPoint"
                            }
                        }
                    ]
                },
                {
                    xtype: "colorbutton",
                    id: "pointstylefillcolorhex",
                    bind: "{grid_style.selection.fill_color_hex}",
                    width: "100%",
                    height: 25,
                    margin: "0, 0, 5, 0",
                    listeners: {
                        change: "onDrawPoint"
                    }
                },
                {
                    xtype: "numberfield",
                    fieldLabel: "Deckkraft",
                    labelWidth: 150,
                    id: "pointstylefillopacity",
                    width: "100%",
                    bind: "{grid_style.selection.fill_opacity}",
                    maxValue: 1,
                    minValue: 0,
                    allowDecimals: true,
                    decimalPrecision: 2,
                    step: 0.01,
                    listeners: {
                        change: "onDrawPoint"
                    }
                }
            ]
        },
        {
            xtype: "numberfield",
            fieldLabel: "MinScaleDenominator",
            id: "pointstyleminscaledenominator",
            labelWidth: 150,
            allowDecimals: false,
            bind: "{grid_style.selection.minscaledenominator}",
            allowBlank: true,
            margin: "10 0 10 0",
            maxValue: 10000000,
            minValue: 0
        },
        {
            xtype: "numberfield",
            fieldLabel: "MaxScaleDenominator",
            id: "pointstylemaxscaledenominator",
            labelWidth: 150,
            bind: "{grid_style.selection.maxscaledenominator}",
            allowDecimals: false,
            allowBlank: true,
            margin: "10 0 10 0",
            maxValue: 10000000,
            minValue: 0
        },
        {
            xtype: "form",
            id: "pointstyleuploadform",
            hidden: "true",
            items:
                [
                    {
                        xtype: "container",
                        flex: 1,
                        layout: {
                            type: "vbox",
                            align: "stretch"
                        },
                        items:
                            [
                                {
                                    xtype: "filefield",
                                    buttonOnly: true,
                                    id: "pointstylefilefield",
                                    buttonConfig: {
                                        width: 110,
                                        text: "Icon hochladen",
                                        tooltip: "Uploadformate: png, svg, gif",
                                        baseCls: "x-btn"
                                    },
                                    listeners: {
                                        change: "onUpload"
                                    }
                                }
                            ]
                    }
                ]
        }
    ]
});
