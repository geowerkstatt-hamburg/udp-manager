Ext.define("UDPManager.view.datasets.datasetcollections.styleeditor.StyleEditorWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.styleeditor",

    /**
     * called when item in style grid is selected
     * @param {Object} sender - event source
     * @param {Object} record - data of selected item
     * @returns {void}
     */
    onItemSelect: function (sender, record) {
        const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(record.data.geom_type);
        const me = this;

        Ext.getCmp("style_editor_formular_point").setHidden(true);
        Ext.getCmp("style_editor_formular_line").setHidden(true);
        Ext.getCmp("style_editor_formular_polygon").setHidden(true);
        Ext.getCmp("style_editor_formular_text").setHidden(true);
        Ext.getCmp("point-style-canvas-panel").setHidden(true);
        Ext.getCmp("polygon-style-canvas-panel").setHidden(true);
        Ext.getCmp("line-style-canvas-panel").setHidden(true);
        Ext.getCmp("text-style-canvas-panel").setHidden(true);

        const attribute_config_array = [];

        Ext.getStore("AttributeConfigs").each(function (attr_record) {
            const namespace = Ext.getCmp("datasetcollectionnamespace").getSubmitValue();

            if (attr_record.data.attr_name_db.length > 0 && attr_record.data.attr_name_service.length > 0) {
                if (attr_record.data.attr_name_service.indexOf(namespace.split("=")[0]) > -1) {
                    attribute_config_array.push(attr_record.data.attr_name_service);
                }
                else {
                    attribute_config_array.push(namespace.split("=")[0] + ":" + attr_record.data.attr_name_service);
                }
            }
        });

        if (record.data.text_symbolizer) {
            Ext.getCmp("style_editor_formular_text").setHidden(false);
            Ext.getCmp("text-style-canvas-panel").setHidden(false);
            Ext.getCmp("textstylelabelattribute").bindStore(attribute_config_array);
            Ext.getCmp("textstylelabelattribute").getStore().sync();
        }
        else {
            Ext.getCmp("style_editor_formular_" + geomType).setHidden(false);
            Ext.getCmp(geomType + "-style-canvas-panel").setHidden(false);

            let form;
            let size;

            if (geomType === "point") {
                form = Ext.getCmp("pointstyleuploadform");
                size = Object.keys(form).length;

                if (size > 0) {
                    Ext.getCmp("styleeditor-grid").getView().refresh();
                    me.onDrawPoint();

                    if (form.getEl().down("input[type=file]").dom.files[0] !== undefined) {
                        // https://stackoverflow.com/questions/11118949/extjs-reset-filefield-input
                        const fileField = document.getElementById("pointstylefilefield");
                        const parentNod = fileField.parentNode;
                        const tmpForm = document.createElement("form");

                        parentNod.replaceChild(tmpForm, fileField);
                        tmpForm.appendChild(fileField);
                        tmpForm.reset();
                        parentNod.replaceChild(fileField, tmpForm);
                    }
                }
            }
            else if (geomType === "polygon") {
                form = Ext.getCmp("polygonstyleuploadform");
                size = Object.keys(form).length;

                if (size > 0) {
                    Ext.getCmp("styleeditor-grid").getView().refresh();
                    me.onDrawPolygon();

                    if (form.getEl().down("input[type=file]").dom.files[0] !== undefined) {
                        // https://stackoverflow.com/questions/11118949/extjs-reset-filefield-input
                        const fileField = document.getElementById("polygonstylefilefield");
                        const parentNod = fileField.parentNode;
                        const tmpForm = document.createElement("form");

                        parentNod.replaceChild(tmpForm, fileField);
                        tmpForm.appendChild(fileField);
                        tmpForm.reset();
                        parentNod.replaceChild(fileField, tmpForm);
                    }
                }
            }

            if (Ext.getStore("AttributeConfigs").count() > 0) {
                if (geomType === "point") {
                    Ext.getCmp("pointstylefillcolordynamicattribute").bindStore(attribute_config_array);
                    Ext.getCmp("pointstylefillcolordynamicattribute").getStore().sync();
                    Ext.getCmp("pointstylefillcolordynamiccheckbox").setDisabled(false);
                    Ext.getCmp("stylefilterconfig").setDisabled(false);
                    Ext.getCmp("pointstylestrokecolordynamicattribute").bindStore(attribute_config_array);
                    Ext.getCmp("pointstylestrokecolordynamicattribute").getStore().sync();
                    Ext.getCmp("pointstylestrokecolordynamiccheckbox").setDisabled(false);
                    Ext.getCmp("stylefilterattributes").bindStore(attribute_config_array);
                    Ext.getCmp("pointstylestrokecolordynamicattribute").getStore().sync();
                }
                else if (geomType === "line") {
                    Ext.getCmp("stylefilterconfig").setDisabled(false);
                    Ext.getCmp("linestylestrokecolordynamicattribute").bindStore(attribute_config_array);
                    Ext.getCmp("linestylestrokecolordynamicattribute").getStore().sync();
                    Ext.getCmp("linestylestrokecolordynamiccheckbox").setDisabled(false);
                    Ext.getCmp("stylefilterattributes").bindStore(attribute_config_array);
                    Ext.getCmp("linestylestrokecolordynamicattribute").getStore().sync();
                }
                else if (geomType === "polygon") {
                    Ext.getCmp("polygonstylefillcolordynamicattribute").bindStore(attribute_config_array);
                    Ext.getCmp("polygonstylefillcolordynamicattribute").getStore().sync();
                    Ext.getCmp("polygonstylefillcolordynamiccheckbox").setDisabled(false);
                    Ext.getCmp("stylefilterconfig").setDisabled(false);
                    Ext.getCmp("polygonstylestrokecolordynamicattribute").bindStore(attribute_config_array);
                    Ext.getCmp("polygonstylestrokecolordynamicattribute").getStore().sync();
                    Ext.getCmp("polygonstylestrokecolordynamiccheckbox").setDisabled(false);
                    Ext.getCmp("stylefilterattributes").bindStore(attribute_config_array);
                    Ext.getCmp("polygonstylestrokecolordynamicattribute").getStore().sync();
                }
            }
        }
    },

    /**
     * After geom Attribute is selected, buttons for adding style rules are enabled
     * @returns {void}
    */
    onSelectGeom: function () {
        Ext.getCmp("button_add_rule").setDisabled(window.read_only);
        Ext.getCmp("button_add_text_rule").setDisabled(window.read_only);
    },

    /**
     * add new default style rule to grid store, depending on the collection geometry type of the linked table
     * @param {Object} btn - button object from source event
     * @returns {void}
     */
    onAddStyleRule: function (btn) {
        Ext.getCmp("style_editor_formular_point").setHidden(true);
        Ext.getCmp("style_editor_formular_line").setHidden(true);
        Ext.getCmp("style_editor_formular_polygon").setHidden(true);
        Ext.getCmp("style_editor_formular_text").setHidden(true);
        Ext.getCmp("point-style-canvas-panel").setHidden(true);
        Ext.getCmp("polygon-style-canvas-panel").setHidden(true);
        Ext.getCmp("line-style-canvas-panel").setHidden(true);
        Ext.getCmp("text-style-canvas-panel").setHidden(true);

        if (Ext.getCmp("add_style_rule_geom").getSelection()) {
            const geomAttr = Ext.getCmp("add_style_rule_geom").getSelection().data;

            Ext.getCmp("rule_name_id").setHidden(false);
            Ext.getCmp("rule_name_col_id").setHidden(false);
            Ext.getCmp("styles_useforallcollections").setHidden(false);
            Ext.getCmp("stylefilterconfig").setHidden(false);

            const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(geomAttr.type);

            let myRecordDef;

            if (btn.id === "button_add_rule") {
                Ext.getCmp(geomType + "-style-canvas-panel").setHidden(false);
                Ext.getCmp("style_editor_formular_" + geomType).setHidden(false);

                if (geomType === "point") {
                    myRecordDef = Ext.data.Record.create({
                        rule_name: "point_style",
                        title: "point_style",
                        point_style: "Kreis",
                        point_wellknownname: null,
                        strokecolor_dynamic: false,
                        stroke_color_dynamic_attribute: null,
                        stroke_color_hex: "406edc",
                        stroke_opacity: 1.0,
                        fill_color_hex: "406edc",
                        fill_color_dynamic: false,
                        fill_color_dynamic_attribute: null,
                        fill_opacity: 1.0,
                        size: 10,
                        stroke_width: 3,
                        icon_rotation: 0,
                        filter_attribute: null,
                        filter_condition: null,
                        filter_literal: null,
                        filter_literal_min: null,
                        filter_literal_max: null,
                        geom_name: geomAttr.name,
                        geom_type: geomType,
                        file_name_exticon: null,
                        file_type_exticon: null,
                        file_content_type_exticon: null,
                        data_url_exticon: null
                    });
                }
                else if (geomType === "line") {
                    myRecordDef = Ext.data.Record.create({
                        rule_name: "line_style",
                        title: "line_style",
                        stroke_color_dynamic: false,
                        stroke_color_dynamic_attribute: null,
                        stroke_color_hex: "406edc",
                        stroke_width_line: 3,
                        stroke_opacity: 1.0,
                        pattern_style: "durchgezogen",
                        pattern_dash_array: 5,
                        filter_attribute: null,
                        filter_condition: null,
                        filter_literal: null,
                        filter_literal_min: null,
                        filter_literal_max: null,
                        geom_name: geomAttr.name,
                        geom_type: geomType
                    });
                }
                else if (geomType === "polygon") {
                    myRecordDef = Ext.data.Record.create({
                        rule_name: "polygon_style",
                        title: "polygon_style",
                        fill_type: "einfarbig",
                        stroke_color_hex: "406edc",
                        stroke_color_dynamic: false,
                        stroke_color_dynamic_attribute: null,
                        stroke_opacity: 1.0,
                        fill_color_hex: "406edc",
                        fill_color_dynamic: false,
                        fill_color_dynamic_attribute: null,
                        fill_opacity: 1.0,
                        pattern_style: "horizontal",
                        pattern_density: 4,
                        pattern_thickness: 1,
                        stroke_width: 1,
                        icon_size: 10,
                        icon_rotation: 0,
                        filter_attribute: null,
                        filter_condition: null,
                        filter_literal: null,
                        filter_literal_min: null,
                        filter_literal_max: null,
                        geom_name: geomAttr.name,
                        geom_type: geomType,
                        file_name: null,
                        file_type: null,
                        file_content_type: null,
                        data_url: null,
                        file_name_exticon: null,
                        file_type_exticon: null,
                        file_content_type_exticon: null,
                        data_url_exticon: null
                    });
                }
            }
            else if (btn.id === "button_add_text_rule") {
                Ext.getCmp("text-style-canvas-panel").setHidden(false);
                Ext.getCmp("style_editor_formular_text").setHidden(false);

                myRecordDef = Ext.data.Record.create({
                    rule_name: "text_style",
                    title: "text_style",
                    label_attribute: null,
                    label_placement: "Punkt",
                    label_font: "Arial",
                    label_font_style: "normal",
                    label_font_weight: "normal",
                    label_font_size: 15,
                    fill_color_hex: "406edc",
                    fill_opacity: 1,
                    anchor_point_x: 0,
                    anchor_point_y: 0,
                    displacement_x: 0,
                    displacement_y: 0,
                    rotation: 0,
                    is_repeated: false,
                    initial_gap: 0,
                    is_aligned: false,
                    halo_radius: 0,
                    halo_color_hex: "406edc",
                    halo_opacity: 0.5,
                    filter_attribute: null,
                    filter_condition: null,
                    filter_literal: null,
                    filter_literal_min: null,
                    filter_literal_max: null,
                    geom_name: geomAttr.name,
                    geom_type: geomType,
                    text_symbolizer: true
                });
            }

            // Insert new record and select last row in the grid
            Ext.getStore("DatasetCollectionStyle").insert(Ext.getStore("DatasetCollectionStyle").count(), myRecordDef);
            Ext.getCmp("styleeditor-grid").getSelectionModel().select(Ext.getStore("DatasetCollectionStyle").count() - 1);

            Ext.getCmp("savestyle").setDisabled(window.read_only);
        }
    },

    /**
     * show/hide filter fields after input change. Set filter to model
     * @returns {void}
     */
    onChangeFilterRule: function () {
        const rule = Ext.getCmp("stylefiltercondition").getSubmitValue();

        Ext.getCmp("styleresetfilter").setDisabled(false);
        Ext.getCmp("stylefilterislikehelp").setHidden(true);

        if (rule.includes("Between")) {
            Ext.getCmp("stylefilterliteralmax").setHidden(false);
            Ext.getCmp("stylefilterliteralmin").setHidden(false);
            Ext.getCmp("stylefilterliteral").setHidden(true);
        }
        else if (rule.includes("IsNull")) {
            Ext.getCmp("stylefilterliteralmin").setHidden(true);
            Ext.getCmp("stylefilterliteralmax").setHidden(true);
            Ext.getCmp("stylefilterliteralmin").setValue("");
            Ext.getCmp("stylefilterliteralmax").setValue("");
            Ext.getCmp("stylefilterliteral").setHidden(true);
        }
        else if (rule === "PropertyIsLike") {
            Ext.getCmp("stylefilterliteralmin").setHidden(true);
            Ext.getCmp("stylefilterliteralmax").setHidden(true);
            Ext.getCmp("stylefilterliteralmin").setValue("");
            Ext.getCmp("stylefilterliteralmax").setValue("");
            Ext.getCmp("stylefilterliteral").setHidden(false);
            Ext.getCmp("stylefilterislikehelp").setHidden(false);
        }
        else {
            Ext.getCmp("stylefilterliteralmin").setHidden(true);
            Ext.getCmp("stylefilterliteralmax").setHidden(true);
            Ext.getCmp("stylefilterliteralmin").setValue("");
            Ext.getCmp("stylefilterliteralmax").setValue("");
            Ext.getCmp("stylefilterliteral").setHidden(false);
        }
    },

    /**
     * empty all filter fields after reset pressed. Set filter to null in model
     * @returns {void}
     */
    onResetFilter: function () {
        Ext.getCmp("styleresetfilter").setDisabled(true);

        const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();

        model[0].set("filter_attribute", "");
        model[0].set("filter_condition", "");
        model[0].set("filter_literal", "");
        model[0].set("filter_literal_min", "");
        model[0].set("filter_literal_max", "");
    },

    /**
     * called when upload external graphic is triggered
     * @param {Object} input - upload button
     * @returns {void}
     */
    onUpload: function (input) {
        const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();
        const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(model[0].data.geom_type);
        const me = this;

        Ext.getCmp(geomType + "styleminscaledenominator").setHidden(false);
        Ext.getCmp(geomType + "stylemaxscaledenominator").setHidden(false);
        Ext.getCmp("stylefilterconfig").setHidden(false);

        if (geomType === "point") {
            Ext.getCmp("pointstylesize").setHidden(false);
            Ext.getCmp("pointstyleiconrotationexticon").setHidden(false);
        }
        else if (geomType === "polygon") {
            Ext.getCmp("polygonstyleiconrotationexticon").setHidden(false);
        }

        if (input.getEl().down("input[type=file]") !== null) {
            const file = input.getEl().down("input[type=file]").dom.files[0];

            // nur png, svg, gif -> 2do reduce file size
            if (file.type === "image/svg+xml" || file.type === "image/png" || file.type === "image/gif") {
                let file_type = file.type.split("/")[1];

                if (file_type.includes("svg+xml")) {
                    file_type = file_type.split("+")[0];
                }

                model[0].set("file_type_exticon", file_type);
                model[0].set("file_content_type_exticon", file.type);
                model[0].set("file_name_exticon", file.name);

                const reader = new FileReader();

                reader.onload = function (e) {
                    model[0].set("data_url_exticon", e.target.result);

                    if (geomType === "polygon") {
                        me.onDrawPolygon();
                    }
                    else if (geomType === "point") {
                        me.onDrawPoint();
                    }
                };

                reader.readAsDataURL(file);
                reader.onerror = function (error) {
                    console.log("Error: ", error);
                };
            }
            else {
                Ext.Msg.alert("Fehler", "Erlaubte Dateiformate: png, svg, gif!");
            }
        }
    },

    /**
     * called when dynamic fill option activated/deactivated. Hide/show color fields
     * @param {Object} checkbox - Checkbox object from source event
     * @param {Boolean} newValue - value of the checkbox after change
     * @returns {void}
     */
    onFillDynamic: function (checkbox, newValue) {
        const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();

        if (model.length > 0) {
            const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(model[0].data.geom_type);

            if (newValue) {
                Ext.getCmp(geomType + "stylefillcolordynamicattribute").setHidden(false);
                Ext.getCmp(geomType + "stylefillcolorhex").setHidden(true);
            }
            else {
                Ext.getCmp(geomType + "stylefillcolordynamicattribute").setHidden(true);
                Ext.getCmp(geomType + "stylefillcolorhex").setHidden(false);
            }
        }
    },

    /**
     * called when dynamic stroke option activated/deactivated. Hide/show color fields
     * @param {Object} checkbox - Checkbox object from source event
     * @param {Boolean} newValue - value of the checkbox after change
     * @returns {void}
     */
    onStrokeDynamic: function (checkbox, newValue) {
        const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();

        if (model.length > 0) {
            const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(model[0].data.geom_type);

            if (newValue) {
                Ext.getCmp(geomType + "stylestrokecolordynamicattribute").setHidden(false);
                Ext.getCmp(geomType + "stylestrokecolorhex").setHidden(true);
            }
            else {
                Ext.getCmp(geomType + "stylestrokecolordynamicattribute").setHidden(true);
                Ext.getCmp(geomType + "stylestrokecolorhex").setHidden(false);
            }
        }
    },

    /**
     * called when line style type changed
     * @param {Object} combo - line type combo
     * @returns {void}
     */
    onChangeLineType: function (combo) {
        const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();

        if (model.length > 0 && combo.value) {
            if (combo.value === "durchgezogen") {
                Ext.getCmp("linestyledasharray").setHidden(true);
            }
            else if (combo.value === "gestrichelt") {
                Ext.getCmp("linestyledasharray").setHidden(false);
            }
            this.onDrawLine();
        }
    },

    /**
     * called when polygon fill style type changed
     * @param {Object} combo - fill type combo
     * @returns {void}
     */
    onChangeFillType: function (combo) {
        const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();

        if (model.length > 0) {
            if (combo.value === "einfarbig") {
                Ext.getCmp("polygonstylestrokecolorfieldset").setHidden(false);
                Ext.getCmp("polygonstylestrokewidth").setHidden(false);
                Ext.getCmp("polygonstylefillcolorfieldset").setHidden(false);
                Ext.getCmp("polygonstyleminscaledenominator").setHidden(false);
                Ext.getCmp("polygonstylemaxscaledenominator").setHidden(false);
                Ext.getCmp("polygonstylepatterndensity").setHidden(true);
                Ext.getCmp("polygonstylepatternstyle").setHidden(true);
                Ext.getCmp("polygonstylepatternthickness").setHidden(true);
                Ext.getCmp("polygonstyleuploadform").setHidden(true);
                Ext.getCmp("polygonstyleiconsize").setHidden(true);
                Ext.getCmp("polygonstylestrokecolordynamiccheckbox").setHidden(false);
                Ext.getCmp("polygonstylefillcolordynamiccheckbox").setHidden(false);
            }
            else if (combo.value === "gemustert") {
                Ext.getCmp("polygonstylepatterndensity").setHidden(false);
                Ext.getCmp("polygonstylestrokecolorfieldset").setHidden(false);
                Ext.getCmp("polygonstylefillcolorfieldset").setHidden(false);
                Ext.getCmp("polygonstylestrokewidth").setHidden(false);
                Ext.getCmp("polygonstylepatternstyle").setHidden(false);
                Ext.getCmp("polygonstylepatternthickness").setHidden(false);
                Ext.getCmp("polygonstyleminscaledenominator").setHidden(false);
                Ext.getCmp("polygonstylemaxscaledenominator").setHidden(false);
                Ext.getCmp("polygonstyleuploadform").setHidden(true);
                Ext.getCmp("polygonstyleiconsize").setHidden(true);
                Ext.getCmp("polygonstylestrokecolordynamiccheckbox").setHidden(true);
                Ext.getCmp("polygonstylefillcolordynamiccheckbox").setHidden(true);
            }
            else if (combo.value === "Externes Icon") {
                Ext.getCmp("polygonstylepatterndensity").setHidden(true);
                Ext.getCmp("polygonstylepatternstyle").setHidden(true);
                Ext.getCmp("polygonstylepatternthickness").setHidden(true);
                Ext.getCmp("polygonstylestrokecolorfieldset").setHidden(true);
                Ext.getCmp("polygonstylefillcolorfieldset").setHidden(true);
                Ext.getCmp("polygonstylestrokewidth").setHidden(true);
                Ext.getCmp("polygonstyleuploadform").setHidden(false);
                Ext.getCmp("polygonstylestrokecolordynamiccheckbox").setHidden(false);
                Ext.getCmp("polygonstylefillcolordynamiccheckbox").setHidden(false);
            }

            this.onDrawPolygon();
        }
    },

    onChangeLabelPlacement: function (combo) {
        if (combo.getSubmitValue() === "Linie") {
            Ext.getCmp("textstylepointplacement").setHidden(true);
            Ext.getCmp("textstylelineplacement").setHidden(false);
        }
        else {
            Ext.getCmp("textstylepointplacement").setHidden(false);
            Ext.getCmp("textstylelineplacement").setHidden(true);
        }
    },

    /**
     * update canvas field with given style parameters
     * @returns {void}
     */
    onDrawPoint: function () {
        const count = Ext.getStore("DatasetCollectionStyle").count();

        if (count > 0) {
            const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();

            if (model.length > 0) {
                const data = model[0].data;
                const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(data.geom_type);
                const canvas = document.getElementById("point-style-canvas");
                const ctx = canvas.getContext("2d");
                const color = "#" + Ext.getCmp("pointstylefillcolorhex").getValue();
                const color_stroke = "#" + Ext.getCmp("pointstylestrokecolorhex").getValue();
                const stroke_opacity = Ext.getCmp("pointstylestrokeopacity").getSubmitValue();
                const point_style = Ext.getCmp("pointstylesymbol").getSubmitValue();
                const pattern_thickness = Ext.getCmp("pointstylesize").getSubmitValue();
                const stroke_width = Ext.getCmp("pointstylestrokewidth").getSubmitValue();
                const fill_opacity = Ext.getCmp("pointstylefillopacity").getSubmitValue();

                if (canvas.getContext && geomType === "point" && color && color_stroke && stroke_opacity && point_style && pattern_thickness && stroke_width) {
                    Ext.getCmp("pointstyleminscaledenominator").setHidden(false);
                    Ext.getCmp("pointstylemaxscaledenominator").setHidden(false);
                    Ext.getCmp("pointstyleuploadform").setHidden(true);
                    Ext.getCmp("pointstylestrokecolorfieldset").setHidden(false);
                    Ext.getCmp("pointstylefillcolorfieldset").setHidden(false);
                    Ext.getCmp("pointstylestrokewidth").setHidden(false);
                    Ext.getCmp("stylefilterconfig").setHidden(false);
                    Ext.getCmp("pointstylesize").setHidden(false);

                    const point_style_params = {
                        point_wellknownname: "",
                        file_content_type: "image/png",
                        file_type: "png"
                    };

                    ctx.save(); // saves the current state of the coordinate system.
                    ctx.clearRect(0, 0, canvas.width, canvas.height);

                    if (point_style === "Kreis") {
                        Ext.getCmp("pointstyleiconrotationsquare").setHidden(true);
                        Ext.getCmp("pointstyleiconrotationtriangle").setHidden(true);
                        Ext.getCmp("pointstyleiconrotationexticon").setHidden(true);

                        point_style_params.point_wellknownname = "circle";

                        ctx.beginPath();
                        ctx.arc(50, 50, pattern_thickness, 0, 2 * Math.PI, false);
                        ctx.fillStyle = color;
                        ctx.globalAlpha = fill_opacity;
                        ctx.fill();
                        ctx.lineWidth = stroke_width;
                        ctx.strokeStyle = color_stroke;
                        ctx.globalAlpha = stroke_opacity;
                        ctx.stroke();
                    }
                    else if (point_style === "Dreieck") {
                        Ext.getCmp("pointstyleiconrotationsquare").setHidden(true);
                        Ext.getCmp("pointstyleiconrotationtriangle").setHidden(false);
                        Ext.getCmp("pointstyleiconrotationexticon").setHidden(true);

                        const item_rotation = Ext.getCmp("pointstyleiconrotationtriangle").getSubmitValue();

                        point_style_params.point_wellknownname = "triangle";

                        ctx.clearRect(0, 0, canvas.width, canvas.height);

                        // https://developer.mozilla.org/de/docs/Web/API/Canvas_API/Tutorial/Drawing_shapes
                        if (item_rotation > 0) {
                            // https://stackoverflow.com/questions/24391461/how-to-rotate-triangle-around-center
                            const pos_x = 50;
                            const pos_y = 50;
                            const sides = 3;
                            const a = (Math.PI * 2) / sides;

                            let side_length = parseInt(pattern_thickness);

                            // the components have negative contributions
                            if (item_rotation === 270) {
                                side_length *= -1;
                            }

                            ctx.beginPath();

                            if (item_rotation === 270 || item_rotation === 90) {
                                ctx.beginPath();
                                ctx.moveTo(pos_x + side_length, pos_y);

                                for (let i = 1; i < sides + 1; i++) {
                                    ctx.lineTo(pos_x + side_length * Math.cos(a * i), pos_y + side_length * Math.sin(a * i));
                                }
                                ctx.closePath();
                            }
                            else if (item_rotation === 180) {
                                ctx.beginPath();
                                ctx.moveTo(pos_y, pos_x + side_length);

                                for (let i = 1; i < sides + 1; i++) {
                                    ctx.lineTo(pos_y + side_length * Math.sin(a * i), pos_x + side_length * Math.cos(a * i));
                                }
                                ctx.closePath();
                            }

                            ctx.fillStyle = color;
                            ctx.globalAlpha = fill_opacity;
                            ctx.fill();
                            ctx.globalAlpha = stroke_opacity;
                            ctx.lineWidth = stroke_width;
                            ctx.strokeStyle = color_stroke;
                            ctx.stroke();
                        }
                        else {
                            const side = pattern_thickness * 2;
                            const h = side * (Math.sqrt(3) / 2);

                            ctx.beginPath();
                            ctx.translate(50, 50);
                            ctx.beginPath();
                            ctx.moveTo(0, -h / 2);
                            ctx.lineTo(-side / 2, h / 2);
                            ctx.lineTo(side / 2, h / 2);
                            ctx.lineTo(0, -h / 2);
                            ctx.closePath();
                            ctx.translate(-50, -50);
                            ctx.fillStyle = color;
                            ctx.globalAlpha = fill_opacity;
                            ctx.fill();
                            ctx.globalAlpha = stroke_opacity;
                            ctx.lineWidth = stroke_width;
                            ctx.strokeStyle = color_stroke;
                            ctx.stroke();
                            ctx.restore();
                        }
                    }
                    else if (point_style === "Quadrat") {
                        Ext.getCmp("pointstyleiconrotationsquare").setHidden(false);
                        Ext.getCmp("pointstyleiconrotationtriangle").setHidden(true);
                        Ext.getCmp("pointstyleiconrotationexticon").setHidden(true);

                        point_style_params.point_wellknownname = "square";

                        const item_rotation = Ext.getCmp("pointstyleiconrotationsquare").getSubmitValue();
                        const x = 50 - pattern_thickness;
                        const y = pattern_thickness * 2;

                        if (item_rotation > 0) {
                            ctx.beginPath();
                            ctx.rect(x, x, y, y);
                            ctx.closePath();

                            // the fill color
                            ctx.fillStyle = color;

                            // Matrix transformation
                            ctx.translate(50, 50);
                            ctx.rotate(item_rotation * Math.PI / 180);
                            ctx.translate(-50, -50);
                            ctx.globalAlpha = fill_opacity;
                            ctx.fillRect(x, x, y, y);

                            ctx.beginPath();
                            ctx.rect(x, x, y, y);
                            ctx.closePath();

                            // the outline
                            ctx.lineWidth = stroke_width;
                            ctx.strokeStyle = color_stroke;
                            ctx.globalAlpha = stroke_opacity;
                            ctx.stroke();

                            ctx.restore();
                        }
                        else {
                            ctx.fillStyle = color;
                            ctx.globalAlpha = fill_opacity;
                            ctx.fillRect(x, x, y, y);

                            ctx.beginPath();
                            ctx.rect(x, x, y, y);
                            ctx.closePath();

                            // the outline
                            ctx.lineWidth = stroke_width;
                            ctx.strokeStyle = color_stroke;
                            ctx.globalAlpha = stroke_opacity;
                            ctx.stroke();
                            ctx.restore();
                        }
                    }
                    else if (point_style === "Stern") {
                        Ext.getCmp("pointstyleiconrotationsquare").setHidden(true);
                        Ext.getCmp("pointstyleiconrotationtriangle").setHidden(true);
                        Ext.getCmp("pointstyleiconrotationexticon").setHidden(true);

                        point_style_params.point_wellknownname = "star";

                        let rot = Math.PI / 2 * 3;
                        const cx = 50;
                        const cy = 50;
                        let x = 50;
                        let y = 50;
                        const step = Math.PI / 5;

                        ctx.globalAlpha = stroke_opacity;
                        ctx.beginPath();
                        ctx.moveTo(cx, cy - pattern_thickness);

                        for (let i = 0; i < 5; i++) {
                            x = cx + Math.cos(rot) * pattern_thickness;
                            y = cy + Math.sin(rot) * pattern_thickness;

                            ctx.lineTo(x, y);
                            rot += step;

                            x = cx + Math.cos(rot) * (pattern_thickness / 2);
                            y = cy + Math.sin(rot) * (pattern_thickness / 2);
                            ctx.lineTo(x, y);
                            rot += step;
                        }

                        ctx.lineTo(cx, cy - pattern_thickness);
                        ctx.closePath();
                        ctx.lineWidth = stroke_width;
                        ctx.strokeStyle = color_stroke;
                        ctx.stroke();
                        ctx.globalAlpha = fill_opacity;
                        ctx.fillStyle = color;
                        ctx.fill();
                        ctx.restore();
                    }
                    else if (point_style === "Externes Icon") {
                        point_style_params.point_wellknownname = "externalGraphic";

                        model[0].set("point_wellknownname", point_style_params.point_wellknownname);

                        Ext.getCmp("pointstyleuploadform").setHidden(false);
                        Ext.getCmp("pointstylestrokecolorfieldset").setHidden(true);
                        Ext.getCmp("pointstylefillcolorfieldset").setHidden(true);
                        Ext.getCmp("pointstylestrokewidth").setHidden(true);
                        Ext.getCmp("pointstyleiconrotationsquare").setHidden(true);
                        Ext.getCmp("pointstyleiconrotationtriangle").setHidden(true);

                        if (!model[0].data.file_name_exticon) {
                            Ext.getCmp("pointstyleminscaledenominator").setHidden(true);
                            Ext.getCmp("pointstylemaxscaledenominator").setHidden(true);
                            Ext.getCmp("pointstylesize").setHidden(true);
                            Ext.getCmp("stylefilterconfig").setHidden(true);
                            Ext.getCmp("pointstyleiconrotationexticon").setHidden(true);
                        }
                        else {
                            Ext.getCmp("pointstyleminscaledenominator").setHidden(false);
                            Ext.getCmp("pointstylemaxscaledenominator").setHidden(false);
                            Ext.getCmp("pointstylesize").setHidden(false);
                            Ext.getCmp("stylefilterconfig").setHidden(false);
                            Ext.getCmp("pointstyleiconrotationexticon").setHidden(false);

                            const item_rotation = Ext.getCmp("pointstyleiconrotationexticon").getSubmitValue();
                            const strDataURI = model[0].data.data_url_exticon;
                            const img = new Image();

                            img.addEventListener("load", function () {

                                ctx.clearRect(0, 0, canvas.width, canvas.height);

                                if (item_rotation > 0) {
                                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                                    ctx.save();
                                    ctx.translate(canvas.width / 2, canvas.height / 2);
                                    ctx.rotate(item_rotation * Math.PI / 180);
                                    ctx.globalAlpha = stroke_opacity;
                                    ctx.drawImage(img, -parseInt(pattern_thickness), -parseInt(pattern_thickness), parseInt(pattern_thickness) * 2, parseInt(pattern_thickness) * 2 * img.height / img.width);
                                    ctx.restore();
                                }
                                else {
                                    ctx.globalAlpha = stroke_opacity;
                                    ctx.drawImage(img, 0, 0, parseInt(pattern_thickness) * 2, parseInt(pattern_thickness) * 2 * img.height / img.width);
                                }
                            });
                            img.setAttribute("src", strDataURI);
                        }
                    }

                    if (point_style !== "Externes Icon") {
                        model[0].set("point_wellknownname", point_style_params.point_wellknownname);
                    }
                }
            }
        }
        else {
            if (Ext.getCmp("styles_useforallcollections")) {
                Ext.getCmp("rule_name_id").setHidden(true);
                Ext.getCmp("rule_name_col_id").setHidden(true);
                Ext.getCmp("styles_useforallcollections").setHidden(true);
            }
        }
    },

    /**
     * update canvas field with given style parameters
     * @returns {void}
     */
    onDrawLine: function () {
        const count = Ext.getStore("DatasetCollectionStyle").count();

        if (count > 0) {
            const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();

            if (model.length > 0) {
                const canvas = document.getElementById("line-style-canvas");
                const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(model[0].data.geom_type);
                const ctx = canvas.getContext("2d");
                const stroke_opacity = Ext.getCmp("linestylestrokeopacity").getSubmitValue();
                const stroke_width = Ext.getCmp("linestylestrokewidth").getSubmitValue();
                const dash_array = parseInt(Ext.getCmp("linestyledasharray").getSubmitValue());
                const line_style = Ext.getCmp("linestylepatternstyle").getSubmitValue();
                const color_stroke = "#" + Ext.getCmp("linestylestrokecolorhex").getValue();

                if (canvas.getContext && geomType === "line" && stroke_width && dash_array && line_style) {
                    Ext.getCmp("stylefilterconfig").setHidden(false);

                    let origin = 0;

                    if (stroke_width % 2 !== 0) {
                        origin = 0.5;
                    }

                    ctx.clearRect(0, 0, canvas.width, canvas.height);

                    if (line_style === "gestrichelt") {
                        ctx.setLineDash([dash_array, dash_array]);
                    }
                    else {
                        ctx.setLineDash([0]);
                    }

                    ctx.globalAlpha = stroke_opacity;
                    ctx.strokeStyle = color_stroke;
                    ctx.lineWidth = stroke_width;

                    ctx.beginPath();
                    ctx.moveTo(0, 50 + origin);
                    ctx.lineTo(100, 50 + origin);
                    ctx.stroke();
                }
            }
        }
        else {
            if (Ext.getCmp("styles_useforallcollections")) {
                Ext.getCmp("rule_name_id").setHidden(true);
                Ext.getCmp("rule_name_col_id").setHidden(true);
                Ext.getCmp("styles_useforallcollections").setHidden(true);
            }
        }
    },

    /**
     * update canvas field with given style parameters
     * @returns {void}
     */
    onDrawPolygon: function () {
        const count = Ext.getStore("DatasetCollectionStyle").count();

        if (count > 0) {
            const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();

            if (model.length > 0) {
                const data = model[0].data;
                const geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(data.geom_type);
                const canvas = document.getElementById("polygon-style-canvas");
                const canvas_surround = document.getElementById("polygon-style-surround-canvas");
                const fill_type = Ext.getCmp("polygonstylefilltype").getSubmitValue();
                const ctx = canvas.getContext("2d");
                const ctx_sourround = canvas_surround.getContext("2d");
                const stroke_opacity = Ext.getCmp("polygonstylestrokeopacity").getSubmitValue();
                const fill_opacity = Ext.getCmp("polygonstylefillopacity").getSubmitValue();
                const line_width = Ext.getCmp("polygonstylepatternthickness").getSubmitValue();
                const stroke_width = Ext.getCmp("polygonstylestrokewidth").getSubmitValue();
                const strokecolor_dynamic = Ext.getCmp("polygonstylestrokecolordynamiccheckbox").getSubmitValue();
                const fillcolor_dynamic = Ext.getCmp("polygonstylefillcolordynamiccheckbox").getSubmitValue();

                let color = "#" + Ext.getCmp("polygonstylefillcolorhex").getValue();
                let color_stroke = "#" + Ext.getCmp("polygonstylestrokecolorhex").getValue();

                if (canvas && geomType === "polygon" && stroke_width && color && color_stroke) {
                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                    ctx_sourround.clearRect(0, 0, canvas.width, canvas.height);

                    if (fill_type === "gemustert") {
                        Ext.getCmp("polygonstylestrokecolordynamiccheckbox").setHidden(true);
                        Ext.getCmp("polygonstylefillcolordynamiccheckbox").setHidden(true);
                        Ext.getCmp("polygonstylefillcolorhex").setHidden(false);
                        Ext.getCmp("polygonstylestrokecolorhex").setHidden(false);
                        Ext.getCmp("stylefilterconfig").setHidden(false);
                        Ext.getCmp("polygonstyleiconrotationexticon").setHidden(true);

                        const style = Ext.getCmp("polygonstylepatternstyle").getSubmitValue();
                        const numberOfStripes = Ext.getCmp("polygonstylepatterndensity").getValue();

                        let origin = 0;

                        if (line_width % 2 !== 0) {
                            origin = 0.5;
                        }

                        ctx.strokeStyle = color;
                        ctx.lineWidth = line_width;

                        if (style === "schräg links") {
                            const thickness = (canvas.width / numberOfStripes) * 2;

                            for (let i = 0; i < numberOfStripes + 1; i++) {
                                ctx.beginPath();
                                ctx.globalAlpha = fill_opacity;
                                ctx.moveTo(i * thickness - canvas.width - 50, -50);
                                ctx.lineTo(i * thickness + 50, canvas.width + 50);
                                ctx.stroke();
                            }
                        }
                        else if (style === "schräg rechts") {
                            const thickness = (canvas.width / numberOfStripes) * 2;

                            for (let i = 0; i < numberOfStripes + 1; i++) {
                                ctx.beginPath();
                                ctx.globalAlpha = fill_opacity;
                                ctx.moveTo(i * thickness - canvas.width - 50, canvas.width + 50);
                                ctx.lineTo(i * thickness + 50, -50);
                                ctx.stroke();
                            }
                        }
                        else if (style === "horizontal") {
                            const thickness = canvas.width / numberOfStripes;

                            for (let i = 0; i < numberOfStripes; i++) {
                                ctx.beginPath();
                                ctx.globalAlpha = fill_opacity;
                                ctx.moveTo(0, i * thickness + thickness - ctx.lineWidth + origin);
                                ctx.lineTo(100, i * thickness + thickness - ctx.lineWidth + origin);
                                ctx.stroke();
                            }
                        }
                        else if (style === "vertikal") {
                            const thickness = canvas.width / numberOfStripes;

                            for (let i = 0; i < numberOfStripes; i++) {
                                ctx.beginPath();
                                ctx.globalAlpha = fill_opacity;
                                ctx.moveTo(i * thickness + thickness - ctx.lineWidth + origin, 0);
                                ctx.lineTo(i * thickness + thickness - ctx.lineWidth + origin, canvas.width);
                                ctx.stroke();
                            }
                        }
                        else if (style === "Raster gerade") {
                            const thickness = canvas.width / numberOfStripes;

                            for (let i = 0; i < numberOfStripes; i++) {
                                ctx.beginPath();
                                ctx.globalAlpha = fill_opacity;
                                ctx.moveTo(i * thickness + thickness - ctx.lineWidth + origin, 0);
                                ctx.lineTo(i * thickness + thickness - ctx.lineWidth + origin, canvas.width);
                                ctx.stroke();
                            }
                            for (let i = 0; i < numberOfStripes; i++) {
                                ctx.beginPath();
                                ctx.globalAlpha = fill_opacity;
                                ctx.moveTo(0, i * thickness + thickness - ctx.lineWidth + origin);
                                ctx.lineTo(100, i * thickness + thickness - ctx.lineWidth + origin);
                                ctx.stroke();
                            }
                        }
                        else if (style === "Raster schräg") {
                            const thickness = (canvas.width / numberOfStripes) * 2;

                            for (let i = 0; i < numberOfStripes + 1; i++) {
                                ctx.beginPath();
                                ctx.globalAlpha = fill_opacity;
                                ctx.moveTo(i * thickness - canvas.width - 50, -50);
                                ctx.lineTo(i * thickness + 50, canvas.width + 50);
                                ctx.stroke();
                            }
                            for (let i = 0; i < numberOfStripes + 1; i++) {
                                ctx.beginPath();
                                ctx.globalAlpha = fill_opacity;
                                ctx.moveTo(i * thickness - canvas.width - 50, canvas.width + 50);
                                ctx.lineTo(i * thickness + 50, -50);
                                ctx.stroke();
                            }
                        }
                        else if (style === "Punkte") {
                            const thickness = canvas.width / numberOfStripes;

                            ctx.fillStyle = color;

                            for (let i = 0; i < numberOfStripes; i++) {
                                for (let j = 0; j < numberOfStripes; j++) {
                                    ctx.beginPath();
                                    ctx.globalAlpha = fill_opacity;
                                    ctx.arc(i * thickness + (thickness - origin) / 2, j * thickness + (thickness - origin) / 2, line_width / 2, 0, Math.PI * 2, true);
                                    ctx.fill();
                                    ctx.closePath();
                                }
                            }
                        }
                        else if (style === "Punkte versetzt") {
                            const thickness = canvas.width / numberOfStripes;

                            ctx.fillStyle = color;

                            for (let i = 0; i < numberOfStripes; i++) {
                                for (let j = 0; j < numberOfStripes; j++) {
                                    let offset = 0;

                                    if (i % 2 !== 0) {
                                        offset = thickness / 2;
                                    }

                                    ctx.beginPath();
                                    ctx.globalAlpha = fill_opacity;
                                    ctx.arc(i * thickness + ((thickness - origin) / 2) - line_width, j * thickness + ((thickness - origin) / 2) + offset - line_width, line_width / 2, 0, Math.PI * 2, true);
                                    ctx.fill();
                                    ctx.closePath();
                                }
                            }
                        }

                        if (stroke_width > 0) {
                            ctx_sourround.strokeStyle = color_stroke;
                            ctx_sourround.lineWidth = stroke_width;
                            ctx_sourround.globalAlpha = stroke_opacity;
                            ctx_sourround.strokeRect(stroke_width / 2, stroke_width / 2, canvas.width - stroke_width, canvas.height - stroke_width);
                        }

                        model[0].set("data_url", canvas.toDataURL());
                        model[0].set("file_content_type", "image/png");
                    }
                    else if (fill_type === "einfarbig") {
                        Ext.getCmp("stylefilterconfig").setHidden(false);
                        Ext.getCmp("polygonstyleiconrotationexticon").setHidden(true);
                        if (fillcolor_dynamic) {
                            color = "#406edc";

                            Ext.getCmp("polygonstylefillcolordynamicattribute").setHidden(false);
                            Ext.getCmp("polygonstylefillcolorhex").setHidden(true);
                        }
                        else {
                            Ext.getCmp("polygonstylefillcolordynamicattribute").setHidden(true);
                        }

                        if (strokecolor_dynamic) {
                            color_stroke = "#406edc";

                            Ext.getCmp("polygonstylestrokecolordynamicattribute").setHidden(false);
                            Ext.getCmp("polygonstylestrokecolorhex").setHidden(true);
                        }
                        else {
                            Ext.getCmp("polygonstylestrokecolordynamicattribute").setHidden(true);
                            Ext.getCmp("polygonstylestrokecolorhex").setHidden(false);
                        }

                        ctx.fillStyle = color;
                        ctx.globalAlpha = fill_opacity;
                        ctx.fillRect(0, 0, canvas.width, canvas.height);

                        if (stroke_width > 0) {
                            ctx_sourround.globalAlpha = stroke_opacity;
                            ctx_sourround.strokeStyle = color_stroke;
                            ctx_sourround.lineWidth = stroke_width;
                            ctx_sourround.strokeRect(stroke_width / 2, stroke_width / 2, canvas.width - stroke_width, canvas.height - stroke_width);
                        }

                        model[0].set("data_url", canvas.toDataURL());
                    }
                    else if (fill_type === "Externes Icon") {
                        Ext.getCmp("polygonstylestrokecolorfieldset").setHidden(true);
                        Ext.getCmp("polygonstylefillcolorfieldset").setHidden(true);
                        //Ext.getCmp("polygonstyleuploadform").setHidden(false);
                        Ext.getCmp("stylefilterconfig").setHidden(true);

                        if (strokecolor_dynamic) {
                            color_stroke = "#406edc";

                            Ext.getCmp("polygonstylestrokecolordynamicattribute").setHidden(false);
                            Ext.getCmp("polygonstylestrokecolorhex").setHidden(true);
                        }
                        else {
                            Ext.getCmp("polygonstylestrokecolordynamicattribute").setHidden(true);
                            Ext.getCmp("polygonstylestrokecolorhex").setHidden(false);
                        }

                        if (!model[0].data.file_name_exticon) {
                            Ext.getCmp("polygonstyleminscaledenominator").setHidden(true);
                            Ext.getCmp("polygonstylemaxscaledenominator").setHidden(true);
                            Ext.getCmp("polygonstyleiconrotationexticon").setHidden(true);
                        }
                        else {
                            Ext.getCmp("polygonstyleminscaledenominator").setHidden(false);
                            Ext.getCmp("polygonstylemaxscaledenominator").setHidden(false);
                            Ext.getCmp("polygonstylestrokecolorfieldset").setHidden(false);
                            Ext.getCmp("polygonstylestrokewidth").setHidden(false);
                            Ext.getCmp("polygonstyleiconsize").setHidden(false);
                            Ext.getCmp("polygonstyleiconrotationexticon").setHidden(false);
                            Ext.getCmp("stylefilterconfig").setHidden(false);

                            const icon_size = Ext.getCmp("polygonstyleiconsize").getSubmitValue();

                            //model[0].set("data_url", canvas.toDataURL());

                            const item_rotation = Ext.getCmp("polygonstyleiconrotationexticon").getSubmitValue();
                            const strDataURI = model[0].data.data_url_exticon;
                            const img = new Image();

                            img.addEventListener("load", function () {
                                ctx.clearRect(0, 0, canvas.width, canvas.height);

                                if (item_rotation > 0) {
                                    ctx.clearRect(0, 0, canvas.width, canvas.height);
                                    ctx.save();
                                    ctx.translate(canvas.width / 2, canvas.height / 2);
                                    ctx.rotate(item_rotation * Math.PI / 180);
                                    ctx.globalAlpha = stroke_opacity;
                                    ctx.drawImage(img, -parseInt(icon_size), -parseInt(icon_size), parseInt(icon_size) * 2, parseInt(icon_size) * 2 * img.height / img.width);
                                    ctx.restore();
                                }
                                else {
                                    if (stroke_width > 0) {
                                        ctx_sourround.clearRect(0, 0, canvas.width, canvas.height);
                                        ctx_sourround.strokeStyle = color_stroke;
                                        ctx_sourround.lineWidth = stroke_width;
                                        ctx_sourround.globalAlpha = stroke_opacity;
                                        ctx_sourround.strokeRect(stroke_width / 2, stroke_width / 2, canvas.width - stroke_width, canvas.height - stroke_width);
                                    }
                                    ctx.globalAlpha = stroke_opacity;
                                    ctx.drawImage(img, 0, 0, parseInt(icon_size) * 2, parseInt(icon_size) * 2 * img.height / img.width);
                                }
                            });
                            img.setAttribute("src", strDataURI);
                        }
                    }
                }
            }
        }
        else {
            if (Ext.getCmp("styles_useforallcollections")) {
                Ext.getCmp("rule_name_id").setHidden(true);
                Ext.getCmp("rule_name_col_id").setHidden(true);
                Ext.getCmp("styles_useforallcollections").setHidden(true);
            }
        }
    },

    /**
     * update canvas field with given label parameters
     * @returns {void}
     */
    onDrawLabel: function () {
        const count = Ext.getStore("DatasetCollectionStyle").count();

        if (count > 0) {
            const model = Ext.getCmp("styleeditor-grid").getSelectionModel().getSelection();

            if (model.length > 0) {
                if (model[0].data.text_symbolizer) {
                    const canvas = document.getElementById("text-style-canvas");
                    const ctx = canvas.getContext("2d");
                    const opacity = Ext.getCmp("textstylefillopacity").getSubmitValue();
                    const font = Ext.getCmp("textstylefont").getSubmitValue();
                    const font_size = Ext.getCmp("textstylefontsize").getSubmitValue();
                    const font_weight = Ext.getCmp("textstylefontweight").getSubmitValue();
                    const font_style = Ext.getCmp("textstylefontstyle").getSubmitValue();
                    const color_fill = "#" + Ext.getCmp("textstylefillcolorhex").getValue();
                    const color_halo = "#" + Ext.getCmp("textstylehalocolorhex").getValue();
                    const halo_radius = Ext.getCmp("textstylehaloradius").getSubmitValue();
                    const halo_opacity = Ext.getCmp("textstylehaloopacity").getSubmitValue();

                    if (canvas.getContext && font_size && font_weight && font_style && color_fill && font) {
                        Ext.getCmp("stylefilterconfig").setHidden(false);

                        ctx.clearRect(0, 0, canvas.width, canvas.height);

                        ctx.globalAlpha = halo_opacity;
                        ctx.shadowColor = color_halo;
                        ctx.shadowBlur = halo_radius;
                        ctx.fillStyle = color_fill;
                        ctx.globalAlpha = opacity;
                        ctx.font = font_style + " " + font_weight + " " + font_size + "px " + font;
                        ctx.fillText("Label", 10, 90);
                    }
                }
            }
        }
        else {
            if (Ext.getCmp("styles_useforallcollections")) {
                Ext.getCmp("rule_name_id").setHidden(true);
                Ext.getCmp("rule_name_col_id").setHidden(true);
                Ext.getCmp("styles_useforallcollections").setHidden(true);
            }
        }
    },

    /**
     * save style rules in collection db table
     * @returns {void}
     */
    saveStyle: function () {
        let style_config = [];
        let geomType = "";

        const style_store = Ext.getStore("DatasetCollectionStyle");
        const use_for_all_collections = Ext.getCmp("styles_useforallcollections").getValue();
        const style_config_array = [];
        const dataset_id = Ext.getCmp("datasetid").getValue();

        if (Ext.getCmp("add_style_rule_geom").getSelection()) {
            const geomAttr = Ext.getCmp("add_style_rule_geom").getSelection().data;

            geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(geomAttr.type);
        }
        else if (Ext.getCmp("styleeditor-grid").getSelection().length > 0) {
            const geomAttr = Ext.getCmp("styleeditor-grid").getSelection()[0].data;

            geomType = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getGeomType(geomAttr.geom_type);
        }

        style_store.each(function (record, i) {
            if (record.get("fill_type") === "gemustert" && record.get("file_name")) {
                record.set("file_name", "pattern_" + i + ".png");
            }
            style_config_array.push(record.data);
        });

        style_config = JSON.stringify(style_config_array);

        Ext.Ajax.request({
            url: "backend/updatestyleconfig",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                collection_id: Ext.getCmp("datasetcollectionid").getSubmitValue(),
                use_for_all_collections: use_for_all_collections,
                dataset_id: dataset_id,
                last_edited_by: window.auth_user,
                style: style_config,
                geomType: geomType
            },
            success: function (response) {
                const collectiongrid = Ext.getCmp("grid_datasetcollections");
                const scroll_x = collectiongrid.getView().getScrollX();
                const scroll_y = collectiongrid.getView().getScrollY();
                const selectedCollection = collectiongrid.getSelectionModel().getSelection()[0].data;
                const response_json = Ext.decode(response.responseText);
                const dataset_status = Ext.getCmp("datasetstatus").getValue();

                if (response_json.status === "success") {
                    Ext.getStore("CollectionsDataset").load({
                        params: {dataset_id: dataset_id, dataset_status: dataset_status},
                        callback: function (records) {
                            collectiongrid.getView().setScrollX(scroll_x);
                            collectiongrid.getView().setScrollY(scroll_y);

                            if (records.length > 0) {
                                for (let i = 0; i < records.length; i++) {
                                    if (records[i].data.id === selectedCollection.id) {
                                        const row = collectiongrid.store.indexOf(records[i]);

                                        collectiongrid.getSelectionModel().select(row);
                                        collectiongrid.ensureVisible(row);
                                        Ext.getCmp("styleeditor-window").close();
                                        break;
                                    }
                                }
                            }
                        }
                    });

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Style erfolgreich gespeichert!");
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
            },
            failure: function (response) {
                Ext.MessageBox.alert("Status", "Es ist ein Fehler aufgetreten!");
                console.log(response.responseText);
            }
        });
    },

    /**
     * remove style rule from grid
     * @param {Object} view -
     * @param {Object} recIndex -
     * @param {Object} cellIndex -
     * @param {Object} item -
     * @param {Object} e -
     * @param {Object} record -
     * @returns {void}
     */
    onRemoveStyleRule: function (view, recIndex, cellIndex, item, e, record) {
        record.drop();

        if (Ext.getStore("DatasetCollectionStyle").count() === 0) {
            Ext.getCmp("style_editor_formular_point").setHidden(true);
            Ext.getCmp("style_editor_formular_polygon").setHidden(true);
            Ext.getCmp("style_editor_formular_line").setHidden(true);
            Ext.getCmp("point-style-canvas-panel").setHidden(true);
            Ext.getCmp("polygon-style-canvas-panel").setHidden(true);
            Ext.getCmp("line-style-canvas-panel").setHidden(true);
            Ext.getCmp("stylefilterconfig").setHidden(true);
        }
    },

    onIconDisplay: function () {
        return "<div data-qtip=\"Drag & Drop zum Umsortieren\"><span class=\"x-fa fa-ellipsis-v\" style=\"color:#00000066\"></span><span class=\"x-fa fa-ellipsis-v\" style=\"color:#00000066\"></span></div>";
    }
});
