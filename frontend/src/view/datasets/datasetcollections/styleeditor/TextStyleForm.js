Ext.define("UDPManager.view.datasets.datasetcollections.styleeditor.TextStyleForm", {
    extend: "Ext.form.Panel",
    xtype: "textstyle-form",
    alias: "styleeditor.TextStyleForm",
    bodyPadding: "10 10 10 10",
    defaultType: "textfield",
    layout: "anchor",
    defaults: {
        anchor: "0",
        labelWidth: 60,
        layout: {
            type: "hbox",
            align: "center"
        }
    },
    items: [
        {
            id: "textstylename",
            labelWidth: 150,
            width: "100%",
            fieldLabel: "Bezeichnung",
            allowBlank: false,
            bind: "{grid_style.selection.rule_name}"
        },
        {
            id: "textstyletitle",
            labelWidth: 150,
            width: "100%",
            fieldLabel: "Titel",
            allowBlank: false,
            bind: "{grid_style.selection.title}"
        },
        {
            xtype: "combobox",
            id: "textstylelabelattribute",
            fieldLabel: "Attribut für Label",
            labelWidth: 150,
            store: [],
            editable: false,
            allowBlank: false,
            sortable: false,
            hideable: false,
            bind: "{grid_style.selection.label_attribute}"
        },
        {
            xtype: "combo",
            fieldLabel: "Font",
            id: "textstylefont",
            editable: false,
            labelWidth: 150,
            flex: 1,
            margin: "10 0 10 0",
            store: ["Arial", "Times"],
            name: "label_font",
            bind: "{grid_style.selection.label_font}",
            listeners: {
                change: "onDrawLabel"
            }
        },
        {
            xtype: "combo",
            fieldLabel: "Fontstil",
            id: "textstylefontstyle",
            editable: false,
            labelWidth: 150,
            flex: 1,
            margin: "10 0 10 0",
            store: ["normal", "italic", "oblique"],
            name: "label_font_style",
            bind: "{grid_style.selection.label_font_style}",
            listeners: {
                change: "onDrawLabel"
            }
        },
        {
            xtype: "combo",
            fieldLabel: "Fontdicke",
            id: "textstylefontweight",
            editable: false,
            labelWidth: 150,
            bind: "{grid_style.selection.label_font_weight}",
            flex: 1,
            margin: "10 0 10 0",
            store: ["normal", "bold"],
            name: "label_font_weight",
            listeners: {
                change: "onDrawLabel"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "Schriftgröße",
            id: "textstylefontsize",
            labelWidth: 150,
            name: "label_font_size",
            bind: "{grid_style.selection.label_font_size}",
            minValue: 1,
            maxValue: 50,
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawLabel"
            }
        },
        {
            xtype: "fieldset",
            id: "textstylefillcolorfieldset",
            title: "Schriftfarbe definieren",
            defaultType: "textfield",
            layout: "anchor",
            defaults: {
                anchor: "0",
                labelWidth: 60,
                layout: {
                    type: "hbox",
                    align: "center"
                }
            },
            items: [
                {
                    xtype: "colorbutton",
                    id: "textstylefillcolorhex",
                    bind: "{grid_style.selection.fill_color_hex}",
                    width: "100%",
                    height: 25,
                    margin: "0, 0, 5, 0",
                    listeners: {
                        change: "onDrawLabel"
                    }
                },
                {
                    xtype: "numberfield",
                    fieldLabel: "Deckkraft",
                    labelWidth: 150,
                    id: "textstylefillopacity",
                    width: "100%",
                    bind: "{grid_style.selection.fill_opacity}",
                    maxValue: 1,
                    minValue: 0,
                    allowDecimals: true,
                    decimalPrecision: 2,
                    step: 0.01,
                    listeners: {
                        change: "onDrawLabel"
                    }
                }
            ]
        },
        {
            xtype: "fieldset",
            id: "textstylehalocolorfieldset",
            title: "Halo definieren",
            defaultType: "textfield",
            layout: "anchor",
            defaults: {
                anchor: "0",
                labelWidth: 60,
                layout: {
                    type: "hbox",
                    align: "center"
                }
            },
            items: [
                {
                    xtype: "numberfield",
                    fieldLabel: "Radius",
                    id: "textstylehaloradius",
                    labelWidth: 150,
                    name: "halo_radius",
                    bind: "{grid_style.selection.halo_radius}",
                    minValue: 0,
                    maxValue: 50,
                    margin: "10 0 10 0",
                    listeners: {
                        change: "onDrawLabel"
                    }
                },
                {
                    xtype: "colorbutton",
                    id: "textstylehalocolorhex",
                    bind: "{grid_style.selection.halo_color_hex}",
                    width: "100%",
                    height: 25,
                    margin: "0, 0, 5, 0",
                    listeners: {
                        change: "onDrawLabel"
                    }
                },
                {
                    xtype: "numberfield",
                    fieldLabel: "Deckkraft",
                    labelWidth: 150,
                    id: "textstylehaloopacity",
                    width: "100%",
                    bind: "{grid_style.selection.halo_opacity}",
                    maxValue: 1,
                    minValue: 0,
                    allowDecimals: true,
                    decimalPrecision: 2,
                    step: 0.01,
                    listeners: {
                        change: "onDrawLabel"
                    }
                }
            ]
        },
        {
            xtype: "combo",
            fieldLabel: "Ausrichtung an",
            id: "textstyletype",
            editable: false,
            labelWidth: 150,
            flex: 1,
            margin: "10 0 10 0",
            store: ["Punkt", "Linie"],
            name: "label_placement",
            bind: "{grid_style.selection.label_placement}",
            listeners: {
                change: "onChangeLabelPlacement"
            }
        },
        {
            xtype: "fieldset",
            id: "textstylepointplacement",
            title: "Ausrichtung an Punkt",
            defaultType: "numberfield",
            layout: "vbox",
            hidden: true,
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    width: "100%",
                    defaultType: "numberfield",
                    items: [
                        {
                            fieldLabel: "Ankerpunkt X",
                            id: "textstyleanchorpointx",
                            labelWidth: 90,
                            width: 160,
                            name: "anchor_point_x",
                            bind: "{grid_style.selection.anchor_point_x}",
                            minValue: 0,
                            maxValue: 50,
                            margin: "10 5 10 0"
                        },
                        {
                            fieldLabel: "Y",
                            id: "textstyleanchorpointy",
                            labelWidth: 20,
                            width: 90,
                            name: "anchor_point_y",
                            bind: "{grid_style.selection.anchor_point_y}",
                            minValue: 0,
                            maxValue: 50,
                            margin: "10 0 10 5"
                        }
                    ]
                },
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    width: "100%",
                    defaultType: "numberfield",
                    items: [
                        {
                            fieldLabel: "Versatz X",
                            id: "textstyledisplacementx",
                            labelWidth: 90,
                            width: 160,
                            name: "displacement_x",
                            bind: "{grid_style.selection.displacement_x}",
                            minValue: 0,
                            maxValue: 200,
                            margin: "10 5 10 0"
                        },
                        {
                            fieldLabel: "Y",
                            id: "textstyledisplacementy",
                            labelWidth: 20,
                            width: 90,
                            name: "displacement_y",
                            bind: "{grid_style.selection.displacement_y}",
                            minValue: 0,
                            maxValue: 200,
                            margin: "10 0 10 5"
                        }
                    ]
                },
                {
                    fieldLabel: "Rotation",
                    id: "textstylerotation",
                    labelWidth: 90,
                    width: 160,
                    name: "rotation",
                    bind: "{grid_style.selection.rotation}",
                    minValue: 0,
                    maxValue: 360,
                    margin: "10 0 10 0"
                }
            ]
        },
        {
            xtype: "fieldset",
            id: "textstylelineplacement",
            title: "Ausrichtung an Linie",
            defaultType: "numberfield",
            layout: "vbox",
            hidden: true,
            items: [
                {
                    xtype: "checkbox",
                    boxLabel: "Wiederholend",
                    labelWidth: 150,
                    checked: false,
                    id: "textstyleisrepeated",
                    name: "is_repeated",
                    bind: "{grid_style.selection.is_repeated}"
                },
                {
                    fieldLabel: "Anfangsabstand",
                    id: "textstyleinitialgap",
                    labelWidth: 150,
                    name: "initial_gap",
                    bind: "{grid_style.selection.initial_gap}",
                    minValue: 0,
                    maxValue: 1000,
                    margin: "10 0 10 0"
                },
                {
                    xtype: "checkbox",
                    boxLabel: "Ausgerichtet",
                    labelWidth: 150,
                    checked: false,
                    id: "textstyleisaligned",
                    name: "is_aligned",
                    bind: "{grid_style.selection.is_aligned}"
                }
            ]
        },
        {
            xtype: "numberfield",
            fieldLabel: "MinScaleDenominator",
            id: "textstyleminscaledenominator",
            labelWidth: 150,
            allowDecimals: false,
            bind: "{grid_style.selection.minscaledenominator}",
            allowBlank: true,
            margin: "10 0 10 0",
            maxValue: 10000000,
            minValue: 0
        },
        {
            xtype: "numberfield",
            fieldLabel: "MaxScaleDenominator",
            id: "textstylemaxscaledenominator",
            labelWidth: 150,
            bind: "{grid_style.selection.maxscaledenominator}",
            allowDecimals: false,
            allowBlank: true,
            margin: "10 0 10 0",
            maxValue: 10000000,
            minValue: 0
        }
    ]
});
