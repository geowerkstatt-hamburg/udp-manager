Ext.define("UDPManager.view.datasets.datasetcollections.styleeditor.PolygonStyleForm", {
    extend: "Ext.form.Panel",
    xtype: "polygonstyle-form",
    alias: "styleeditor.PolygonStyleForm",
    bodyPadding: "10 10 10 10",
    defaultType: "textfield",
    layout: "anchor",
    defaults: {
        anchor: "0",
        labelWidth: 60,
        layout: {
            type: "hbox",
            align: "center"
        }
    },
    items: [
        {
            id: "polygonstylename",
            labelWidth: 150,
            width: "100%",
            fieldLabel: "Bezeichnung",
            allowBlank: false,
            bind: "{grid_style.selection.rule_name}"
        },
        {
            id: "polygonstyletitle",
            labelWidth: 150,
            width: "100%",
            fieldLabel: "Titel",
            allowBlank: false,
            bind: "{grid_style.selection.title}"
        },
        {
            xtype: "combo",
            fieldLabel: "Füllung",
            id: "polygonstylefilltype",
            allowBlank: false,
            labelWidth: 150,
            margin: "10 0 10 0",
            store: ["einfarbig", "gemustert", "Externes Icon"],
            bind: "{grid_style.selection.fill_type}",
            listeners: {
                change: "onChangeFillType"
            }
        },
        {
            xtype: "fieldset",
            id: "polygonstylestrokecolorfieldset",
            title: "Umringfarbe definieren",
            defaultType: "textfield",
            layout: "anchor",
            width: "100%",
            defaults: {
                anchor: "0",
                labelWidth: 60,
                layout: {
                    type: "hbox",
                    align: "center"
                }
            },
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "vbox",
                    defaults: {
                        height: 35,
                        flex: 1
                    },
                    items: [
                        {
                            xtype: "checkbox",
                            boxLabel: "Farbwert aus Attribut",
                            labelWidth: 150,
                            checked: false,
                            disabled: true,
                            id: "polygonstylestrokecolordynamiccheckbox",
                            name: "strokecolor_dynamic",
                            bind: "{grid_style.selection.stroke_color_dynamic}",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Hex-Farbwerte aus Datenbank, Format: #946edc"
                            },
                            listeners: {
                                change: "onStrokeDynamic"
                            }
                        },
                        {
                            xtype: "combobox",
                            id: "polygonstylestrokecolordynamicattribute",
                            name: "stroke_color_dynamic_attribute",
                            bind: "{grid_style.selection.stroke_color_dynamic_attribute}",
                            fieldLabel: "Attribut",
                            labelWidth: 150,
                            store: [],
                            editable: false,
                            allowBlank: false,
                            sortable: false,
                            hideable: false,
                            hidden: true,
                            listeners: {
                                change: "onDrawPolygon"
                            }
                        }
                    ]
                },
                {
                    xtype: "colorbutton",
                    id: "polygonstylestrokecolorhex",
                    bind: "{grid_style.selection.stroke_color_hex}",
                    width: "100%",
                    height: 25,
                    margin: "0, 0, 5, 0",
                    listeners: {
                        change: "onDrawPolygon"
                    }
                },
                {
                    xtype: "numberfield",
                    fieldLabel: "Deckkraft",
                    labelWidth: 150,
                    id: "polygonstylestrokeopacity",
                    width: "100%",
                    bind: "{grid_style.selection.stroke_opacity}",
                    maxValue: 1,
                    minValue: 0,
                    allowDecimals: true,
                    decimalPrecision: 2,
                    step: 0.01,
                    listeners: {
                        change: "onDrawPolygon"
                    }
                }
            ]
        },
        {
            xtype: "numberfield",
            fieldLabel: "Umring Strichbreite",
            id: "polygonstylestrokewidth",
            labelWidth: 150,
            bind: "{grid_style.selection.stroke_width}",
            minValue: 0,
            maxValue: 10,
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawPolygon"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "Icongröße",
            id: "polygonstyleiconsize",
            labelWidth: 150,
            hidden: true,
            bind: "{grid_style.selection.icon_size}",
            minValue: 0,
            maxValue: 200,
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawPolygon"
            }
        },
        {
            xtype: "fieldset",
            id: "polygonstylefillcolorfieldset",
            title: "Füllfarbe definieren",
            defaultType: "textfield",
            layout: "anchor",
            defaults: {
                anchor: "0",
                labelWidth: 60,
                layout: {
                    type: "hbox",
                    align: "center"
                }
            },
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "vbox",
                    defaults: {
                        height: 35,
                        flex: 1
                    },
                    items: [
                        {
                            xtype: "checkbox",
                            boxLabel: "Farbwert aus Attribut",
                            labelWidth: 150,
                            checked: false,
                            disabled: true,
                            id: "polygonstylefillcolordynamiccheckbox",
                            bind: "{grid_style.selection.fill_color_dynamic}",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Hex-Farbwerte aus Datenbank, Format: #946edc"
                            },
                            listeners: {
                                change: "onFillDynamic"
                            }
                        }, {
                            xtype: "combobox",
                            id: "polygonstylefillcolordynamicattribute",
                            fieldLabel: "Attribut",
                            labelWidth: 150,
                            store: [],
                            editable: false,
                            allowBlank: false,
                            sortable: false,
                            hideable: false,
                            bind: "{grid_style.selection.fill_color_dynamic_attribute}",
                            hidden: true,
                            listeners: {
                                change: "onDrawPolygon"
                            }
                        }
                    ]
                },
                {
                    xtype: "colorbutton",
                    id: "polygonstylefillcolorhex",
                    bind: "{grid_style.selection.fill_color_hex}",
                    width: "100%",
                    height: 25,
                    margin: "0, 0, 5, 0",
                    listeners: {
                        change: "onDrawPolygon"
                    }
                },
                {
                    xtype: "numberfield",
                    fieldLabel: "Deckkraft",
                    labelWidth: 150,
                    id: "polygonstylefillopacity",
                    width: "100%",
                    bind: "{grid_style.selection.fill_opacity}",
                    maxValue: 1,
                    minValue: 0,
                    allowDecimals: true,
                    decimalPrecision: 2,
                    step: 0.01,
                    listeners: {
                        change: "onDrawPolygon"
                    }
                }
            ]
        },
        {
            xtype: "combo",
            fieldLabel: "Rotation",
            id: "polygonstyleiconrotationexticon",
            labelWidth: 150,
            hidden: true,
            editable: false,
            allowBlank: false,
            store: [0, 90, 180, 270],
            bind: "{grid_style.selection.icon_rotation}",
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawPolygon"
            }
        },
        {
            xtype: "combo",
            fieldLabel: "Stil",
            id: "polygonstylepatternstyle",
            hidden: true,
            labelWidth: 150,
            store: ["horizontal", "vertikal", "schräg links", "schräg rechts", "Raster gerade", "Raster schräg", "Punkte", "Punkte versetzt"],
            bind: "{grid_style.selection.pattern_style}",
            listeners: {
                change: "onDrawPolygon"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "Muster Strichbreite",
            id: "polygonstylepatternthickness",
            hidden: true,
            labelWidth: 150,
            bind: "{grid_style.selection.pattern_thickness}",
            minValue: 1,
            maxValue: 10,
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawPolygon"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "Musterdichte",
            id: "polygonstylepatterndensity",
            hidden: true,
            labelWidth: 150,
            bind: "{grid_style.selection.pattern_density}",
            minValue: 2,
            maxValue: 50,
            step: 2,
            listeners: {
                change: "onDrawPolygon"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "MinScaleDenominator",
            id: "polygonstyleminscaledenominator",
            labelWidth: 150,
            allowDecimals: false,
            bind: "{grid_style.selection.minscaledenominator}",
            allowBlank: true,
            margin: "10 0 10 0",
            maxValue: 10000000,
            minValue: 0
        },
        {
            xtype: "numberfield",
            fieldLabel: "MaxScaleDenominator",
            id: "polygonstylemaxscaledenominator",
            labelWidth: 150,
            bind: "{grid_style.selection.maxscaledenominator}",
            allowDecimals: false,
            allowBlank: true,
            margin: "10 0 10 0",
            maxValue: 10000000,
            minValue: 0
        },
        {
            xtype: "form",
            id: "polygonstyleuploadform",
            hidden: true,
            items:
                [
                    {
                        xtype: "container",
                        flex: 1,
                        layout: {
                            type: "vbox",
                            align: "stretch"
                        },
                        items:
                            [
                                {
                                    xtype: "filefield",
                                    buttonOnly: true,
                                    id: "polygonstylefilefield",
                                    buttonConfig: {
                                        width: 110,
                                        text: "Icon hochladen",
                                        tooltip: "Uploadformate: png, svg, gif",
                                        baseCls: "x-btn"
                                    },
                                    listeners: {
                                        change: "onUpload"
                                    }
                                }
                            ]
                    }
                ]
        }
    ]
});
