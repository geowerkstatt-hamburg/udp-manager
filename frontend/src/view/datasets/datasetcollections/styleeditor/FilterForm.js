Ext.define("UDPManager.view.datasets.datasetcollections.styleeditor.FilterForm", {
    extend: "Ext.form.FieldSet",
    xtype: "style-filterform",
    alias: "styleeditor.FilterForm",
    frame: true,
    margin: "10 10 10 10",
    collapsible: true,
    items: [
        {
            xtype: "panel",
            layout: "vbox",
            items: [
                {
                    xtype: "button",
                    id: "styleresetfilter",
                    buttonAlign: "center",
                    tooltip: "Alle Filtereingaben löschen",
                    text: "Zurücksetzen",
                    disabled: true,
                    listeners: {
                        click: "onResetFilter"
                    }
                },
                {
                    xtype: "combobox",
                    fieldLabel: "Attribut",
                    id: "stylefilterattributes",
                    editable: false,
                    allowBlank: false,
                    sortable: false,
                    hideable: false,
                    labelWidth: 150,
                    bind: "{grid_style.selection.filter_attribute}",
                    width: "100%",
                    store: [],
                    listeners: {
                        change: "onChangeFilterRule"
                    }
                },
                {
                    xtype: "combobox",
                    fieldLabel: "Bedingung",
                    id: "stylefiltercondition",
                    editable: false,
                    sortable: false,
                    hideable: false,
                    labelWidth: 150,
                    bind: "{grid_style.selection.filter_condition}",
                    width: "100%",
                    allowBlank: false,
                    store: ["PropertyIsEqualTo", "PropertyIsNotEqualTo", "PropertyIsLike", "PropertyIsLessThan", "PropertyIsGreaterThan", "PropertyIsLessThanOrEqualTo", "PropertyIsGreaterThanOrEqualTo", "PropertyIsNull", "PropertyIsBetween"],
                    listeners: {
                        change: "onChangeFilterRule"
                    }
                },
                {
                    xtype: "component",
                    id: "stylefilterislikehelp",
                    hidden: true,
                    margin: "0 0 5 155",
                    html: "wildCard = *, singleChar = #, escapeChar = !"
                },
                {
                    xtype: "textfield",
                    id: "stylefilterliteral",
                    labelWidth: 150,
                    width: "100%",
                    fieldLabel: "Literal",
                    hidden: true,
                    allowBlank: false,
                    bind: "{grid_style.selection.filter_literal}",
                    listeners: {
                        change: "onChangeFilterRule"
                    }
                },
                {
                    xtype: "textfield",
                    id: "stylefilterliteralmin",
                    labelWidth: 150,
                    width: "100%",
                    fieldLabel: "Literal min",
                    hidden: true,
                    allowBlank: false,
                    bind: "{grid_style.selection.filter_literal_min}",
                    listeners: {
                        change: "onChangeFilterRule"
                    }
                },
                {
                    xtype: "textfield",
                    id: "stylefilterliteralmax",
                    labelWidth: 150,
                    width: "100%",
                    hidden: true,
                    fieldLabel: "Literal max",
                    bind: "{grid_style.selection.filter_literal_max}",
                    allowBlank: false,
                    listeners: {
                        change: "onChangeFilterRule"
                    }
                }
            ]
        },
    ]
});
