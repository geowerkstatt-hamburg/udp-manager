Ext.define("UDPManager.view.datasets.datasetcollections.styleeditor.LineStyleForm", {
    extend: "Ext.form.Panel",
    xtype: "linestyle-form",
    alias: "styleeditor.LineStyleForm",
    bodyPadding: "10 10 10 10",
    defaultType: "textfield",
    layout: "anchor",
    defaults: {
        anchor: "0",
        labelWidth: 60,
        layout: {
            type: "hbox",
            align: "center"
        }
    },
    items: [
        {
            id: "linestylename",
            labelWidth: 150,
            width: "100%",
            fieldLabel: "Bezeichnung",
            allowBlank: false,
            bind: "{grid_style.selection.rule_name}"
        },
        {
            id: "linestyletitle",
            labelWidth: 150,
            width: "100%",
            fieldLabel: "Titel",
            allowBlank: false,
            bind: "{grid_style.selection.title}"
        },
        {
            xtype: "fieldset",
            id: "linestylestrokecolorfieldset",
            title: "Strichfarbe definieren",
            defaultType: "textfield",
            layout: "anchor",
            defaults: {
                anchor: "0",
                labelWidth: 60,
                layout: {
                    type: "hbox",
                    align: "center"
                }
            },
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "vbox",
                    defaults: {
                        height: 35,
                        flex: 1
                    },
                    items: [
                        {
                            xtype: "checkbox",
                            boxLabel: "Farbwert aus Attribut",
                            labelWidth: 150,
                            checked: false,
                            disabled: true,
                            id: "linestylestrokecolordynamiccheckbox",
                            bind: "{grid_style.selection.stroke_color_dynamic}",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Hex-Farbwerte aus Datenbank, Format: #946edc"
                            },
                            listeners: {
                                change: "onStrokeDynamic"
                            }
                        }, {
                            xtype: "combobox",
                            id: "linestylestrokecolordynamicattribute",
                            fieldLabel: "Attribut",
                            labelWidth: 150,
                            store: [],
                            editable: false,
                            allowBlank: false,
                            sortable: false,
                            hideable: false,
                            bind: "{grid_style.selection.stroke_color_dynamic_attribute}",
                            hidden: true,
                            listeners: {
                                change: "onDrawLine"
                            }
                        }
                    ]
                },
                {
                    xtype: "colorbutton",
                    id: "linestylestrokecolorhex",
                    bind: "{grid_style.selection.stroke_color_hex}",
                    width: "100%",
                    height: 25,
                    margin: "0, 0, 5, 0",
                    listeners: {
                        change: "onDrawLine"
                    }
                }
            ]
        },
        {
            xtype: "numberfield",
            fieldLabel: "Strichbreite",
            id: "linestylestrokewidth",
            labelWidth: 150,
            bind: "{grid_style.selection.stroke_width_line}",
            minValue: 0,
            maxValue: 20,
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawLine"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "Deckkraft",
            labelWidth: 150,
            id: "linestylestrokeopacity",
            width: "100%",
            bind: "{grid_style.selection.stroke_opacity}",
            maxValue: 1,
            minValue: 0,
            allowDecimals: true,
            decimalPrecision: 1,
            step: 0.01,
            listeners: {
                change: "onDrawLine"
            }
        },
        {
            xtype: "combo",
            fieldLabel: "Stil",
            id: "linestylepatternstyle",
            labelWidth: 150,
            store: ["durchgezogen", "gestrichelt"],
            bind: "{grid_style.selection.pattern_style}",
            listeners: {
                change: "onChangeLineType"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "Muster Strichlänge",
            id: "linestyledasharray",
            minValue: 1,
            hidden: true,
            labelWidth: 150,
            bind: "{grid_style.selection.pattern_dash_array}",
            value: "2 2",
            margin: "10 0 10 0",
            listeners: {
                change: "onDrawLine"
            }
        },
        {
            xtype: "numberfield",
            fieldLabel: "MinScaleDenominator",
            id: "linestyleminscaledenominator",
            labelWidth: 150,
            allowDecimals: false,
            allowBlank: true,
            bind: "{grid_style.selection.minscaledenominator}",
            margin: "10 0 10 0",
            maxValue: 10000000,
            minValue: 0
        },
        {
            xtype: "numberfield",
            fieldLabel: "MaxScaleDenominator",
            id: "linestylemaxscaledenominator",
            labelWidth: 150,
            bind: "{grid_style.selection.maxscaledenominator}",
            allowDecimals: false,
            allowBlank: true,
            margin: "10 0 10 0",
            maxValue: 10000000,
            minValue: 0
        }
    ]
});
