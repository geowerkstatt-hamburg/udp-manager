Ext.define("UDPManager.view.datasets.datasetcollections.styleeditor.StyleEditorWindow", {
    extend: "Ext.window.Window",
    id: "styleeditor-window",
    alias: "styleeditor.StyleEditorWindow",
    height: Ext.Element.getViewportHeight(),
    maxWidth: 1400,
    width: Ext.Element.getViewportWidth(),
    title: "Style Editor",
    header: true,
    resizable: false,
    closeAction: "hide",
    controller: "styleeditor",
    layout: "column",
    viewModel: true,
    bbar: ["->",
        {
            xtype: "component",
            id: "styledragdropinfo",
            cls: "x-form-item-label-default",
            html: "Drag & Drop zum Umsortieren"
        },
        {
            xtype: "checkboxfield",
            id: "styles_useforallcollections",
            fieldLabel: "für alle Collections übernehmen",
            labelWidth: 180
        },
        {
            id: "savestyle",
            text: "speichern",
            scale: "medium",
            cls: "btn-save",
            disabled: true,
            handler: "saveStyle"
        }
    ],
    initComponent: function () {
        this.items = [
            {
                xtype: "gridpanel",
                store: "DatasetCollectionStyle",
                reference: "grid_style",
                id: "styleeditor-grid",
                height: Ext.Element.getViewportHeight() - 73,
                columnWidth: 0.35,
                columnLines: true,
                viewConfig: {
                    plugins: {
                        ptype: "gridviewdragdrop",
                        dragText: "Drag & Drop zum Umsortieren"
                    }
                },
                listeners: {select: "onItemSelect"},
                columns: [{
                    width: 30,
                    align: "center",
                    sortable: false,
                    menuDisabled: true,
                    resizeable: false,
                    renderer: "onIconDisplay"
                },
                {
                    text: "Bezeichnung",
                    id: "rule_name_id",
                    dataIndex: "rule_name",
                    align: "left",
                    flex: 1,
                    sortable: false
                },
                {
                    text: "Attributname",
                    id: "rule_geom_name",
                    dataIndex: "geom_name",
                    align: "left",
                    width: 150,
                    sortable: false
                },
                {
                    text: "Typ",
                    id: "rule_geom_type",
                    dataIndex: "geom_type",
                    align: "left",
                    width: 70,
                    sortable: false
                },
                {
                    xtype: "actioncolumn",
                    id: "rule_name_col_id",
                    width: 30,
                    align: "center",
                    sortable: false,
                    hidden: window.read_only,
                    menuDisabled: true,
                    handler: "onRemoveStyleRule",
                    items: [
                        {
                            iconCls: "x-fa fa-ban deleteIcon",
                            tooltip: "Regel löschen"
                        }
                    ]
                }],
                tbar: [
                    {
                        xtype: "combo",
                        id: "add_style_rule_geom",
                        displayField: "name",
                        allowBlank: false,
                        emptyText: "Geometrieattribut wählen",
                        width: 200,
                        listeners: {
                            select: "onSelectGeom"
                        }
                    },
                    {
                        text: "Regel hinzufügen",
                        id: "button_add_rule",
                        disabled: true,
                        handler: "onAddStyleRule"
                    },
                    {
                        text: "Beschriftung hinzufügen",
                        id: "button_add_text_rule",
                        disabled: true,
                        handler: "onAddStyleRule"
                    }
                ]
            },
            {
                xtype: "container",
                columnWidth: 0.45,
                height: Ext.Element.getViewportHeight() - 105,
                autoScroll: true,
                items: [
                    {
                        xtype: "pointstyle-form",
                        id: "style_editor_formular_point",
                        hidden: true
                    },
                    {
                        xtype: "linestyle-form",
                        id: "style_editor_formular_line",
                        hidden: true
                    },
                    {
                        xtype: "polygonstyle-form",
                        id: "style_editor_formular_polygon",
                        hidden: true
                    },
                    {
                        xtype: "textstyle-form",
                        id: "style_editor_formular_text",
                        hidden: true
                    },
                    {
                        xtype: "style-filterform",
                        id: "stylefilterconfig",
                        title: "Filter definieren",
                        hidden: true,
                        collapsed: false,
                        disabled: true
                    }
                ]
            },
            {
                xtype: "panel",
                id: "style-canvas-container",
                bodyPadding: "20 10 10 10",
                columnWidth: 0.2,
                autoScroll: true,
                items: [
                    {
                        xtype: "panel",
                        id: "point-style-canvas-panel",
                        padding: "24 24 24 24",
                        style: {
                            borderColor: "#C0C0C0", borderStyle: "solid", borderWidth: "1px"
                        },
                        height: 150,
                        width: 150,
                        hidden: true,
                        html: "<canvas id='point-style-canvas' style='z-index: 1; position: absolute; top: 0px; left: 0px;' width='100' height='100'></canvas>"
                    },
                    {
                        xtype: "panel",
                        id: "polygon-style-canvas-panel",
                        padding: "24 24 24 24",
                        style: {
                            borderColor: "#C0C0C0", borderStyle: "solid", borderWidth: "1px"
                        },
                        height: 150,
                        width: 150,
                        hidden: true,
                        html: "<canvas id='polygon-style-surround-canvas' style='z-index: 2; position: absolute; top: 0px; left: 0px;' width='100' height='100'></canvas><canvas id='polygon-style-canvas' style='z-index: 1; position: absolute; top: 0px; left: 0px;' width='100' height='100'></canvas>"
                    },
                    {
                        xtype: "panel",
                        id: "line-style-canvas-panel",
                        padding: "24 24 24 24",
                        style: {
                            borderColor: "#C0C0C0", borderStyle: "solid", borderWidth: "1px"
                        },
                        height: 150,
                        width: 150,
                        hidden: true,
                        html: "<canvas id='line-style-canvas' style='z-index: 1; position: absolute; top: 0px; left: 0px;' width='100' height='100'></canvas>"
                    },
                    {
                        xtype: "panel",
                        id: "text-style-canvas-panel",
                        padding: "24 24 24 24",
                        style: {
                            borderColor: "#C0C0C0", borderStyle: "solid", borderWidth: "1px"
                        },
                        height: 150,
                        width: 150,
                        hidden: true,
                        html: "<canvas id='text-style-canvas' style='z-index: 1; position: absolute; top: 0px; left: 0px;' width='100' height='100'></canvas>"
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }

});
