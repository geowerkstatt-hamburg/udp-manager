Ext.define("UDPManager.view.datasets.datasetcollections.tableschema.TableSchemaEditor", {
    extend: "Ext.window.Window",
    id: "tableschemaeditor-window",
    alias: "tableschema.TableSchemaEditor",
    width: 500,
    height: 500,
    title: "Datenschema konfigurieren",
    resizable: false,
    scrollable: true,
    controller: "dataset_collections",
    initComponent: function () {
        this.items = [
            {
                xtype: "grid",
                store: "TableSchema",
                id: "tableschemaeditor-grid",
                height: 460,
                scrollable: true,
                columnLines: true,
                plugins: {
                    cellediting: {
                        clicksToEdit: 1
                    }
                },
                selModel: {
                    type: "cellmodel"
                },
                viewConfig: {
                    enableTextSelection: true
                },
                columns: [
                    {
                        header: "Attributname (DB)",
                        dataIndex: "attr_name_db",
                        align: "left",
                        flex: 1,
                        sortable: false
                    },
                    {
                        header: "Datentyp",
                        dataIndex: "attr_datatype",
                        width: 160,
                        sortable: false,
                        align: "left",
                        editor: {
                            allowBlank: false,
                            xtype: "combobox",
                            store: ["text", "number", "numeric(10,2)", "integer", "double precision", "date", "timestamp without time zone", "timestamp with time zone", "boolean", "geometry [point]", "geometry [line]", "geometry [polygon]"]
                        }
                    },
                    {
                        xtype: "checkcolumn",
                        dataIndex: "pk",
                        text: "PK",
                        align: "center",
                        headerCheckbox: false,
                        sortable: false,
                        width: 85,
                        disabled: true
                    }
                ],
                bbar: [
                    "->",
                    {
                        text: "Tabelle anlegen",
                        id: "createdbtable",
                        tooltip: "Erzeugt die Tabelle nach vorgegebener Konfiguration",
                        handler: "onCreateDbTable"
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }
});
