Ext.define("UDPManager.view.datasets.datasetcollections.CollectionsImportChoice", {
    extend: "Ext.window.Window",
    id: "collectionimportchoice-window",
    alias: "datasetcollections.collectionimportchoice",
    height: 125,
    width: 400,
    title: "Schnittstelle auswählen",
    resizable: false,
    closeAction: "hide",
    controller: "dataset_collections",
    listeners: {
        hide: "onClose",
        destroy: "onClose"
    },
    items: [
        {
            xtype: "panel",
            bodyPadding: 10,
            items: [
                {
                    xtype: "combo",
                    fieldLabel: "Schnittstelle wählen",
                    allowBlank: false,
                    id: "collectionsimportservicetype",
                    store: "ServicesDataset",
                    queryMode: "local",
                    displayField: "title",
                    labelWidth: 120,
                    width: 380
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    "->",
                    {
                        id: "retrievecollections",
                        text: "Collections Abrufen",
                        listeners: {
                            click: "onImportCollection"
                        }
                    }
                ]
            }]
        }
    ]
});
