Ext.define("UDPManager.view.datasets.datasetcollections.styleeditor.StyleSldEditorWindow", {
    extend: "Ext.window.Window",
    alias: "styleeditor.StyleSldEditorWindow",
    id: "stylesldeditor-window",
    height: Ext.Element.getViewportHeight() - 120,
    width: Ext.Element.getViewportWidth() - 120,
    resizable: false,
    title: "Style bearbeiten",
    header: true,
    controller: "dataset_collections",

    initComponent: function () {
        this.items = [
            {
                xtype: "container",
                layout: "border",
                bodyBorder: false,
                height: Ext.Element.getViewportHeight() - 200,
                defaults: {
                    split: true,
                    height: Ext.Element.getViewportHeight() - 200,
                    autoScroll: true,
                    collapsible: false
                },
                items: [
                    {
                        xtype: "gridpanel",
                        id: "grid_stylesldicons",
                        title: "Icons",
                        store: "StyleSldIcons",
                        columns: [
                            {
                                text: "Dateiname",
                                dataIndex: "file_name",
                                flex: 1
                            },
                            {
                                xtype: "actioncolumn",
                                width: 26,
                                sortable: false,
                                menuDisabled: true,
                                align: "center",
                                handler: "onDeleteSldIcon",
                                hidden: window.read_only,
                                iconCls: "x-fa fa-trash deleteIcon"
                            }
                        ],
                        rowLines: false,
                        viewConfig: {
                            //stripeRows: true,
                            enableTextSelection: true,
                        },
                        width: "15%",
                        border: true,
                        region: "west",
                        buttonAlign: "left",
                        buttons: [
                            {
                                xtype: "filefield",
                                buttonOnly: true,
                                id: "styleslduploadicon",
                                disabled: window.read_only,
                                width: 80,
                                buttonConfig: {
                                    width: 80,
                                    iconCls: "x-btn x-fa fa-upload",
                                    tooltip: "Uploadformate: png, jpeg, gif, svg",
                                    text: "Upload",
                                    textAlign: "center"
                                },
                                listeners: {
                                    change: "onUploadStyleSldIcon"
                                }
                            }
                        ],
                        plugins: {
                            rowexpander: {
                                rowBodyTpl: new Ext.XTemplate(
                                    '<tpl if="isImage">',
                                        '<img height="50" width="50" src="{data_url}"</img>',
                                    '</tpl>'
                                )
                            }
                        }
                    },
                    {
                        xtype: "panel",
                        id: "stylesldview-panel",
                        width: "85%",
                        region: "center",
                        html: "<div id=\"sldcontainer\" style=\"width: 100%; height: 100%; border: 1px solid grey\"></div>"
                    }
                ]
            },
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                width: Ext.Element.getViewportWidth() - 122,
                items: [
                    {
                        xtype: "component",
                        flex: 1,
                        html: ""
                    },
                    {
                        xtype: "button",
                        id: "savestylesld",
                        scale: "medium",
                        cls: "btn-save",
                        text: "Speichern",
                        tooltip: "Style Konfiguration wird in der Datenbank gespeichert",
                        disabled: window.read_only,
                        width: 140,
                        margin: "5 10 5 5",
                        listeners: {
                            click: "onSaveStyleSld"
                        }
                    }
                ]
            }
        ];
        this.callParent(arguments);
    }
});
