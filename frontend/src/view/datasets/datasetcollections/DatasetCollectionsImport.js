Ext.define("UDPManager.view.datasets.datasetcollections.DatasetCollectionsImport", {
    extend: "Ext.window.Window",
    xtype: "collectionimport-window",
    id: "collectionimport-window",
    alias: "datasetcollections.DatasetCollectionsImport",
    controller: "dataset_collections",
    title: "Collection Auswahl",
    height: 500,
    width: 500,
    scrollable: true,

    layout: {
        type: "vbox",
        align: "stretch"
    },

    buttons: [{
        text: "Übernehmen",
        handler: "onCollectionSelectionDone"
    }],

    items: [
        // auskommentiert 04.07.22 - Funktionalität für Gruppenlayer noch nicht vorhanden
        // {
        //     xtype: "grid",
        //     id: "select-grouplayer_grid",
        //     columns: [{
        //         text: "Gruppen-Collection-Name",
        //         dataIndex: "name",
        //         flex: 0.5,
        //         align: "left"
        //     }, {
        //         text: "Gruppen-Collection-Titel",
        //         dataIndex: "title",
        //         flex: 0.5,
        //         align: "left"
        //     }],
        //     selModel: {
        //         selType: "checkboxmodel"
        //     }
        // },
        {
            xtype: "grid",
            id: "select-layer-grid",
            columns: [{
                text: "Collection-Name",
                dataIndex: "name",
                flex: 0.5,
                align: "left"
            }, {
                text: "Collection-Titel",
                dataIndex: "title",
                flex: 0.5,
                align: "left"
            }],
            selModel: {
                selType: "checkboxmodel"
            }
        }
    ]
});
