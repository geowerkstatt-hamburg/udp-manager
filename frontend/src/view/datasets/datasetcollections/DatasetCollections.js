Ext.define("UDPManager.view.datasets.datasetcollections.DatasetCollections", {
    extend: "Ext.panel.Panel",
    xtype: "dataset_collections",
    alias: "widget.dataset_collections",
    controller: "dataset_collections",
    title: "Collections",
    layout: "vbox",
    listeners: {
        select: "onItemSelected"
    },
    viewModel: {
        data: {
            form_collections: null
        },
        stores: {
            gridstore_collections: {
                type: "collectionsdataset"
            },
            gridstore_layers: {
                type: "layerscollection"
            }
        }
    },
    initComponent: function () {
        this.items = [
            {
                xtype: "panel",
                layout: "column",
                width: "100%",
                height: "100%",
                items: [
                    {
                        xtype: "gridpanel",
                        id: "grid_datasetcollections",
                        reference: "grid_datasetcollections",
                        style: "border-right: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                        columnWidth: 0.35,
                        columns: [
                            {
                                dataIndex: "group_object",
                                width: 30,
                                align: "center",
                                sortable: false,
                                menuDisabled: true,
                                resizeable: false,
                                renderer: "onIconChange"
                            },
                            {
                                text: "ID",
                                dataIndex: "id",
                                width: 70
                            },
                            {
                                text: "Titel",
                                dataIndex: "title",
                                flex: 1
                            },
                            {
                                xtype: "actioncolumn",
                                width: 40,
                                menuDisabled: true,
                                sortable: false,
                                resizeable: false,
                                align: "left",
                                renderer: function (value, metadata, record) {
                                    if (record.get("collection_changed") === true) {
                                        this.items[0].iconCls = "x-fa fa-wrench";
                                        this.items[0].tooltip = "Collection bearbeitet<br>Für mehr Informationen siehe Änderungslog";
                                    }
                                    else {
                                        this.items[0].iconCls = "";
                                        this.items[0].tooltip = "";
                                    }
                                },
                                items: [
                                    {
                                        iconCls: "x-fa fa-wrench",
                                        tooltip: "Änderungen sichtbar im Änderungslog",
                                        handler: "onClickChangelog"
                                    }
                                ]
                            }
                        ],
                        rowLines: false,
                        viewConfig: {
                            //stripeRows: true,
                            enableTextSelection: true,
                            getRowClass: function () {
                                return "multiline-row";
                            }
                        },
                        height: Ext.Element.getViewportHeight() - 106,
                        autoScroll: true,
                        listeners: {
                            select: "onGridCollectionItemSelected"
                        },
                        buttonAlign: "left",
                        header: false,
                        bind: {
                            store: "{gridstore_collections}"
                        },
                        dockedItems: [{
                            xtype: "toolbar",
                            dock: "bottom",
                            items: [
                                {
                                    id: "addnewcollection",
                                    iconCls: "x-fa fa-plus",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    disabled: true,
                                    tooltip: "Neue Collection anlegen (Blanko)",
                                    listeners: {
                                        click: "onAddNewCollection"
                                    }
                                },
                                {
                                    id: "importcollection",
                                    iconCls: "x-fa fa-file-import",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    disabled: true,
                                    tooltip: "Neue Collection aus Schnittstelle importieren",
                                    listeners: {
                                        click: "onImportCollection"
                                    }
                                },
                                {
                                    id: "importcollectionexternal",
                                    iconCls: "x-fa fa-file-import",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    disabled: true,
                                    tooltip: "Neue Collection aus externer Schnittstelle importieren",
                                    listeners: {
                                        click: "onServiceChoice"
                                    }
                                },
                                {
                                    xtype: "button",
                                    id: "duplicatecollection",
                                    disabled: true,
                                    iconCls: "x-fa fa-copy",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    tooltip: "Ausgewählte Collection duplizieren",
                                    listeners: {
                                        click: "onDuplicateCollection"
                                    }
                                },
                                {
                                    xtype: "button",
                                    iconCls: "x-fa fa-trash",
                                    disabled: true,
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    id: "deletecollection",
                                    tooltip: "Ausgewählte Collection löschen",
                                    listeners: {
                                        click: "onDeleteCollection"
                                    }
                                },
                                {
                                    id: "sharecollection",
                                    iconCls: "x-fa fa-share-alt",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    disabled: true,
                                    tooltip: "Link auf diese Collection teilen",
                                    listeners: {
                                        click: "onShareCollection"
                                    }
                                }
                            ]
                        }]
                    },
                    {
                        xtype: "tabpanel",
                        id: "collections_main_tabs",
                        activeTab: 0,
                        listeners: {
                            tabchange: "onTabChange"
                        },
                        tabPosition: "left",
                        tabRotation: 0,
                        columnWidth: 0.65,
                        items: [
                            {
                                xtype: "container",
                                style: "white-space: pre-line;border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                title: "Collection-&#013;Details",
                                items: [
                                    {
                                        xtype: "form",
                                        id: "datasetcollectiondetailsform",
                                        disabled: true,
                                        defaultType: "textfield",
                                        scrollable: true,
                                        autoScroll: true,
                                        bodyPadding: 20,
                                        height: Ext.Element.getViewportHeight() - 109,
                                        items: [
                                            {
                                                xtype: "component",
                                                margin: "0 0 10 165",
                                                id: "collectioneditnotes",
                                                html: "",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Nur die Layer betreffende Attribute können auch ohne Bearbeitungsmodus aktualisiert werden"
                                                }
                                            },
                                            {
                                                fieldLabel: "Collection-ID",
                                                id: "datasetcollectionid",
                                                name: "id",
                                                xtype: "numberfield",
                                                labelWidth: 160,
                                                width: 360,
                                                readOnly: true,
                                                hidden: true,
                                                bind: "{grid_datasetcollections.selection.id}"
                                            },
                                            {
                                                fieldLabel: "Gruppenobjekt",
                                                id: "datasetcollectiongroupobject",
                                                name: "group_object",
                                                xtype: "checkboxfield",
                                                labelWidth: 160,
                                                bind: "{grid_datasetcollections.selection.group_object}",
                                                listeners: {
                                                    change: "onGroupCheck"
                                                },
                                                editModeOnly: true
                                            },
                                            {
                                                fieldLabel: "Name (technisch)",
                                                id: "datasetcollectionname",
                                                name: "name",
                                                labelWidth: 160,
                                                width: "100%",
                                                requiredField: true,
                                                listeners: {
                                                    change: "onCollectionNameChange"
                                                },
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Unterliegt Namenskonventionen, der Layername, und ggf. der Tabellenname werden hiervon abgeleitet"
                                                },
                                                bind: "{grid_datasetcollections.selection.name}",
                                                editModeOnly: true
                                            },
                                            {
                                                fieldLabel: "Name (alternativ/extern)",
                                                id: "datasetcollectionnamealternative",
                                                name: "alternative_name",
                                                labelWidth: 160,
                                                width: "100%",
                                                requiredField: false,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Für externe Datensätze oder speziellen Anforderungen (z.B. INSPIRE) kann die Namenskonventionen des technischen Namens hiermit übersteuert werden"
                                                },
                                                bind: "{grid_datasetcollections.selection.alternative_name}",
                                                editModeOnly: true
                                            },
                                            {
                                                fieldLabel: "Titel",
                                                id: "datasetcollectiontitle",
                                                name: "title",
                                                labelWidth: 160,
                                                width: "100%",
                                                requiredField: true,
                                                bind: "{grid_datasetcollections.selection.title}",
                                                editModeOnly: true
                                            },
                                            {
                                                xtype: "combobox",
                                                store: Ext.create("Ext.data.Store", {
                                                    fields: ["value", "display"],
                                                    data: [
                                                        {"value": "visual_download", "display": "Visualisierung & Download"},
                                                        {"value": "only_visual", "display": "Nur Visualisierung"},
                                                        {"value": "only_download", "display": "Nur Download"}
                                                    ]
                                                }),
                                                displayField: "display",
                                                valueField: "value",
                                                fieldLabel: "API-Zugriff",
                                                id: "datasetcollectionapiconstraint",
                                                name: "api_constraint",
                                                labelWidth: 160,
                                                width: "100%",
                                                requiredField: true,
                                                editable: false,
                                                bind: "{grid_datasetcollections.selection.api_constraint}",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Wird gesetzt, um zu steuern, über welche Schnittstellen die Collection bereitgestellt werden soll. </br> Nur Visualisierung: WMS </br> Nur Download: WFS, OAF"
                                                },
                                                editModeOnly: true
                                            },
                                            {
                                                xtype: "fieldset",
                                                title: "Beschreibung",
                                                padding: "0 5 0 5",
                                                cls: "prio-fieldset",
                                                collapsible: true,
                                                collapsed: true,
                                                items: [
                                                    {
                                                        id: "collectiondescription",
                                                        name: "description",
                                                        xtype: "textarea",
                                                        height: 120,
                                                        labelWidth: 0,
                                                        width: "100%",
                                                        bind: "{grid_datasetcollections.selection.description}"
                                                    }
                                                ],
                                                editModeOnly: true
                                            },
                                            {
                                                xtype: "container",
                                                layout: "hbox",
                                                id: "datasetcollectionlegendcontainer",
                                                margin: "5 0 5 0",
                                                items: [
                                                    {
                                                        xtype: "textfield",
                                                        id: "datasetcollectionlegenddataurl",
                                                        name: "legend_data_url",
                                                        hidden: true,
                                                        bind: "{grid_datasetcollections.selection.legend_data_url}"
                                                    },
                                                    {
                                                        xtype: "textfield",
                                                        id: "datasetcollectionlegendcontenttype",
                                                        name: "legend_content_type",
                                                        hidden: true,
                                                        bind: "{grid_datasetcollections.selection.legend_content_type}"
                                                    },
                                                    {
                                                        xtype: "textfield",
                                                        fieldLabel: "Legende (Datei)",
                                                        id: "datasetcollectionlegendfilename",
                                                        name: "legend_file_name",
                                                        labelWidth: 160,
                                                        flex: 1,
                                                        readOnly: true,
                                                        bind: "{grid_datasetcollections.selection.legend_file_name}",
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        xtype: "filefield",
                                                        buttonOnly: true,
                                                        id: "datasetcollectionuploadlegend",
                                                        disabled: window.read_only,
                                                        margin: "0 5 5 10",
                                                        width: 30,
                                                        buttonConfig: {
                                                            width: 30,
                                                            iconCls: "x-btn x-fa fa-upload",
                                                            tooltip: "Uploadformate: png, jpeg, gif",
                                                            text: "",
                                                            textAlign: "center"
                                                        },
                                                        listeners: {
                                                            change: "onUploadLegend"
                                                        }
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "datasetcollectiondownloadlegend",
                                                        margin: "0 10 5 5",
                                                        iconCls: "x-fa fa-download",
                                                        width: 30,
                                                        hidden: true,
                                                        listeners: {
                                                            click: "onDownloadLegend"
                                                        }
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "datasetcollectiondeletelegend",
                                                        margin: "0 5 5 0",
                                                        iconCls: "x-fa fa-ban",
                                                        width: 30,
                                                        hidden: true,
                                                        listeners: {
                                                            click: "onDeleteLegend"
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                fieldLabel: "Legenden URL (Intranet)",
                                                id: "datasetcollectionlegendurlintranet",
                                                name: "legend_url_intranet",
                                                labelWidth: 160,
                                                width: "100%",
                                                readOnly: false,
                                                allowBlank: true,
                                                bind: "{grid_datasetcollections.selection.legend_url_intranet}"
                                            },
                                            {
                                                fieldLabel: "Legenden URL (Internet)",
                                                id: "datasetcollectionlegendurlinternet",
                                                name: "legend_url_internet",
                                                labelWidth: 160,
                                                width: "100%",
                                                allowBlank: true,
                                                bind: "{grid_datasetcollections.selection.legend_url_internet}"
                                            },
                                            {
                                                fieldLabel: "Transparenz (%)",
                                                id: "datasetcollectiontransparency",
                                                name: "transparency",
                                                xtype: "numberfield",
                                                value: 0,
                                                maxValue: 100,
                                                minValue: 0,
                                                step: 25,
                                                labelWidth: 160,
                                                width: 360,
                                                allowBlank: true,
                                                defaultValue: true,
                                                bind: "{grid_datasetcollections.selection.transparency}"
                                            },
                                            {
                                                fieldLabel: "Min-Scale",
                                                id: "datasetcollectionscalemin",
                                                name: "scale_min",
                                                labelWidth: 160,
                                                width: "100%",
                                                allowBlank: true,
                                                defaultValue: true,
                                                bind: "{grid_datasetcollections.selection.scale_min}"
                                            },
                                            {
                                                fieldLabel: "Max-Scale",
                                                id: "datasetcollectionscalemax",
                                                name: "scale_max",
                                                labelWidth: 160,
                                                width: "100%",
                                                allowBlank: true,
                                                defaultValue: true,
                                                bind: "{grid_datasetcollections.selection.scale_max}"
                                            },
                                            {
                                                xtype: "container",
                                                layout: "hbox",
                                                id: "datasetcollectioncustombboxcontainer",
                                                margin: "5 0 5 0",
                                                items: [
                                                    {
                                                        xtype: "textfield",
                                                        fieldLabel: "BBox (benutzerdefiniert)",
                                                        id: "datasetcollectioncumstombbox",
                                                        name: "custom_bbox",
                                                        labelWidth: 160,
                                                        flex: 1,
                                                        allowBlank: true,
                                                        emptyText: "xmin, ymin, xmax, ymax",
                                                        submitEmptyText: false,
                                                        bind: "{grid_datasetcollections.selection.custom_bbox}",
                                                        autoEl: {
                                                            tag: "div",
                                                            "data-qtip": "Wenn gesetzt, wird die Bounding Box als Envelope in der deegree Konfiguration des Layers verwendet."
                                                        },
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "datasetcollectiongeneratecustombbox",
                                                        margin: "0 5 5 10",
                                                        width: 30,
                                                        autoEl: {
                                                            tag: "div",
                                                            "data-qtip": "Aus den Daten berechnen"
                                                        },
                                                        iconCls: "x-fa fa-box",
                                                        disabled: true,
                                                        listeners: {
                                                            click: "onGenerateBbox"
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                fieldLabel: "Namespace",
                                                id: "datasetcollectionnamespace",
                                                name: "namespace",
                                                labelWidth: 160,
                                                width: "100%",
                                                defaultValue: true,
                                                bind: "{grid_datasetcollections.selection.namespace}",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Falls ein anderer Namespace benötigt wird, bitte analog zum Stadardwert angeben: Präfix=URL"
                                                },
                                                editModeOnly: true
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                fieldLabel: "Komplexes Datenmodell",
                                                id: "datasetcollectioncomplexscheme",
                                                name: "complex_scheme",
                                                labelWidth: 160,
                                                allowBlank: true,
                                                bind: "{grid_datasetcollections.selection.complex_scheme}",
                                                editModeOnly: true
                                            },
                                            {
                                                xtype: "combo",
                                                store: ["Raster", "Vektor"],
                                                fieldLabel: "Datentyp",
                                                id: "datasetcollectionstoretype",
                                                name: "store_type",
                                                labelWidth: 160,
                                                width: "100%",
                                                requiredField: true,
                                                editable: false,
                                                bind: "{grid_datasetcollections.selection.store_type}",
                                                listeners: {
                                                    change: "onStoreTypeChange"
                                                },
                                                editModeOnly: true
                                            },
                                            {
                                                fieldLabel: "Tileindex",
                                                id: "datasetcollectiontileindex",
                                                name: "tileindex",
                                                labelWidth: 160,
                                                width: "100%",
                                                bind: "{grid_datasetcollections.selection.tileindex}",
                                                editModeOnly: true
                                            },
                                            {
                                                xtype: "container",
                                                layout: "hbox",
                                                id: "datasetcollectiondbschemacontainer",
                                                margin: "5 0 5 0",
                                                items: [
                                                    {
                                                        xtype: "combo",
                                                        fieldLabel: "Schema",
                                                        store: [],
                                                        id: "datasetcollectiondbschema",
                                                        queryMode: "local",
                                                        name: "db_schema",
                                                        labelWidth: 160,
                                                        flex: 1,
                                                        bind: "{grid_datasetcollections.selection.db_schema}",
                                                        listeners: {
                                                            select: "onDbSchemaSelect",
                                                            change: "onDbSchemaChange"
                                                        },
                                                        defaultValue: true,
                                                        autoEl: {
                                                            tag: "div",
                                                            "data-qtip": "Default ist der Datensatz Kurzname. Bitte nur anpassen, wenn der Schemaname in Ziel-DB bereits vergeben ist."
                                                        },
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "datasetcollectionadddbschema",
                                                        disabled: true,
                                                        margin: "0 5 0 10",
                                                        width: 30,
                                                        autoEl: {
                                                            tag: "div",
                                                            "data-qtip": "Datenbankschema neu anlegen"
                                                        },
                                                        iconCls: "x-fa fa-plus-circle",
                                                        listeners: {
                                                            click: "onAddDbSchema"
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "container",
                                                layout: "hbox",
                                                id: "datasetcollectiondbtablecontainer",
                                                margin: "5 0 5 0",
                                                items: [
                                                    {
                                                        xtype: "combo",
                                                        fieldLabel: "Tabelle",
                                                        store: [],
                                                        id: "datasetcollectiondbtable",
                                                        queryMode: "local",
                                                        name: "db_table",
                                                        labelWidth: 160,
                                                        flex: 1,
                                                        bind: "{grid_datasetcollections.selection.db_table}",
                                                        listeners: {
                                                            change: "onDbTableChange"
                                                        },
                                                        defaultValue: true,
                                                        emptyText: "Default ist der Collection Name (bitte erst eintragen)",
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "datasetcollectionadddbtable",
                                                        disabled: true,
                                                        margin: "0 5 0 10",
                                                        width: 30,
                                                        autoEl: {
                                                            tag: "div",
                                                            "data-qtip": "Datenbanktabelle neu anlegen"
                                                        },
                                                        iconCls: "x-fa fa-plus-circle",
                                                        listeners: {
                                                            click: "onAddDbTable"
                                                        }
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "numberfield",
                                                id: "datasetcollectionnumberfeatures",
                                                fieldLabel: "Anzahl Features",
                                                minValue: 0,
                                                labelWidth: 160,
                                                width: 360,
                                                name: "number_features",
                                                bind: "{grid_datasetcollections.selection.number_features}"
                                            },
                                            {
                                                xtype: "fieldset",
                                                title: "Quelldaten",
                                                padding: "0 5 0 5",
                                                cls: "prio-fieldset-only-edit-mode",
                                                collapsible: true,
                                                collapsed: true,
                                                defaultType: "textfield",
                                                items: [
                                                    {
                                                        xtype: "combo",
                                                        fieldLabel: "Quelldatentyp",
                                                        id: "datasetcollectionsourcedatatype",
                                                        name: "source_data_type",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        store: ["excel", "csv", "sde", "mssql", "postgresql"],
                                                        editable: true,
                                                        bind: "{grid_datasetcollections.selection.source_data_type}",
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        xtype: "combo",
                                                        fieldLabel: "DB-Verbindung",
                                                        id: "datasetcollectionsourcedbconnectionid",
                                                        store: "ConfigSourceDbConnections",
                                                        queryMode: "local",
                                                        displayField: "name",
                                                        valueField: "id",
                                                        name: "source_db_connection_id",
                                                        labelWidth: 160,
                                                        editable: false,
                                                        width: "100%",
                                                        bind: "{grid_datasetcollections.selection.source_db_connection_id}",
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        fieldLabel: "Ablageort",
                                                        id: "datasetcollectionsourcedatalocation",
                                                        name: "source_data_location",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        bind: "{grid_datasetcollections.selection.source_data_location}",
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        fieldLabel: "Dateiname",
                                                        id: "datasetcollectionsourcedataname",
                                                        name: "source_data_name",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        bind: "{grid_datasetcollections.selection.source_data_name}",
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        fieldLabel: "Schema",
                                                        id: "datasetcollectionsourcedataschema",
                                                        name: "source_data_schema",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        bind: "{grid_datasetcollections.selection.source_data_schema}",
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        fieldLabel: "Tabelle",
                                                        id: "datasetcollectionsourcedatatable",
                                                        name: "source_data_table",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        bind: "{grid_datasetcollections.selection.source_data_table}",
                                                        editModeOnly: true
                                                    },
                                                    {
                                                        fieldLabel: "Filter",
                                                        id: "datasetcollectionsourcedataquery",
                                                        name: "source_data_query",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        bind: "{grid_datasetcollections.selection.source_data_query}",
                                                        editModeOnly: true
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldset",
                                                title: "zusätzliche Informationen",
                                                padding: "0 5 0 5",
                                                cls: "prio-fieldset",
                                                collapsible: true,
                                                collapsed: true,
                                                items: [{
                                                    xtype: "textareafield",
                                                    grow: false,
                                                    allowBlank: true,
                                                    id: "datasetcollectionadditionalinfo",
                                                    name: "additional_info",
                                                    height: 90,
                                                    width: "100%",
                                                    bind: "{grid_datasetcollections.selection.additional_info}"
                                                }]
                                            },
                                            {
                                                xtype: "fieldset",
                                                title: "Spezialeinstellungen",
                                                defaultType: "textfield",
                                                defaults: {
                                                    anchor: "100%"
                                                },
                                                width: "100%",
                                                collapsible: true,
                                                collapsed: true,
                                                items: [
                                                    {
                                                        fieldLabel: "Alternativer Titel",
                                                        id: "datasetcollectiontitlealt",
                                                        name: "title_alt",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        allowBlank: true,
                                                        tooltip: "Mit diesem Attribut lässt sich der Titel des Layers im Themenbaum des Masterportals überschreiben.",
                                                        bind: "{grid_datasetcollections.selection.title_alt}"
                                                    },
                                                    {
                                                        fieldLabel: "Attribution",
                                                        id: "datasetcollectionattribution",
                                                        name: "attribution",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        readOnly: false,
                                                        allowBlank: true,
                                                        bind: "{grid_datasetcollections.selection.attribution}"
                                                    },
                                                    {
                                                        fieldLabel: "Zusätzliche Kategorien",
                                                        id: "datasetcollectionadditionalcatagories",
                                                        name: "additional_categories",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        allowBlank: true,
                                                        bind: "{grid_datasetcollections.selection.additional_categories}"
                                                    },
                                                    {
                                                        fieldLabel: "Service URL sichtbar",
                                                        id: "datasetcollectionserviceurlvisible",
                                                        name: "service_url_visible",
                                                        xtype: "checkboxfield",
                                                        labelWidth: 160,
                                                        defaultValue: true,
                                                        bind: "{grid_datasetcollections.selection.service_url_visible}"
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldset",
                                                title: "GFI Konfiguration",
                                                defaultType: "textfield",
                                                collapsible: true,
                                                collapsed: true,
                                                defaults: {
                                                    anchor: "100%"
                                                },
                                                width: "100%",
                                                items: [
                                                    {
                                                        fieldLabel: "GFI Theme",
                                                        id: "datasetcollectiongfitheme",
                                                        name: "gfi_theme",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        allowBlank: true,
                                                        defaultValue: true,
                                                        bind: "{grid_datasetcollections.selection.gfi_theme}"
                                                    },
                                                    {
                                                        xtype: "fieldset",
                                                        title: "Theme Parameter (JSON)",
                                                        padding: "0 5 0 5",
                                                        cls: "prio-fieldset",
                                                        collapsible: true,
                                                        collapsed: true,
                                                        items: [{
                                                            xtype: "textareafield",
                                                            grow: false,
                                                            id: "datasetcollectiongfithemeparams",
                                                            name: "gfi_theme_params",
                                                            height: 80,
                                                            width: "100%",
                                                            bind: "{grid_datasetcollections.selection.gfi_theme_params}"
                                                        }]
                                                    },
                                                    {
                                                        fieldLabel: "GFI als neues Fenster",
                                                        id: "datasetcollectiongfiasnewwindow",
                                                        name: "gfi_as_new_window",
                                                        xtype: "checkboxfield",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        allowBlank: true,
                                                        listeners: {
                                                            change: "onGfiAsNewWindow"
                                                        },
                                                        bind: "{grid_datasetcollections.selection.gfi_as_new_window}"
                                                    },
                                                    {
                                                        fieldLabel: "GFI Fenster Parameter",
                                                        id: "datasetcollectiongfiwindowspecs",
                                                        name: "gfi_window_specs",
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        hidden: true,
                                                        bind: "{grid_datasetcollections.selection.gfi_window_specs}"
                                                    },
                                                    {
                                                        xtype: "checkboxfield",
                                                        id: "datasetcollectiongfidisable",
                                                        name: "gfi_disabled",
                                                        fieldLabel: "GFI deaktivieren",
                                                        autoEl: {
                                                            tag: "div",
                                                            "data-qtip": "Wenn angehakt, wird die GFI Konfiguration auf 'ignore' gesetzt"
                                                        },
                                                        labelWidth: 160,
                                                        width: "100%",
                                                        bind: "{grid_datasetcollections.selection.gfi_disabled}"
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "container",
                                                layout: "hbox",
                                                margin: "5 0 5 0",
                                                items: [
                                                    {
                                                        xtype: "button",
                                                        id: "editattributeconfig",
                                                        text: "Attribute konfigurieren",
                                                        margin: "0 5 0 0",
                                                        width: 160,
                                                        disabled: true,
                                                        listeners: {
                                                            click: "onEditAttributeConfig"
                                                        }
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "editstyle",
                                                        text: "Style konfigurieren",
                                                        margin: "0 5 0 5",
                                                        width: 160,
                                                        disabled: true,
                                                        listeners: {
                                                            click: "onOpenStyleEditor"
                                                        }
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "editstylesldfile",
                                                        text: "Style manuell bearbeiten",
                                                        margin: "0 5 0 5",
                                                        width: 160,
                                                        disabled: true,
                                                        listeners: {
                                                            click: "onOpenSldFileEditor"
                                                        }
                                                    },
                                                    {
                                                        xtype: "checkbox",
                                                        boxLabel: "Manuelle Stylekonfiguration verwenden",
                                                        margin: "0 5 0 5",
                                                        labelWidth: 160,
                                                        id: "datasetcollectionusestylesld",
                                                        name: "use_style_sld",
                                                        bind: "{grid_datasetcollections.selection.use_style_sld}",
                                                        editModeOnly: true
                                                    }
                                                ]
                                            }
                                        ],
                                        dockedItems: [{
                                            xtype: "toolbar",
                                            dock: "bottom",
                                            border: true,
                                            style: "border-top: 1px solid #cfcfcf !important;",
                                            items: [
                                                {
                                                    xtype: "component",
                                                    html: "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Pflichtfeld</span>"
                                                },
                                                {
                                                    xtype: "component",
                                                    html: "<span style='color:grey;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Standardwert</span>"
                                                },
                                                {
                                                    xtype: "component",
                                                    flex: 1,
                                                    html: ""
                                                },
                                                {
                                                    id: "savecollectiondetails",
                                                    text: "Speichern",
                                                    scale: "medium",
                                                    cls: "btn-save",
                                                    disabled: true,
                                                    listeners: {
                                                        click: "onSaveCollectionDetails"
                                                    }
                                                }
                                            ]
                                        }]
                                    }
                                ]
                            },
                            {
                                xtype: "panel",
                                style: "border-left: 1px solid #cfcfcf !important;border-right: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                title: "Änderungslog",
                                id: "datasetcollectionchangestab",
                                height: Ext.Element.getViewportHeight() - 106,
                                bodyPadding: 20,
                                items: [
                                    {
                                        xtype: "component",
                                        id: "datasetcollectionchangeslabel",
                                        width: "100%",
                                        html: ""
                                    },
                                    {
                                        xtype: "component",
                                        id: "datasetcollectionchanges",
                                        width: "100%",
                                        height: "100%",
                                        html: "",
                                        scrollable: true
                                    }
                                ]
                            },
                            {
                                xtype: "dataset_collections_layerconf",
                                title: "Layer",
                                bodyPadding: 0,
                                scrollable: true
                            }
                        ]
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }
});
