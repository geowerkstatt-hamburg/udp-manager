Ext.define("UDPManager.view.datasets.datasetcollections.layerconf.NewLayerWindow", {
    extend: "Ext.window.Window",
    id: "newlayer-window",
    alias: "datasetcollections.newlayer",
    height: 125,
    width: 400,
    title: "Neuen Layer Hinzufügen",
    resizable: false,
    closeAction: "hide",
    controller: "newlayer",
    listeners: {
        hide: "onClose",
        destroy: "onClose"
    },
    items: [
        {
            xtype: "panel",
            bodyPadding: 10,
            items: [
                {
                    xtype: "combo",
                    fieldLabel: "Schnittstelle wählen",
                    allowBlank: false,
                    id: "newlayerservicetype",
                    store: "ServicesDataset",
                    queryMode: "local",
                    displayField: "title",
                    labelWidth: 120,
                    width: 380
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    "->",
                    {
                        id: "addnewlayermanually",
                        text: "Hinzufügen",
                        listeners: {
                            click: "onAddNewLayer"
                        }
                    }
                ]
            }]
        }
    ]
});
