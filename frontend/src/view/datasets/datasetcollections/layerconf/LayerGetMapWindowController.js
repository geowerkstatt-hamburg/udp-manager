Ext.define("UDPManager.view.datasets.datasetcollections.layerconf.LayerGetMapWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.LayerGetMapWindowController",

    onChangeGetMapBbox: function () {
        const req = this.getViewModel().data.req;
        const bbox = Ext.getCmp("getmapbbox").getSubmitValue();
        const size = Ext.getCmp("getmapsize").getSubmitValue();
        const layer_name = Ext.getCmp("layername").getSubmitValue();
        const service_id = Ext.getCmp("layerserviceid").getSubmitValue();
        const service = Ext.getStore("ServicesDataset").findRecord("id", service_id, 0, false, false, true);
        const srs = Ext.getCmp("datasetsrs").getSubmitValue() ? Ext.getCmp("datasetsrs").getSubmitValue() : UDPManager.Configs.getDefaultDatasetSrs();

        let proj = "CRS";

        if (service.get("version") === "1.1.1") {
            proj = "SRS";
        }

        let url = "";

        if (req === "getMap - intern") {
            url = service.get("url_int") + "?SERVICE=WMS&VERSION=" + service.get("version") + "&REQUEST=GetMap&FORMAT=image/png&BBOX=" + bbox + "&WIDTH=" + size + "&HEIGHT=" + size + "&STYLES=&" + proj + "=" + srs + "&LAYERS=" + layer_name;
        }
        else if (req === "getMap - extern") {
            url = service.get("url_ext") + "?SERVICE=WMS&VERSION=" + service.get("version") + "&REQUEST=GetMap&FORMAT=image/png&BBOX=" + bbox + "&WIDTH=" + size + "&HEIGHT=" + size + "&STYLES=&" + proj + "=" + srs + "&LAYERS=" + layer_name;
        }

        const getMapRequestWindow = Ext.getCmp("layergetmap-window");

        getMapRequestWindow.int_ext_url = url;

        if (Ext.getCmp("getmapbbox").getValue() !== null) {
            Ext.getCmp("getMapRequest").setValue(url);
        }
        else {
            Ext.getCmp("bbox_getMap_ok").disable();
            Ext.getCmp("bbox_getMap_cancel").disable();
        }
    },

    onRequestgetMapBbox: function () {
        const getMapRequestWindow = Ext.getCmp("layergetmap-window");

        window.open(getMapRequestWindow.int_ext_url, "_blank");
    },

    onCancelgetMapBbox: function () {
        Ext.getCmp("layergetmap-window").close();
        return false;
    }
});
