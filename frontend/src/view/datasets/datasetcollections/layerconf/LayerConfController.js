Ext.define("UDPManager.view.datasets.datasetcollections.layerconf.LayerConfController", {
    extend: "Ext.app.ViewController",

    alias: "controller.dataset_collections_layerconf",

    onGridLayerItemSelected: function (sender, record) {
        const editMode = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditMode();

        this.getViewModel().set("layerdetailsform", record);

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").updateLayerJsonField(record.data.id);
        Ext.getCmp("layerswitchcollectionqs").setDisabled(false);
        Ext.getCmp("layerswitchcollectionprod").setDisabled(false);
        Ext.getCmp("layerjsonspecificicon").setHtml("<div class='orangeYellowIconSmall'></div>");

        Ext.getStore("LayerPortals").load({
            params: {layer_id: record.data.id},
            callback: function (records) {
                if (records.length === 0) {
                    Ext.getCmp("deletelayerbutton").setDisabled(!editMode);
                    Ext.getCmp("layerdeleterequestbutton").setDisabled(true);
                }
                else {
                    Ext.getCmp("layerdeleterequestbutton").setDisabled(window.read_only);
                    Ext.getCmp("deletelayerbutton").setDisabled(true);
                }
            }
        });

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllLayerDetailsFieldsetsHidden();
        UDPManager.app.getController("UDPManager.controller.PublicFunctions").emptyLayerJsonField();

        if (editMode) {
            Ext.getCmp("layerjsonupdate").setDisabled(false);
            Ext.getCmp("layerjsonupdateall").setDisabled(false);
        }

        if (record.data.service_type !== "WFS-T" && record.data.service_type !== "WMS-Time") {
            if (Ext.getCmp(record.data.service_type.toLowerCase() + "attrfieldset")) {
                Ext.getCmp(record.data.service_type.toLowerCase() + "attrfieldset").setHidden(false);
            }
        }
        else if (record.data.service_type === "WFS-T") {
            Ext.getCmp("wfsattrfieldset").setHidden(false);
        }
        else if (record.data.service_type === "WMS-Time") {
            Ext.getCmp("wmsattrfieldset").setHidden(false);
        }

        if (record.data.delete_request) {
            Ext.getCmp("layerdeleterequest").setHidden(false);
        }

        Ext.getCmp("layerdetailsform").setDisabled(false);
    },

    onGoToService: function () {
        Ext.getCmp("dataset_main_tabs").setActiveTab(2);

        const datasetServicesGrid = Ext.getCmp("grid_datasetservices");
        const records = Ext.getStore("ServicesDataset").getData().items;
        const service_id = Ext.getCmp("layerserviceid").getSubmitValue();

        for (let i = 0; i < records.length; i++) {
            if (records[i].data.id === service_id) {
                datasetServicesGrid.fireEvent("cellclick", datasetServicesGrid, null, 0, records[i]);
                const row = datasetServicesGrid.store.indexOf(records[i]);

                datasetServicesGrid.getSelectionModel().select(row);
                datasetServicesGrid.ensureVisible(row);
                break;
            }
        }
    },

    onNewLayer: function () {
        let newLayerWindow = Ext.getCmp("newlayer-window");

        if (newLayerWindow) {
            newLayerWindow.destroy();
        }

        if (Ext.getCmp("datasetcollectiongroupobject").getValue()) {
            const wmsFilter = new Ext.util.Filter({
                id: "wmsFilter",
                filterFn: function (item) {
                    return item.get("type") === "WMS" || item.get("type") === "WMS-Time";
                }
            });

            Ext.getStore("ServicesDataset").addFilter(wmsFilter);
        }


        newLayerWindow = Ext.create("datasetcollections.newlayer");

        newLayerWindow.show();
    },

    onSaveLayerDetails: function (btn) {
        btn.setDisabled(true);

        const fieldvalues = Ext.getCmp("layerdetailsform").getForm().getFieldValues();
        const collection_id = Ext.getCmp("datasetcollectionid").getValue();
        const dataset_id = Ext.getCmp("datasetid").getValue();
        const service = Ext.getStore("ServicesDataset").findRecord("id", fieldvalues.service_id, 0, false, false, true);
        const collection = Ext.getStore("CollectionsDataset").findRecord("id", collection_id, 0, false, false, true);
        const st_attributes = {};

        if (!fieldvalues.limit_oaf || fieldvalues.limit_oaf === "" || fieldvalues.limit_oaf <= 0) {
            fieldvalues.limit_oaf = null;
        }

        if (service.data.type === "WFS-T") {
            service.data.type = "wfs";
        }
        if (service.data.type === "WMS-Time") {
            service.data.type = "wms";
        }

        for (const [key, value] of Object.entries(fieldvalues)) {
            if (key.indexOf("_" + service.data.type.toLowerCase()) > -1) {
                st_attributes[key.replace("_" + service.data.type.toLowerCase(), "")] = value;
            }
        }

        const post_data = {
            id: fieldvalues.id,
            collection_id: collection_id,
            dataset_id: dataset_id,
            service_id: fieldvalues.service_id,
            last_edited_by: window.auth_user,
            service_type: service.data.type,
            service_security_type: service.data.security_type,
            service_title: service.data.title,
            service_url_int: service.data.url_int,
            service_url_sec: service.data.url_sec,
            collection_name: collection.data.name,
            collection_title: collection.data.title,
            collection_namespace: collection.data.namespace,
            st_attributes: JSON.stringify(st_attributes)
        };

        Ext.Ajax.request({
            url: "backend/savelayer",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: post_data,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Layer erfolgreich gespeichert!");

                    Ext.getStore("LayersCollection").load({
                        params: {collection_id: collection_id},
                        callback: function (records) {
                            const layer = Ext.getStore("LayersCollection").findRecord("id", response_json.id, 0, false, false, true);
                            const layer_grid = Ext.getCmp("grid_collectionlayers");
                            const row_layer = layer_grid.store.indexOf(layer);

                            layer_grid.getSelectionModel().select(row_layer);
                            layer_grid.ensureVisible(row_layer);

                            if (records.length === 0) {
                                Ext.getCmp("deletecollection").setDisabled(window.read_only);
                            }
                            else {
                                Ext.getCmp("deletecollection").setDisabled(true);
                            }
                        }
                    });

                    // load layers for selected service, when load finished fill theme layer tree
                    Ext.getStore("LayersService").load({
                        params: {service_id: service.data.id},
                        callback: function () {
                            if (service.data.type === "WMS" || service.data.type === "WMS-Time") {
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").fillLayerTreeStore();
                            }
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }

                btn.setDisabled(false);
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                btn.setDisabled(false);
            }
        });
    },

    onDeleteLayer: function () {
        Ext.getCmp("deletelayerbutton").setDisabled(true);

        const mb = Ext.MessageBox;

        mb.buttonText.yes = "ja";
        mb.buttonText.no = "nein";

        mb.confirm("Löschen", "Wirklich löschen?", function (btn) {
            if (btn === "yes") {
                const fieldvalues = Ext.getCmp("layerdetailsform").getForm().getFieldValues();
                const collection_id = Ext.getCmp("datasetcollectionid").getValue();
                const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
                const service = Ext.getStore("ServicesDataset").findRecord("id", fieldvalues.service_id, 0, false, false, true);
                const collection = Ext.getStore("CollectionsDataset").findRecord("id", collection_id, 0, false, false, true);
                const responsible_party_values = Ext.getCmp("datasetresponsibleparty").getSelection().data;
                const mail_from = "\"" + responsible_party_values.inbox_name + "\" <" + responsible_party_values.mail + ">";

                Ext.Ajax.request({
                    url: "backend/deletelayer",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        id: Ext.getCmp("layerid").getSubmitValue(),
                        service_security_type: service.data.security_type,
                        service_title: service.data.title,
                        service_url_int: service.data.url_int,
                        collection_name: collection.data.name,
                        title: collection.data.title,
                        dataset_id: dataset_id,
                        last_edited_by: window.auth_user,
                        mail_from: mail_from
                    },
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);

                        if (response_json.status === "success") {
                            Ext.getStore("LayersCollection").load({
                                params: {collection_id: Ext.getCmp("datasetcollectionid").getValue()},
                                callback: function (records) {
                                    if (records.length === 0) {
                                        Ext.getCmp("deletecollection").setDisabled(window.read_only);
                                    }
                                    else {
                                        Ext.getCmp("deletecollection").setDisabled(true);
                                    }
                                }
                            });

                            // load layers for selected service, when load finished fill theme layer tree
                            Ext.getStore("LayersService").load({
                                params: {service_id: service.data.id},
                                callback: function () {
                                    if (service.data.type === "WMS" || service.data.type === "WMS-Time") {
                                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").fillLayerTreeStore();
                                    }
                                }
                            });

                            Ext.getCmp("layerdetailsform").getForm().reset();
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllLayerDetailsFieldsetsHidden();
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").emptyLayerJsonField();
                            Ext.getCmp("layerdeleterequestbutton").setDisabled(true);
                        }
                        else {
                            Ext.MessageBox.alert("Fehler", response_json.message);
                        }
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Layer konnte nicht gelöscht werden");
                    }
                });
            }
            else {
                Ext.getCmp("deletelayerbutton").setDisabled(false);
            }
        });
    },

    onLayerDeleteRequest: function () {
        Ext.getCmp("layerdeleterequest").setHidden(false);
        Ext.getCmp("layerdeleterequestcomment").setReadOnly(false);
    },

    onSaveLayerDeleteRequest: function () {
        const linked_portals = [];
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const selectedLayer = Ext.getCmp("grid_collectionlayers").getSelectionModel().getSelection()[0].data;
        const responsible_party_values = Ext.getCmp("datasetresponsibleparty").getSelection().data;
        let mail_from = "";

        if (responsible_party_values.inbox_name_alt && responsible_party_values.mail_alt) {
            mail_from = "\"" + responsible_party_values.inbox_name_alt + "\" <" + responsible_party_values.mail_alt + ">";
        }
        else {
            mail_from = "\"" + responsible_party_values.inbox_name + "\" <" + responsible_party_values.mail + ">";
        }

        Ext.getStore("LayerPortals").getRange().forEach(function (record) {
            linked_portals.push(record.data);
        });

        Ext.Ajax.request({
            url: "backend/addlayerdeleterequest",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                id: Ext.getCmp("layerid").getSubmitValue(),
                comment: Ext.getCmp("layerdeleterequestcomment").getSubmitValue(),
                title: selectedLayer.title,
                service_id: selectedLayer.service_id,
                linked_portals: linked_portals,
                dataset_id: dataset_id,
                last_edited_by: window.auth_user,
                mail_from: mail_from
            },
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    Ext.getStore("LayersCollection").load({
                        params: {collection_id: Ext.getCmp("datasetcollectionid").getValue()}
                    });

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Löschvermerk eingetragen!");
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Löschvermerk konnte nicht gespeichert werden");
            }
        });
    },

    onDeleteLayerDeleteRequest: function () {
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();

        Ext.Ajax.request({
            url: "backend/deletelayerdeleterequest",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                id: Ext.getCmp("layerid").getSubmitValue(),
                dataset_id: dataset_id,
                last_edited_by: window.auth_user
            },
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    Ext.getCmp("layerdeleterequest").setHidden(true);

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Löschvermerk entfernt!");
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Löschvermerk konnte nicht gespeichert werden");
            }
        });
    },

    onRecreateJson: function (btn) {
        const dataset_status = Ext.getCmp("datasetstatus").getValue();
        let env = "dev";

        if (dataset_status === "veröffentlicht" || dataset_status === "veröffentlicht - vorveröffentlicht" || dataset_status === "vorveröffentlicht") {
            env = "prod";
        }

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").onRecreateJson(btn.id, env);
    },

    onDeletePortalLayerLink: function (grid, rowIndex) {
        const data = grid.getStore().getAt(rowIndex).data;
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();
        const editMode = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getEditMode();

        if (data.update_mode === "manual") {
            Ext.getCmp("portallinks-grid").getSelectionModel().deselectAll();

            Ext.Ajax.request({
                url: "backend/deleteportallayerlink",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    layer_id: data.layer_id,
                    portal_id: data.id,
                    dataset_id: dataset_id,
                    last_edited_by: window.auth_user
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        grid.getStore().removeAt(rowIndex);

                        if (grid.getStore().count() === 0) {
                            Ext.getCmp("deletelayerbutton").setDisabled(!editMode);
                            Ext.getCmp("layerdeleterequestbutton").setDisabled(true);
                        }
                        else {
                            Ext.getCmp("deletelayerbutton").setDisabled(true);
                            Ext.getCmp("layerdeleterequestbutton").setDisabled(window.read_only);
                        }
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Link konnte nicht gelöscht werden");
                }
            });
        }
    },

    onSelectLayerJson: function (btn) {
        const layerId = Ext.getCmp("grid_collectionlayers").getSelectionModel().getSelection()[0].id;
        let selectionEnv;

        if (btn.id === "layerswitchcollectionqs") {
            selectionEnv = "stage";
            Ext.getCmp("layerjsonspecificicon").setHtml("<div class='orangeYellowIconSmall'></div>");
        }
        else if (btn.id === "layerswitchcollectionprod") {
            selectionEnv = "prod";
            Ext.getCmp("layerjsonspecificicon").setHtml("<div class='x-fa fa-circle greenIconSmall'></div>");
        }

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").updateLayerJsonField(layerId, selectionEnv);
    }
});
