Ext.define("UDPManager.view.datasets.datasetcollections.layerconf.NewLayerWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.newlayer",

    onAddNewLayer: function () {
        const rec = new UDPManager.model.UDPManagerModel({
            id: 0,
            name: !Ext.getCmp("datasetcollectionnamealternative").getSubmitValue() ? Ext.getCmp("datasetcollectionname").getSubmitValue() : Ext.getCmp("datasetcollectionnamealternative").getSubmitValue(),
            service_type: Ext.getCmp("newlayerservicetype").getSelection().data.type,
            service_id: Ext.getCmp("newlayerservicetype").getSelection().data.id,
            service_title: Ext.getCmp("newlayerservicetype").getSelection().data.title
        });

        if (Ext.getCmp("newlayerservicetype").getSelection().data.type === "WMS" || Ext.getCmp("newlayerservicetype").getSelection().data.type === "WMS-Time") {
            rec.set("group_layer_wms", Ext.getCmp("datasetcollectiongroupobject").getValue());
        }

        const count = Ext.getStore("LayersCollection").count();
        const service_type = Ext.getCmp("newlayerservicetype").getSelection().data.type;
        const namespace = Ext.getCmp("datasetcollectionnamespace").getSubmitValue();
        const reg_namespace = /(.*)=(.*)/g;

        if (!namespace && (service_type === "WFS" || service_type === "WFS-T")) {
            Ext.MessageBox.alert("Hinweis", "Layer konnte nicht angelegt werden. Das Feld Namespace muss gefüllt sein! <br><br> Anzugeben in der Schreibweise Prefix=URL.");
        }
        else if (!reg_namespace.test(namespace) && (service_type === "WFS" || service_type === "WFS-T")) {
            Ext.MessageBox.alert("Hinweis", "Layer konnte nicht angelegt werden. Schreibweise des Namespace ist ungültig! <br><br> Anzugeben in der Schreibweise Prefix=URL.");
        }
        else {
            const layer = Ext.getStore("LayersCollection").findRecord("service_type", service_type, 0, false, false, true);

            if (layer) {
                Ext.MessageBox.alert("Hinweis", "Ein Layer diesen Typs ist bereits vorhanden!");
            }
            else {
                Ext.getStore("LayersCollection").insert(count, rec);
                Ext.getCmp("grid_collectionlayers").getSelectionModel().select(count);
                // Ext.getCmp(Ext.getCmp("newlayerservicetype").getSubmitValue().toLowerCase() + "attrfieldset").setHidden(false);
                Ext.getCmp("newlayer-window").destroy();
            }
        }
    },

    onClose: function () {
        if (Ext.getStore("ServicesDataset").getFilters().length > 0) {
            Ext.getStore("ServicesDataset").removeFilter("wmsFilter");
        }
    }
});
