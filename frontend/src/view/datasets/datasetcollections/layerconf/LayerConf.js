Ext.define("UDPManager.view.datasets.datasetcollections.layerconf.LayerConf", {
    extend: "Ext.panel.Panel",
    xtype: "dataset_collections_layerconf",
    alias: "widget.dataset_collections_layerconf",
    controller: "dataset_collections_layerconf",
    layout: "vbox",
    listeners: {
        select: "onItemSelected"
    },
    viewModel: {
        data: {
            layerdetailsform: null
        }
    },

    initComponent: function () {
        this.items = [
            {
                xtype: "panel",
                layout: "column",
                width: "100%",
                title: "Layerkonfiguration",
                header: false,
                items: [
                    {
                        xtype: "gridpanel",
                        id: "grid_collectionlayers",
                        bind: {
                            store: "{gridstore_layers}"
                        },
                        reference: "grid_collectionlayers",
                        style: "border-left: 1px solid #cfcfcf !important;border-right: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                        rowLines: false,
                        columns: [
                            {text: "ID", dataIndex: "id", width: 70},
                            {text: "Schnittstellenlayer", dataIndex: "service_title", flex: 1}
                        ],
                        viewConfig: {
                            //stripeRows: true,
                            enableTextSelection: true,
                            getRowClass: function () {
                                return "multiline-row";
                            }
                        },
                        columnWidth: 0.3,
                        listeners: {
                            select: "onGridLayerItemSelected"
                        },
                        height: Ext.Element.getViewportHeight() - 106,
                        autoScroll: true,
                        buttonAlign: "left",
                        dockedItems: [{
                            xtype: "toolbar",
                            dock: "bottom",
                            items: [
                                {
                                    id: "newlayerbutton",
                                    iconCls: "x-fa fa-plus",
                                    tooltip: "Neuen Layer anlegen",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    disabled: window.read_only,
                                    listeners: {
                                        click: "onNewLayer"
                                    }
                                },
                                {
                                    xtype: "button",
                                    iconCls: "x-fa fa-trash",
                                    id: "deletelayerbutton",
                                    cls: "custom-btn-icon",
                                    scale: "medium",
                                    disabled: true,
                                    tooltip: "Ausgewählten Layer löschen",
                                    listeners: {
                                        click: "onDeleteLayer"
                                    }
                                }
                            ]
                        }]
                    },
                    {
                        xtype: "tabpanel",
                        id: "collectionlayers_main_tabs",
                        activeTab: 0,
                        tabPosition: "left",
                        tabRotation: 0,
                        columnWidth: 0.699,
                        items: [
                            {
                                title: "Layer-Details",
                                xtype: "form",
                                id: "layerdetailsform",
                                defaultType: "textfield",
                                style: "border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                height: Ext.Element.getViewportHeight() - 106,
                                autoScroll: true,
                                bodyPadding: 20,
                                scrollable: true,
                                disabled: true,
                                items: [
                                    {
                                        xtype: "component",
                                        margin: "0 0 10 165",
                                        id: "layereditnotes",
                                        html: "",
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "Layer-Attribute können auch ohne Bearbeitungsmodus aktualisiert werden"
                                        }
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "LÖSCHVERMERK",
                                        id: "layerdeleterequest",
                                        hidden: true,
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "textareafield",
                                                id: "layerdeleterequestcomment",
                                                labelWidth: 0,
                                                width: "100%",
                                                height: 100,
                                                grow: true,
                                                bind: "{layerdetailsform.delete_request_comment}"
                                            },
                                            {
                                                xtype: "fieldcontainer",
                                                layout: "hbox",
                                                width: "100%",
                                                items: [
                                                    {
                                                        xtype: "button",
                                                        id: "savelayerdeleterequestbutton",
                                                        text: "Speichern",
                                                        width: "120px",
                                                        margin: "10 25 25 0",
                                                        listeners: {
                                                            click: "onSaveLayerDeleteRequest"
                                                        }
                                                    },
                                                    {
                                                        xtype: "button",
                                                        id: "deletelayerdeleterequestbutton",
                                                        text: "Entfernen",
                                                        width: "120px",
                                                        margin: "10 25 25 0",
                                                        listeners: {
                                                            click: "onDeleteLayerDeleteRequest"
                                                        }
                                                    }
                                                ]
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "numberfield",
                                        fieldLabel: "Layer-ID",
                                        id: "layerid",
                                        name: "id",
                                        readOnly: true,
                                        hidden: true,
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "ID des Layers"
                                        },
                                        labelWidth: 160,
                                        width: 360,
                                        bind: "{layerdetailsform.id}"
                                    },
                                    {
                                        fieldLabel: "Name",
                                        id: "layername",
                                        name: "name",
                                        readOnly: true,
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "Name des Layers"
                                        },
                                        labelWidth: 160,
                                        width: "100%",
                                        bind: "{layerdetailsform.name}"
                                    },
                                    {
                                        xtype: "fieldcontainer",
                                        layout: "hbox",
                                        items: [
                                            {
                                                xtype: "textfield",
                                                id: "layerservicetitle",
                                                name: "service_title",
                                                labelWidth: 160,
                                                fieldLabel: "Schnittstelle",
                                                flex: 1,
                                                readOnly: true,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Titel der bereitstellenden Schnittstelle"
                                                },
                                                bind: "{layerdetailsform.service_title}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                id: "layerserviceid",
                                                name: "service_id",
                                                hidden: true,
                                                bind: "{layerdetailsform.service_id}"
                                            },
                                            {
                                                xtype: "button",
                                                id: "goToService",
                                                iconCls: "x-fa fa-share",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Zu Schnittstelle springen"
                                                },
                                                listeners: {
                                                    click: "onGoToService"
                                                },
                                                width: 50,
                                                margin: "0 0 0 20"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "WMS Parameter",
                                        id: "wmsattrfieldset",
                                        hidden: true,
                                        defaultType: "textfield",
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "checkboxfield",
                                                id: "layergrouplayer",
                                                name: "group_layer_wms",
                                                fieldLabel: "Gruppenlayer",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{layerdetailsform.group_layer_wms}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                fieldLabel: "Gutter (Px)",
                                                id: "layergutter",
                                                name: "gutter_wms",
                                                minValue: 0,
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{layerdetailsform.gutter_wms}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                id: "layertilesize",
                                                name: "tile_size_wms",
                                                fieldLabel: "Kachelgröße (Px)",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{layerdetailsform.tile_size_wms}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "layersingletile",
                                                name: "single_tile_wms",
                                                fieldLabel: "Singletile",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{layerdetailsform.single_tile_wms}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "layertransparent",
                                                name: "transparent_wms",
                                                fieldLabel: "Transparent",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{layerdetailsform.transparent_wms}"
                                            },
                                            {
                                                xtype: "combo",
                                                fieldLabel: "Ausgabeformat",
                                                store: ["image/png", "image/jpeg"],
                                                name: "output_format_wms",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Standardrückgabeformat bei WMS getMap Requests"
                                                },
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{layerdetailsform.output_format_wms}"
                                            },
                                            {
                                                xtype: "combo",
                                                fieldLabel: "GFI Format",
                                                store: ["", "text/html"],
                                                name: "gfi_format_wms",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Wenn das Feld nicht ausgefüllt wird, wird text/xml verwendet"
                                                },
                                                labelWidth: 150,
                                                bind: "{layerdetailsform.gfi_format_wms}"
                                            },
                                            {
                                                xtype: "numberfield",
                                                fieldLabel: "Feature Count",
                                                name: "feature_count_wms",
                                                labelWidth: 150,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Anzahl der maximal abfragbaren Feature bei einem Klick."
                                                },
                                                defaultValue: true,
                                                bind: "{layerdetailsform.feature_count_wms}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                name: "notsupportedin3d_wms",
                                                fieldLabel: "Nicht für 3D Anwendungen",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Häkchen setzen, wenn Layer nicht in 3D Anwendungen benutzbar ist."
                                                },
                                                defaultValue: true,
                                                bind: "{layerdetailsform.notsupportedin3d_wms}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                name: "time_series_wms",
                                                fieldLabel: "Zeitreihe (WMS-Time)",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Häkchen setzen, wenn der Layer eine zeitliche Komponente für eine Zeitreihe (WMS-Time) enthält."
                                                },
                                                defaultValue: true,
                                                bind: "{layerdetailsform.time_series_wms}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "WMTS Parameter",
                                        id: "wmtsattrfieldset",
                                        hidden: true,
                                        defaultType: "textfield",
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "WFS Parameter",
                                        id: "wfsattrfieldset",
                                        hidden: true,
                                        defaultType: "textfield",
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "combo",
                                                fieldLabel: "Ausgabeformat",
                                                store: ["XML"],
                                                name: "output_format_wfs",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Standardrückgabeformat bei WFS getFeature Requests"
                                                },
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{layerdetailsform.output_format_wfs}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "OAF Parameter",
                                        id: "oafattrfieldset",
                                        hidden: true,
                                        defaultType: "textfield",
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "numberfield",
                                                fieldLabel: "Limit",
                                                id: "layeroaflimit",
                                                name: "limit_oaf",
                                                labelWidth: 150,
                                                bind: "{layerdetailsform.limit_oaf}",
                                                autoEl: {
                                                    tag: "div",
                                                    "data-qtip": "Limitiert die Anzahl der zurückgegebenen Elemente. Nur positive Ganzzahl gültig!"
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "Terrain3D Einstellungen",
                                        id: "terrain3dattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "checkboxfield",
                                                id: "layerrequestvertexnormals",
                                                name: "request_vertex_normals_terrain3d",
                                                fieldLabel: "requestVertexNormals",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{layerdetailsform.request_vertex_normals_terrain3d}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "TileSet3D Einstellungen",
                                        id: "tileset3dattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "numberfield",
                                                fieldLabel: "maxScreenSpaceError",
                                                id: "layermaximumscreenspaceerror",
                                                name: "maximum_screen_space_error_tileset3d",
                                                minValue: 0,
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.maximum_screen_space_error_tileset3d}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "Oblique Einstellungen",
                                        id: "obliqueattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "numberfield",
                                                id: "layerhidelevels",
                                                name: "hidelevels_oblique",
                                                fieldLabel: "hideLevels",
                                                minValue: 0,
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.hidelevels_oblique}"
                                            }, {
                                                xtype: "numberfield",
                                                id: "layerminzoom",
                                                name: "minzoom_oblique",
                                                minValue: 0,
                                                fieldLabel: "minZoom",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.minzoom_oblique}"
                                            }, {
                                                xtype: "numberfield",
                                                id: "layerresolution",
                                                name: "resolution_oblique",
                                                fieldLabel: "resolution",
                                                minValue: 0,
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.resolution_oblique}"
                                            }, {
                                                xtype: "textfield",
                                                id: "layerprojection",
                                                name: "projection_oblique",
                                                fieldLabel: "projection",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.projection_oblique}"
                                            }, {
                                                xtype: "textfield",
                                                id: "layerterrainurl",
                                                name: "terrainurl_oblique",
                                                fieldLabel: "terrainUrl",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.terrainurl_oblique}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "STA Einstellungen",
                                        id: "staattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "combo",
                                                fieldLabel: "URL Param. - Root",
                                                store: ["Datastreams", "Things"],
                                                id: "layerURLParamsRootEl",
                                                name: "rootel_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                defaultValue: true,
                                                bind: "{layerdetailsform.rootel_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "URL Param. - Filter",
                                                id: "layerURLParamsFilter",
                                                name: "filter_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.filter_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "URL Param. - Expand",
                                                id: "layerURLParamsExpand",
                                                name: "expand_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.expand_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "EPSG",
                                                name: "epsg_sta",
                                                id: "layerEPSG",
                                                labelWidth: 150,
                                                width: "100%",
                                                defaultValue: true,
                                                bind: "{layerdetailsform.epsg_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "Zu ersetzende LayerIDs",
                                                id: "layerRelatedWMSLayerIDs",
                                                name: "related_wms_layers_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.related_wms_layers_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "Style ID",
                                                id: "layerStyleID",
                                                name: "style_id_sta",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.style_id_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                id: "layerClusterDistance",
                                                name: "cluster_distance_sta",
                                                fieldLabel: "ClusterDistance (Px)",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.cluster_distance_sta}"
                                            },
                                            {
                                                xtype: "textfield",
                                                id: "layerMouseHoverField",
                                                name: "mouse_hover_field_sta",
                                                fieldLabel: "MouseHoverField",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.mouse_hover_field_sta}"
                                            },
                                            {
                                                xtype: "textareafield",
                                                id: "layerStaTestUrl",
                                                name: "sta_test_url_sta",
                                                fieldLabel: "Test URL",
                                                labelWidth: 150,
                                                width: "100%",
                                                height: 100,
                                                grow: true,
                                                bind: "{layerdetailsform.sta_test_url_sta}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                id: "layerLoadThingsOnlyInCurrentExtent",
                                                name: "load_things_only_in_current_extent_sta",
                                                fieldLabel: "Nur Objeckte in aktuellem Ausschnitt laden",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                defaultValue: true,
                                                bind: "{layerdetailsform.load_things_only_in_current_extent_sta}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldset",
                                        title: "Vector Tiles Einstellungen",
                                        id: "vectortilesattrfieldset",
                                        hidden: true,
                                        defaults: {
                                            anchor: "100%"
                                        },
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "Extent",
                                                id: "layerextent",
                                                name: "extent_vectortiles",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.extent_vectortiles}"
                                            },
                                            {
                                                xtype: "textfield",
                                                fieldLabel: "Origin",
                                                id: "layerorigin",
                                                name: "origin_vectortiles",
                                                labelWidth: 150,
                                                width: "100%",
                                                bind: "{layerdetailsform.origin_vectortiles}"
                                            },
                                            {
                                                xtype: "textareafield",
                                                fieldLabel: "Resolutions",
                                                id: "layerresolutions",
                                                name: "resolutions_vectortiles",
                                                labelWidth: 150,
                                                width: "100%",
                                                height: 100,
                                                grow: true,
                                                bind: "{layerdetailsform.resolutions_vectortiles}"
                                            },
                                            {
                                                xtype: "textareafield",
                                                fieldLabel: "VT Styles",
                                                id: "layervtstyles",
                                                name: "vtstyles_vectortiles",
                                                labelWidth: 150,
                                                width: "100%",
                                                height: 100,
                                                grow: true,
                                                bind: "{layerdetailsform.vtstyles_vectortiles}"
                                            },
                                            {
                                                xtype: "checkboxfield",
                                                fieldLabel: "Visibility",
                                                id: "layervisibility",
                                                name: "visibility_vectortiles",
                                                boxLabel: "",
                                                labelWidth: 150,
                                                bind: "{layerdetailsform.visibility_vectortiles}"
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldcontainer",
                                        layout: "hbox",
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "button",
                                                id: "layerdeleterequestbutton",
                                                text: "Löschvermerk",
                                                width: 120,
                                                margin: "0 25 10 0",
                                                disabled: true,
                                                listeners: {
                                                    click: "onLayerDeleteRequest"
                                                }
                                            }
                                        ]
                                    }
                                ],
                                dockedItems: [{
                                    xtype: "toolbar",
                                    dock: "bottom",
                                    border: true,
                                    style: "border-top: 1px solid #cfcfcf !important;",
                                    items: [
                                        {
                                            xtype: "component",
                                            html: "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Pflichtfeld</span>",
                                            hidden: true
                                        },
                                        {
                                            xtype: "component",
                                            html: "<span style='color:grey;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Standardwert</span>"
                                        },
                                        {
                                            xtype: "component",
                                            flex: 1,
                                            html: ""
                                        },
                                        {
                                            id: "savelayerdetailsbutton",
                                            text: "Speichern",
                                            scale: "medium",
                                            cls: "btn-save",
                                            disabled: true,
                                            listeners: {
                                                click: "onSaveLayerDetails"
                                            }
                                        }
                                    ]
                                }]
                            },
                            {
                                title: "Portale",
                                bodyPadding: "0 0 0 0",
                                width: "100%",
                                items: [
                                    {
                                        xtype: "gridpanel",
                                        store: "LayerPortals",
                                        title: "gekoppelte Portale",
                                        id: "portallinks-grid",
                                        autoScroll: true,
                                        height: Ext.Element.getViewportHeight() - 149,
                                        style: "border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                        width: "100%",
                                        viewConfig: {
                                            enableTextSelection: true
                                        },
                                        columnLines: true,
                                        selModel: "rowmodel",
                                        features: [{
                                            ftype: "grouping",
                                            startCollapsed: true,
                                            groupHeaderTpl: '{columnName}: {name} ({rows.length} Portal{[values.rows.length > 1 ? "e" : ""]})'
                                        }],
                                        columns: [
                                            {
                                                text: "Titel",
                                                dataIndex: "title",
                                                align: "left",
                                                flex: 1
                                            },
                                            {
                                                text: "URI",
                                                dataIndex: "url",
                                                align: "left",
                                                flex: 1
                                            },
                                            {
                                                text: "Host",
                                                dataIndex: "host",
                                                align: "left",
                                                hidden: true,
                                                flex: 1
                                            },
                                            {
                                                text: "Link",
                                                dataIndex: "url",
                                                align: "left",
                                                width: 70,
                                                renderer: function (value, metaData, record) {
                                                    if (value !== "") {
                                                        return "<a href=\"" + record.data.host + value + "\" target=\"_blank\">Link</a>";
                                                    }
                                                    else {
                                                        return "";
                                                    }
                                                }
                                            },
                                            {
                                                xtype: "actioncolumn",
                                                header: "Löschen",
                                                name: "delete",
                                                align: "center",
                                                sortable: false,
                                                menuDisabled: true,
                                                width: 80,
                                                renderer: function (value, metadata, record) {
                                                    if (record.get("update_mode") === "manual" && !window.read_only) {
                                                        this.items[0].iconCls = "x-fa fa-times";
                                                        this.items[0].tooltip = "Verlinkung löschen";
                                                        this.items[0].handler = "onDeletePortalLayerLink";
                                                    }
                                                    else {
                                                        this.items[0].iconCls = "";
                                                        this.items[0].tooltip = "Keine Funktion verfügbar";
                                                    }
                                                },
                                                items: [
                                                    {
                                                        iconCls: "x-fa fa-times",
                                                        tooltip: "Verlinkung löschen",
                                                        handler: "onDeletePortalLayerLink"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            },
                            {
                                title: "JSON",
                                bodyPadding: "5 5 5 5",
                                height: Ext.Element.getViewportHeight() - 149,
                                style: "border-left: 1px solid #cfcfcf !important;border-top: 1px solid #cfcfcf !important;",
                                items: [
                                    {
                                        xtype: "fieldcontainer",
                                        layout: "hbox",
                                        items: [
                                            {
                                                xtype: "button",
                                                id: "layerjsonupdate",
                                                text: "JSON neu erzeugen",
                                                margin: "15 10 10 15",
                                                width: 230,
                                                disabled: true,
                                                listeners: {
                                                    click: "onRecreateJson"
                                                }
                                            },
                                            {
                                                xtype: "button",
                                                id: "layerjsonupdateall",
                                                text: "JSON neu erzeugen (ganze Collection)",
                                                margin: "15 15 10 10",
                                                width: 230,
                                                disabled: true,
                                                listeners: {
                                                    click: "onRecreateJson"
                                                }
                                            }
                                        ]
                                    },
                                    {
                                        xtype: "fieldcontainer",
                                        margin: "0 0 10 15",
                                        items: [{
                                            xtype: "segmentedbutton",
                                            items: [
                                                {
                                                    id: "layerswitchcollectionqs",
                                                    text: "dev / stage",
                                                    scale: "medium",
                                                    disabled: true,
                                                    cls: "btn-api-stage-gradient",
                                                    listeners: {
                                                        click: "onSelectLayerJson"
                                                    }
                                                },
                                                {
                                                    id: "layerswitchcollectionprod",
                                                    text: "prod",
                                                    scale: "medium",
                                                    disabled: true,
                                                    cls: "btn-api-prod",
                                                    listeners: {
                                                        click: "onSelectLayerJson"
                                                    }
                                                }
                                            ]
                                        }]
                                    },
                                    {
                                        xtype: "fieldset",
                                        id: "layerjsonspecificfieldset",
                                        padding: "5 5 5 15",
                                        cls: "prio-fieldset",
                                        collapsible: false,
                                        collapsed: false,
                                        border: false,
                                        items: [
                                            {
                                                xtype: "component",
                                                id: "layerjsonspecificicon",
                                                html: "<div class='orangeYellowIconSmall'></div>"
                                            },
                                            {
                                                xtype: "textareafield",
                                                id: "layer_json_field",
                                                labelWidth: 0,
                                                height: Ext.Element.getViewportHeight() - 279,
                                                width: "100%",
                                                autoscroll: true
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    }
                ]
            }
        ];

        this.callParent(arguments);
    }
});
