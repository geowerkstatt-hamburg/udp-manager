Ext.define("UDPManager.view.datasets.datasetcollections.layerconf.LayerGetMapWindow", {
    extend: "Ext.window.Window",
    xtype: "layergetmapwindow",
    id: "layergetmap-window",
    alias: "datasetcollections.LayerGetMapWindow",
    height: 200,
    width: 600,
    title: "Auswahl Bounding Box für Testrequest",
    bodyStyle: "margin: 10px;",
    controller: "LayerGetMapWindowController",
    int_ext_url: "int",
    initComponent: function () {
        const srs = Ext.getCmp("datasetsrs").getSubmitValue() ? Ext.getCmp("datasetsrs").getSubmitValue() : UDPManager.Configs.getDefaultDatasetSrs();

        this.items = [
            {
                xtype: "combo",
                fieldLabel: "Bounding Box",
                store: UDPManager.Configs.getTestBboxBySrs(srs),
                displayField: "name",
                allowBlank: false,
                id: "getmapbbox",
                valueField: "bbox",
                editable: false,
                margin: "0 10 10 0",
                labelWidth: 100,
                width: 300,
                listeners: {
                    change: "onChangeGetMapBbox"
                }
            },
            {
                xtype: "combo",
                fieldLabel: "Bildgröße",
                store: ["256", "512", "1024"],
                value: "256",
                allowBlank: false,
                id: "getmapsize",
                margin: "0 10 10 0",
                labelWidth: 100,
                width: 300,
                listeners: {
                    change: "onChangeGetMapBbox"
                }
            },
            {
                xtype: "textfield",
                id: "getMapRequest",
                fieldLabel: "get Map Aufruf",
                width: 550,
                allowBlank: false,
                style: "font-weight: bold; color: #003168;"
            }
        ];
        this.buttons = [
            {
                text: "Request ausführen",
                margin: "0 5 0 10",
                id: "bbox_getMap_ok",
                disabled: false,
                listeners: {
                    click: "onRequestgetMapBbox"
                }
            },
            {
                text: "Abbrechen",
                margin: "0 5 0 10",
                id: "bbox_getMap_cancel",
                disabled: false,
                listeners: {
                    click: "onCancelgetMapBbox"
                }
            }
        ];
        this.callParent(arguments);
    }
});
