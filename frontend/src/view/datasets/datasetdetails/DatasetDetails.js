Ext.define("UDPManager.view.datasets.datasetdetails.DatasetDetails", {
    extend: "Ext.panel.Panel",
    xtype: "dataset_details",
    alias: "widget.dataset_details",
    title: "Details",
    controller: "dataset_details",
    layout: "vbox",
    height: Ext.Element.getViewportHeight() - 100,
    listeners: {
        beforeRender: "beforeRender"
    },
    initComponent: function () {
        this.items = [
            {
                xtype: "form",
                id: "datasetdetailsform",
                width: "100%",
                disabled: true,
                defaultType: "textfield",
                scrollable: true,
                autoScroll: true,
                bodyPadding: 20,
                flex: 1,
                style: "border-top: 1px solid #cfcfcf !important;",
                items: [
                    {
                        xtype: "fieldset",
                        padding: "10 5 5 10",
                        cls: "prio-fieldset",
                        width: "100%",
                        style: "background-color: #f6f6f6",
                        items: [
                            {
                                xtype: "fieldcontainer",
                                layout: "hbox",
                                width: "100%",
                                items: [
                                    {
                                        xtype: "component",
                                        id: "datasetstatusicon",
                                        margin: "12 0 0 40",
                                        html: "<div class=\"x-fa fa-globe-africa statusIcon\" data-qtip=\"veröffentlicht\"></div>"
                                    },
                                    {
                                        xtype: "fieldcontainer",
                                        layout: "vbox",
                                        width: "100%",
                                        items: [
                                            {
                                                xtype: "fieldcontainer",
                                                layout: "hbox",
                                                width: "100%",
                                                defaultType: "textfield",
                                                margin: "0 0 2 80",
                                                items: [
                                                    {
                                                        fieldLabel: "Status",
                                                        labelWidth: 105,
                                                        width: 305,
                                                        id: "datasetstatus",
                                                        editable: false,
                                                        readOnly: true,
                                                        name: "status",
                                                        bind: "{form_datasets.status}"
                                                    },
                                                    {
                                                        fieldLabel: "Veröffentlichung",
                                                        id: "datasetpublishdate",
                                                        name: "publish_date",
                                                        labelWidth: 105,
                                                        width: 305,
                                                        margin: "0 0 0 20",
                                                        editable: false,
                                                        readOnly: true,
                                                        hidden: true,
                                                        bind: "{form_datasets.publish_date}"
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldcontainer",
                                                layout: "hbox",
                                                width: "100%",
                                                defaultType: "textfield",
                                                margin: "0 0 2 80",
                                                items: [
                                                    {
                                                        fieldLabel: "Erstellt am",
                                                        id: "datasetcreatedate",
                                                        name: "create_date",
                                                        labelWidth: 105,
                                                        width: 305,
                                                        readOnly: true,
                                                        editable: false,
                                                        bind: "{form_datasets.create_date}"
                                                    },
                                                    {
                                                        fieldLabel: "von",
                                                        id: "datasetcreatedby",
                                                        name: "created_by",
                                                        labelWidth: 105,
                                                        width: 305,
                                                        margin: "0 0 0 20",
                                                        readOnly: true,
                                                        editable: false,
                                                        bind: "{form_datasets.created_by}"
                                                    }
                                                ]
                                            },
                                            {
                                                xtype: "fieldcontainer",
                                                layout: "hbox",
                                                width: "100%",
                                                defaultType: "textfield",
                                                margin: "0 0 2 80",
                                                items: [
                                                    {
                                                        fieldLabel: "Zuletzt bearbeitet",
                                                        id: "datasetlasteditdate",
                                                        name: "last_edit_date",
                                                        labelWidth: 105,
                                                        width: 305,
                                                        readOnly: true,
                                                        editable: false,
                                                        bind: "{form_datasets.last_edit_date}"
                                                    },
                                                    {
                                                        fieldLabel: "von",
                                                        id: "datasetlasteditedby",
                                                        name: "last_edited_by",
                                                        labelWidth: 105,
                                                        width: 305,
                                                        margin: "0 0 0 20",
                                                        readOnly: true,
                                                        editable: false,
                                                        bind: "{form_datasets.last_edited_by}"
                                                    }
                                                ]
                                            }
                                        ]
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: "component",
                        margin: "20 0 10 175",
                        id: "dataseteditnotes",
                        html: "",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Redaktionelle Inhalte können auch ohne Bearbeitungsmodus aktualisiert werden"
                        }
                    },
                    {
                        xtype: "numberfield",
                        fieldLabel: "Datensatz-ID",
                        id: "datasetid",
                        name: "id",
                        editable: false,
                        readOnly: true,
                        hidden: true,
                        labelWidth: 170,
                        width: 360,
                        bind: "{form_datasets.id}"
                    },
                    {
                        fieldLabel: "Titel",
                        id: "datasettitle",
                        name: "title",
                        labelWidth: 170,
                        width: "100%",
                        bind: "{form_datasets.title}",
                        requiredField: true,
                        editModeOnly: true
                    },
                    {
                        xtype: "textfield",
                        id: "datasetshortname",
                        name: "shortname",
                        labelWidth: 170,
                        requiredField: true,
                        fieldLabel: "Kurzname",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Kurzname des Datensatzes. Wichtig für weitere Verwendung unter Collection (Schema) und Schnittstelle (Endpunktname)."
                        },
                        width: "100%",
                        bind: "{form_datasets.shortname}",
                        editModeOnly: true
                    },
                    {
                        fieldLabel: "Externer Datensatz",
                        id: "datasetexternal",
                        name: "external",
                        xtype: "checkboxfield",
                        labelWidth: 170,
                        width: "100%",
                        bind: "{form_datasets.external}",
                        listeners: {
                            change: "onExternalChange"
                        },
                        editModeOnly: true
                    },
                    {
                        id: "datasetrestrictionlevel",
                        name: "restriction_level",
                        fieldLabel: "Freigabeebene",
                        xtype: "combo",
                        store: ["internet", "intranet"],
                        labelWidth: 170,
                        width: "100%",
                        requiredField: true,
                        editable: false,
                        bind: "{form_datasets.restriction_level}",
                        editModeOnly: true
                    },
                    {
                        fieldLabel: "Verantwortliche Stelle",
                        xtype: "combo",
                        labelWidth: 170,
                        width: "100%",
                        id: "datasetresponsibleparty",
                        name: "responsible_party",
                        store: Ext.create("Ext.data.Store", {
                            fields: ["name", "mail", "inbox_name"],
                            data: UDPManager.Configs.getResponsibleParty()
                        }),
                        displayField: "name",
                        valueField: "name",
                        bind: "{form_datasets.responsible_party}",
                        requiredField: true
                    },
                    {
                        fieldLabel: "Projekt / Kostensammler",
                        id: "datasetprojectreference",
                        name: "project_reference",
                        labelWidth: 170,
                        width: "100%",
                        bind: "{form_datasets.project_reference}"
                    },
                    {
                        xtype: "tagfield",
                        fieldLabel: "Filter-Schlagworte",
                        id: "datasetfilterkeywords",
                        name: "filter_keywords",
                        labelWidth: 170,
                        width: "100%",
                        autoEl: {
                            tag: "div",
                            "data-qtip": "Auswahl über Dropddown, oder durch Eingabe, gefolgt von &quot;,&quot;"
                        },
                        store: "DatasetFilterKeywords",
                        displayField: "text",
                        valueField: "text",
                        forceSelection: false,
                        queryMode: "local",
                        createNewOnEnter: true,
                        filterPickList: true,
                        bind: "{form_datasets.filter_keywords}",
                        readOnly: false
                    },
                    {
                        xtype: "fieldset",
                        title: "Regionale Zugehörigkeit",
                        padding: "0 5 0 5",
                        cls: "prio-fieldset",
                        collapsible: true,
                        collapsed: true,
                        items: [
                            {
                                id: "datasetfederalstate",
                                name: "federalstate",
                                fieldLabel: "Länderebene",
                                xtype: "combo",
                                editable: false,
                                store: Ext.create("Ext.data.Store", {
                                    data: UDPManager.Configs.regionLandlevelFilterListGen()
                                }),
                                listeners: {
                                    change: "onChangeLandlevel"
                                },
                                labelWidth: 165,
                                width: "100%",
                                bind: "{form_datasets.federalstate}"
                            },
                            {
                                id: "datasetdistrict",
                                name: "district",
                                fieldLabel: "Kreisebene",
                                xtype: "combo",
                                editable: false,
                                store: [],
                                listeners: {
                                    focus: "onSetDatasetdistrictStore"
                                },
                                labelWidth: 165,
                                width: "100%",
                                bind: "{form_datasets.district}"
                            }
                        ]
                    },
                    {
                        fieldLabel: "Datenquelle/Fachsystem",
                        id: "datasetdatasource",
                        name: "datasource",
                        labelWidth: 170,
                        width: "100%",
                        bind: "{form_datasets.datasource}"
                    },
                    {
                        fieldLabel: "Koordinatenreferenzsystem",
                        id: "datasetsrs",
                        name: "srs",
                        labelWidth: 170,
                        width: "100%",
                        defaultValue: true,
                        bind: "{form_datasets.srs}",
                        editModeOnly: true
                    },
                    {
                        fieldLabel: "Bounding Box",
                        id: "datasetbbox",
                        name: "bbox",
                        labelWidth: 170,
                        width: "100%",
                        hidden: true,
                        bind: "{form_datasets.bbox}",
                        editModeOnly: true
                    },
                    {
                        xtype: "combo",
                        store: ["Raster", "Vektor", "Beides"],
                        fieldLabel: "Datentyp",
                        id: "datasetstoretype",
                        name: "store_type",
                        labelWidth: 170,
                        width: "100%",
                        defaultValue: true,
                        editable: false,
                        bind: "{form_datasets.store_type}",
                        listeners: {
                            change: "onStoreTypeChange"
                        },
                        editModeOnly: true
                    },
                    {
                        xtype: "combo",
                        fieldLabel: "DB-Verbindung (lesend)",
                        id: "datasetdbconnectionrid",
                        store: "ConfigDbConnections",
                        queryMode: "local",
                        displayField: "name",
                        valueField: "id",
                        name: "db_connection_r_id",
                        labelWidth: 170,
                        hidden: true,
                        defaultValue: true,
                        editable: false,
                        width: "100%",
                        bind: "{form_datasets.db_connection_r_id}",
                        editModeOnly: true
                    },
                    {
                        xtype: "combo",
                        fieldLabel: "DB-Verbindung (schreibend)",
                        id: "datasetdbconnectionwid",
                        store: "ConfigDbConnections",
                        queryMode: "local",
                        displayField: "name",
                        valueField: "id",
                        name: "db_connection_w_id",
                        labelWidth: 170,
                        hidden: true,
                        defaultValue: true,
                        editable: false,
                        width: "100%",
                        bind: "{form_datasets.db_connection_w_id}",
                        editModeOnly: true
                    },
                    {
                        fieldLabel: "Speicherort",
                        id: "datasetfilestore",
                        name: "file_store",
                        labelWidth: 170,
                        width: "100%",
                        bind: "{form_datasets.file_store}",
                        editModeOnly: true
                    },
                    {
                        xtype: "fieldset",
                        title: "Datenfreigabe",
                        padding: "0 5 0 5",
                        cls: "prio-fieldset",
                        collapsible: true,
                        collapsed: true,
                        width: "100%",
                        items: [
                            {
                                xtype: "fieldcontainer",
                                layout: "hbox",
                                scrollable: true,
                                items: [
                                    {
                                        id: "datasetfreigabelink",
                                        name: "freigabe_link",
                                        fieldLabel: "Link zur Freigabe",
                                        xtype: "textfield",
                                        margin: "0 10 10 0",
                                        labelWidth: 165,
                                        flex: 1,
                                        bind: "{form_datasets.freigabe_link}"
                                    },
                                    {
                                        xtype: "button",
                                        id: "datasetfreigabelinkcall",
                                        disabled: window.read_only,
                                        margin: "0 5 0 0",
                                        width: 50,
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "Freigabe aufrufen"
                                        },
                                        iconCls: "x-fa fa-external-link-alt",
                                        listeners: {
                                            click: "onCallFreigabenLink"
                                        }
                                    }
                                ]
                            },
                            {
                                id: "datasetorganisation",
                                name: "organisation",
                                fieldLabel: "Datenproduzierende Stelle (Behörde)",
                                xtype: "textfield",
                                labelWidth: 165,
                                width: "100%",
                                bind: "{form_datasets.organisation}"
                            },
                            {
                                id: "datasetdepartment",
                                name: "department",
                                fieldLabel: "Datenproduzierende Stelle (Abteilung)",
                                xtype: "textfield",
                                labelWidth: 165,
                                width: "100%",
                                bind: "{form_datasets.department}"
                            },
                            {
                                xtype: "fieldset",
                                title: "Berechtigungen",
                                padding: "5 5 5 5",
                                margin: "0 20 0 0",
                                cls: "prio-fieldset",
                                collapsible: true,
                                collapsed: true,
                                layout: "vbox",
                                width: "100%",
                                items: [
                                    {
                                        xtype: "component",
                                        cls: "x-form-item-label-default",
                                        html: "Personen:"
                                    },
                                    {
                                        xtype: "tagfield",
                                        id: "datasetcontacts",
                                        name: "contacts",
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "Auswahl über Dropddown"
                                        },
                                        store: "Contacts",
                                        displayField: "full_name",
                                        valueField: "id",
                                        forceSelection: false,
                                        filterPickList: true,
                                        width: "100%",
                                        queryMode: "local",
                                        bind: "{form_datasets.contacts}"
                                    },
                                    {
                                        xtype: "button",
                                        id: "opensetdatasetallowedgroupsbutton",
                                        margin: "10 0 5 0",
                                        width: 180,
                                        tooltip: "AD Gruppen berechtigen",
                                        text: "AD Gruppen berechtigen",
                                        listeners: {
                                            click: "onOpenSetAllowedGroups"
                                        }
                                    }
                                ]
                            },
                            {
                                xtype: "fieldcontainer",
                                layout: "hbox",
                                width: "100%",
                                labelWidth: 165,
                                fieldLabel: "Portalgebunden",
                                items: [
                                    {
                                        id: "datasetportalbound",
                                        name: "portal_bound",
                                        xtype: "checkboxfield",
                                        listeners: {
                                            change: "onPortalboundChecked"
                                        },
                                        margin: "10 0 0 00",
                                        bind: "{form_datasets.portal_bound}"
                                    },
                                    {
                                        xtype: "combobox",
                                        store: "Portals",
                                        valueField: "title",
                                        displayField: "title",
                                        queryMode: "local",
                                        fieldLabel: "Name des Portals",
                                        id: "datasetportalboundname",
                                        name: "portal_bound_name",
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "Name des Portals, an welches der Datensatz gebunden ist."
                                        },
                                        labelWidth: 100,
                                        flex: 1,
                                        margin: "10 0 0 20",
                                        hidden: true,
                                        editable: false,
                                        bind: "{form_datasets.portal_bound_name}"
                                    }
                                ]
                            }
                        ]
                    },
                    {
                        xtype: "fieldset",
                        title: "Metadaten",
                        padding: "0 5 0 5",
                        cls: "prio-fieldset-only-edit-mode",
                        collapsible: true,
                        collapsed: true,
                        items: [
                            {
                                fieldLabel: "ohne Katalogkopplung",
                                id: "datasetnocataloguelink",
                                name: "no_catalogue_link",
                                xtype: "checkboxfield",
                                labelWidth: 165,
                                width: "100%",
                                bind: "{form_datasets.no_catalogue_link}",
                                listeners: {
                                    change: "onNoCatalogueLinkChange"
                                },
                                editModeOnly: true
                            },
                            {
                                xtype: "textfield",
                                fieldLabel: "Metadaten UUID",
                                name: "md_id",
                                labelWidth: 165,
                                width: "100%",
                                id: "datasetmdid",
                                bind: "{form_datasets.md_id}",
                                readOnly: false,
                                editModeOnly: true
                            },
                            {
                                xtype: "textfield",
                                fieldLabel: "Ressource ID",
                                id: "datasetrsid",
                                name: "rs_id",
                                labelWidth: 165,
                                width: "100%",
                                bind: "{form_datasets.rs_id}",
                                readOnly: false,
                                editModeOnly: true
                            },
                            {
                                xtype: "fieldcontainer",
                                layout: "hbox",
                                id: "datasetmetadatacataloguefieldset",
                                width: "100%",
                                items: [
                                    {
                                        xtype: "combo",
                                        fieldLabel: "Quellkatalog",
                                        store: "ConfigMetadataCatalogs",
                                        displayField: "name",
                                        id: "datasetsourcecswname",
                                        name: "config_metadata_catalog_name",
                                        editable: false,
                                        margin: "0 10 0 0",
                                        labelWidth: 165,
                                        flex: 1,
                                        editModeOnly: true
                                    },
                                    {
                                        xtype: "button",
                                        id: "datasetsearchmetadata",
                                        disabled: window.read_only,
                                        margin: "0 10 0 0",
                                        width: 50,
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "Abfrage des Metadatenkatalogs via CSW, es wird nach dem eingegebenen Datensatz-Titel oder der eingegebenen Metadaten-ID gesucht"
                                        },
                                        iconCls: "x-fa fa-search",
                                        listeners: {
                                            click: "onSearchMetadata"
                                        }
                                    },
                                    {
                                        xtype: "button",
                                        id: "deletedatasetmetadatacoupling",
                                        width: 50,
                                        autoEl: {
                                            tag: "div",
                                            "data-qtip": "Löscht die Verknüpfung auf den Metadatensatz."
                                        },
                                        hidden: true,
                                        disabled: true,
                                        iconCls: "x-fa fa-times",
                                        listeners: {
                                            click: "onDeleteMetadataCouplingDataset"
                                        }
                                    },
                                    {
                                        xtype: "displayfield",
                                        id: "datasetmetadatalink",
                                        name: "metadata_link",
                                        value: "Keine Metadaten verknüpft",
                                        labelWidth: 0,
                                        width: 180,
                                        margin: "0 10 0 10"
                                    }
                                ]
                            },
                            {
                                xtype: "fieldset",
                                title: "Beschreibung",
                                padding: "5 5 0 5",
                                margin: "5 20 5 0",
                                cls: "prio-fieldset",
                                collapsible: true,
                                collapsed: true,
                                width: "100%",
                                items: [
                                    {
                                        id: "datasetdescription",
                                        name: "description",
                                        xtype: "textareafield",
                                        height: 120,
                                        labelWidth: 0,
                                        width: "100%",
                                        bind: "{form_datasets.description}",
                                        readOnly: false,
                                        editModeOnly: true
                                    }
                                ]
                            },
                            {
                                xtype: "fieldset",
                                title: "Schlagworte",
                                padding: "5 5 5 5",
                                margin: "5 20 5 0",
                                cls: "prio-fieldset",
                                collapsible: true,
                                collapsed: true,
                                layout: "fit",
                                width: "100%",
                                items: [{
                                    xtype: "tagfield",
                                    id: "datasetkeywords",
                                    name: "keywords",
                                    width: "100%",
                                    autoEl: {
                                        tag: "div",
                                        "data-qtip": "Auswahl über Dropddown, oder durch Eingabe, gefolgt von &quot;,&quot;"
                                    },
                                    store: "DatasetKeywords",
                                    displayField: "text",
                                    valueField: "text",
                                    forceSelection: false,
                                    queryMode: "local",
                                    createNewOnEnter: true,
                                    filterPickList: true,
                                    bind: "{form_datasets.keywords}",
                                    readOnly: false,
                                    editModeOnly: true
                                }]
                            },
                            {
                                xtype: "fieldset",
                                title: "Zugangsbeschränkungen",
                                margin: "5 20 5 0",
                                padding: "5 5 0 5",
                                cls: "prio-fieldset",
                                collapsible: true,
                                collapsed: true,
                                items: [
                                    {
                                        xtype: "textarea",
                                        id: "datasetaccessconstraints",
                                        name: "access_constraints",
                                        labelWidth: 0,
                                        height: 120,
                                        width: "100%",
                                        bind: "{form_datasets.access_constraints}",
                                        readOnly: false,
                                        editModeOnly: true
                                    }
                                ]
                            },
                            {
                                xtype: "fieldset",
                                title: "Nutzungsbedingungen",
                                margin: "5 20 5 0",
                                padding: "5 5 0 5",
                                cls: "prio-fieldset",
                                collapsible: true,
                                collapsed: true,
                                items: [
                                    {
                                        xtype: "textarea",
                                        id: "datasetfees",
                                        name: "fees",
                                        labelWidth: 0,
                                        height: 120,
                                        width: "100%",
                                        bind: "{form_datasets.fees}",
                                        readOnly: false,
                                        editModeOnly: true
                                    }
                                ]
                            },
                            {
                                xtype: "fieldset",
                                title: "Nutzungsbedingungen (JSON)",
                                margin: "5 20 5 0",
                                padding: "5 5 0 5",
                                cls: "prio-fieldset",
                                collapsible: true,
                                collapsed: true,
                                items: [
                                    {
                                        xtype: "textarea",
                                        id: "datasetfeesjson",
                                        name: "fees_json",
                                        labelWidth: 0,
                                        height: 120,
                                        width: "100%",
                                        bind: "{form_datasets.fees_json}",
                                        readOnly: false,
                                        editModeOnly: true
                                    }
                                ]
                            },
                            {
                                xtype: "fieldset",
                                title: "Kategorien",
                                margin: "5 20 5 0",
                                padding: "5 5 0 5",
                                cls: "prio-fieldset",
                                collapsible: true,
                                collapsed: true,
                                items: [
                                    {
                                        xtype: "textfield",
                                        fieldLabel: "HmbTG",
                                        id: "datasetcathmbtg",
                                        name: "cat_hmbtg",
                                        labelWidth: 165,
                                        width: "100%",
                                        bind: "{form_datasets.cat_hmbtg}",
                                        readOnly: false,
                                        editModeOnly: true
                                    },
                                    {
                                        xtype: "textfield",
                                        fieldLabel: "INSPIRE",
                                        id: "datasetcatinspire",
                                        name: "cat_inspire",
                                        labelWidth: 165,
                                        width: "100%",
                                        bind: "{form_datasets.cat_inspire}",
                                        readOnly: false,
                                        editModeOnly: true
                                    },
                                    {
                                        xtype: "textfield",
                                        fieldLabel: "OpenData",
                                        id: "datasetcatopendata",
                                        name: "cat_opendata",
                                        labelWidth: 165,
                                        width: "100%",
                                        bind: "{form_datasets.cat_opendata}",
                                        readOnly: false,
                                        editModeOnly: true
                                    },
                                    {
                                        xtype: "textfield",
                                        fieldLabel: "Organisationen",
                                        id: "datasetcatorg",
                                        name: "cat_org",
                                        labelWidth: 165,
                                        width: "100%",
                                        bind: "{form_datasets.cat_org}",
                                        readOnly: false,
                                        editModeOnly: true
                                    }
                                ]
                            },
                            {
                                xtype: "textfield",
                                fieldLabel: "Kontakt E-Mail-Adresse",
                                id: "datasetcontactmail",
                                name: "md_contact_mail",
                                labelWidth: 165,
                                width: "100%",
                                bind: "{form_datasets.md_contact_mail}",
                                readOnly: false,
                                editModeOnly: true
                            },
                            {
                                fieldLabel: "INSPIRE-identifiziert",
                                id: "datasetinspire",
                                name: "inspire",
                                xtype: "checkboxfield",
                                labelWidth: 165,
                                width: "100%",
                                bind: "{form_datasets.inspire}",
                                readOnly: false,
                                editModeOnly: true
                            }
                        ]
                    },
                    {
                        xtype: "fieldset",
                        title: "zusätzliche Informationen",
                        padding: "0 5 0 5",
                        cls: "prio-fieldset",
                        collapsible: true,
                        collapsed: true,
                        items: [{
                            xtype: "textareafield",
                            grow: false,
                            readOnly: false,
                            allowBlank: true,
                            id: "datasetadditionalinfo",
                            name: "additional_info",
                            height: 120,
                            width: "100%",
                            bind: "{form_datasets.additional_info}"
                        }]
                    }
                ]
            }
        ];

        this.dockedItems = [{
            xtype: "toolbar",
            dock: "bottom",
            border: true,
            style: "border-top: 1px solid #cfcfcf !important;",
            items: [
                {
                    xtype: "component",
                    html: "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px'>* </span><span style='color:#666'>Pflichtfeld</span>"
                },
                {
                    xtype: "component",
                    html: "<span style='color:grey;font-weight:bold;font-size:initial;padding-left:1px;'>* </span><span style='color:#666'>Standardwert</span>"
                },
                {
                    xtype: "component",
                    flex: 1,
                    html: ""
                },
                {
                    id: "savedatasetdetails",
                    text: "Speichern",
                    cls: "btn-save",
                    scale: "medium",
                    disabled: true,
                    listeners: {
                        click: "onSaveDatasetDetails"
                    }
                }
            ]
        }];

        this.callParent(arguments);
    }
});
