Ext.define("UDPManager.view.datasets.datasetdetails.DatasetDetailsController", {
    extend: "Ext.app.ViewController",

    alias: "controller.dataset_details",

    /**
     * function is called befor gui element is rendered.
     * listener is added to contacts tag field to open contact details window on double click.
     * mandatory form fields are marked with an asterisk.
     * form fields with default values are marked with an asterisk.
     * @returns {void}
     */
    beforeRender: function () {
        // funcionality for contacts
        // const contactsTagField = Ext.getCmp("datasetcontacts");

        // contactsTagField.selectionModel.on("selectionchange", function (model, selected) {
        //     let contactDetailsWindow = Ext.getCmp("contactdetails-window");

        //     if (selected[0] !== undefined) {
        //         const data = selected[0].data;

        //         if (contactDetailsWindow === undefined) {
        //             contactDetailsWindow = Ext.create("datasetdetails.contactdetails");
        //             contactDetailsWindow.show();
        //         }
        //         else {
        //             contactDetailsWindow.show();
        //         }

        //         Ext.getCmp("contactdetailsgivenname").setValue(data.given_name);
        //         Ext.getCmp("contactdetailssurname").setValue(data.surname);
        //         Ext.getCmp("contactdetailscompany").setValue(data.company);
        //         Ext.getCmp("contactdetailsemail").setValue(data.email);
        //         Ext.getCmp("contactdetailstel").setValue(data.tel);
        //         Ext.getCmp("contactdetailsadaccount").setValue(data.ad_account);
        //     }
        //     else {
        //         if (contactDetailsWindow) {
        //             contactDetailsWindow.close();
        //         }
        //     }
        // });

        // funcionality for mandatory fields and default values
        const mandatory_form_fields = Ext.ComponentQuery.query("form component[requiredField/='true']");
        const default_form_fields = Ext.ComponentQuery.query("form component[defaultValue/='true']");
        const mandatory_form_fields_id = mandatory_form_fields.map(field => field.id);
        const editorial_form_fields = Ext.ComponentQuery.query("form component[editModeOnly/='true']");

        editorial_form_fields.forEach(editorial_field => {
            Ext.getCmp(editorial_field.id).setUserCls("edit-mode-only");
        });

        mandatory_form_fields.forEach(mandatory_field => {
            Ext.getCmp(mandatory_field.id).afterLabelTextTpl = "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px' data-qtip='Pflichtfeld'>*</span>";
        });

        default_form_fields.forEach(default_field => {
            if (mandatory_form_fields_id.includes(default_field.id)) {
                Ext.getCmp(default_field.id).afterLabelTextTpl = "<span style='color:red;font-weight:bold;font-size:initial;padding-left:1px'>*</span><span style='color:grey;font-weight:bold;font-size:initial;padding-left:1px'>*</span>";
            }
            else {
                Ext.getCmp(default_field.id).afterLabelTextTpl = "<span style='color:grey;font-weight:bold;font-size:initial;padding-left:1px' data-qtip='Standardwert'>*</span>";
            }
        });
    },

    onSaveDatasetDetails: function () {
        const me = this;
        const fieldvalues = Ext.getCmp("datasetdetailsform").getForm().getFieldValues();

        if ((fieldvalues.status === "veröffentlicht" || fieldvalues.status === "veröffentlicht - vorveröffentlicht" || fieldvalues.status === "vorveröffentlicht") && !Ext.getCmp("datasetexternal").getValue()) {
            this.saveEditorial();
        }
        else {
            // shortname validation
            fieldvalues.shortname = fieldvalues.shortname.replace(/ü/gi, "ue");
            fieldvalues.shortname = fieldvalues.shortname.replace(/ö/gi, "oe");
            fieldvalues.shortname = fieldvalues.shortname.replace(/ä/gi, "ae");
            fieldvalues.shortname = fieldvalues.shortname.replace(/ß/gi, "ss");
            fieldvalues.shortname = fieldvalues.shortname.replace(/ /gi, "_");

            if (fieldvalues.external) {
                fieldvalues.status = "veröffentlicht";
            }

            Ext.getCmp("datasetshortname").setValue(fieldvalues.shortname);

            const unique_shortname_test = Ext.getStore("Datasets").findRecord("shortname", fieldvalues.shortname, 0, false, false, true);
            let db_connections_valid = true;

            if (fieldvalues.db_connection_r_id && fieldvalues.db_connection_w_id) {
                const db_connection_r = Ext.getStore("ConfigDbConnections").findRecord("id", fieldvalues.db_connection_r_id, 0, false, false, true);
                const db_connection_w = Ext.getStore("ConfigDbConnections").findRecord("id", fieldvalues.db_connection_w_id, 0, false, false, true);

                if (db_connection_r.data.host !== db_connection_w.data.host || db_connection_r.data.port !== db_connection_w.data.port || db_connection_r.data.database !== db_connection_w.data.database) {
                    db_connections_valid = false;
                }
            }

            if (fieldvalues.shortname === "") {
                Ext.MessageBox.alert("Fehler", "Es muss ein Kurzname vergeben werden!");
            }
            else if (unique_shortname_test && unique_shortname_test.data.id !== fieldvalues.id) {
                Ext.MessageBox.alert("Fehler", "Kurzname bereits vergeben!");
            }
            else if (!db_connections_valid) {
                Ext.MessageBox.alert("Fehler", "Die lesende und schreibende DB Connection zeigen auf unterschiedliche Datenbanken!");
            }
            else if (fieldvalues.shortname.match(/^\d/)) {
                Ext.MessageBox.alert("Fehler", "Der Kurzname darf nicht mit einer Zahl beginnen!");
            }
            else if (fieldvalues.shortname.match(/[^a-z0-9_]/)) {
                Ext.MessageBox.alert("Fehler", "Mit Ausnahme von UNDERSCORE sind Sonderzeichen und Großschreibung nicht zulässig. Um mehrere Wörter zu verbinden bitte Snakecase benutzen!");
            }
            else if (fieldvalues.shortname.length > 60) {
                Ext.MessageBox.alert("Fehler", "Der Kurzname darf nicht länger als 60 Zeichen sein!");
            }
            else if (fieldvalues.shortname[0] === "_") {
                Ext.MessageBox.alert("Fehler", "Der Kurzname darf nicht mit einem UNDERSCORE beginnen!");
            }
            else if (fieldvalues.srs.indexOf("EPSG:") === -1 && fieldvalues.srs.length > 0) {
                Ext.MessageBox.alert("Fehler", "Koordinatenrefrenzsystem muss in der Form EPSG:{Nummer} angegeben werden");
            }
            else if (fieldvalues.portal_bound && fieldvalues.portal_bound_name === null) {
                Ext.MessageBox.alert("Hinweis", "Bitte ein Portal auswählen, an welches dieser Datensatz gebunden ist.");
            }
            else if (!UDPManager.app.getController("UDPManager.controller.PublicFunctions").isJsonString(fieldvalues.fees_json) && fieldvalues.fees_json !== "") {
                Ext.MessageBox.alert("Fehler", "Nutzungsbedingungen (JSON) kein valider JSON String");
            }
            else if (!fieldvalues.responsible_party) {
                Ext.MessageBox.alert("Fehler", "Für den Datensatz muss eine verantwortliche Stelle eingetragen sein!");
            }
            else if (!fieldvalues.restriction_level) {
                Ext.MessageBox.alert("Fehler", "Für den Datensatz muss eine Freigabebene ausgewählt sein!");
            }
            else {
                const source_csw_selection = Ext.getCmp("datasetsourcecswname").getSelection();

                fieldvalues.metadata_catalog_id = null;
                fieldvalues.last_edit_date = moment().format("YYYY-MM-DD HH:mm");
                fieldvalues.last_edited_by = window.auth_user;

                if (fieldvalues.id === 0) {
                    fieldvalues.create_date = moment().format("YYYY-MM-DD HH:mm");
                    fieldvalues.created_by = window.auth_user;
                }
                else {
                    const d = new Date(fieldvalues.create_date);

                    fieldvalues.create_date = moment(d).format("YYYY-MM-DD HH:mm");
                }

                if (source_csw_selection) {
                    fieldvalues.metadata_catalog_id = source_csw_selection.data.id;
                }

                Ext.getCmp("savedatasetdetails").setDisabled(true);

                Ext.Ajax.request({
                    url: "backend/savedataset",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: fieldvalues,
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);

                        if (response_json.status === "success") {
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Datensatz erfolgreich gespeichert!");

                            me.saveContacts(fieldvalues.id);

                            Ext.getStore("Datasets").load({
                                callback: function () {
                                    const dataset = Ext.getStore("Datasets").findRecord("id", response_json.id, 0, false, false, true);
                                    const dataset_grid = Ext.getCmp("grid_dataset");
                                    const row_dataset = dataset_grid.store.indexOf(dataset);

                                    dataset_grid.getSelectionModel().deselectAll();
                                    dataset_grid.getSelectionModel().select(row_dataset);
                                    dataset_grid.ensureVisible(row_dataset);
                                }
                            });

                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").onRecreateJson("savedatasetdetails", "dev");
                            Ext.getStore("DatasetKeywords").load();
                            Ext.getStore("DatasetFilterKeywords").load();

                            if (UDPManager.Configs.getWebsocket()) {
                                window.socket.emit("dataset_details_changed");
                            }

                            Ext.getCmp("datasetcollectionstab").setDisabled(false);
                            Ext.getCmp("datasetservicestab").setDisabled(false);
                            Ext.getCmp("datasetetltab").setDisabled(false);
                            Ext.getCmp("datasetcommentstab").setDisabled(false);

                            if (UDPManager.Configs.getModules().jiraTickets) {
                                Ext.getCmp("datasetjiratab").setDisabled(false);
                            }
                        }
                        else {
                            if (response_json.message.indexOf("unique_datasets_shortname") > -1) {
                                Ext.MessageBox.alert("Fehler", "Kurzname bereits bei einem anderen Datensatz vergeben!");
                            }
                            else {
                                Ext.MessageBox.alert("Fehler", response_json.message);
                            }
                        }
                        Ext.getCmp("savedatasetdetails").setDisabled(false);

                        if (fieldvalues.metadata_catalog_id !== null) {
                            Ext.getCmp("datasetrsid").setReadOnly(true);
                            Ext.getCmp("datasetdescription").setReadOnly(true);
                            Ext.getCmp("datasetkeywords").setReadOnly(true);
                            Ext.getCmp("datasetaccessconstraints").setReadOnly(true);
                            Ext.getCmp("datasetfees").setReadOnly(true);
                            Ext.getCmp("datasetfeesjson").setReadOnly(true);
                            Ext.getCmp("datasetcathmbtg").setReadOnly(true);
                            Ext.getCmp("datasetcatinspire").setReadOnly(true);
                            Ext.getCmp("datasetcatopendata").setReadOnly(true);
                            Ext.getCmp("datasetcatorg").setReadOnly(true);
                            Ext.getCmp("datasetinspire").setReadOnly(true);
                            Ext.getCmp("datasetcontactmail").setReadOnly(true);
                        }
                        else {
                            Ext.getCmp("datasetrsid").setReadOnly(false);
                            Ext.getCmp("datasetdescription").setReadOnly(false);
                            Ext.getCmp("datasetkeywords").setReadOnly(false);
                            Ext.getCmp("datasetaccessconstraints").setReadOnly(false);
                            Ext.getCmp("datasetfees").setReadOnly(false);
                            Ext.getCmp("datasetfeesjson").setReadOnly(false);
                            Ext.getCmp("datasetcathmbtg").setReadOnly(false);
                            Ext.getCmp("datasetcatinspire").setReadOnly(false);
                            Ext.getCmp("datasetcatopendata").setReadOnly(false);
                            Ext.getCmp("datasetcatorg").setReadOnly(false);
                            Ext.getCmp("datasetinspire").setReadOnly(false);
                            Ext.getCmp("datasetcontactmail").setReadOnly(false);
                        }
                    },
                    failure: function (response) {
                        Ext.getCmp("savedatasetdetails").setDisabled(false);
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    }
                });
            }
        }
    },

    saveEditorial: function () {
        const fieldvalues = Ext.getCmp("datasetdetailsform").getForm().getFieldValues();
        const me = this;

        fieldvalues.last_edited_by = window.auth_user;

        Ext.Ajax.request({
            url: "backend/savedataseteditorial",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: fieldvalues,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Datensatz erfolgreich gespeichert!");

                    me.saveContacts(fieldvalues.id);

                    Ext.getStore("Datasets").load({
                        callback: function () {
                            const dataset = Ext.getStore("Datasets").findRecord("id", response_json.id, 0, false, false, true);
                            const dataset_grid = Ext.getCmp("grid_dataset");
                            const row_dataset = dataset_grid.store.indexOf(dataset);

                            dataset_grid.getSelectionModel().deselectAll();
                            dataset_grid.getSelectionModel().select(row_dataset);
                            dataset_grid.ensureVisible(row_dataset);
                        }
                    });
                    Ext.getStore("DatasetKeywords").load();
                    Ext.getStore("DatasetFilterKeywords").load();

                    if (UDPManager.Configs.getWebsocket()) {
                        window.socket.emit("dataset_details_changed");
                    }

                    Ext.getCmp("datasetcollectionstab").setDisabled(false);
                    Ext.getCmp("datasetservicestab").setDisabled(false);
                    Ext.getCmp("datasetetltab").setDisabled(false);
                    Ext.getCmp("datasetcommentstab").setDisabled(false);
                    if (UDPManager.Configs.getModules().jiraTickets) {
                        Ext.getCmp("datasetjiratab").setDisabled(false);
                    }
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
                Ext.getCmp("savedatasetdetails").setDisabled(false);
            },
            failure: function (response) {
                Ext.getCmp("savedatasetdetails").setDisabled(false);
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
            }
        });
    },

    /**
     * function is called after dataset details are saved successful.
     * Contact ID from contacts tag field records are extracted und combined with dataset ID posted to backend where these params are written into contact_links table.
     * @param {Integer} dataset_id - id of selected dataset
     * @returns {void}
     */
    saveContacts: function (dataset_id) {
        const contacts_records = Ext.getCmp("datasetcontacts").getValueRecords();
        const contacts = [];

        for (let i = 0; i < contacts_records.length; i++) {
            contacts.push({contact_id: contacts_records[i].data.id});
        }

        Ext.Ajax.request({
            url: "backend/updatecontactlinks",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                dataset_id: dataset_id,
                contacts: contacts
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
            }
        });
    },

    changeStatusColor: function (e) {
        if (e.rawValue === "veröffentlicht") {
            Ext.getCmp("datasetstatus").setFieldStyle("background-color:#A8D44D;");
        }
        else if (e.rawValue === "in Bearbeitung") {
            Ext.getCmp("datasetstatus").setFieldStyle("background-color:#FFA500;");
        }
        else {
            Ext.getCmp("datasetstatus").setFieldStyle("background-color:#FAD801;");
        }
    },

    onSearchMetadata: function (btn) {
        const source_csw_selection = Ext.getCmp("datasetsourcecswname").getSelection();
        const datasettitle = Ext.getCmp("datasettitle").getSubmitValue();
        const datasetmdid = Ext.getCmp("datasetmdid").getSubmitValue();

        if (source_csw_selection) {
            btn.setDisabled(true);

            const loadingsMask = new Ext.LoadMask({
                msg: "Bitte warten...",
                target: Ext.getCmp("dataset_main_tabs")
            });

            loadingsMask.show();

            Ext.Ajax.request({
                url: "backend/getdatasetmetadata",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    datasettitle: datasettitle,
                    datasetmdid: datasetmdid,
                    metadata_catalog_id: source_csw_selection.data.id
                },
                success: function (response) {
                    const results = Ext.decode(response.responseText);

                    btn.setDisabled(false);

                    if (!results.error) {
                        if (results.results.length > 0) {
                            const store = Ext.getStore("MetadataSets");
                            const linkMetadataWindow = Ext.getCmp("linkmetadata-window");

                            store.removeAll();
                            store.add(results.results);

                            if (!linkMetadataWindow) {
                                Ext.create("datasets.metadata.linkmetadata").showWindow("dataset");
                            }
                            else {
                                linkMetadataWindow.showWindow("dataset");
                            }
                        }
                        else {
                            Ext.MessageBox.alert("Status", "Keine Treffer im " + source_csw_selection.data.name);
                        }
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", "Fehler beim Abruf des Katalogs " + source_csw_selection.data.name);
                    }

                    loadingsMask.hide();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Fehler beim Abruf des Katalogs " + source_csw_selection.data.name);
                    btn.setDisabled(false);
                    loadingsMask.hide();
                }
            });
        }
    },

    onStoreTypeChange: function (el, newValue) {
        if (newValue === "Vektor") {
            Ext.getCmp("datasetdbconnectionrid").setHidden(false);
            Ext.getCmp("datasetdbconnectionwid").setHidden(false);
            Ext.getCmp("datasetfilestore").setHidden(true);
            Ext.getCmp("datasetfilestore").setValue("");
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(false);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(false);
            Ext.getCmp("datasetcollectiontileindex").setHidden(true);
            Ext.getCmp("datasetcollectiontileindex").setValue("");
            Ext.getCmp("datasetcollectionnamespace").setHidden(false);
            Ext.getCmp("datasetcollectioncomplexscheme").setHidden(false);
            Ext.getCmp("datasetcollectionstoretype").setHidden(true);
            Ext.getCmp("datasetcollectionstoretype").setValue(null);
        }
        else if (newValue === "Raster") {
            Ext.getCmp("datasetdbconnectionrid").setHidden(true);
            Ext.getCmp("datasetdbconnectionrid").setValue(null);
            Ext.getCmp("datasetdbconnectionwid").setHidden(true);
            Ext.getCmp("datasetdbconnectionwid").setValue(null);
            Ext.getCmp("datasetfilestore").setHidden(false);
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(true);
            Ext.getCmp("datasetcollectiondbschema").setValue(null);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(true);
            Ext.getCmp("datasetcollectiondbtable").setValue(null);
            Ext.getCmp("editattributeconfig").setDisabled(true);
            Ext.getCmp("editstyle").setDisabled(true);
            Ext.getCmp("editstylesldfile").setDisabled(true);
            Ext.getCmp("datasetcollectiontileindex").setHidden(false);
            Ext.getCmp("datasetcollectionnamespace").setHidden(true);
            Ext.getCmp("datasetcollectioncomplexscheme").setHidden(true);
            Ext.getCmp("datasetcollectionstoretype").setHidden(true);
            Ext.getCmp("datasetcollectionstoretype").setValue(null);
        }
        else if (newValue === "Beides") {
            Ext.getCmp("datasetdbconnectionrid").setHidden(false);
            Ext.getCmp("datasetdbconnectionwid").setHidden(false);
            Ext.getCmp("datasetfilestore").setHidden(false);
            Ext.getCmp("editattributeconfig").setDisabled(true);
            Ext.getCmp("editstyle").setDisabled(true);
            Ext.getCmp("editstylesldfile").setDisabled(true);
            Ext.getCmp("datasetcollectiontileindex").setHidden(true);
            Ext.getCmp("datasetcollectionnamespace").setHidden(true);
            Ext.getCmp("datasetcollectioncomplexscheme").setHidden(true);
            Ext.getCmp("datasetcollectiondbschemacontainer").setHidden(true);
            Ext.getCmp("datasetcollectiondbtablecontainer").setHidden(true);
            Ext.getCmp("datasetcollectionstoretype").setHidden(false);
        }
    },

    onExternalChange: function (checkbox, newValue) {
        const datastoretype = Ext.getCmp("datasetstoretype").getValue();

        if (newValue) {
            Ext.getCmp("datasetdbconnectionrid").setHidden(true);
            Ext.getCmp("datasetdbconnectionwid").setHidden(true);
            Ext.getCmp("datasetfilestore").setHidden(true);
            Ext.getCmp("datasetstoretype").setHidden(true);
        }
        else {
            Ext.getCmp("datasetstoretype").setHidden(false);
            this.onStoreTypeChange(null, datastoretype);
        }
    },

    onRestrictedAccessChange: function (cb, newValue) {
        if (newValue) {
            Ext.getCmp("datasetrestrictiontype").setHidden(false);
            Ext.getCmp("datasetrestrictionreasonlabel").setHidden(false);
            Ext.getCmp("datasetrestrictionreason").setHidden(false);
        }
        else {
            Ext.getCmp("datasetrestrictiontype").setHidden(true);
            Ext.getCmp("datasetrestrictionreasonlabel").setHidden(true);
            Ext.getCmp("datasetrestrictionreason").setHidden(true);
        }
    },

    onCallFreigabenLink: function () {
        const freigaben_link = Ext.getCmp("datasetfreigabelink").getValue();

        window.open(freigaben_link, "_blank");
    },

    onDeleteMetadataCouplingDataset: function (btn) {
        UDPManager.app.getController("UDPManager.controller.PublicFunctions").onDeleteMetadataCoupling(btn);
    },

    onNoCatalogueLinkChange: function (checkbox) {
        if (checkbox.value) {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").onDeleteMetadataCoupling(Ext.getCmp("deletedatasetmetadatacoupling"));
            Ext.getCmp("datasetmdid").setHidden(true);
            Ext.getCmp("datasetrsid").setHidden(true);
            Ext.getCmp("datasetmetadatacataloguefieldset").setHidden(true);
            Ext.getCmp("datasetmetadatalink").setHidden(true);
        }
        else {
            Ext.getCmp("datasetmdid").setHidden(false);
            Ext.getCmp("datasetrsid").setHidden(false);
            Ext.getCmp("datasetmetadatacataloguefieldset").setHidden(false);
            Ext.getCmp("datasetmetadatalink").setHidden(false);
        }
    },

    onPortalboundChecked: function (checkbox) {
        if (checkbox.checked) {
            Ext.getCmp("datasetportalboundname").setHidden(false);
        }
        else {
            Ext.getCmp("datasetportalboundname").setHidden(true);
            Ext.getCmp("datasetportalboundname").setValue(null);
        }
    },

    onOpenSetAllowedGroups: function () {
        const allowed_groups_store = Ext.getStore("AllowedADGroups");
        const ad_groups_store = Ext.getStore("ADGroups");
        const dataset = Ext.getStore("Datasets").query("id", Ext.getCmp("datasetid").getSubmitValue(), false, false, true).items[0].data;

        allowed_groups_store.removeAll();
        ad_groups_store.removeAll();

        if (dataset.allowed_groups !== "" && dataset.allowed_groups) {
            for (let i = 0; i < dataset.allowed_groups.length; i++) {
                allowed_groups_store.add({name: dataset.allowed_groups[i]});
            }
        }

        let allowedGroupsChooserWindow = Ext.getCmp("allowedgroupschooser-window");

        if (!allowedGroupsChooserWindow) {
            allowedGroupsChooserWindow = Ext.create("allowedgroupschooser.AllowedGroupsChooser");
        }
        allowedGroupsChooserWindow.show();

        Ext.getCmp("savedatasetallowedgroupsbutton").setHidden(false);
    },

    onChangeLandlevel: function () {
        Ext.getCmp("datasetdistrict").setValue("");
    },

    onSetDatasetdistrictStore: function () {
        const regions_obj = UDPManager.Configs.getRegions();
        const chosen_landlevel = Ext.getCmp("datasetfederalstate").getValue();
        let area_items;

        for (const [key, value] of Object.entries(regions_obj)) {
            if (value.landlevel === chosen_landlevel) {
                area_items = value.arealevel;
            }
        }

        Ext.getCmp("datasetdistrict").setStore(area_items);
    },

    onOpenDatasetHelpLink: function () {
        window.open(UDPManager.Configs.getHelpLink() + "dataset/", "_blank");
    }
});
