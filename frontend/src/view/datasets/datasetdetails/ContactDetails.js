Ext.define("UDPManager.view.datasets.datasetdetails.ContactDetails", {
    extend: "Ext.window.Window",
    id: "contactdetails-window",
    alias: "datasetdetails.contactdetails",
    height: 250,
    width: 450,

    title: "Kontaktdetails",

    alwaysOnTop: true,

    items: [
        {
            xtype: "fieldset",
            title: "Angaben",
            margin: "5 5 5 5",
            layout: "anchor",
            defaultType: "textfield",

            items: [
                {
                    fieldLabel: "Vorname",
                    id: "contactdetailsgivenname",
                    readOnly: true,
                    width: 400
                }, {
                    fieldLabel: "Nachname",
                    id: "contactdetailssurname",
                    readOnly: true,
                    width: 400
                }, {
                    fieldLabel: "Unternehmen",
                    id: "contactdetailscompany",
                    readOnly: true,
                    width: 400
                }, {
                    fieldLabel: "E-Mail",
                    id: "contactdetailsemail",
                    readOnly: true,
                    width: 400
                }, {
                    fieldLabel: "Telefonnummer",
                    id: "contactdetailstel",
                    readOnly: true,
                    width: 400
                }, {
                    fieldLabel: "AD Account",
                    id: "contactdetailsadaccount",
                    readOnly: true,
                    width: 400
                }
            ]
        }
    ]
});
