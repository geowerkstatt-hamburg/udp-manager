Ext.define("UDPManager.view.main.Main", {
    extend: "Ext.container.Viewport",
    xtype: "mainviewport",
    id: "mainviewport",
    requires: [
        "*"
    ],
    controller: "main",
    layout: {
        type: "vbox",
        align: "stretch"
    },
    listeners: {
        render: "onMainViewRender"
    },
    items: [
        {
            xtype: "toolbar",
            cls: "udpm-dash-dash-headerbar toolbar-btn-shadow",
            height: 70,
            itemId: "headerBar",
            items: [
                {
                    xtype: "component",
                    style: "background-color: #005ca9; filter: drop-shadow(3px 5px 2px rgb(255 255 255 / 0.4));",
                    width: 180,
                    height: 70
                },
                {
                    xtype: "image",
                    reference: "udpmLogo",
                    src: "resources/images/logo_udpm.svg",
                    centered: true,
                    width: 140,
                    height: 70
                },
                {
                    xtype: "udpsearchfield",
                    id: "searchInput",
                    width: 260,
                    margin: "0 5px 0 15px",
                    height: 49,
                    storeId: "Datasets"
                },
                {
                    xtype: "tbspacer",
                    width: 20
                },
                {
                    id: "startdatasetediting",
                    text: "Datensatz aktualisieren",
                    cls: "btn-save",
                    scale: "medium",
                    disabled: true,
                    listeners: {
                        click: "onStartDatasetEditing"
                    }
                },
                {
                    xtype: "tbspacer",
                    flex: 1
                },
                {
                    iconCls: "x-fa fa-question header_link",
                    id: "helplinkbutton",
                    ui: "header",
                    href: "",
                    hidden: true,
                    tooltip: "Handbuch"
                },
                {
                    xtype: "tbspacer",
                    width: 20
                },
                {
                    xtype: "component",
                    id: "versionField",
                    html: ""
                },
                {
                    xtype: "tbspacer",
                    width: 20
                },
                {
                    xtype: "component",
                    id: "userField",
                    html: ""
                },
                {
                    xtype: "tbspacer",
                    width: 20
                },
                {
                    xtype: "component",
                    id: "logoutLink",
                    hidden: true,
                    html: "<a href='logout'>Logout</a>"
                }
            ]
        },
        {
            xtype: "tabpanel",
            id: "main-nav-tab-panel",
            ui: "navigation",
            tabPosition: "left",
            tabRotation: 0,
            height: Ext.Element.getViewportHeight() - 70,
            tabBar: {
                flex: 1,
                layout: {
                    align: "stretch",
                    overflowHandler: "none"
                }
            },
            defaults: {
                tabConfig: {
                    width: 180
                }
            },
            listeners: {
                tabchange: "onTabChange"
            },
            items: [{
                title: "Datensätze",
                id: "datasetstab",
                iconCls: "x-fa fa-database",
                layout: {
                    type: "hbox",
                    pack: "start",
                    align: "stretch"
                },
                storeId: "Datasets",
                items: [
                    {
                        xtype: "datasetlist",
                        id: "datasetlist-id",
                        width: "100%"
                    }
                ]
            },
            {
                title: "Collections",
                id: "collectionstab",
                iconCls: "x-fa fa-folder-open",
                storeId: "Collections",
                items: [
                    {
                        xtype: "collectionlist"
                    }
                ]
            },
            {
                title: "Schnittstellen",
                id: "servicestab",
                iconCls: "x-fa fa-share-alt",
                storeId: "Services",
                items: [
                    {
                        xtype: "servicelist",
                        id: "serviceslist"
                    }
                ]
            },
            {
                title: "Layer",
                id: "layerstab",
                iconCls: "x-fa fa-layer-group",
                storeId: "Layers",
                items: [
                    {
                        xtype: "layerlist"
                    }
                ]
            },
            {
                title: "Portale",
                id: "portalstab",
                iconCls: "x-fa fa-map-marked-alt",
                storeId: "Portals",
                items: [
                    {
                        xtype: "portallist"
                    }
                ]
            },
            {
                title: "Statistiken",
                id: "statisticstab",
                iconCls: "x-fa fa-chart-bar",
                items: [
                    {
                        xtype: "statistics"
                    }
                ]
            },
            {
                title: "Verwaltung",
                id: "managementtab",
                iconCls: "x-fa fa-cog",
                items: [
                    {
                        xtype: "managementpanel",
                        margin: "0 10 0 0"
                    }
                ]
            }]
        }
    ]
});
