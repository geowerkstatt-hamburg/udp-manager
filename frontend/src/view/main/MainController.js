/**
 * This class is the controller for the main view for the application. It is specified as
 * the "controller" of the Main view class.
 */
Ext.define("UDPManager.view.main.MainController", {
    extend: "Ext.app.ViewController",
    alias: "controller.main",

    /**
     * if dataset_id is passed as get parameter, select the dataset with that ID
     * @returns {void}
     */
    onMainViewRender: function () {
        let params = {};

        const datasetStore = Ext.getStore("Datasets");

        if (window.location.href.indexOf("?") > 0) {
            params = Ext.Object.fromQueryString(window.location.href.split("?")[1]);
        }

        if (params.dataset_id) {
            datasetStore.on("load", function () {
                const dataset = datasetStore.findRecord("id", params.dataset_id, 0, false, false, true);

                if (dataset) {
                    const dataset_grid = Ext.getCmp("grid_dataset");
                    const row_dataset = dataset_grid.store.indexOf(dataset);

                    Ext.getCmp("main-nav-tab-panel").setActiveTab(0);

                    dataset_grid.getSelectionModel().deselectAll();
                    dataset_grid.getSelectionModel().select(row_dataset);
                    dataset_grid.ensureVisible(row_dataset);

                    // Functionality to select Service or Collection if ID is passed in URL is handled in DatasetListController in Store callback functions.
                }
            }, this, {single: true});
        }
    },

    /**
     * set search field store, if clicked tab has storeId
     * @param {Collection} tabs - Array with all tab objects from main navigation
     * @param {Object} newTab - object of clicked tab
     * @returns {void}
     */
    onTabChange: function (tabs, newTab) {
        if (newTab.storeId) {
            Ext.getCmp("searchInput").onClearClick();
            Ext.getCmp("searchInput").setDisabled(false);
            Ext.getCmp("searchInput").storeId = newTab.storeId;
            Ext.getStore(newTab.storeId).load();
        }
        else {
            Ext.getCmp("searchInput").setDisabled(true);
        }

        if (newTab.id !== "staticticstab") {
            Ext.getStore("LayerStats").load({
                params: {scope: "all"},
                callback: function (records) {
                    let total_layer_count = 0;

                    for (let i = 0; i < records.length; i++) {
                        total_layer_count += records[i].data.count;
                    }
                    Ext.getCmp("layerStatisticsChart").setConfig("title", "Anzahl Layer / FeatureTypes - gesamt: " + total_layer_count);
                }
            });
            Ext.getStore("ServiceStats").load({
                params: {scope: "all"},
                callback: function (records) {
                    let total_service_count = 0;

                    for (let i = 0; i < records.length; i++) {
                        total_service_count += records[i].data.count;
                    }
                    Ext.getCmp("serviceStatisticsChart").setConfig("title", "Diensttypen - gesamt: " + total_service_count);
                }
            });
            Ext.getStore("SoftwareStats").load({
                params: {scope: "all"},
                callback: function (records) {
                    let total_software_count = 0;

                    for (let i = 0; i < records.length; i++) {
                        total_software_count += records[i].data.count;
                    }
                    Ext.getCmp("softwareStatisticsChart").setConfig("title", "Eingesetzte Software - gesamt: " + total_software_count);
                }
            });

            if (UDPManager.Configs.getModules().visitStatistics) {
                Ext.getStore("VisitsTop").load({
                    params: {month: moment().format("YYYY-MM"), software: "Alle"}
                });
                Ext.getStore("VisitsTotal").load();

                Ext.Ajax.request({
                    url: "backend/getdistinctmonth",
                    method: "GET",
                    headers: {token: window.apiToken},
                    success: function (response) {
                        const response_json = Ext.decode(response.responseText);
                        const month = [];

                        for (let i = 0; i < response_json.length; i++) {
                            month.push(response_json[i].month);
                        }
                        if (month.length > 0) {
                            Ext.getCmp("chooseTopVisitsMonth").setStore(month);
                        }
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                    }
                });
            }
        }

        if (newTab.id !== "datasetstab") {
            Ext.getCmp("dataset_main_tabs").setActiveTab(0);
            Ext.getCmp("filterkeywords").setDisabled(true);
            Ext.getCmp("startdatasetediting").setHidden(true);
        }
        else {
            Ext.getCmp("filterkeywords").setDisabled(false);
            Ext.getCmp("dataset_main_tabs").setActiveTab(0);
            Ext.getCmp("startdatasetediting").setHidden(false);
        }

        // clear filter from contacts store. Filter is set, when contacts are mapped in dataset details.
        Ext.getStore("Contacts").clearFilter();
    },

    /**
     * add keyword filter to dataset store, when keyword is chosen from combo
     * @returns {void}
     */
    onKeywordFilter: function () {
        const keywords_selected = Ext.getCmp("filterkeywords").getValueRecords();
        const keywords = [];

        for (let i = 0; i < keywords_selected.length; i++) {
            keywords.push(keywords_selected[i].data.text);
        }

        Ext.getStore("Datasets").removeFilter("keywordfilter");

        if (keywords.length > 0) {
            Ext.getStore("Datasets").addFilter(new Ext.util.Filter({
                id: "keywordfilter",
                filterFn: function (item) {
                    if (item.get("filter_keywords")) {
                        return keywords.filter(value => item.get("filter_keywords").includes(value)).length > 0;
                    }
                    else {
                        return false;
                    }
                }
            }));
        }
    },

    onStartDatasetEditing: function (btn) {
        const externalDataset = Ext.getCmp("datasetexternal").getValue();
        let allServicesExternal = true;
        const servicesToDeploy = {};
        const dataset_id = Ext.getCmp("datasetid").getSubmitValue();

        Ext.getStore("ServicesDataset").each(function (service) {
            if (!service.data.external) {
                allServicesExternal = false;
            }
            else {
                servicesToDeploy[service.data.id] = {
                    service_id: service.data.id,
                    title: service.data.title,
                    src: "prod",
                    dest: "dev",
                    external: true,
                    status_dev: service.data.status_dev,
                    status_stage: service.data.status_stage,
                    status_prod: service.data.status_prod,
                    dataset_id: parseInt(dataset_id),
                    last_edited_by: window.auth_user
                };
            }
        });

        if (!externalDataset && allServicesExternal) {
            Ext.Ajax.request({
                url: "backend/deployserviceconfig",
                method: "POST",
                headers: {token: window.apiToken},
                timeout: 120000,
                jsonData: servicesToDeploy,
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    Ext.getCmp("datasetstatus").setValue(response_json.newStatus);

                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast(`Bearbeitung gestartet!`);
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").setAllFormsWritable();
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                },
                failure: function (response) {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
                    Ext.Msg.alert("Fehler", "Die Schnittstellenkonfiguration konnte nicht verschoben werden!");
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else {
            // create Workspace Chooser, if only one workspace for dev is available, don't show window and execute move to dev automatically
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").openWorkspaceChooserWindow(btn);
        }
    }
});
