Ext.define("UDPManager.view.userchooser.UserChooser", {
    extend: "Ext.window.Window",
    id: "userchooser-window",
    alias: "userchooser.UserChooser",
    listeners: {
        beforeRender: "beforeRender"
    },
    controller: "userchooser",
    height: 200,
    width: 500,
    title: "Bearbeiter Wählen",
    closable: false,
    modal: true,
    closeAction: "hide",
    items: [
        {
            xtype: "container",
            padding: "20 20 20 20",
            autoScroll: true,
            items: [{
                xtype: "label",
                id: "userchooser_label",
                text: "Bitte wählen Sie einen Bearbeiter, unter dessen Namen die Änderungen gespeichert werden"
            }, {
                xtype: "combo",
                fieldLabel: "Bearbeiter",
                allowBlank: false,
                id: "userchooser_combo",
                labelWidth: 160,
                width: 450
            }]
        }
    ],
    buttons: [
        {
            text: "OK",
            listeners: {
                click: "onUserChooserOk"
            }
        }
    ]
});
