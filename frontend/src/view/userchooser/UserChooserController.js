Ext.define("UDPManager.view.userchooser.UserChooserController", {
    extend: "Ext.app.ViewController",
    alias: "controller.userchooser",

    onUserChooserOk: function () {
        if (Ext.getCmp("userchooser_combo").getSubmitValue() !== "") {
            window.auth_user = Ext.getCmp("userchooser_combo").getSubmitValue();
            Ext.getCmp("userchooser-window").hide();
        }
    },

    beforeRender: function () {
        const users = UDPManager.Configs.getUser();
        const combo = Ext.getCmp("userchooser_combo");

        combo.setStore(users);
        combo.select(combo.getStore().getAt(0));
    }
});
