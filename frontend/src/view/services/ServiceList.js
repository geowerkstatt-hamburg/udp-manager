Ext.define("UDPManager.view.services.ServiceList", {
    extend: "Ext.grid.Panel",
    xtype: "servicelist",
    store: "Services",
    controller: "service_list",
    listeners: {
        celldblclick: "onServiceDblClick"
    },
    viewConfig: {
        enableTextSelection: true,
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },
    headerBorders: true,
    rowLines: false,
    autoScroll: true,
    loadMask: true,
    height: Ext.Element.getViewportHeight() - 70,

    initComponent: function () {
        this.tbar = [
            {
                prependText: "Typ: ",
                xtype: "cycle",
                id: "servicetypefilterbutton",
                showText: true,
                width: 150,
                margin: "0 5px 0 5px",
                textAlign: "left",
                listeners: {
                    change: "onFilter"
                },
                menu: {
                    items: UDPManager.Configs.serviceTypeFilterListGen(true)
                }
            },
            {
                prependText: "Software: ",
                xtype: "cycle",
                id: "softwarefilterbutton",
                showText: true,
                width: 150,
                margin: "0 5px 0 5px",
                textAlign: "left",
                listeners: {
                    change: "onFilter"
                },
                menu: {
                    items: UDPManager.Configs.softwareFilterListGen(true)
                }
            },
            {
                prependText: "Status: ",
                xtype: "cycle",
                showText: true,
                width: 150,
                margin: "0 5px 0 5px",
                textAlign: "left",
                listeners: {
                    change: "onFilter"
                },
                menu: {
                    items: [{
                        text: "Alle",
                        type: "status_alle",
                        itemId: "status_alle",
                        checked: true
                    },
                    {
                        text: "prod",
                        type: "status_prod",
                        itemId: "status_prod"
                    },
                    {
                        text: "stage",
                        type: "status_stage",
                        itemId: "status_stage"
                    },
                    {
                        text: "dev",
                        type: "status_dev",
                        itemId: "status_dev"
                    }]
                }
            },
            {
                prependText: "Stelle: ",
                xtype: "cycle",
                id: "servicesresponsiblepartyfilter",
                showText: true,
                width: 150,
                margin: "0 5px 0 5px",
                textAlign: "left",
                listeners: {
                    change: "onResponsiblePartyFilter"
                },
                menu: {
                    items: UDPManager.Configs.responsiblePartyFilterListGen()
                }
            },
            {
                xtype: "checkboxfield",
                id: "servicefiltermultiplelinks",
                fieldLabel: "Sammelschnittstellen",
                margin: "0 5px 0 5px",
                labelWidth: 120,
                listeners: {
                    change: "onFilterMultipleLinks"
                }
            },
            {
                xtype: "component",
                id: "servicescounter",
                margin: "0 5px 0 5px",
                cls: "x-form-item-label-default",
                html: ""
            }
        ];

        this.callParent(arguments);
    },

    columns: [
        {
            dataIndex: "id",
            text: "ID",
            align: "center",
            width: 100
        },
        {
            xtype: "actioncolumn",
            text: "Status",
            width: 90,
            menuDisabled: true,
            sortable: false,
            align: "center",
            renderer: function (value, metadata, record) {
                if (record.get("status_dev") === true) {
                    this.items[0].iconCls = "x-fa fa-info-circle orangeIcon firstIcon";
                    this.items[0].tooltip = "Schnittstelle in Status dev aufrufen";
                    this.items[1].iconCls = "x-fa fa-info-circle greyIcon secondIcon";
                    this.items[1].tooltip = "Schnittstelle in Status stage aufrufen";
                    this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                    this.items[2].tooltip = "Schnittstelle in Status prod aufrufen";
                }
                else {
                    this.items[0].iconCls = "x-fa fa-info-circle greyIcon firstIcon";
                    this.items[1].iconCls = "x-fa fa-info-circle greyIcon secondIcon";
                    this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                }
                if (record.get("status_stage") === true) {
                    this.items[1].iconCls = "x-fa fa-info-circle yellowIcon secondIcon";
                    this.items[1].tooltip = "Schnittstelle in Status stage aufrufen";
                    this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                    this.items[2].tooltip = "Schnittstelle in Status prod aufrufen";
                }
                else {
                    this.items[1].iconCls = "x-fa fa-info-circle greyIcon secondIcon";
                    this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                }
                if (record.get("status_prod") === true) {
                    this.items[2].iconCls = "x-fa fa-info-circle greenIcon";
                    this.items[2].tooltip = "Schnittstelle in Status prod aufrufen";
                }
                else {
                    this.items[2].iconCls = "x-fa fa-info-circle greyIcon";
                }
            },
            items: [
                {
                    itemId: "servicelistinfostatusdev",
                    iconCls: "x-fa fa-info-circle greyIcon",
                    tooltip: "Schnittstelle in Status dev aufrufen",
                    handler: "onApiCall"
                },
                {
                    itemId: "servicelistinfostatusstage",
                    iconCls: "x-fa fa-info-circle greyIcon",
                    tooltip: "Schnittstelle in Status stage aufrufen",
                    handler: "onApiCall"
                },
                {
                    itemId: "servicelistinfostatusprod",
                    iconCls: "x-fa fa-info-circle greyIcon",
                    tooltip: "Schnittstelle in Status prod aufrufen",
                    handler: "onApiCall"
                }
            ]
        },
        {
            dataIndex: "type",
            text: "Typ",
            align: "center",
            width: 100
        },
        {
            dataIndex: "software",
            text: "Software",
            align: "center",
            width: 120
        },
        {
            dataIndex: "title",
            text: "Titel",
            flex: 2,
            minWidth: 200,
            align: "left",
            renderer: function (value) {
                return "<b>" + value + "</b>";
            }
        },
        {
            dataIndex: "ds_count",
            text: "Sammelschnittstelle",
            width: 140,
            renderer: function (value) {
                if (value > 1) {
                    return value + " Datensätze";
                }
                else {
                    return "";
                }
            }
        }
    ],

    plugins: {
        rowwidget: {
            widget: {
                xtype: "grid",
                bind: {
                    store: "{record.datasets}"
                },
                columns: [
                    {
                        text: "ID",
                        dataIndex: "id",
                        width: 100
                    },
                    {
                        text: "Datensatz-Titel",
                        dataIndex: "title",
                        flex: 1
                    },
                    {
                        text: "Verantwortliche Stelle",
                        dataIndex: "responsible_party",
                        flex: 1
                    },
                    {
                        xtype: "actioncolumn",
                        header: "Aktionen",
                        name: "aktionen",
                        align: "center",
                        sortable: false,
                        menuDisabled: true,
                        width: 110,
                        items: [
                            {
                                iconCls: "x-fa fa-share faIconAlt",
                                tooltip: "Datensatz aufrufen",
                                handler: "onOpenDataset"
                            }
                        ]
                    }
                ]
            }
        }
    }
});
