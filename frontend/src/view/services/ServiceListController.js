Ext.define("UDPManager.view.services.ServiceListController", {
    extend: "Ext.app.ViewController",
    alias: "controller.service_list",

    onFilter: function (btn, menuitem) {
        let type = "";
        let filterItem = "";

        if (menuitem.type.indexOf("geprueft") > -1) {
            type = "geprueft";
            filterItem = "check_status";
        }

        if (menuitem.type.indexOf("typ") > -1) {
            type = "typ";
            filterItem = "type";
        }

        if (menuitem.type.indexOf("software") > -1) {
            type = "software";
            filterItem = "software";
        }

        if (menuitem.type.indexOf("status") > -1) {
            type = "status";
            filterItem = "status";
        }

        if (menuitem.type.indexOf("alle") > -1) {
            Ext.getStore("Services").removeFilter("filter_" + type);
        }
        else {
            Ext.getStore("Services").addFilter(new Ext.util.Filter({
                id: "filter_" + type,
                filterFn: function (item) {
                    return item.get(filterItem) === menuitem.text;
                }
            }));
        }
    },

    onResponsiblePartyFilter: function (btn, menuitem) {
        if (menuitem.type.indexOf("alle") > -1) {
            Ext.getStore("Services").removeFilter("filter_responsible_party");
        }
        else {
            Ext.getStore("Services").addFilter(new Ext.util.Filter({
                id: "filter_responsible_party",
                filterFn: function (item) {
                    return item.get("datasets").find((dataset) => dataset.responsible_party === menuitem.text);
                }
            }));
        }
    },

    onServiceDblClick: function (view, td, cellIndex, record) {
        const dataset = Ext.getStore("Datasets").findRecord("id", record.data.datasets[0].id, 0, false, false, true);
        const dataset_grid = Ext.getCmp("grid_dataset");
        const services_datase_grid = Ext.getCmp("grid_datasetservices");
        const servicesDatasetStore = Ext.getStore("ServicesDataset");

        Ext.getCmp("main-nav-tab-panel").setActiveTab(0);
        Ext.getCmp("dataset_main_tabs").setActiveTab(2);

        const row_dataset = dataset_grid.store.indexOf(dataset);

        dataset_grid.getSelectionModel().deselectAll();
        dataset_grid.getSelectionModel().select(row_dataset);
        dataset_grid.ensureVisible(row_dataset);

        servicesDatasetStore.on("load", function () {
            const service = servicesDatasetStore.findRecord("id", record.data.id, 0, false, false, true);
            const row_service = services_datase_grid.store.indexOf(service);

            services_datase_grid.getSelectionModel().select(row_service);
            services_datase_grid.ensureVisible(row_service);
        }, this, {single: true});
    },

    onApiCall: function (grid, rowIndex, colIndex, item) {
        const status = "status_" + item.itemId.split("status")[1];
        const row_data = grid.getStore().getAt(rowIndex).data;

        if (row_data[status]) {
            UDPManager.app.getController("UDPManager.controller.PublicFunctions").onApiCall(grid, rowIndex, status);
        }
    },

    onOpenDataset: function (grid, rowIndex) {
        const data = grid.getStore().getAt(rowIndex).data;

        const dataset = Ext.getStore("Datasets").findRecord("id", data.id, 0, false, false, true);

        const row_dataset = Ext.getStore("Datasets").indexOf(dataset);

        Ext.getCmp("main-nav-tab-panel").setActiveTab(0);

        const dataset_grid = Ext.getCmp("grid_dataset");

        dataset_grid.getSelectionModel().deselectAll();
        dataset_grid.getSelectionModel().select(row_dataset);
        dataset_grid.ensureVisible(row_dataset);
    },

    onFilterMultipleLinks: function (cb, newValue) {
        if (newValue) {
            Ext.getStore("Services").addFilter(new Ext.util.Filter({
                id: "filter_muliplelinks",
                filterFn: function (item) {
                    return item.get("ds_count") > 1;
                }
            }));
        }
        else {
            Ext.getStore("Services").removeFilter("filter_muliplelinks");
        }
    }
});
