Ext.define("UDPManager.view.statistics.StatisticsController", {
    extend: "Ext.app.ViewController",
    alias: "controller.statistics",

    init: function () {
        if (UDPManager.Configs.getModules().visitStatistics) {
            let sprite_width = Ext.Element.getViewportWidth() - 100;

            if (sprite_width > 1470) {
                sprite_width = 1470;
            }

            const statisticsTabPanel = Ext.getCmp("statisticsTabPanel");

            statisticsTabPanel.add({
                xtype: "container",
                title: "Zugriffe",
                layout: {
                    type: "hbox",
                    align: "stretch"
                },
                items: [{
                    xtype: "panel",
                    width: "50%",
                    height: Ext.Element.getViewportHeight() - 150,
                    items: [{
                        xtype: "cartesian",
                        id: "serviceTopVisitsChart",
                        width: "100%",
                        maxHeight: 620,
                        height: Ext.Element.getViewportHeight() - 190,
                        animation: {
                            duration: 200
                        },
                        store: "VisitsTop",
                        flipXY: true,
                        insetPadding: 40,
                        innerPadding: {
                            left: 20,
                            right: 20
                        },
                        legend: {
                            type: "dom",
                            docked: "bottom"
                        },
                        sprites: [{
                            type: "text",
                            text: "Top 10 Zugriffe " + moment().format("YYYY-MM"),
                            fontSize: 22,
                            width: sprite_width,
                            height: 30,
                            x: 40,
                            y: 20
                        }],
                        axes: [{
                            type: "numeric",
                            position: "bottom",
                            grid: true,
                            minimum: 0
                        }, {
                            type: "category",
                            position: "left",
                            grid: true
                        }],
                        series: [{
                            type: "bar",
                            title: ["Internet", "Intranet"],
                            xField: "name_ext",
                            yField: ["visits_internet", "visits_intranet"],
                            stacked: true,
                            style: {
                                opacity: 0.80,
                                maxBarWidth: 30
                            },
                            highlight: {
                                fillStyle: "yellow"
                            },
                            tooltip: {
                                renderer: "onBarTooltipRender"
                            }
                        }]
                    }],
                    dockedItems: [{
                        xtype: "toolbar",
                        id: "visitsTopStatisticsToolbar",
                        dock: "top",
                        items: [
                            {
                                xtype: "combo",
                                store: [],
                                value: moment().format("YYYY-MM"),
                                id: "chooseTopVisitsMonth",
                                listeners: {
                                    change: "onFilterTopVisits"
                                }
                            },
                            {
                                xtype: "combo",
                                store: UDPManager.Configs.softwareListGen(true),
                                value: "Alle",
                                id: "chooseTopVisitsSoftware",
                                listeners: {
                                    change: "onFilterTopVisits"
                                }
                            },
                            {
                                id: "saveTopVisitsStatistics",
                                text: "Grafik speichern",
                                handler: "onSaveTopVisitsStatistics"
                            }
                        ]
                    }]
                },
                {
                    xtype: "panel",
                    width: "50%",
                    height: Ext.Element.getViewportHeight() - 150,
                    items: [{
                        xtype: "cartesian",
                        id: "serviceVisitsTotalChart",
                        width: "100%",
                        maxHeight: 620,
                        height: Ext.Element.getViewportHeight() - 190,
                        animation: {
                            duration: 200
                        },
                        store: "VisitsTotal",
                        insetPadding: 40,
                        innerPadding: {
                            left: 20,
                            right: 20
                        },
                        legend: {
                            type: "dom",
                            docked: "bottom"
                        },
                        sprites: [{
                            type: "text",
                            text: "Gesamtzugriffe",
                            fontSize: 22,
                            width: sprite_width,
                            height: 30,
                            x: 40,
                            y: 20
                        }],
                        axes: [{
                            type: "numeric",
                            position: "left",
                            grid: true,
                            minimum: 0
                        }, {
                            type: "category",
                            position: "bottom",
                            grid: true,
                            label: {
                                rotate: {
                                    degrees: -45
                                }
                            }
                        }],
                        series: [{
                            type: "bar",
                            title: ["Internet", "Intranet"],
                            xField: "month",
                            yField: ["visits_internet", "visits_intranet"],
                            stacked: true,
                            style: {
                                opacity: 0.80,
                                maxBarWidth: 30
                            },
                            highlight: {
                                fillStyle: "yellow"
                            },
                            tooltip: {
                                renderer: "onBarTooltipRender"
                            }
                        }]
                    }],
                    dockedItems: [{
                        xtype: "toolbar",
                        id: "visitsTotalStatisticsToolbar",
                        dock: "top",
                        items: [
                            {
                                id: "saveTotalVisitsStatistics",
                                text: "Grafik speichern",
                                handler: "onSaveTotalVisitsStatistics"
                            }
                        ]
                    }]
                }]
            });
        }
    },

    viewIsShown: function () {
        this.onFilterServiceStatistics();
        this.onFilterSoftwareStatistics();
        this.onFilterlayerStatistics();
    },

    onSaveServiceStatisticsPie: function () {
        this.downloadChart("serviceStatistics");
    },

    onSaveSoftwareStatisticsPie: function () {
        this.downloadChart("softwareStatistics");
    },

    onSavelayerStatisticsPie: function () {
        this.downloadChart("layerStatistics");
    },

    downloadChart: function (source) {
        let dt;

        if (source === "softwareStatistics") {
            dt = Ext.getCmp("softwareStatisticsChart").getImage().data.replace(/^data:image\/[^;]*/, "data:application/octet-stream");
        }
        else if (source === "layerStatistics") {
            dt = Ext.getCmp("layerStatisticsChart").getImage().data.replace(/^data:image\/[^;]*/, "data:application/octet-stream");
        }
        else if (source === "serviceStatistics") {
            dt = Ext.getCmp("serviceStatisticsChart").getImage().data.replace(/^data:image\/[^;]*/, "data:application/octet-stream");
        }

        if (dt) {
            const download_link = document.createElement("a");

            download_link.href = dt;
            download_link.target = "_blank";
            download_link.download = source + ".png";
            download_link.click();
            download_link.remove();
        }
    },

    onserviceStatisticsTooltipRender: function (tooltip, record) {
        tooltip.setHtml(record.get("type") + " - " + record.get("count"));
    },

    onSoftwareStatisticsTooltipRender: function (tooltip, record) {
        tooltip.setHtml(record.get("software") + " - " + record.get("count"));
    },

    onlayerStatisticsTooltipRender: function (tooltip, record) {
        tooltip.setHtml(record.get("service_type") + " - " + record.get("count"));
    },

    onFilterSoftwareStatistics: function () {
        var value = Ext.getCmp("chooseProdComboSoftware").getValue();

        if (value === "Alle Dienste") {
            Ext.getStore("SoftwareStats").load({
                params: {scope: "all"},
                callback: function (records) {
                    var anz_gesamt_software = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_software += records[i].data.count;
                    }
                    Ext.getCmp("softwareStatisticsChart").setConfig("title", "Eingesetzte Software - gesamt: " + anz_gesamt_software);
                }
            });
        }
        else if (value === "Interne prod. Dienste") {
            Ext.getStore("SoftwareStats").load({
                params: {scope: "intern"},
                callback: function (records) {
                    var anz_gesamt_software = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_software += records[i].data.count;
                    }
                    Ext.getCmp("softwareStatisticsChart").setConfig("title", "Eingesetzte Software - gesamt: " + anz_gesamt_software);
                }
            });
        }
        else if (value === "Nur externe Dienste") {
            Ext.getStore("SoftwareStats").load({
                params: {scope: "extern"},
                callback: function (records) {
                    var anz_gesamt_software = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_software += records[i].data.count;
                    }
                    Ext.getCmp("softwareStatisticsChart").setConfig("title", "Eingesetzte Software - gesamt: " + anz_gesamt_software);
                }
            });
        }
        else if (value === "Nur produktive Dienste") {
            Ext.getStore("SoftwareStats").load({
                params: {scope: "prod"},
                callback: function (records) {
                    var anz_gesamt_software = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_software += records[i].data.count;
                    }
                    Ext.getCmp("softwareStatisticsChart").setConfig("title", "Eingesetzte Software - gesamt: " + anz_gesamt_software);
                }
            });
        }
    },

    onFilterServiceStatistics: function () {
        var value = Ext.getCmp("chooseProdComboService").getValue();

        if (value === "Alle Dienste") {
            Ext.getStore("ServiceStats").load({
                params: {scope: "all"},
                callback: function (records) {
                    var anz_gesamt_dienste = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_dienste += records[i].data.count;
                    }
                    Ext.getCmp("serviceStatisticsChart").setConfig("title", "Diensttypen - gesamt: " + anz_gesamt_dienste);
                }
            });
        }
        else if (value === "Interne prod. Dienste") {
            Ext.getStore("ServiceStats").load({
                params: {scope: "intern"},
                callback: function (records) {
                    var anz_gesamt_dienste = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_dienste += records[i].data.count;
                    }
                    Ext.getCmp("serviceStatisticsChart").setConfig("title", "Diensttypen - gesamt: " + anz_gesamt_dienste);
                }
            });
        }
        else if (value === "Nur externe Dienste") {
            Ext.getStore("ServiceStats").load({
                params: {scope: "extern"},
                callback: function (records) {
                    var anz_gesamt_dienste = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_dienste += records[i].data.count;
                    }
                    Ext.getCmp("serviceStatisticsChart").setConfig("title", "Diensttypen - gesamt: " + anz_gesamt_dienste);
                }
            });
        }
        else if (value === "Nur produktive Dienste") {
            Ext.getStore("ServiceStats").load({
                params: {scope: "prod"},
                callback: function (records) {
                    var anz_gesamt_dienste = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_dienste += records[i].data.count;
                    }
                    Ext.getCmp("serviceStatisticsChart").setConfig("title", "Diensttypen - gesamt: " + anz_gesamt_dienste);
                }
            });
        }
    },

    onFilterlayerStatistics: function () {
        var value = Ext.getCmp("chooseProdComboLayer").getValue();

        if (value === "Alle Dienste") {
            Ext.getStore("LayerStats").load({
                params: {scope: "all"},
                callback: function (records) {
                    var anz_gesamt_layer = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_layer += records[i].data.count;
                    }
                    Ext.getCmp("layerStatisticsChart").setConfig("title", "Anzahl Layer / FeatureTypes - gesamt: " + anz_gesamt_layer);
                }
            });
        }
        else if (value === "Interne prod. Dienste") {
            Ext.getStore("LayerStats").load({
                params: {scope: "intern"},
                callback: function (records) {
                    var anz_gesamt_layer = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_layer += records[i].data.count;
                    }
                    Ext.getCmp("layerStatisticsChart").setConfig("title", "Anzahl Layer / FeatureTypes - gesamt: " + anz_gesamt_layer);
                }
            });
        }
        else if (value === "Nur externe Dienste") {
            Ext.getStore("LayerStats").load({
                params: {scope: "extern"},
                callback: function (records) {
                    var anz_gesamt_layer = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_layer += records[i].data.count;
                    }
                    Ext.getCmp("layerStatisticsChart").setConfig("title", "Anzahl Layer / FeatureTypes - gesamt: " + anz_gesamt_layer);
                }
            });
        }
        else if (value === "Nur produktive Dienste") {
            Ext.getStore("LayerStats").load({
                params: {scope: "prod"},
                callback: function (records) {
                    var anz_gesamt_layer = 0;

                    for (var i = 0; i < records.length; i++) {
                        anz_gesamt_layer += records[i].data.count;
                    }
                    Ext.getCmp("layerStatisticsChart").setConfig("title", "Anzahl Layer / FeatureTypes - gesamt: " + anz_gesamt_layer);
                }
            });
        }
    },

    onBarTooltipRender: function (tooltip, record, item) {
        const fieldIndex = Ext.Array.indexOf(item.series.getYField(), item.field);
        const source = item.series.getTitle()[fieldIndex];

        tooltip.setHtml("Zugriffe - " + source + ": " + record.get(item.field));
    },

    onSaveTopVisitsStatistics: function () {
        const dt = Ext.getCmp("serviceTopVisitsChart").getImage().data.replace(/^data:image\/[^;]*/, "data:application/octet-stream");

        if (dt) {
            const download_link = document.createElement("a");

            download_link.href = dt;
            download_link.target = "_blank";
            download_link.download = "top10_zugriffe.png";
            download_link.click();
            download_link.remove();
        }
    },

    onSaveTotalVisitsStatistics: function () {
        const dt = Ext.getCmp("serviceVisitsTotalChart").getImage().data.replace(/^data:image\/[^;]*/, "data:application/octet-stream");

        if (dt) {
            const download_link = document.createElement("a");

            download_link.href = dt;
            download_link.target = "_blank";
            download_link.download = "gesamtzugriffe.png";
            download_link.click();
            download_link.remove();
        }
    },

    onFilterTopVisits: function () {
        const month = Ext.getCmp("chooseTopVisitsMonth").getSubmitValue();
        const software = Ext.getCmp("chooseTopVisitsSoftware").getSubmitValue();

        Ext.getStore("VisitsTop").load({
            params: {month: month, software: software}
        });
        let sprite_width = Ext.Element.getViewportWidth() - 100;

        if (sprite_width > 1470) {
            sprite_width = 1470;
        }
        Ext.getCmp("serviceTopVisitsChart").setSprites([{
            type: "text",
            text: "Top 10 Zugriffe " + month + " (" + software + ")",
            fontSize: 22,
            width: sprite_width,
            height: 30,
            x: 40,
            y: 20
        }]);
    }
});
