Ext.define("UDPManager.view.collections.CollectionList", {
    extend: "Ext.grid.Panel",
    xtype: "collectionlist",
    store: "Collections",
    controller: "collections",
    listeners: {
        celldblclick: "onCollectionDblClick"
    },
    viewConfig: {
        enableTextSelection: true,
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },
    headerBorders: true,
    rowLines: false,
    autoScroll: true,
    height: Ext.Element.getViewportHeight() - 70,

    columns: [
        {
            header: "ID",
            dataIndex: "id",
            align: "center",
            width: 80
        }, {
            header: "Name",
            dataIndex: "name",
            align: "left",
            flex: 1
        }, {
            header: "Titel",
            dataIndex: "title",
            align: "left",
            flex: 1
        }, {
            header: "Datensatz-Titel",
            dataIndex: "dataset_title",
            align: "left",
            flex: 1
        }
    ]
});
