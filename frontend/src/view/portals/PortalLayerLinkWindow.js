Ext.define("UDPManager.view.portals.PortalLayerLinkWindow", {
    extend: "Ext.window.Window",
    id: "portallayerlink-window",
    alias: "portals.PortalLayerLinkWindow",
    height: 400,
    width: 400,
    title: "Layer verknüpfen",
    resizable: false,
    closeAction: "hide",
    controller: "portallayerlink",
    listeners: {
        close: "onWindowClose"
    },
    items: [
        {
            xtype: "panel",
            bodyPadding: 10,
            items: [
                {
                    xtype: "gridpanel",
                    id: "grid_portallayerlink",
                    store: "Layers",
                    border: true,
                    columns: [
                        {
                            text: "ID", 
                            dataIndex: "id",
                            flex: 0.3,
                            layout: "hbox",
                            items: {
                                xtype: "numberfield",
                                fieldStyle: "",
                                id: "idfilterfield",
                                flex: 1,
                                margin: 2,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: "onIdFilterKeyup",
                                    buffer: 500
                                }
                            }
                        },
                        {
                            text: "Titel",
                            dataIndex: "title",
                            flex: 0.7,
                            layout: "hbox",
                            items: {
                                xtype: "textfield",
                                fieldStyle: "",
                                id: "titlefilterfield",
                                flex: 1,
                                margin: 2,
                                enableKeyEvents: true,
                                listeners: {
                                    keyup: "onTitleFilterKeyup",
                                    buffer: 500
                                }
                            }
                        }
                    ],
                    height: 300,
                    autoScroll: true
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    "->",
                    {
                        id: "addnewlayermanually",
                        text: "Verknüpfen",
                        listeners: {
                            click: "onAddPortalLayerLink"
                        }
                    }
                ]
            }]
        }
    ]
});