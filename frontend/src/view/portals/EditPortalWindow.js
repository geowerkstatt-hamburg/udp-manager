Ext.define("UDPManager.view.portals.EditPortalWindow", {
    extend: "Ext.window.Window",
    xtype: "editportalwindow",
    id: "editportal-window",
    alias: "portals.EditPortalWindow",
    height: 200,
    width: 500,
    title: "Attribute Bearbeiten",
    bodyStyle: "margin: 10px;",
    controller: "EditPortalWindowController",
    initComponent: function () {
        this.items = [
            {
                xtype: "numberfield",
                id: "portalid",
                hidden: true
            },
            {
                xtype: "textfield",
                fieldLabel: "Titel",
                allowBlank: false,
                id: "portaltitle",
                margin: "0 10 10 0",
                labelWidth: 60,
                width: 470
            },
            {
                xtype: "textfield",
                fieldLabel: "Host",
                allowBlank: false,
                id: "portalhost",
                margin: "0 10 10 0",
                labelWidth: 60,
                width: 470
            },
            {
                xtype: "textfield",
                fieldLabel: "URL",
                allowBlank: false,
                id: "portalurl",
                margin: "0 10 10 0",
                labelWidth: 60,
                width: 470
            }
        ];
        this.buttons = [
            {
                text: "Portal löschen",
                margin: "0 5 0 10",
                id: "deleteportal",
                listeners: {
                    click: "onDeletePortal"
                }
            },
            {
                text: "Speichern",
                margin: "0 5 0 10",
                id: "saveportal",
                listeners: {
                    click: "onSavePortal"
                }
            }
        ];
        this.callParent(arguments);
    }
});
