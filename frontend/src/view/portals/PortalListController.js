Ext.define("UDPManager.view.portals.PortalListController", {
    extend: "Ext.app.ViewController",
    alias: "controller.portal_list",

    onSelectPortal: function (view, record) {
        Ext.getStore("PortalLayers").load({
            params: {portal_id: record.data.id}
        });
        if (record.get("update_mode") === "manual") {
            Ext.getCmp("addlayerlink").setDisabled(window.read_only);
        }
        else {
            Ext.getCmp("addlayerlink").setDisabled(true);
        }
    },

    onPortalLayerDblClick: function (view, td, cellIndex, record) {
        const dataset = Ext.getStore("Datasets").findRecord("id", record.data.dataset_id, 0, false, false, true);
        const dataset_grid = Ext.getCmp("grid_dataset");
        const collections_datase_grid = Ext.getCmp("grid_datasetcollections");
        const layers_collection_grid = Ext.getCmp("grid_collectionlayers");
        const collectionsDatasetStore = Ext.getStore("CollectionsDataset");
        const layersCollectionStore = Ext.getStore("LayersCollection");

        Ext.getCmp("main-nav-tab-panel").setActiveTab(0);
        Ext.getCmp("dataset_main_tabs").setActiveTab(1);
        Ext.getCmp("collections_main_tabs").setActiveTab(1);

        const row_dataset = dataset_grid.store.indexOf(dataset);

        dataset_grid.getSelectionModel().deselectAll();

        dataset_grid.getSelectionModel().select(row_dataset);
        dataset_grid.ensureVisible(row_dataset);

        collectionsDatasetStore.on("load", function () {
            const collection = collectionsDatasetStore.findRecord("id", record.data.collection_id, 0, false, false, true);
            const row_collection = collections_datase_grid.store.indexOf(collection);

            collections_datase_grid.getSelectionModel().select(row_collection);
            collections_datase_grid.ensureVisible(row_collection);

        }, this, {single: true});

        layersCollectionStore.on("load", function () {
            const layer = layersCollectionStore.findRecord("id", record.data.id, 0, false, false, true);
            const row_layer = layers_collection_grid.store.indexOf(layer);

            layers_collection_grid.getSelectionModel().select(row_layer);
            layers_collection_grid.ensureVisible(row_layer);

        }, this, {single: true});
    },

    onAddNewPortal: function () {
        let editPortalWindow = Ext.getCmp("editportal-window");

        if (editPortalWindow) {
            editPortalWindow.destroy();
        }

        editPortalWindow = Ext.create("portals.EditPortalWindow");

        editPortalWindow.show();

        Ext.getCmp("portalid").setValue(0);

        Ext.getCmp("deleteportal").setDisabled(true);
    },

    onEditPortal: function (grid, rowIndex) {
        const data = grid.getStore().getAt(rowIndex).data;

        if (data.update_mode === "manual") {
            let editPortalWindow = Ext.getCmp("editportal-window");

            if (editPortalWindow) {
                editPortalWindow.destroy();
            }

            editPortalWindow = Ext.create("portals.EditPortalWindow");

            editPortalWindow.show();

            Ext.getCmp("portalid").setValue(data.id);
            Ext.getCmp("portaltitle").setValue(data.title);
            Ext.getCmp("portalhost").setValue(data.host);
            Ext.getCmp("portalurl").setValue(data.url);
        }
    },

    onAddLayerLink: function () {
        Ext.getStore("Layers").load();

        let portalLayerLinkWindow = Ext.getCmp("portallayerlink-window");

        if (portalLayerLinkWindow) {
            portalLayerLinkWindow.destroy();
        }

        portalLayerLinkWindow = Ext.create("portals.PortalLayerLinkWindow");

        portalLayerLinkWindow.show();
    },

    onDeletePortalLayerLink: function (grid, rowIndex) {
        const data = grid.getStore().getAt(rowIndex).data;

        if (data.update_mode === "manual") {
            Ext.getCmp("portallayers-grid").getSelectionModel().deselectAll();

            Ext.Ajax.request({
                url: "backend/deleteportallayerlink",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    layer_id: data.id,
                    portal_id: data.portal_id
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        grid.getStore().removeAt(rowIndex);
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Link konnte nicht gelöscht werden");
                }
            });
        }
    }
});
