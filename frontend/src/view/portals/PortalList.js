Ext.define("UDPManager.view.portals.PortalList", {
    extend: "Ext.panel.Panel",
    xtype: "portallist",
    controller: "portal_list",

    layout: {
        type: "hbox",
        align: "stretch"
    },
    viewConfig: {
        enableTextSelection: true,
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    headerBorders: false,
    rowLines: false,
    autoScroll: true,

    height: Ext.Element.getViewportHeight() - 70,

    items: [{
        title: "Portale",
        xtype: "grid",
        id: "portals-grid",
        store: "Portals",
        buttonAlign: "left",
        flex: 1,
        listeners: {
            select: "onSelectPortal"
        },
        features: [{
            ftype: "grouping",
            startCollapsed: true,
            groupHeaderTpl: '{columnName}: {name} ({rows.length} Portal{[values.rows.length > 1 ? "e" : ""]})'
        }],
        columns: [
            {
                text: "Titel",
                dataIndex: "title",
                align: "left",
                flex: 1
            },
            {
                text: "URI",
                dataIndex: "url",
                align: "left",
                flex: 1
            },
            {
                text: "Host",
                dataIndex: "host",
                align: "left",
                hidden: true,
                flex: 1
            },
            {
                text: "Link",
                dataIndex: "url",
                align: "left",
                width: 70,
                renderer: function (value, metaData, record) {
                    if (value !== "") {
                        return "<a href=\"" + record.data.host + value + "\" target=\"_blank\">Link</a>";
                    }
                    else {
                        return "";
                    }
                }
            },
            {
                xtype: "actioncolumn",
                header: "Editieren",
                name: "edit",
                align: "center",
                sortable: false,
                menuDisabled: true,
                width: 80,
                renderer: function (value, metadata, record) {
                    if (record.get("update_mode") === "manual" && !window.read_only) {
                        this.items[0].iconCls = "x-fa fa-pen";
                        this.items[0].tooltip = "Eintrag bearbeiten";
                        this.items[0].handler = "onEditPortal";
                    }
                    else {
                        this.items[0].iconCls = "";
                        this.items[0].tooltip = "Keine Funktion verfügbar";
                    }
                },
                items: [
                    {
                        iconCls: "x-fa fa-pen",
                        tooltip: "Eintrag bearbeiten",
                        handler: "onEditPortal"
                    }
                ]
            }
        ],
        dockedItems: [{
            xtype: "toolbar",
            dock: "bottom",
            items: [
                {
                    id: "addnewportal",
                    iconCls: "x-fa fa-plus",
                    disabled: window.read_only,
                    scale: "medium",
                    cls: "custom-btn-icon",
                    listeners: {
                        click: "onAddNewPortal"
                    }
                }
            ]
        }]
    }, {
        title: "Enthaltene Layer",
        xtype: "grid",
        id: "portallayers-grid",
        store: "PortalLayers",
        flex: 1,
        buttonAlign: "left",
        margin: "0 0 0 10",
        listeners: {
            celldblclick: "onPortalLayerDblClick"
        },
        columns: [
            {
                text: "Layer-ID",
                dataIndex: "id",
                align: "left"
            },
            {
                text: "Layer-Name",
                dataIndex: "name",
                align: "left",
                flex: 1
            },
            {
                text: "Layer-Titel",
                dataIndex: "title",
                align: "left",
                flex: 1
            },
            {
                xtype: "actioncolumn",
                header: "Löschen",
                name: "delete",
                align: "center",
                sortable: false,
                menuDisabled: true,
                width: 80,
                renderer: function (value, metadata, record) {
                    if (record.get("update_mode") === "manual" && !window.read_only) {
                        this.items[0].iconCls = "x-fa fa-times";
                        this.items[0].tooltip = "Verlinkung löschen";
                        this.items[0].handler = "onDeletePortalLayerLink";
                    }
                    else {
                        this.items[0].iconCls = "";
                        this.items[0].tooltip = "Keine Funktion verfügbar";
                    }
                },
                items: [
                    {
                        iconCls: "x-fa fa-times",
                        tooltip: "Verlinkung löschen",
                        handler: "onDeletePortalLayerLink"
                    }
                ]
            }
        ],
        viewConfig: {
            emptyText: "Kein Portal ausgewählt",
            deferEmptyText: false
        },
        dockedItems: [{
            xtype: "toolbar",
            dock: "bottom",
            items: [
                {
                    id: "addlayerlink",
                    iconCls: "x-fa fa-link",
                    disabled: true,
                    scale: "medium",
                    cls: "custom-btn-icon",
                    listeners: {
                        click: "onAddLayerLink"
                    }
                }
            ]
        }]
    }]
});
