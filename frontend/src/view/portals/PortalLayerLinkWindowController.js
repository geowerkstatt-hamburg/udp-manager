Ext.define("UDPManager.view.portals.PortalLayerLinkController", {
    extend: "Ext.app.ViewController",
    alias: "controller.portallayerlink",

    onAddPortalLayerLink: function () {
        const layer_grid = Ext.getCmp("grid_portallayerlink");
        const portal_grid = Ext.getCmp("portals-grid");

        if (layer_grid.getSelectionModel().hasSelection() && portal_grid.getSelectionModel().hasSelection()) {
            const layer = layer_grid.getSelectionModel().getSelection()[0].data;
            const portal = portal_grid.getSelectionModel().getSelection()[0].data;

            Ext.Ajax.request({
                url: "backend/addportallayerlink",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    layer_id: layer.id,
                    portal_id: portal.id
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        Ext.getStore("PortalLayers").load({
                            params: {portal_id: portal.id}
                        });
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
    },

    onIdFilterKeyup: function() {
        const filterValue = Ext.getCmp("idfilterfield").getValue();
        const filters =Ext.getStore("Layers").getFilters();

        if (filterValue) {
            this.idFilter = filters.add({
                id: "idFilter",
                property: "id",
                value: filterValue,
                anyMatch: true,
                caseSensitive: false
            });
        }
        else if (this.idFilter) {
            filters.remove(this.idFilter);
            this.idFilter = null;
        }
    },

    onTitleFilterKeyup: function() {
        const filterValue = Ext.getCmp("titlefilterfield").getValue();
        const filters = Ext.getStore("Layers").getFilters();

        if (filterValue) {
            this.titleFilter = filters.add({
                id: "titleFilter",
                property: "title",
                value: filterValue,
                anyMatch: true,
                caseSensitive: false
            });
        }
        else if (this.titleFilter) {
            filters.remove(this.titleFilter);
            this.titleFilter = null;
        }
    },

    onWindowClose: function () {
        Ext.getStore("Layers").removeFilter("idFilter");
        Ext.getStore("Layers").removeFilter("titleFilter");
    }
});
