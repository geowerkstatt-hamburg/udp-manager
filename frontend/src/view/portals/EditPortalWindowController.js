Ext.define("UDPManager.view.portals.EditPortalWindowController", {
    extend: "Ext.app.ViewController",
    alias: "controller.EditPortalWindowController",

    onSavePortal: function () {
        const title = Ext.getCmp("portaltitle").getSubmitValue();
        const url = Ext.getCmp("portalurl").getSubmitValue();
        const host = Ext.getCmp("portalhost").getSubmitValue();

        if (title && url) {
            Ext.Ajax.request({
                url: "backend/saveportal",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    id: Ext.getCmp("portalid").getValue(),
                    title: title,
                    host: host,
                    url: url,
                    update_mode: "manual"
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        Ext.getStore("Portals").load();
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                    Ext.getCmp("editportal-window").destroy();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    Ext.getCmp("editportal-window").destroy();
                }
            });
        }
    },

    onDeletePortal: function () {
        const id = Ext.getCmp("portalid").getValue();

        if (id !== 0) {
            Ext.Ajax.request({
                url: "backend/deleteportal",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    id: id
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        Ext.getStore("Portals").load();
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                    Ext.getCmp("editportal-window").destroy();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Portal konnte nicht gelöscht werden");
                    Ext.getCmp("editportal-window").destroy();
                }
            });
        }
    }
});
