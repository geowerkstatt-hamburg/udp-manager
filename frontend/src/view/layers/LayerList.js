Ext.define("UDPManager.view.collections.LayerList", {
    extend: "Ext.grid.Panel",
    xtype: "layerlist",

    store: "Layers",

    controller: "layers",

    listeners: {
        celldblclick: "onLayerDblClick"
    },

    viewConfig: {
        enableTextSelection: true,
        preserveScrollOnRefresh: true,
        preserveScrollOnReload: true
    },

    headerBorders: false,
    rowLines: false,
    autoScroll: true,

    height: Ext.Element.getViewportHeight() - 70,

    columns: [
        {
            header: "ID",
            dataIndex: "id",
            align: "center",
            width: 100
        },
        {
            header: "Datensatz ID",
            dataIndex: "dataset_id",
            align: "center",
            width: 100
        },
        {
            header: "Name",
            dataIndex: "name",
            align: "left",
            flex: 1
        },
        {
            header: "Titel",
            dataIndex: "title",
            align: "left",
            flex: 1
        },
        {
            header: "Typ",
            dataIndex: "service_type",
            align: "center",
            width: 85
        },
        {
            header: "Schnittstellen-Titel",
            dataIndex: "service_title",
            align: "left",
            flex: 1
        }
    ]
});
