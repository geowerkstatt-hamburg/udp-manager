Ext.define("UDPManager.view.layers.LayerListController", {
    extend: "Ext.app.ViewController",

    alias: "controller.layers",

    onLayerDblClick: function (view, td, cellIndex, record) {
        const dataset = Ext.getStore("Datasets").findRecord("id", record.data.dataset_id, 0, false, false, true);
        const dataset_grid = Ext.getCmp("grid_dataset");
        const collections_datase_grid = Ext.getCmp("grid_datasetcollections");
        const layers_collection_grid = Ext.getCmp("grid_collectionlayers");
        const collectionsDatasetStore = Ext.getStore("CollectionsDataset");
        const layersCollectionStore = Ext.getStore("LayersCollection");

        Ext.getCmp("main-nav-tab-panel").setActiveTab(0);
        Ext.getCmp("dataset_main_tabs").setActiveTab(1);
        Ext.getCmp("collections_main_tabs").setActiveTab(1);

        const row_dataset = dataset_grid.store.indexOf(dataset);

        dataset_grid.getSelectionModel().deselectAll();

        dataset_grid.getSelectionModel().select(row_dataset);
        dataset_grid.ensureVisible(row_dataset);

        collectionsDatasetStore.on("load", function () {
            const collection = collectionsDatasetStore.findRecord("id", record.data.collection_id, 0, false, false, true);
            const row_collection = collections_datase_grid.store.indexOf(collection);

            collections_datase_grid.getSelectionModel().select(row_collection);
            collections_datase_grid.ensureVisible(row_collection);

        }, this, {single: true});

        layersCollectionStore.on("load", function () {
            const layer = layersCollectionStore.findRecord("id", record.data.id, 0, false, false, true);
            const row_layer = layers_collection_grid.store.indexOf(layer);

            layers_collection_grid.getSelectionModel().select(row_layer);
            layers_collection_grid.ensureVisible(row_layer);

        }, this, {single: true});
    }
});
