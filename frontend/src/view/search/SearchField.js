Ext.define("UDPManager.view.search.SearchField", {
    extend: "Ext.form.field.Text",

    xtype: "udpsearchfield",
    emptyText: "Suchen",
    alias: "search.searchfield",

    triggers: {
        clear: {
            weight: 0,
            cls: Ext.baseCSSPrefix + "form-clear-trigger",
            hidden: true,
            handler: "onClearClick",
            scope: "this"
        },
        search: {
            weight: 1,
            cls: Ext.baseCSSPrefix + "form-search-trigger",
            handler: "onSearchClick",
            scope: "this"
        }
    },

    hasSearch: false,

    initComponent: function () {
        var me = this;

        me.callParent(arguments);

        me.on("change", function () {
            me.onSearchClick();
        });
    },

    onClearClick: function () {
        if (this.hasSearch) {
            this.setValue("");
            this.store.removeFilter("searchFilter");
            this.hasSearch = false;
            this.getTrigger("clear").hide();
            this.updateLayout();

            Ext.getCmp("dataset_main_tabs").setActiveTab(0);
            Ext.getCmp("datasetlistcollapsible").setCollapsed(false);

            // if (Ext.getCmp("dataset_main_tabs").getActiveTab().xtype !== "dataset_details") {
            //     Ext.getCmp("datasetlistcollapsible").setCollapsed(true);
            // }
        }
    },

    onSearchClick: function () {
        this.store = Ext.getStore(this.storeId);

        if (this.storeId === "Datasets") {
            Ext.getCmp("datasetlistcollapsible").setCollapsed(false);
            Ext.getCmp("dataset_main_tabs").setActiveTab(0);
        }

        const searchValueNormalized = this.getValue().toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");
        const filterFunction = this.store.filterFunction;

        if (searchValueNormalized.length > 0) {
            const customFilter = new Ext.util.Filter({
                id: "searchFilter",
                filterFn: function (item) {
                    return filterFunction(item, searchValueNormalized);
                }
            });

            this.store.addFilter(customFilter);
            this.hasSearch = true;
            this.getTrigger("clear").show();
            this.updateLayout();
        }
        else {
            this.onClearClick();
        }
    }
});
