Ext.define("UDPManager.view.management.settings.FmeServer", {
    extend: "Ext.panel.Panel",
    xtype: "settings_fmeserver",
    controller: "settings",
    title: "FME Flow Datenbank",
    layout: "column",
    height: 400,
    viewModel: {
        data: {
            ConfigFmeServer: null
        }
    },
    items: [
        {
            xtype: "gridpanel",
            id: "config_fmeserver_grid",
            autoScroll: true,
            height: 300,
            columnWidth: 0.35,
            bind: {
                selection: "{ConfigFmeServer}"
            },
            store: "ConfigFmeServers",
            columns: [
                {
                    dataIndex: "name",
                    text: "Bezeichnung",
                    align: "center",
                    flex: 1
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Parameter",
            columnWidth: 0.65,
            margin: "0 10 0 10",
            layout: "anchor",
            defaultType: "textfield",
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    labelWidth: 0,
                    width: "100%",
                    items: [
                        {
                            xtype: "button",
                            id: "add_fmeserver",
                            margin: "0 20 5 0",
                            tooltip: "Neue FME-Server Datenbank eingeben",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Neue FME-Server Datenbank eingeben"
                            },
                            text: "Neu",
                            listeners: {
                                click: "onNewConfigFmeServer"
                            }
                        },
                        {
                            xtype: "button",
                            id: "delete_fmeserver",
                            margin: "0 0 5 0",
                            tooltip: "FME-Server Datenbank löschen",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "FME-Server Datenbank löschen"
                            },
                            text: "Löschen",
                            listeners: {
                                click: "onDeleteConfigFmeServer"
                            }
                        }
                    ]
                },
                {
                    fieldLabel: "Bezeichnung",
                    labelWidth: 160,
                    id: "configfmeservername",
                    bind: "{ConfigFmeServer.name}",
                    width: "100%"
                },
                {
                    fieldLabel: "Host",
                    labelWidth: 160,
                    id: "configfmeserverhost",
                    bind: "{ConfigFmeServer.host}",
                    width: "100%"
                },
                {
                    fieldLabel: "Port",
                    labelWidth: 160,
                    id: "configfmeserverport",
                    bind: "{ConfigFmeServer.port}",
                    width: "100%"
                },
                {
                    fieldLabel: "Datenbankname",
                    labelWidth: 160,
                    id: "configfmeserverdatabase",
                    bind: "{ConfigFmeServer.database}",
                    width: "100%"
                },
                {
                    fieldLabel: "Schema",
                    labelWidth: 160,
                    id: "configfmeserverschema",
                    bind: "{ConfigFmeServer.schema}",
                    width: "100%"
                },
                {
                    fieldLabel: "DB-User",
                    labelWidth: 160,
                    id: "configfmeserverdbuser",
                    bind: "{ConfigFmeServer.user}",
                    width: "100%"
                },
                {
                    fieldLabel: "Passwort",
                    inputType: "password",
                    labelWidth: 160,
                    id: "configfmeserverpassword",
                    bind: "{ConfigFmeServer.password}",
                    width: "100%"
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        border: true,
        style: "border-top: 1px solid #cfcfcf !important;",
        items: [
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                xtype: "button",
                id: "save_fmeserver",
                margin: "10 10 5 0",
                scale: "medium",
                cls: "btn-save",
                text: "Speichern",
                listeners: {
                    click: "onSaveConfigFmeServer"
                }
            }
        ]
    }]
});
