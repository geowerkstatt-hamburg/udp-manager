Ext.define("UDPManager.view.management.settings.MetadataCatalogs", {
    extend: "Ext.panel.Panel",
    xtype: "settings_metadatacatalogs",
    controller: "settings",
    title: "Metadatenkataloge",
    // cls: "prio-fieldset",
    // collapsible: true,
    // collapsed: true,
    layout: "column",
    height: 400,
    viewModel: {
        data: {
            ConfigMetadataCatalog: null
        }
    },
    items: [
        {
            xtype: "gridpanel",
            id: "config_metadatacatalogs_grid",
            autoScroll: true,
            height: 300,
            columnWidth: 0.35,
            bind: {
                selection: "{ConfigMetadataCatalog}"
            },
            store: "ConfigMetadataCatalogs",
            columns: [
                {
                    dataIndex: "name",
                    text: "Bezeichnung",
                    align: "center",
                    flex: 1
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Parameter",
            columnWidth: 0.65,
            margin: "0 10 0 10",
            layout: "anchor",
            defaultType: "textfield",
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    labelWidth: 0,
                    width: "100%",
                    items: [
                        {
                            xtype: "button",
                            id: "new_configmetadatacatalog",
                            margin: "0 20 5 0",
                            tooltip: "Neuen Metadatenkatalog eingeben",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Neuen Metadatenkatalog eingeben"
                            },
                            text: "Neu",
                            listeners: {
                                click: "onNewConfigMetadataCatalog"
                            }
                        },
                        {
                            xtype: "button",
                            id: "delete_configmetadatacatalog",
                            margin: "0 0 5 0",
                            tooltip: "Metadatenkatalog löschen",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Metadatenkatalog löschen"
                            },
                            text: "Löschen",
                            listeners: {
                                click: "onDeleteConfigMetadataCatalog"
                            }
                        }
                    ]
                },
                {
                    fieldLabel: "Bezeichnung",
                    labelWidth: 160,
                    id: "configmetadatacatalogname",
                    bind: "{ConfigMetadataCatalog.name}",
                    width: "100%"
                },
                {
                    fieldLabel: "CSW URL",
                    labelWidth: 160,
                    id: "configmetadatacatalogcswurl",
                    bind: "{ConfigMetadataCatalog.csw_url}",
                    width: "100%"
                },
                {
                    fieldLabel: "Show Doc URL",
                    labelWidth: 160,
                    id: "configmetadatacatalogshowdocurl",
                    bind: "{ConfigMetadataCatalog.show_doc_url}",
                    width: "100%"
                },
                {
                    fieldLabel: "Metadaten SRS",
                    labelWidth: 160,
                    id: "configmetadatacatalogsrs",
                    bind: "{ConfigMetadataCatalog.srs_metadata}",
                    width: "100%"
                },
                {
                    xtype: "checkbox",
                    labelWidth: 160,
                    fieldLabel: "Proxy",
                    id: "configmetadatacatalogproxy",
                    bind: "{ConfigMetadataCatalog.proxy}",
                    width: "100%"
                },
                {
                    xtype: "checkbox",
                    labelWidth: 160,
                    fieldLabel: "In Internet Layer JSON verwenden",
                    id: "configmetadatacataloguseininternetjson",
                    bind: "{ConfigMetadataCatalog.use_in_internet_json}",
                    width: "100%"
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        border: true,
        style: "border-top: 1px solid #cfcfcf !important;",
        items: [
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                xtype: "button",
                id: "save_configmetadatacatalog",
                margin: "10 10 5 0",
                text: "Speichern",
                scale: "medium",
                cls: "btn-save",
                listeners: {
                    click: "onSaveConfigMetadataCatalog"
                }
            }
        ]
    }]
});
