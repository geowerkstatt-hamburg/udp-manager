Ext.define("UDPManager.view.management.settings.ElasticSearch", {
    extend: "Ext.panel.Panel",
    xtype: "settings_elasticsearch",
    controller: "settings",
    title: "Elasticsearch",
    id: "elastic-search-conf-fieldset",
    // cls: "prio-fieldset",
    // collapsible: true,
    // collapsed: true,
    height: 700,
    viewModel: {
        data: {
            ElasticIndex: null
        }
    },
    items: [
        {
            xtype: "panel",
            items: [
                {
                    xtype: "button",
                    id: "editelasticsettings",
                    text: "Settings bearbeiten",
                    margin: "5 5 10 5",
                    width: 160,
                    listeners: {
                        click: "onOpenSettingsEditor"
                    }
                },
                {
                    xtype: "button",
                    id: "editelasticmappings",
                    text: "Mappings bearbeiten",
                    margin: "5 5 10 5",
                    width: 160,
                    listeners: {
                        click: "onOpenMappingsEditor"
                    }
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Indexe",
            layout: "column",
            items: [
                {
                    xtype: "gridpanel",
                    id: "elastic-index-config",
                    autoScroll: true,
                    height: 600,
                    columnWidth: 0.35,
                    bind: {
                        selection: "{ElasticIndex}"
                    },
                    listeners: {
                        select: "onElasticGridItemSelected"
                    },
                    store: "ElasticIndexes",
                    columns: [
                        {
                            dataIndex: "name",
                            text: "Indexname",
                            align: "center",
                            flex: 1
                        },
                        {
                            dataIndex: "only_stage",
                            text: "dev/stage",
                            align: "center",
                            width: 85,
                            renderer: function (value) {
                                if (value) {
                                    return "X";
                                }
                                else {
                                    return "";
                                }
                            }
                        },
                        {
                            dataIndex: "only_prod",
                            text: "prod",
                            align: "center",
                            width: 85,
                            renderer: function (value) {
                                if (value) {
                                    return "X";
                                }
                                else {
                                    return "";
                                }
                            }
                        },
                        {
                            xtype: "actioncolumn",
                            header: "Info",
                            name: "info",
                            align: "center",
                            sortable: false,
                            menuDisabled: true,
                            width: 85,
                            items: [
                                {
                                    iconCls: "x-fa fa-info-circle faIconAlt",
                                    tooltip: "Aufrufen des Index",
                                    handler: "onOpenIndex"
                                }
                            ]
                        }
                    ]
                },
                {
                    xtype: "fieldset",
                    title: "Parameter",
                    columnWidth: 0.65,
                    margin: "0 10 0 10",
                    layout: "anchor",
                    defaultType: "textfield",
                    items: [
                        {
                            xtype: "fieldcontainer",
                            layout: "hbox",
                            labelWidth: 0,
                            width: "100%",
                            items: [
                                {
                                    xtype: "button",
                                    id: "add_elastic_index",
                                    margin: "0 20 5 0",
                                    tooltip: "Neuen Index hinzufügen",
                                    autoEl: {
                                        tag: "div",
                                        "data-qtip": "Neuen Index hinzufügen"
                                    },
                                    text: "Neu",
                                    listeners: {
                                        click: "onNewElasticIndex"
                                    }
                                },
                                {
                                    xtype: "button",
                                    id: "delete_elastic_index",
                                    margin: "0 0 5 0",
                                    tooltip: "Index löschen",
                                    autoEl: {
                                        tag: "div",
                                        "data-qtip": "Index löschen"
                                    },
                                    text: "Löschen",
                                    listeners: {
                                        click: "onDeleteElasticIndex"
                                    }
                                }
                            ]
                        },
                        {
                            fieldLabel: "Name",
                            labelWidth: 160,
                            id: "elasticindexname",
                            bind: "{ElasticIndex.name}",
                            width: "100%"
                        },
                        {
                            fieldLabel: "Server",
                            labelWidth: 160,
                            id: "elasticindexserver",
                            bind: "{ElasticIndex.server}",
                            width: "100%"
                        },
                        {
                            xtype: "checkboxfield",
                            fieldLabel: "Nur produktive Layer",
                            id: "elasticindexonlyprod",
                            labelWidth: 160,
                            bind: "{ElasticIndex.only_prod}",
                            listeners: {
                                change: "onElasticOnlyProdStageChange"
                            }
                        },
                        {
                            xtype: "checkboxfield",
                            fieldLabel: "Nur dev und stage Layer",
                            id: "elasticindexonlystage",
                            labelWidth: 160,
                            bind: "{ElasticIndex.only_stage}",
                            listeners: {
                                change: "onElasticOnlyProdStageChange"
                            }
                        },
                        {
                            xtype: "checkboxfield",
                            fieldLabel: "Inklusive interne Layer",
                            id: "elasticindexinternal",
                            labelWidth: 160,
                            bind: "{ElasticIndex.internal}"
                        },
                        {
                            fieldLabel: "Nur Datensätze mit diesen Schlagworten",
                            labelWidth: 160,
                            id: "elasticindexkeywordfilterinclude",
                            bind: "{ElasticIndex.keyword_filter_include}",
                            width: "100%"
                        },
                        {
                            fieldLabel: "Datensätze mit diesen Schlagworten ausschließen",
                            labelWidth: 160,
                            id: "elasticindexkeywordfilterexclude",
                            bind: "{ElasticIndex.keyword_filter_exclude}",
                            width: "100%"
                        }
                    ]
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        border: true,
        style: "border-top: 1px solid #cfcfcf !important;",
        items: [
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                xtype: "button",
                id: "saveelasticconfig",
                text: "Speichern",
                scale: "medium",
                cls: "btn-save",
                margin: "5 10 10 5",
                listeners: {
                    click: "onSaveElasticConfig"
                }
            }
        ]
    }]
});
