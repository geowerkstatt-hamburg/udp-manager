Ext.define("UDPManager.view.management.settings.SourceDbConnections", {
    extend: "Ext.panel.Panel",
    xtype: "settings_sourcedbconnections",
    controller: "settings",
    title: "Quelldatenbank-Verbindungen",
    // cls: "prio-fieldset",
    // collapsible: true,
    // collapsed: true,
    layout: "column",
    viewModel: {
        data: {
            ConfigSourceDbConnection: null
        }
    },
    items: [
        {
            xtype: "gridpanel",
            id: "config_source_db_connections_grid",
            autoScroll: true,
            height: 700,
            columnWidth: 0.35,
            bind: {
                selection: "{ConfigSourceDbConnection}"
            },
            store: "ConfigSourceDbConnections",
            columns: [
                {
                    dataIndex: "name",
                    text: "Bezeichnung",
                    align: "center",
                    flex: 1
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Parameter",
            columnWidth: 0.65,
            margin: "0 10 0 10",
            layout: "anchor",
            defaultType: "textfield",
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    labelWidth: 0,
                    width: "100%",
                    items: [
                        {
                            xtype: "button",
                            id: "add_sourcedbconnection",
                            margin: "0 20 5 0",
                            tooltip: "Neue Quelldatenbankverbindung eingeben",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Neue Quellatenbankverbindung eingeben"
                            },
                            text: "Neu",
                            listeners: {
                                click: "onNewSourceDbConnection"
                            }
                        },
                        {
                            xtype: "button",
                            id: "delete_sourcedbconnection",
                            margin: "0 0 5 0",
                            tooltip: "Quelldatenbankverbindung löschen",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Quelldatenbankverbindung löschen"
                            },
                            text: "Löschen",
                            listeners: {
                                click: "onDeleteSourceDbConnection"
                            }
                        }
                    ]
                },
                {
                    fieldLabel: "Bezeichnung",
                    labelWidth: 160,
                    id: "sourcedbconnectionname",
                    bind: "{ConfigSourceDbConnection.name}",
                    width: "100%"
                },
                {
                    fieldLabel: "DBMS",
                    labelWidth: 160,
                    id: "sourcedbconnectiondbms",
                    bind: "{ConfigSourceDbConnection.dbms}",
                    width: "100%"
                },
                {
                    fieldLabel: "Host",
                    labelWidth: 160,
                    id: "sourcedbconnectionhost",
                    bind: "{ConfigSourceDbConnection.host}",
                    width: "100%"
                },
                {
                    fieldLabel: "Port",
                    labelWidth: 160,
                    id: "sourcedbconnectionport",
                    bind: "{ConfigSourceDbConnection.port}",
                    width: "100%"
                },
                {
                    fieldLabel: "Datenbankname",
                    labelWidth: 160,
                    id: "sourcedbconnectiondatabase",
                    bind: "{ConfigSourceDbConnection.database}",
                    width: "100%"
                },
                {
                    fieldLabel: "DB-User",
                    labelWidth: 160,
                    id: "sourcedbconnectionuser",
                    bind: "{ConfigSourceDbConnection.user}",
                    width: "100%"
                },
                {
                    fieldLabel: "Passwort",
                    inputType: "password",
                    labelWidth: 160,
                    id: "sourcedbconnectionpassword",
                    bind: "{ConfigSourceDbConnection.password}",
                    width: "100%"
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        items: [
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                xtype: "button",
                id: "save_sourcedbconnection",
                margin: "10 10 5 0",
                scale: "medium",
                cls: "btn-save",
                text: "Speichern",
                listeners: {
                    click: "onSaveSourceDbConnection"
                }
            }
        ]
    }]
});
