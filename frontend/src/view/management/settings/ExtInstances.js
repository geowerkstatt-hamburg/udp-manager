Ext.define("UDPManager.view.management.settings.ExtInstances", {
    extend: "Ext.panel.Panel",
    xtype: "settings_extinstances",
    controller: "settings",
    title: "Externe UDP-Manager-Instanzen",
    // cls: "prio-fieldset",
    // collapsible: true,
    // collapsed: true,
    layout: "column",
    height: 400,
    viewModel: {
        data: {
            ConfigExtInstance: null
        }
    },
    items: [
        {
            xtype: "gridpanel",
            id: "config_extinstances_grid",
            autoScroll: true,
            height: 300,
            columnWidth: 0.35,
            bind: {
                selection: "{ConfigExtInstance}"
            },
            store: "ConfigExtInstances",
            columns: [
                {
                    dataIndex: "name",
                    text: "Bezeichnung",
                    align: "center",
                    flex: 1
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Parameter",
            columnWidth: 0.65,
            margin: "0 10 0 10",
            layout: "anchor",
            defaultType: "textfield",
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    labelWidth: 0,
                    width: "100%",
                    items: [
                        {
                            xtype: "button",
                            id: "add_extinstance",
                            margin: "0 20 5 0",
                            tooltip: "Neue UDP-Manager-Instanz eingeben",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Neue UDP-Manager-Instanz eingeben"
                            },
                            text: "Neu",
                            listeners: {
                                click: "onNewConfigExtInstance"
                            }
                        },
                        {
                            xtype: "button",
                            id: "delete_extinstance",
                            margin: "0 0 5 0",
                            tooltip: "UDP-Manager Instanz löschen",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "UDP-Manager Instanz löschen"
                            },
                            text: "Löschen",
                            listeners: {
                                click: "onDeleteConfigExtInstance"
                            }
                        }
                    ]
                },
                {
                    fieldLabel: "Bezeichnung",
                    labelWidth: 160,
                    id: "configextinstancename",
                    bind: "{ConfigExtInstance.name}",
                    width: "100%"
                },
                {
                    fieldLabel: "Host",
                    labelWidth: 160,
                    id: "configextinstancehost",
                    bind: "{ConfigExtInstance.host}",
                    width: "100%"
                },
                {
                    fieldLabel: "Port",
                    labelWidth: 160,
                    id: "configextinstanceport",
                    bind: "{ConfigExtInstance.port}",
                    width: "100%"
                },
                {
                    fieldLabel: "Datenbankname",
                    labelWidth: 160,
                    id: "configextinstancedatabase",
                    bind: "{ConfigExtInstance.database}",
                    width: "100%"
                },
                {
                    fieldLabel: "DB-User",
                    labelWidth: 160,
                    id: "configextinstancedbuser",
                    bind: "{ConfigExtInstance.user}",
                    width: "100%"
                },
                {
                    fieldLabel: "Passwort",
                    inputType: "password",
                    labelWidth: 160,
                    id: "configextinstancepassword",
                    bind: "{ConfigExtInstance.password}",
                    width: "100%"
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        border: true,
        style: "border-top: 1px solid #cfcfcf !important;",
        items: [
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                xtype: "button",
                id: "save_extinstance",
                margin: "10 10 5 0",
                scale: "medium",
                cls: "btn-save",
                text: "Speichern",
                listeners: {
                    click: "onSaveConfigExtInstance"
                }
            }
        ]
    }]
});
