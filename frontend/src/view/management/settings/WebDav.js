Ext.define("UDPManager.view.management.settings.WebDav", {
    extend: "Ext.panel.Panel",
    xtype: "settings_webdav",
    controller: "settings",
    title: "WebDAV (Masterportal Server)",
    // cls: "prio-fieldset",
    // collapsible: true,
    // collapsed: true,
    layout: "column",
    height: 400,
    viewModel: {
        data: {
            ConfigWebDav: null
        }
    },
    items: [
        {
            xtype: "gridpanel",
            id: "config_webdav_grid",
            autoScroll: true,
            height: 300,
            columnWidth: 0.35,
            bind: {
                selection: "{ConfigWebDav}"
            },
            store: "ConfigWebDavs",
            columns: [
                {
                    dataIndex: "name",
                    text: "Bezeichnung",
                    align: "center",
                    flex: 1
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Parameter",
            columnWidth: 0.65,
            margin: "0 10 0 10",
            layout: "anchor",
            defaultType: "textfield",
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    labelWidth: 0,
                    width: "100%",
                    items: [
                        {
                            xtype: "button",
                            id: "new_configwebdav",
                            margin: "0 20 5 0",
                            tooltip: "Neue WebDAV Verbindung eingeben",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Neue WebDAV Verbindung eingeben"
                            },
                            text: "Neu",
                            listeners: {
                                click: "onNewConfigWebDav"
                            }
                        },
                        {
                            xtype: "button",
                            id: "delete_configwebdav",
                            margin: "0 0 5 0",
                            tooltip: "WebDAV Verbindung löschen",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "WebDAV Verbindung löschen"
                            },
                            text: "Löschen",
                            listeners: {
                                click: "onDeleteConfigWebDav"
                            }
                        }
                    ]
                },
                {
                    fieldLabel: "Bezeichnung",
                    labelWidth: 160,
                    id: "configwebdavname",
                    bind: "{ConfigWebDav.name}",
                    width: "100%"
                },
                {
                    fieldLabel: "Basis-URL",
                    labelWidth: 160,
                    id: "configwebdavbaseurl",
                    bind: "{ConfigWebDav.base_url}",
                    width: "100%"
                },
                {
                    fieldLabel: "Verezeichnis",
                    labelWidth: 160,
                    id: "configwebdavdirectory",
                    bind: "{ConfigWebDav.directory}",
                    width: "100%"
                },
                {
                    fieldLabel: "Vollqualifizierte Domain",
                    labelWidth: 160,
                    id: "configwebdavfullqualifieddomain",
                    bind: "{ConfigWebDav.full_qualified_domain}",
                    width: "100%"
                },
                {
                    fieldLabel: "Benutzername",
                    labelWidth: 160,
                    id: "configwebdavusername",
                    bind: "{ConfigWebDav.username}",
                    width: "100%"
                },
                {
                    fieldLabel: "Passwort",
                    inputType: "password",
                    labelWidth: 160,
                    id: "configwebdavpassword",
                    bind: "{ConfigWebDav.password}",
                    width: "100%"
                },
                {
                    xtype: "checkbox",
                    labelWidth: 160,
                    fieldLabel: "Proxy",
                    id: "configwebdavproxy",
                    bind: "{ConfigWebDav.proxy}",
                    width: "100%"
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        border: true,
        style: "border-top: 1px solid #cfcfcf !important;",
        items: [
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                xtype: "button",
                id: "save_configwebdav",
                margin: "10 10 5 0",
                text: "Speichern",
                scale: "medium",
                cls: "btn-save",
                listeners: {
                    click: "onSaveConfigWebDav"
                }
            }
        ]
    }]
});
