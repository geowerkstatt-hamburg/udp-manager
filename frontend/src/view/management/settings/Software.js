Ext.define("UDPManager.view.management.settings.Software", {
    extend: "Ext.panel.Panel",
    xtype: "settings_software",
    controller: "settings",
    title: "Software",
    layout: "column",
    height: 400,
    viewModel: {
        data: {
            ConfigSoftware: null
        }
    },
    items: [
        {
            xtype: "gridpanel",
            id: "config_software_grid",
            autoScroll: true,
            height: 500,
            columnWidth: 0.25,
            bind: {
                selection: "{ConfigSoftware}"
            },
            store: "ConfigSoftware",
            listeners: {
                select: "onSoftwareItemSelected"
            },
            columns: [
                {
                    dataIndex: "name",
                    text: "Bezeichnung",
                    align: "center",
                    flex: 1
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Parameter",
            columnWidth: 0.75,
            margin: "0 10 0 10",
            layout: "anchor",
            defaultType: "textfield",
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    labelWidth: 0,
                    width: "100%",
                    items: [
                        {
                            xtype: "button",
                            id: "new_configsoftware",
                            margin: "0 20 5 0",
                            tooltip: "Neue Software Konfiguration eingeben",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Neue Software Konfiguration eingeben"
                            },
                            text: "Neu",
                            listeners: {
                                click: "onNewConfigSoftware"
                            }
                        },
                        {
                            xtype: "button",
                            id: "delete_configsoftware",
                            margin: "0 0 5 0",
                            tooltip: "Software Konfiguration löschen",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Software Konfiguration löschen"
                            },
                            text: "Löschen",
                            listeners: {
                                click: "onDeleteConfigSoftware"
                            }
                        }
                    ]
                },
                {
                    fieldLabel: "Bezeichnung",
                    labelWidth: 160,
                    id: "configsoftwarename",
                    bind: "{ConfigSoftware.name}",
                    width: "100%"
                },
                {
                    xtype: "textareafield",
                    labelWidth: 0,
                    id: "configsoftwarejson",
                    bind: "{ConfigSoftware.json}",
                    width: "100%",
                    height: 650
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        border: true,
        style: "border-top: 1px solid #cfcfcf !important;",
        items: [
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                xtype: "button",
                id: "save_configsoftware",
                margin: "10 10 5 0",
                text: "Speichern",
                scale: "medium",
                cls: "btn-save",
                listeners: {
                    click: "onSaveConfigApp"
                }
            }
        ]
    }]
});
