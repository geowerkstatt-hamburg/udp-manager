Ext.define("UDPManager.view.management.settings.App", {
    extend: "Ext.panel.Panel",
    xtype: "settings_app",
    controller: "settings",
    title: "UDP-Manager",
    layout: "vbox",
    defaultType: "textareafield",
    height: 1600,
    scrollable: true,
    autoScroll: true,
    items: [
        {
            fieldLabel: "Server",
            labelWidth: 160,
            id: "configappserver",
            width: "100%",
            height: 500
        },
        {
            fieldLabel: "Schnittstellen Typen",
            labelWidth: 160,
            id: "configappservicetypes",
            width: "100%",
            height: 500
        },
        {
            fieldLabel: "Test BBoxes",
            labelWidth: 160,
            id: "configapptestbbox",
            width: "100%",
            height: 400
        },
        {
            xtype: "textfield",
            fieldLabel: "Default BBox",
            labelWidth: 160,
            id: "configappdefaultbbox",
            width: "100%"
        },
        {
            xtype: "textfield",
            fieldLabel: "Unterstützte Projektionen",
            labelWidth: 160,
            id: "configappavailableprojections",
            width: "100%"
        },
        {
            xtype: "textfield",
            fieldLabel: "Attribute in Layer-JSON ignorieren",
            labelWidth: 160,
            id: "configapplayerattributesexcludes",
            width: "100%"
        },
        {
            fieldLabel: "Client Konfiguration",
            labelWidth: 160,
            id: "configappclientconfig",
            width: "100%",
            height: 500
        },
        {
            fieldLabel: "Capabilities Metadaten",
            labelWidth: 160,
            id: "configappcapabilitiesmetadata",
            width: "100%",
            height: 310
        },
        {
            fieldLabel: "LDAP",
            labelWidth: 160,
            id: "configappldap",
            width: "100%",
            height: 330
        },
        {
            xtype: "checkbox",
            labelWidth: 160,
            fieldLabel: "JIRA Modul aktivieren",
            id: "configappjiraticketsmodule",
            width: "100%",
            listeners: {
                change: "onJiraModuleChange"
            }
        },
        {
            fieldLabel: "JIRA",
            labelWidth: 160,
            id: "configappjira",
            disabled: true,
            width: "100%",
            height: 500
        },
        {
            fieldLabel: "Mail",
            labelWidth: 160,
            id: "configappmail",
            width: "100%",
            height: 140
        },
        {
            xtype: "checkbox",
            labelWidth: 160,
            fieldLabel: "git Modul aktivieren",
            id: "configappgitmodule",
            width: "100%",
            listeners: {
                change: "onGitModuleChange"
            }
        },
        {
            fieldLabel: "git",
            labelWidth: 160,
            id: "configappgit",
            disabled: true,
            width: "100%",
            height: 120
        },
        {
            xtype: "checkbox",
            labelWidth: 160,
            fieldLabel: "Modul geschützte Schnittstellen",
            id: "configappservicesecuritymodule",
            width: "100%"
        },
        {
            xtype: "checkbox",
            labelWidth: 160,
            fieldLabel: "Modul Zugriffsstatistiken",
            id: "configappvisitstatisticsmodule",
            width: "100%"
        },
        {
            xtype: "checkbox",
            labelWidth: 160,
            fieldLabel: "Modul Elasticsearch",
            id: "configappelasticsearchmodule",
            width: "100%"
        },
        {
            xtype: "checkbox",
            labelWidth: 160,
            fieldLabel: "Modul externer Collection-Import",
            id: "configappexternalcollectionimportmodule",
            width: "100%"
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        border: true,
        style: "border-top: 1px solid #cfcfcf !important;",
        items: [
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                xtype: "button",
                id: "save_configapp",
                margin: "10 10 5 0",
                text: "Speichern",
                scale: "medium",
                cls: "btn-save",
                listeners: {
                    click: "onSaveConfigApp"
                }
            }
        ]
    }]
});
