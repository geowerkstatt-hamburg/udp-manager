Ext.define("UDPManager.view.management.settings.DbConnections", {
    extend: "Ext.panel.Panel",
    xtype: "settings_dbconnections",
    controller: "settings",
    title: "Datenbank-Verbindungen",
    // cls: "prio-fieldset",
    // collapsible: true,
    // collapsed: true,
    layout: "column",
    viewModel: {
        data: {
            ConfigDbConnection: null
        }
    },
    items: [
        {
            xtype: "gridpanel",
            id: "config_db_connections_grid",
            autoScroll: true,
            height: 700,
            columnWidth: 0.35,
            bind: {
                selection: "{ConfigDbConnection}"
            },
            store: "ConfigDbConnections",
            columns: [
                {
                    dataIndex: "name",
                    text: "Bezeichnung",
                    align: "center",
                    flex: 1
                },
                {
                    dataIndex: "use_as_default_r",
                    width: 30,
                    align: "center",
                    sortable: false,
                    menuDisabled: true,
                    resizeable: false,
                    renderer: function (value, metaData, record) {
                        if (record.data.use_as_default_r) {
                            return "<div class=\"x-fa fa-file-alt\" style=\"color:#00000066\"></div>";
                        }
                        else if (record.data.use_as_default_w) {
                            return "<div class=\"x-fa fa-pen\" style=\"color:#00000066\"></div>";
                        }
                        else {
                            return "<div></div>";
                        }
                    }
                }
            ]
        },
        {
            xtype: "fieldset",
            title: "Parameter",
            columnWidth: 0.65,
            margin: "0 10 0 10",
            layout: "anchor",
            defaultType: "textfield",
            items: [
                {
                    xtype: "fieldcontainer",
                    layout: "hbox",
                    labelWidth: 0,
                    width: "100%",
                    items: [
                        {
                            xtype: "button",
                            id: "add_dbconnection",
                            margin: "0 20 5 0",
                            tooltip: "Neue Datenbankverbindung eingeben",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Neue Datenbankverbindung eingeben"
                            },
                            text: "Neu",
                            listeners: {
                                click: "onNewDbConnection"
                            }
                        },
                        {
                            xtype: "button",
                            id: "delete_dbconnection",
                            margin: "0 0 5 0",
                            tooltip: "Datenbankverbindung löschen",
                            autoEl: {
                                tag: "div",
                                "data-qtip": "Datenbankverbindung löschen"
                            },
                            text: "Löschen",
                            listeners: {
                                click: "onDeleteDbConnection"
                            }
                        }
                    ]
                },
                {
                    fieldLabel: "Bezeichnung",
                    labelWidth: 160,
                    id: "dbconnectionname",
                    bind: "{ConfigDbConnection.name}",
                    width: "100%"
                },
                {
                    fieldLabel: "Host",
                    labelWidth: 160,
                    id: "dbconnectionhost",
                    bind: "{ConfigDbConnection.host}",
                    width: "100%"
                },
                {
                    fieldLabel: "Port",
                    labelWidth: 160,
                    id: "dbconnectionport",
                    bind: "{ConfigDbConnection.port}",
                    width: "100%"
                },
                {
                    fieldLabel: "Datenbankname",
                    labelWidth: 160,
                    id: "dbconnectiondatabase",
                    bind: "{ConfigDbConnection.database}",
                    width: "100%"
                },
                {
                    fieldLabel: "DB-User",
                    labelWidth: 160,
                    id: "dbconnectionuser",
                    bind: "{ConfigDbConnection.user}",
                    width: "100%"
                },
                {
                    fieldLabel: "Passwort",
                    inputType: "password",
                    labelWidth: 160,
                    id: "dbconnectionpassword",
                    bind: "{ConfigDbConnection.password}",
                    width: "100%"
                },
                {
                    xtype: "checkbox",
                    labelWidth: 160,
                    fieldLabel: "Default Lesend",
                    id: "dbconnectionuseasdefaultr",
                    bind: "{ConfigDbConnection.use_as_default_r}",
                    width: "100%"
                },
                {
                    xtype: "checkbox",
                    labelWidth: 160,
                    fieldLabel: "Default Schreibend",
                    id: "dbconnectionuseasdefaultw",
                    bind: "{ConfigDbConnection.use_as_default_w}",
                    width: "100%"
                }
            ]
        }
    ],
    dockedItems: [{
        xtype: "toolbar",
        dock: "bottom",
        items: [
            {
                xtype: "component",
                flex: 1,
                html: ""
            },
            {
                xtype: "button",
                id: "save_dbconnection",
                margin: "10 10 5 0",
                scale: "medium",
                cls: "btn-save",
                text: "Speichern",
                listeners: {
                    click: "onSaveDbConnection"
                }
            }
        ]
    }]
});
