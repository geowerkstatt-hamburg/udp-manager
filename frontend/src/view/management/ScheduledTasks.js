Ext.define("UDPManager.view.management.ScheduledTasks", {
    extend: "Ext.container.Container",
    xtype: "scheduledtasks",
    controller: "scheduledtasks",
    items: [
        {
            xtype: "grid",
            store: "ScheduledTasks",
            autoScroll: true,
            height: 400,
            id: "scheduledtasks-grid",
            viewConfig: {
                enableTextSelection: true,
                stripeRows: false
            },
            columnLines: true,
            selModel: "rowmodel",
            plugins: [
                {
                    ptype: "cellediting",
                    clicksToMoveEditor: 1
                },
                {
                    ptype: "rowexpander",
                    rowBodyTpl: new Ext.XTemplate("<p>{message}</p>")
                }
            ],
            columns: [
                {
                    dataIndex: "name",
                    header: "Name",
                    align: "left",
                    width: 200
                },
                {
                    dataIndex: "description",
                    header: "Beschreibung",
                    align: "left",
                    flex: 1
                },
                {
                    dataIndex: "last_run",
                    header: "Letzte Ausführung",
                    align: "center",
                    width: 200
                },
                {
                    dataIndex: "status",
                    header: "Status",
                    align: "center",
                    width: 200
                },
                {
                    dataIndex: "schedule",
                    header: "Zeitplan",
                    align: "center",
                    width: 300,
                    editor: {
                        allowBlank: false
                    }
                },
                {
                    xtype: "checkcolumn",
                    dataIndex: "enabled",
                    header: "aktiv",
                    align: "center",
                    width: 120,
                    editor: {
                        xtype: "checkbox"
                    }
                },
                {
                    xtype: "checkcolumn",
                    dataIndex: "execute_on_startup",
                    header: "initial Ausführen",
                    align: "center",
                    width: 120,
                    editor: {
                        xtype: "checkbox"
                    }
                },
                {
                    xtype: "actioncolumn",
                    header: "Starten",
                    align: "center",
                    width: 100,
                    items: [
                        {
                            iconCls: "x-fa fa-play faIcon",
                            tooltip: "Aufgabe starten",
                            handler: "onStartScheduledTask"
                        }
                    ],
                    renderer: function (value, meta, record) {
                        if (record.get("status") === "in Ausführung") {
                            this.items[0].iconCls = "x-fa fa-times faIcon";
                            this.items[0].tooltip = "Aufgabe läuft";
                        }
                        else {
                            this.items[0].iconCls = "x-fa fa-play faIcon";
                            this.items[0].tooltip = "Aufgabe starten";
                            this.items[0].handler = "onStartScheduledTask";
                        }
                    }
                }
            ],
            dockedItems: [{
                xtype: "toolbar",
                dock: "bottom",
                items: [
                    {
                        xtype: "component",
                        flex: 1,
                        html: ""
                    },
                    {
                        iconCls: "x-fa fa-redo",
                        scale: "medium",
                        cls: "custom-btn-icon",
                        handler: "getStatus"
                    },
                    {
                        text: "Speichern",
                        scale: "medium",
                        cls: "btn-save",
                        handler: "saveChanges"
                    }
                ]
            }]
        }
    ]
});
