Ext.define("UDPManager.view.management.ContactsController", {
    extend: "Ext.app.ViewController",
    alias: "controller.contacts",

    init: function () {
        const contactDetailsButtons = Ext.getCmp("contactdetailsbuttons");

        contactDetailsButtons.add({
            xtype: "button",
            id: "new_contact",
            margin: "0 20 5 0",
            tooltip: "Neuen Kontakt manuell eingeben",
            text: "Neu",
            listeners: {
                click: "onNewContact"
            }
        });

        if (UDPManager.Configs.getAuthMethod() === "ldap") {
            contactDetailsButtons.add({
                xtype: "button",
                id: "new_ad_contact",
                margin: "0 20 5 0",
                tooltip: "Neuen Kontakt aus LDAP importieren",
                text: "Neu aus LDAP",
                listeners: {
                    click: "onNewADContact"
                }
            });
        }

        contactDetailsButtons.add({
            xtype: "button",
            id: "delete_contact",
            margin: "0 0 5 0",
            tooltip: "Kontakt löschen",
            text: "Löschen",
            listeners: {
                click: "onDeleteContact"
            }
        });
    },

    onSelectionChange: function (selModel, records) {
        const rec = records[0];

        if (rec) {
            this.getView().getForm().loadRecord(rec);
        }
    },

    onSaveContact: function () {
        const selection = Ext.getCmp("contacts_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const data = selection[0].data;

            Ext.Ajax.request({
                url: "backend/updatecontact",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    contact_id: data.contact_id,
                    given_name: data.given_name,
                    surname: data.surname,
                    name: data.surname + ", " + data.given_name,
                    company: data.company,
                    ad_account: data.ad_account,
                    email: data.email,
                    tel: data.tel,
                    objectguid: data.objectguid
                },
                success: function () {
                    Ext.getStore("Contacts").load();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else if (selection.length === 0 && Ext.getCmp("contactgivenname").getSubmitValue() !== "" && Ext.getCmp("contactsurname").getSubmitValue() !== "") {
            const contact = Ext.getStore("Contacts").findRecord("email", Ext.getCmp("contactemail").getSubmitValue(), 0, false, false, true);

            if (!contact) {
                Ext.Ajax.request({
                    url: "backend/addcontact",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        given_name: Ext.getCmp("contactgivenname").getSubmitValue(),
                        surname: Ext.getCmp("contactsurname").getSubmitValue(),
                        name: Ext.getCmp("contactgivenname").getSubmitValue() + ", " + Ext.getCmp("contactsurname").getSubmitValue(),
                        company: Ext.getCmp("contactcompany").getSubmitValue(),
                        ad_account: Ext.getCmp("contactadaccount").getSubmitValue(),
                        email: Ext.getCmp("contactemail").getSubmitValue(),
                        tel: Ext.getCmp("contacttel").getSubmitValue(),
                        objectguid: ""
                    },
                    success: function () {
                        Ext.getStore("Contacts").load();
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                    }
                });
            }
            else {
                Ext.MessageBox.alert("Achtung", "Kontakt schon vorhanden");
            }

        }
    },

    onNewContact: function () {
        Ext.getCmp("contacts_grid").getSelectionModel().deselectAll();
    },

    onNewADContact: function () {
        let newADContactWindow = Ext.getCmp("newadcontact-window");

        if (!newADContactWindow) {
            newADContactWindow = Ext.create("contacts.newadcontact");
        }
        newADContactWindow.show();
    },

    onDeleteContact: function () {
        const selection = Ext.getCmp("contacts_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Kontakt Löschen", "wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    const data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deletecontact",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            id: data.id
                        },
                        success: function () {
                            Ext.getStore("Contacts").load();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    }
});
