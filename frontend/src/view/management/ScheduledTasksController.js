Ext.define("UDPManager.view.management.ScheduledTasksController", {
    extend: "Ext.app.ViewController",
    alias: "controller.scheduledtasks",

    onStartScheduledTask: function (grid, rowIndex) {
        const store = grid.getStore();
        const model = store.getAt(rowIndex);

        if (model.get("status") === "in Ausführung") {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Zurücksetzen", "Status zurück setzen? Die laufende Aufgabe wird nicht abgebrochen!", function (btn) {
                if (btn === "yes") {
                    model.set("status", "Abgebrochen");

                    Ext.Ajax.request({
                        url: "backend/cancelscheduledtask",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            task_name: model.data.name
                        }
                    });
                }
            });
        }
        else {
            Ext.Ajax.request({
                url: "backend/startscheduledtask",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    task_name: model.data.name
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        model.set("status", "in Ausführung");
                    }
                    else if (response_json.status === "warning") {
                        model.set("status", "in Ausführung");
                        Ext.MessageBox.alert("Warnung", "Aufgabe wird bereits ausgeführt!");
                    }
                    else {
                        model.set("status", "Fehler");
                    }
                },
                failure: function () {
                    model.set("status", "Fehler");
                }
            });
        }
    },

    getStatus: function () {
        Ext.getStore("ScheduledTasks").load();
    },

    saveChanges: function () {
        const updated_records = Ext.getStore("ScheduledTasks").getUpdatedRecords();
        const data = [];
        let valid_cron = true;

        for (let i = 0; i < updated_records.length; i++) {
            const cronregex = new RegExp(/^(\*|([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])|\*\/([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])) (\*|([0-9]|1[0-9]|2[0-3])|\*\/([0-9]|1[0-9]|2[0-3])) (\*|([1-9]|1[0-9]|2[0-9]|3[0-1])|\*\/([1-9]|1[0-9]|2[0-9]|3[0-1])) (\*|([1-9]|1[0-2])|\*\/([1-9]|1[0-2])) (\*|([0-6])|\*\/([0-6]))$/);

            if (!cronregex.test(updated_records[i].data.schedule)) {
                valid_cron = false;
            }

            data.push({
                name: updated_records[i].data.name,
                schedule: updated_records[i].data.schedule,
                enabled: updated_records[i].data.enabled,
                execute_on_startup: updated_records[i].data.execute_on_startup
            });
        }

        if (valid_cron && data.length > 0) {
            Ext.Ajax.request({
                url: "backend/updatescheduledtasks",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    tasks: data
                },
                success: function () {
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Aufgaben erfolgreich gespeichert!");
                    Ext.getStore("ScheduledTasks").load();
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else if (!valid_cron && data.length > 0) {
            Ext.Msg.alert("Format ungültig", "cron Formateingabe ungültig");
        }
    }
});
