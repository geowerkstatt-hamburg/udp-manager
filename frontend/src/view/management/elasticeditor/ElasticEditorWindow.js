Ext.define("UDPManager.view.management.elasticeditor.ElasticEditorWindow", {
    extend: "Ext.window.Window",
    alias: "management.ElasticEditorWindow",
    id: "elasticeditor-window",
    height: Ext.Element.getViewportHeight() - 120,
    width: Ext.Element.getViewportWidth() - 120,
    resizable: false,
    title: "Elasticsearch Konfiguration",
    header: true,
    controller: "settings",

    initComponent: function () {
        this.items = [
            {
                xtype: "panel",
                id: "configview-panel",
                height: Ext.Element.getViewportHeight() - 200,
                autoScroll: true,
                html: "<div id=\"elasticconfigcontainer\" style=\"width: " + parseInt(Ext.Element.getViewportWidth() - 122) + "px; height: " + parseInt(Ext.Element.getViewportHeight() - 200) + "px; border: 1px solid grey\"></div>"
            },
            {
                xtype: "fieldcontainer",
                layout: "hbox",
                width: Ext.Element.getViewportWidth() - 122,
                items: [
                    {
                        xtype: "component",
                        flex: 1,
                        html: ""
                    },
                    {
                        xtype: "button",
                        id: "setelasticconfig",
                        text: "OK",
                        cls: "btn-save",
                        scale: "medium",
                        tooltip: "Die Konfiguration wird zwischengespeichert",
                        width: 140,
                        margin: "5 10 5 5",
                        listeners: {
                            click: "onSetElasticConfig"
                        }
                    }
                ]
            }
        ];
        this.callParent(arguments);
    }
});
