Ext.define("UDPManager.view.management.NewADContact", {
    extend: "Ext.window.Window",
    id: "newadcontact-window",
    alias: "contacts.newadcontact",
    height: 435,
    width: 730,
    controller: "newadcontact",
    title: "Benutzer im AD Suchen und hinzufügen",
    items: [
        {
            xtype: "fieldcontainer",
            layout: "hbox",
            labelWidth: 0,
            width: "100%",
            items: [
                {
                    xtype: "textfield",
                    id: "contact_search_field",
                    margin: "5 20 5 5",
                    width: 200,
                    tooltip: "Nach Kontakt suchen",
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Nach Kontakt suchen"
                    }
                }, {
                    xtype: "button",
                    id: "search_contact",
                    margin: "5 0 5 0",
                    tooltip: "Suche nach Kontakt starten",
                    autoEl: {
                        tag: "div",
                        "data-qtip": "Suche nach Kontakt starten"
                    },
                    iconCls: "right-icon white-icon pictos pictos-search",
                    listeners: {
                        click: "onSearchContact"
                    }
                }
            ]
        },
        {
            xtype: "grid",
            store: "ADContacts",
            id: "adcontacts-grid",
            height: 310,
            autoScroll: true,
            columns: [
                {
                    header: "Accountname",
                    dataIndex: "sAMAccountName",
                    align: "left",
                    flex: 1
                },
                {
                    header: "Vorname",
                    dataIndex: "givenName",
                    align: "left",
                    flex: 1
                }, {
                    header: "Nachname",
                    dataIndex: "sn",
                    align: "left",
                    flex: 1
                }, {
                    header: "Unternehmen",
                    dataIndex: "company",
                    align: "left",
                    flex: 2
                }
            ],
            columnLines: true,
            selModel: {
                type: "checkboxmodel",
                checkOnly: false,
                mode: "SINGLE"
            }
        }
    ],
    buttons: [{
        text: "",
        iconCls: "right-icon white-icon pictos pictos-add",
        tooltip: "Der gewählte Kontakt wird in die Liste hinzugefügt.",
        listeners: {
            click: "onAddContact"
        }
    }]
});
