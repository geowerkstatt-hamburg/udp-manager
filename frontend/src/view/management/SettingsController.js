Ext.define("UDPManager.view.management.SettingsController", {
    extend: "Ext.app.ViewController",
    alias: "controller.settings",

    onSaveConfigMetadataCatalog: function () {
        const selection = Ext.getCmp("config_metadatacatalogs_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const data = selection[0].data;

            Ext.Ajax.request({
                url: "backend/updateconfigmetadatacatalog",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    id: data.id,
                    name: data.name,
                    csw_url: data.csw_url,
                    show_doc_url: data.show_doc_url,
                    proxy: data.proxy,
                    use_in_internet_json: data.use_in_internet_json,
                    srs_metadata: parseInt(data.srs_metadata)
                },
                success: function () {
                    Ext.getStore("ConfigMetadataCatalogs").load();
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Metadatenkatalog erfolgreich gespeichert!");
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
        else if (selection.length === 0 && Ext.getCmp("configmetadatacatalogname").getSubmitValue() !== "" && Ext.getCmp("configmetadatacatalogcswurl").getSubmitValue() !== "") {
            const configMetadataCatalog_url = Ext.getStore("ConfigMetadataCatalogs").findRecord("csw_url", Ext.getCmp("configmetadatacatalogcswurl").getSubmitValue(), 0, false, false, true);

            if (!configMetadataCatalog_url) {
                const configMetadataCatalog_name = Ext.getStore("ConfigMetadataCatalogs").findRecord("name", Ext.getCmp("configmetadatacatalogname").getSubmitValue(), 0, false, false, true);

                if (!configMetadataCatalog_name) {
                    Ext.Ajax.request({
                        url: "backend/addconfigmetadatacatalog",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            name: Ext.getCmp("configmetadatacatalogname").getSubmitValue(),
                            csw_url: Ext.getCmp("configmetadatacatalogcswurl").getSubmitValue(),
                            show_doc_url: Ext.getCmp("configmetadatacatalogshowdocurl").getSubmitValue(),
                            proxy: Ext.getCmp("configmetadatacatalogproxy").getValue(),
                            use_in_internet_json: Ext.getCmp("configmetadatacataloguseininternetjson").getValue(),
                            srs_metadata: parseInt(Ext.getCmp("configmetadatacatalogsrs").getSubmitValue())
                        },
                        success: function () {
                            Ext.getStore("ConfigMetadataCatalogs").load();
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Metadatenkatalog erfolgreich gespeichert!");
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                            Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Achtung", "Name schon vergeben");
                }
            }
            else {
                Ext.MessageBox.alert("Achtung", "Metadatenkatalog schon vorhanden");
            }

        }
    },

    onNewConfigMetadataCatalog: function () {
        Ext.getCmp("config_metadatacatalogs_grid").getSelectionModel().deselectAll();
    },

    onDeleteConfigMetadataCatalog: function () {
        const selection = Ext.getCmp("config_metadatacatalogs_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Metadatenkatalog Löschen", "wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    const data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deleteconfigmetadatacatalog",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            id: data.id
                        },
                        success: function () {
                            Ext.getStore("ConfigMetadataCatalogs").load();
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Metadatenkatalog gelöscht!");
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                            Ext.MessageBox.alert("Fehler", "Operation konnte nicht ausgeführt werden");
                        }
                    });
                }
            });
        }
    },

    onNewConfigExtInstance: function () {
        Ext.getCmp("config_extinstances_grid").getSelectionModel().deselectAll();
    },

    onDeleteConfigExtInstance: function () {
        const selection = Ext.getCmp("config_extinstances_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "Ja";
            mb.buttonText.no = "Nein";

            mb.confirm("UDP-Manager-Instanz Löschen", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    const data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deleteconfigextinstance",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            id: data.id
                        },
                        success: function () {
                            Ext.getStore("ConfigExtInstances").load();
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onSaveConfigExtInstance: function () {
        const selection = Ext.getCmp("config_extinstances_grid").getSelectionModel().getSelection();
        const name = Ext.getCmp("configextinstancename").getSubmitValue();
        const database = Ext.getCmp("configextinstancedatabase").getSubmitValue();
        const host = Ext.getCmp("configextinstancehost").getSubmitValue();
        const port = Ext.getCmp("configextinstanceport").getSubmitValue();
        const user = Ext.getCmp("configextinstancedbuser").getSubmitValue();
        const password = Ext.getCmp("configextinstancepassword").getSubmitValue();

        if (selection.length === 1) {
            const data = selection[0].data;

            Ext.Ajax.request({
                url: "backend/updateconfigextinstance",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    id: data.id,
                    name: data.name,
                    host: data.host,
                    port: parseInt(data.port),
                    database: data.database,
                    user: data.user,
                    password: data.password
                },
                success: function (response) {
                    if (Ext.decode(response.responseText).error) {
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    }
                    else {
                        Ext.getStore("ConfigExtInstances").load();
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Externe Instanz erfolgreich gespeichert!");
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
        else if (selection.length === 0 && name !== "" && database !== "" && host !== "" && port !== "" && user !== "" && password !== "") {
            let configExtInstance = Ext.getStore("ConfigExtInstances").findRecord("name", name, 0, false, false, true);

            if (!configExtInstance) {
                configExtInstance = Ext.getStore("ConfigExtInstances").findRecord("host", host, 0, false, false, true);
                if (!configExtInstance) {
                    Ext.Ajax.request({
                        url: "backend/addconfigextinstance",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            name: name,
                            host: host,
                            port: parseInt(port),
                            database: database,
                            user: user,
                            password: password
                        },
                        success: function (response) {
                            if (Ext.decode(response.responseText).error) {
                                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                            }
                            else {
                                Ext.getStore("ConfigExtInstances").load();
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Externe Instanz erfolgreich gespeichert!");
                            }
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                            Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Achtung", "Name schon vergeben");
                }
            }
            else {
                Ext.MessageBox.alert("Achtung", "UDP-Manager-Instanz schon vorhanden");
            }
        }
    },

    onNewDbConnection: function () {
        Ext.getCmp("config_db_connections_grid").getSelectionModel().deselectAll();
    },

    onDeleteDbConnection: function () {
        const selection = Ext.getCmp("config_db_connections_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "Ja";
            mb.buttonText.no = "Nein";

            mb.confirm("Datenbankverbindung löschen", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    const data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deletedbconnection",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            id: data.id
                        },
                        success: function (response) {
                            const resp_json = Ext.decode(response.responseText);

                            if (resp_json.status === "error") {
                                if (resp_json.message === "linked_datasets") {
                                    Ext.MessageBox.alert("Achtung", "Löschen nicht möglich. Es sind noch Datensätze mit dieser Datenbankverbindung verknüpft!");
                                }
                            }
                            else {
                                Ext.getStore("ConfigDbConnections").load();
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Datenbankverbindung erfolgreich gelöscht!");
                            }
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onSaveDbConnection: function () {
        const selection = Ext.getCmp("config_db_connections_grid").getSelectionModel().getSelection();
        const database = Ext.getCmp("dbconnectiondatabase").getSubmitValue();
        const host = Ext.getCmp("dbconnectionhost").getSubmitValue();
        const port = Ext.getCmp("dbconnectionport").getSubmitValue();
        const user = Ext.getCmp("dbconnectionuser").getSubmitValue();
        const password = Ext.getCmp("dbconnectionpassword").getSubmitValue();
        const use_as_default_r = Ext.getCmp("dbconnectionuseasdefaultr").getValue();
        const use_as_default_w = Ext.getCmp("dbconnectionuseasdefaultw").getValue();
        let name = Ext.getCmp("dbconnectionname").getSubmitValue();

        name = name.replace(/ü/gi, "ue");
        name = name.replace(/ö/gi, "oe");
        name = name.replace(/ä/gi, "ae");
        name = name.replace(/ß/gi, "ss");
        name = name.replace(/ /gi, "_");

        Ext.getCmp("dbconnectionname").setValue(name);

        Ext.getStore("ConfigDbConnections").filter("use_as_default_r", true);

        const db_connection_r_count = Ext.getStore("ConfigDbConnections").count();

        Ext.getStore("ConfigDbConnections").clearFilter();

        Ext.getStore("ConfigDbConnections").filter("use_as_default_w", true);

        const db_connection_w_count = Ext.getStore("ConfigDbConnections").count();

        Ext.getStore("ConfigDbConnections").clearFilter();

        if (name.match(/^\d/)) {
            Ext.MessageBox.alert("Fehler", "Der DB-Verbindungsname darf nicht mit einer Zahl beginnen!");
        }
        else if (name.match(/[^a-z0-9_/]/)) {
            Ext.MessageBox.alert("Fehler", "Mit Ausnahme von UNDERSCORE sind Sonderzeichen und Großschreibung nicht zulässig. Um mehrere Wörter zu verbinden bitte Snakecase benutzen!");
        }
        else if (name.length > 60) {
            Ext.MessageBox.alert("Fehler", "Der DB-Verbindungsname darf nicht länger als 60 Zeichen sein!");
        }
        else if (name[0] === "_") {
            Ext.MessageBox.alert("Fehler", "Der DB-Verbindungsname darf nicht mit einem UNDERSCORE beginnen!");
        }
        else if (db_connection_r_count > 1) {
            Ext.MessageBox.alert("Fehler", "Default lesend bereits vergeben");
        }
        else if (db_connection_w_count > 1) {
            Ext.MessageBox.alert("Fehler", "Default schreibend bereits vergeben");
        }
        else {
            if (selection.length === 1) {
                const data = selection[0].data;

                Ext.Ajax.request({
                    url: "backend/updatedbconnection",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        id: data.id,
                        name: data.name,
                        host: data.host,
                        port: parseInt(data.port),
                        database: data.database,
                        user: data.user,
                        password: data.password,
                        use_as_default_r: data.use_as_default_r,
                        use_as_default_w: data.use_as_default_w
                    },
                    success: function (response) {
                        if (Ext.decode(response.responseText).error) {
                            Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                        }
                        else {
                            Ext.getStore("ConfigDbConnections").load({
                                callback: function () {
                                    const dbconnection = Ext.getStore("ConfigDbConnections").findRecord("id", data.id, 0, false, false, true);
                                    const config_db_connections_grid = Ext.getCmp("config_db_connections_grid");
                                    const row_dbconnection = config_db_connections_grid.store.indexOf(dbconnection);

                                    config_db_connections_grid.getSelectionModel().deselectAll();
                                    config_db_connections_grid.getSelectionModel().select(row_dbconnection);
                                    config_db_connections_grid.ensureVisible(row_dbconnection);
                                }
                            });
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Datenbankverbindung erfolgreich gespeichert!");
                        }
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    }
                });
            }
            else if (selection.length === 0 && name !== "" && database !== "" && host !== "" && port !== "" && user !== "" && password !== "") {
                const dbConnection = Ext.getStore("ConfigDbConnections").findRecord("name", name, 0, false, false, true);

                if (!dbConnection) {
                    Ext.Ajax.request({
                        url: "backend/adddbconnection",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            name: name,
                            host: host,
                            port: parseInt(port),
                            database: database,
                            user: user,
                            password: password,
                            use_as_default_r: use_as_default_r,
                            use_as_default_w: use_as_default_w
                        },
                        success: function (response) {
                            const response_json = Ext.decode(response.responseText);

                            if (response_json.error) {
                                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                            }
                            else {
                                Ext.getStore("ConfigDbConnections").load({
                                    callback: function () {
                                        const dbconnection = Ext.getStore("ConfigDbConnections").findRecord("id", response_json.id, 0, false, false, true);
                                        const config_db_connections_grid = Ext.getCmp("config_db_connections_grid");
                                        const row_dbconnection = config_db_connections_grid.store.indexOf(dbconnection);

                                        config_db_connections_grid.getSelectionModel().deselectAll();
                                        config_db_connections_grid.getSelectionModel().select(row_dbconnection);
                                        config_db_connections_grid.ensureVisible(row_dbconnection);
                                    }
                                });
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Datenbankverbindung erfolgreich gespeichert!");
                            }
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                            Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Achtung", "Name schon vergeben");
                }
            }
        }
    },

    onNewSourceDbConnection: function () {
        Ext.getCmp("config_source_db_connections_grid").getSelectionModel().deselectAll();
    },

    onDeleteSourceDbConnection: function () {
        const selection = Ext.getCmp("config_source_db_connections_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "Ja";
            mb.buttonText.no = "Nein";

            mb.confirm("Quelldatenbankverbindung löschen", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    const data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deletesourcedbconnection",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            id: data.id
                        },
                        success: function (response) {
                            const resp_json = Ext.decode(response.responseText);

                            if (resp_json.status === "error") {
                                if (resp_json.message === "linked_collections") {
                                    Ext.MessageBox.alert("Achtung", "Löschen nicht möglich. Es sind noch Collections mit dieser Quelldatenbankverbindung verknüpft!");
                                }
                            }
                            else {
                                Ext.getStore("ConfigSourceDbConnections").load();
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Quelldatenbankverbindung erfolgreich gelöscht!");
                            }
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onSaveSourceDbConnection: function () {
        const selection = Ext.getCmp("config_source_db_connections_grid").getSelectionModel().getSelection();
        const dbms = Ext.getCmp("sourcedbconnectiondbms").getSubmitValue();
        const database = Ext.getCmp("sourcedbconnectiondatabase").getSubmitValue();
        const host = Ext.getCmp("sourcedbconnectionhost").getSubmitValue();
        const port = Ext.getCmp("sourcedbconnectionport").getSubmitValue();
        const user = Ext.getCmp("sourcedbconnectionuser").getSubmitValue();
        const password = Ext.getCmp("sourcedbconnectionpassword").getSubmitValue();
        let name = Ext.getCmp("sourcedbconnectionname").getSubmitValue();

        name = name.replace(/ü/gi, "ue");
        name = name.replace(/ö/gi, "oe");
        name = name.replace(/ä/gi, "ae");
        name = name.replace(/ß/gi, "ss");
        name = name.replace(/ /gi, "_");

        Ext.getCmp("sourcedbconnectionname").setValue(name);

        if (name.match(/^\d/)) {
            Ext.MessageBox.alert("Fehler", "Der Quell-DB-Verbindungsname darf nicht mit einer Zahl beginnen!");
        }
        else if (name.match(/[^a-z0-9_/]/)) {
            Ext.MessageBox.alert("Fehler", "Mit Ausnahme von UNDERSCORE sind Sonderzeichen und Großschreibung nicht zulässig. Um mehrere Wörter zu verbinden bitte Snakecase benutzen!");
        }
        else if (name.length > 60) {
            Ext.MessageBox.alert("Fehler", "Der Quell-DB-Verbindungsname darf nicht länger als 60 Zeichen sein!");
        }
        else if (name[0] === "_") {
            Ext.MessageBox.alert("Fehler", "Der Quell-DB-Verbindungsname darf nicht mit einem UNDERSCORE beginnen!");
        }
        else {
            if (selection.length === 1) {
                const data = selection[0].data;

                Ext.Ajax.request({
                    url: "backend/updatesourcedbconnection",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        id: data.id,
                        dbms: data.dbms,
                        name: data.name,
                        host: data.host,
                        port: parseInt(data.port),
                        database: data.database,
                        user: data.user,
                        password: data.password
                    },
                    success: function (response) {
                        if (Ext.decode(response.responseText).error) {
                            Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                        }
                        else {
                            Ext.getStore("ConfigSourceDbConnections").load({
                                callback: function () {
                                    const sourcedbconnection = Ext.getStore("ConfigSourceDbConnections").findRecord("id", data.id, 0, false, false, true);
                                    const config_source_db_connections_grid = Ext.getCmp("config_source_db_connections_grid");
                                    const row_sourcedbconnection = config_source_db_connections_grid.store.indexOf(sourcedbconnection);

                                    config_source_db_connections_grid.getSelectionModel().deselectAll();
                                    config_source_db_connections_grid.getSelectionModel().select(row_sourcedbconnection);
                                    config_source_db_connections_grid.ensureVisible(row_sourcedbconnection);
                                }
                            });
                            UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Quelldatenbankverbindung erfolgreich gespeichert!");
                        }
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    }
                });
            }
            else if (selection.length === 0 && name !== "" && database !== "" && host !== "" && port !== "" && user !== "" && password !== "") {
                const dbConnection = Ext.getStore("ConfigSourceDbConnections").findRecord("name", name, 0, false, false, true);

                if (!dbConnection) {
                    Ext.Ajax.request({
                        url: "backend/addsourcedbconnection",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            name: name,
                            dbms: dbms,
                            host: host,
                            port: parseInt(port),
                            database: database,
                            user: user,
                            password: password
                        },
                        success: function (response) {
                            const response_json = Ext.decode(response.responseText);

                            if (response_json.error) {
                                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                            }
                            else {
                                Ext.getStore("ConfigSourceDbConnections").load({
                                    callback: function () {
                                        const sourcedbconnection = Ext.getStore("ConfigSourceDbConnections").findRecord("id", response_json.id, 0, false, false, true);
                                        const config_source_db_connections_grid = Ext.getCmp("config_source_db_connections_grid");
                                        const row_sourcedbconnection = config_source_db_connections_grid.store.indexOf(sourcedbconnection);

                                        config_source_db_connections_grid.getSelectionModel().deselectAll();
                                        config_source_db_connections_grid.getSelectionModel().select(row_sourcedbconnection);
                                        config_source_db_connections_grid.ensureVisible(row_sourcedbconnection);
                                    }
                                });
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Quelldatenbankverbindung erfolgreich gespeichert!");
                            }
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                            Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                        }
                    });
                }
                else {
                    Ext.MessageBox.alert("Achtung", "Name schon vergeben");
                }
            }
        }
    },

    onElasticGridItemSelected: function (sender, record) {
        
    },

    onOpenSettingsEditor: function () {
        let elasticConfigWindow = Ext.getCmp("elasticeditor-window");

        if (elasticConfigWindow) {
            elasticConfigWindow.destroy();
        }

        elasticConfigWindow = Ext.create("management.ElasticEditorWindow");
        elasticConfigWindow.show();
        elasticConfigWindow.setTitle("Elastic Settings bearbeiten");

        const editor = monaco.editor.create(document.getElementById("elasticconfigcontainer"), {
            language: "json"
        });

        editor.setValue(JSON.stringify(Ext.getStore("ConfigElastic").getAt(0).get("settings"), null, "\t"));
        editor.context = "settings";

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").setElasticEditor(editor);
    },

    onOpenMappingsEditor: function () {
        let elasticConfigWindow = Ext.getCmp("elasticeditor-window");

        if (elasticConfigWindow) {
            elasticConfigWindow.destroy();
        }

        elasticConfigWindow = Ext.create("management.ElasticEditorWindow");
        elasticConfigWindow.show();
        elasticConfigWindow.setTitle("Elastic Mappings bearbeiten");

        const editor = monaco.editor.create(document.getElementById("elasticconfigcontainer"), {
            language: "json"
        });

        editor.setValue(JSON.stringify(Ext.getStore("ConfigElastic").getAt(0).get("mappings"), null, "\t"));
        editor.context = "mappings";

        UDPManager.app.getController("UDPManager.controller.PublicFunctions").setElasticEditor(editor);
    },

    onSetElasticConfig: function () {
        const editor = UDPManager.app.getController("UDPManager.controller.PublicFunctions").getElasticEditor();
        const context = editor.context;
        const valid = UDPManager.app.getController("UDPManager.controller.PublicFunctions").isJsonString(editor.getValue());

        if (valid) {
            const config = JSON.parse(editor.getValue());

            Ext.getStore("ConfigElastic").getAt(0).set(context, config);

            const elasticConfigWindow = Ext.getCmp("elasticeditor-window");

            if (elasticConfigWindow) {
                elasticConfigWindow.destroy();
            }
        }
        else {
            Ext.MessageBox.alert("Fehler", "JSON nicht valide");
        }
    },

    onSaveElasticConfig: function () {
        const record = Ext.getStore("ConfigElastic").getAt(0).data;
        const indexes = [];

        Ext.getStore("ElasticIndexes").each(function (index) {
            if (!Array.isArray(index.data.keyword_filter_include)) {
                index.data.keyword_filter_include = index.data.keyword_filter_include.split(",");
            }
            if (!Array.isArray(index.data.keyword_filter_exclude)) {
                index.data.keyword_filter_exclude = index.data.keyword_filter_exclude.split(",");
            }
            if (!Array.isArray(index.data.server)) {
                index.data.server = index.data.server.split(",");
            }

            indexes.push(index.data);
        });

        Ext.Ajax.request({
            url: "backend/updateelasticconfig",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                settings: JSON.stringify(record.settings),
                mappings: JSON.stringify(record.mappings),
                indexes: JSON.stringify(indexes)
            },
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    Ext.getStore("ConfigElastic").load();
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("Elastic Konfiguration gespeichert!");
                }
                else {
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
            }
        });
    },

    onNewElasticIndex: function () {
        const rec = new UDPManager.model.UDPManagerModel({
            name: "new_index",
            only_prod: false,
            only_stage: true,
            internal: true,
            keyword_filter_include: "",
            keyword_filter_exclude: ""
        });
        const count = Ext.getStore("ElasticIndexes").count();

        Ext.getStore("ElasticIndexes").insert(count, rec);
        Ext.getCmp("elastic-index-config").getSelectionModel().select(count, true);
    },

    onDeleteElasticIndex: function () {
        const selection = Ext.getCmp("elastic-index-config").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "ja";
            mb.buttonText.no = "nein";

            mb.confirm("Index Löschen", "wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    selection[0].drop();
                }
            });
        }
    },

    onElasticOnlyProdStageChange: function (el, newValue) {
        if (newValue && el.id === "elasticindexonlyprod") {
            Ext.getCmp("elasticindexonlystage").setValue(false);
        }
        else if (!newValue && el.id === "elasticindexonlyprod") {
            Ext.getCmp("elasticindexonlystage").setValue(true);
        }
        if (newValue && el.id === "elasticindexonlystage") {
            Ext.getCmp("elasticindexonlyprod").setValue(false);
        }
        else if (!newValue && el.id === "elasticindexonlystage") {
            Ext.getCmp("elasticindexonlyprod").setValue(true);
        }
    },

    onOpenIndex: function (grid, rowIndex) {
        const data = grid.getStore().getAt(rowIndex).data;

        if (data.server) {
            const serverArray = data.server.split(",");

            for (const url of serverArray) {
                window.open(`${url}/${data.name}/_search`, "_blank");
            }
        }
    },

    onJiraModuleChange: function (el, newValue) {
        Ext.getCmp("configappjira").setDisabled(!newValue);
    },

    onGitModuleChange: function (el, newValue) {
        Ext.getCmp("configappgit").setDisabled(!newValue);
    },

    onSaveConfigApp: function () {
        const server = Ext.getCmp("configappserver").getSubmitValue();
        const capabilitiesmetadata = Ext.getCmp("configappcapabilitiesmetadata").getSubmitValue();
        const service_types = Ext.getCmp("configappservicetypes").getSubmitValue();
        const test_bbox = Ext.getCmp("configapptestbbox").getSubmitValue();
        const clientconfig = Ext.getCmp("configappclientconfig").getSubmitValue();
        const ldap = Ext.getCmp("configappldap").getSubmitValue();
        const jira = Ext.getCmp("configappjira").getSubmitValue();
        const mail = Ext.getCmp("configappmail").getSubmitValue();
        const git = Ext.getCmp("configappgit").getSubmitValue();
        const defaultbbox = Ext.getCmp("configappdefaultbbox").getSubmitValue();
        const available_projections = Ext.getCmp("configappavailableprojections").getSubmitValue() ? Ext.getCmp("configappavailableprojections").getSubmitValue().split(",") : [];
        const layerattributesexcludes = Ext.getCmp("configapplayerattributesexcludes").getSubmitValue();
        const modules = {
            serviceSecurity: Ext.getCmp("configappservicesecuritymodule").getValue(),
            jiraTickets: Ext.getCmp("configappjiraticketsmodule").getValue(),
            git: Ext.getCmp("configappgitmodule").getValue(),
            visitStatistics: Ext.getCmp("configappvisitstatisticsmodule").getValue(),
            elasticsearch: Ext.getCmp("configappelasticsearchmodule").getValue(),
            externalCollectionImport: Ext.getCmp("configappexternalcollectionimportmodule").getValue()
        };
        const config_software = {};

        try {
            Ext.getStore("ConfigSoftware").each(function (record) {
                config_software[record.data.name] = JSON.parse(record.data.json);
            });

            Ext.Ajax.request({
                url: "backend/updateudpmconfig",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    server: JSON.parse(server),
                    capabilities_metadata: JSON.parse(capabilitiesmetadata),
                    service_types: JSON.parse(service_types),
                    test_bbox: JSON.parse(test_bbox),
                    client_config: JSON.parse(clientconfig),
                    ldap: JSON.parse(ldap),
                    jira: JSON.parse(jira),
                    mail: JSON.parse(mail),
                    git: JSON.parse(git),
                    modules: modules,
                    software: config_software,
                    defaultbbox: defaultbbox,
                    available_projections: available_projections,
                    layer_attributes_excludes: JSON.parse(layerattributesexcludes)
                },
                success: function (response) {
                    const response_json = Ext.decode(response.responseText);

                    if (response_json.status === "success") {
                        Ext.getStore("ConfigApp").load();

                        Ext.MessageBox.alert("UDP-Manager Konfiguration gespeichert!", "Die Änderungen werden nach einem Neuladen der Anwendung im Browser gültig.");
                    }
                    else {
                        Ext.MessageBox.alert("Fehler", response_json.message);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
        catch (error) {
            console.log(error);
            Ext.MessageBox.alert("Fehler", "Nicht valides JSON Format");
        }

    },

    onSoftwareItemSelected: function (sender, record) {
        if (record.data.name === "deegree" || record.data.name === "GeoServer" || record.data.name === "ldproxy" || record.data.name === "MapServer") {
            Ext.getCmp("delete_configsoftware").setDisabled(true);
        }
        else {
            Ext.getCmp("delete_configsoftware").setDisabled(false);
        }
    },

    onNewConfigSoftware: function () {
        const rec = new UDPManager.model.UDPManagerModel({
            name: "software_neu",
            json: `{
                "dev":{
                   "instances":[
                      "fachdaten_dev"
                   ],
                   "urlIntTemplate":"{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
                   "urlExtTemplate":"{{protocol}}://{{server}}/{{service_name}}",
                   "defaultServer":"server_int",
                   "softwareSpecificParameter":{
                      "apiKey":null,
                      "workspaceReloadAllowed":true
                   }
                },
                "stage":{
                   "instances":[
                      "fachdaten_stage"
                   ],
                   "urlIntTemplate":"{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
                   "urlExtTemplate":"{{protocol}}://{{server}}/{{service_name}}",
                   "defaultServer":"server_int",
                   "softwareSpecificParameter":{
                      "apiKey":null,
                      "workspaceReloadAllowed":true
                   }
                },
                "prod":{
                   "instances":[
                      "fachdaten_prod"
                   ],
                   "urlIntTemplate":"{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
                   "urlExtTemplate":"{{protocol}}://{{server}}/{{service_name}}",
                   "defaultServer":"server_int",
                   "softwareSpecificParameter":{
                      "apiKey":null,
                      "workspaceReloadAllowed":true
                   }
                },
                "supportedApiTypes":{
                   "wms":{

                   },
                   "wfs":{
                      "availableProjections":[
                         "EPSG:25832",
                         "EPSG:25833",
                         "EPSG:4326",
                         "EPSG:4258",
                         "EPSG:31467",
                         "EPSG:3857",
                         "EPSG:3044",
                         "EPSG:3034",
                         "EPSG:3035"
                      ]
                   },
                   "wfst":{
                      "availableProjections":[
                         "EPSG:25832",
                         "EPSG:25833",
                         "EPSG:4326",
                         "EPSG:4258",
                         "EPSG:31467",
                         "EPSG:3857",
                         "EPSG:3044",
                         "EPSG:3034",
                         "EPSG:3035"
                      ]
                   },
                   "wps":{

                   },
                   "wmts":{
                      "availableProjections":[
                         "EPSG:25832",
                         "EPSG:25833",
                         "EPSG:4326",
                         "EPSG:4258",
                         "EPSG:31467",
                         "EPSG:3857",
                         "EPSG:3044",
                         "EPSG:3034",
                         "EPSG:3035"
                      ]
                   }
                },
                "dataTypeMap":{
                   "smallint":"number",
                   "integer":"number",
                   "int":"number",
                   "int2":"number",
                   "int4":"number",
                   "int8":"number",
                   "float":"number",
                   "float2":"number",
                   "float4":"number",
                   "float8":"number",
                   "varchar":"text",
                   "char":"text",
                   "string":"text",
                   "time":"date",
                   "timestamp":"date",
                   "timestampz":"date",
                   "interval":"date",
                   "bool":"boolean",
                   "geometry_point":"geometry [point]",
                   "geometry_line":"geometry [line]",
                   "geometry_multiline":"geometry [line]",
                   "geometry_polygon":"geometry [polygon]",
                   "geometry_multipolygon":"geometry [polygon]",
                   "text":"text",
                   "MULTIPOLYGON":"geometry [polygon]",
                   "LINESTRING":"geometry [line]",
                   "timestamptz":"date",
                   "bpchar":"text",
                   "POINT":"geometry [point]",
                   "POLYGON":"geometry [polygon]",
                   "MULTILINESTRING":"geometry [line]",
                   "MULTIPOINT":"geometry [point]",
                   "uuid":"text",
                   "date":"date",
                   "numeric":"number"
                },
                "inspireSettings":{
                   "languageCode":"ger"
                }
             }`
        });
        const count = Ext.getStore("ConfigSoftware").count();

        Ext.getStore("ConfigSoftware").insert(count, rec);
        Ext.getCmp("config_software_grid").getSelectionModel().select(count, true);
    },

    onDeleteConfigSoftware: function () {
        const selection = Ext.getCmp("config_software_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "Ja";
            mb.buttonText.no = "Nein";

            mb.confirm("Softwarekonfiguration Löschen", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    selection[0].drop();
                }
            });
        }
    },

    onSaveConfigFmeServer: function () {
        const selection = Ext.getCmp("config_fmeserver_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const data = selection[0].data;

            Ext.Ajax.request({
                url: "backend/savefmeserverconnection",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: data,
                success: function (response) {
                    if (Ext.decode(response.responseText).error) {
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    }
                    else {
                        Ext.getStore("ConfigFmeServers").load();
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("FME Flow Verbindung erfolgreich gespeichert!");
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
    },

    onNewConfigFmeServer: function () {
        const rec = new UDPManager.model.UDPManagerModel({
            id: 0,
            name: "FME Flow",
            host: "server.de",
            port: 5432,
            database: "fmeserver",
            schema: "public",
            user: "fmeserver",
            password: "password"
        });
        const count = Ext.getStore("ConfigFmeServers").count();

        Ext.getStore("ConfigFmeServers").insert(count, rec);
        Ext.getCmp("config_fmeserver_grid").getSelectionModel().select(count, true);
    },

    onDeleteConfigFmeServer: function () {
        const selection = Ext.getCmp("config_fmeserver_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "Ja";
            mb.buttonText.no = "Nein";

            mb.confirm("FME Flow Verbindung löschen", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    const data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deletefmeserverconnection",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            id: data.id
                        },
                        success: function (response) {
                            const resp_json = Ext.decode(response.responseText);

                            if (resp_json.status === "error") {
                                Ext.MessageBox.alert("Achtung", "Löschen fehlgeschlagen!");
                            }
                            else {
                                Ext.getStore("ConfigFmeServers").load();
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("FME Flow Verbindung erfolgreich gelöscht!");
                            }
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    },

    onSaveConfigWebDav: function () {
        const selection = Ext.getCmp("config_webdav_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const data = selection[0].data;

            Ext.Ajax.request({
                url: "backend/savewebdavconnection",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: data,
                success: function (response) {
                    if (Ext.decode(response.responseText).error) {
                        Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                    }
                    else {
                        Ext.getStore("ConfigWebDavs").load();
                        UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("WebDAV Verbindung erfolgreich gespeichert!");
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
                }
            });
        }
    },

    onNewConfigWebDav: function () {
        const rec = new UDPManager.model.UDPManagerModel({
            id: 0,
            name: "WebDAV Server",
            base_url: "http://server.de",
            directory: "portal",
            full_qualified_domain: "https://www.server.de",
            username: "username",
            password: "",
            proxy: true
        });
        const count = Ext.getStore("ConfigWebDavs").count();

        Ext.getStore("ConfigWebDavs").insert(count, rec);
        Ext.getCmp("config_webdav_grid").getSelectionModel().select(count, true);
    },

    onDeleteConfigWebDav: function () {
        const selection = Ext.getCmp("config_webdav_grid").getSelectionModel().getSelection();

        if (selection.length === 1) {
            const mb = Ext.MessageBox;

            mb.buttonText.yes = "Ja";
            mb.buttonText.no = "Nein";

            mb.confirm("WebDAV Verbindung löschen", "Wirklich löschen?", function (btn) {
                if (btn === "yes") {
                    const data = selection[0].data;

                    Ext.Ajax.request({
                        url: "backend/deletewebdavconnection",
                        method: "POST",
                        headers: {token: window.apiToken},
                        jsonData: {
                            id: data.id
                        },
                        success: function (response) {
                            const resp_json = Ext.decode(response.responseText);

                            if (resp_json.status === "error") {
                                Ext.MessageBox.alert("Achtung", "Löschen fehlgeschlagen!");
                            }
                            else {
                                Ext.getStore("ConfigWebDavs").load();
                                UDPManager.app.getController("UDPManager.controller.PublicFunctions").showToast("WebDAV Verbindung erfolgreich gelöscht!");
                            }
                        },
                        failure: function (response) {
                            console.log(Ext.decode(response.responseText));
                        }
                    });
                }
            });
        }
    }
});
