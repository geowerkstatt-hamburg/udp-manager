Ext.define("UDPManager.view.management.Settings", {
    extend: "Ext.tab.Panel",
    xtype: "settings",
    controller: "settings",
    autoScroll: true,
    width: 700,
    height: Ext.Element.getViewportHeight() - 110,
    // layout: {
    //     type: "accordion",
    //     titleCollapse: true,
    //     animate: true,
    //     collapseFirst: true
    // },
    defaults: {
        bodyPadding: 10
    },
    tabPosition: "left",
    tabRotation: 0,
    style: "border-top: 1px solid #cfcfcf !important;",
    items: [
        {
            xtype: "settings_app"
        },
        {
            xtype: "settings_software"
        },
        {
            xtype: "settings_metadatacatalogs"
        },
        {
            xtype: "settings_dbconnections"
        },
        {
            xtype: "settings_sourcedbconnections"
        },
        {
            xtype: "settings_elasticsearch"
        },
        {
            xtype: "settings_webdav"
        },
        {
            xtype: "settings_fmeserver"
        }
        // ,
        // {
        //     xtype: "settings_extinstances"
        // }
    ]
});
