Ext.define("UDPManager.view.management.NewADContactController", {
    extend: "Ext.app.ViewController",
    alias: "controller.newadcontact",

    onSearchContact: function () {
        const name = Ext.getCmp("contact_search_field").getSubmitValue();

        let givenname = "*";
        let surname = Ext.getCmp("contact_search_field").getSubmitValue();

        if (name.indexOf(",") > 0) {
            surname = name.split(",")[0];
            givenname = name.split(",")[1];
        }
        else if (name.indexOf(" ") > 0) {
            surname = name.split(" ")[1];
            givenname = name.split(" ")[0];
        }

        givenname = givenname.replace(new RegExp("ü", "g"), "ue").replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ß", "g"), "ss").replace(new RegExp("Ü", "g"), "Ue").replace(new RegExp("Ö", "g"), "Oe").replace(new RegExp("Ä", "g"), "Ae").trim();
        surname = surname.replace(new RegExp("ü", "g"), "ue").replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ß", "g"), "ss").replace(new RegExp("Ü", "g"), "Ue").replace(new RegExp("Ö", "g"), "Oe").replace(new RegExp("Ä", "g"), "Ae").trim();

        Ext.getStore("ADContacts").load({
            params: {givenname: givenname, surname: surname}
        });
    },

    onAddContact: function () {
        const adcontacts = Ext.getCmp("adcontacts-grid");

        if (adcontacts.getSelectionModel().getSelection()[0] !== undefined) {
            const selectedADContact = adcontacts.getSelectionModel().getSelection()[0].data;
            const contact = Ext.getStore("Contacts").findRecord("ad_account", selectedADContact.sAMAccountName, 0, false, false, true);

            if (!contact) {
                Ext.Ajax.request({
                    url: "backend/addcontact",
                    method: "POST",
                    headers: {token: window.apiToken},
                    jsonData: {
                        given_name: selectedADContact.givenName,
                        surname: selectedADContact.sn,
                        name: selectedADContact.sn + ", " + selectedADContact.givenName,
                        company: selectedADContact.company,
                        ad_account: selectedADContact.sAMAccountName,
                        email: selectedADContact.mail,
                        tel: selectedADContact.telephoneNumber,
                        objectguid: selectedADContact.objectGUID
                    },
                    success: function () {
                        Ext.getStore("Contacts").load();
                    },
                    failure: function (response) {
                        console.log(Ext.decode(response.responseText));
                    }
                });
            }
            else {
                Ext.MessageBox.alert("Achtung", "Kontakt schon vorhanden");
            }
        }
    }
});
