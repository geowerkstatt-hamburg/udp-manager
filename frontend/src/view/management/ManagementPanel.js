Ext.define("UDPManager.view.management.ManagementPanel", {
    extend: "Ext.panel.Panel",
    xtype: "managementpanel",
    width: "100%",
    items: [{
        xtype: "tabpanel",
        activeTab: 0,
        items: [
            {
                xtype: "settings",
                title: "Konfigurationen"
            },
            {
                xtype: "scheduledtasks",
                title: "Geplante Aufgaben"
            },
            {
                xtype: "contacts",
                title: "Kontakte verwalten"
            }
        ]
    }]
});
