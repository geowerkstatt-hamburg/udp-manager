Ext.define("UDPManager.Configs", {
    singleton: true,

    config: {
        version: null,
        responsibleParty: null,
        serviceTypes: null,
        server: null,
        websocket: false,
        mapPreview: null,
        mapPreviewProd: null,
        mapPreviewBaseLayerId: null,
        endpointerUrl: null,
        mapPreviewComplexAppend: null,
        securityProxyUrlSso: null,
        securityProxyUrlAuth: null,
        modules: null,
        defaultDatasetSrs: null,
        defaultCollectionNamespace: null,
        testBbox: null,
        srsServices: null,
        jira: null,
        jiraProjects: null,
        jiraLabels: null,
        user: null,
        fmeServer: [],
        helpLink: null,
        softwareConfig: null,
        regions: null,
        authMethod: null,
        mailEnabled: false
    },

    constructor: function (config) {
        this.initConfig(config);
    },

    responsiblePartyFilterListGen: function () {
        const items = [{
            text: "Alle",
            type: "responsible_party_alle",
            itemId: "responsible_party_alle",
            checked: true
        }];

        for (let i = 0; i < this.getResponsibleParty().length; i++) {
            items.push({
                text: this.getResponsibleParty()[i].name,
                type: "responsible_party_" + this.getResponsibleParty()[i].name.toLowerCase().replace(/\s/g, "_"),
                itemId: "responsible_party_" + this.getResponsibleParty()[i].name.toLowerCase().replace(/\s/g, "_")
            });
        }

        return items;
    },

    serviceTypeFilterListGen: function (includeAll) {
        const items = [];

        if (includeAll) {
            items.push({
                text: "Alle",
                type: "typ_alle",
                itemId: "typ_alle",
                checked: true
            });
        }

        for (let i = 0; i < this.getServiceTypes().length; ++i) {
            items.push({
                text: this.getServiceTypes()[i].name,
                type: "typ_" + this.getServiceTypes()[i].name.toLowerCase(),
                itemId: "typ_" + this.getServiceTypes()[i].name.toLowerCase()
            });
        }

        return items;
    },

    softwareFilterListGen: function (includeAll) {
        const items = [];

        if (includeAll) {
            items.push({
                text: "Alle",
                type: "software_alle",
                itemId: "software_alle",
                checked: true
            });
        }

        for (const key of Object.keys(this.getSoftwareConfig())) {
            items.push({
                text: key,
                type: "software_" + key.toLowerCase().replace(/ /gi, "_"),
                itemId: "software_" + key.toLowerCase().replace(/ /gi, "_")
            });
        }

        return items;
    },

    softwareListGen: function (includeAll) {
        const items = [];

        if (includeAll) {
            items.push("Alle");
        }

        for (const key of Object.keys(this.getSoftwareConfig())) {
            items.push(key);
        }

        return items;
    },

    getSoftwareConfigByName: function (name) {
        let returnValue = {};

        for (const key of Object.keys(this.getSoftwareConfig())) {
            if (key === name) {
                returnValue = this.getSoftwareConfig()[key];
            }
        }

        return returnValue;
    },

    getSoftwareByType: function (type) {
        const values = [];

        if (type) {
            const typeNormalized = type.toLowerCase().replace("-", "");

            for (const key of Object.keys(this.getSoftwareConfig())) {
                const filterResult = this.getSoftwareConfig()[key].supportedApiTypes[typeNormalized];

                if (filterResult) {
                    values.push(key);
                }
            }
        }

        return values;
    },

    getServiceVersions: function (service_type) {
        const result = this.getServiceTypes().find((service) => service_type === service.name);

        if (result) {
            return result.versions;
        }
        else {
            return [];
        }
    },

    serverFilterListGen: function (includeAll) {
        const items = [];

        if (includeAll) {
            items.push({
                text: "Alle",
                type: "server_alle",
                itemId: "server_alle",
                checked: true
            });
        }

        for (let i = 0; i < this.getServer().length; ++i) {
            items.push(this.getServer()[i].name);
        }

        return items;
    },

    securityTypeFilterListGen: function () {
        const items = [];
        const securityTypes = [
            "keine",
            "Basic Auth",
            "AD SSO",
            "IP Schutz",
            "Referer-Filter"
        ];

        for (let i = 0; i < securityTypes.length; ++i) {
            items.push({
                text: securityTypes[i],
                type: "security_type_" + securityTypes[i].toLowerCase(),
                itemId: "security_type_" + securityTypes[i].toLowerCase()
            });
        }

        return items;
    },

    getTestBboxBySrs: function (srs) {
        const items = [];

        for (let i = 0; i < this.getTestBbox().length; ++i) {
            if (this.getTestBbox()[i].srs === srs) {
                items.push(this.getTestBbox()[i]);
            }
        }

        return items;
    },

    getDefaultServer: function (software) {
        const softwareConfig = this.getSoftwareConfig();

        if (softwareConfig[software]) {
            return {
                dev: softwareConfig[software].dev.defaultServer,
                stage: softwareConfig[software].stage.defaultServer,
                prod: softwareConfig[software].prod.defaultServer
            };
        }
        else {
            return {
                dev: "",
                stage: "",
                prod: ""
            };
        }
    },

    getServerByName: function (name) {
        return this.getServer().find((server) => server.name === name);
    },

    regionLandlevelFilterListGen: function () {
        const items = [];

        for (let i = 0; i < this.getRegions().length; ++i) {
            items.push({
                text: this.getRegions()[i].landlevel,
                type: "regions_landlevel_" + this.getRegions()[i].landlevel.toLowerCase(),
                itemId: "regions_landlevel_" + this.getRegions()[i].landlevel.toLowerCase()
            });
        }

        return items;
    }
});
