Ext.define("UDPManager.controller.PublicFunctions", {
    extend: "Ext.app.Controller",
    alias: "controller.publicfunctions",
    editor: null,
    sldeditor: null,
    elasticeditor: null,
    storeLoadState: {
        collections: false,
        layerscollections: false,
        layersservices: false,
        services: false
    },

    setEditor: function (editor) {
        this.editor = editor;
    },

    getEditor: function () {
        return this.editor;
    },

    setSldEditor: function (sldeditor) {
        this.sldeditor = sldeditor;
    },

    getSldEditor: function () {
        return this.sldeditor;
    },

    setElasticEditor: function (elasticeditor) {
        this.elasticeditor = elasticeditor;
    },

    getElasticEditor: function () {
        return this.elasticeditor;
    },

    getStoreLoadState: function () {
        if (this.storeLoadState.collections && this.storeLoadState.layerscollections && this.storeLoadState.layersservices && this.storeLoadState.services) {
            return true;
        }
        else {
            return false;
        }
    },

    setStoreLoadState: function (component, value) {
        this.storeLoadState[component] = value;
    },

    resetStoreLoadState: function () {
        this.storeLoadState = {
            collections: false,
            layerscollections: false,
            layerservices: false,
            services: false
        };
    },

    showLoadMask: function (component) {
        if (this.loadingsMask) {
            this.loadingsMask.destroy();
        }

        this.loadingsMask = new Ext.LoadMask({
            msg: "Bitte warten...",
            target: Ext.getCmp(component)
        });
        this.loadingsMask.show();
    },

    hideLoadMask: function () {
        if (this.loadingsMask) {
            this.loadingsMask.hide();
        }
    },

    setAllLayerDetailsFieldsetsHidden: function () {
        Ext.getCmp("layerdeleterequest").setHidden(true);
        Ext.getCmp("wmsattrfieldset").setHidden(true);
        Ext.getCmp("wfsattrfieldset").setHidden(true);
        Ext.getCmp("oafattrfieldset").setHidden(true);
        Ext.getCmp("staattrfieldset").setHidden(true);
        Ext.getCmp("vectortilesattrfieldset").setHidden(true);
        Ext.getCmp("oafattrfieldset").setHidden(true);
        Ext.getCmp("terrain3dattrfieldset").setHidden(true);
        Ext.getCmp("tileset3dattrfieldset").setHidden(true);
        Ext.getCmp("obliqueattrfieldset").setHidden(true);
    },

    emptyLayerJsonField: function () {
        Ext.getCmp("layer_json_field").setValue("");
    },

    setAllServiceLayerDetailsFieldsetsHidden: function () {
        Ext.getCmp("servicelayerdeleterequest").setHidden(true);
        Ext.getCmp("servicelayerwmsattrfieldset").setHidden(true);
        Ext.getCmp("servicelayerwfsattrfieldset").setHidden(true);
        Ext.getCmp("servicelayeroafattrfieldset").setHidden(true);
        Ext.getCmp("servicelayerstaattrfieldset").setHidden(true);
        Ext.getCmp("servicelayervectortilesattrfieldset").setHidden(true);
        Ext.getCmp("servicelayeroafattrfieldset").setHidden(true);
        Ext.getCmp("servicelayerterrain3dattrfieldset").setHidden(true);
        Ext.getCmp("servicelayertileset3dattrfieldset").setHidden(true);
        Ext.getCmp("servicelayerobliqueattrfieldset").setHidden(true);
    },

    getEditMode: function () {
        const editMode = (Ext.getCmp("datasetstatus").getValue() === "in Bearbeitung" || Ext.getCmp("datasetstatus").getValue() === "veröffentlicht - in Bearbeitung" || Ext.getCmp("datasetexternal").getValue()) && !window.read_only;

        return editMode;
    },

    setAllFormsReadOnly: function () {
        // ATTENTION!
        // service details and service layer form are disabled in DatasetServicesController and datasetservices.layerconf.LayerConfController
        // some control buttons are disabled in the corresponding controllers

        Ext.getCmp("datasetdetailsform").getForm().getFields().each(function (field) {
            field.setReadOnly(true);
        });

        Ext.getCmp("datasetcollectiondetailsform").getForm().getFields().each(function (field) {
            field.setReadOnly(true);
        });

        Ext.getCmp("layerdetailsform").getForm().getFields().each(function (field) {
            field.setReadOnly(true);
        });

        Ext.getCmp("servicelayerdetailsform").getForm().getFields().each(function (field) {
            field.setReadOnly(true);
        });

        Ext.getCmp("savedatasetdetails").setDisabled(true);
        Ext.getCmp("savecollectiondetails").setDisabled(true);
        Ext.getCmp("savelayerdetailsbutton").setDisabled(true);

        Ext.getCmp("datasetsearchmetadata").setDisabled(true);
        Ext.getCmp("deletedatasetmetadatacoupling").setDisabled(true);

        Ext.getCmp("datasetcollectiongeneratecustombbox").setDisabled(true);
        Ext.getCmp("datasetcollectiondownloadlegend").setDisabled(true);
        Ext.getCmp("datasetcollectionuploadlegend").setDisabled(true);
        Ext.getCmp("datasetcollectiondeletelegend").setDisabled(true);
        Ext.getCmp("datasetcollectionadddbschema").setDisabled(true);
        Ext.getCmp("datasetcollectionadddbtable").setDisabled(true);
        Ext.getCmp("opensetdatasetallowedgroupsbutton").setDisabled(true);

        // Ext.getCmp("savelayerdeleterequestbutton").setDisabled(true);
        // Ext.getCmp("deletelayerdeleterequestbutton").setDisabled(true);
        // Ext.getCmp("layerdeleterequestbutton").setDisabled(true);
        Ext.getCmp("layerjsonupdate").setDisabled(true);
        Ext.getCmp("layerjsonupdateall").setDisabled(true);

        Ext.getCmp("addnewcollection").setDisabled(true);
        Ext.getCmp("duplicatecollection").setDisabled(true);
        Ext.getCmp("newlayerbutton").setDisabled(true);
        Ext.getCmp("deletelayerbutton").setDisabled(true);

        Ext.getCmp("generateconfigbutton").setDisabled(true);

        Ext.getCmp("startdatasetediting").setDisabled(window.read_only);
    },

    setAllFormsReadOnlyEditorial: function () {
        // ATTENTION!
        // service details form are disabled in DatasetServicesController and datasetservices.layerconf.LayerConfController
        // some control buttons are disabled in the corresponding controllers

        Ext.getCmp("datasetdetailsform").getForm().getFields().each(function (field) {
            if (field.editModeOnly) {
                field.setReadOnly(true);
            }
        });

        Ext.getCmp("datasetcollectiondetailsform").getForm().getFields().each(function (field) {
            if (field.editModeOnly) {
                field.setReadOnly(true);
            }
        });

        Ext.getCmp("savedatasetdetails").setDisabled(false);
        Ext.getCmp("savecollectiondetails").setDisabled(false);
        Ext.getCmp("savelayerdetailsbutton").setDisabled(false);

        Ext.getCmp("datasetsearchmetadata").setDisabled(true);
        Ext.getCmp("deletedatasetmetadatacoupling").setDisabled(true);

        Ext.getCmp("datasetcollectiongeneratecustombbox").setDisabled(true);
        Ext.getCmp("datasetcollectiondownloadlegend").setDisabled(true);
        Ext.getCmp("datasetcollectionuploadlegend").setDisabled(true);
        Ext.getCmp("datasetcollectiondeletelegend").setDisabled(true);
        Ext.getCmp("datasetcollectionadddbschema").setDisabled(true);
        Ext.getCmp("datasetcollectionadddbtable").setDisabled(true);

        Ext.getCmp("layerjsonupdate").setDisabled(false);
        Ext.getCmp("layerjsonupdateall").setDisabled(false);

        Ext.getCmp("addnewcollection").setDisabled(true);
        Ext.getCmp("duplicatecollection").setDisabled(true);
        Ext.getCmp("newlayerbutton").setDisabled(true);
        Ext.getCmp("deletelayerbutton").setDisabled(true);

        Ext.getCmp("generateconfigbutton").setDisabled(true);

        Ext.getCmp("startdatasetediting").setDisabled(window.read_only);
    },

    setAllFormsWritable: function () {
        // ATTENTION!
        // service details and service layer form are enabled in DatasetServicesController and datasetservices.layerconf.LayerConfController
        // some control buttons are enabled in the corresponding controllers

        Ext.getCmp("datasetdetailsform").getForm().getFields().each(function (field) {
            if (field.xtype !== "datefield") {
                field.setReadOnly(false);
            }
        });

        Ext.getCmp("datasetcollectiondetailsform").getForm().getFields().each(function (field) {
            field.setReadOnly(false);
        });

        Ext.getCmp("layerdetailsform").getForm().getFields().each(function (field) {
            field.setReadOnly(false);
        });

        Ext.getCmp("servicelayerdetailsform").getForm().getFields().each(function (field) {
            field.setReadOnly(false);
        });

        Ext.getCmp("savedatasetdetails").setDisabled(false);
        Ext.getCmp("savecollectiondetails").setDisabled(false);
        Ext.getCmp("savelayerdetailsbutton").setDisabled(false);

        Ext.getCmp("datasetsearchmetadata").setDisabled(false);
        Ext.getCmp("deletedatasetmetadatacoupling").setDisabled(false);

        Ext.getCmp("datasetcollectiongeneratecustombbox").setDisabled(false);
        Ext.getCmp("datasetcollectiondownloadlegend").setDisabled(false);
        Ext.getCmp("datasetcollectionuploadlegend").setDisabled(false);
        Ext.getCmp("datasetcollectiondeletelegend").setDisabled(false);
        Ext.getCmp("datasetcollectionadddbschema").setDisabled(false);
        Ext.getCmp("datasetcollectionadddbtable").setDisabled(false);
        Ext.getCmp("opensetdatasetallowedgroupsbutton").setDisabled(false);

        // Ext.getCmp("savelayerdeleterequestbutton").setDisabled(false);
        // Ext.getCmp("deletelayerdeleterequestbutton").setDisabled(false);
        // Ext.getCmp("layerdeleterequestbutton").setDisabled(false);

        Ext.getCmp("layerjsonupdate").setDisabled(false);
        Ext.getCmp("layerjsonupdateall").setDisabled(false);

        Ext.getCmp("addnewcollection").setDisabled(false);
        Ext.getCmp("newlayerbutton").setDisabled(false);

        Ext.getCmp("startdatasetediting").setDisabled(true);
    },

    emptyServiceLayerJsonField: function () {
        Ext.getCmp("servicelayer_json_field").setValue("");
    },

    updateLayerJsonField: function (id, env) {
        if (env && id) {
            Ext.Ajax.request({
                url: "backend/getlayerjson?env=" + env + "&style=pretty&id=" + id,
                method: "GET",
                headers: {token: window.apiToken},
                success: function (response) {
                    Ext.getCmp("layer_json_field").setValue(response.responseText);
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else if (id && !env) {
            Ext.Ajax.request({
                url: "backend/getlayerjson?env=stage&style=pretty&id=" + id,
                method: "GET",
                headers: {token: window.apiToken},
                success: function (response) {
                    Ext.getCmp("layer_json_field").setValue(response.responseText);
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    updateServiceLayerJsonField: function (id, env) {
        if (env && id) {
            Ext.Ajax.request({
                url: "backend/getlayerjson?env=" + env + "&style=pretty&id=" + id,
                method: "GET",
                headers: {token: window.apiToken},
                success: function (response) {
                    Ext.getCmp("servicelayer_json_field").setValue(response.responseText);
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
        else if (id && !env) {
            Ext.Ajax.request({
                url: "backend/getlayerjson?env=stage&style=pretty&id=" + id,
                method: "GET",
                headers: {token: window.apiToken},
                success: function (response) {
                    Ext.getCmp("servicelayer_json_field").setValue(response.responseText);
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                }
            });
        }
    },

    onRecreateJson: function (scope_id, env) {
        const id = scope_id === "layerjsonupdate" ? Ext.getCmp("layerid").getSubmitValue() : Ext.getCmp("servicelayerid").getSubmitValue();
        const dataset_id = scope_id === "savedatasetdetails" ? Ext.getCmp("datasetid").getSubmitValue() : "";
        const external_dataset = Ext.getCmp("datasetexternal").getValue();
        const service_id = scope_id === "saveservicedetails" ? Ext.getCmp("serviceid").getSubmitValue() : "";
        let external = false;
        const me = this;

        let collection_id = "";

        if (scope_id === "layerjsonupdateall") {
            collection_id = Ext.getCmp("datasetcollectionid").getSubmitValue();
        }
        else if (scope_id === "servicelayerjsonupdateall") {
            collection_id = Ext.getCmp("servicelayercollectionid").getSubmitValue();
        }

        if (scope_id === "layerjsonupdateall" || scope_id === "layerjsonupdate") {
            const service = Ext.getStore("ServicesDataset").findRecord("id", Ext.getCmp("layerserviceid").getSubmitValue(), 0, false, false, true);

            external = service?.data?.external;
        }
        else if (scope_id === "servicelayerjsonupdateall" || scope_id === "servicelayerjsonupdate") {
            const service = Ext.getStore("ServicesDataset").findRecord("id", Ext.getCmp("serviceid").getSubmitValue(), 0, false, false, true);

            external = service?.data?.external;
        }

        if (scope_id === "savedatasetdetails") {
            external = external_dataset;
        }
        else if (scope_id === "saveservicedetails") {
            const service = Ext.getStore("ServicesDataset").findRecord("id", Ext.getCmp("serviceid").getSubmitValue(), 0, false, false, true);

            external = service?.data?.external;
        }

        Ext.Ajax.request({
            url: "backend/updatelayerjson?id=" + id + "&collection_id=" + collection_id + "&service_id=" + service_id + "&dataset_id=" + dataset_id + "&env=" + env + "&external=" + external,
            method: "GET",
            headers: {token: window.apiToken},
            success: function () {
                if (scope_id === "servicelayerjsonupdateall" || scope_id === "servicelayerjsonupdate") {
                    me.updateServiceLayerJsonField(id);
                }
                else {
                    me.updateLayerJsonField(id);
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Aufruf fehlgeschlagen!");
            }
        });
    },

    onApiCall: function (grid, rowIndex, status) {
        const data = grid.getStore().getAt(rowIndex).data;
        const infoWindow = Ext.create({
            xtype: "serviceinfowindow",
            viewModel: {
                data: {
                    service: data
                }
            }
        });
        const items = [];

        if (status === "status_prod") {
            infoWindow.prod = true;
        }

        items.push({
            text: "Vorschauportal",
            listeners: {
                click: "onSetTestRequest"
            }
        });

        Ext.getStore("TestLayersService").removeAll();

        Ext.getStore("TestLayersService").load({
            params: {service_id: data.id},
            callback: function () {
                infoWindow.show();
            }
        });


        if (data.type === "WMS" || data.type === "WMS-Time") {
            if (data.software !== "Extern") {
                items.push({
                    text: "getMap - intern",
                    listeners: {
                        click: "onSetTestRequest"
                    }
                });

                items.push({
                    text: "getLegendGraphic - intern",
                    listeners: {
                        click: "onSetTestRequest"
                    }
                });
            }

            items.push({
                text: "getMap - extern",
                listeners: {
                    click: "onSetTestRequest"
                }
            });

            items.push({
                text: "getLegendGraphic - extern",
                listeners: {
                    click: "onSetTestRequest"
                }
            });
        }
        else if (data.type === "WFS" || data.type === "WFS-T") {
            if (data.software !== "Extern") {
                items.push(
                    {
                        text: "getFeature - intern",
                        listeners: {
                            click: "onSetTestRequest"
                        }
                    },
                    {
                        text: "describeFeatureType - intern",
                        listeners: {
                            click: "onSetTestRequest"
                        }
                    }
                );
            }

            items.push(
                {
                    text: "getFeature - extern",
                    listeners: {
                        click: "onSetTestRequest"
                    }
                },
                {
                    text: "describeFeatureType - extern",
                    listeners: {
                        click: "onSetTestRequest"
                    }
                }
            );
        }

        Ext.getCmp("servicetestrequestbutton").setMenu(
            new Ext.menu.Menu({
                items: items
            })
        );

        Ext.getCmp("servicetestrequestbutton").setDisabled(false);
    },

    getPrimaryGeomType: function () {
        const rec = Ext.getStore("AttributeConfigs").findRecord("primary_geom", true, 0, false, false, true);

        if (rec) {
            const geomType = rec.get("attr_datatype");

            if (geomType.indexOf("polygon") > -1) {
                return "polygon";
            }
            else if (geomType.indexOf("point") > -1) {
                return "point";
            }
            else if (geomType.indexOf("line") > -1) {
                return "line";
            }
            else if (geomType.indexOf("unspecified") > -1) {
                return "unspecified";
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    },

    getGeomType: function (geomType) {
        if (geomType) {
            if (geomType.indexOf("polygon") > -1) {
                return "polygon";
            }
            else if (geomType.indexOf("point") > -1) {
                return "point";
            }
            else if (geomType.indexOf("line") > -1) {
                return "line";
            }
            else {
                return null;
            }
        }
        else {
            return null;
        }
    },

    getGeomTypes: function () {
        const geomAttributes = [];

        Ext.getStore("AttributeConfigs").each((record) => {
            if (record.get("attr_datatype").indexOf("geometry") > -1) {
                geomAttributes.push({name: record.get("attr_name_service"), type: record.get("attr_datatype")});
            }
        });

        return geomAttributes;
    },

    onDeleteMetadataCoupling: function (btn) {
        if (btn.id === "deletedatasetmetadatacoupling") {
            Ext.getCmp("datasetmdid").setValue("");
            Ext.getCmp("datasetrsid").setValue("");
            Ext.getCmp("datasetsourcecswname").setValue("");
            Ext.getCmp("datasetmetadatalink").setValue("keine Metadaten verknüpft");
            btn.setDisabled(true);
        }

        if (btn.id === "deleteservicemetadatacoupling") {
            Ext.getCmp("servicemdid").setValue("");
            Ext.getCmp("servicesourcecswname").setValue("");
            Ext.getCmp("servicemetadatalink").setValue("keine Metadaten verknüpft");
            btn.setDisabled(true);
        }
    },

    fillLayerTreeStore: function () {
        const root = Ext.getStore("WMSThemeLayer").getRoot();

        root.removeAll();

        Ext.getStore("LayersService").each((record) => {
            if (record.data.group_layer_wms && !Ext.getStore("WMSThemeConfig").findNode("id", record.data.id)) {
                root.appendChild(
                    {
                        id: record.data.id,
                        type: "group",
                        name: record.data.name,
                        text: record.data.name,
                        title: record.data.title,
                        children: []
                    }
                );
            }
            else if (!record.data.group_layer_wms && !Ext.getStore("WMSThemeConfig").findNode("id", record.data.id)) {
                root.appendChild(
                    {
                        id: record.data.id,
                        type: "layer",
                        name: record.data.name,
                        text: record.data.name,
                        title: record.data.title,
                        leaf: true
                    }
                );
            }
        });
    },

    /**
     * Helper function to set the service type specific attributes of a layer.
     * @param {*} service_type - type of the service
     * @param {*} group_object - false or true, depending on whether the collection is a group or not
     * @param {*} layervalues - object with values about the layer
     * @returns {Object} - layervalues with st_attributes
     */
    stAttributesSetter: function (service_type, group_object, layervalues) {
        if (service_type === "WMS" || service_type === "WMS-Time") {
            layervalues.st_attributes = {
                gutter: 0,
                tile_size: 512,
                single_tile: false,
                transparent: true,
                output_format: "image/png",
                gfi_format: null,
                feature_count: 1,
                notsupportedin3d: false,
                time_series: service_type === "WMS" ? false : true
            };
            if (group_object) {
                layervalues.st_attributes.group_layer = true;
            }
            else if (!group_object) {
                layervalues.st_attributes.group_layer = false;
            }
        }
        else if (!group_object && (service_type === "WFS" || service_type === "WFS-T")) {
            layervalues.st_attributes = {
                output_format: "XML"
            };
        }
        else if (!group_object && service_type === "OAF") {
            layervalues.st_attributes = {
                limit: null
            };
        }
        else if (!group_object && service_type === "Terrain3D") {
            layervalues.st_attributes = {
                request_vertex_normals: false
            };
        }
        else if (!group_object && service_type === "TileSet3D") {
            layervalues.st_attributes = {
                maximum_screen_space_error: null
            };
        }
        else if (!group_object && service_type === "Oblique") {
            layervalues.st_attributes = {
                hidelevels: null,
                minzoom: null,
                resolution: null,
                projection: null,
                terrainurl: null
            };
        }
        else if (!group_object && service_type === "STA") {
            layervalues.st_attributes = {
                rootel: "Things",
                filter: null,
                expand: null,
                epsg: "EPSG:4326",
                related_wms_layers: null,
                style_id: null,
                cluster_distance: null,
                mouse_hover_field: null,
                sta_test_url: null,
                load_things_only_in_current_extent: true
            };
        }
        else if (!group_object && service_type === "VectorTiles") {
            layervalues.st_attributes = {
                extent: null,
                origin: null,
                resolutions: null,
                vtstyles: null,
                visibility: null
            };
        }
        else {
            layervalues.st_attributes = {};
        }

        return layervalues;
    },

    /**
     * Helper function to create and save a layer. Is used to create the layers automatically when a new service or a new collection is added.
     * @param {Object} post_data - informations about the layer to be created
     * @returns {void}
     */
    autoCreateLayer: function (post_data) {
        Ext.Ajax.request({
            url: "backend/savelayer",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: post_data,
            success: function (response) {
                const response_json = Ext.decode(response.responseText);

                if (response_json.status === "success") {
                    Ext.getStore("LayersCollection").load({
                        params: {collection_id: post_data.collection_id}
                    });
                    UDPManager.app.getController("UDPManager.controller.PublicFunctions").updateLayerJsonField(response_json.id);
                }
                else {
                    Ext.MessageBox.alert("Fehler", response_json.message);
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.MessageBox.alert("Fehler", "Daten konnten nicht gespeichert werden");
            }
        });
    },

    checkServiceConfig: function () {
        const services = [];
        const shortname = Ext.getCmp("datasetshortname").getValue();
        const externalDataset = Ext.getCmp("datasetexternal").getValue();

        Ext.getStore("ServicesDataset").each(function (service) {
            if (!service.data.disable_config_management && !service.data.external) {
                services.push(
                    {
                        software: service.data.software,
                        server: service.data.server,
                        folder: service.data.folder,
                        name: service.data.name,
                        type: service.data.type,
                        status: "dev",
                        id: service.data.id,
                        shortname: shortname
                    }
                );
            }
        });

        if (services.length > 0) {
            Ext.Ajax.request({
                url: "backend/checkserviceconfig",
                method: "POST",
                headers: {token: window.apiToken},
                jsonData: {
                    services: services
                },
                success: function (response) {
                    const respJson = Ext.decode(response.responseText);

                    if (respJson.allConfigsInFolder && !externalDataset) {
                        Ext.getCmp("deploystageconfigbutton").setDisabled(false);
                    }
                    else if (!respJson.allConfigsInFolder || externalDataset) {
                        Ext.getCmp("deploystageconfigbutton").setDisabled(true);
                    }
                },
                failure: function (response) {
                    console.log(Ext.decode(response.responseText));
                    Ext.getCmp("conferrorstatus").setHidden(false);
                }
            });
        }
    },

    getServiceConfig: function (data, status) {
        Ext.getStore("ServiceConfig").removeAll();
        Ext.getStore("FeatureTypes").removeAll();
        Ext.getStore("FeatureTypesGeoserver").removeAll();
        Ext.getStore("NestedMapfiles").removeAll();
        Ext.getStore("MapserverTemplates").removeAll();

        const shortname = Ext.getCmp("datasetshortname").getValue();
        const server = status === "prod" ? data.server_prod : data.server;
        const folder = status === "prod" ? data.folder_prod : data.folder;

        Ext.Ajax.request({
            url: "backend/getserviceconfig",
            method: "POST",
            headers: {token: window.apiToken},
            jsonData: {
                software: data.software,
                server: server,
                folder: folder,
                name: data.name,
                type: data.type,
                status: status,
                id: data.id,
                shortname: shortname
            },
            success: function (response) {
                const endpointDetails = Ext.decode(response.responseText);
                const softwareConfig = UDPManager.Configs.getSoftwareConfig();

                if (endpointDetails.error) {
                    // Ext.getCmp("deploystageconfigbutton").setDisabled(true);
                    Ext.getCmp("conferrorstatus").setHidden(false);
                    Ext.getCmp("deegreeconfcontainer").setHidden(true);
                    Ext.getCmp("geoserverconfcontainer").setHidden(true);
                    Ext.getCmp("ldproxyconfcontainer").setHidden(true);
                    Ext.getCmp("mapserverconfcontainer").setHidden(true);
                }
                else {
                    Ext.getCmp("conferrorstatus").setHidden(true);

                    if (status === "dev") {
                        Ext.getCmp("statusspecificconfigicondeegree").setHtml("<div class='x-fa fa-circle orangeIconSmall'>");
                        Ext.getCmp("statusspecificconfigicongeoserver").setHtml("<div class='x-fa fa-circle orangeIconSmall'>");
                        Ext.getCmp("statusspecificconfigiconldproxy").setHtml("<div class='x-fa fa-circle orangeIconSmall'>");
                        Ext.getCmp("statusspecificconfigiconmapserver").setHtml("<div class='x-fa fa-circle orangeIconSmall'>");
                    }
                    else if (status === "stage") {
                        Ext.getCmp("statusspecificconfigicondeegree").setHtml("<div class='x-fa fa-circle yellowIconSmall'>");
                        Ext.getCmp("statusspecificconfigicongeoserver").setHtml("<div class='x-fa fa-circle yellowIconSmall'>");
                        Ext.getCmp("statusspecificconfigiconldproxy").setHtml("<div class='x-fa fa-circle yellowIconSmall'>");
                        Ext.getCmp("statusspecificconfigiconmapserver").setHtml("<div class='x-fa fa-circle yellowIconSmall'>");
                    }
                    else if (status === "prod") {
                        Ext.getCmp("statusspecificconfigicondeegree").setHtml("<div class='x-fa fa-circle greenIconSmall'>");
                        Ext.getCmp("statusspecificconfigicongeoserver").setHtml("<div class='x-fa fa-circle greenIconSmall'>");
                        Ext.getCmp("statusspecificconfigiconldproxy").setHtml("<div class='x-fa fa-circle greenIconSmall'>");
                        Ext.getCmp("statusspecificconfigiconmapserver").setHtml("<div class='x-fa fa-circle greenIconSmall'>");
                    }

                    Ext.getCmp("statusspecificconfigicondeegree").env = status;
                    Ext.getCmp("statusspecificconfigicongeoserver").env = status;
                    Ext.getCmp("statusspecificconfigiconldproxy").env = status;
                    Ext.getCmp("statusspecificconfigiconmapserver").env = status;

                    if (data.software === "deegree") {
                        Ext.getCmp("deegreeconfcontainer").setHidden(false);
                        Ext.getCmp("geoserverconfcontainer").setHidden(true);
                        Ext.getCmp("details_endpoint").setText(endpointDetails.serviceId);
                        Ext.getCmp("details_metadata").setText(endpointDetails.serviceMdId);
                        Ext.getStore("ServiceConfig").add(endpointDetails);
                        Ext.getStore("FeatureTypes").add(endpointDetails.featureTypes);
                        Ext.getCmp("details_endpoint").setDisabled(false);
                        Ext.getCmp("details_metadata").setDisabled(false);

                        const allowed = softwareConfig.deegree[status].softwareSpecificParameter.workspaceReloadAllowed;

                        if (allowed) {
                            Ext.getCmp("validatebutton").setDisabled(window.read_only);
                            Ext.getCmp("restartbutton").setDisabled(window.read_only);
                        }
                        else {
                            Ext.getCmp("validatebutton").setDisabled(true);
                            Ext.getCmp("restartbutton").setDisabled(true);
                        }

                        if (data.type === "WMS" || data.type === "WMS-Time") {
                            Ext.getCmp("details_theme").setDisabled(false);
                            Ext.getCmp("details_theme").setHidden(false);
                            Ext.getCmp("theme_comp").setHidden(false);
                            Ext.getCmp("details_theme").setText(endpointDetails.themeId);
                        }
                        if (data.type === "WFS" || data.type === "WFS-T") {
                            Ext.getCmp("details_theme").setHidden(true);
                            Ext.getCmp("theme_comp").setHidden(true);
                        }

                        Ext.Ajax.request({
                            url: "backend/getworkspacetimestamp",
                            method: "POST",
                            headers: {token: window.apiToken},
                            jsonData: {
                                url: data.url_int,
                                url_prod: data.url_int_prod,
                                folder: data.folder,
                                folder_prod: data.folder_prod,
                                status: status
                            },
                            success: function (timestampResponse) {
                                const timeStampResponseJson = Ext.decode(timestampResponse.responseText);

                                if (timeStampResponseJson.status === "success") {
                                    Ext.getCmp("deegreeworkspacetimestamp").setHtml(timeStampResponseJson.timeStamp);
                                }
                                else {
                                    Ext.getCmp("deegreeworkspacetimestamp").setHtml("");
                                }
                            }
                        });
                    }
                    else if (data.software === "MapServer") {
                        Ext.getCmp("deegreeconfcontainer").setHidden(true);
                        Ext.getCmp("ldproxyconfcontainer").setHidden(true);
                        Ext.getCmp("mapserverconfcontainer").setHidden(false);
                        Ext.getCmp("geoserverconfcontainer").setHidden(true);
                        Ext.getStore("ServiceConfig").add(endpointDetails);
                        Ext.getStore("NestedMapfiles").add(endpointDetails.nestedMapfiles);
                        Ext.getStore("MapserverTemplates").add(endpointDetails.mapserverTemplates);
                        Ext.getCmp("details_mapfile").setText(endpointDetails.mapfileId);
                        Ext.getCmp("details_mapfile").setDisabled(false);
                    }
                    else if (data.software === "ldproxy") {
                        Ext.getCmp("deegreeconfcontainer").setHidden(true);
                        Ext.getCmp("ldproxyconfcontainer").setHidden(false);
                        Ext.getCmp("mapserverconfcontainer").setHidden(true);
                        Ext.getCmp("geoserverconfcontainer").setHidden(true);
                        Ext.getStore("ServiceConfig").add(endpointDetails);
                        Ext.getCmp("details_service").setText(endpointDetails.serviceId);
                        Ext.getCmp("details_provider").setText(endpointDetails.providerId);
                        Ext.getCmp("details_service").setDisabled(false);
                        Ext.getCmp("details_provider").setDisabled(false);

                        const allowed = softwareConfig.ldproxy[status].softwareSpecificParameter.workspaceReloadAllowed;

                        if (allowed) {
                            Ext.getCmp("ldproxyrestartbutton").setDisabled(window.read_only);
                        }
                        else {
                            Ext.getCmp("ldproxyrestartbutton").setDisabled(true);
                        }
                    }
                    else if (data.software === "GeoServer") {
                        Ext.getCmp("deegreeconfcontainer").setHidden(true);
                        Ext.getCmp("ldproxyconfcontainer").setHidden(true);
                        Ext.getCmp("mapserverconfcontainer").setHidden(true);
                        Ext.getCmp("geoserverconfcontainer").setHidden(false);
                        Ext.getStore("ServiceConfig").add(endpointDetails);
                        Ext.getStore("FeatureTypesGeoserver").add(endpointDetails.featureTypesGeoserver);
                        Ext.getCmp("details_workspace").setText(endpointDetails.workspace);
                        Ext.getCmp("details_namespace").setText(endpointDetails.namespace);
                        Ext.getCmp("details_datastore_Geoserver").setText(endpointDetails.datastore);
                        Ext.getCmp("details_layer_Geoserver").setText(endpointDetails.layer);
                        Ext.getCmp("details_featuretype_Geoserver").setText(endpointDetails.featuretype);
                        Ext.getCmp("details_style_Geoserver").setText(endpointDetails.style);
                        Ext.getCmp("details_sld_Geoserver").setText(endpointDetails.sld);
                        Ext.getCmp("details_workspace").setDisabled(false);
                        Ext.getCmp("details_namespace").setDisabled(false);
                        Ext.getCmp("details_datastore_Geoserver").setDisabled(false);

                        const allowed = softwareConfig.GeoServer[status].softwareSpecificParameter.workspaceReloadAllowed;

                        if (allowed) {
                            Ext.getCmp("geoserverrestartbutton").setDisabled(window.read_only);
                        }
                        else {
                            Ext.getCmp("geoserverrestartbutton").setDisabled(true);
                        }
                    }
                }
            },
            failure: function (response) {
                console.log(Ext.decode(response.responseText));
                Ext.getCmp("conferrorstatus").setHidden(false);
            }
        });
    },

    generateUrlInt: function (software, folder, server, service_name, status, typ) {
        const softwareConfig = UDPManager.Configs.getSoftwareConfig();
        const serverObj = UDPManager.Configs.getServerByName(server);
        let urlInt = "";

        if (softwareConfig[software] && serverObj) {
            if (softwareConfig[software][status].urlIntTemplate) {
                if ((folder && softwareConfig[software][status].instances.length > 0) || (!folder && softwareConfig[software][status].instances.length === 0)) {
                    urlInt = softwareConfig[software][status].urlIntTemplate.replace("{{protocol}}", serverObj.protocol).replace("{{server}}", server).replace("{{domain}}", serverObj.domain).replace("{{folder}}", folder).replace("{{service_name}}", service_name);
                }
            }
        }

        return urlInt;
    },

    generateUrlExt: function (software, folder, server, service_name, status, typ) {
        const softwareConfig = UDPManager.Configs.getSoftwareConfig();
        const serverObj = UDPManager.Configs.getServerByName(server);
        let urlExt = "";

        if (softwareConfig[software] && serverObj) {
            if (softwareConfig[software][status].urlExtTemplate) {
                urlExt = softwareConfig[software][status].urlExtTemplate.replace("{{protocol}}", serverObj.protocol).replace("{{server}}", server).replace("{{domain}}", serverObj.domain).replace("{{folder}}", folder).replace("{{service_name}}", service_name);
            }
        }

        return urlExt;
    },

    openWorkspaceChooserWindow: function (btn) {
        const dataset_shortname = Ext.getCmp("datasetshortname").getValue();

        if (dataset_shortname.includes("change_it_") && btn.id !== "startdatasetediting") {
            Ext.Msg.alert("Achtung", "Der Kurzname des Datensatzes muss angepasst werden!");
        }
        else {
            const dest = {
                "generateconfigbutton": "dev",
                "startdatasetediting": "dev",
                "deploystageconfigbutton": "stage",
                "deployprodconfigbutton": "prod",
                "undeployprodconfigbutton": "undeploy"
            };
            let showWindow = Ext.getStore("ServicesDataset").count();
            let triggerClick = true;
            let chooseWorkspaceWindow = Ext.getCmp("workspacechooser-window");

            if (chooseWorkspaceWindow) {
                chooseWorkspaceWindow.destroy();
            }

            chooseWorkspaceWindow = Ext.create("datasetservices.workspacechooser");

            const services_checkbox = Ext.getCmp("generateconfigservices");

            services_checkbox.removeAll();

            // add all dataset services to checkboxgroup
            Ext.getStore("ServicesDataset").data.items.forEach(function (service) {
                if (!service.data.external) {
                    services_checkbox.add({
                        boxLabel: service.data.title,
                        labelWidth: "auto",
                        itemId: "generateconfigservices_" + service.data.id,
                        id: "generateconfigservices_" + service.data.id,
                        name: service.data.id,
                        inputValue: service.data.name,
                        editable: false,
                        readOnly: true,
                        checked: true
                    });
                    services_checkbox.add({
                        xtype: "combobox",
                        id: "workspacechoosercombo_" + service.data.id,
                        name: service.data.id,
                        margin: "0 10 5 10",
                        width: "100%",
                        listeners: {
                            change: "onChangeWorkspace"
                        }
                    });

                    if ((dest[btn.id] === "dev" && (service.data.status_stage || service.data.status_dev)) || (dest[btn.id] === "stage" && service.data.status_dev) || (dest[btn.id] === "prod" && service.data.status_stage) || dest[btn.id] === "undeploy") {
                        const software_config = UDPManager.Configs.getSoftwareConfigByName(service.data.software);
                        const combo = Ext.getCmp("workspacechoosercombo_" + service.data.id);

                        if (dest[btn.id] === "undeploy" || dest[btn.id] === "prod") {
                            showWindow--;
                        }
                        else if (service.data.disable_config_management) {
                            showWindow--;

                            if (service.data.folder) {
                                combo.setStore([service.data.folder]);
                                combo.select(service.data.folder);
                            }
                            else {
                                combo.setStore(["keine Auswahl notwendig"]);
                                combo.select("keine Auswahl notwendig");
                            }
                        }
                        else {
                            combo.setStore(software_config[dest[btn.id]].instances);

                            if (software_config[dest[btn.id]].instances.length === 1) {
                                combo.select(combo.getStore().getAt(0));
                                showWindow--;
                            }
                            else if (software_config[dest[btn.id]].instances.length === 0) {
                                combo.setStore(["keine Auswahl notwendig"]);
                                combo.select("keine Auswahl notwendig");
                                showWindow--;
                            }
                            else {
                                Ext.getCmp("workspacechoosergeneratebutton").setDisabled(true);
                                Ext.getCmp("workspacechooserdeploydevbutton").setDisabled(true);
                                Ext.getCmp("workspacechooserdeploystagebutton").setDisabled(true);
                                Ext.getCmp("workspacechooserdeployprodbutton").setDisabled(true);
                                Ext.getCmp("workspacechooserundeployprodbutton").setDisabled(true);
                            }
                        }
                    }
                }
                else {
                    showWindow--;
                }
            });

            // open window only for stage deployment or if more than one dev instance is choosable
            if ((dest[btn.id] === "stage" && showWindow > 0) || (dest[btn.id] === "dev" && showWindow > 0)) {
                triggerClick = false;

                chooseWorkspaceWindow.show();
            }

            // trigger click event directly without opening the window
            // events handled in serviceconfiguration controller
            if (btn.id === "generateconfigbutton") {
                if (triggerClick) {
                    Ext.getCmp("workspacechoosergeneratebutton").fireEvent("click");
                }
            }
            else if (btn.id === "startdatasetediting") {
                if (triggerClick) {
                    Ext.getCmp("workspacechooserdeploydevbutton").fireEvent("click");
                }
            }
            else if (btn.id === "deploystageconfigbutton") {
                if (triggerClick) {
                    Ext.getCmp("workspacechooserdeploystagebutton").fireEvent("click");
                }
                else {
                    Ext.getCmp("workspacechoosergeneratebutton").setHidden(true);
                    Ext.getCmp("workspacechooserdeploydevbutton").setHidden(true);
                    Ext.getCmp("workspacechooserdeploystagebutton").setHidden(false);
                    Ext.getCmp("workspacechooserdeployprodbutton").setHidden(true);
                    Ext.getCmp("workspacechooserundeployprodbutton").setHidden(true);
                }
            }
            else if (btn.id === "undeployprodconfigbutton") {
                if (triggerClick) {
                    Ext.getCmp("workspacechooserundeployprodbutton").fireEvent("click");
                }
            }
            else {
                if (triggerClick) {
                    Ext.getCmp("workspacechooserdeployprodbutton").fireEvent("click");
                }
            }
        }
    },

    showToast: function (text, alert) {
        let iconCls = "x-fa fa-clipboard-check";
        let title = "Status";
        let html = text;

        if (alert) {
            iconCls = "x-fa fa-exclamation-circle redItemImportant";
            title = "<b style='color:red'>ACHTUNG</b>";
            html = "<b style='color:red'>" + text + "</b>";
        }
        Ext.toast({
            html: html,
            closable: false,
            align: "t",
            slideDuration: 400,
            maxWidth: 500,
            //autoCloseDelay: 50000,
            iconCls: iconCls,
            title: title,
            shadow: true
        });
    },

    isJsonString: function (str) {
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    }
});
