Ext.define("UDPManager.store.ConfigFmeServersStore", {
    extend: "Ext.data.Store",
    alias: "store.configfmeservers",
    storeId: "ConfigFmeServers",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getfmeserverconnections",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
