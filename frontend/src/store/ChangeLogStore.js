Ext.define("UDPManager.store.ChangeLogStore", {
    extend: "Ext.data.Store",
    alias: "store.changelog",
    storeId: "ChangeLog",
    model: "UDPManager.model.UDPManagerModel",
    sorters: [
        {
            property: "change_date",
            direction: "DESC"
        }
    ],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getchangelog",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
