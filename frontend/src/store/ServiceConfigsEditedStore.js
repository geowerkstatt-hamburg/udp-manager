Ext.define("UDPManager.store.ServiceConfigsEditedStore", {
    extend: "Ext.data.Store",
    storeId: "ServiceConfigsEdited",
    alias: "store.serviceconfigsedited",
    model: "UDPManager.model.UDPManagerModel",

    sorters: [{
        property: "text",
        direction: "ASC"
    }],

    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getserviceconfigsedited",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
