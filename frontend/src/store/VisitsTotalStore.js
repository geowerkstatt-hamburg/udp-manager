Ext.define("UDPManager.store.VisitsTotalStore", {
    extend: "Ext.data.Store",
    alias: "store.visitstotal",
    storeId: "VisitsTotal",
    model: "UDPManager.model.UDPManagerModel",
    sorters: [{
        property: "month",
        direction: "ASC"
    }, {
        property: "visits_total",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getvisitstotal",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
