Ext.define("UDPManager.store.ADContactsStore", {
    extend: "Ext.data.Store",
    alias: "store.adcontacts",
    storeId: "ADContacts",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getadcontacts",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});