Ext.define("UDPManager.store.NestedMapfilesStore", {
    extend: "Ext.data.Store",
    storeId: "NestedMapfiles",
    alias: "store.nestedmapfiles",

    model: "UDPManager.model.UDPManagerModel",

    proxy: {
        type: "memory",
        reader: {
            type: "json"
        }
    }
});
