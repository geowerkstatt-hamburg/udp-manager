Ext.define("UDPManager.store.SoftwareStatsStore", {
    extend: "Ext.data.Store",
    alias: "store.softwarestats",
    storeId: "SoftwareStats",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getsoftwarestats",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
