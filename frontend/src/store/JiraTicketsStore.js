Ext.define("UDPManager.store.JiraTicketsStore", {
    extend: "Ext.data.Store",
    storeId: "JiraTickets",
    alias: "store.jiratickets",
    model: "UDPManager.model.UDPManagerModel",
    // sorters: [{
    //     property: "id",
    //     direction: "DESC"
    // }],
    proxy: {
        type: "ajax",
        actionMethods: {
            read: "GET"
        },
        url: "backend/getjiraticketsdataset",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    },
    filterFunction: function (item, value) {
        const datasetNameNormalized = item.get("name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

        const searchStrings = value.split(" ");

        if (searchStrings.length > 1) {
            return searchStrings.every((searchString) => datasetNameNormalized.includes(searchString));
        }
        else {
            return datasetNameNormalized.indexOf(value.toLowerCase()) > -1;
        }
    }
});
