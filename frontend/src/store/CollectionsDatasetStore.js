Ext.define("UDPManager.store.CollectionsDatasetStore", {
    extend: "Ext.data.Store",
    storeId: "CollectionsDataset",
    alias: "store.collectionsdataset",

    model: "UDPManager.model.UDPManagerModel",
    listeners: {
        load: function (records) {
            if (records.count() && !Ext.getCmp("grid_datasetcollections").selModel.hasSelection()) {
                Ext.getCmp("grid_datasetcollections").selModel.select(0);
            }
            else {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").setStoreLoadState("layerscollections", true);
            }

            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setStoreLoadState("collections", true);

            if (UDPManager.app.getController("UDPManager.controller.PublicFunctions").getStoreLoadState()) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            }
        }
    },

    sorters: [{
        property: "title",
        direction: "ASC"
    }],

    proxy: {
        type: "ajax",
        actionMethods: {
            read: "GET"
        },
        url: "backend/getcollectionsdataset",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    },

    filterFunction: function (item, value) {
        const datasetNameNormalized = item.get("name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

        return datasetNameNormalized.indexOf(value.toLowerCase()) > -1;
    }
});
