Ext.define("UDPManager.store.PortalStore", {
    extend: "Ext.data.Store",
    storeId: "Portals",
    alias: "store.portal",

    groupField: "host",

    model: "UDPManager.model.UDPManagerModel",

    sorters: [
        {
            property: "title",
            direction: "ASC"
        }
    ],

    autoLoad: true,

    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getportals",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    },

    filterFunction: function (item, value) {
        const urlNormalized = item.get("url").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");
        const titleNormalized = item.get("title").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

        return urlNormalized.indexOf(value.toLowerCase()) > -1 || titleNormalized.indexOf(value.toLowerCase()) > -1;
    }
});
