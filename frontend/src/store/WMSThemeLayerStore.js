Ext.define("UDPManager.store.WMSThemeLayerStore", {
    extend: "Ext.data.TreeStore",
    alias: "store.wmsthemelayer",
    storeId: "WMSThemeLayer",
    root: {
        text: "Root",
        id: 0,
        type: "root",
        expanded: true
    },
    folderSort: true
});
