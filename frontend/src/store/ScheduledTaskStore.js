Ext.define("UDPManager.store.ScheduledTaskStore", {
    extend: "Ext.data.Store",
    alias: "store.scheduledtasks",
    storeId: "ScheduledTasks",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    sorters: [{
        property: "name",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getscheduledtasks",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
