Ext.define("UDPManager.store.ETLProcessCollectionsStore", {
    extend: "Ext.data.Store",
    alias: "store.etlprocesscollections",
    storeId: "ETLProcessCollections",
    model: "UDPManager.model.UDPManagerModel",

    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "etlprocesscollections"
        }
    }
});