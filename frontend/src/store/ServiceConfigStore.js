Ext.define("UDPManager.store.ServiceConfigStore", {
    extend: "Ext.data.Store",
    alias: "store.serviceconfig",
    storeId: "ServiceConfig",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "memory",
        reader: {
            type: "json"
        }
    }
});
