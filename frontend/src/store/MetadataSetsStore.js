Ext.define("UDPManager.store.MetadataSetsStore", {
    extend: "Ext.data.Store",
    storeId: "MetadataSets",
    alias: "store.metadatasets",

    model: "UDPManager.model.UDPManagerModel",

    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "MetadataSets"
        }
    }
});
