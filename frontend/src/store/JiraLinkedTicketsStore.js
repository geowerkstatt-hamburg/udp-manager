Ext.define("UDPManager.store.JiraLinkedTicketsStore", {
    extend: "Ext.data.Store",
    storeId: "JiraLinkedTickets",
    alias: "store.jiralinkedtickets",
    model: "UDPManager.model.UDPManagerModel",
    // sorters: [{
    //     property: "id",
    //     direction: "DESC"
    // }],
    proxy: {
        type: "ajax",
        useDefaultXhrHeader: false,
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlinkedjiratickets",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    },
    filterFunction: function (item, value) {
        const datasetNameNormalized = item.get("name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

        const searchStrings = value.split(" ");

        if (searchStrings.length > 1) {
            return searchStrings.every((searchString) => datasetNameNormalized.includes(searchString));
        }
        else {
            return datasetNameNormalized.indexOf(value.toLowerCase()) > -1;
        }
    }
});
