Ext.define("UDPManager.store.DatasetCollectionStyleStore", {
    extend: "Ext.data.Store",
    alias: "store.datasetcollectionstyle",
    storeId: "DatasetCollectionStyle",
    model: "UDPManager.model.UDPManagerModel",
    // listeners: {
    //     add: function (records) {
    //         console.log("load")
    //         console.log(records)

    //        // if (records.count() && !Ext.getCmp("styleeditor-grid").selModel.hasSelection()) {

    //             Ext.getCmp("styleeditor-grid").selModel.select(0);
    //         //}
    //     }
    // },
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "items"
        }
    }
});
