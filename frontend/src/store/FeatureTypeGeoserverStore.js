Ext.define("UDPManager.store.FeatureTypesGeoserverStore", {
    extend: "Ext.data.Store",
    alias: "store.featuretypesgeoserver",
    storeId: "FeatureTypesGeoserver",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "memory",
        reader: {
            type: "json"
        }
    }
});
