Ext.define("UDPManager.store.LayersServiceStore", {
    extend: "Ext.data.Store",
    storeId: "LayersService",
    alias: "store.layersservice",
    model: "UDPManager.model.UDPManagerModel",
    listeners: {
        load: function (store) {
            if (store.count() && !Ext.getCmp("grid_servicelayers").selModel.hasSelection()) {
                Ext.getCmp("grid_servicelayers").selModel.select(0);
            }

            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setStoreLoadState("layersservices", true);

            if (UDPManager.app.getController("UDPManager.controller.PublicFunctions").getStoreLoadState()) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            }
        }
    },
    proxy: {
        type: "ajax",
        actionMethods: {
            read: "GET"
        },
        url: "backend/getlayersservice",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
