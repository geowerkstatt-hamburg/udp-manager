Ext.define("UDPManager.store.ServiceStore", {
    extend: "Ext.data.Store",
    storeId: "Services",
    alias: "store.services",

    model: "UDPManager.model.UDPManagerModel",

    sorters: [{
        property: "title",
        direction: "ASC"
    }],

    listeners: {
        refresh: function (store) {
            Ext.getCmp("servicescounter").setHtml("Anzahl: " + store.count());
        }
    },

    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getservices",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    },

    filterFunction: function (item, value) {
        const name = item.data.name ? item.data.name : "";
        const title = item.data.title ? item.data.title : "";
        const url_ext = item.data.url_ext ? item.data.url_ext : "";
        const url_int = item.data.url_int ? item.data.url_int : "";

        const nameNormalized = name.toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");
        const titleNormalized = title.toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");
        const urlExtNormalized = url_ext.toLowerCase();
        const urlIntNormalized = url_int.toLowerCase();

        if (!isNaN(value) && !isNaN(parseFloat(value))) {
            return parseInt(item.get("id")) === parseInt(value);
        }
        else {
            const searchStrings = value.split(" ");

            if (searchStrings.length > 1) {
                return searchStrings.every((searchString) => titleNormalized.includes(searchString));
            }
            else {
                return nameNormalized.indexOf(value.toLowerCase()) > -1 || titleNormalized.indexOf(value.toLowerCase()) > -1 || urlExtNormalized.indexOf(value.toLowerCase()) > -1 || urlIntNormalized.indexOf(value.toLowerCase()) > -1;
            }
        }
    }
});
