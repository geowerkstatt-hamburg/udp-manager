Ext.define("UDPManager.store.FeatureTypesStore", {
    extend: "Ext.data.Store",
    alias: "store.featuretypes",
    storeId: "FeatureTypes",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "memory",
        reader: {
            type: "json"
        }
    }
});
