Ext.define("UDPManager.store.VisitsStore", {
    extend: "Ext.data.Store",
    alias: "store.visits",
    storeId: "Visits",
    model: "UDPManager.model.UDPManagerModel",
    sorters: [{
        property: "month",
        direction: "ASC"
    }, {
        property: "visits_total",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getvisits",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
