Ext.define("UDPManager.store.TestLayersServiceStore", {
    extend: "Ext.data.Store",
    storeId: "TestLayersService",
    alias: "store.testlayersservice",

    model: "UDPManager.model.UDPManagerModel",

    proxy: {
        type: "ajax",
        actionMethods: {
            read: "GET"
        },
        url: "backend/getlayersservice",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
