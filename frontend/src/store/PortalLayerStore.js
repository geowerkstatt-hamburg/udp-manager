Ext.define("UDPManager.store.PortalLayerStore", {
    extend: "Ext.data.Store",
    storeId: "PortalLayers",
    alias: "store.portallayers",

    model: "UDPManager.model.UDPManagerModel",

    sorters: [{
        property: "layer_id",
        direction: "ASC"
    }],

    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlayersportal",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
