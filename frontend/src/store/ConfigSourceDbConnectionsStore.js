Ext.define("UDPManager.store.ConfigSourceDbConnectionsStore", {
    extend: "Ext.data.Store",
    alias: "store.configsourcedbconnections",
    storeId: "ConfigSourceDbConnections",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getsourcedbconnections",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
