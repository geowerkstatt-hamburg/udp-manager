Ext.define("UDPManager.store.LayerStatsStore", {
    extend: "Ext.data.Store",
    alias: "store.layerstats",
    storeId: "LayerStats",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlayerstats",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
