Ext.define("UDPManager.store.ConfigAppStore", {
    extend: "Ext.data.Store",
    alias: "store.configapp",
    storeId: "ConfigApp",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    listeners: {
        load: function (store) {
            if (store.count()) {
                if (store.getAt(0).data && !window.read_only) {
                    const data = store.getAt(0).data;

                    Ext.getCmp("configappserver").setValue(JSON.stringify(data.server, undefined, 4));
                    Ext.getCmp("configappcapabilitiesmetadata").setValue(JSON.stringify(data.capabilities_metadata, undefined, 4));
                    Ext.getCmp("configappservicetypes").setValue(JSON.stringify(data.service_types, undefined, 4));
                    Ext.getCmp("configapptestbbox").setValue(JSON.stringify(data.test_bbox, undefined, 4));
                    Ext.getCmp("configappclientconfig").setValue(JSON.stringify(data.client_config, undefined, 4));
                    Ext.getCmp("configappldap").setValue(JSON.stringify(data.ldap, undefined, 4));
                    Ext.getCmp("configappjira").setValue(JSON.stringify(data.jira, undefined, 4));
                    Ext.getCmp("configappmail").setValue(JSON.stringify(data.mail, undefined, 4));
                    Ext.getCmp("configappgit").setValue(JSON.stringify(data.git, undefined, 4));
                    Ext.getCmp("configappdefaultbbox").setValue(data.defaultbbox);
                    Ext.getCmp("configappavailableprojections").setValue(data.available_projections.toString());
                    Ext.getCmp("configapplayerattributesexcludes").setValue(JSON.stringify(data.layer_attributes_excludes, undefined, 4));

                    Ext.getCmp("configappservicesecuritymodule").setValue(data.modules.serviceSecurity);
                    Ext.getCmp("configappjiraticketsmodule").setValue(data.modules.jiraTickets);
                    Ext.getCmp("configappgitmodule").setValue(data.modules.git);
                    Ext.getCmp("configappvisitstatisticsmodule").setValue(data.modules.visitStatistics);
                    Ext.getCmp("configappelasticsearchmodule").setValue(data.modules.elasticsearch);
                    Ext.getCmp("configappexternalcollectionimportmodule").setValue(data.modules.externalCollectionImport);
                }
                if (store.getAt(0).data.software) {
                    const softwareList = store.getAt(0).data.software;
                    const softwareConfigStore = Ext.getStore("ConfigSoftware");

                    softwareConfigStore.removeAll();

                    for (const [key, value] of Object.entries(softwareList)) {
                        softwareConfigStore.add({name: key, json: JSON.stringify(value, undefined, 4)});
                    }
                }
            }
        }
    },
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getudpmconfig",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json"
        }
    }
});
