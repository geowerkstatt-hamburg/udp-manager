Ext.define("UDPManager.store.ADGroupsStore", {
    extend: "Ext.data.Store",
    alias: "store.adgroups",
    storeId: "ADGroups",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getadgroups",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
