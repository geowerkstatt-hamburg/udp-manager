Ext.define("UDPManager.store.DatasetKeywordsStore", {
    extend: "Ext.data.Store",
    storeId: "DatasetKeywords",
    alias: "store.datasetkeywords",
    autoLoad: true,
    model: "UDPManager.model.UDPManagerModel",

    sorters: [{
        property: "text",
        direction: "ASC"
    }],

    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getdistinctdatasetkeywords",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
