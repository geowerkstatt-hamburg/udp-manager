Ext.define("UDPManager.store.StyleSldIconsStore", {
    extend: "Ext.data.Store",
    alias: "store.stylesldicons",
    storeId: "StyleSldIcons",
    model: "UDPManager.model.UDPManagerModel",
    sorters: [{
        property: "file_name",
        direction: "ASC"
    }],
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "style_sld"
        }
    }
});
