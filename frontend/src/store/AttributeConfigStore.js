Ext.define("UDPManager.store.AttributeConfigsStore", {
    extend: "Ext.data.Store",
    alias: "store.attributeconfigs",
    storeId: "AttributeConfigs",
    model: "UDPManager.model.UDPManagerModel",

    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "attributeconfig"
        }
    }
});
