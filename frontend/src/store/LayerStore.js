Ext.define("UDPManager.store.LayerStore", {
    extend: "Ext.data.Store",
    storeId: "Layers",
    alias: "store.layers",

    model: "UDPManager.model.UDPManagerModel",

    sorters: [{
        property: "name",
        direction: "ASC"
    }],

    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlayers",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    },

    filterFunction: function (item, value) {
        const nameNormalized = item.get("name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");
        const titleNormalized = item.get("title").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");
        const serviceTitleNormalized = item.get("service_title").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

        if (!isNaN(value) && !isNaN(parseFloat(value))) {
            return parseInt(item.get("id")) === parseInt(value);
        }
        else if (value.indexOf("ds:") > -1) {
            return parseInt(item.get("dataset_id")) === parseInt(value.split("ds:")[1]);
        }
        else {
            const searchStrings = value.split(" ");

            if (searchStrings.length > 1) {
                return searchStrings.every((searchString) => titleNormalized.includes(searchString));
            }
            else {
                return nameNormalized.indexOf(value.toLowerCase()) > -1 || titleNormalized.indexOf(value.toLowerCase()) > -1 || serviceTitleNormalized.indexOf(value.toLowerCase()) > -1;
            }
        }
    }
});
