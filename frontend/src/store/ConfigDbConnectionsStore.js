Ext.define("UDPManager.store.ConfigDbConnectionsStore", {
    extend: "Ext.data.Store",
    alias: "store.configdbconnections",
    storeId: "ConfigDbConnections",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getdbconnections",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
