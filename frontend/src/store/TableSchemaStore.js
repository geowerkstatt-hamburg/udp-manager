Ext.define("UDPManager.store.TableSchemaStore", {
    extend: "Ext.data.Store",
    alias: "store.tableschema",
    storeId: "TableSchema",
    model: "UDPManager.model.UDPManagerModel",

    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "tableschema"
        }
    }
});
