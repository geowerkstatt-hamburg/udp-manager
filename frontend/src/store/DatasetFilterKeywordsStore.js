Ext.define("UDPManager.store.DatasetFilterKeywordsStore", {
    extend: "Ext.data.Store",
    storeId: "DatasetFilterKeywords",
    alias: "store.datasetfilterkeywords",
    autoLoad: true,
    model: "UDPManager.model.UDPManagerModel",

    sorters: [{
        property: "text",
        direction: "ASC"
    }],

    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getdistinctdatasetfilterkeywords",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
