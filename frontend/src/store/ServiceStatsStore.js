Ext.define("UDPManager.store.ServiceStatsStore", {
    extend: "Ext.data.Store",
    alias: "store.servicestats",
    storeId: "ServiceStats",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getservicestats",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
