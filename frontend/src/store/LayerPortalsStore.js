Ext.define("UDPManager.store.LayerPortalsStore", {
    extend: "Ext.data.Store",
    alias: "store.layerportals",
    storeId: "LayerPortals",
    model: "UDPManager.model.UDPManagerModel",
    groupField: "host",
    sorters: [{
        property: "title",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getlinkedportals",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
