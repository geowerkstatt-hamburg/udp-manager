Ext.define("UDPManager.store.ConfigMetadataCatalogStore", {
    extend: "Ext.data.Store",
    alias: "store.configmetadatacatalogs",
    storeId: "ConfigMetadataCatalogs",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    listeners: {
        load: function (store) {
            // sometimes wenn the first dataset ist selected in the grid, this store has not finished loading.
            // In that case the metadata fields in datasets details view are filled in afterwards

            const dataset = Ext.getStore("Datasets").findRecord("id", Ext.getCmp("datasetid").getSubmitValue(), 0, false, false, true);

            if (dataset) {
                const csw = store.findRecord("id", dataset.data.metadata_catalog_id, 0, false, false, true);

                if (csw) {
                    Ext.getCmp("datasetmdid").setValue(dataset.data.md_id);
                    Ext.getCmp("datasetsourcecswname").setValue(csw.data.name);
                    Ext.getCmp("datasetmetadatalink").setValue("<a href=\"" + csw.data.show_doc_url + dataset.data.md_id + "\" target=\"_blank\">Metadaten aufrufen</a>");
                    if (!window.read_only) {
                        Ext.getCmp("deletedatasetmetadatacoupling").setHidden(false);
                    }
                }
            }

            const service = Ext.getStore("ServicesDataset").findRecord("id", Ext.getCmp("serviceid").getSubmitValue(), 0, false, false, true);

            if (service) {
                const csw = store.findRecord("id", service.data.metadata_catalog_id, 0, false, false, true);

                if (csw) {
                    Ext.getCmp("servicemdid").setValue(service.data.md_id);
                    Ext.getCmp("servicesourcecswname").setValue(csw.data.name);
                    Ext.getCmp("servicemetadatalink").setValue("<a href=\"" + csw.data.show_doc_url + service.data.md_id + "\" target=\"_blank\">Metadaten aufrufen</a>");
                    if (!window.read_only) {
                        Ext.getCmp("deleteservicemetadatacoupling").setHidden(false);
                    }
                }
            }
        }
    },
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getconfigmetadatacatalogs",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
