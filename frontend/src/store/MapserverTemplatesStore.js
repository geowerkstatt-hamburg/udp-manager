Ext.define("UDPManager.store.MapserverTemplatesStore", {
    extend: "Ext.data.Store",
    storeId: "MapserverTemplates",
    alias: "store.mapservertemplates",

    model: "UDPManager.model.UDPManagerModel",

    proxy: {
        type: "memory",
        reader: {
            type: "json"
        }
    }
});