Ext.define("UDPManager.store.ConfigElasticStore", {
    extend: "Ext.data.Store",
    alias: "store.configelastic",
    storeId: "ConfigElastic",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    listeners: {
        load: function (store) {
            if (store.count() && store.getAt(0).data?.indexes) {
                for (const index of store.getAt(0).data.indexes) {
                    if (index.server) {
                        index.server = index.server.toString();
                    }
                }
                Ext.getStore("ElasticIndexes").loadData(store.getAt(0).data.indexes);
            }
        }
    },
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getelasticconfig",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json"
        }
    }
});
