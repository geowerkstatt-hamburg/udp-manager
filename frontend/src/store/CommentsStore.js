Ext.define("UDPManager.store.CommentsStore", {
    extend: "Ext.data.Store",
    storeId: "Comments",
    alias: "store.comments",

    model: "UDPManager.model.UDPManagerModel",
    sorters: [{
        property: "post_date",
        direction: "DESC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            read: "GET"
        },
        url: "backend/getcomments",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
