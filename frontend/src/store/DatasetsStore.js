Ext.define("UDPManager.store.DatasetsStore", {
    extend: "Ext.data.Store",
    storeId: "Datasets",
    alias: "store.datasets",

    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    listeners: {
        refresh: function (store) {
            if (store.count() && !Ext.getCmp("grid_dataset").selModel.hasSelection()) {
                Ext.getCmp("grid_dataset").selModel.select(0);
            }
            Ext.getCmp("datasetscounter").setHtml("Anzahl: " + store.count());
        }
    },
    sorters: [
        {
            property: "last_edit_date",
            direction: "DESC"
        },
        {
            property: "title",
            direction: "ASC"
        }
    ],
    proxy: {
        type: "ajax",
        actionMethods: {
            read: "GET"
        },
        url: "backend/getdatasets",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    },

    filterFunction: function (item, value) {
        const datasetTitleNormalized = item.get("title").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");
        const shortname = item.get("shortname") ? item.get("shortname") : "";

        if (!isNaN(value) && !isNaN(parseFloat(value))) {
            return parseInt(item.get("id")) === parseInt(value);
        }
        else {
            const searchStrings = value.split(" ");

            if (searchStrings.length > 1) {
                return searchStrings.every((searchString) => datasetTitleNormalized.includes(searchString));
            }
            else {
                return datasetTitleNormalized.indexOf(value.toLowerCase()) > -1 || shortname.indexOf(value.toLowerCase()) > -1 || datasetTitleNormalized.indexOf("*neu*") > -1;
            }
        }
    }
});
