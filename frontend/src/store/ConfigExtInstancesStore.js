Ext.define("UDPManager.store.ConfigExtInstancesStore", {
    extend: "Ext.data.Store",
    alias: "store.configextinstances",
    storeId: "ConfigExtInstances",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getconfigextinstances",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
