Ext.define("UDPManager.store.ConfigSoftwareStore", {
    extend: "Ext.data.Store",
    alias: "store.configsoftware",
    storeId: "ConfigSoftware",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "softwareconfig"
        }
    }
});
