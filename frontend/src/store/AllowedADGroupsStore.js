Ext.define("UDPManager.store.AllowedADGroupsStore", {
    extend: "Ext.data.Store",
    alias: "store.allowedadgroups",
    storeId: "AllowedADGroups",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "allowed_groups"
        }
    }
});
