Ext.define("UDPManager.store.ServicesDatasetStore", {
    extend: "Ext.data.Store",
    storeId: "ServicesDataset",
    alias: "store.servicesdataset",

    model: "UDPManager.model.UDPManagerModel",
    listeners: {
        load: function (store) {
            if (store.count() && !Ext.getCmp("grid_datasetservices").selModel.hasSelection()) {
                Ext.getCmp("grid_datasetservices").selModel.select(0);
            }
            else {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").setStoreLoadState("layersservices", true);
            }

            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setStoreLoadState("services", true);

            if (UDPManager.app.getController("UDPManager.controller.PublicFunctions").getStoreLoadState()) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            }

        }
    },

    proxy: {
        type: "ajax",
        actionMethods: {
            read: "GET"
        },
        url: "backend/getservicesdataset",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    },

    sorters: [{
        property: "id",
        direction: "ASC"
    }],

    filterFunction: function (item, value) {
        const datasetNameNormalized = item.get("name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

        return datasetNameNormalized.indexOf(value.toLowerCase()) > -1;
    }
});
