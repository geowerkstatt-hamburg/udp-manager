Ext.define("UDPManager.store.JiraTicketsCollectionStore", {
    extend: "Ext.data.Store",
    alias: "store.jiraticketscollection",
    storeId: "JiraTicketsCollection",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
