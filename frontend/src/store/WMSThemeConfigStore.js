Ext.define("UDPManager.store.WMSThemeConfigStore", {
    extend: "Ext.data.TreeStore",
    alias: "store.wmsthemeconfig",
    storeId: "WMSThemeConfig",
    root: {
        text: "Root",
        id: 0,
        type: "root",
        expanded: true
    },
    folderSort: false
});
