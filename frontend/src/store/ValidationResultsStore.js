Ext.define("UDPManager.store.ValidationResultsStore", {
    extend: "Ext.data.Store",
    alias: "store.validationresults",
    storeId: "ValidationResults",
    model: "UDPManager.model.UDPManagerModel",

    proxy: {
        type: "memory",
        reader: {
            type: "json",
            rootProperty: "validationresults"
        }
    }
});
