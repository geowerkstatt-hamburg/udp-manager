Ext.define("UDPManager.store.LayersCollectionStore", {
    extend: "Ext.data.Store",
    storeId: "LayersCollection",
    alias: "store.layerscollection",
    model: "UDPManager.model.UDPManagerModel",
    listeners: {
        load: function (store) {
            if (store.count() && !Ext.getCmp("grid_collectionlayers").selModel.hasSelection()) {
                Ext.getCmp("grid_collectionlayers").selModel.select(0);
            }

            UDPManager.app.getController("UDPManager.controller.PublicFunctions").setStoreLoadState("layerscollections", true);

            if (UDPManager.app.getController("UDPManager.controller.PublicFunctions").getStoreLoadState()) {
                UDPManager.app.getController("UDPManager.controller.PublicFunctions").hideLoadMask();
            }
        }
    },
    sorters: [{
        property: "service_title",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            read: "GET"
        },
        url: "backend/getlayerscollection",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    },

    filterFunction: function (item, value) {
        const nameNormalized = item.get("name").toLowerCase().replace(new RegExp("ä", "g"), "ae").replace(new RegExp("ö", "g"), "oe").replace(new RegExp("ü", "g"), "ue");

        return nameNormalized.indexOf(value.toLowerCase()) > -1;
    }
});
