Ext.define("UDPManager.store.ContactsStore", {
    extend: "Ext.data.Store",
    storeId: "Contacts",
    alias: "store.contacts",
    autoLoad: true,
    model: "UDPManager.model.UDPManagerModel",
    sorters: [{
        property: "surname",
        direction: "ASC"
    }],
    proxy: {
        type: "ajax",
        actionMethods: {
            read: "GET"
        },
        url: "backend/getcontacts",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
