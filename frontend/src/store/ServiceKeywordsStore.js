Ext.define("UDPManager.store.ServiceKeywordsStore", {
    extend: "Ext.data.Store",
    storeId: "ServiceKeywords",
    alias: "store.servicekeywords",
    autoLoad: true,
    model: "UDPManager.model.UDPManagerModel",

    sorters: [{
        property: "text",
        direction: "ASC"
    }],

    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getdistinctservicekeywords",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
