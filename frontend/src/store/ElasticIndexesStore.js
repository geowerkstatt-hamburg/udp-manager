Ext.define("UDPManager.store.ElasticIndexesStore", {
    extend: "Ext.data.Store",
    alias: "store.elasticindexes",
    storeId: "ElasticIndexes",
    model: "UDPManager.model.UDPManagerModel",
    proxy: {
        type: "memory",
        reader: {
            type: "json"
        }
    }
});
