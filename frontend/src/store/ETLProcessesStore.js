Ext.define("UDPManager.store.ETLProcessesStore", {
    extend: "Ext.data.Store",
    storeId: "ETLProcesses",
    alias: "store.etlprocesses",
    model: "UDPManager.model.UDPManagerModel",

    sorters: [{
        property: "name",
        direction: "ASC"
    }],

    listeners: {
        load: function (records) {
            if (records.count() && !Ext.getCmp("etlprocessesgrid").selModel.hasSelection()) {

                Ext.getCmp("etlprocessesgrid").selModel.select(0);
            }
        }
    },

    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getetlprocesses",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {type: "json", rootProperty: "results", totalProperty: "total"}
    }
});
