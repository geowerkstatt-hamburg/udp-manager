Ext.define("UDPManager.store.ConfigWebDavsStore", {
    extend: "Ext.data.Store",
    alias: "store.configwebdavs",
    storeId: "ConfigWebDavs",
    model: "UDPManager.model.UDPManagerModel",
    autoLoad: true,
    proxy: {
        type: "ajax",
        actionMethods: {
            create: "GET",
            read: "GET",
            update: "GET",
            destroy: "GET"
        },
        url: "backend/getwebdavconnections",
        method: "GET",
        headers: {token: window.apiToken},
        reader: {
            type: "json",
            rootProperty: "results"
        }
    }
});
