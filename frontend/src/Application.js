Ext.define("UDPManager.Application", {
    extend: "Ext.app.Application",
    name: "UDPManager",

    // stores initialization
    stores: [
        "ADGroupsStore",
        "ADContactsStore",
        "AllowedADGroupsStore",
        "ChangeLogStore",
        "CommentsStore",
        "ContactsStore",
        "ConfigSoftwareStore",
        "ConfigAppStore",
        "ConfigMetadataCatalogStore",
        "ConfigExtInstancesStore",
        "ConfigDbConnectionsStore",
        "ConfigSourceDbConnectionsStore",
        "ConfigFmeServersStore",
        "ConfigWebDavsStore",
        "ConfigElasticStore",
        "ETLProcessesStore",
        "ETLProcessCollectionsStore",
        "FeatureTypesStore",
        "FeatureTypesGeoserverStore",
        "ScheduledTaskStore",
        "MetadataSetsStore",
        "DatasetsStore",
        "ElasticIndexesStore",
        "CollectionsDatasetStore",
        "CollectionStore",
        "LayerStore",
        "LayersCollectionStore",
        "LayersServiceStore",
        "ServiceStore",
        "LayerPortalsStore",
        "ServicesDatasetStore",
        "AttributeConfigsStore",
        "PortalStore",
        "PortalLayerStore",
        "DatasetFilterKeywordsStore",
        "DatasetKeywordsStore",
        "ServiceKeywordsStore",
        "ServiceConfigStore",
        "ServiceConfigsEditedStore",
        "DatasetCollectionStyleStore",
        "JiraTicketsStore",
        "JiraLinkedTicketsStore",
        "ValidationResultsStore",
        "WMSThemeConfigStore",
        "WMSThemeLayerStore",
        "TableSchemaStore",
        "TestLayersServiceStore",
        "VisitsStore",
        "VisitsTopStore",
        "VisitsTotalStore",
        "SoftwareStatsStore",
        "ServiceStatsStore",
        "StyleSldIconsStore",
        "LayerStatsStore",
        "NestedMapfilesStore",
        "MapserverTemplatesStore",
        "JiraTicketsCollectionStore"
    ],

    /**
     * called on application boot, before launch function
     * @returns {void}
     */
    init: function () {
        // initially sets the client config parameters to ExtJS singleton
        UDPManager.Configs.setConfig(JSON.parse(window.clientConfig.replace(/&quot;/g, "\"")));
        UDPManager.Configs.setDefaultCollectionNamespace(UDPManager.Configs.getDefaultCollectionNamespace().replace("&#x3D;", "="));
    },

    /**
     * called when page has completely loaded.
     * @returns {void}
     */
    launch: function () {
        // set program version to header bar
        Ext.getCmp("versionField").setHtml("v" + UDPManager.Configs.getVersion());

        // set user name
        Ext.getCmp("userField").setHtml(window.auth_user);

        if (UDPManager.Configs.getAuthMethod() === "keycloak") {
            Ext.getCmp("logoutLink").setHidden(false);
        }

        if (UDPManager.Configs.getAuthMethod() !== "ldap") {
            Ext.getCmp("configappldap").setDisabled(true);
        }

        if (!UDPManager.Configs.getMailEnabled()) {
            Ext.getCmp("configappmail").setDisabled(true);
        }

        // if not logged in as admin, the management nav item will be removed
        if (!window.admin) {
            Ext.getCmp("main-nav-tab-panel").remove(Ext.getCmp("managementtab"));
        }

        // if websocket is set to true, listen to message to reload the datasets store
        if (UDPManager.Configs.getWebsocket()) {
            window.socket.on("update_dataset_list", function () {
                if (parseInt(Ext.getCmp("datasetid").getSubmitValue()) !== 0) {
                    Ext.getStore("Datasets").load();
                }
            });
        }

        // if AD authorization was not executed, window.auth_user is not defined. Then open Window with drop-down to manually choose user name.
        if (window.auth_user === "" || window.auth_user === undefined || window.auth_user === null) {
            const userchooserwindow = Ext.getCmp("userchooser-window");

            if (userchooserwindow) {
                userchooserwindow.show();
            }
            else {
                Ext.create("userchooser.UserChooser").show();
            }
        }

        if (UDPManager.Configs.getHelpLink()) {
            Ext.getCmp("helplinkbutton").setHidden(false);
            Ext.getCmp("helplinkbutton").setHref(UDPManager.Configs.getHelpLink());
        }

        if (!UDPManager.Configs.getModules().visitStatistics) {
            Ext.getCmp("services_main_tabs").remove("datasetservices_statistics");
        }

        if (!UDPManager.Configs.getModules().elasticsearch && !window.read_only) {
            Ext.getCmp("elastic-search-conf-fieldset").setHidden(true);
        }
    }
});
