/*
 * This file launches the application by asking Ext JS to create
 * and launch() the Application class.
 */
Ext.application({
    extend: "UDPManager.Application",

    name: "UDPManager",

    requires: [
        // This will automatically load all classes in the UDPManager namespace
        // so that application classes do not need to require each other.
        "UDPManager.*"
    ],

    // The name of the initial view to create.
    mainView: "UDPManager.view.main.Main"
});
