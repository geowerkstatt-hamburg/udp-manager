const moment = require("moment");
const Logger = require("../utils/logger.util");
const axios = require("axios");

/**
 * starts elastic index redeployment
 * @param {Object} db_udpm -  pg-promise db repo
 * @returns {void}
 */
module.exports = ({db_udpm}) => ({
    execute: async function () {
        const config = require("../config.js").getConfig();

        if (config.modules.elasticsearch) {
            const layerJsonElastic = require("../elastic/layerJson.elastic")({axios, db_udpm});

            const result = await layerJsonElastic.reDeployIndexes();

            if (result.status === "success") {
                Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - Alle Elasticsearch Indizes wurden neue initialisiert.");
                await db_udpm.exec.updateScheduledTaskStatus({name: "initialize_elastic_indexes", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Alle Elasticsearch Indizes wurden erfolgreich neu initialisiert."});
            }
            else {
                Logger.error(moment().format("YYYY-MM-DD HH:mm:ss") + " - Initialisieren der Elastic Indexe fehlgeschlagen.");
                await db_udpm.exec.updateScheduledTaskStatus({name: "initialize_elastic_indexes", status: "Fehler", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Initialisieren der Elasticsearch Indizes fehlgeschlagen."});
            }
        }
        else {
            await db_udpm.exec.updateScheduledTaskStatus({name: "initialize_elastic_indexes", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Elasticsearch Modul nicht aktiviert."});
        }
    }
});
