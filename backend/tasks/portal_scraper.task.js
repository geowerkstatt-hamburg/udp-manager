const moment = require("moment");
const Logger = require("../utils/logger.util");

/**
 * starts the processPortals function from portalsService
 * @param {Object} db_udpm -  pg-promise db repo
 * @returns {void}
 */
module.exports = ({db_udpm}) => ({
    execute: async function () {
        const portalsService = require("../services/portals.service")({db_udpm});
        const config = require("../config.js").getConfig();

        if (config.hasOwnProperty("webdav")) {
            const result = await portalsService.processPortals();

            if (result.status === "success") {
                Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der Liste der in Portalen verlinkten LayerIDs abgeschlossen. " + result.number_portals + " Portale analysiert.");
                await db_udpm.exec.updateScheduledTaskStatus({name: "portal_scraper", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Aktualisierung der Liste der in Portalen verlinkten LayerIDs abgeschlossen. " + result.number_portals + " Portale analysiert."});
            }
            else {
                await db_udpm.exec.updateScheduledTaskStatus({name: "portal_scraper", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Portalliste konnte nicht aktualisiert werden."});
            }
        }
        else {
            await db_udpm.exec.updateScheduledTaskStatus({name: "portal_scraper", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Portalliste konnte nicht aktualisiert werden, WebDAV nicht konfiguriert."});
        }
    }
});
