const moment = require("moment");
const Logger = require("../utils/logger.util");
const gitUtil = require("../utils/git.util");
const fs = require("fs").promises;
const path = require("path");

/**
 * starts git clean and git checkout
 * @param {Object} db_udpm -  pg-promise db repo
 * @returns {void}
 */
module.exports = ({db_udpm}) => ({
    execute: async function () {
        const config = require("../config.js").getConfig();

        if (config.modules.git) {
            try {
                await fs.rm(path.join(process.env.CONFIG_OUT_PATH, "/.git/index.lock"), {force: true});
            }
            catch (error) {
                Logger.debug("index.lock not found");
            }

            const result_push = await gitUtil.push(config.git.branch);
            const result_checkout = await gitUtil.checkout(config.git.branch);
            const result_clean = await gitUtil.clean();

            if (result_push.status === "success" && result_checkout.status === "success" && result_clean.status === "success") {
                Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - git clean und git checkout erfolgreich ausgeführt.");
                await db_udpm.exec.updateScheduledTaskStatus({name: "git_clean", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "git clean und git checkout erfolgreich ausgeführt."});
            }
            else {
                await db_udpm.exec.updateScheduledTaskStatus({name: "git_clean", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "git clean und git checkout konnten nicht ausgeführt werden."});
            }
        }
        else {
            Logger.debug("git Konfiguration nicht vorhanden.");

            await db_udpm.exec.updateScheduledTaskStatus({name: "git_clean", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "git Konfiguration nicht vorhanden."});
        }
    }
});
