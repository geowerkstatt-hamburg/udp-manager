const moment = require("moment");
const Logger = require("../utils/logger.util");
const workspaceGenerator = require("../utils/workspaceGenerator.util");
const dataCache = require("../utils/data_cache.util");

module.exports = ({db_udpm}) => ({
    execute: async function (start) {
        if (dataCache.getChange("stage") || start) {
            const servicesRestrictionLevel = await db_udpm.exec.getServicesRestrictionLevel();
            const result = await workspaceGenerator.execute("stage", servicesRestrictionLevel);

            if (result.status === "success") {
                Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - stage Workspaces erfolgreich zusammen gestellt.");
                db_udpm.exec.updateScheduledTaskStatus({name: "prepare_stage_workspaces", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "stage Workspace zusammen gestellt."});
            }
            else {
                Logger.error(moment().format("YYYY-MM-DD HH:mm:ss") + " - Fehler bei der stage Workspace zusammenstellung.");
                db_udpm.exec.updateScheduledTaskStatus({name: "prepare_stage_workspaces", status: "Fehler", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Fehler bei der stage Workspace Zusammenstellung."});
            }
        }
        else {
            db_udpm.exec.updateScheduledTaskStatus({name: "prepare_stage_workspaces", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Keine Änderung in stage Konfigurationen."});
        }
    }
});
