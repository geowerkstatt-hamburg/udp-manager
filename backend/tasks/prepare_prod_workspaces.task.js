const moment = require("moment");
const Logger = require("../utils/logger.util");
const workspaceGenerator = require("../utils/workspaceGenerator.util");
const dataCache = require("../utils/data_cache.util");

module.exports = ({db_udpm}) => ({
    execute: async function (start) {
        if (dataCache.getChange("prod") || start) {
            const servicesRestrictionLevel = await db_udpm.exec.getServicesRestrictionLevel();
            const result = await workspaceGenerator.execute("prod", servicesRestrictionLevel);

            if (result.status === "success") {
                Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - prod Workspaces erfolgreich zusammen gestellt.");
                db_udpm.exec.updateScheduledTaskStatus({name: "prepare_prod_workspaces", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "prod Workspace zusammen gestellt."});
            }
            else {
                Logger.error(moment().format("YYYY-MM-DD HH:mm:ss") + " - Fehler bei der prod Workspace zusammenstellung.");
                db_udpm.exec.updateScheduledTaskStatus({name: "prepare_prod_workspaces", status: "Fehler", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Fehler bei der prod Workspace Zusammenstellung."});
            }
        }
        else {
            db_udpm.exec.updateScheduledTaskStatus({name: "prepare_prod_workspaces", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Keine Änderung in prod Konfigurationen."});
        }
    }
});
