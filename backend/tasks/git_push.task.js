const moment = require("moment");
const Logger = require("../utils/logger.util");
const gitUtil = require("../utils/git.util");
const dataCache = require("../utils/data_cache.util");

/**
 * starts git clean and git checkout
 * @param {Object} db_udpm -  pg-promise db repo
 * @returns {void}
 */
module.exports = ({db_udpm}) => ({
    execute: async function () {
        const config = require("../config.js").getConfig();

        if (config.modules.git) {
            if (dataCache.getCommitCount() > 0) {
                const result_push = await gitUtil.push(config.git.branch);

                if (result_push.status === "success") {
                    dataCache.setCommitCountZero();

                    Logger.debug(moment().format("YYYY-MM-DD HH:mm:ss") + " - git push erfolgreich ausgeführt.");
                    await db_udpm.exec.updateScheduledTaskStatus({name: "git_push", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "git push erfolgreich ausgeführt."});
                }
                else {
                    await db_udpm.exec.updateScheduledTaskStatus({name: "git_push", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "git push konnte nicht ausgeführt werden."});
                }
            }
        }
        else {
            await db_udpm.exec.updateScheduledTaskStatus({name: "git_push", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "git Konfiguration nicht vorhanden."});
        }
    }
});
