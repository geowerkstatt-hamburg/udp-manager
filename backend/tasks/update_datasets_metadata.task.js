const moment = require("moment");
const Logger = require("../utils/logger.util");
const axios = require("axios");

module.exports = ({db_udpm}) => ({
    execute: async function () {
        const datasets = await db_udpm.exec.getLinkedMetadataDatasets();
        const metadataService = require("../services/metadata.service")({axios, db_udpm});
        // const layerJsonElastic = require("../elastic/layerJson.elastic")({axios, db_udpm});
        // const layersService = require("../services/layers.service")({db_udpm, layerJsonElastic});

        let counter = 0;

        for (const dataset of datasets) {

            const dataset_params = {
                datasettitle: dataset.title,
                datasetmdid: dataset.md_id,
                metadata_catalog_id: dataset.metadata_catalog_id
            };
            const result = await metadataService.getDatasetMetadata(dataset_params);

            if (result.length > 0) {
                const update_metadata_params = {
                    dataset_id: dataset.id,
                    title: result[0].title,
                    description: result[0].description,
                    access_constraints: result[0].access_constraints,
                    bbox: result[0].bbox,
                    cat_hmbtg: result[0].cat_hmbtg,
                    cat_inspire: result[0].cat_inspire,
                    cat_opendata: result[0].cat_opendata,
                    cat_org: result[0].cat_org,
                    fees: result[0].fees,
                    fees_json: result[0].fees_json,
                    rs_id: result[0].rs_id,
                    md_id: result[0].md_id,
                    keywords: [result[0].keywords],
                    md_contact_mail: result[0].md_contact_mail
                };

                await db_udpm.exec.updateMetadataDataset(update_metadata_params);
                await layersService.updateLayerJson({dataset_id: dataset.id}, false);
                counter += 1;
            }
        }

        Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der Datensatzmetadaten abgeschlossen. " + counter + " von insgesamt " + datasets.length + " gekoppelten Datensätzen aktualisiert.");
        db_udpm.exec.updateScheduledTaskStatus({name: "update_datasets_metadata", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Alle Datensatzmetadaten aktualisiert."});
    }
});
