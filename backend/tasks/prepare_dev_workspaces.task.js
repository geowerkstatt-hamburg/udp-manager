const moment = require("moment");
const Logger = require("../utils/logger.util");
const workspaceGenerator = require("../utils/workspaceGenerator.util");
const dataCache = require("../utils/data_cache.util");

module.exports = ({db_udpm}) => ({
    execute: async function (start) {
        if (dataCache.getChange("dev") || start) {
            const servicesRestrictionLevel = await db_udpm.exec.getServicesRestrictionLevel();
            const result = await workspaceGenerator.execute("dev", servicesRestrictionLevel);

            if (result.status === "success") {
                Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - dev Workspaces erfolgreich zusammen gestellt.");
                db_udpm.exec.updateScheduledTaskStatus({name: "prepare_dev_workspaces", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "dev Workspace zusammen gestellt."});
            }
            else {
                Logger.error(moment().format("YYYY-MM-DD HH:mm:ss") + " - Fehler bei der dev Workspace zusammenstellung.");
                db_udpm.exec.updateScheduledTaskStatus({name: "prepare_dev_workspaces", status: "Fehler", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Fehler bei der dev Workspace Zusammenstellung."});
            }
        }
        else {
            db_udpm.exec.updateScheduledTaskStatus({name: "prepare_dev_workspaces", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Keine Änderung in dev Konfigurationen."});
        }
    }
});
