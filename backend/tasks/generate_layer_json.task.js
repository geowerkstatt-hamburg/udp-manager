const moment = require("moment");
const Logger = require("../utils/logger.util");
const axios = require("axios");

/**
 * starts the updateLayerJson function from layersService
 * @param {Object} db_udpm -  pg-promise db repo
 * @returns {void}
 */
module.exports = ({db_udpm}) => ({
    execute: async function () {
        const layerJsonElastic = require("../elastic/layerJson.elastic")({axios, db_udpm});
        const layersService = require("../services/layers.service")({db_udpm, layerJsonElastic});

        const result = await layersService.updateLayerJson({all: true}, false);

        if (result.status === "success") {
            Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - Alle Layer-JSON-Objekte wurden in der Datenbank neu generiert.");
            await db_udpm.exec.updateScheduledTaskStatus({name: "generate_layer_json", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Alle Layer-JSON-Objekte wurden in der Datenbank neu generiert."});
        }
        else {
            await db_udpm.exec.updateScheduledTaskStatus({name: "generate_layer_json", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Layer JSON Objekte konnten nicht generiert werden."});
        }
    }
});
