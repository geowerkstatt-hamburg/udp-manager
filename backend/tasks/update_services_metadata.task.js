const moment = require("moment");
const Logger = require("../utils/logger.util");
const axios = require("axios");

/**
 * starts the update service metadata function
 * @param {Object} db_udpm -  pg-promise db repo
 * @returns {void}
 */
module.exports = ({db_udpm}) => ({
    execute: async function () {
        const services = await db_udpm.exec.getLinkedMetadataServices();
        const metadataService = require("../services/metadata.service")({axios, db_udpm});

        let counter = 0;

        for (const service of services) {
            const service_params = {
                servicetitle: service.title,
                servicemdid: service.md_id,
                metadata_catalog_id: service.metadata_catalog_id
            };
            const result = await metadataService.getServiceMetadata(service_params);

            if (result.length > 0) {
                const update_metadata_params = {
                    service_id: service.id,
                    title: result[0].title,
                    description: result[0].description,
                    access_constraints: result[0].access_constraints,
                    fees: result[0].fees,
                    fees_json: result[0].fees_json
                };

                await db_udpm.exec.updateMetadataService(update_metadata_params);
                counter += 1;
            }
        }

        Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - Aktualisierung der Dienstmetadaten abgeschlossen. " + counter + " von insgesamt " + services.length + " gekoppelten Diensten aktualisiert.");
        db_udpm.exec.updateScheduledTaskStatus({name: "update_services_metadata", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "Alle Dienstmetadaten aktualisiert."});
    }
});
