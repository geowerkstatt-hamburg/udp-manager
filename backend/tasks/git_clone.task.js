const moment = require("moment");
const Logger = require("../utils/logger.util");
const gitUtil = require("../utils/git.util");

/**
 * starts git clean and git checkout
 * @param {Object} db_udpm -  pg-promise db repo
 * @returns {void}
 */
module.exports = ({db_udpm}) => ({
    execute: async function () {
        const config = require("../config.js").getConfig();

        if (config.modules.git) {
            const result_clone = await gitUtil.clone();

            if (result_clone.status === "success") {
                Logger.debug(moment().format("YYYY-MM-DD HH:mm:ss") + " - git clone erfolgreich ausgeführt.");
                await db_udpm.exec.updateScheduledTaskStatus({name: "git_clone", status: "Erfolgreich", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "git clone erfolgreich ausgeführt."});
            }
            else {
                await db_udpm.exec.updateScheduledTaskStatus({name: "git_clone", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "git clone konnte nicht ausgeführt werden."});
            }
        }
        else {
            await db_udpm.exec.updateScheduledTaskStatus({name: "git_clone", status: "Hinweis", last_run: moment().format("YYYY-MM-DD HH:mm:ss"), message: "git Konfiguration nicht vorhanden."});
        }
    }
});
