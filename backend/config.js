const config = {
    proxy: {
        host: process.env.PROXY_HOST,
        port: process.env.PROXY_PORT
    },
    database: {
        udp_manager: {
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            database: process.env.DB_NAME,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD
        }
    },
    openDataCat: {
        SOCI: "Bevölkerung und Gesellschaft",
        EDUC: "Bildung, Kultur und Sport",
        ENER: "Energie",
        HEAL: "Gesundheit",
        INTR: "Internationale Themen",
        JUST: "Justiz, Rechtssystem und öffentliche Sicherheit",
        AGRI: "Landwirtschaft, Fischerei, Forstwirtschaft und Nahrungsmittel",
        GOVE: "Regierung und öffentlicher Sektor",
        REGI: "Regionen und Städte",
        ENVI: "Umwelt",
        TRAN: "Verkehr",
        ECON: "Wirtschaft und Finanzen",
        TECH: "Wissenschaft und Technologie"
    },
    dbDatatypesMapping: {
        integer: "number",
        int: "number",
        int2: "number",
        int4: "number",
        int8: "number",
        float: "number",
        float2: "number",
        float4: "number",
        float8: "number",
        varchar: "text",
        char: "text",
        string: "text",
        time: "date",
        timestamp: "date",
        timestampz: "date",
        interval: "date",
        bool: "boolean",
        geometry_point: "geometry [point]",
        "gml:PointPropertyType": "geometry [point]",
        geometry_line: "geometry [line]",
        "gml:LinePropertyType": "geometry [line]",
        "gml:MultiLinePropertyType": "geometry [line]",
        geometry_polygon: "geometry [polygon]",
        "gml:PolygonPropertyType": "geometry [polygon]",
        "gml:MultiPolygonPropertyType": "geometry [polygon]"
    },
    defaultBbox: "548315 5916868 588060 5955212",
    availableProjections: ["EPSG:25832", "EPSG:25833", "EPSG:4326", "EPSG:4258", "EPSG:31467", "EPSG:3857", "EPSG:3044", "EPSG:3034", "EPSG:3035"],
    applyDpiFactor: true,
    layerAttributesExcludes: ["geom", "the_geom", "geometry"],
    modules: {
        serviceSecurity: false,
        jiraTickets: false,
        git: false,
        visitStatistics: false,
        elasticsearch: false,
        externalCollectionImport: true
    },
    ldap: {
        allowed_groups: [
            {
                name: process.env.LDAP_ADMIN_GROUP || "admin-Group",
                role: "admin"
            },
            {
                name: "gruppe_2",
                role: "reader"
            }
        ],
        allowed_users: [
            {
                name: "AccountName",
                role: "admin"
            }
        ]
    },
    jira: {
        "url": "https://www.jira-host.de/rest/api/2",
        "browseUrl": "https://www.jira-host.de/browse",
        "issueTypeId": {
            "task": "1",
            "sub_task": "2"
        },
        "projects": [
            {
                name: "PROJECT",
                "name_alias": "Projektname (PROJECT)",
                "new_as_subtask": true,
                "use_in_ticketsearch": true,
                "story_points_value": 2,
                "transition_execution": false,
                "transition_id": [
                    1,
                    2
                ],
                "components": [
                    {
                        name: "Testzweck"
                    }
                ]
            }
        ],
        "defaultAssignee": "",
        "labels": [
            "Label_1",
            "Label_2"
        ],
        "proxy": true
    },
    mail: {
        portals_from: "\"Absender\" <mail@mail.de>",
        prod_to: "mail@mail.de",
        delete_layer_to: "mail@mail.de",
        delete_layer_request_to: "mail@mail.de"
    },
    capabilitiesMetadata: {
        providername: "Provider Name",
        providersite: "",
        individualname: "",
        positionname: "Postion",
        phone: "",
        facsimile: "",
        electronicmailaddress: "mail@gmail.de",
        deliverypoint: "Straße 1",
        city: "Stadt",
        administrativearea: "CODE",
        postalcode: "12345",
        country: "Germany",
        onlineresource: "http://www.name.de",
        hoursofservice: "",
        contactinstructions: "",
        metadataurl: "https://katalog.de/csw",
        authorityname: "Name",
        authorityurl: "http://www.name.de"
    },
    testBbox: [
        {
            name: "St.Pauli",
            srs: "EPSG:25832",
            bbox: "562394,5932816,564194,5934616"
        }
    ],
    visualizationApis: ["WMS", "WMS-Time", "WMTS"],
    vectorApis: ["WFS", "WFS-T", "OAF", "VectorTiles"],
    vectorSoftwares: ["deegree", "GeoServer", "ldproxy"],
    rasterSoftwares: ["MapServer"],
    serviceTypes: [
        {
            name: "WMS",
            category: "visualization",
            versions: [
                "1.1.1",
                "1.3.0"
            ]
        },
        {
            name: "WMS-Time",
            category: "visualization",
            versions: [
                "1.1.1",
                "1.3.0"
            ]
        },
        {
            name: "WFS",
            category: "vector",
            versions: [
                "1.1.0",
                "2.0.0"
            ]
        },
        {
            name: "WFS-T",
            category: "vector",
            versions: [
                "1.1.0",
                "2.0.0"
            ]
        },
        {
            name: "OAF",
            category: "vector",
            versions: [
                "1.0.0"
            ]
        },
        {
            name: "WPS",
            category: "misc",
            versions: [
                "1.0.0"
            ]
        },
        {
            name: "WMTS",
            category: "visualization",
            versions: [
                "1.0.0"
            ]
        },
        {
            name: "VectorTiles",
            category: "vector",
            versions: [
                "1.0.0"
            ]
        },
        {
            name: "GeoJSON",
            category: "misc",
            versions: [
                ""
            ]
        },
        {
            name: "Terrain3D",
            category: "misc",
            versions: [
                ""
            ]
        },
        {
            name: "TileSet3D",
            category: "misc",
            versions: [
                ""
            ]
        },
        {
            name: "STA",
            category: "misc",
            versions: [
                "1.1",
                "1.0"
            ]
        },
        {
            name: "Oblique",
            category: "misc",
            versions: [
                ""
            ]
        },
        {
            name: "Elastic",
            category: "misc",
            versions: [
                ""
            ]
        }
    ],
    server: [
        {
            name: "server1",
            domain: ".de",
            protocol: "http",
            lb: false,
            server: []
        },
        {
            name: "server2",
            domain: ".de",
            protocol: "http",
            lb: false,
            server: []
        },
        {
            name: "server3",
            domain: ".de",
            protocol: "http",
            lb: true,
            server: [
                "serverlb1",
                "serverlb2"
            ]
        }
    ],
    clientConfig: {
        helpLink: "https://geoportal-hamburg.de/udp_manager_docs/de/user_doc/",
        user: [
            "Hans Dampf"
        ],
        responsibleParty: [
            {
                name: "Team 1",
                mail: "",
                mail_alt: "",
                inbox_name: "",
                inbox_name_alt: ""
            },
            {
                name: "Team 2",
                mail: "",
                mail_alt: "",
                inbox_name: "",
                inbox_name_alt: ""
            }
        ],
        regions: [
            {
                landlevel: "Deutschland",
                arealevel: [
                    "alle Kreise"
                ]
            }
        ],
        defaultDatasetSrs: "EPSG:25832",
        defaultCollectionNamespace: "ns=https://www.namespace.de",
        mapPreview: "/UDPPreview",
        mapPreviewProd: "/UDPPreview",
        mapPreviewBaseLayerId: "1",
        mapPreviewComplexAppend: "../masterportal/config.json",
        securityProxyUrlSso: {
            dev: "http://secure.sevices-dev.de/",
            stage: "http://secure.sevices-stage.de/",
            prod: "http://secure.sevices.de/"
        },
        securityProxyUrlAuth: "http://localhost/security-proxy/auth/services/"
    },
    git: {
        options_config: [],
        branch: "main",
        ssh: false
    },
    software: {
        deegree: {
            dev: {
                instances: ["fachdaten_dev"],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    apiKey: null,
                    workspaceReloadAllowed: true,
                    additionalOutputFormats: []
                }
            },
            stage: {
                instances: ["fachdaten_a_b"],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    apiKey: null,
                    workspaceReloadAllowed: true,
                    additionalOutputFormats: []
                }
            },
            prod: {
                instances: ["fachdaten_a_b"],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    apiKey: null,
                    workspaceReloadAllowed: true,
                    additionalOutputFormats: []
                }
            },
            seperateDevWorkspace: true,
            supportedApiTypes: {
                wms: {
                    availableProjections: ["CRS:84", "EPSG:25832", "EPSG:25833", "EPSG:4326", "EPSG:4258", "EPSG:31467", "EPSG:3857", "EPSG:3044", "EPSG:3034", "EPSG:3035"]
                },
                wmstime: {
                    availableProjections: ["CRS:84", "EPSG:25832", "EPSG:25833", "EPSG:4326", "EPSG:4258", "EPSG:31467", "EPSG:3857", "EPSG:3044", "EPSG:3034", "EPSG:3035"]
                },
                wfs: {
                    availableProjections: ["EPSG:25832", "EPSG:25833", "EPSG:4326", "EPSG:4258", "EPSG:31467", "EPSG:3857", "EPSG:3044", "EPSG:3034", "EPSG:3035"]
                },
                wfst: {
                    availableProjections: ["EPSG:25832", "EPSG:25833", "EPSG:4326", "EPSG:4258", "EPSG:31467", "EPSG:3857", "EPSG:3044", "EPSG:3034", "EPSG:3035"]
                },
                wps: {},
                wmts: {}
            },
            dataTypeMap: {
                "smallint": "number",
                "integer": "number",
                "int": "number",
                "int2": "number",
                "int4": "number",
                "int8": "number",
                "float": "number",
                "float2": "number",
                "float4": "number",
                "float8": "number",
                "varchar": "text",
                "char": "text",
                "string": "text",
                "time": "date",
                "timestamp": "date",
                "timestampz": "date",
                "interval": "date",
                "bool": "boolean",
                "geometry_point": "geometry [point]",
                "geometry_line": "geometry [line]",
                "geometry_multiline": "geometry [line]",
                "geometry_polygon": "geometry [polygon]",
                "geometry_multipolygon": "geometry [polygon]",
                "text": "text",
                "MULTIPOLYGON": "geometry [polygon]",
                "LINESTRING": "geometry [line]",
                "timestamptz": "date",
                "bpchar": "text",
                "POINT": "geometry [point]",
                "POLYGON": "geometry [polygon]",
                "MULTILINESTRING": "geometry [line]",
                "MULTIPOINT": "geometry [point]",
                "uuid": "text",
                "date": "date",
                "numeric": "number"
            },
            inspireSettings: {
                languageCode: "ger"
            }
        },
        GeoServer: {
            dev: {
                instances: [],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/geoserver/{{service_name}}/ows",
                urlExtTemplate: "{{protocol}}://{{server}}{{domain}}/geoserver/{{service_name}}/ows",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    basicAuth: {
                        username: null,
                        password: null
                    },
                    webappContext: null,
                    workspaceReloadAllowed: true
                }
            },
            stage: {
                instances: [],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/geoserver/{{service_name}}/ows",
                urlExtTemplate: "{{protocol}}://{{server}}{{domain}}/geoserver/{{service_name}}/ows",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    basicAuth: {
                        username: null,
                        password: null
                    },
                    webappContext: null,
                    workspaceReloadAllowed: true
                }
            },
            prod: {
                instances: [],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/geoserver/{{service_name}}/ows",
                urlExtTemplate: "{{protocol}}://{{server}}{{domain}}/geoserver/{{service_name}}/ows",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    basicAuth: {
                        username: null,
                        password: null
                    },
                    webappContext: null,
                    workspaceReloadAllowed: true
                }
            },
            seperateDevWorkspace: true,
            supportedApiTypes: {
                wms: {
                    availableProjections: ["CRS:84", "EPSG:25832", "EPSG:25833", "EPSG:4326", "EPSG:3857"]
                },
                wfs: {
                    availableProjections: ["CRS:84", "EPSG:25832", "EPSG:25833", "EPSG:4326", "EPSG:3857"]
                }
            },
            dataTypeMap: {
                "smallint": "number",
                "integer": "number",
                "int": "number",
                "int2": "number",
                "int4": "number",
                "int8": "number",
                "float": "number",
                "float2": "number",
                "float4": "number",
                "float8": "number",
                "varchar": "text",
                "char": "text",
                "string": "text",
                "time": "date",
                "timestamp": "date",
                "timestampz": "date",
                "interval": "date",
                "bool": "boolean",
                "geometry_point": "geometry [point]",
                "geometry_line": "geometry [line]",
                "geometry_multiline": "geometry [line]",
                "geometry_polygon": "geometry [polygon]",
                "geometry_multipolygon": "geometry [polygon]",
                "text": "text",
                "MULTIPOLYGON": "geometry [polygon]",
                "LINESTRING": "geometry [line]",
                "timestamptz": "date",
                "bpchar": "text",
                "POINT": "geometry [point]",
                "POLYGON": "geometry [polygon]",
                "MULTILINESTRING": "geometry [line]",
                "MULTIPOINT": "geometry [point]",
                "uuid": "text",
                "date": "date",
                "numeric": "number"
            }
        },
        ldproxy: {
            dev: {
                instances: ["ldproxy_dev"],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{folder}}/{{service_name}}",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    workspaceReloadAllowed: true
                }
            },
            stage: {
                instances: ["ldproxy"],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    workspaceReloadAllowed: true
                }
            },
            prod: {
                instances: ["ldproxy"],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    workspaceReloadAllowed: true
                }
            },
            seperateDevWorkspace: true,
            internalExternalWorkspace: {
                internal: "ldproxy_intranet",
                external: "ldproxy"
            },
            supportedApiTypes: {
                oaf: {
                    availableProjections: ["EPSG:4326", "EPSG:25832", "CRS84"]
                }
            },
            dataTypeMap: {
                "bytea": {
                    "type": "STRING"
                },
                "smallint": {
                    "type": "INTEGER"
                },
                "integer": {
                    "type": "INTEGER"
                },
                "int": {
                    "type": "INTEGER"
                },
                "int2": {
                    "type": "INTEGER"
                },
                "int4": {
                    "type": "INTEGER"
                },
                "int8": {
                    "type": "INTEGER"
                },
                "float": {
                    "type": "FLOAT"
                },
                "float2": {
                    "type": "FLOAT"
                },
                "float4": {
                    "type": "FLOAT"
                },
                "float8": {
                    "type": "FLOAT"
                },
                "numeric": {
                    "type": "FLOAT"
                },
                "date": {
                    "type": "DATE"
                },
                "time": {
                    "type": "DATETIME"
                },
                "timestamptz": {
                    "type": "DATETIME"
                },
                "timestamp": {
                    "type": "STRING"
                },
                "timestampz": {
                    "type": "DATETIME"
                },
                "interval": {
                    "type": "DATETIME"
                },
                "bool": {
                    "type": "BOOLEAN"
                },
                "geometry_point": {
                    "type": "GEOMETRY",
                    "geometryType": "POINT"
                },
                "POINT": {
                    "type": "GEOMETRY",
                    "geometryType": "POINT"
                },
                "MULTIPOINT": {
                    "type": "GEOMETRY",
                    "geometryType": "MULTI_POINT"
                },
                "geometry_line": {
                    "type": "GEOMETRY",
                    "geometryType": "LINE_STRING"
                },
                "geometry_multiline": {
                    "type": "GEOMETRY",
                    "geometryType": "MULTI_LINE_STRING"
                },
                "geometry_polygon": {
                    "type": "GEOMETRY",
                    "geometryType": "POLYGON"
                },
                "geometry_multipolygon": {
                    "type": "GEOMETRY",
                    "geometryType": "MULTI_POLYGON"
                },
                "MULTIPOLYGON": {
                    "type": "GEOMETRY",
                    "geometryType": "MULTI_POLYGON"
                },
                "POLYGON": {
                    "type": "GEOMETRY",
                    "geometryType": "POLYGON"
                },
                "MULTILINESTRING": {
                    "type": "GEOMETRY",
                    "geometryType": "MULTI_LINE_STRING"
                },
                "LINESTRING": {
                    "type": "GEOMETRY",
                    "geometryType": "LINE_STRING"
                },
                "GEOMETRY": {
                    "type": "GEOMETRY",
                    "geometryType": "ANY"
                },
                "bpchar": {
                    "type": "STRING"
                },
                "text": {
                    "type": "STRING"
                },
                "uuid": {
                    "type": "STRING"
                },
                "varchar": {
                    "type": "STRING"
                },
                "char": {
                    "type": "STRING"
                },
                "string": {
                    "type": "STRING"
                }
            }
        },
        MapServer: {
            dev: {
                instances: [],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{service_name}}_ms",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    shapePath: "/apps/mapserver/ms_service"
                }
            },
            stage: {
                instances: [],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{service_name}}_ms",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    shapePath: "/apps/mapserver/ms_service"
                }
            },
            prod: {
                instances: [],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{service_name}}_ms",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1",
                softwareSpecificParameter: {
                    shapePath: "/apps/mapserver/ms_service"
                }
            },
            seperateDevWorkspace: false,
            supportedApiTypes: {
                wms: {
                    availableProjections: ["CRS:84", "EPSG:25832", "EPSG:25833", "EPSG:4326", "EPSG:4258", "EPSG:31467", "EPSG:3857", "EPSG:3044", "EPSG:3034", "EPSG:3035"]
                },
                wmstime: {
                    availableProjections: ["CRS:84", "EPSG:25832", "EPSG:25833", "EPSG:4326", "EPSG:4258", "EPSG:31467", "EPSG:3857", "EPSG:3044", "EPSG:3034", "EPSG:3035"]
                },
                wmts: {}
            },
            inspireSettings: {
                languageCode: "ger"
            }
        },
        GWC: {
            dev: {
                instances: ["geowebcache"],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{folder}}/service/wms",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1"
            },
            stage: {
                instances: ["geowebcache"],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{folder}}/service/wms",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1"
            },
            prod: {
                instances: ["geowebcache"],
                urlIntTemplate: "{{protocol}}://{{server}}{{domain}}/{{folder}}/service/wms",
                urlExtTemplate: "{{protocol}}://{{server}}/{{service_name}}",
                defaultServer: "server1"
            },
            supportedApiTypes: {
                wms: {},
                wmts: {}
            }
        },
        Proxy: {
            dev: {
                instances: [],
                urlIntTemplate: "",
                urlExtTemplate: "",
                defaultServer: ""
            },
            stage: {
                instances: [],
                urlIntTemplate: "",
                urlExtTemplate: "",
                defaultServer: ""
            },
            prod: {
                instances: [],
                urlIntTemplate: "",
                urlExtTemplate: "",
                defaultServer: ""
            },
            supportedApiTypes: {
                wms: {},
                wmts: {},
                wfs: {},
                wfst: {},
                oaf: {},
                wps: {},
                vectortiles: {},
                geojson: {},
                terrain3d: {},
                tileset3d: {},
                sta: {},
                oblique: {},
                elastic: {}
            }
        },
        ReverseProxy: {
            dev: {
                instances: [],
                urlIntTemplate: "",
                urlExtTemplate: "",
                defaultServer: ""
            },
            stage: {
                instances: [],
                urlIntTemplate: "",
                urlExtTemplate: "",
                defaultServer: ""
            },
            prod: {
                instances: [],
                urlIntTemplate: "",
                urlExtTemplate: "",
                defaultServer: ""
            },
            supportedApiTypes: {
                wms: {},
                wmts: {},
                wfs: {},
                wfst: {},
                oaf: {},
                wps: {},
                vectortiles: {},
                geojson: {},
                terrain3d: {},
                tileset3d: {},
                sta: {},
                oblique: {},
                elastic: {}
            }
        }
    }
};

module.exports = {
    getConfig: function () {
        return config;
    },

    getConfigEl: function (key) {
        if (key.indexOf(".") > 0) {
            config[key.split(".")[0]][key.split(".")[1]];
        }
        else {
            return config[key];
        }
    },

    setConfig: function (key, value) {
        if (key.indexOf(".") > 0) {
            config[key.split(".")[0]][key.split(".")[1]] = value;
        }
        else {
            config[key] = value;
        }
    }
};
