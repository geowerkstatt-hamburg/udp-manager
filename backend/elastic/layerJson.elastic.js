const Logger = require("../utils/logger.util");
const {Client} = require("@elastic/elasticsearch");

/**
 * insert/update/delete layer-json objects in elasticsearch indexes
 * @param {Object} db_udpm - udp-manager db connection.
 * @returns {void}
 */
module.exports = ({db_udpm}) => ({
    /**
     * called after layer json object (re-)generation. Inserts layer json via http post request in ea index
     * @param {Object} layer_data - all params regarding a layer object
     * @param {Object} json_data - layer json objects for intranet/internet
     * @param {Boolean} prod - true if prod layer json is submitted
     * @returns {void}
     */
    async updateIndex (layer_data, json_data, prod) {
        const filter_keywords = layer_data.dataset_params.filter_keywords ? layer_data.dataset_params.filter_keywords : [];
        const restriction_level = layer_data.dataset_params.restriction_level;
        const url_objects = [];
        const es_config = await db_udpm.exec.getElasticConfig();

        if (es_config.indexes) {
            // for every index configured, generate collection with url and layer json object
            for (const es_index_params of es_config.indexes) {
                let include_check = true;
                let exclude_check = true;

                es_index_params.keyword_filter_include.forEach((kw) => {
                    if (kw !== "") {
                        if (filter_keywords) {
                            if (filter_keywords.indexOf(kw) === -1) {
                                include_check = false;
                            }
                        }
                    }
                });
                es_index_params.keyword_filter_exclude.forEach((kw) => {
                    if (kw !== "") {
                        if (filter_keywords) {
                            if (filter_keywords.indexOf(kw) > -1) {
                                exclude_check = false;
                            }
                        }
                    }
                });

                let insert_intranet_internet = true;
                let insert_prod = true;

                if (restriction_level === "intranet" && !es_index_params.internal) {
                    insert_intranet_internet = false;
                }

                if ((es_index_params.only_prod && !prod) || (es_index_params.only_stage && prod)) {
                    insert_prod = false;
                }

                // check if layer json object must be inserted in es index
                if (include_check && exclude_check && insert_intranet_internet && insert_prod) {
                    for (const val_es_server_name of es_index_params.server) {
                        if (es_index_params.internal) {
                            url_objects.push({url: val_es_server_name, index: es_index_params.name, params: JSON.parse(json_data.json_intranet_es), id: layer_data.id});
                        }
                        else {
                            url_objects.push({url: val_es_server_name, index: es_index_params.name, params: JSON.parse(json_data.json_internet_es), id: layer_data.id});
                        }
                    }
                }
            }

            for (const url_object of url_objects) {
                try {
                    const client = new Client({
                        node: url_object.url,
                        requestTimeout: 1000
                    });

                    await client.index({
                        index: url_object.index,
                        id: url_object.id,
                        document: url_object.params
                    });
                }
                catch (error) {
                    Logger.error(error);
                }
            }
        }
        else {
            Logger.debug("no elastic indexes found");
        }
    },

    /**
     * Delete layer json object from index
     * @param {Object} params - Object with layer-id (id)
     * @returns {void}
     */
    async deleteFromIndex (params) {
        const es_config = await db_udpm.exec.getElasticConfig();

        if (es_config.indexes) {
            for (const es_index_params of es_config.indexes) {
                for (const val_es_server_name of es_index_params.server) {
                    Logger.debug("Delete Layer " + params.id + " from ES index " + es_index_params.name);

                    try {
                        const client = new Client({
                            node: val_es_server_name,
                            requestTimeout: 1000
                        });

                        await client.delete({id: params.id, index: es_index_params.name});
                    }
                    catch (error) {
                        Logger.error(error);
                    }
                }
            }
        }
    },

    /**
     * delete and reinitialize all configured elastic indexes
     * @returns {void}
     */
    async reDeployIndexes () {
        const result = {status: "success"};
        const layers = await db_udpm.exec.getLayerJsonES();
        const filteredObjects = {
            prod: {},
            qs: {}
        };
        const es_config = await db_udpm.exec.getElasticConfig();

        for (const es_index_params of es_config.indexes) {
            // create arrays, one per index, for filtered list of layer jsons
            const indexObject = {
                layerjson: [],
                server: es_index_params.server
            };

            if (es_index_params.only_prod) {
                filteredObjects.prod[es_index_params.name] = indexObject;
            }
            else {
                filteredObjects.qs[es_index_params.name] = indexObject;
            }

            // delete and create indices
            for (const val_es_server_name of es_index_params.server) {
                Logger.info("Delete Index " + es_index_params.name);

                const client = new Client({
                    node: val_es_server_name,
                    requestTimeout: 1000
                });

                try {
                    await client.indices.delete({
                        index: es_index_params.name,
                        ignore_unavailable: true
                    });
                }
                catch (error) {
                    result.status = "error";
                    Logger.error(error);
                }

                Logger.info("Create Index " + es_index_params.name);

                try {
                    await client.indices.create({
                        index: es_index_params.name,
                        settings: es_config.settings,
                        mappings: es_config.mappings
                    });
                }
                catch (error) {
                    result.status = "error";
                    Logger.error(error);
                }
            }
        }

        // fill arrays per index with layer jsons
        for (const layer of layers) {
            for (const es_index_params of es_config.indexes) {
                let include_check = true;
                let exclude_check = true;

                es_index_params.keyword_filter_include.forEach((kw) => {
                    if (kw !== "") {
                        if (layer.keywords) {
                            if (layer.keywords.indexOf(kw) === -1) {
                                include_check = false;
                            }
                        }
                    }
                });
                es_index_params.keyword_filter_exclude.forEach((kw) => {
                    if (kw !== "") {
                        if (layer.keywords) {
                            if (layer.keywords.indexOf(kw) > -1) {
                                exclude_check = false;
                            }
                        }
                    }
                });

                let insert_intranet_internet = true;

                if (layer.restriction_level === "intranet" && !es_index_params.internal) {
                    insert_intranet_internet = false;
                }

                if (include_check && exclude_check && es_index_params.only_prod && layer.status_prod && insert_intranet_internet) {
                    if (layer.json_intranet_es_prod && layer.json_internet_es_prod) {
                        filteredObjects.prod[es_index_params.name].layerjson.push(es_index_params.internal ? layer.json_intranet_es_prod : layer.json_internet_es_prod);
                    }
                }
                else if (include_check && exclude_check && es_index_params.only_stage && (layer.status_stage || layer.status_dev || layer.external) && insert_intranet_internet) {
                    if (layer.json_intranet_es && layer.json_internet_es) {
                        filteredObjects.qs[es_index_params.name].layerjson.push(es_index_params.internal ? layer.json_intranet_es : layer.json_internet_es);
                    }
                }
            }
        }

        // bulk insert json objects into indices
        for (const index in filteredObjects.qs) {
            const operations = filteredObjects.qs[index].layerjson.flatMap(doc => [{index: {_index: index, _id: doc.id}}, doc]);

            if (operations.length > 0) {
                for (const val_es_server_name of filteredObjects.qs[index].server) {
                    try {
                        const client = new Client({
                            node: val_es_server_name
                        });

                        const bulkResponse = await client.bulk({refresh: true, body: operations});

                        if (bulkResponse.errors) {
                            result.status = "error";
                        }
                    }
                    catch (error) {
                        Logger.error(error);
                        result.status = "error";
                    }
                }

                Logger.debug(`Index ${index} updated with ${filteredObjects.qs[index].layerjson.length} objects`);
            }
            else {
                Logger.debug(`No Objects for index ${index}`);
            }
        }

        // bulk insert json objects into indices
        for (const index in filteredObjects.prod) {
            const operations = filteredObjects.prod[index].layerjson.flatMap(doc => [{index: {_index: index, _id: doc.id}}, doc]);

            if (operations.length > 0) {
                for (const val_es_server_name of filteredObjects.prod[index].server) {
                    try {
                        const client = new Client({
                            node: val_es_server_name
                        });

                        const bulkResponse = await client.bulk({refresh: true, body: operations});

                        if (bulkResponse.errors) {
                            result.status = "error";
                        }
                    }
                    catch (error) {
                        Logger.error(error);
                        result.status = "error";
                    }
                }

                Logger.debug(`Index ${index} updated with ${filteredObjects.prod[index].layerjson.length} objects`);
            }
            else {
                Logger.debug(`No Objects for index ${index}`);
            }
        }

        return result;
    }
});
