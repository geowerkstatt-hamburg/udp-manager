require("dotenv").config();
const express = require("express");
const Logger = require("./utils/logger.util");
const moment = require("moment");
const {db_udpm, db_ext} = require("./db");
const axios = require("axios");
const layerJsonElastic = require("./elastic/layerJson.elastic")({axios, db_udpm});

async function startServer () {
    const app = express();
    const server = require("http").createServer(app);
    const io = require("socket.io")(server, {
        path: "/udpm_ws"
    });

    let keycloakIssuer = null;

    if (process.env.AUTH_METHOD === "keycloak") {
        const {Issuer} = require("openid-client");

        keycloakIssuer = await Issuer.discover(process.env.KC_ISSUER_URL);
    }

    await require("./loaders")(app, db_udpm, db_ext, io, axios, layerJsonElastic, keycloakIssuer);

    const listen_host = process.env.NODE_HOST ? process.env.NODE_HOST.trim() : "localhost";
    const listen_port = process.env.NODE_PORT ? process.env.NODE_PORT.trim() : 3000;

    server.listen(listen_port, listen_host, () => {
        Logger.info(moment().format("YYYY-MM-DD HH:mm:ss") + " - listening on " + listen_host + ":" + listen_port);
    }).on("error", err => {
        Logger.error(err);
    });
}

startServer();
