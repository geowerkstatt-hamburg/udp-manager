const Logger = require("../utils/logger.util");
const dataCache = require("../utils/data_cache.util");

/**
 * on Program start this function checks if the db_version table is accassible and if version info is inside.
 * If no data could be selected, a check is run, if the database is empty. Is  any schema (besides public) is inside
 * the flag dbValid is set to false.
 * @param {Object} db_udpm - pgPromise DB Object
 * @returns {void}
 */
module.exports = async (db_udpm) => {
    try {
        const result = await db_udpm.exec.getDBVersion();

        dataCache.setDbVersion(result[0].version);
        dataCache.setDbEmpty(false);

        Logger.debug("DB Version: " + result[0].version);
    }
    catch (error) {
        const result = await db_udpm.exec.getSchemaList();

        if (result.length === 0) {
            Logger.error("DB is empty");
            dataCache.setDbEmpty(true);
        }
        else {
            Logger.error("DB schema is not valid");
            dataCache.setDbValid(false);
        }
    }
};
