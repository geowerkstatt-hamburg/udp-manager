const express = require("express");
const {create} = require("express-handlebars");
const path = require("path");
const env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

/**
 * set application routes, middlewares and templating engine
 *
 * @param {Object} app - express app Object
 * @param {Object} db_udpm - pg-promise db repo for UDP-Manager DB
 * @param {Object} db_ext - pg-promise db repo for external UDP-Manager DB
 * @param {Object} axios - module for generating http requests
 * @param {Object} layerJsonElastic - elastic index management Module
 * @param {Object} keycloakIssuer - Keycloak Issuer
 * @return {void}
 * @return {void}
 */
module.exports = (app, db_udpm, db_ext, axios, layerJsonElastic, keycloakIssuer) => {
    const routes = require("../api/routes")(db_udpm, db_ext, axios, layerJsonElastic);

    app.use(express.urlencoded({extended: true, limit: "5mb"}));
    app.use(express.json({limit: "5mb"}));

    const hbs = create({
        defaultLayout: "main",
        helpers: {
            toJSON: function (object) {
                return JSON.stringify(object);
            }
        },
        layoutsDir: path.join(__dirname, "../../frontend/views/layouts")
    });

    app.engine("handlebars", hbs.engine);
    app.set("view engine", "handlebars");
    app.set("views", path.join(__dirname, "../../frontend/views"));

    if (process.env.AUTH_METHOD === "keycloak") {
        const {Strategy} = require("openid-client");
        const passport = require("passport");
        const expressSession = require("express-session");
        const client = new keycloakIssuer.Client({
            client_id: process.env.KC_CLIENT_ID,
            client_secret: process.env.KC_CLIENT_SECRET,
            redirect_uris: [process.env.KC_REDIRECT_URL],
            post_logout_redirect_uris: [process.env.KC_POST_LOGOUT_REDIRECT_URL],
            response_types: ["code"]
        });
        const memoryStore = new expressSession.MemoryStore();

        app.use(
            expressSession({
                secret: process.env.SECRET,
                resave: false,
                saveUninitialized: true,
                store: memoryStore
            })
        );

        app.use(passport.initialize());
        app.use(passport.authenticate("session"));

        // this creates the strategy
        passport.use("oidc", new Strategy({client}, (tokenSet, userinfo, done) => {
            return done(null, tokenSet.claims());
        }));

        passport.serializeUser(function (user, done) {
            done(null, user);
        });

        passport.deserializeUser(function (user, done) {
            done(null, user);
        });

        // default protected route /test
        app.get("/login", (req, res, next) => {
            passport.authenticate("oidc")(req, res, next);
        });

        // callback always routes to test
        app.get("/auth/callback", (req, res, next) => {
            passport.authenticate("oidc", {
                successRedirect: "/app",
                failureRedirect: "/"
            })(req, res, next);
        });

        // start logout request
        app.get("/logout", (req, res) => {
            res.redirect(client.endSessionUrl());
        });

        // logout callback
        app.get("/logout/callback", (req, res) => {
            // clears the persisted user from the local storage
            req.logout(function (err) {
                if (err) {
                    return next(err);
                }
                else {
                    res.redirect("/");
                }
            });
        });

        const authorizationKeycloak = require("../api/middleware/authorizationKeycloak.middleware.js");

        app.use(authorizationKeycloak);
    }
    else if (process.env.AUTH_METHOD === "ldap") {
        const authorizationLdap = require("../api/middleware/authorizationLdap.middleware.js");

        app.use(authorizationLdap);
    }
    else {
        const authorizationLocal = require("../api/middleware/authorizationLocal.middleware.js");

        app.use(authorizationLocal);
    }

    app.use("/favicon.ico", express.static(path.join(__dirname, "../../frontend/resources/images/favicon.ico")));

    app.use("/tmp", express.static(path.join(__dirname, "../../tmp")));

    app.use("/resources", express.static(path.join(__dirname, "../../frontend/resources")));
    app.use("/resources/libs", express.static(path.join(__dirname, "../../node_modules")));
    app.use("/testresources", express.static(path.join(__dirname, "../../tests/integration/externalDataset")));
    app.use("/ext", express.static(path.join(__dirname, "../../frontend/ext")));

    app.get("/classic.json", function (req, res) {
        res.sendFile(path.join(__dirname, "../../frontend/classic.json"));
    });

    app.get("/bootstrap.js", function (req, res) {
        res.sendFile(path.join(__dirname, "../../frontend/bootstrap.js"));
    });

    app.get("/bootstrap.json", function (req, res) {
        res.sendFile(path.join(__dirname, "../../frontend/bootstrap.json"));
    });

    app.get("/app.js", function (req, res) {
        res.sendFile(path.join(__dirname, "../../frontend/app.js"));
    });

    if (env === "dev") {
        app.use("/build", express.static(path.join(__dirname, "../../frontend/build")));
        app.use("/.sencha", express.static(path.join(__dirname, "../../frontend/.sencha")));
        app.use("/src", express.static(path.join(__dirname, "../../frontend/src")));
    }

    app.use("/backend", routes);

    app.get("/", function (req, res) {
        if (process.env.AUTH_METHOD === "keycloak") {
            res.redirect("login");
        }
        else {
            res.redirect("app");
        }
    });
};
