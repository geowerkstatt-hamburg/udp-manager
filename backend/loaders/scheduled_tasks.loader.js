const schedule = require("node-schedule");
const Logger = require("../utils/logger.util");
const dataCache = require("../utils/data_cache.util");
const config = require("../config.js").getConfig();

/**
 * Initialiszes all enabled scheduled tasks
 * @param {Object} db_udpm - pg-promise db repo
 * @return {void}
 */
module.exports = async (db_udpm) => {
    try {
        const results = await db_udpm.exec.getScheduledTasks();
        const cronregex = new RegExp(/^(\*|([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])|\*\/([0-9]|1[0-9]|2[0-9]|3[0-9]|4[0-9]|5[0-9])) (\*|([0-9]|1[0-9]|2[0-3])|\*\/([0-9]|1[0-9]|2[0-3])) (\*|([1-9]|1[0-9]|2[0-9]|3[0-1])|\*\/([1-9]|1[0-9]|2[0-9]|3[0-1])) (\*|([1-9]|1[0-2])|\*\/([1-9]|1[0-2])) (\*|([0-6])|\*\/([0-6]))$/);
        const scheduledTasks = {};

        for (const scheduled_task of results) {
            const scheduled_task_obj = require("../tasks/" + scheduled_task.name + ".task")({db_udpm});

            if (scheduled_task.name === "git_push" && !config.modules.git) {
                scheduled_task.enabled = false;
            }

            if (scheduled_task.enabled && cronregex.test(scheduled_task.schedule)) {
                scheduledTasks[scheduled_task.name] = schedule.scheduleJob(scheduled_task.schedule, function () {
                    scheduled_task_obj.execute();
                });

                Logger.info("Scheduled Task " + scheduled_task.name + " initialized");
            }

            if (scheduled_task.execute_on_startup) {
                scheduled_task_obj.execute();
            }
        }

        // save alle scheduled tasks objects in dataCache (for rescheduling)
        dataCache.setScheduledTasks(scheduledTasks);
    }
    catch (error) {
        Logger.error(error);
    }
};
