/**
 * listens to websocket events
 * @param {Object} io - socket.io object
 * @returns {void}
 */
module.exports = (io) => {
    io.on("connection", function (socket) {
        // if any client changes dataset details, a message will be broadcasted to every connected client to reload the datasets store.
        socket.on("dataset_details_changed", function () {
            socket.broadcast.emit("update_dataset_list");
        });
    });
};
