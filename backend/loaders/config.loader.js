const Logger = require("../utils/logger.util");
const configGenerator = require("../utils/configGenerator.util");
const cryptor = require("../utils/cryptor.util");

module.exports = async (db_udpm) => {
    try {
        const config_db = await db_udpm.exec.getAppConfig();
        const config_webdav = await db_udpm.exec.getWebDavConnections();

        config_webdav.forEach(element => {
            if (element.password) {
                if (element.password.indexOf("__") > 0) {
                    element.password = cryptor.decrypt(element.password);
                }
            }
        });

        const config_fmeserver = await db_udpm.exec.getFmeServerConnections();

        config_fmeserver.forEach(element => {
            if (element.password) {
                if (element.password.indexOf("__") > 0) {
                    element.password = cryptor.decrypt(element.password);
                }
            }
        });

        configGenerator.prepare(config_db, config_webdav, config_fmeserver);
    }
    catch (error) {
        Logger.error(error);
    }
};
