const fs = require("fs").promises;
const path = require("path");
const config = require("../config.js").getConfig();
const Logger = require("../utils/logger.util");

/**
 * Check if folder, configured under process.env.CONFIG_OUT_PATH, exists. If not the required folder structure will be created.
 * If git is configured and no repository is checked out under process.env.CONFIG_OUT_PATH, git clone will be executed.
 * @returns {void}
 */
module.exports = async () => {
    // check if outPath folder exists, create if not
    try {
        await fs.access(process.env.CONFIG_OUT_PATH);
    }
    catch (err) {
        await fs.mkdir(process.env.CONFIG_OUT_PATH);
    }

    const gitUtil = require("../utils/git.util");

    // if git is configured, clone repository
    if (config.modules.git) {
        try {
            await fs.access(path.join(process.env.CONFIG_OUT_PATH, "/.git"));
        }
        catch (err) {
            Logger.info("Cloning Repository...");
            await gitUtil.clone();
        }
    }
    else {
        // create basic folder Structure
        try {
            await fs.access(path.join(process.env.CONFIG_OUT_PATH, "/configs"));
        }
        catch (err) {
            await fs.mkdir(path.join(process.env.CONFIG_OUT_PATH, "/configs"));
        }
        try {
            await fs.access(path.join(process.env.CONFIG_OUT_PATH, "/workspaces"));
        }
        catch (err) {
            await fs.mkdir(path.join(process.env.CONFIG_OUT_PATH, "/workspaces"));
        }
        try {
            await fs.access(path.join(process.env.CONFIG_OUT_PATH, "/static_files/"));
        }
        catch (err) {
            await fs.mkdir(path.join(process.env.CONFIG_OUT_PATH, "/static_files/"));
        }
    }
};
