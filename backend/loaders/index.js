const configLoader = require("./config.loader");
const expressLoader = require("./express.loader");
const checkDBLoader = require("./check_db.loader");
const scheduledTasksLoader = require("./scheduled_tasks.loader");
const websocketLoader = require("./websocket.loader");
const checkOutPathLoader = require("./checkOutPath.loader");
const Logger = require("../utils/logger.util");
const dataCache = require("../utils/data_cache.util");

module.exports = async (expressApp, db_udpm, db_ext, io, axios, layerJsonElastic, keycloakIssuer) => {
    await checkDBLoader(db_udpm);
    Logger.info("CheckDB loaded");
    await configLoader(db_udpm);
    Logger.info("config loaded");
    await checkOutPathLoader();
    Logger.info("checkOutPath loaded");
    await expressLoader(expressApp, db_udpm, db_ext, axios, layerJsonElastic, keycloakIssuer);
    Logger.info("Express loaded");

    if (!dataCache.getDbEmpty() && dataCache.getDbValid()) {
        await scheduledTasksLoader(db_udpm);
        Logger.info("ScheduledTasks loaded");

        websocketLoader(io);
        Logger.info("Websocket loaded");
    }
};
