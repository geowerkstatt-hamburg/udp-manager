const Logger = require("../utils/logger.util");
const dataCache = require("../utils/data_cache.util");
const configLoader = require("../loaders/config.loader");

module.exports = ({db_udpm}) => ({
    async clearDb () {
        try {
            await db_udpm.exec.clearDb();

            dataCache.setDbEmpty(true);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async initDb () {
        if (dataCache.getDbEmpty()) {
            try {
                await db_udpm.exec.initDb();

                const config = require("../config.js").getConfig();

                const defaultConfig = {};

                defaultConfig.ldap = JSON.stringify(config.ldap);
                defaultConfig.jira = JSON.stringify(config.jira);
                defaultConfig.mail = JSON.stringify(config.mail);
                defaultConfig.capabilities_metadata = JSON.stringify(config.capabilitiesMetadata);
                defaultConfig.test_bbox = JSON.stringify(config.testBbox);
                defaultConfig.service_types = JSON.stringify(config.serviceTypes);
                defaultConfig.modules = JSON.stringify(config.modules);
                defaultConfig.server = JSON.stringify(config.server);
                defaultConfig.client_config = JSON.stringify(config.clientConfig);
                defaultConfig.git = JSON.stringify(config.git);
                defaultConfig.software = JSON.stringify(config.software);
                defaultConfig.defaultbbox = config.defaultBbox;
                defaultConfig.available_projections = JSON.stringify(config.availableProjections);
                defaultConfig.layer_attributes_excludes = JSON.stringify(config.layerAttributesExcludes);

                await db_udpm.exec.addDefaultConfig(defaultConfig);

                await configLoader(db_udpm);

                dataCache.setDbEmpty(false);

                return "Initialisierung erfolgreich!";
            }
            catch (error) {
                Logger.error(error);

                return error.message;
            }
        }
        else {
            return "Datenbank nicht leer!";
        }
    },

    async updateDb () {
        const version_history = dataCache.getVersionHistory();
        const version_index = version_history.findIndex(version_obj => version_obj.version === dataCache.getDbVersion());

        if (version_index === version_history.length - 1) {
            return "Datenbankversion bereits auf aktuellem Stand!";
        }
        else {
            try {
                for (let i = version_index + 1; i < version_history.length; i++) {
                    Logger.debug("Aktualisierung auf v" + version_history[i].version);
                    await db_udpm.exec.updateDb(version_history[i].version.replace(/\./g, "_"));
                }

                if (version_history[version_history.length - 1].version === "2.0") {
                    const config = require("../config.js").getConfig();

                    const defaultConfig = {};

                    defaultConfig.ldap = JSON.stringify(config.ldap);
                    defaultConfig.jira = JSON.stringify(config.jira);
                    defaultConfig.mail = JSON.stringify(config.mail);
                    defaultConfig.capabilities_metadata = JSON.stringify(config.capabilitiesMetadata);
                    defaultConfig.test_bbox = JSON.stringify(config.testBbox);
                    defaultConfig.service_types = JSON.stringify(config.serviceTypes);
                    defaultConfig.modules = JSON.stringify(config.modules);
                    defaultConfig.server = JSON.stringify(config.server);
                    defaultConfig.client_config = JSON.stringify(config.clientConfig);
                    defaultConfig.git = JSON.stringify(config.git);
                    defaultConfig.software = JSON.stringify(config.software);
                    defaultConfig.defaultbbox = JSON.stringify(config.defaultBbox);
                    defaultConfig.available_projections = JSON.stringify(config.availableProjections);
                    defaultConfig.layer_attributes_excludes = JSON.stringify(config.layerAttributesExcludes);

                    await db_udpm.exec.addDefaultConfig(defaultConfig);
                }

                await configLoader(db_udpm);

                dataCache.setDbVersion(version_history[version_history.length - 1].version);

                return "Aktualisierung erfolgreich!";
            }
            catch (error) {
                Logger.error(error);

                return error.message;
            }
        }
    }
});
