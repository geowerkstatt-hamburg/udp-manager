const Logger = require("../utils/logger.util");
const ldap = require("../utils/ldap.util");

module.exports = () => ({
    async getADGroups (params) {
        try {
            const results = await ldap.findGroups(params.groupname);

            return results;
        }
        catch (error) {
            Logger.error("ad.service - getADGroups:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getGroupMembershipForUser (params) {
        try {
            const results = await ldap.getGroupMembershipForUser(params);

            return results;
        }
        catch (error) {
            Logger.error("ad.service - getGroupMembershipForUser:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getADUsers (params) {
        try {
            if (params.mail) {
                const results = await ldap.findUsersByMail(params.mail);

                return results;
            }
            else if (params.surname && params.givenname) {
                const results = await ldap.findUsers(params.surname, params.givenname);

                return results;
            }
            else {
                return {status: "error", message: "must provide givenname and surname or mail."};
            }

        }
        catch (error) {
            Logger.error("ad.service - getADUsers:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getADUserDetails (params) {
        try {
            if (params.accountname || params.accountName) {
                if (params.accountName) {
                    params.accountname = params.accountName;
                }
                if (params.accountname.indexOf("%5C") > -1) {
                    params.accountname = params.accountname.split("%5C")[1];
                }
                if (params.accountname.indexOf("\\") > -1) {
                    params.accountname = params.accountname.split("\\")[1];
                }

                const results = await ldap.findUser(params.accountname);

                return results;
            }
            else if (params.objectguid) {
                const results = await ldap.findUserByObjectGuid(params.objectguid);

                return results;
            }
            else {
                return {status: "error", message: "must provide accountname or objectguid."};
            }

        }
        catch (error) {
            Logger.error("ad.service - getADUsers:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    }
});
