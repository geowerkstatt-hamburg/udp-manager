const Logger = require("../utils/logger.util");
const https_agent = require("../utils/https_agent.util");
const capabilities_parser = require("../utils/capabilities_parser.util");

module.exports = ({axios}) => ({

    /**
     * Collecting informations from the capabilities of wms and wfs services and join them together
     * @param {Object} service_info - all needed informations about the service
     * @returns {Object} - needed informations for collections and their layers to create in DB
     */
    async getServiceCollections (service_info) {
        const gc_promises = [];
        const config = require("../config.js").getConfig();

        // prepare capabilities requests for wms and wfs service
        if (service_info.wms) {
            service_info.wms.proxy_settings = service_info.wms.software === "Extern" || service_info.wms.external_service ? config.proxy : false;
            service_info.wms.https_url = service_info.wms.url.indexOf("https:") > -1;
            service_info.wms.httpsAgent = service_info.wms.https_url ? https_agent.getHttpsAgent(service_info.wms.proxy_settings) : null;

            if (service_info.wms.url.indexOf("?") > -1) {
                service_info.wms.gc_url = service_info.wms.url + "&Service=WMS&Request=GetCapabilities&Version=" + service_info.wms.version;
            }
            else {
                service_info.wms.gc_url = service_info.wms.url + "?Service=WMS&Request=GetCapabilities&Version=" + service_info.wms.version;
            }

            if (service_info.wms.is_mrh) {
                service_info.wms.headers = {"X-Alt-Referer": "http://geoportal.metropolregion.hamburg.de/mrhportal"};
            }
            else {
                service_info.wms.headers = null;
            }
        }
        if (service_info.wfs) {
            service_info.wfs.proxy_settings = service_info.wfs.software === "Extern" || service_info.wfs.external_service ? config.proxy : false;
            service_info.wfs.https_url = service_info.wfs.url.indexOf("https:") > -1;
            service_info.wfs.httpsAgent = service_info.wfs.https_url ? https_agent.getHttpsAgent(service_info.wfs.proxy_settings) : null;

            if (service_info.wfs.url.indexOf("?") > -1) {
                service_info.wfs.gc_url = service_info.wfs.url + "&Service=WFS&Request=GetCapabilities&Version=" + service_info.wfs.version;
            }
            else {
                service_info.wfs.gc_url = service_info.wfs.url + "?Service=WFS&Request=GetCapabilities&Version=" + service_info.wfs.version;
            }

            if (service_info.wfs.is_mrh) {
                service_info.wfs.headers = {"X-Alt-Referer": "http://geoportal.metropolregion.hamburg.de/mrhportal"};
            }
            else {
                service_info.wfs.headers = null;
            }
        }
        if (service_info.wmts) {
            service_info.wmts.proxy_settings = service_info.wmts.software === "Extern" || service_info.wmts.external_service ? config.proxy : false;
            service_info.wmts.https_url = service_info.wmts.url.indexOf("https:") > -1;
            service_info.wmts.httpsAgent = service_info.wmts.https_url ? https_agent.getHttpsAgent(service_info.wmts.proxy_settings) : null;

            if (service_info.wmts.url.indexOf("?") > -1) {
                service_info.wmts.gc_url = service_info.wmts.url + "&Service=WFS&Request=GetCapabilities&Version=" + service_info.wmts.version;
            }
            else {
                service_info.wmts.gc_url = service_info.wmts.url + "?Service=WFS&Request=GetCapabilities&Version=" + service_info.wmts.version;
            }

            if (service_info.wmts.is_mrh) {
                service_info.wmts.headers = {"X-Alt-Referer": "http://geoportal.metropolregion.hamburg.de/mrhportal"};
            }
            else {
                service_info.wmts.headers = null;
            }
        }

        // bundle the capabilities requests in an axios promise array
        for (const [key, value] of Object.entries(service_info)) {
            gc_promises.push(axios({
                method: "get",
                url: value.gc_url,
                headers: value.headers,
                proxy: value.https_url ? false : value.proxy_settings,
                httpsAgent: value.httpsAgent,
                service_type: value.service_type,
                version: value.version
            }));
        }
        try {
            const get_capabilities = await axios.all(gc_promises);
            const cap_data = {};
            const check_result = {};

            get_capabilities.forEach(service => {
                if (service.config.service_type === "WMS" || service.config.service_type === "WMS-Time") {
                    cap_data.wms = capabilities_parser.parseWMSCapabilities(service.data, true, service.config.version);
                }

                if (service.config.service_type === "WFS" || service.config.service_type === "WFS-T") {
                    cap_data.wfs = capabilities_parser.parseWFSCapabilities(service.data, true, service.config.version);
                }

                if (service.config.service_type === "WMTS") {
                    cap_data.wmts = capabilities_parser.parseWMTSCapabilities(service.data, true, service.config.version);
                }
            });

            for (const [key, value] of Object.entries(cap_data)) {
                if (value.version_check) {
                    value.collection_list.forEach(collection => {
                        collection.type = collection.group_layer ? "groupLayer" : "layer";
                        collection["service_id_" + key] = service_info[key].service_id;
                        collection["service_name_" + key] = service_info[key].service_title;
                        if (config.clientConfig.modules.externalCollectionImport) {
                            collection.service_type = key.toUpperCase();
                        }
                    });
                    check_result[key] = true;
                }
                else {
                    check_result[key] = false;
                }
            }
            if (Object.keys(check_result).length === 2) {
                if (!check_result.wms && !check_result.wfs) {
                    return {status: "error", errorMessage: "WMS und WFS Dienst Versionen stimmt nicht überein."};
                }
                else if (!check_result.wms) {
                    return {status: "error", errorMessage: "WMS Dienst Version stimmt nicht überein."};
                }
                else if (!check_result.wfs) {
                    return {status: "error", errorMessage: "WFS Dienst Version stimmt nicht überein."};
                }
                else {
                    return {status: "success", result_data: cap_data};
                }
            }
            else if (Object.keys(check_result).length === 1) {
                if (check_result.wms || check_result.wfs || check_result.wmts) {
                    return {status: "success", result_data: cap_data};
                }
                else if (!check_result.wms || !check_result.wfs || !check_result.wmts) {
                    return {status: "error", errorMessage: Object.keys(check_result)[0].toUpperCase() + " Dienst Version stimmt nicht überein."};
                }
            }
        }
        catch (error) {
            Logger.error("ogcapi.service - getServiceCollections:");
            Logger.error(error);
            return {status: "error", message: error.message, response: error.response.data};
        }

    }
});
