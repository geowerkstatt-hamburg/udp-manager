const Logger = require("../utils/logger.util");
const https_agent = require("../utils/https_agent.util");
const metadata_parser = require("../utils/metadata_parser.util");

module.exports = ({axios, db_udpm}) => ({
    async getDatasetMetadata (params) {
        let search_string = "%" + params.datasettitle + "%";
        let search_filter = "apiso:AnyText";

        if (params.datasetmdid) {
            search_string = params.datasetmdid;
            search_filter = "apiso:Identifier";
        }

        const csw_request = `<csw:GetRecords xmlns:csw="http://www.opengis.net/cat/csw/2.0.2" xmlns:ogc="http://www.opengis.net/ogc" service="CSW" version="2.0.2" resultType="results" startPosition="1" maxRecords="20" outputFormat="application/xml" outputSchema="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/cat/csw/2.0.2 http://schemas.opengis.net/csw/2.0.2/CSW-discovery.xsd" xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:apiso="http://www.opengis.net/cat/csw/apiso/1.0">
        <csw:Query typeNames="csw:Record">
        <csw:ElementSetName>full</csw:ElementSetName>
        <csw:Constraint version="1.1.0">
        <ogc:Filter>
        <ogc:And>
        <ogc:PropertyIsLike matchCase="false" wildCard="%" singleChar="_" escapeChar="\\">
        <ogc:PropertyName>` + search_filter + `</ogc:PropertyName>
        <ogc:Literal>` + search_string + `</ogc:Literal>
        </ogc:PropertyIsLike>
        </ogc:And>
        </ogc:Filter>
        </csw:Constraint>
        </csw:Query>
        </csw:GetRecords>`;

        try {
            const config = require("../config.js").getConfig();
            const metadata_catalogs = await db_udpm.exec.getMetadataCatalog(params);
            const source_csw = metadata_catalogs[0];
            const proxy_settings = source_csw.proxy ? config.proxy : false;
            const https_url = source_csw.csw_url.indexOf("https:") > -1;
            const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

            const csw_results = await axios({
                method: "post",
                url: source_csw.csw_url,
                data: csw_request,
                headers: {
                    "Content-Type": "application/xml"
                },
                proxy: https_url ? false : proxy_settings,
                httpsAgent: httpsAgent
            });

            return metadata_parser.parseDatasetMDArray(csw_results.data);
        }
        catch (error) {
            Logger.error("metadata.service - getDatasetMetadata:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getServiceMetadata (params) {
        let search_string = "%" + params.servicetitle + "%";
        let search_filter = "apiso:AnyText";

        if (params.servicemdid) {
            search_string = params.servicemdid;
            search_filter = "apiso:Identifier";
        }

        const csw_request = `<csw:GetRecords xmlns:csw="http://www.opengis.net/cat/csw/2.0.2" xmlns:ogc="http://www.opengis.net/ogc" service="CSW" version="2.0.2" resultType="results" startPosition="1" maxRecords="20" outputFormat="application/xml" outputSchema="http://www.isotc211.org/2005/gmd" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opengis.net/cat/csw/2.0.2 http://schemas.opengis.net/csw/2.0.2/CSW-discovery.xsd" xmlns:gmd="http://www.isotc211.org/2005/gmd" xmlns:apiso="http://www.opengis.net/cat/csw/apiso/1.0">
        <csw:Query typeNames="csw:Record">
        <csw:ElementSetName>full</csw:ElementSetName>
        <csw:Constraint version="1.1.0">
        <ogc:Filter>
        <ogc:And>
        <ogc:PropertyIsLike matchCase="false" wildCard="%" singleChar="_" escapeChar="\\">
        <ogc:PropertyName>` + search_filter + `</ogc:PropertyName>
        <ogc:Literal>` + search_string + `</ogc:Literal>
        </ogc:PropertyIsLike>
        </ogc:And>
        </ogc:Filter>
        </csw:Constraint>
        </csw:Query>
        </csw:GetRecords>`;

        try {
            const config = require("../config.js").getConfig();
            const metadata_catalogs = await db_udpm.exec.getMetadataCatalog(params);
            const source_csw = metadata_catalogs[0];
            const proxy_settings = source_csw.proxy ? config.proxy : false;
            const https_url = source_csw.csw_url.indexOf("https:") > -1;
            const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

            const csw_results = await axios({
                method: "post",
                url: source_csw.csw_url,
                data: csw_request,
                headers: {
                    "Content-Type": "application/xml"
                },
                proxy: https_url ? false : proxy_settings,
                httpsAgent: httpsAgent
            });

            return metadata_parser.parseServiceMDArray(csw_results.data);
        }
        catch (error) {
            Logger.error("metadata.service - getServiceMetadata:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    }
});
