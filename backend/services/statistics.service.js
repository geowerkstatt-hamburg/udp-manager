const Logger = require("../utils/logger.util");

module.exports = ({db_udpm}) => ({
    async getDistinctMonth (params) {
        try {
            const results = await db_udpm.exec.getDistinctMonth(params);

            return results;
        }
        catch (error) {
            Logger.error("contacts.service - getDistinctMonth:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getVisits (params) {
        try {
            const results = await db_udpm.exec.getVisits(params);

            return results;
        }
        catch (error) {
            Logger.error("contacts.service - getVisits:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getVisitsTop (params) {
        try {
            const results = await db_udpm.exec.getVisitsTop(params);

            return results;
        }
        catch (error) {
            Logger.error("contacts.service - getVisitsTop:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getVisitsTotal (params) {
        try {
            const results = await db_udpm.exec.getVisitsTotal(params);

            return results;
        }
        catch (error) {
            Logger.error("contacts.service - getVisitsTotal:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getSoftwareStats (params) {
        try {
            const results = await db_udpm.exec.getSoftwareStats(params);

            return results;
        }
        catch (error) {
            Logger.error("contacts.service - getSoftwareStats:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getServiceStats (params) {
        try {
            const results = await db_udpm.exec.getServiceStats(params);

            return results;
        }
        catch (error) {
            Logger.error("contacts.service - getServiceStats:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getLayerStats (params) {
        try {
            const results = await db_udpm.exec.getLayerStats(params);

            return results;
        }
        catch (error) {
            Logger.error("contacts.service - getLayerStats:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    }
});
