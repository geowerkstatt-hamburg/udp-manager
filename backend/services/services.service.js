const Logger = require("../utils/logger.util");
const configDataGen = require("../utils/configDataGen.util");
const configDataValidator = require("../utils/configDataValidator.util");
const serviceConfigManagement = require("../utils/serviceConfigManagement.util");
const gitUtil = require("../utils/git.util");
const fs = require("fs").promises;
const moment = require("moment");
const path = require("path");
const mailer = require("../utils/mail.util");
const stringUtil = require("../utils/string.util");
const dataCache = require("../utils/data_cache.util");

module.exports = ({db_udpm, db_ext, managementService, layersService}) => ({
    async getServices () {
        try {
            const results = await db_udpm.exec.getServices();

            return results;
        }
        catch (error) {
            Logger.error("services.service - getServices:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getServicesDataset (params) {
        if (params.dataset_id) {
            try {
                const result = await db_udpm.exec.getServicesDataset(params);

                return result;
            }
            catch (error) {
                Logger.error(error);

                return error;
            }
        }
        else {
            Logger.debug("missing dataset_id");
            return {error: "missing dataset_id"};
        }

    },

    async createService (params) {
        try {
            const config = require("../config.js").getConfig();
            const result = await db_udpm.exec.addService(stringUtil.removeTrailingWhitespacesInObject(params));
            const service_links_ids = {dataset_id: params.dataset_id, service_id: result[0].id};

            await db_udpm.exec.addServiceLink(service_links_ids);

            let preview_text = "";

            params.service_id = result[0].id;

            if ((params.type === "WMS" || params.type === "WMS-Time") && params.status_prod === true) {
                db_udpm.exec.getLayersService(params).then(
                    function (resultObject) {
                        if (resultObject.length > 0 && config.preview_portal_url) {
                            var layerList = "";

                            resultObject.forEach(record => {
                                layerList += record.id + ",";
                            });
                            var previewURL = "<a href=\"" + config.preview_portal_url + "?Map/layerids=" + layerList.substring(0, layerList.length - 1) + "\">Im Vorschauportal betrachten</a>";

                            preview_text += previewURL;
                        }
                        else {
                            preview_text;
                        }
                    }
                );
            }

            // temporarily deactivate this function (send an e-mail when a service is put into production).
            // if (config.mail && params.status === "prod") {
            //     await mailer.onProdService(result[0].id, params.title, params.type, preview_text, params.mail_from);
            // }

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Neue Schnittstelle (ID: ${result[0].id}) angelegt`;
            params.entity = "service";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", id: result[0].id};
        }
        catch (error) {
            Logger.error("services.service - createService:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async updateService (params) {
        try {
            const old_service_values = await db_udpm.exec.getService(stringUtil.removeTrailingWhitespacesInObject(params));
            const old_qa = old_service_values[0].qa;
            const config = require("../config.js").getConfig();

            let preview_text = "";

            params.service_id = params.id;

            if ((params.type === "WMS" || params.type === "WMS-Time") && params.status_prod === true) {
                db_udpm.exec.getLayersService(params).then(
                    function (resultObject) {
                        if (resultObject.length > 0 && config.preview_portal_url) {
                            var layerList = "";

                            resultObject.forEach(record => {
                                layerList += record.id + ",";
                            });
                            var previewURL = "<a href=\"" + config.preview_portal_url + "?Map/layerids=" + layerList.substring(0, layerList.length - 1) + "\">Im Vorschauportal betrachten</a>";

                            preview_text += previewURL;
                        }
                        else {
                            preview_text;
                        }
                    }
                );
            }

            await db_udpm.exec.updateService(params);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Schnittstellendetails (ID: ${params.id}) aktualisiert`;
            params.entity = "service";

            await db_udpm.exec.addChangeLog(params);

            if (config.mail && params.qa && !old_qa) {
                await mailer.onQAService(params.title, params.last_edited_by, params.responsible_party_mail, params.mail_from);
            }

            // temporarily deactivate this function (send an e-mail when a service is put into production).
            // if (config.mail && params.status === "prod" && old_status !== "prod") {
            //     await mailer.onProdService(params.id, params.title, params.type, preview_text, params.mail_from);
            // }

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error("services.service - updateService:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async deleteService (params) {
        if (params.delete_service) {
            const me = this;
            const foldersToDelete = [];

            if (params.status_dev && !params.disable_config_management && !params.external) {
                foldersToDelete.push(path.join(process.env.CONFIG_OUT_PATH, "/configs/dev/" + params.software + "/" + params.id));
            }

            if (params.status_stage && !params.disable_config_management && !params.external) {
                foldersToDelete.push(path.join(process.env.CONFIG_OUT_PATH, "/configs/stage/" + params.software + "/" + params.id));
            }

            if (params.status_prod && !params.external && !params.disable_config_management) {
                foldersToDelete.push(path.join(process.env.CONFIG_OUT_PATH, "/configs/prod/" + params.software + "/" + params.id));
            }

            try {
                const result = await db_udpm.exec.deleteService(params);

                params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
                params.change_message = `Schnittstelle (ID: ${params.id}) gelöscht`;
                params.entity = "service";

                await db_udpm.exec.addChangeLog(params);

                if (result.error) {
                    return {status: "error", message: result.error, code: result.code};
                }
                else if (!params.external && !params.disable_config_management) {
                    await me._deleteServiceConfigs(foldersToDelete, params);
                    return {status: "success"};
                }
                else {
                    return {status: "success"};
                }
            }
            catch (error) {
                Logger.error(error);

                return {status: "error", message: error.message, code: error.code};
            }
        }
        else {
            try {
                await db_udpm.exec.deleteServiceLink(params);

                params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
                params.change_message = `Schnittstellenverknüpfung (ID: ${params.id}) gelöscht`;
                params.entity = "service";

                await db_udpm.exec.addChangeLog(params);

                return {status: "success"};
            }
            catch (error) {
                Logger.error(error);

                return {status: "error", message: error.message};
            }
        }
    },

    async _deleteServiceConfigs (foldersToDelete, service_params) {
        const config = require("../config.js").getConfig();

        try {
            for (const folder of foldersToDelete) {
                await fs.rm(folder, {recursive: true, force: true});
            }

            if (config.modules.git) {
                const commit_message = "delete service " + service_params.title + " [" + service_params.id + "]";

                await gitUtil.addCommit(commit_message);

                dataCache.incrementCommitCount();
                if (service_params.status_dev) {
                    dataCache.setChange("dev", true);
                }
                if (service_params.status_stage) {
                    dataCache.setChange("stage", true);
                }
                if (service_params.status_prod) {
                    dataCache.setChange("prod", true);
                }
            }
        }
        catch (error) {
            Logger.error("services.service - _deleteServiceConfigs");
            Logger.error(error);
        }
    },

    async getDistinctKeywords () {
        try {
            const results = await db_udpm.exec.getDistinctServiceKeywords();

            const keywords = [];

            for (let i = 0; i < results.length; i++) {
                keywords.push({text: results[i]});
            }

            return results;
        }
        catch (error) {
            Logger.error("services.service - getDistinctKeywords:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async updateAllowedGroups (params) {
        try {
            await db_udpm.exec.updateServiceAllowedGroups(params);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Liste der berechtigten Gruppen an der Schnittstelle (ID: ${params.service_id}) aktualisiert`;
            params.entity = "service";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error("services.service - updateAllowedGroups:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async addServiceLink (params) {
        try {
            const db_resp = await db_udpm.exec.addServiceLink(params);

            if (db_resp.error) {
                return {error: true, message: db_resp.error};
            }
            else {
                params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
                params.change_message = `Neue Verknüpfung auf Schnittstelle (ID: ${params.service_id}) eingetragen`;
                params.entity = "service";

                await db_udpm.exec.addChangeLog(params);

                return {status: "success"};
            }
        }
        catch (error) {
            Logger.error("services.service - addServiceLink:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getServiceLinks (params) {
        try {
            const result = await db_udpm.exec.getServiceLinks(params);

            return result;
        }
        catch (error) {
            Logger.error("services.service - getServiceLinks:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async updateWmsThemeConfig (params) {
        try {
            const result = await db_udpm.exec.updateWmsThemeConfig(params);

            if (result.error) {
                return {error: true, message: result.error};
            }
            else {
                params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
                params.change_message = `Themenkonfiguration für Schnittstelle (ID: ${params.service_id}) aktualisiert`;
                params.entity = "service";

                await db_udpm.exec.addChangeLog(params);

                return {status: "success"};
            }
        }
        catch (error) {
            Logger.error("services.service - updateWmsThemeConfig:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    /**
     * Generates the configuration files for given services
     * First step - generate data object for the config generation process
     * Second step - generate the configuration files and commit them to git
     * @param {Object} params - service id, service title
     * @returns {Object} - status: success
     */
    async generateConfig (params) {
        const me = this;
        const result = {newStatus: "in Bearbeitung", services: {}};
        const queryParams = {status: "in Bearbeitung", dest: "dev"};

        for (const service_id in params) {
            const service = params[service_id];
            let updateServiceParams = true;

            service.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            service.entity = "service";

            queryParams.dataset_id = service.dataset_id;

            if (service.disable_config_module) {
                result.services[service_id] = {status: "skipped", disable_config_module: true};

                service.change_message = `Schnittstellenkonfiguration (ID: ${service.service_id}) nach dev verschoben`;
            }
            else if (service.disable_config_management) {
                result.services[service_id] = {status: "skipped", disable_config_management: true};

                service.change_message = `Schnittstellenobjekt (ID: ${service.service_id}) nach dev verschoben`;
            }
            else {
                try {
                    const configDataFromDb = await db_udpm.exec.getConfigDataFromDb(service);
                    const db_data = configDataFromDb.data;
                    const db_connection = await managementService.getDbConnectionById(db_data.dataset.db_connection_r_id, true);

                    await db_ext.exec.dbConnect(db_connection);

                    for (const collection of db_data.collections) {
                        if (collection.crs) {
                            collection.crs = await db_ext.exec.getCrs(collection);
                        }
                    }

                    const configsEdited = await db_udpm.exec.getServiceConfigsEdited(service);
                    const db_data_types = await me._getDataTypesFromDb(db_data);

                    service.configDataObject = configDataGen(db_data, db_data_types);
                    service.configDataObject.configsEdited = configsEdited;
                    service.change_message = `Neue Schnittstellenkonfiguration (ID: ${service.service_id}) erzeugt`;

                    if (service.configDataObject.dataset.store_type !== "Raster") {
                        configDataValidator.validate(service.configDataObject);
                    }

                    const resultService = await serviceConfigManagement.generateServiceConfig(service);

                    if (resultService.error) {
                        updateServiceParams = false;
                    }

                    if (configsEdited.length > 0) {
                        resultService.configsEdited = true;
                    }
                    else {
                        resultService.configsEdited = false;
                    }

                    result.services[service_id] = resultService;
                }
                catch (error) {
                    Logger.error("services.service - generateConfig:");
                    Logger.error(error);
                    result.services[service_id] = {status: "error", message: error.message};
                }
            }

            if (updateServiceParams) {
                try {
                    await db_udpm.exec.updateServiceStatus({status_dev: true, status_stage: false, status_prod: service.status_prod, folder: service.targetWorkspace, url_int: service.newUrlInt, url_ext: service.newUrlExt, id: service_id, server: service.server});
                    await db_udpm.exec.addChangeLog(service);
                }
                catch (error) {
                    Logger.error("services.service - generateConfig:");
                    Logger.error(error);
                    result.services[service_id] = {status: "error", message: error.message};
                }
            }
        }

        const newStatus = await this.evaluateAndSetNewDatasetStatus(queryParams);

        result.newStatus = newStatus;

        return result;
    },

    /**
     * Deploys the configuration files for given services to the specified destination (dev, stage, prod).
     * First step - update the service status in the database.
     * Second step - add a change log entry to the database.
     * Third step - update the layer JSON file if the destination is prod.
     * Fourth step - update the services production log if the destination is prod.
     * Fifth step - evaluate and set the new status of the dataset.
     * @param {Object} params - An object containing the service id, service title, and destination.
     * @returns {Object} - An object containing the new status of the dataset and the status of each service.
     */
    async deployServiceConfig (params) {
        const result = {newStatus: "in Bearbeitung", services: {}};
        const queryParams = {status: "in Bearbeitung", dest: "dev"};

        for (const service_id in params) {
            const service = params[service_id];
            let updateServiceParams = true;

            queryParams.dest = service.dest;

            service.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            service.entity = "service";

            queryParams.dataset_id = service.dataset_id;

            if (service.disable_config_management) {
                service.change_message = `Schnittstellenobjekt (ID: ${service.service_id}) nach ${service.dest} verschoben`;

                result.services[service_id] = {status: "success"};
            }
            else if (service.external) {
                updateServiceParams = false;
                if (service.dest === "dev") {
                    service.change_message = `Externes Schnittstellenobjekt (ID: ${service.service_id}) in Bearbeitung`;
                    queryParams.status = "veröffentlicht";
                }
                else {
                    service.change_message = `Externes Schnittstellenobjekt (ID: ${service.service_id}) bearbeitet`;
                }

                result.services[service_id] = {status: "success"};
            }
            else {
                try {
                    service.change_message = `Schnittstellenkonfiguration (ID: ${service.service_id}) nach ${service.dest} verschoben`;
                    service.servicesRestrictionLevel = await db_udpm.exec.getServicesRestrictionLevel();

                    const resultService = await serviceConfigManagement.deployServiceConfig(service);

                    if (resultService.error) {
                        updateServiceParams = false;
                    }

                    result.services[service_id] = resultService;
                }
                catch (error) {
                    Logger.error("services.service - deployServiceConfig:");
                    Logger.error(error);
                    result.services[service_id] = {status: "error", message: error.message};
                }
            }

            if (updateServiceParams) {
                try {
                    if (service.dest === "dev") {
                        const values = {status_dev: true, status_stage: false, status_prod: service.status_prod, folder: service.targetWorkspace, url_int: service.newUrlInt, url_ext: service.newUrlExt, id: service_id, server: service.server};

                        await db_udpm.exec.updateServiceStatus(values);
                    }
                    else if (service.dest === "stage") {
                        const values = {status_dev: false, status_stage: true, status_prod: service.status_prod, folder: service.targetWorkspace, url_int: service.newUrlInt, url_ext: service.newUrlExt, id: service_id, server: service.server};

                        await db_udpm.exec.updateServiceStatus(values);
                    }
                    else if (service.dest === "prod") {
                        const values = {status_dev: false, status_stage: true, status_prod: true, folder_prod: service.targetWorkspace, url_int_prod: service.newUrlInt, url_ext_prod: service.newUrlExt, id: service_id, server_prod: service.server};

                        await db_udpm.exec.updateServiceStatusProd(values);
                        await layersService.updateLayerJson({service_id: service_id}, true);
                        await db_udpm.exec.updateDatasetsVersionLog(service);
                    }

                    await db_udpm.exec.addChangeLog(service);
                }
                catch (error) {
                    Logger.error("services.service - deployServiceConfig:");
                    Logger.error(error);
                    result.services[service_id] = {status: "error", message: error.message};
                }
            }
        }

        const newStatus = await this.evaluateAndSetNewDatasetStatus(queryParams);

        result.newStatus = newStatus;

        return result;
    },

    /**
     * Undeploys the configuration files for given services from the prod.
     * First step - update the service status in the database.
     * Second step - add a change log entry to the database.
     * Third step - delete the prod layer JSON file if the destination is prod.
     * Fourth step - evaluate and set the new status of the dataset.
     * @param {Object} params - An object containing the service id, service title, and destination.
     * @returns {Object} - An object containing the new status of the dataset and the status of each service.
     */
    async unDeployServiceConfig (params) {
        const result = {newStatus: "in Bearbeitung", services: {}};
        const queryParams = {status: "in Bearbeitung", dest: "stage"};

        for (const service_id in params) {
            const service = params[service_id];

            queryParams.dataset_id = service.dataset_id;

            try {
                service.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
                service.change_message = `Schnittstellenkonfiguration (ID: ${service.service_id}) aus prod gelöscht`;
                service.entity = "service";
                service.servicesRestrictionLevel = await db_udpm.exec.getServicesRestrictionLevel();

                const resultService = await serviceConfigManagement.unDeployServiceConfig(service);

                if (!resultService.error) {
                    const values = {status_dev: service.status_dev, status_stage: service.status_stage, status_prod: false, folder_prod: "", url_int_prod: "", url_ext_prod: "", id: service_id, server_prod: ""};

                    await db_udpm.exec.updateServiceStatusProd(values);
                    await db_udpm.exec.addChangeLog(service);
                    await db_udpm.exec.deleteLayerJsonProd({service_id: service_id});
                }

                result.services[service_id] = resultService;
            }
            catch (error) {
                Logger.error("services.service - unDeployServiceConfig:");
                Logger.error(error);
                result.services[service_id] = {status: "error", message: error.message};
            }
        }

        const newStatus = await this.evaluateAndSetNewDatasetStatus(queryParams);

        result.newStatus = newStatus;

        return result;
    },

    /**
     * Evaluates the status of a dataset based on the status of its services and updates the dataset status in the database.
     * It iterates through the services of the dataset, counts the number of services in each status (development, production, staging),
     * and sets the new status of the dataset accordingly. If an error occurs during the process, it logs the error and returns null.
     * @param {Object} queryParams - The query parameters for getting services dataset.
     * @param {string} queryParams.status - The current status of the dataset.
     * @param {number} queryParams.dataset_id - The ID of the dataset.
     * @returns {string|null} - The new status of the dataset or null if an error occurred.
     */
    async evaluateAndSetNewDatasetStatus (queryParams) {
        try {
            const servicesDataset = await db_udpm.exec.getServicesDataset(queryParams);
            let newStatus = queryParams.status;
            let numDev = 0;
            let numStage = 0;
            let numProd = 0;
            let numExternal = 0;

            for (const service of servicesDataset) {
                if (service.status_dev) {
                    numDev++;
                }
                if (service.status_prod) {
                    numProd++;
                }
                if (service.status_stage) {
                    numStage++;
                }
                if (service.external) {
                    numExternal++;
                }
            }

            if (numProd === servicesDataset.length && numDev === 0 && numExternal < servicesDataset.length) {
                if (queryParams.dest === "stage") {
                    queryParams.status = "veröffentlicht - vorveröffentlicht";
                    newStatus = "veröffentlicht - vorveröffentlicht";
                }
                else {
                    queryParams.status = "veröffentlicht";
                    newStatus = "veröffentlicht";
                }
            }
            else if (numProd > 0 && numDev > 0) {
                queryParams.status = "veröffentlicht - in Bearbeitung";
                newStatus = "veröffentlicht - in Bearbeitung";
            }
            else if (numStage === servicesDataset.length && numProd === 0) {
                queryParams.status = "vorveröffentlicht";
                newStatus = "vorveröffentlicht";
            }
            else if (numExternal === servicesDataset.length && queryParams.status === "veröffentlicht") {
                queryParams.status = "veröffentlicht - in Bearbeitung";
                newStatus = "veröffentlicht - in Bearbeitung";
            }
            else if (numExternal === servicesDataset.length && queryParams.status === "in Bearbeitung") {
                queryParams.status = "veröffentlicht";
                newStatus = "veröffentlicht";
            }

            await db_udpm.exec.updateDatasetStatus(queryParams);

            return newStatus;
        }
        catch (error) {
            Logger.error("services.service - deployServiceConfig:");
            Logger.error(error);

            return null;
        }
    },

    async overwriteServiceConfig (params) {
        const config = require("../config.js").getConfig();

        try {
            await fs.writeFile(params.uri, params.config);

            let workspaceEnv = "dev";
            let configEnv = "dev";

            // software configs with seperateDevWorkspace=false are directly copied to stage workspace
            if (!config.software[params.software].seperateDevWorkspace && params.status_dev === true) {
                workspaceEnv = "stage";
            }

            if (params.targetEnv) {
                workspaceEnv = params.targetEnv;
                configEnv = params.targetEnv;
            }
            if (params.targetEnv === "dev" && !config.software[params.software].seperateDevWorkspace) {
                workspaceEnv = "stage";
            }

            let basePath = path.join(process.env.CONFIG_OUT_PATH, "/configs/" + configEnv + "/" + params.software + "/" + params.id);
            let destWS = path.join(process.env.CONFIG_OUT_PATH, "/workspaces/" + workspaceEnv + "/" + params.software);

            if (params.targetWorkspace) {
                basePath = path.join(basePath, "/" + params.folder);
                destWS = path.join(destWS, "/" + params.folder);
            }

            const filePath = path.normalize(params.uri);

            await fs.cp(filePath, filePath.replace(path.normalize(basePath), path.normalize(destWS)));

            let status = {status: "success"};

            if (config.modules.git) {
                const commit_message = "overwrite config in " + configEnv + " - " + params.title + " [" + params.id + "]";

                status = await gitUtil.addCommit(commit_message);

                dataCache.incrementCommitCount();
                dataCache.setChange(configEnv, true);
            }

            if (status.status === "success") {
                let uri = "";

                if (params.folder) {
                    uri = params.uri.replace(basePath + "\\" + params.folder, "").replace(/\\/g, "/").substring(1);
                }
                else {
                    uri = params.uri.replace(basePath, "").replace(/\\/g, "/").substring(1);
                }

                const entry = await db_udpm.exec.getServiceConfigEdited({
                    service_id: params.id,
                    uri: uri
                });

                if (entry.length === 0) {
                    await db_udpm.exec.addServiceConfigEdited({
                        service_id: params.id,
                        uri: uri
                    });
                }

                params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
                params.change_message = `Schnittstellenkonfiguration (ID: ${params.id}) ${uri} manuell überschrieben`;
                params.entity = "service";

                await db_udpm.exec.addChangeLog(params);
            }

            return status;
        }
        catch (error) {
            Logger.error("services.service - overwriteServiceConfig");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getServiceConfigsEdited (params) {
        try {
            const results = await db_udpm.exec.getServiceConfigsEdited(params);

            return results;
        }
        catch (error) {
            Logger.error("services.service - getServiceConfigsEdited:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async deleteServiceConfigEdited (params) {
        try {
            const results = await db_udpm.exec.deleteServiceConfigEdited(params);

            return results;
        }
        catch (error) {
            Logger.error("services.service - deleteServiceConfigEdited:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async createConfigFile (params) {
        const basePath = params.path ? `${process.env.CONFIG_OUT_PATH}/configs/dev/${params.software}/${params.service_id}${params.path}` : `${process.env.CONFIG_OUT_PATH}/configs/dev/${params.software}/${params.service_id}`;
        const fpath = `${basePath}/${params.filename}`;

        try {
            await fs.mkdir(basePath, {recursive: true}, (err) => {
                if (err) {
                    throw err;
                }
            });
            await fs.writeFile(fpath, "");

            return {status: "success"};
        }
        catch (error) {
            Logger.error("services.service - createConfigFile:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async _getDataTypesFromDb (params) {
        // get data types and geometry types for collection attributes
        const db_data_types = {};

        if (params.dataset.db_connection_r_id) {
            const db_connection = await managementService.getDbConnectionById(params.dataset.db_connection_r_id, true);

            await db_ext.exec.dbConnect(db_connection);

            for (const collection of params.collections) {
                if (!collection.group_object) {
                    const attr_data_types = await db_ext.exec.getAttributesForMapping(collection.db_schema, collection.db_table);

                    db_data_types[collection.id] = attr_data_types;
                }
                else {
                    db_data_types[collection.id] = [];
                }
            }
        }

        return db_data_types;
    }
});
