const Logger = require("../utils/logger.util");
const cryptor = require("../utils/cryptor.util");
const moment = require("moment");

module.exports = ({db_ext, db_udpm}) => ({
    /**
     * search for datasets in external UDP-Manager Instance
     * @param {Object} params - Object containing of search string and db connection of ext instance 
     * @returns {Object} - search results
     */
    async searchDatasetInExtInstance (params) {
        try {
            if (params.connectionId) {
                const connection = await db_udpm.exec.getExtInstanceById({id: params.connectionId});

                if (connection.password) {
                    if (connection.password.indexOf("__") > 0) {
                        connection.password = cryptor.decrypt(connection.password);
                    }
                }

                await db_ext.exec.dbConnect(connection);

                const results = await db_ext.exec.searchDataset(params.searchStr);

                return results;
            }
            else {
                return {status: "error", message: "no valid DB connection parameters"};
            }
        }
        catch (error) {
            Logger.error("import.service - searchDatasetInExtInstance:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    /**
     * Import dataset by ID from external UDP-Manager Instance
     * @param {Object} params - ID of dataset to import and editor metadata
     * @param {boolean} update - true if existing dataset should be updated, false when import as new
     * @returns {Object} - status info, contains dataset ID if import process was successfull
     */
    async importDataset (params, update) {
        const config = require("../config.js").getConfig();

        try {
            const connection = await db_udpm.exec.getExtInstanceById({id: params.connectionId});

            if (connection.password) {
                if (connection.password.indexOf("__") > 0) {
                    connection.password = cryptor.decrypt(connection.password);
                }
            }

            await db_ext.exec.dbConnect(connection);
            // try get all dataset (incl. collections, services, layer, comments, keywords) parameters from external instance
            const result = await db_ext.exec.getDatasetData(params.id);

            if (!result.error) {
                let layer_count = 0;
                let service_count = 0;
                let collection_count = 0;
                let services_dev = [];

                services_dev = result.services.filter((service_obj) => service_obj.status_dev === true);

                if (services_dev.length > 0) {
                    Logger.error("import failed - services with status dev found");

                    return {status: "error", message: "dev_services_found"};
                }
                else {
                    // check if db_connection_r_id from imported dataset exists
                    if (result.db_connection_r_id) {
                        try {
                            const db_connection_r_obj = await db_udpm.exec.getDbConnectionByName({name: result.db_connection_r_obj.name});

                            result.db_connection_r_id = db_connection_r_obj.id;
                        }
                        catch (err) {
                            result.db_connection_r_insert = true;
                        }
                    }

                    // check if db_connection_w_id from imported dataset exists
                    if (result.db_connection_w_id) {
                        try {
                            const db_connection_w_obj = await db_udpm.exec.getDbConnectionByName({name: result.db_connection_w_obj.name});

                            result.db_connection_w_id = db_connection_w_obj.id;
                        }
                        catch (err) {
                            result.db_connection_w_insert = true;
                        }
                    }

                    // check if metadata_catalog_id from imported dataset exists
                    if (result.metadata_catalog_id) {
                        const md_catalogs = await db_udpm.exec.getMetadataCatalog({metadata_catalog_id: result.metadata_catalog_id});

                        if (md_catalogs.length === 0) {
                            result.metadata_catalog_insert = true;
                        }
                    }

                    result.last_edit_date = params.last_edit_date;
                    result.last_edited_by = params.last_edited_by;

                    // if update = true, delete dataset and all depending resources first
                    if (update) {
                        try {
                            await db_udpm.exec.deleteDatasetComplete(params);
                        }
                        catch (error) {
                            if (error.code === "23503") {
                                try {
                                    await db_udpm.exec.deleteDatasetNoServices(params);
                                    await db_udpm.exec.deleteNotLinkedServices();
                                }
                                catch (errorNoServices) {
                                    Logger.error(errorNoServices);
                                }
                            }
                        }
                    }
                    else {
                        const layer_ids = [];
                        const service_ids = [];
                        const collection_ids = [];

                        // check if layer_ids, collection_ids and service_ids from imported dataset already exists
                        if (result.layers.length > 0) {
                            for (let i = 0; i < result.layers.length; i++) {
                                layer_ids.push(result.layers[i].id);
                            }

                            const layer_check_result = await db_udpm.exec.checkIfLayerIdExists({id_list: layer_ids.toString()});

                            layer_count = parseInt(layer_check_result[0].count);
                        }

                        if (result.collections.length > 0) {
                            for (let i = 0; i < result.collections.length; i++) {
                                collection_ids.push(result.collections[i].id);
                            }

                            const collection_check_result = await db_udpm.exec.checkIfCollectionIdExists({id_list: collection_ids.toString()});

                            collection_count = parseInt(collection_check_result[0].count);
                        }

                        if (result.services.length > 0) {
                            for (let i = 0; i < result.services.length; i++) {
                                if (!result.services[i].ignore) {
                                    service_ids.push(result.services[i].id);
                                }
                            }

                            const service_check_result = await db_udpm.exec.checkIfServiceIdExists({id_list: service_ids.toString()});

                            service_count = parseInt(service_check_result[0].count);
                        }
                    }

                    let allExternal = true;

                    for (let i = 0; i < result.services.length; i++) {
                        if (!result.services[i].external) {
                            allExternal = false;
                        }
                    }

                    const git_clean = require("../tasks/git_clean.task")({db_udpm});

                    if (params.createNewIds && !update) {
                        const import_result = await db_udpm.exec.importDataset(result, true);

                        if (import_result) {
                            if (config.modules.git && !allExternal) {
                                await git_clean.execute();
                            }

                            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
                            params.change_message = `Datensatz mit neuer ID (ID: ${import_result}) importiert`;
                            params.dataset_id = import_result;
                            params.entity = "dataset";

                            await db_udpm.exec.addChangeLog(params);

                            return {status: "success", id: import_result};
                        }
                        else {
                            return {status: "error", message: import_result};
                        }
                    }
                    else if ((layer_count === 0 && collection_count === 0 && service_count === 0) || update) {
                        const import_result = await db_udpm.exec.importDataset(result, false);

                        if (import_result) {
                            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
                            params.change_message = `Datensatz (ID: ${import_result}) importiert`;
                            params.dataset_id = import_result;
                            params.entity = "dataset";

                            await db_udpm.exec.addChangeLog(params);

                            await db_udpm.exec.setLayerIdSeq();
                            await db_udpm.exec.setServiceIdSeq();
                            await db_udpm.exec.setCollectionIdSeq();
                            await db_udpm.exec.setDbConnectionsIdSeq();
                            await db_udpm.exec.setMetadataCatalogsIdSeq();
                            await db_udpm.exec.setEtlProcessesIdSeq();

                            if (config.modules.git && !allExternal) {
                                await git_clean.execute();
                            }

                            return {status: "success", id: import_result};
                        }
                        else {
                            return {status: "error", message: import_result};
                        }
                    }
                    else {
                        return {status: "error", message: "ids_existing"};
                    }
                }
            }
            else {
                return {status: "error", message: "could_not_get_dataset_data"};
            }
        }
        catch (error) {
            Logger.error("import.service - importDataset:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    }
});
