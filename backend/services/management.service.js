const Logger = require("../utils/logger.util");
const config = require("../config.js");
const schemaValidator = require("../utils/schemaValidator.util");
const configValidator = require("../utils/configValidator.util");
const cryptor = require("../utils/cryptor.util");
const configGenerator = require("../utils/configGenerator.util");

module.exports = ({db_udpm}) => ({
    async getMetadataCatalogs () {
        try {
            const results = await db_udpm.exec.getMetadataCatalogs();

            return results;
        }
        catch (error) {
            Logger.error("management.service - getMetadataCatalogs:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async addMetadataCatalog (params) {
        if (schemaValidator.validate("addMetadataCatalog", params)) {
            try {
                if (params.use_in_internet_json) {
                    await db_udpm.exec.setAllMetadataCatalogsExtFalse();
                }

                const result = await db_udpm.exec.addMetadataCatalog(params);

                return result;
            }
            catch (error) {
                Logger.error("management.service - addMetadataCatalog:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "provided data not valid"};
        }
    },

    async updateMetadataCatalog (params) {
        if (schemaValidator.validate("updateMetadataCatalog", params)) {
            try {
                if (params.use_in_internet_json) {
                    await db_udpm.exec.setAllMetadataCatalogsExtFalse();
                }

                const result = await db_udpm.exec.updateMetadataCatalog(params);

                return result;
            }
            catch (error) {
                Logger.error("management.service - updateMetadataCatalog:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "provided data not valid"};
        }
    },

    async deleteMetadataCatalog (params) {
        if (params.id) {
            try {
                const result = await db_udpm.exec.deleteMetadataCatalog(params);

                return result;
            }
            catch (error) {
                Logger.error("management.service - deleteMetadataCatalog:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "id not provided"};
        }

    },

    async getExtInstances () {
        try {
            const results = await db_udpm.exec.getExtInstances();

            results.forEach(element => {
                if (element.password) {
                    if (element.password.indexOf("__") > 0) {
                        element.password = cryptor.decrypt(element.password);
                    }
                }
            });

            return results;
        }
        catch (error) {
            Logger.error("management.service - getExtInstances:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async addExtInstance (params) {
        if (schemaValidator.validate("addExtInstance", params)) {
            try {
                params.password = cryptor.encrypt(params.password);

                const result = await db_udpm.exec.addExtInstance(params);

                return result;
            }
            catch (error) {
                Logger.error("management.service - addExtInstance:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "provided data not valid"};
        }
    },

    async updateExtInstance (params) {
        if (schemaValidator.validate("updateExtInstance", params)) {
            try {
                params.password = cryptor.encrypt(params.password);

                const result = await db_udpm.exec.updateExtInstance(params);

                return result;
            }
            catch (error) {
                Logger.error("management.service - updateExtInstance:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "provided data not valid"};
        }
    },

    async deleteExtInstance (params) {
        if (params.id) {
            try {
                const result = await db_udpm.exec.deleteExtInstance(params);

                return result;
            }
            catch (error) {
                Logger.error("management.service - deleteExtInstance:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "id not provided"};
        }

    },

    async getDbConnections () {
        try {
            const results = await db_udpm.exec.getDbConnections();

            results.forEach(element => {
                element.password = "password_not_transmitted";
            });

            return results;
        }
        catch (error) {
            Logger.error("management.service - getDbConnections:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getDbConnectionById (id, providePassword) {
        if (!id) {
            return {error: true, message: "no 'id' parameter provided"};
        }
        else {
            try {
                const result = await db_udpm.exec.getDbConnectionById({id: id});

                if (result.password && providePassword) {
                    if (result.password.indexOf("__") > 0) {
                        result.password = cryptor.decrypt(result.password);
                    }
                }
                else {
                    delete result.password;
                }

                return result;
            }
            catch (error) {
                Logger.error("management.service - getDbConnectionById:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
    },

    async addDbConnection (params) {
        if (schemaValidator.validate("addDbConnection", params)) {
            try {
                params.password = cryptor.encrypt(params.password);

                const result = await db_udpm.exec.addDbConnection(params);

                return result;
            }
            catch (error) {
                Logger.error("management.service - addDbConnection:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "provided data not valid"};
        }
    },

    async updateDbConnection (params) {
        if (schemaValidator.validate("updateDbConnection", params)) {
            try {
                if (params.password === "password_not_transmitted") {
                    return await db_udpm.exec.updateDbConnectionWoPassword(params);
                }
                else {
                    params.password = cryptor.encrypt(params.password);

                    return await db_udpm.exec.updateDbConnection(params);
                }
            }
            catch (error) {
                Logger.error("management.service - updateDbConnection:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "provided data not valid"};
        }
    },

    async deleteDbConnection (params) {
        if (params.id) {
            try {
                const linked_datasets = await db_udpm.exec.getDatasetsByDbConnectionId(params);

                if (linked_datasets.length > 0) {
                    return {status: "error", message: "linked_datasets"};
                }
                else {
                    const result = await db_udpm.exec.deleteDbConnection(params);

                    return result;
                }
            }
            catch (error) {
                Logger.error("management.service - deleteDbConnection:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "id not provided"};
        }

    },

    async getSourceDbConnections () {
        try {
            const results = await db_udpm.exec.getSourceDbConnections();

            results.forEach(element => {
                element.password = "password_not_transmitted";
            });

            return results;
        }
        catch (error) {
            Logger.error("management.service - getSourceDbConnections:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getSourceDbConnectionById (id, providePassword) {
        if (!id) {
            return {error: true, message: "no 'id' parameter provided"};
        }
        else {
            try {
                const result = await db_udpm.exec.getSourceDbConnectionById({id: id});

                if (result.password && providePassword) {
                    if (result.password.indexOf("__") > 0) {
                        result.password = cryptor.decrypt(result.password);
                    }
                }
                else {
                    delete result.password;
                }

                return result;
            }
            catch (error) {
                Logger.error("management.service - getSourceDbConnectionById:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
    },

    async addSourceDbConnection (params) {
        try {
            params.password = cryptor.encrypt(params.password);

            const result = await db_udpm.exec.addSourceDbConnection(params);

            return result;
        }
        catch (error) {
            Logger.error("management.service - addSourceDbConnection:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async updateSourceDbConnection (params) {
        try {
            if (params.password === "password_not_transmitted") {
                return await db_udpm.exec.updateSourceDbConnectionWoPassword(params);
            }
            else {
                params.password = cryptor.encrypt(params.password);

                return await db_udpm.exec.updateSourceDbConnection(params);
            }
        }
        catch (error) {
            Logger.error("management.service - updateSourceDbConnection:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async deleteSourceDbConnection (params) {
        if (params.id) {
            try {
                const linked_collections = await db_udpm.exec.getCollectionsBySourceDbConnectionId(params);

                if (linked_collections.length > 0) {
                    return {status: "error", message: "linked_collections"};
                }
                else {
                    const result = await db_udpm.exec.deleteSourceDbConnection(params);

                    return result;
                }
            }
            catch (error) {
                Logger.error("management.service - deleteSourceDbConnection:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "id not provided"};
        }

    },

    async getElasticConfig () {
        try {
            const results = await db_udpm.exec.getElasticConfig();

            return results;
        }
        catch (error) {
            Logger.error("management.service - getElasticConfig:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async updateElasticConfig (params) {
        try {
            await db_udpm.exec.updateElasticConfig(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error("management.service - updateElasticConfig:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async getAppConfig () {
        try {
            const results = await db_udpm.exec.getAppConfig();

            return [results];
        }
        catch (error) {
            Logger.error("management.service - getAppConfig:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async updateAppConfig (params) {
        try {
            configValidator.validate(params);

            configGenerator.prepare(params);

            params.server = JSON.stringify(params.server);
            params.capabilities_metadata = JSON.stringify(params.capabilities_metadata);
            params.test_bbox = JSON.stringify(params.test_bbox);
            params.service_types = JSON.stringify(params.service_types);
            params.client_config = JSON.stringify(params.client_config);
            params.ldap = JSON.stringify(params.ldap);
            params.jira = JSON.stringify(params.jira);
            params.mail = JSON.stringify(params.mail);
            params.git = JSON.stringify(params.git);
            params.modules = JSON.stringify(params.modules);
            params.software = JSON.stringify(params.software);
            params.available_projections = JSON.stringify(params.available_projections);
            params.layer_attributes_excludes = JSON.stringify(params.layer_attributes_excludes);

            await db_udpm.exec.updateAppConfig(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error("management.service - updateAppConfig:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getFmeServerConnections () {
        try {
            const results = await db_udpm.exec.getFmeServerConnections();

            results.forEach(element => {
                element.password = "password_not_transmitted";
            });

            return results;
        }
        catch (error) {
            Logger.error("management.service - getFmeServerConnections:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async addFmeServerConnection (params) {
        try {
            params.password = cryptor.encrypt(params.password);

            const result = await db_udpm.exec.addFmeServerConnection(params);

            const fmeServerConnections = await db_udpm.exec.getFmeServerConnections();

            fmeServerConnections.forEach(element => {
                if (element.password) {
                    if (element.password.indexOf("__") > 0) {
                        element.password = cryptor.decrypt(element.password);
                    }
                }
            });

            config.setConfig("database.fmeServer", fmeServerConnections);

            return result;
        }
        catch (error) {
            Logger.error("management.service - addFmeServerConnection:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async updateFmeServerConnection (params) {
        try {
            const result = {};

            if (params.password === "password_not_transmitted") {
                result = await db_udpm.exec.updateFmeServerConnectionWoPassword(params);
            }
            else {
                params.password = cryptor.encrypt(params.password);

                result = await db_udpm.exec.updateFmeServerConnection(params);
            }

            const fmeServerConnections = await db_udpm.exec.getFmeServerConnections();

            fmeServerConnections.forEach(element => {
                if (element.password) {
                    if (element.password.indexOf("__") > 0) {
                        element.password = cryptor.decrypt(element.password);
                    }
                }
            });

            config.setConfig("database.fmeServer", fmeServerConnections);

            return result;
        }
        catch (error) {
            Logger.error("management.service - updateFmeServerConnection:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async deleteFmeServerConnection (params) {
        if (params.id) {
            try {
                const result = await db_udpm.exec.deleteFmeServerConnection(params);

                return result;
            }
            catch (error) {
                Logger.error("management.service - deleteFmeServerConnection:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "id not provided"};
        }

    },

    async getWebDavConnections () {
        try {
            const results = await db_udpm.exec.getWebDavConnections();

            results.forEach(element => {
                element.password = "password_not_transmitted";
            });

            return results;
        }
        catch (error) {
            Logger.error("management.service - getWebDavConnections:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async addWebDavConnection (params) {
        try {
            params.password = cryptor.encrypt(params.password);

            const result = await db_udpm.exec.addWebDavConnection(params);

            const webDavConnections = await db_udpm.exec.getWebDavConnections();

            webDavConnections.forEach(element => {
                if (element.password) {
                    if (element.password.indexOf("__") > 0) {
                        element.password = cryptor.decrypt(element.password);
                    }
                }
            });

            config.setConfig("webdav", webDavConnections);

            return result;
        }
        catch (error) {
            Logger.error("management.service - addWebDavConnections:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async updateWebDavConnection (params) {
        try {
            let result = {};

            if (params.password === "password_not_transmitted") {
                result = await db_udpm.exec.updateWebDavConnectionWoPassword(params);
            }
            else {
                params.password = cryptor.encrypt(params.password);

                result = await db_udpm.exec.updateWebDavConnection(params);
            }

            const webDavConnections = await db_udpm.exec.getWebDavConnections();

            webDavConnections.forEach(element => {
                if (element.password) {
                    if (element.password.indexOf("__") > 0) {
                        element.password = cryptor.decrypt(element.password);
                    }
                }
            });

            config.setConfig("webdav", webDavConnections);

            return result;
        }
        catch (error) {
            Logger.error("management.service - updateWebDavConnections:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async deleteWebDavConnection (params) {
        if (params.id) {
            try {
                const result = await db_udpm.exec.deleteWebDavConnection(params);

                return result;
            }
            catch (error) {
                Logger.error("management.service - deleteWebDavConnection:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        else {
            return {error: true, message: "id not provided"};
        }

    }
});
