const Logger = require("../utils/logger.util");
const stringUtil = require("../utils/string.util");
// const ServicesService = require("./services.service")({db_udpm, db_ext, managementService, ConfiguratorFactory});
const path = require("path");
const moment = require("moment");

module.exports = ({db_udpm}) => ({
    async getDatasets () {
        try {
            const result = await db_udpm.exec.getDatasets();

            return result;
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async getDatasetsByUser (user) {
        try {
            const result = await db_udpm.exec.getDatasetsByUser({user: user});

            return result;
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async getDatasetsByObjectGuid (objectguid) {
        try {
            const result = await db_udpm.exec.getDatasetsByObjectGuid({objectguid: objectguid});

            return result;
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async getDatasetById (id) {
        try {
            const result = await db_udpm.exec.getDatasetById({id: id});

            return result;
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async getDatasetByShortname (shortname) {
        try {
            const result = await db_udpm.exec.getDatasetByShortname({shortname: shortname});

            return result;
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async saveDataset (params) {
        params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
        params.entity = "dataset";
        params.dataset_id = params.id;

        try {
            if (params.id === 0) {
                const result = await db_udpm.exec.addDataset(stringUtil.removeTrailingWhitespacesInObject(params));

                params.change_message = `Neuer Datensatz (ID: ${result[0].id}) angelegt`;

                await db_udpm.exec.addChangeLog(params);

                return {status: "success", id: result[0].id};
            }
            else {
                await db_udpm.exec.updateDataset(stringUtil.removeTrailingWhitespacesInObject(params));

                params.change_message = `Datensatzdetails (ID: ${params.id}) aktualisiert`;

                await db_udpm.exec.addChangeLog(params);

                return {status: "success", id: params.id};
            }
        }
        catch (error) {
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async saveDatasetEditorial (params) {
        params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
        params.entity = "dataset";
        params.dataset_id = params.id;

        try {
            await db_udpm.exec.updateDatasetEditorial(stringUtil.removeTrailingWhitespacesInObject(params));

            params.change_message = `Datensatzdetails (ID: ${params.id}) aktualisiert`;

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async deleteDataset (params) {
        try {
            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Datensatz (ID: ${params.id}) gelöscht`;
            params.entity = "dataset";
            params.dataset_id = params.id;

            const ServicesService = require("./services.service")({db_udpm});
            const coupledServices = await db_udpm.exec.getDistinctServicesDataset(params);

            // first: deleting the configs from the repo
            for (const service_params of coupledServices) {
                service_params.endpoint_name = service_params.title;
                service_params.proxy_url = service_params.url_int;

                const foldersToDelete = [];

                if (service_params.status_dev) {
                    foldersToDelete.push(path.join(process.env.CONFIG_OUT_PATH, "/configs/dev/" + service_params.software + "/" + service_params.id));
                }

                if (service_params.status_stage) {
                    foldersToDelete.push(path.join(process.env.CONFIG_OUT_PATH, "/configs/stage/" + service_params.software + "/" + service_params.id));
                }

                if (service_params.status_prod) {
                    foldersToDelete.push(path.join(process.env.CONFIG_OUT_PATH, "/configs/prod/" + service_params.software + "/" + service_params.id));
                }

                await ServicesService._deleteServiceConfigs(foldersToDelete, service_params);
            }

            // second: deleting the dataset record in the database
            await db_udpm.exec.deleteDatasetComplete(params);

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", noServices: false};
        }
        catch (error) {
            if (error.code === "23503") {
                try {
                    await db_udpm.exec.deleteDatasetNoServices(params);
                    await db_udpm.exec.deleteNotLinkedServices();

                    return {status: "success", noServices: true};
                }
                catch (errorNoServices) {
                    Logger.error(errorNoServices);

                    return {status: "error", message: errorNoServices.message};
                }
            }
            else {
                Logger.error(error);

                return {status: "error", message: error.message};
            }
        }
    },

    async getDistinctKeywords () {
        try {
            const results = await db_udpm.exec.getDistinctDatasetKeywords();

            const keywords = [];

            for (let i = 0; i < results.length; i++) {
                keywords.push({text: results[i]});
            }

            return results;
        }
        catch (error) {
            Logger.error("datasets.service - getDistinctKeywords:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getDistinctFilterKeywords () {
        try {
            const results = await db_udpm.exec.getDistinctDatasetFilterKeywords();

            const keywords = [];

            for (let i = 0; i < results.length; i++) {
                keywords.push({text: results[i]});
            }

            return results;
        }
        catch (error) {
            Logger.error("datasets.service - getDistinctFilterKeywords:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async updateAllowedGroups (params) {
        try {
            await db_udpm.exec.updateDatasetAllowedGroups(params);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Liste der berechtigten Gruppen am Datensatz (ID: ${params.dataset_id}) aktualisiert`;
            params.entity = "dataset";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error("datasets.service - updateAllowedGroups:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getChangeLog (params) {
        try {
            const result = await db_udpm.exec.getChangeLog(params);

            return result;
        }
        catch (error) {
            Logger.error("datasets.service - getChangeLog:");
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async addDatasetsVersionLogEntries () {
        try {
            const datasets = await db_udpm.exec.getDatasets();

            for (const dataset of datasets) {
                if (dataset.status === "veröffentlicht" && !dataset.external) {
                    dataset.dataset_id = dataset.id;
                    dataset.last_edited_by = "UDP-Manager";

                    await db_udpm.exec.updateDatasetsVersionLog(dataset);
                }
            }

            return {status: "success"};
        }
        catch (error) {
            Logger.error("datasets.service - addDatasetsVersionLogEntries:");
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    }
});
