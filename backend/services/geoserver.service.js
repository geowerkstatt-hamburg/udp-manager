const Logger = require("../utils/logger.util");
const https_agent = require("../utils/https_agent.util");
const path = require("path");
const fs = require("fs").promises;

module.exports = ({axios}) => ({
    /**
     * Get the geoserver config from files.
     * @param {Object} params The params.
     * @returns {Object} The geoserver config.
     */
    async getGeoserverConfig (params) {
        try {
            const geoserverConfig = {
                datastore: "datastore",
                featuretype: "featuretype",
                featureTypesGeoserver: [],
                layer: "layer",
                namespace: "namespace",
                serviceId: params.name,
                sld: "sld",
                style: "style",
                type: params.type,
                workspace: "workspace"
            };
            const env = "/" + params.status;
            const base_path = path.join(process.env.CONFIG_OUT_PATH, "/configs/" + env + "/GeoServer/" + params.id + "/" + params.name);
            const datastoreFolders = (await fs.readdir(base_path)).filter(folder => !folder.endsWith(".xml") && folder !== "styles");

            for (let i = 0; i < datastoreFolders.length; i++) {
                const datastorePath = path.join(base_path, "/" + datastoreFolders[i]);
                const layerFolders = (await fs.readdir(datastorePath)).filter(folder => !folder.endsWith(".xml") && !folder.endsWith(".sld"));

                geoserverConfig.workspaceUri = path.join(base_path, "/workspace.xml");
                geoserverConfig.workspaceRaw = await fs.readFile(geoserverConfig.workspaceUri, {encoding: "utf8"});
                geoserverConfig.namespaceUri = path.join(base_path, "/namespace.xml");
                geoserverConfig.namespaceRaw = await fs.readFile(geoserverConfig.namespaceUri, {encoding: "utf8"});

                await this.getgeoServerConfigSub(geoserverConfig, base_path, datastorePath, layerFolders);
            }

            return geoserverConfig;
        }
        catch (error) {
            Logger.error("geoserver.service - getGeoserverConf:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    /**
     * Get the geoserver config from files of sub folders.
     * @param {Object} geoserverConfig The geoserver config
     * @param {String} base_path The base path.
     * @param {String} datastorePath The datastore path.
     * @param {String[]} layerFolders The layer folder names.
     * @returns {void}
     */
    async getgeoServerConfigSub (geoserverConfig, base_path, datastorePath, layerFolders) {
        for (let i = 0; i < layerFolders.length; i++) {
            const datastoreUri = path.join(datastorePath, "/datastore.xml");
            const datastoreRaw = await fs.readFile(datastoreUri, {encoding: "utf8"});
            const featuretypeUri = path.join(datastorePath, "/" + layerFolders[i] + "/featuretype.xml");
            const featuretypeRaw = await fs.readFile(featuretypeUri, {encoding: "utf8"});
            const layerUri = path.join(datastorePath, "/" + layerFolders[i] + "/layer.xml");
            const layerRaw = await fs.readFile(layerUri, {encoding: "utf8"});
            const styleXMLUri = path.join(base_path, "/styles/" + layerFolders[i] + ".xml");
            const styleXMLRaw = await fs.readFile(styleXMLUri, {encoding: "utf8"}).catch(() => {/* no style exists => do nothing! */});
            const styleSLDUri = path.join(base_path, "/styles/" + layerFolders[i] + ".sld");
            const styleSLDRaw = await fs.readFile(styleSLDUri, {encoding: "utf8"}).catch(() => {/* no style exists => do nothing! */});
            const name = layerFolders[i];
            const type = "Geoserver";

            geoserverConfig.featureTypesGeoserver.push({
                datastoreUri: datastoreUri,
                datastoreRaw: datastoreRaw,
                featuretypeUri: featuretypeUri,
                featuretypeRaw: featuretypeRaw,
                layerUri: layerUri,
                layerRaw: layerRaw,
                styleXMLUri: styleXMLUri,
                styleXMLRaw: styleXMLRaw || null,
                styleSLDUri: styleSLDUri,
                styleSLDRaw: styleSLDRaw || null,
                name: name,
                type: type
            });
        }
    },

    async restartWorkspace (params) {
        const config = require("../config.js").getConfig();
        const server = config.server.find((obj) => obj.name === params.server);
        const urls = [];
        const proxy_settings = config.proxy;
        const https_url = server.protocol === "https";
        const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;
        const basicAuth = config.software.GeoServer[params.status].softwareSpecificParameter.basicAuth;
        const webappContext = config.software.GeoServer[params.status].softwareSpecificParameter.webappContext || "geoserver";

        if (server.lb) {
            for (const lbServer of server.server) {
                urls.push(`${server.protocol}://${lbServer}/${webappContext}/rest/reload`);
            }
        }
        else {
            urls.push(`${server.protocol}://${server.name}${server.domain}/${webappContext}/rest/reload`);
        }

        const headers = {};

        if (basicAuth.username && basicAuth.password) {
            headers.Authorization = "Basic " + Buffer.from(`${basicAuth.username}:${basicAuth.password}`).toString("base64");
        }

        const promises = [];

        for (const url of urls) {
            promises.push(
                axios({
                    method: "post",
                    url: url,
                    headers: headers,
                    proxy: https_url ? false : proxy_settings,
                    httpsAgent: httpsAgent
                })
            );
        }

        return Promise.all(promises)
            .then(function () {
                return {status: "success"};
            })
            .catch(function (error) {
                Logger.error(error);
                return {status: "error", message: error.message};
            });
    }
});
