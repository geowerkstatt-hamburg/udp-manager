const Logger = require("../utils/logger.util");
const https_agent = require("../utils/https_agent.util");
const gfi_attributes_parser = require("../utils/gfi_attributes_parser.util");
const stringUtil = require("../utils/string.util");
const logDiffParser = require("../utils/LogDiffParser.util");
const moment = require("moment");

module.exports = ({axios, db_udpm, db_ext, managementService}) => ({
    async getCollections () {
        try {
            const results = await db_udpm.exec.getCollections();

            return results;
        }
        catch (error) {
            Logger.error("collections.service - getCollections: ");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async getCollectionsDataset (params) {
        if (params.dataset_id) {
            try {
                const result = await db_udpm.exec.getCollectionsDataset(params);
                const datasetVersionLog = await db_udpm.exec.getDatasetVersionLogById({dataset_id: params.dataset_id});

                // check for every collection (max 30) if there is a older version available
                // if yes, generate change log for the attribute configuration
                if (result.length < 31 && (params.dataset_status === 'in Bearbeitung' || params.dataset_status === 'veröffentlicht - in Bearbeitung')) {
                    for (const collection of result) {
                        if (datasetVersionLog.length === 0) {
                            collection.attr_log = {status: "no version log", result: {attr_diff: "", timestamp: null, attr_diff_json: null}};
                        }
                        else {
                            const hasVersionLog = datasetVersionLog[0].data.collections.filter(c => c.id === collection.id).length > 0;

                            if (hasVersionLog) {
                                const params_log = {
                                    dataset_id: collection.dataset_id,
                                    selected_collection_id: collection.id,
                                    selected_collection_attr_config: collection.attribute_config,
                                    datasetVersionLog: datasetVersionLog
                                };

                                const attr_diff = await logDiffParser.generateAttrDiffLog(params_log);

                                collection.attr_log = attr_diff;
                            }
                            else {
                                collection.attr_log = {status: "no version log", result: {attr_diff: "", timestamp: null, attr_diff_json: null}};
                            }
                        }
                    }
                }

                return result;
            }
            catch (error) {
                Logger.error("collections.service - getCollectionsDataset: ");
                Logger.error(error);
                return {status: "error", message: error.message};
            }
        }
        else {
            Logger.debug("missing dataset_id");
            return {error: "missing dataset_id"};
        }
    },

    async saveCollectionsDataset (params) {
        params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");

        if (params.id === 0) {
            try {
                const results = await db_udpm.exec.addCollectionsDataset(stringUtil.removeTrailingWhitespacesInObject(params));

                params.change_message = `Neue Collection (ID: ${results[0].id}) angelegt`;
                params.entity = "collection";

                await db_udpm.exec.addChangeLog(params);

                return {status: "success", id: results[0].id};
            }
            catch (error) {
                Logger.error("collections.service - addCollectionsDataset: ");
                Logger.error(error);
                return {status: "error", message: error.message};
            }
        }
        else {
            try {
                await db_udpm.exec.updateCollectionsDataset(stringUtil.removeTrailingWhitespacesInObject(params));

                params.change_message = `Collection (ID: ${params.id}) aktualisiert`;
                params.entity = "collection";

                await db_udpm.exec.addChangeLog(params);

                return {status: "success", id: params.id};
            }
            catch (error) {
                Logger.error("collections.service - updateCollectionsDataset: ");
                Logger.error(error);
                return {status: "error", message: error.message};
            }
        }
    },

    async saveCollectionsDatasetEditorial (params) {
        params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");

        try {
            await db_udpm.exec.updateCollectionsDatasetEditorial(stringUtil.removeTrailingWhitespacesInObject(params));

            params.change_message = `Collection (ID: ${params.id}) aktualisiert`;
            params.entity = "collection";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error("collections.service - updateCollectionsDataset: ");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async deleteCollectionsDataset (params) {
        try {
            await db_udpm.exec.deleteCollectionsDataset(params);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Collection (ID: ${params.id}) gelöscht`;
            params.entity = "collection";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async updateAttributeConfig (params) {
        if (params.attribute_config && params.collection_id) {
            try {
                if (params.attribute_config !== null) {
                    params.attribute_config = JSON.stringify(params.attribute_config);
                }
            }
            catch (error) {
                Logger.debug(error);
            }

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.entity = "collection";

            if (params.useforallcollections) {
                try {
                    await db_udpm.exec.updateAttributeConfigAllCollections(params);

                    params.change_message = `Attributkonfiguration für alle Collection des Datensatzes (ID: ${params.dataset_id}) aktualisiert`;

                    await db_udpm.exec.addChangeLog(params);

                    return {status: "success"};
                }
                catch (error) {
                    Logger.error("collections.service - updateAttributeConfigAllCollections: ");
                    Logger.error(error);
                    return {status: "error", message: error.message};
                }
            }
            else {
                try {
                    await db_udpm.exec.updateAttributeConfig(params);

                    params.change_message = `Attributkonfiguration für Collection (ID: ${params.collection_id}) aktualisiert`;

                    await db_udpm.exec.addChangeLog(params);

                    return {status: "success"};
                }
                catch (error) {
                    Logger.error("collections.service - updateAttributeConfig: ");
                    Logger.error(error);
                    return {status: "error", message: error.message};
                }
            }
        }
        else {
            return {status: "error", message: "parameters collection_id and attribute_config must be included"};
        }
    },

    async updateStyleConfig (params) {
        try {
            if (params.use_for_all_collections) {
                const collections = await db_udpm.exec.getCollectionsDataset(params);

                for (let i = 0; i < collections.length; i++) {
                    if (collections[i].attribute_config) {
                        let sameGeom = false;

                        for (let j = 0; j < collections[i].attribute_config.length; j++) {
                            if (collections[i].attribute_config[j].attr_datatype.indexOf(params.geomType) > 0 && collections[i].attribute_config[j].primary_geom) {
                                sameGeom = true;
                            }
                        }

                        if (sameGeom) {
                            const paramsTemp = {
                                collection_id: collections[i].id,
                                style: params.style
                            };

                            await db_udpm.exec.updateStyleConfig(paramsTemp);
                        }
                    }
                }
            }
            else {
                await db_udpm.exec.updateStyleConfig(params);
            }

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Stylekonfiguration für Collection (ID: ${params.collection_id}) aktualisiert`;
            params.entity = "collection";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error("collections.service - updateStyleConfig: ");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async updateStyleSld (params) {
        try {
            await db_udpm.exec.updateStyleSld(params);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `SLD für Collection (ID: ${params.id}) aktualisiert`;
            params.entity = "collection";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error("collections.service - updateStyleSld: ");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async updateNumberFeatures (params) {
        try {
            if (params.number_features && params.id) {
                await db_udpm.exec.updateNumberFeatures(params);

                return {status: "success"};
            }
            else {
                return {status: "error", message: "missing parameter id or number_features"};
            }
        }
        catch (error) {
            Logger.error("collections.service - updateNumberFeatures: ");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    /**
     * get gfi attributes directly from database
     * @param {Object} params - db connection params, db table name and db schema name
     * @returns {Object} - status info, contains gfi attributes
     */
    async getAttributesDb (params) {
        try {
            const db_connection = await managementService.getDbConnectionById(params.db_connection_r_id, true);

            if (db_connection) {
                await db_ext.exec.dbConnect(db_connection);

                const results = await db_ext.exec.getAttributes(params.db_schema, params.db_table);
                const config = require("../config.js").getConfig();

                const gfi_attributes = [];

                results.query_datatype.forEach((column) => {
                    // add the geometry type (point, line, polygon) to the columns with datatype 'geometry'
                    if (column.udt_name === "geometry") {
                        results.query_geomtype.forEach((element) => {
                            if (Object.values(element).includes(column.column_name)) {
                                column.udt_name += "_" + element.type.toLowerCase();
                            }
                        });
                    }
                    // mapping of datatypes between db and display in udp manager
                    if (config.dbDatatypesMapping[column.udt_name]) {
                        column.udt_name = config.dbDatatypesMapping[column.udt_name];
                    }
                    if (column.column_name === "gid") {
                        gfi_attributes.push(
                            {
                                attr_name_db: column.column_name,
                                attr_name_service: column.column_name,
                                attr_name_masterportal: column.column_name,
                                attr_datatype: column.udt_name,
                                pk: true
                            }
                        );
                    }
                    else {
                        gfi_attributes.push(
                            {
                                attr_name_db: column.column_name,
                                attr_name_service: column.column_name,
                                attr_name_masterportal: column.column_name,
                                attr_datatype: column.udt_name
                            }
                        );
                    }
                });

                return {status: "success", result_data: gfi_attributes};
            }
            else {
                return {status: "error", message: "no DB connection"};
            }
        }
        catch (error) {
            Logger.error("collections.service - getAttributesDb: ");
            Logger.error(error);
            return {status: "error", message: error.message, response: error.response.data};
        }
    },

    async getAttributesService (url, version, type, layername, externalService) {
        let gfi_url;
        const config = require("../config.js").getConfig();
        const proxy_settings = externalService ? config.proxy : false;
        const https_url = url.indexOf("https: ") > -1;
        const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

        if (type === "WMS" || type === "WMS-Time") {
            if (url.indexOf("?") > -1) {
                gfi_url = url + "&request=GetFeatureInfoSchema&version=" + version + "&layers=" + layername;
            }
            else {
                gfi_url = url + "?request=GetFeatureInfoSchema&version=" + version + "&layers=" + layername;
            }
        }

        if (type === "WFS" || type === "WFS-T") {
            if (url.indexOf("?") > -1) {
                gfi_url = url + "&service=WFS&request=DescribeFeatureType&version=" + version + "&typename=" + layername;
            }
            else {
                gfi_url = url + "?service=WFS&request=DescribeFeatureType&version=" + version + "&typename=" + layername;
            }
        }
        try {
            const gml_schema_response = await axios({
                method: "get",
                url: gfi_url,
                proxy: https_url ? false : proxy_settings,
                httpsAgent: httpsAgent
            });

            return {status: "success", result_data: gfi_attributes_parser.parseGFIAttributes(gml_schema_response, gfi_url, layername)};
        }
        catch (error) {
            Logger.error("collections.service - getAttributesService: ");
            Logger.error(error);
            return {status: "error", message: error.message, response: error.response.data};
        }
    },

    async getDBSchemas (params) {
        try {
            const db_connection = await managementService.getDbConnectionById(params.db_connection_r_id, true);

            if (db_connection) {
                await db_ext.exec.dbConnect(db_connection);
                const results = await db_ext.exec.getDBSchemas(params);

                return results;
            }
            else {
                return {status: "error", message: "no DB connection"};
            }
        }
        catch (error) {
            Logger.error("collections.service - getDBSchemas: ");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async getDBTables (params) {
        try {
            const db_connection = await managementService.getDbConnectionById(params.db_connection_r_id, true);

            if (db_connection) {
                await db_ext.exec.dbConnect(db_connection);
                const results = await db_ext.exec.getDBTables(params);

                return results;
            }
            else {
                return {status: "error", message: "no DB connection"};
            }
        }
        catch (error) {
            Logger.error("collections.service - getDBTables: ");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async addDBSchema (params) {
        try {
            const db_connection_w = await managementService.getDbConnectionById(params.db_connection_w_id, true);
            const db_connection_r = await managementService.getDbConnectionById(params.db_connection_r_id, true);

            params.user_w = db_connection_w.user;
            params.user_r = db_connection_r.user;

            if (db_connection_w) {
                await db_ext.exec.dbConnect(db_connection_w);
                await db_ext.exec.addDBSchema(params);

                return {success: true};
            }
            else {
                return {error: true, message: "no DB connection"};
            }
        }
        catch (error) {
            Logger.error("collections.service - addDBSchema: ");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async addDBTable (params) {
        try {
            const db_connection_w = await managementService.getDbConnectionById(params.db_connection_w_id, true);
            const db_connection_r = await managementService.getDbConnectionById(params.db_connection_r_id, true);

            params.user_w = db_connection_w.user;
            params.user_r = db_connection_r.user;

            if (db_connection_w) {
                await db_ext.exec.dbConnect(db_connection_w);
                await db_ext.exec.addDBTable(params);

                return {success: true};
            }
            else {
                return {error: true, message: "no DB connection"};
            }
        }
        catch (error) {
            Logger.error("collections.service - addDBTable: ");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    /**
     * Generates a Bbox from database.
     * @param {Object} params The db params.
     * @returns {Object} The Bbox of the database.
     */
    async generateBbox (params) {
        try {
            const db_connection = await managementService.getDbConnectionById(params.db_connection_r_id, true);

            if (db_connection) {
                await db_ext.exec.dbConnect(db_connection);

                const results = await db_ext.exec.getGeometries(params.db_schema, params.db_table, params.crs);

                const xCoordinates = [];
                const yCoordinates = [];

                results?.forEach(result => {
                    const coordinates = JSON.parse(result.st_asgeojson)?.coordinates[0];

                    coordinates.forEach(coord => {
                        xCoordinates.push(coord[0]);
                        yCoordinates.push(coord[1]);
                    });
                });

                return {
                    xmin: Math.min(...xCoordinates),
                    ymin: Math.min(...yCoordinates),
                    xmax: Math.max(...xCoordinates),
                    ymax: Math.max(...yCoordinates)
                }
            }
        }
        catch (error) {
            Logger.error("collections.service - generateBbox:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getCollectionsChangeLog (params) {
        if (params.dataset_id) {
            try {
                const result = await db_udpm.exec.getCollectionsDataset(params);
                const datasetVersionLog = await db_udpm.exec.getDatasetVersionLogById(params);

                if (datasetVersionLog.length > 0 && result.length < 31) {
                    const collection_params_log = {
                        dataset_id: params.dataset_id,
                        collections: result,
                        datasetVersionLog: datasetVersionLog
                    };

                    const collection_diff = await logDiffParser.generateCollectionDiffLog(collection_params_log);

                    return collection_diff;
                }
                else {
                    return {status: "no version log", result: {attr_diff: "", timestamp: null, editor: null, attr_diff_json: null}};
                }
            }
            catch (error) {
                Logger.error("collections.service - getCollectionsChangeLog: ");
                Logger.error(error);
                return {status: "error", message: error.message};
            }
        }
        else {
            Logger.debug("missing dataset_id");
            return {error: "missing dataset_id"};
        }
    }
});
