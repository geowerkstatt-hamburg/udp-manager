const intersectionObjects = require("../utils/intersect.util");

module.exports = (ldap) => ({
    async match (accountname, onlyUserName) {
        const user = await ldap.findUser(accountname);
        const config = require("../config.js").getConfig();

        if (user.sn) {
            const username = user.givenName + " " + user.sn;

            if (onlyUserName) {
                return {username: username};
            }
            else {
                const groups = await ldap.getGroupMembershipForUser(user);
                const matches = intersectionObjects(config.ldap.allowed_groups, groups);

                return {username: username, matches: matches};
            }

        }
        else {
            return {username: null, matches: []};
        }
    }
});
