const Logger = require("../utils/logger.util");
const https_agent = require("../utils/https_agent.util");
const _ = require("underscore");
const fs = require("fs").promises;
const path = require("path");
const stringUtil = require("../utils/string.util");
const moment = require("moment");

module.exports = ({axios, db_udpm}) => ({

    async getDatasetJiraTickets (params) {
        try {
            const results = await db_udpm.exec.getDatasetJiraTickets(params);

            return results;
        }
        catch (error) {
            Logger.error("jiraticket.service - getDatasetJiraTickets:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    /**
     * Collects all issues located in the jira projects to be searched. The projects to be searched can be set in the config.
     * The search runs via the JIRA API.
     * @returns {Array} - contains all issues of the projects that can be searched
     */
    async searchJiraTickets () {
        const config = require("../config.js").getConfig();
        const projects_to_search = _.where(config.jira.projects, {use_in_ticketsearch: true});
        let all_issues = [];

        try {
            // prepare urls for each project to be searched
            for (let j = 0; j < projects_to_search.length; j++) {
                const url_issue_count = config.jira.url + "/search?jql=project=" + projects_to_search[j].name + "&maxResults=0";
                const url = config.jira.url + "/search?jql=project=" + projects_to_search[j].name + "&fields=id,key,summary,creator,assignee,project,created&maxResults=500&startAt=";
                const proxy_settings = config.jira.proxy ? config.proxy : false;
                const https_url = config.jira.url.indexOf("https:") > -1;
                const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

                // count the issues per project
                const issue_count_response = await axios.get(url_issue_count, {
                    proxy: https_url ? false : proxy_settings,
                    httpsAgent: httpsAgent,
                    auth: {
                        username: process.env.JIRA_USERNAME,
                        password: process.env.JIRA_PASSWORD
                    }
                });

                const iteratee = Math.ceil(issue_count_response.data.total / 100);

                // get every issue with their fields for each project to be searched
                for (let i = 0; i < iteratee; i++) {
                    const issue_resp = await axios.get(url + i * 100, {
                        proxy: https_url ? false : proxy_settings,
                        httpsAgent: httpsAgent,
                        auth: {
                            username: process.env.JIRA_USERNAME,
                            password: process.env.JIRA_PASSWORD
                        }
                    });

                    // if there is no error, the issues of all projects to be searched will be concat
                    if (issue_resp.data.hasOwnProperty("errorMessages")) {
                        Logger.error("jiratickets.service - searchJiraTickets:", issue_resp.data.errorMessages);
                    }
                    else {
                        all_issues = all_issues.concat(issue_resp.data.issues);
                    }
                }
            }
            return all_issues;
        }
        catch (error) {
            Logger.error("jiratickets.service - searchJiraTickets - JIRA API Request:");
            Logger.error(error);
            return {error: true, errorMessages: error.response.data.errorMessages, errors: error.response.data.errors};
        }
    },

    async addDatasetJiraTicket (params) {
        try {
            const results = await db_udpm.exec.addDatasetJiraTicket(stringUtil.removeTrailingWhitespacesInObject(params));

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = "JIRA Ticket verknüpft";
            params.entity = "jira";

            await db_udpm.exec.addChangeLog(params);

            return results;
        }
        catch (error) {
            Logger.error("jiraticket.service - addDatasetJiraTicket:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async deleteDatasetJiraTicket (params) {
        try {
            const results = await db_udpm.exec.deleteDatasetJiraTicket(params);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = "JIRA Ticket Verknüpfung gelöscht";
            params.entity = "jira";

            await db_udpm.exec.addChangeLog(params);

            return results;
        }
        catch (error) {
            Logger.error("jiraticket.service - deleteDatasetJiraTicket:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async getLinkedJiraTickets (params) {
        try {
            const results = await db_udpm.exec.getLinkedJiraTickets(params);

            return results;
        }
        catch (error) {
            Logger.error("jiraticket.service - getLinkedJiraTickets:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    /**
     * Creates a Jira ticket via JIRA API.
     * @param {Object} params - all needed information from the frontend to fill the Jira ticket
     * @returns {Object} - success: true
     */
    async createJiraTicket (params) {
        try {
            const me = this;
            const config = require("../config.js").getConfig();
            const url = config.jira.url + "/issue";
            const proxy_settings = config.jira.proxy ? config.proxy : false;
            const https_url = config.jira.url.indexOf("https:") > -1;
            const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;
            const chars_to_replace = {"ä": "ae", "ö": "oe", "ü": "ue", " ": "_"};
            const create_date_new_ticket = moment().format("YYYY-MM-DD 00:00:00");

            // Variables to get and fill in the template for the requested type of task. this will be the description part of the new jira ticket.
            const description_template_type = params.tickettype.replace(/[ä-ü\s]/g, m => chars_to_replace[m]) + "_template";
            const read_template_file = await fs.readFile(path.join(__dirname, "templates/" + description_template_type + ".txt"), {encoding: "utf8"});
            const template_data = {
                update_components_text: params.update_components_text,
                fme_text: params.fme_text,
                servicetype_text: params.servicetype_text,
                datasource: params.datasource,
                workspace: params.workspace,
                servicename: params.servicename,
                description: params.description,
                responsible_party: params.responsible_party,
                contact_person: params.user
            };
            const template = _.template(read_template_file);
            const template_service_type = template(template_data);

            // Variable to build the data object for the JIRA API request, which is needed to create the new ticket. Generated using an auxiliary function.
            const dataObjectForRequest = await me._getJiraApiRequestDataObject(params, template_service_type);

            // Post request to the JIRA API to create the new ticket.
            var postTicketResponse = await axios({
                method: "post",
                url: url,
                httpsAgent: httpsAgent,
                proxy: https_url ? false : proxy_settings,
                auth: {
                    username: process.env.JIRA_USERNAME,
                    password: process.env.JIRA_PASSWORD
                },
                headers: {
                    "Content-Type": "application/json"
                },
                data: dataObjectForRequest
            });

            // Variable to get the index of the Jira project in which the new ticket was created.
            const project_index = config.jira.projects.findIndex(project => project.name === params.jiraproject_name_to_write);

            // If the ticket is to be moved in another column on the Jira board, a JIRA API request will be executed.
            // The parameter to perform the transition has to be set for each project in the config file.
            // Jira workflows can prevent a ticket from being moved to any column. And since only one move can be implemented per request, a for loop is used.
            // The transition steps are set for each project in the config file.
            if (config.jira.projects[project_index].transition_execution === true) {
                try {
                    for (let i = 0; i < config.jira.projects[project_index].transition_id.length; i++) {
                        await axios({
                            method: "post",
                            url: config.jira.url + "/issue/" + postTicketResponse.data.key + "/transitions",
                            httpsAgent: httpsAgent,
                            proxy: https_url ? false : proxy_settings,
                            auth: {
                                username: process.env.JIRA_USERNAME,
                                password: process.env.JIRA_PASSWORD
                            },
                            headers: {
                                "Content-Type": "application/json"
                            },
                            data: {
                                "transition": {
                                    "id": config.jira.projects[project_index].transition_id[i]
                                }
                            }
                        });
                    }
                }
                catch (error) {
                    Logger.error("jiratickets.service - createJiraTickets - JIRA API Transition-Request:");
                    Logger.error(error);
                    return {error: true, errorType: "transition", errorMessages: error.response.data.errorMessages, errors: error.response.data.errors, errorStatus: error.response.status};
                }
            }

            // informations about the new Jira ticket are stored in the database.
            try {
                await db_udpm.exec.addDatasetJiraTicket({id: postTicketResponse.data.id, dataset_id: params.dataset_id, key: postTicketResponse.data.key, creator: params.user, assignee: params.user, project: params.jiraproject_name_to_write, summary: params.tickettype + ": " + params.dataset_id, create_date: create_date_new_ticket});

                params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
                params.change_message = "neues JIRA Ticket angelegt";
                params.entity = "jira";

                await db_udpm.exec.addChangeLog(params);
            }
            catch (error) {
                Logger.error("jiratickets.service - createJiraTickets - Add Jira Ticket to Database:");
                Logger.error(error);
                return {error: true, errorType: "add_to_database", errorCode: error.code, jiraTicketKey: postTicketResponse.data.key};
            }

            return {success: true};
        }
        catch (error) {
            Logger.error("jiratickets.service - createJiraTickets - JIRA API Request:");
            Logger.error(error);
            return {error: true, errorMessages: error.response.data.errorMessages, errors: error.response.data.errors};
        }
    },

    /**
     * Auxiliary function to build the data object needed for the JIRA API request to create a new Jira ticket.
     * @param {Object} params - all needed information from the frontend to fill the Jira ticket
     * @param {String} template_service_type - filled template for the requested type of task.
     * @returns {Object} - data object needed for the JIRA API request to create a new Jira ticket.
     */
    _getJiraApiRequestDataObject: function (params, template_service_type) {
        return new Promise((resolve, reject) => {
            const config = require("../config.js").getConfig();
            const proxy_settings = config.jira.proxy ? config.proxy : false;
            const https_url = config.jira.url.indexOf("https:") > -1;
            const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;
            const get_customfields_name_url = config.jira.url + "/field";
            const get_component_names = config.jira.url + "/project/" + params.jiraproject_name_to_write + "/components";
            let check_project_fields_url = config.jira.url + "/issue/createmeta/" + params.jiraproject_name_to_write + "/issuetypes/";
            const get_jira_ticket_info = config.jira.url + "/issue/" + params.linkedticket;
            const dataObject = {
                "fields": {
                    "project": {
                        "key": params.jiraproject_name_to_write
                    },
                    "summary": params.tickettype + ": " + params.dataset_id,
                    "description": template_service_type,
                    "issuetype": {
                        "name": params.issuetype
                    },
                    "assignee": {
                        "name": config.jira.defaultAssignee
                    }
                }
            };

            let StoryPointsId;

            (async () => {
                try {
                    // JIRA API request to get the customfield ids.
                    const customfieldResponse = await axios({
                        method: "get",
                        url: get_customfields_name_url,
                        httpsAgent: httpsAgent,
                        proxy: https_url ? false : proxy_settings,
                        auth: {
                            username: process.env.JIRA_USERNAME,
                            password: process.env.JIRA_PASSWORD
                        },
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });

                    for (let i = 0; i < customfieldResponse.data.length; i++) {
                        if (Object.values(customfieldResponse.data[i]).includes("Story Points")) {
                            StoryPointsId = customfieldResponse.data[i].id;
                        }
                    }

                    // JIRA API request to check all available project fields of the given issuetype.
                    // This is necessary to avoid errors when creating the ticket if project fields are missing.
                    if (params.issuetype === "Sub-task") {
                        check_project_fields_url = check_project_fields_url + config.jira.issueTypeId.sub_task;
                        dataObject.fields.parent = {"key": params.linkedticket};
                    }
                    else if (params.issuetype === "Task") {
                        check_project_fields_url = check_project_fields_url + config.jira.issueTypeId.task;
                    }
                    const checkProjectfieldResponse = await axios({
                        method: "get",
                        url: check_project_fields_url,
                        httpsAgent: httpsAgent,
                        proxy: https_url ? false : proxy_settings,
                        auth: {
                            username: process.env.JIRA_USERNAME,
                            password: process.env.JIRA_PASSWORD
                        },
                        headers: {
                            "Content-Type": "application/json"
                        }
                    });

                    // The existing project fields of the given issuetype will be provided.
                    var field_keys = checkProjectfieldResponse.data.fields.map(field => {
                        return field.fieldId;
                    });

                    // Depending on the existence of the project fields, the data object will be extended.
                    if (field_keys.includes("duedate")) {
                        dataObject.fields.duedate = params.duedate;
                    }
                    if (field_keys.includes("labels")) {
                        dataObject.fields.labels = params.jiraproject_label;
                    }
                    if (field_keys.includes(StoryPointsId)) {
                        dataObject.fields[StoryPointsId] = params.story_points_value;
                    }
                    if (field_keys.includes("components")) {
                        // JIRA API request to get the components.
                        const componentsResponse = await axios({
                            method: "get",
                            url: get_component_names,
                            httpsAgent: httpsAgent,
                            proxy: https_url ? false : proxy_settings,
                            auth: {
                                username: process.env.JIRA_USERNAME,
                                password: process.env.JIRA_PASSWORD
                            },
                            headers: {
                                "Content-Type": "application/json"
                            }
                        });

                        if (componentsResponse.data.length > 0) {
                            dataObject.fields.components = params.project_components;
                        }
                    }

                    // JIRA API request to get priority id of linked main ticket to set it for the new Jira sub-ticket.
                    if (params.issuetype === "Sub-task") {
                        const priorityResponse = await axios({
                            method: "get",
                            url: get_jira_ticket_info + "?fields=priority",
                            httpsAgent: httpsAgent,
                            proxy: https_url ? false : proxy_settings,
                            auth: {
                                username: process.env.JIRA_USERNAME,
                                password: process.env.JIRA_PASSWORD
                            },
                            headers: {
                                "Content-Type": "application/json"
                            }
                        });

                        const linkedticket_prio_id = priorityResponse.data.fields.priority.id;

                        dataObject.fields.priority = {"id": linkedticket_prio_id};
                        resolve(dataObject);
                    }
                    else if (params.issuetype === "Task") {
                        resolve(dataObject);
                    }
                }
                catch (error) {
                    Logger.error("jiratickets.service - helper function getJiraApiRequestDataObject - JIRA API Request:");
                    Logger.error(error);
                    reject(error);
                }
            })();
        });
    }
});
