const Logger = require("../utils/logger.util");
const stringUtil = require("../utils/string.util");
const moment = require("moment");

module.exports = ({db_udpm}) => ({
    async getComments (params) {
        try {
            const results = await db_udpm.exec.getComments(params);

            return results;
        }
        catch (error) {
            Logger.error("comments.service - getComments:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },
    async saveComment (params) {
        try {
            if (!params.id) {
                const result = await db_udpm.exec.addComment(stringUtil.removeTrailingWhitespacesInObject(params));

                return {status: "success", id: result[0].id};
            }
            else {
                await db_udpm.exec.updateComment(stringUtil.removeTrailingWhitespacesInObject(params));

                return {status: "success", id: params.id};
            }
        }
        catch (error) {
            Logger.error("comments.service - saveComment:");
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },
    async deleteComment (params) {
        try {
            await db_udpm.exec.deleteComment(params);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Kommentar (ID: ${params.id}) gelöscht`;
            params.entity = "comment";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error("comments.service - deleteComment:");
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    }
});
