const Logger = require("../utils/logger.util");
const dataCache = require("../utils/data_cache.util");

module.exports = ({db_udpm, schedule}) => ({
    async getScheduledTasks () {
        try {
            const results = await db_udpm.exec.getPublicScheduledTasks();

            return results;
        }
        catch (error) {
            Logger.error("scheduledTasks.service - getScheduledTasks:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async startScheduledTask (params) {
        try {
            const task = await db_udpm.exec.getScheduledTaskByName(params);

            if (task.status === "in Ausführung") {
                return {status: "warning", message: "is running"};
            }
            else {
                const scheduled_task = require("../tasks/" + params.task_name + ".task")({db_udpm});

                await db_udpm.exec.updateScheduledTaskStatus({name: params.task_name, status: "in Ausführung", last_run: null, message: ""});

                scheduled_task.execute(true);

                return {status: "success"};
            }
        }
        catch (error) {
            Logger.error("scheduledTasks.service - startScheduledTask:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async cancelScheduledTask (params) {
        try {
            await db_udpm.exec.updateScheduledTaskStatus({name: params.task_name, status: "Zurückgesetzt", last_run: null, message: ""});

            return {status: "success"};
        }
        catch (error) {
            Logger.error("scheduledTasks.service - cancelScheduledTask:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async updateScheduledTasks (params) {
        try {
            const scheduledTaks = dataCache.getScheduledTasks();

            // Update scheduled tasks in DB and reschedule active tasks
            for (let i = 0; i < params.tasks.length; i++) {
                await db_udpm.exec.updateScheduledTask(params.tasks[i]);

                if (scheduledTaks[params.tasks[i].name] && params.tasks[i].enabled) {
                    scheduledTaks[params.tasks[i].name].reschedule(params.tasks[i].schedule);
                }
                else if (params.tasks[i].enabled) {
                    const scheduled_task_obj = require("../tasks/" + params.tasks[i].name + ".task")({db_udpm});

                    scheduledTaks[params.tasks[i].name] = schedule.scheduleJob(params.tasks[i].schedule, function () {
                        scheduled_task_obj.execute();
                    });
                    Logger.debug("Scheduled Task " + params.tasks[i].name + " initialized");
                }
                else if (scheduledTaks[params.tasks[i].name] && !params.tasks[i].enabled) {
                    scheduledTaks[params.tasks[i].name].cancel();
                    delete scheduledTaks[params.tasks[i].name];
                    Logger.debug("Scheduled Task " + scheduledTaks[params.tasks[i].name] + " canceled");
                }
            }

            dataCache.setScheduledTasks(scheduledTaks);

            return {status: "success"};
        }
        catch (error) {
            Logger.error("scheduledTasks.service - updateScheduledTask:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    }
});
