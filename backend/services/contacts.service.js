const Logger = require("../utils/logger.util");
const stringUtil = require("../utils/string.util");

module.exports = ({db_udpm}) => ({
    async getContacts (params) {
        try {
            const results = await db_udpm.exec.getContacts(params);

            return results;
        }
        catch (error) {
            Logger.error("contacts.service - getContacts:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async addContact (params) {
        try {
            const result = await db_udpm.exec.addContact(stringUtil.removeTrailingWhitespacesInObject(params));

            return {status: "success", id: result[0].id};
        }
        catch (error) {
            Logger.error("contacts.service - addContact:");
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async updateContact (params) {
        try {
            await db_udpm.exec.updateContact(stringUtil.removeTrailingWhitespacesInObject(params));

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error("contacts.service - updateContact:");
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async deleteContact (params) {
        try {
            await db_udpm.exec.deleteContact(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error("contacts.service - deleteContact:");
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async updateContactLinks (params) {
        try {
            await db_udpm.exec.deleteContactLinks(params);

            for (let i = 0; i < params.contacts.length; i++) {
                await db_udpm.exec.addContactLink({dataset_id: params.dataset_id, contact_id: params.contacts[i].contact_id});
            }

            return {status: "success"};
        }
        catch (error) {
            Logger.error("contacts.service - updateContactLinks:");
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    }
});
