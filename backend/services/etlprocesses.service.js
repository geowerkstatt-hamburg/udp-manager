const Logger = require("../utils/logger.util");
const stringUtil = require("../utils/string.util");
const moment = require("moment");

module.exports = ({db_udpm, db_ext}) => ({
    async getEtlProcesses (params) {
        try {
            const results = await db_udpm.exec.getEtlProcesses(params);

            return results;
        }
        catch (error) {
            Logger.error("etlprocesses.service - getEtlProcesses:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getEtlProcessesByCollection (params) {
        try {
            const results = await db_udpm.exec.getEtlProcessesByCollection(params);

            return results;
        }
        catch (error) {
            Logger.error("etlprocesses.service - getEtlProcessesByCollection:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async addEtlProcess (params) {
        try {
            if (params.last_run === "") {
                params.last_run = null;
            }

            const result = await db_udpm.exec.addEtlProcess(stringUtil.removeTrailingWhitespacesInObject(params));

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Neuen ETL Prozess (ID: ${result[0].id}) beschrieben`;
            params.entity = "etl";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", id: result[0].id};
        }
        catch (error) {
            Logger.error("etlprocesses.service - addEtlProcess:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async updateEtlProcess (params) {
        try {
            if (params.last_run === "") {
                params.last_run = null;
            }

            await db_udpm.exec.updateEtlProcess(stringUtil.removeTrailingWhitespacesInObject(params));

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `ETL-Prozess (ID: ${params.id}) aktualisiert`;
            params.entity = "etl";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error("etlprocesses.service - updateEtlProcess:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async deleteEtlProcess (params) {
        try {
            await db_udpm.exec.deleteEtlProcess(params);

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error("etlprocesses.service - deleteEtlProcess:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getFmeRepositories (params) {
        try {
            const config = require("../config.js").getConfig();
            const db_connection = config.database.fmeServer.find((fmeServer) => fmeServer.name === params.host);

            if (db_connection) {
                await db_ext.exec.dbConnect(db_connection);

                const results = await db_ext.exec.getFmeRepositories();

                if (results.length > 0) {
                    return {status: "success", repositories: results[0].array_agg};
                }
                else {
                    return {status: "error", message: "error fetching data"};
                }
            }
            else {
                return {status: "error", message: "no FME Flow DB connection"};
            }
        }
        catch (error) {
            Logger.error("etlprocesses.service - getFmeRepositories:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async getFmeWorkspaces (params) {
        try {
            const config = require("../config.js").getConfig();
            const db_connection = config.database.fmeServer.find((fmeServer) => fmeServer.name === params.host);

            if (db_connection) {
                await db_ext.exec.dbConnect(db_connection);

                const results = await db_ext.exec.getFmeWorkspaces(params);

                if (results.length > 0) {
                    return {status: "success", workspaces: results[0].array_agg};
                }
                else {
                    return {status: "error", message: "error fetching data"};
                }
            }
            else {
                return {status: "error", message: "no FME Flow DB connection"};
            }
        }
        catch (error) {
            Logger.error("etlprocesses.service - getFmeWorkspaces:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async getFmeWorkspaceDetails (params) {
        try {
            const config = require("../config.js").getConfig();
            const db_connection = config.database.fmeServer.find((fmeServer) => fmeServer.name === params.host);

            if (db_connection) {
                await db_ext.exec.dbConnect(db_connection);

                const result = await db_ext.exec.getFmeWorkspaceDetails(params);

                return {status: "success", workspace: result};
            }
            else {
                return {status: "error", message: "no FME Flow DB connection"};
            }
        }
        catch (error) {
            Logger.error("etlprocesses.service - getFmeWorkspaceDetails:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async getFmeAutomations (params) {
        try {
            const config = require("../config.js").getConfig();
            const db_connection = config.database.fmeServer.find((fmeServer) => fmeServer.name === params.host);

            if (db_connection) {
                await db_ext.exec.dbConnect(db_connection);

                const results = await db_ext.exec.getFmeAutomations(params);

                if (results.length > 0) {
                    return {status: "success", automations: results[0].array_agg};
                }
                else {
                    return {status: "error", message: "error fetching data"};
                }
            }
            else {
                return {status: "error", message: "no FME Flow DB connection"};
            }
        }
        catch (error) {
            Logger.error("etlprocesses.service - getFmeAutomations:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async updateEtlSummary (params) {
        try {
            const etlProcess = await db_udpm.exec.getEtlProcessById(params);

            etlProcess.last_run = params.last_run;
            etlProcess.mapped_collections = JSON.stringify(etlProcess.mapped_collections);

            await db_udpm.exec.updateEtlProcess(etlProcess);

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error("etlprocesses.service - updateEtlSummary:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    }
});
