const Logger = require("../utils/logger.util");
const https_agent = require("../utils/https_agent.util");
const fs = require("fs").promises;
const path = require("path");

module.exports = ({axios}) => ({
    /**
     * get ldproxy configs per endpoint from filesystem (if git is enabled) or REST API
     * @param {JSON} params - id, name, title, folder, status, type attributes
     * @returns {JSON} - Object with all config entities for given endpoint
     */
    async getLdproxyConfig (params) {
        try {
            const ldproxyConfig = {
                type: params.type,
                serviceId: params.shortname,
                providerId: params.shortname
            };
            const env = "/" + params.status;
            const base_path = path.join(process.env.CONFIG_OUT_PATH, "/configs/" + env + "/ldproxy/" + params.id + "/" + params.folder + "/store/entities");

            ldproxyConfig.serviceRaw = await fs.readFile(path.join(base_path, "/services/" + params.shortname + ".yaml"), {encoding: "utf8"});
            ldproxyConfig.serviceUri = path.join(base_path, "/services/" + params.shortname + ".yaml");
            ldproxyConfig.providerRaw = await fs.readFile(path.join(base_path, "/providers/" + params.shortname + ".yaml"), {encoding: "utf8"});
            ldproxyConfig.providerUri = path.join(base_path, "/providers/" + params.shortname + ".yaml");

            return ldproxyConfig;
        }
        catch (error) {
            Logger.error("ldproxy.service - getLdproxyConf:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async restartWorkspace (params) {
        const config = require("../config.js").getConfig();
        const server = config.server.find((obj) => obj.name === (params.status === "prod" ? params.server_prod : params.server));
        const urls_provider = [];
        const urls_services = [];
        const proxy_settings = false;
        const https_url = server.protocol === "https";
        const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

        if (config.software.ldproxy.internalExternalWorkspace && params.status !== "dev") {
            if (server.lb) {
                for (const lbServer of server.server) {
                    urls_provider.push(`${server.protocol}://${lbServer}/${config.software.ldproxy.internalExternalWorkspace.internal}/reload-entities?ids=*&types=providers`);
                    urls_services.push(`${server.protocol}://${lbServer}/${config.software.ldproxy.internalExternalWorkspace.internal}/reload-entities?ids=${params.name}&types=services`);
                    urls_provider.push(`${server.protocol}://${lbServer}/${config.software.ldproxy.internalExternalWorkspace.external}/reload-entities?ids=*&types=providers`);
                    urls_services.push(`${server.protocol}://${lbServer}/${config.software.ldproxy.internalExternalWorkspace.external}/reload-entities?ids=${params.name}&types=services`);
                }
            }
            else {
                urls_provider.push(`${server.protocol}://${server.name}${server.domain}/${config.software.ldproxy.internalExternalWorkspace.internal}/reload-entities?ids=*&types=providers`);
                urls_services.push(`${server.protocol}://${server.name}${server.domain}/${config.software.ldproxy.internalExternalWorkspace.internal}/reload-entities?ids=${params.name}&types=services`);
                urls_provider.push(`${server.protocol}://${server.name}${server.domain}/${config.software.ldproxy.internalExternalWorkspace.external}/reload-entities?ids=*&types=providers`);
                urls_services.push(`${server.protocol}://${server.name}${server.domain}/${config.software.ldproxy.internalExternalWorkspace.external}/reload-entities?ids=${params.name}&types=services`);
            }
        }
        else {
            if (server.lb) {
                for (const lbServer of server.server) {
                    urls_provider.push(`${server.protocol}://${lbServer}/${params.folder}/reload-entities?ids=*&types=providers`);
                    urls_services.push(`${server.protocol}://${lbServer}/${params.folder}/reload-entities?ids=${params.name}&types=services`);
                }
            }
            else {
                urls_provider.push(`${server.protocol}://${server.name}${server.domain}/${params.folder}/reload-entities?ids=*&types=providers`);
                urls_services.push(`${server.protocol}://${server.name}${server.domain}/${params.folder}/reload-entities?ids=${params.name}&types=services`);
            }
        }

        const promises_provider = [];
        const promises_services = [];

        for (const url_provider of urls_provider) {
            promises_provider.push(
                axios({
                    method: "POST",
                    url: url_provider,
                    proxy: https_url ? false : proxy_settings,
                    httpsAgent: httpsAgent
                })
            );
        }

        const resp_provider = await Promise.all(promises_provider)
            .then(function () {
                return {status: "success"};
            })
            .catch(function (error) {
                Logger.error(error);
                return {status: "error", message: error.message};
            });


        if (resp_provider.status === "success") {
            for (const url_services of urls_services) {
                promises_services.push(
                    axios({
                        method: "POST",
                        url: url_services,
                        proxy: https_url ? false : proxy_settings,
                        httpsAgent: httpsAgent
                    })
                );
            }

            const resp_services = await Promise.all(promises_services)
                .then(function () {
                    return {status: "success"};
                })
                .catch(function (error) {
                    Logger.error(error);
                    return {status: "error", message: error.message};
                });

            return resp_services;
        }
        else {
            return resp_provider;
        }
    }
});
