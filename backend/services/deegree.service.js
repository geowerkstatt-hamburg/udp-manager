const Logger = require("../utils/logger.util");
const deegreeParser = require("../utils/deegree_parser.util");
const https_agent = require("../utils/https_agent.util");
const fs = require("fs").promises;
const path = require("path");

module.exports = ({axios}) => ({
    /**
     * get deegree configs per endpoint from filesystem
     * @param {JSON} params - id, name, title, folder, status, type attributes
     * @returns {JSON} - Object with all config entities for given endpoint
     */
    async getDeegreeConfig (params) {
        try {
            const deegreeConfig = {
                type: params.type,
                serviceId: params.name,
                serviceMdId: params.name + "_metadata",
                themeId: null,
                featureTypes: []
            };

            const env = "/" + params.status;
            const base_path = path.join(process.env.CONFIG_OUT_PATH, "/configs/" + env + "/deegree/" + params.id + "/" + params.folder);

            deegreeConfig.endpointRaw = await fs.readFile(path.join(base_path, "/services/" + params.name + ".xml"), {encoding: "utf8"});
            deegreeConfig.endpointUri = path.join(base_path, "/services/" + params.name + ".xml");
            deegreeConfig.endpointMetadataRaw = await fs.readFile(path.join(base_path, "/services/" + params.name + "_metadata.xml"), {encoding: "utf8"});
            deegreeConfig.endpointMetadataUri = path.join(base_path, "/services/" + params.name + "_metadata.xml");

            if (params.type === "WMS") {
                const endpoint_params = deegreeParser.parseServiceWMS(deegreeConfig.endpointRaw);

                deegreeConfig.themeId = endpoint_params.themeId;

                const theme_data = await fs.readFile(path.join(base_path, "/themes/" + endpoint_params.themeId + ".xml"), {encoding: "utf8"});
                const theme_params = deegreeParser.parseTheme(theme_data);

                deegreeConfig.themeRaw = theme_data;
                deegreeConfig.themeUri = path.join(base_path, "/themes/" + endpoint_params.themeId + ".xml");

                for (let i = 0; i < theme_params.layerStoreIds.length; i++) {
                    const layer_data = await fs.readFile(path.join(base_path, "/layers/" + theme_params.layerStoreIds[i] + ".xml"), {encoding: "utf8"});
                    const layer_params = deegreeParser.parseLayer(layer_data);

                    layer_params.layerStoreId = theme_params.layerStoreIds[i];
                    layer_params.layerRaw = layer_data;
                    layer_params.layerUri = path.join(base_path, "/layers/" + theme_params.layerStoreIds[i] + ".xml");

                    if (layer_params.type === "FeatureLayer") {
                        const feature_store_data = await fs.readFile(path.join(base_path, "/datasources/feature/" + layer_params.datasource + ".xml"), {encoding: "utf8"});
                        const datasource_params = deegreeParser.parseFeatureStore(feature_store_data);

                        layer_params.jdbcId = datasource_params.jdbcId;
                        layer_params.featureStoreRaw = feature_store_data;
                        layer_params.featureStoreUri = path.join(base_path, "/datasources/feature/" + layer_params.datasource + ".xml");

                        const jdbc_data = await fs.readFile(path.join(base_path, "/jdbc/" + datasource_params.jdbcId + ".xml"), {encoding: "utf8"});

                        layer_params.jdbcRaw = jdbc_data;
                        layer_params.jdbcUri = path.join(base_path, "/jdbc/" + datasource_params.jdbcId + ".xml");

                        for (let j = 0; j < layer_params.styles.length; j++) {
                            layer_params.styles[j].styleRaw = await fs.readFile(path.join(base_path, "/styles/" + layer_params.styles[j].styleId + ".xml"), {encoding: "utf8"});
                            layer_params.styles[j].styleUri = path.join(base_path, "/styles/" + layer_params.styles[j].styleId + ".xml");
                        }
                    }
                    else if (layer_params.type === "RemoteWMSLayer") {
                        const feature_store_data = await fs.readFile(path.join(base_path, "/datasources/remoteows/" + layer_params.datasource + ".xml"), {encoding: "utf8"});

                        layer_params.featureStoreRaw = feature_store_data;
                        layer_params.featureStoreUri = path.join(base_path, "/datasources/remoteows/" + layer_params.datasource + ".xml");
                    }

                    deegreeConfig.featureTypes.push(layer_params);
                }
            }
            else if (params.type === "WFS" || params.type === "WFS-T") {
                const endpoint_params = deegreeParser.parseServiceWFS(deegreeConfig.endpointRaw);
                let feature_types = [];

                for (let i = 0; i < endpoint_params.featureStoreIds.length; i++) {
                    const feature_store_data = await fs.readFile(path.join(base_path, "/datasources/feature/" + endpoint_params.featureStoreIds[i] + ".xml"), {encoding: "utf8"});
                    const feature_store_uri = path.join(base_path, "/datasources/feature/" + endpoint_params.featureStoreIds[i] + ".xml");

                    feature_types = deegreeParser.parseWFSFeatureStore(feature_store_data, endpoint_params.featureStoreIds[i], feature_types, feature_store_data, feature_store_uri);
                }

                for (let i = 0; i < feature_types.length; i++) {
                    feature_types[i].jdbcRaw = await fs.readFile(path.join(base_path, "/jdbc/" + feature_types[i].jdbcId + ".xml"), {encoding: "utf8"});
                    feature_types[i].jdbcUri = path.join(base_path, "/jdbc/" + feature_types[i].jdbcId + ".xml");
                }

                deegreeConfig.featureTypes = feature_types;
            }

            return deegreeConfig;
        }
        catch (error) {
            Logger.error("deegree.service - getDeegreeConf:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async validateWorkspace (params) {
        const validateUrl = params.status === "prod" ? params.url_prod.split(params.folder_prod)[0] + params.folder_prod + "/config/validate" : params.url.split(params.folder)[0] + params.folder + "/config/validate";
        const headers = {};
        const config = require("../config.js").getConfig();

        if (config.software.deegree[params.status].softwareSpecificParameter.apiKey) {
            headers.Authorization = "Bearer " + config.software.deegree[params.status].softwareSpecificParameter.apiKey;
        }

        try {
            const proxy_settings = false;
            const https_url = validateUrl.indexOf("https:") > -1;
            const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;
            const result = await axios({
                method: "get",
                url: validateUrl,
                proxy: https_url ? false : proxy_settings,
                httpsAgent: httpsAgent,
                headers: headers
            });
            const validate_errors = [];
            const resp_arr = result.data.split("\n");

            for (let i = 0; i < resp_arr.length; i++) {
                if (resp_arr[i].indexOf("layers") === 0 || resp_arr[i].indexOf("datasources") === 0 || resp_arr[i].indexOf("themes") === 0 || resp_arr[i].indexOf("services") === 0 || resp_arr[i].indexOf("jdbc") === 0 || resp_arr[i].indexOf("styles") === 0) {
                    validate_errors.push({file_name: resp_arr[i].replace(":", "").trim(), message: resp_arr[i + 1]});
                }
            }

            return {status: "success", data: validate_errors};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    async restartWorkspace (params) {
        const config = require("../config.js").getConfig();
        const server = config.server.find((obj) => obj.name === (params.status === "prod" ? params.server_prod : params.server));
        const folder = params.status === "prod" ? params.folder_prod : params.folder;
        const urls = [];
        const proxy_settings = false;
        const https_url = server.protocol === "https";
        const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

        if (server.lb) {
            for (const lbServer of server.server) {
                urls.push(`${server.protocol}://${lbServer}/${folder}/config/restart`);
            }
        }
        else {
            urls.push(`${server.protocol}://${server.name}${server.domain}/${folder}/config/restart`);
        }

        const headers = {};

        if (config.software.deegree[params.status].softwareSpecificParameter.apiKey) {
            headers.Authorization = "Bearer " + config.software.deegree[params.status].softwareSpecificParameter.apiKey;
        }

        const promises = [];

        for (const url of urls) {
            promises.push(
                axios({
                    method: "get",
                    url: url,
                    proxy: https_url ? false : proxy_settings,
                    httpsAgent: httpsAgent,
                    headers: headers
                })
            );
        }

        return Promise.all(promises)
            .then(function () {
                return {status: "success"};
            })
            .catch(function (error) {
                Logger.error(error);
                return {status: "error", message: error.message};
            });
    },

    async getWorkspaceTimeStamp (params) {
        const url = params.status === "prod" ? params.url_prod.split(params.folder_prod)[0] + params.folder_prod + "/config/download/time.stamp" : params.url.split(params.folder)[0] + params.folder + "/config/download/time.stamp";
        const headers = {};
        const config = require("../config.js").getConfig();

        if (config.software.deegree[params.status].softwareSpecificParameter.apiKey) {
            headers.Authorization = "Bearer " + config.software.deegree[params.status].softwareSpecificParameter.apiKey;
        }

        try {
            const proxy_settings = false;
            const https_url = url.indexOf("https:") > -1;
            const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;
            const result = await axios({
                method: "get",
                url: url,
                proxy: https_url ? false : proxy_settings,
                httpsAgent: httpsAgent,
                headers: headers
            });

            if (result.data.indexOf("zuletzt aktualisiert") > -1) {
                return {status: "success", timeStamp: result.data};
            }
            else {
                return {status: "error", message: "file not found"};
            }
        }
        catch (error) {
            return {status: "error", message: "file not found"};
        }
    }
});
