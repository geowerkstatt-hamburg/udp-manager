const Logger = require("../utils/logger.util");
const layerJsonGen = require("../utils/layer_json_gen.util");
const mailer = require("../utils/mail.util");
const moment = require("moment");

module.exports = ({db_udpm, layerJsonElastic}) => ({
    async getLayers (params) {
        try {
            if (params.service_id) {
                return await db_udpm.exec.getLayersService(params);
            }
            else {
                return await db_udpm.exec.getLayers();
            }
        }
        catch (error) {
            Logger.error("layers.service - getLayers:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getLayersPortal (params) {
        try {
            const results = await db_udpm.exec.getLayersPortal(params);

            return results;
        }
        catch (error) {
            Logger.error("layers.service - getLayersPortal:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getLayersCollection (params) {
        if (params.collection_id) {
            try {
                const results = await db_udpm.exec.getLayersCollection(params);
                const results_mod = [];

                for (let i = 0; i < results.length; i++) {
                    const obj = {};

                    if (results[i].name_alt !== null) {
                        results[i].name_tech = results[i].name;
                        results[i].name = results[i].name_alt;
                    }

                    if (results[i].service_type === "WFS-T") {
                        results[i].service_type = "wfs";
                    }
                    if (results[i].service_type === "WMS-Time") {
                        results[i].service_type = "wms";
                    }

                    for (const [key, value] of Object.entries(results[i])) {
                        if (key === "st_attributes") {
                            for (const [key_st_attr, value_st_attr] of Object.entries(results[i].st_attributes)) {
                                obj[key_st_attr + "_" + results[i].service_type.toLowerCase()] = value_st_attr;
                            }
                        }
                        else {
                            obj[key] = value;
                        }
                    }
                    results_mod.push(obj);
                }
                return results_mod;
            }
            catch (error) {
                Logger.error(error);

                return error;
            }
        }
        else {
            Logger.debug("missing collection_id");
            return {error: "missing collection_id"};
        }
    },

    async getLayersService (params) {
        if (params.service_id) {
            try {
                const results = await db_udpm.exec.getLayersService(params);
                const results_mod = [];

                for (let i = 0; i < results.length; i++) {
                    const obj = {};

                    for (const [key, value] of Object.entries(results[i])) {
                        if (key === "st_attributes") {
                            for (const [key_st_attr, value_st_attr] of Object.entries(results[i].st_attributes)) {
                                obj[key_st_attr + "_" + results[i].service_type.toLowerCase()] = value_st_attr;
                            }
                        }
                        else {
                            obj[key] = value;
                        }
                    }
                    results_mod.push(obj);
                }

                return results_mod;
            }
            catch (error) {
                Logger.error(error);

                return error;
            }
        }
        else {
            Logger.debug("missing service_id");
            return {error: "missing service_id"};
        }
    },

    async getLayerJson (params) {
        if (params.id && params.id.match("^[0-9]+$")) {
            params.query = "id";
        }
        else if (params.scope && !params.id) {
            if (params.scope === "intranet") {
                params.query = "intranet";
            }
            else if (params.scope === "internet") {
                params.query = "internet";
            }
            else {
                params.query = "invalid";
            }
        }
        else if (!params.scope && !params.id) {
            params.query = "all";
        }
        else {
            params.query = "invalid";
        }

        if (params.env !== "stage" && params.env !== "prod") {
            params.env = "stage";
        }

        if (params.query !== "invalid") {
            try {
                const result = await db_udpm.exec.getLayerJson(params);
                let res_array = [];

                if (result[0].json_array !== null && result[0].json_array !== undefined) {
                    res_array = result[0].json_array;
                }

                return res_array;
            }
            catch (error) {
                Logger.error(error);

                return error;
            }
        }
        else {
            Logger.debug("layers.service - getLayerJson: invalid query");
            return {error: "query invalid"};
        }
    },

    async createLayer (params) {
        try {
            const result = await db_udpm.exec.addLayer(params);

            await this.updateLayerJson({id: result[0].id}, false);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Neuen Layer (ID: ${result[0].id}) angelegt`;
            params.entity = "layer";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", id: result[0].id};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async updateLayer (params) {
        try {
            await db_udpm.exec.updateLayer(params);

            await this.updateLayerJson({id: params.id}, false);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Layer (ID: ${params.id}) aktualisiert`;
            params.entity = "layer";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success", id: params.id};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async deleteLayer (params) {
        try {
            await db_udpm.exec.deleteLayer(params);

            const config = require("../config.js").getConfig();

            if (config.modules.elasticsearch) {
                await layerJsonElastic.deleteFromIndex(params);
            }

            if (config.mail) {
                await mailer.onDeleteLayer(params.id, params.title, params.mail_from);
            }

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Layer (ID: ${params.id}) gelöscht`;
            params.entity = "layer";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async addLayerDeleteRequest (params) {
        try {
            await db_udpm.exec.addLayerDeleteRequest(params);

            const config = require("../config.js").getConfig();

            if (config.mail) {
                await mailer.onDeleteLayerRequest(params.id, params.title, params.comment, params.linked_portals, params.mail_from);
            }

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Löschvermerk für Layer (ID: ${params.id}) eingetragen`;
            params.entity = "layer";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async deleteLayerDeleteRequest (params) {
        try {
            await db_udpm.exec.deleteLayerDeleteRequest(params);

            params.last_edit_date = moment().format("YYYY-MM-DD HH:mm:ss");
            params.change_message = `Löschvermerk für Layer (ID: ${params.id}) gelöscht`;
            params.entity = "layer";

            await db_udpm.exec.addChangeLog(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async updateLayerJson (params, prod) {
        try {
            let layer_params = [];

            if (params.all) {
                // update layer JSON for all layers
                layer_params = await db_udpm.exec.getCompleteLayerAllParams();
            }
            else if (params.collection_id) {
                // update layer JSON for all layers with same collection_id
                layer_params = await db_udpm.exec.getCompleteLayerCollectionParams(params);
            }
            else if (params.service_id) {
                // update layer JSON for all layers with same service_id
                layer_params = await db_udpm.exec.getCompleteLayerServiceParams(params);
            }
            else if (params.dataset_id) {
                // update layer JSON for all layers with same dataset_id
                layer_params = await db_udpm.exec.getCompleteLayerDatasetParams(params);
            }
            else {
                // update layer JSON for specific layer
                layer_params = await db_udpm.exec.getCompleteLayerParams(params);
            }

            const metadata_catalog_repl = await db_udpm.exec.getMetadataCatalogExtRepl();

            for (let i = 0; i < layer_params.length; i++) {
                let updateProd = prod;

                // delete wfs/wms/wmts prefix of the collection title if it is an external dataset
                if (layer_params[i].dataset_params.external) {
                    const collection_title = layer_params[i].collection_params.title;
                    const prefix_substrings = ["WFS_", "WMS_", "WMTS_"];

                    if (prefix_substrings.some(sub => collection_title.includes(sub))) {
                        const regex = /(W(M|F|MT)S)_/g;

                        layer_params[i].collection_params.title = collection_title.replace(regex, "");
                    }
                }

                if (layer_params[i].service_params.external || params.env === "prod") {
                    updateProd = true;
                }

                const layerJson = layerJsonGen.generateLayerJson(layer_params[i], metadata_catalog_repl, false);

                await db_udpm.exec.updateLayerJson(layerJson);

                if (updateProd) {
                    const layerJsonProd = layerJsonGen.generateLayerJson(layer_params[i], metadata_catalog_repl, true);

                    await db_udpm.exec.updateLayerJsonProd(layerJsonProd);
                }

                const config = require("../config.js").getConfig();

                if (config.modules.elasticsearch) {
                    await layerJsonElastic.updateIndex(layer_params[i], layerJson, prod);
                }
            }

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    }
});
