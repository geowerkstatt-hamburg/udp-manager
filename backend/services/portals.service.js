const Logger = require("../utils/logger.util");
const mailer = require("../utils/mail.util");
const https_agent = require("../utils/https_agent.util");
const layer_id_extractor = require("../utils/layer_id_extractor.util");
const {AuthType, createClient} = require("webdav");

module.exports = ({db_udpm}) => ({
    async getLinkedPortals (params) {
        try {
            const results = await db_udpm.exec.getLinkedPortals(params);

            return results;
        }
        catch (error) {
            Logger.error("portals.service - getLinkedPortals:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async getPortals () {
        try {
            const results = await db_udpm.exec.getPortals();

            return results;
        }
        catch (error) {
            Logger.error("portals.service - getPortals:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async createPortal (params) {
        try {
            await db_udpm.exec.addPortal(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async updatePortal (params) {
        try {
            await db_udpm.exec.updatePortal(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async deletePortal (params) {
        try {
            await db_udpm.exec.deletePortal(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async addPortalLayerLink (params) {
        try {
            await db_udpm.exec.addPortalLayerLink(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    async deletePortalLayerLink (params) {
        try {
            await db_udpm.exec.deletePortalLayerLink(params);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);

            return {status: "error", message: error.message};
        }
    },

    /**
     * scans all webdav directories, that are given in config.js for Masterportal instances, extracts all layer-IDs
     * and saves all information in UDPM database
     * @returns {JSON} - status message
     */
    async processPortals () {
        const apps = [];
        const urls = [];
        const config = require("../config.js").getConfig();

        for (const webdav_conf of config.webdav) {
            const proxy_settings = webdav_conf.proxy ? config.proxy : false;
            const https_url = webdav_conf.base_url.indexOf("https:") > -1;
            const httpsAgent = https_url ? https_agent.getHttpsAgent(proxy_settings) : null;

            const client = createClient(
                webdav_conf.base_url + "/" + webdav_conf.directory,
                {
                    username: webdav_conf.username,
                    password: webdav_conf.password,
                    authType: AuthType.Digest,
                    httpsAgent: httpsAgent
                }
            );

            try {
                // get list of all objects in webdav folder root
                const portals = await client.getDirectoryContents("/");

                for (const portal of portals) {
                    const basename = portal.basename.toLowerCase();

                    if (portal.type === "directory" && !basename.includes("_abnahme") && !basename.includes("-abnahme") && !basename.includes("_test") && !basename.includes("_alt") && !basename.endsWith("_") && !basename.includes("--") && !basename.startsWith("_") && !basename.startsWith(".")) {
                        // try to get Masterportal config.json
                        const portal_config = JSON.parse(await this.getPortalConfig(client, portal));

                        if (portal_config) {
                            // extract all layer-IDs from config.json
                            const app = layer_id_extractor.getApp(webdav_conf, portal, portal_config);

                            // fill array with all Masterportal instances and all Masterportal URLs
                            if (app.url) {
                                apps.push(app);
                                urls.push(app.url);
                            }
                        }
                    }
                }
            }
            catch (error) {
                Logger.error("portals.service - processPortals: Error connecting to WebDAV: " + webdav_conf.base_url);
                Logger.error(error);
            }
        }

        if (apps.length > 0 && urls.length > 0) {
            return this.updatePortalsTable(apps, urls);
        }
        else {
            return {status: "error", message: "no portals found"};
        }
    },

    /**
     * get Masterportal config.json from given folder from WebDAV connection
     * @param {Object} client - WebDAV client object
     * @param {JSON} portal - Masterportal folder object
     * @returns {JSON} - content of Masterportal config.json
     */
    async getPortalConfig (client, portal) {
        try {
            // get all Files from given folder
            const contents = await client.getDirectoryContents(portal.filename);
            let portal_config = null;

            for (const file of contents) {
                if (file.type === "file" && file.basename === "config.json") {
                    // get config.json file content
                    portal_config = await client.getFileContents(portal.filename + "/config.json", {format: "text"});
                }
            }

            return portal_config;
        }
        catch (error) {
            Logger.error("portals.service - getPortalConfig: Error connecting to WebDAV: " + portal.filename);
            Logger.error(error);
            return {};
        }
    },

    /**
     * Update of all autmatically found Masterportal instances
     * @param {Array} apps - List of all found Masterportal Instances
     * @param {Array} urls - List of all Masterportal Instacne URLs
     * @returns {JSON} - status message
     */
    async updatePortalsTable (apps, urls) {
        try {
            // first, delete all portals and portal-layer-links not in the list auf automatically scraped apps
            await db_udpm.exec.deletePortalsNotIncluded({urls: urls});

            for (const app of apps) {
                // check if found Portal is already in db.
                const portal = await db_udpm.exec.getPortalByUrl(app);

                if (portal.length > 0) {
                    // if portal is already in db, then update portal info and portal-layer-links
                    const portal_links = this.getPortalLinksHelper(portal[0].id, app.ids);

                    app.id = portal[0].id;

                    await db_udpm.exec.deleteAllPortalLinks(portal[0]);
                    await db_udpm.exec.updatePortal(app);

                    await this.updatePortalLinks(portal_links);
                }
                else {
                    // if portal is not in db, create a new entry
                    const portal_new = await db_udpm.exec.addPortal(app);
                    const portal_links = this.getPortalLinksHelper(portal_new[0].id, app.ids);

                    await this.updatePortalLinks(portal_links);
                }
            }

            // check, if layers with delete_request=true have any linked portals.
            // if not, send a mail to inform that this layer can now be deleted.
            const deleteLayerRequests = await db_udpm.exec.getDeleteLayerRequests();

            for (const deleteLayerRequest of deleteLayerRequests) {
                Logger.debug("delete layer Request for ID: " + deleteLayerRequest.layer_id);
                const linkedPortals = await db_udpm.exec.getLinkedPortals(deleteLayerRequest);

                if (linkedPortals.length === 0) {
                    await mailer.onNoMoreLinkedPortals(deleteLayerRequest.layer_id, deleteLayerRequest.responsible_party);
                }
            }

            return {status: "success", number_portals: apps.length};
        }
        catch (error) {
            Logger.error("portals.service - updatePortalsTable:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    },

    /**
     * executes portal layer links database insert
     * @param {Array} portal_links -  Collection with all portal links objects for one Masterportal instance
     * @returns {JSON} - status message
     */
    async updatePortalLinks (portal_links) {
        try {
            for (let i = 0; i < portal_links.length; i++) {
                await db_udpm.exec.addPortalLayerLink(portal_links[i]);
            }

            return {status: "success"};
        }
        catch (error) {
            return {status: "error", message: error.message};
        }
    },

    /**
     * Creates Collection of portal layer links in preparation of database insert
     * @param {Integer} portal_id - internal ID of Masterportal Instance
     * @param {Array} layer_ids - Array with all layer-IDs from one Masterportal instance
     * @returns {Array} - Collection with all portal links objects for one Masterportal instance
     */
    getPortalLinksHelper (portal_id, layer_ids) {
        const links = [];

        for (let i = 0; i < layer_ids.length; i++) {
            links.push({portal_id: portal_id, layer_id: layer_ids[i]});
        }

        return links;
    }
});
