const Logger = require("../utils/logger.util");
const fs = require("fs").promises;
const path = require("path");

module.exports = ({db_udpm}) => ({
    /**
     * get Mapserver configs from filesystem (if git is enabled)
     * @param {JSON} params - id, name, title, folder, status, type attributes
     * @returns {JSON} - Object with all config entities for given endpoint
     */
    async getMapserverConfig (params) {
        try {
            const mapserverConfig = {
                type: params.type,
                mapfileId: params.name
            };
            const env = "/" + params.status;
            const base_path = path.join(process.env.CONFIG_OUT_PATH, `/configs/${env}/MapServer/${params.id}/${params.name}`);

            mapserverConfig.mapfileRaw = await fs.readFile(path.join(base_path, `/${params.name}.map`), {encoding: "utf8"});
            mapserverConfig.mapfileUri = path.join(base_path, `/${params.name}.map`);
            mapserverConfig.nestedMapfiles = [];
            mapserverConfig.mapserverTemplates = [];

            const subDirs = (await fs.readdir(base_path, {withFileTypes: true})).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);

            if (subDirs.find((dir) => dir === "nested_mapfiles")) {
                const nestedMapfiles = await fs.readdir(base_path + "/nested_mapfiles");

                for (const nestedMapfile of nestedMapfiles) {
                    mapserverConfig.nestedMapfiles.push(
                        {
                            raw: await fs.readFile(path.join(base_path, "/nested_mapfiles/" + nestedMapfile), {encoding: "utf8"}),
                            uri: path.join(base_path, "/nested_mapfiles/" + nestedMapfile),
                            name: nestedMapfile
                        }
                    );
                }
            }
            if (subDirs.find((dir) => dir === "templates")) {
                const mapserverTemplates = await fs.readdir(base_path + "/templates");

                for (const mapserverTemplate of mapserverTemplates) {
                    mapserverConfig.mapserverTemplates.push(
                        {
                            raw: await fs.readFile(path.join(base_path, "/templates/" + mapserverTemplate), {encoding: "utf8"}),
                            uri: path.join(base_path, "/templates/" + mapserverTemplate),
                            name: mapserverTemplate
                        }
                    );
                }
            }

            return mapserverConfig;
        }
        catch (error) {
            Logger.error("mapserver.service - getMapserverConf:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    async deleteMapserverFile (params) {
        try {
            await fs.rm(params.uri, {force: true});

            const basePath = path.join(process.env.CONFIG_OUT_PATH, "/configs/" + params.status_dev + "/MapServer/" + params.service_id);
            const uri = params.uri.replace(basePath, "").replace(/\\/g, "/").substring(1);

            await db_udpm.exec.deleteServiceConfigEditedByUri({service_id: params.service_id, uri: uri});

            return {status: "success"};
        }
        catch (error) {
            Logger.error("mapserver.service - deleteMapserverFile:");
            Logger.error(error);
            return {status: "error", message: error.message};
        }
    }
});
