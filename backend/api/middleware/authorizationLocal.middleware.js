const dataCache = require("../../utils/data_cache.util.js");
const config = require("../../config.js").getConfig();

/**
 * If route contains "app", and the ldap config is set a authorization test will be evaluated:
 * 1. extract accountname from request header
 * 2. check if user is already authorized
 * 3. check if one of the users AD groups is authorized for access
 * 4. render access granted handlebars template, set client_config, username, read_only an admin variables
 *
 * if dbEmpty=true and role=admin then the new_db template will be rendered
 * if db version and program version not match and role=admin the update_db template will be rendered
 * if db version and program version not match, or db not empty and role!=admin then the db_error template will be rendered
 *
 * @param {Object} req - express request Object
 * @param {Object} res - express response Object
 * @param {Object} next - express next Object
 * @returns {Object} - returns next, if route does not contains "app"
 */
module.exports = async (req, res, next) => {
    if (req.url.indexOf("app") > -1 && req.url.indexOf("app.js") === -1) {
        // evaluate which handlebars template must be rendered
        if (dataCache.getDbEmpty()) {
            res.render("new_db", {apiToken: process.env.API_TOKEN});
        }
        else if (!dataCache.getVersionMatch()) {
            res.render("update_db", {apiToken: process.env.API_TOKEN});
        }
        else if (dataCache.getDbEmpty() || !dataCache.getVersionMatch()) {
            res.render("db_error");
        }
        else {
            res.render("accessgranted", {clientConfig: config.clientConfig, username: "", readonly: false, admin: true, apiToken: process.env.API_TOKEN});
        }
    }
    else if (req.url.indexOf("backend/") > -1) {
        if (req.headers.token === process.env.API_TOKEN || process.env.API_TOKEN === "*" || req.url.indexOf("getlayerjson") > -1) {
            return next();
        }
        else {
            res.status(401);
            res.send("Unauthorized");
        }
    }
    else {
        return next();
    }
};
