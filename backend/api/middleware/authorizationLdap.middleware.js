const config = require("../../config.js").getConfig();
const env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";
const Logger = require("../../utils/logger.util.js");
const dataCache = require("../../utils/data_cache.util.js");
const auth_users = [];

/**
 * If route contains "app", and the ldap config is set a authorization test will be evaluated:
 * 1. extract accountname from request header
 * 2. check if user is already authorized
 * 3. check if one of the users AD groups is authorized for access
 * 4. render access granted handlebars template, set client_config, username, read_only an admin variables
 *
 * if dbEmpty=true and role=admin then the new_db template will be rendered
 * if db version and program version not match and role=admin the update_db template will be rendered
 * if db version and program version not match, or db not empty and role!=admin then the db_error template will be rendered
 *
 * @param {Object} req - express request Object
 * @param {Object} res - express response Object
 * @param {Object} next - express next Object
 * @returns {Object} - returns next, if route does not contains "app"
 */
module.exports = async (req, res, next) => {
    if (req.url.indexOf("app") > -1 && req.url.indexOf("app.js") === -1) {
        // get accountname
        let accountname;

        if (env !== "dev") {
            accountname = req.headers.remoteuser.split("\\")[1];
        }
        else {
            accountname = process.env.DEV_AD_USER;
        }

        // check if user is allready authorized
        const result = auth_users.find((auth_user) => auth_user.accountname === accountname);

        if (result) {
            // if user is already authorized, evaluate which handlebars template must be rendered
            if (dataCache.getDbEmpty() && result.role === "admin") {
                res.render("new_db", {apiToken: process.env.API_TOKEN});
            }
            else if (!dataCache.getVersionMatch() && result.role === "admin") {
                res.render("update_db", {apiToken: process.env.API_TOKEN});
            }
            else if ((dataCache.getDbEmpty() || !dataCache.getVersionMatch()) && result.role !== "admin") {
                res.render("db_error");
            }
            else {
                res.render("accessgranted", {clientConfig: config.clientConfig, username: result.username, readonly: result.role === "reader", admin: result.role === "admin", apiToken: process.env.API_TOKEN});
            }
        }
        else {
            let username = null;
            let matches = [];
            let role = "reader";
            let allowedUser = null;

            if (config.ldap.allowed_users) {
                allowedUser = config.ldap.allowed_users.find((allowed_user) => allowed_user.name === accountname);
            }

            // if user is not part of authorized user array, check user ad groups
            const ldap = require("../../utils/ldap.util.js");
            const {match} = require("../../services/checkUser.service.js")(ldap);

            if (allowedUser) {
                const resultUser = await match(accountname, true);

                username = resultUser.username;
                role = allowedUser.role;
                matches.push(resultUser);

                Logger.debug("User einzeln berechtigt, Rolle: " + role);
            }

            if (!username) {
                const resultGroup = await match(accountname, false);

                matches = resultGroup.matches;
                username = resultGroup.username;

                if (matches.length > 0) {
                    if (matches.find((group) => group.role === "admin")) {
                        role = "admin";
                    }
                    else if (matches.find((group) => group.role === "author")) {
                        role = "author";
                    }

                    Logger.debug("User Teil der berechtigten Gruppen, Rolle: " + role);
                }
            }

            if (username && matches.length > 0) {
                // add user to authorized users array
                auth_users.push({accountname: accountname, username: username, role: role});

                // evaluate which handlebars template must be rendered
                if (dataCache.getDbEmpty() && role === "admin") {
                    res.render("new_db", {apiToken: process.env.API_TOKEN});
                }
                else if (!dataCache.getVersionMatch() && role === "admin") {
                    res.render("update_db", {apiToken: process.env.API_TOKEN});
                }
                else if ((dataCache.getDbEmpty() || !dataCache.getVersionMatch()) && role !== "admin") {
                    res.render("db_error");
                }
                else {
                    res.render("accessgranted", {clientConfig: config.clientConfig, username: username, readonly: role === "reader", admin: role === "admin", apiToken: process.env.API_TOKEN});
                }
            }
            else {
                Logger.debug("Keine Gruppenübereinstimmung für User " + username + " (" + accountname + ") gefunden.");
                res.render("norights", {
                    username: username
                });
            }
        }
    }
    else if (req.url.indexOf("backend/") > -1) {
        if (req.headers.token === process.env.API_TOKEN || process.env.API_TOKEN === "*" || req.url.indexOf("getlayerjson") > -1) {
            return next();
        }
        else {
            res.status(401);
            res.send("Unauthorized");
        }
    }
    else {
        return next();
    }
};
