module.exports = (db_udpm, db_ext, axios, layerJsonElastic) => {
    const Router = require("express-promise-router");
    const router = new Router();
    const schedule = require("node-schedule");
    const dbManagerService = require("../services/db_manager.service")({db_udpm});
    const managementService = require("../services/management.service")({db_udpm});
    const etlProcessesService = require("../services/etlprocesses.service")({db_udpm, db_ext});
    const portalsService = require("../services/portals.service")({db_udpm});
    const collectionsService = require("../services/collections.service")({axios, db_udpm, db_ext, managementService});
    const datasetsService = require("../services/datasets.service")({db_udpm});
    const layersService = require("../services/layers.service")({db_udpm, layerJsonElastic});
    const servicesService = require("../services/services.service")({db_udpm, db_ext, managementService, layersService});
    const scheduledTasksService = require("../services/scheduledTasks.service")({db_udpm, schedule});
    const metadataService = require("../services/metadata.service")({axios, db_udpm});
    const commentsService = require("../services/comments.service")({db_udpm});
    const contactsService = require("../services/contacts.service")({db_udpm});
    const importService = require("../services/import.service")({db_ext, db_udpm});
    const adService = require("../services/ad.service")();
    const deegreeService = require("../services/deegree.service")({db_udpm, axios});
    const geoserverService = require("../services/geoserver.service")({db_udpm, axios});
    const ldproxyService = require("../services/ldproxy.service")({axios});
    const mapserverService = require("../services/mapserver.service")({db_udpm});
    const ogcapiService = require("../services/ogcapi.service")({axios, db_udpm});
    const jiraticketsService = require("../services/jiratickets.service")({axios, db_udpm});
    const statisticsService = require("../services/statistics.service")({db_udpm});
    const etlProcessesController = require("./controllers/etlprocesses.controller")({etlProcessesService});
    const scheduledTasksController = require("./controllers/scheduledTasks.controller")({scheduledTasksService});
    const servicesController = require("./controllers/services.controller")({servicesService, metadataService, deegreeService, geoserverService, ldproxyService, mapserverService});
    const layersController = require("./controllers/layers.controller")({layersService, deegreeService});
    const collectionsController = require("./controllers/collections.controller")({collectionsService, ogcapiService});
    const portalsController = require("./controllers/portals.controller")({portalsService});
    const managementController = require("./controllers/management.controller")({managementService});
    const datasetsController = require("./controllers/datasets.controller")({datasetsService, metadataService});
    const commentsController = require("./controllers/comments.controller")({commentsService});
    const contactsController = require("./controllers/contacts.controller")({contactsService});
    const importController = require("./controllers/import.controller")({importService});
    const adController = require("./controllers/ad.controller")({adService});
    const deegreeController = require("./controllers/deegree.controller")({deegreeService});
    const geoserverController = require("./controllers/geoserver.controller")({geoserverService});
    const ldproxyController = require("./controllers/ldproxy.controller")({ldproxyService});
    const jiraticketsController = require("./controllers/jiratickets.controller")({jiraticketsService});
    const statisticsController = require("./controllers/statistics.controller")({statisticsService});
    const {clearDb, initDb, updateDb} = require("./controllers/db_manager.controller")({dbManagerService});

    router.get("/getscheduledtasks", scheduledTasksController.getScheduledTasks);
    router.post("/startscheduledtask", scheduledTasksController.startScheduledTask);
    router.post("/cancelscheduledtask", scheduledTasksController.cancelScheduledTask);
    router.post("/updatescheduledtasks", scheduledTasksController.updateScheduledTasks);

    router.get("/cleardb", clearDb);
    router.get("/initdbschema", initDb);
    router.get("/updatedbschema", updateDb);

    router.get("/getconfigmetadatacatalogs", managementController.getMetadataCatalogs);
    router.post("/updateconfigmetadatacatalog", managementController.updateMetadataCatalog);
    router.post("/addconfigmetadatacatalog", managementController.addMetadataCatalog);
    router.post("/deleteconfigmetadatacatalog", managementController.deleteMetadataCatalog);
    router.get("/getconfigextinstances", managementController.getExtInstances);
    router.post("/deleteconfigextinstance", managementController.deleteExtInstance);
    router.post("/updateconfigextinstance", managementController.updateExtInstance);
    router.post("/addconfigextinstance", managementController.addExtInstance);
    router.get("/getdbconnections", managementController.getDbConnections);
    router.get("/getdbconnection", managementController.getDbConnection);
    router.post("/deletedbconnection", managementController.deleteDbConnection);
    router.post("/updatedbconnection", managementController.updateDbConnection);
    router.post("/adddbconnection", managementController.addDbConnection);
    router.get("/getsourcedbconnections", managementController.getSourceDbConnections);
    router.get("/getsourcedbconnection", managementController.getSourceDbConnection);
    router.post("/deletesourcedbconnection", managementController.deleteSourceDbConnection);
    router.post("/updatesourcedbconnection", managementController.updateSourceDbConnection);
    router.post("/addsourcedbconnection", managementController.addSourceDbConnection);
    router.get("/getelasticconfig", managementController.getElasticConfig);
    router.post("/updateelasticconfig", managementController.updateElasticConfig);
    router.get("/getudpmconfig", managementController.getAppConfig);
    router.post("/updateudpmconfig", managementController.updateAppConfig);
    router.get("/getfmeserverconnections", managementController.getFmeServerConnections);
    router.post("/savefmeserverconnection", managementController.saveFmeServerConnection);
    router.post("/deletefmeserverconnection", managementController.deleteFmeServerConnection);
    router.get("/getwebdavconnections", managementController.getWebDavConnections);
    router.post("/savewebdavconnection", managementController.saveWebDavConnection);
    router.post("/deletewebdavconnection", managementController.deleteWebDavConnection);

    router.get("/getservices", servicesController.getServices);
    router.get("/getservicesdataset", servicesController.getServicesDataset);
    router.post("/saveservice", servicesController.saveService);
    router.post("/deleteservice", servicesController.deleteService);
    router.post("/getservicemetadata", servicesController.getServiceMetadata);
    router.get("/getdistinctservicekeywords", servicesController.getDistinctKeywords);
    router.post("/updateserviceallowedgroups", servicesController.updateAllowedGroups);
    router.post("/addservicelink", servicesController.addServiceLink);
    router.post("/getservicelinks", servicesController.getServiceLinks);
    router.post("/updatewmsthemeconfig", servicesController.updateWmsThemeConfig);
    router.post("/generateconfig", servicesController.generateConfig);
    router.post("/deployserviceconfig", servicesController.deployServiceConfig);
    router.post("/undeployserviceconfig", servicesController.unDeployServiceConfig);
    router.post("/checkserviceconfig", servicesController.checkServiceConfig);
    router.post("/getserviceconfig", servicesController.getServiceConfig);
    router.post("/overwriteserviceconfig", servicesController.overwriteServiceConfig);
    router.get("/getserviceconfigsedited", servicesController.getServiceConfigsEdited);
    router.post("/deleteserviceconfigedited", servicesController.deleteServiceConfigEdited);
    router.post("/createconfigfile", servicesController.createConfigFile);
    router.post("/deletemapserverfile", servicesController.deleteMapserverFile);

    router.get("/getlayers", layersController.getLayers);
    router.get("/getlayersportal", layersController.getLayersPortal);
    router.get("/getlayerscollection", layersController.getLayersCollection);
    router.get("/getlayersservice", layersController.getLayersService);
    router.get("/getlayerjson", layersController.getLayerJson);
    router.post("/savelayer", layersController.saveLayer);
    router.post("/deletelayer", layersController.deleteLayer);
    router.post("/addlayerdeleterequest", layersController.addLayerDeleteRequest);
    router.post("/deletelayerdeleterequest", layersController.deleteLayerDeleteRequest);
    router.get("/updatelayerjson", layersController.updateLayerJson);

    router.get("/getdatasets", datasetsController.getDatasets);
    router.get("/getdataset", datasetsController.getDataset);
    router.post("/savedataset", datasetsController.saveDataset);
    router.post("/savedataseteditorial", datasetsController.saveDatasetEditorial);
    router.post("/deletedataset", datasetsController.deleteDataset);
    router.post("/getdatasetmetadata", datasetsController.getDatasetMetadata);
    router.get("/getdistinctdatasetkeywords", datasetsController.getDistinctKeywords);
    router.get("/getdistinctdatasetfilterkeywords", datasetsController.getDistinctFilterKeywords);
    router.post("/updatedatasetallowedgroups", datasetsController.updateAllowedGroups);
    router.get("/getchangelog", datasetsController.getChangeLog);
    router.get("/adddatasetsversionlogentries", datasetsController.addDatasetsVersionLogEntries);
    router.post("/pushdataset", datasetsController.pushDataset);
    router.delete("/canceldataset", datasetsController.cancelDataset);

    router.get("/getdbschemas", collectionsController.getDBSchemas);
    router.get("/getdbtables", collectionsController.getDBTables);
    router.post("/adddbschema", collectionsController.addDBSchema);
    router.post("/adddbtable", collectionsController.addDBTable);
    router.post("/generateBbox", collectionsController.generateBbox);
    router.post("/updatestyleconfig", collectionsController.updateStyleConfig);
    router.post("/updatestylesld", collectionsController.updateStyleSld);
    router.post("/updatenumberfeatures", collectionsController.updateNumberFeatures);
    router.get("/getcollections", collectionsController.getCollections);
    router.get("/getcollectionsdataset", collectionsController.getCollectionsDataset);
    router.post("/savecollectionsdataset", collectionsController.saveCollectionsDataset);
    router.post("/savecollectionsdataseteditorial", collectionsController.saveCollectionsDatasetEditorial);
    router.post("/deletecollection", collectionsController.deleteCollectionsDataset);
    router.post("/updateattributeconfig", collectionsController.updateAttributeConfig);
    router.post("/getattributesservice", collectionsController.getAttributesService);
    router.post("/getattributesdb", collectionsController.getAttributesDb);
    router.post("/getservicecollections", collectionsController.getServiceCollections);
    router.get("/getcollectionschangelog", collectionsController.getCollectionsChangeLog);

    router.get("/getlinkedportals", portalsController.getLinkedPortals);
    router.get("/getportals", portalsController.getPortals);
    router.post("/saveportal", portalsController.savePortal);
    router.post("/deleteportal", portalsController.deletePortal);
    router.post("/addportallayerlink", portalsController.addPortalLayerLink);
    router.post("/deleteportallayerlink", portalsController.deletePortalLayerLink);

    router.get("/getcomments", commentsController.getComments);
    router.post("/savecomment", commentsController.saveComment);
    router.post("/deletecomment", commentsController.deleteComment);

    router.get("/getcontacts", contactsController.getContacts);
    router.post("/addcontact", contactsController.saveContact);
    router.post("/deletecontact", contactsController.deleteContact);
    router.post("/updatecontactlinks", contactsController.updateContactLinks);

    router.get("/getadgroups", adController.getADGroups);
    router.get("/getadcontacts", adController.getADUsers);
    router.get("/getaduserdetails", adController.getADUserDetails);
    router.get("/getgroupmembershipforuser", adController.getGroupMembershipForUser);

    router.post("/startworkspacevalidate", deegreeController.validateWorkspace);
    router.post("/startworkspacerestart", deegreeController.restartWorkspace);
    router.post("/getworkspacetimestamp", deegreeController.getWorkspaceTimeStamp);

    router.post("/startgeoserverworkspacerestart", geoserverController.restartWorkspace);

    router.post("/startldproxyworkspacerestart", ldproxyController.restartWorkspace);

    router.get("/getjiraticketsdataset", jiraticketsController.getDatasetJiraTickets);
    router.post("/searchjiratickets", jiraticketsController.searchJiraTickets);
    router.post("/addjiraticketsdataset", jiraticketsController.addDatasetJiraTicket);
    router.post("/deletejiraticketdataset", jiraticketsController.deleteDatasetJiraTicket);
    router.get("/getlinkedjiratickets", jiraticketsController.getLinkedJiraTickets);
    router.post("/createjiraticket", jiraticketsController.createJiraTicket);

    router.post("/searchdatasetinextinstance", importController.searchDatasetInExtInstance);
    router.post("/importdataset", importController.importDataset);

    router.get("/getetlprocesses", etlProcessesController.getEtlProcesses);
    router.post("/addetlprocess", etlProcessesController.addEtlProcess);
    router.post("/updateetlprocess", etlProcessesController.updateEtlProcess);
    router.post("/deleteetlprocess", etlProcessesController.deleteEtlProcess);
    router.post("/getfmerepositories", etlProcessesController.getFmeRepositories);
    router.post("/getfmeworkspaces", etlProcessesController.getFmeWorkspaces);
    router.post("/getfmeworkspacedetails", etlProcessesController.getFmeWorkspaceDetails);
    router.post("/getfmeautomations", etlProcessesController.getFmeAutomations);
    router.post("/updateetlsummary", etlProcessesController.updateEtlSummary);

    router.get("/getdistinctmonth", statisticsController.getDistinctMonth);
    router.get("/getvisits", statisticsController.getVisits);
    router.get("/getvisitstop", statisticsController.getVisitsTop);
    router.get("/getvisitstotal", statisticsController.getVisitsTotal);
    router.get("/getsoftwarestats", statisticsController.getSoftwareStats);
    router.get("/getservicestats", statisticsController.getServiceStats);
    router.get("/getlayerstats", statisticsController.getLayerStats);

    return router;
};
