module.exports = ({layersService, deegreeService}) => ({
    async getLayers (req, res) {
        const results = await layersService.getLayers(req.query);

        res.json(results);
    },

    async getLayersPortal (req, res) {
        const results = await layersService.getLayersPortal(req.query);

        res.json(results);
    },

    async getLayersCollection (req, res) {
        const results = await layersService.getLayersCollection(req.query);

        res.json({results: results});
    },

    async getLayersService (req, res) {
        const results = await layersService.getLayersService(req.query);

        res.json({results: results});
    },

    async getLayerJson (req, res) {
        const results = await layersService.getLayerJson(req.query);

        res.set("Content-Type", "application/json; charset=UTF-8");

        if (req.query.style === "pretty") {
            res.send(JSON.stringify(results, null, 2));
        }
        else {
            res.json(results);
        }
    },

    async saveLayer (req, res) {
        let status = {};

        if (req.body.id === 0) {
            status = await layersService.createLayer(req.body);
        }
        else {
            status = await layersService.updateLayer(req.body);
        }

        res.json(status);
    },

    async deleteLayer (req, res) {
        const status = await layersService.deleteLayer(req.body);

        res.json(status);
    },

    async addLayerDeleteRequest (req, res) {
        const status = await layersService.addLayerDeleteRequest(req.body);

        res.json(status);
    },

    async deleteLayerDeleteRequest (req, res) {
        const status = await layersService.deleteLayerDeleteRequest(req.body);

        res.json(status);
    },

    async updateLayerJson (req, res) {
        let generateProd = false;

        if (req.query?.external === "true") {
            generateProd = true;
        }

        const status = await layersService.updateLayerJson(req.query, generateProd);

        res.json(status);
    }
});
