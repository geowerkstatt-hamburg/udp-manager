module.exports = ({ldproxyService}) => ({
    async restartWorkspace (req, res) {
        const results = await ldproxyService.restartWorkspace(req.body);

        res.json(results);
    }
});
