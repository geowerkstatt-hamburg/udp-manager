module.exports = ({deegreeService}) => ({
    async validateWorkspace (req, res) {
        const results = await deegreeService.validateWorkspace(req.body);

        res.json(results);
    },

    async restartWorkspace (req, res) {
        const results = await deegreeService.restartWorkspace(req.body);

        res.json(results);
    },

    async getWorkspaceTimeStamp (req, res) {
        const results = await deegreeService.getWorkspaceTimeStamp(req.body);

        res.json(results);
    }
});
