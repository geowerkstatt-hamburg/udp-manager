module.exports = ({servicesService, metadataService, deegreeService, geoserverService, ldproxyService, mapserverService}) => ({
    async getServices (req, res) {
        const results = await servicesService.getServices();

        res.json(results);
    },

    async getServicesDataset (req, res) {
        const services = await servicesService.getServicesDataset(req.query);

        res.json({results: services});
    },

    async saveService (req, res) {
        let status = {};

        if (req.body.id === 0) {
            if (req.body.external) {
                req.body.status_dev = false;
                req.body.status_stage = false;
                req.body.status_prod = true;
            }
            else {
                req.body.status_dev = true;
                req.body.status_stage = false;
                req.body.status_prod = false;
            }

            status = await servicesService.createService(req.body);
        }
        else {
            status = await servicesService.updateService(req.body);
        }

        res.json(status);
    },

    async deleteService (req, res) {
        const status = await servicesService.deleteService(req.body);

        res.json(status);
    },

    async getServiceMetadata (req, res) {
        const results = await metadataService.getServiceMetadata(req.body);

        res.json({results: results});
    },

    async getDistinctKeywords (req, res) {
        const results = await servicesService.getDistinctKeywords();

        res.json(results);
    },

    async updateAllowedGroups (req, res) {
        const results = await servicesService.updateAllowedGroups(req.body);

        res.json(results);
    },

    async addServiceLink (req, res) {
        const results = await servicesService.addServiceLink(req.body);

        res.json(results);
    },

    async getServiceLinks (req, res) {
        const results = await servicesService.getServiceLinks(req.body);

        res.json(results);
    },

    async updateWmsThemeConfig (req, res) {
        const results = await servicesService.updateWmsThemeConfig(req.body);

        res.json(results);
    },

    async generateConfig (req, res) {
        const results = await servicesService.generateConfig(req.body);

        res.json(results);
    },

    async deployServiceConfig (req, res) {
        const results = await servicesService.deployServiceConfig(req.body);

        res.json(results);
    },

    async unDeployServiceConfig (req, res) {
        const results = await servicesService.unDeployServiceConfig(req.body);

        res.json(results);
    },

    async checkServiceConfig (req, res) {
        let allConfigsInFolder = true;

        for (const service of req.body.services) {
            if (service.software === "deegree") {
                const conf = await deegreeService.getDeegreeConfig(service);

                if (conf.error) {
                    allConfigsInFolder = false;
                }
            }
            else if (service.software === "MapServer") {
                const conf = await mapserverService.getMapserverConfig(service);

                if (conf.error) {
                    allConfigsInFolder = false;
                }
            }
            else if (service.software === "ldproxy") {
                const conf = await ldproxyService.getLdproxyConfig(service);

                if (conf.error) {
                    allConfigsInFolder = false;
                }
            }
        }

        res.json({allConfigsInFolder: allConfigsInFolder});
    },

    async getServiceConfig (req, res) {
         let results = {};

        if (req.body.software === "deegree") {
            results = await deegreeService.getDeegreeConfig(req.body);
        }
        else if (req.body.software === "GeoServer") {
            results = await geoserverService.getGeoserverConfig(req.body);
        }
        else if (req.body.software === "MapServer") {
            results = await mapserverService.getMapserverConfig(req.body);
        }
        else if (req.body.software === "ldproxy") {
            results = await ldproxyService.getLdproxyConfig(req.body);
        }

        res.json(results);
    },

    async overwriteServiceConfig (req, res) {
        const results = await servicesService.overwriteServiceConfig(req.body);

        res.json(results);
    },

    async getServiceConfigsEdited (req, res) {
        const results = await servicesService.getServiceConfigsEdited(req.query);

        res.json(results);
    },

    async deleteServiceConfigEdited (req, res) {
        const results = await servicesService.deleteServiceConfigEdited(req.body);

        res.json(results);
    },

    async createConfigFile (req, res) {
        const results = await servicesService.createConfigFile(req.body);

        res.json(results);
    },

    async deleteMapserverFile (req, res) {
        const results = await mapserverService.deleteMapserverFile(req.body);

        res.json(results);
    }
});
