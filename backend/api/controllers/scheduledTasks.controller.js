module.exports = ({scheduledTasksService}) => ({
    async getScheduledTasks (req, res) {
        const results = await scheduledTasksService.getScheduledTasks();

        res.json(results);
    },

    async startScheduledTask (req, res) {
        const results = await scheduledTasksService.startScheduledTask(req.body);

        res.json(results);
    },

    async cancelScheduledTask (req, res) {
        const results = await scheduledTasksService.cancelScheduledTask(req.body);

        res.json(results);
    },

    async updateScheduledTasks (req, res) {
        const results = await scheduledTasksService.updateScheduledTasks(req.body);

        res.json(results);
    }
});
