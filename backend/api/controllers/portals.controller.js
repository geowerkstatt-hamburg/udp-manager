module.exports = ({portalsService}) => ({
    async getLinkedPortals (req, res) {
        const results = await portalsService.getLinkedPortals(req.query);

        res.json(results);
    },

    async getPortals (req, res) {
        const results = await portalsService.getPortals();

        res.json(results);
    },

    async savePortal (req, res) {
        let status = {};

        if (req.body.id === 0) {
            status = await portalsService.createPortal(req.body);
        }
        else {
            status = await portalsService.updatePortal(req.body);
        }

        res.json(status);
    },

    async deletePortal (req, res) {
        const results = await portalsService.deletePortal(req.body);

        res.json(results);
    },

    async addPortalLayerLink (req, res) {
        const results = await portalsService.addPortalLayerLink(req.body);

        res.json(results);
    },

    async deletePortalLayerLink (req, res) {
        const results = await portalsService.deletePortalLayerLink(req.body);

        res.json(results);
    }
});
