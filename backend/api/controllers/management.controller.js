module.exports = ({managementService}) => ({
    async getMetadataCatalogs (req, res) {
        const result = await managementService.getMetadataCatalogs();

        res.json(result);
    },

    async getExtInstances (req, res) {
        const result = await managementService.getExtInstances();

        res.json(result);
    },

    async addMetadataCatalog (req, res) {
        const result = await managementService.addMetadataCatalog(req.body);

        res.json(result);
    },

    async updateMetadataCatalog (req, res) {
        const result = await managementService.updateMetadataCatalog(req.body);

        res.json(result);
    },

    async deleteMetadataCatalog (req, res) {
        const result = await managementService.deleteMetadataCatalog(req.body);

        res.json(result);
    },

    async addExtInstance (req, res) {
        const result = await managementService.addExtInstance(req.body);

        res.json(result);
    },

    async updateExtInstance (req, res) {
        const result = await managementService.updateExtInstance(req.body);

        res.json(result);
    },


    async deleteExtInstance (req, res) {
        const result = await managementService.deleteExtInstance(req.body);

        res.json(result);
    },

    async getDbConnections (req, res) {
        const result = await managementService.getDbConnections();

        res.json(result);
    },

    async getDbConnection (req, res) {
        const result = await managementService.getDbConnectionById(req.query.id, false);

        res.json(result);
    },

    async addDbConnection (req, res) {
        const result = await managementService.addDbConnection(req.body);

        res.json(result);
    },

    async updateDbConnection (req, res) {
        const result = await managementService.updateDbConnection(req.body);

        res.json(result);
    },

    async deleteDbConnection (req, res) {
        const result = await managementService.deleteDbConnection(req.body);

        res.json(result);
    },

    async getSourceDbConnections (req, res) {
        const result = await managementService.getSourceDbConnections();

        res.json(result);
    },

    async getSourceDbConnection (req, res) {
        const result = await managementService.getSourceDbConnectionById(req.query.id, false);

        res.json(result);
    },

    async addSourceDbConnection (req, res) {
        const result = await managementService.addSourceDbConnection(req.body);

        res.json(result);
    },

    async updateSourceDbConnection (req, res) {
        const result = await managementService.updateSourceDbConnection(req.body);

        res.json(result);
    },

    async deleteSourceDbConnection (req, res) {
        const result = await managementService.deleteSourceDbConnection(req.body);

        res.json(result);
    },

    async getElasticConfig (req, res) {
        const result = await managementService.getElasticConfig();

        res.json(result);
    },

    async updateElasticConfig (req, res) {
        const result = await managementService.updateElasticConfig(req.body);

        res.json(result);
    },

    async getAppConfig (req, res) {
        const result = await managementService.getAppConfig();

        res.json(result);
    },

    async updateAppConfig (req, res) {
        const result = await managementService.updateAppConfig(req.body);

        res.json(result);
    },

    async getFmeServerConnections (req, res) {
        const result = await managementService.getFmeServerConnections();

        res.json(result);
    },

    async saveFmeServerConnection (req, res) {
        if (req.body.id === 0) {
            const result = await managementService.addFmeServerConnection(req.body);

            res.json(result);
        }
        else {
            const result = await managementService.updateFmeServerConnection(req.body);

            res.json(result);
        }
    },

    async deleteFmeServerConnection (req, res) {
        const result = await managementService.deleteFmeServerConnection(req.body);

        res.json(result);
    },

    async getWebDavConnections (req, res) {
        const result = await managementService.getWebDavConnections();

        res.json(result);
    },

    async saveWebDavConnection (req, res) {
        if (req.body.id === 0) {
            const result = await managementService.addWebDavConnection(req.body);

            res.json(result);
        }
        else {
            const result = await managementService.updateWebDavConnection(req.body);

            res.json(result);
        }
    },

    async deleteWebDavConnection (req, res) {
        const result = await managementService.deleteWebDavConnection(req.body);

        res.json(result);
    }
});
