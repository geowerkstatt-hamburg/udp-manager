module.exports = ({dbManagerService}) => ({
    async clearDb (req, res) {
        const result = await dbManagerService.clearDb();

        res.json(result);
    },
    async initDb (req, res) {
        const result = await dbManagerService.initDb();

        res.json(result);
    },
    async updateDb (req, res) {
        const result = await dbManagerService.updateDb();

        res.json(result);
    }
});
