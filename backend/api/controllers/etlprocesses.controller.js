module.exports = ({etlProcessesService}) => ({
    async getEtlProcesses (req, res) {
        if (req.query.dataset_id) {
            const results = await etlProcessesService.getEtlProcesses(req.query);

            res.json(results);
        }
        else if (req.query.collection_id) {
            const results = await etlProcessesService.getEtlProcessesByCollection(req.query);

            res.json(results);
        }
        else {
            res.json({status: "error", message: "invalid request"});
        }
    },

    async addEtlProcess (req, res) {
        const results = await etlProcessesService.addEtlProcess(req.body);

        res.json(results);
    },

    async updateEtlProcess (req, res) {
        const results = await etlProcessesService.updateEtlProcess(req.body);

        res.json(results);
    },

    async deleteEtlProcess (req, res) {
        const results = await etlProcessesService.deleteEtlProcess(req.body);

        res.json(results);
    },

    async getFmeRepositories (req, res) {
        const results = await etlProcessesService.getFmeRepositories(req.body);

        res.json(results);
    },

    async getFmeWorkspaces (req, res) {
        const results = await etlProcessesService.getFmeWorkspaces(req.body);

        res.json(results);
    },

    async getFmeWorkspaceDetails (req, res) {
        const results = await etlProcessesService.getFmeWorkspaceDetails(req.body);

        res.json(results);
    },

    async getFmeAutomations (req, res) {
        const results = await etlProcessesService.getFmeAutomations(req.body);

        res.json(results);
    },

    async updateEtlSummary (req, res) {
        const results = await etlProcessesService.updateEtlSummary(req.body);

        res.json(results);
    }
});
