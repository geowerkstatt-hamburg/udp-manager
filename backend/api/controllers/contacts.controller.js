module.exports = ({contactsService}) => ({
    async getContacts (req, res) {
        const results = await contactsService.getContacts(req.query);

        res.json(results);
    },

    async saveContact (req, res) {
        let status = {};

        if (req.body.id) {
            status = await contactsService.updateContact(req.body);
        }
        else {
            status = await contactsService.addContact(req.body);
        }

        res.json(status);
    },

    async deleteContact (req, res) {
        const results = await contactsService.deleteContact(req.body);

        res.json(results);
    },

    async updateContactLinks (req, res) {
        const results = await contactsService.updateContactLinks(req.body);

        res.json(results);
    },
});
