module.exports = ({datasetsService, metadataService}) => ({
    async getDatasets (req, res) {
        if (req.query.user) {
            const datasets = await datasetsService.getDatasetsByUser(req.query.user);

            res.json(datasets);
        }
        else if (req.query.objectguid) {
            const datasets = await datasetsService.getDatasetsByObjectGuid(req.query.objectguid);

            res.json(datasets);
        }
        else {
            const datasets = await datasetsService.getDatasets();

            res.json({results: datasets});
        }

    },
    async getDataset (req, res) {
        if (req.query.id) {
            const dataset = await datasetsService.getDatasetById(req.query.id);

            res.json(dataset);
        }
        if (req.query.shortname) {
            const dataset = await datasetsService.getDatasetByShortname(req.query.shortname);

            res.json(dataset);
        }
        else {
            res.json({error: "missing request parameter. One of id or shortname is required."});
        }

    },
    async saveDataset (req, res) {
        const status = await datasetsService.saveDataset(req.body);

        res.json(status);
    },
    async saveDatasetEditorial (req, res) {
        const status = await datasetsService.saveDatasetEditorial(req.body);

        res.json(status);
    },
    async deleteDataset (req, res) {
        const status = await datasetsService.deleteDataset(req.body);

        res.json(status);
    },
    async getDatasetMetadata (req, res) {
        const results = await metadataService.getDatasetMetadata(req.body);

        res.json({results: results});
    },
    async getDistinctKeywords (req, res) {
        const results = await datasetsService.getDistinctKeywords();

        res.json(results);
    },
    async getDistinctFilterKeywords (req, res) {
        const results = await datasetsService.getDistinctFilterKeywords();

        res.json(results);
    },
    async updateAllowedGroups (req, res) {
        const results = await datasetsService.updateAllowedGroups(req.body);

        res.json(results);
    },
    async getChangeLog (req, res) {
        const results = await datasetsService.getChangeLog(req.query);

        res.json(results);
    },
    async addDatasetsVersionLogEntries (req, res) {
        const results = await datasetsService.addDatasetsVersionLogEntries(req.body);

        res.json(results);
    },
    async pushDataset (req, res) {
        res.json({status: "success"});
    },
    async cancelDataset (req, res) {
        res.json({status: "success"});
    }
});
