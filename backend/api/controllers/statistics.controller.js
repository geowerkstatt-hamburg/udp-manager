module.exports = ({statisticsService}) => ({
    async getDistinctMonth (req, res) {
        const results = await statisticsService.getDistinctMonth();

        res.json(results);
    },

    async getVisits (req, res) {
        const results = await statisticsService.getVisits(req.query);

        res.json(results);
    },

    async getVisitsTotal (req, res) {
        const results = await statisticsService.getVisitsTotal();

        res.json(results);
    },

    async getVisitsTop (req, res) {
        const results = await statisticsService.getVisitsTop(req.query);

        res.json(results);
    },

    async getSoftwareStats (req, res) {
        const results = await statisticsService.getSoftwareStats(req.query);

        res.json(results);
    },

    async getServiceStats (req, res) {
        const results = await statisticsService.getServiceStats(req.query);

        res.json(results);
    },

    async getLayerStats (req, res) {
        const results = await statisticsService.getLayerStats(req.query);

        res.json(results);
    }
});
