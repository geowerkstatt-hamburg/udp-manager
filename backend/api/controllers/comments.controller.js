module.exports = ({commentsService}) => ({
    async getComments (req, res) {
        const results = await commentsService.getComments(req.query);

        res.json(results);
    },

    async saveComment (req, res) {
        const results = await commentsService.saveComment(req.body);

        res.json(results);
    },

    async deleteComment (req, res) {
        const results = await commentsService.deleteComment(req.body);

        res.json(results);
    }
});
