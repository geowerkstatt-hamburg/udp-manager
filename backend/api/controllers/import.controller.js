module.exports = ({importService}) => ({
    async searchDatasetInExtInstance (req, res) {
        const results = await importService.searchDatasetInExtInstance(req.body);

        res.json(results);
    },

    async importDataset (req, res) {
        const results = await importService.importDataset(req.body, req.body.update);

        res.json(results);
    }
});
