module.exports = ({adService}) => ({
    async getADGroups (req, res) {
        const results = await adService.getADGroups(req.query);

        res.json(results);
    },

    async getADUsers (req, res) {
        const results = await adService.getADUsers(req.query);

        res.json(results);
    },

    async getGroupMembershipForUser (req, res) {
        const results = await adService.getGroupMembershipForUser(req.query);

        res.json(results);
    },

    async getADUserDetails (req, res) {
        const results = await adService.getADUserDetails(req.query);

        res.json(results);
    }
});
