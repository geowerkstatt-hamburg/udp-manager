module.exports = ({jiraticketsService}) => ({
    async getDatasetJiraTickets (req, res) {
        const results = await jiraticketsService.getDatasetJiraTickets(req.query);

        res.json(results);
    },

    async searchJiraTickets (req, res) {
        const results = await jiraticketsService.searchJiraTickets();

        res.json(results);
    },

    async addDatasetJiraTicket (req, res) {
        const results = await jiraticketsService.addDatasetJiraTicket(req.body);

        res.json(results);
    },

    async deleteDatasetJiraTicket (req, res) {
        const results = await jiraticketsService.deleteDatasetJiraTicket(req.body);

        res.json(results);
    },

    async getLinkedJiraTickets (req, res) {
        const results = await jiraticketsService.getLinkedJiraTickets(req.query);

        res.json(results);
    },

    async createJiraTicket (req, res) {
        const results = await jiraticketsService.createJiraTicket(req.body);

        res.json(results);
    }
});
