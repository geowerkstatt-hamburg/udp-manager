module.exports = ({collectionsService, ogcapiService}) => ({
    async getCollections (req, res) {
        const results = await collectionsService.getCollections();

        res.json(results);
    },
    async getCollectionsDataset (req, res) {

        const collections = await collectionsService.getCollectionsDataset(req.query);

        res.json({results: collections});
    },
    async saveCollectionsDataset (req, res) {

        const results = await collectionsService.saveCollectionsDataset(req.body);

        res.json(results);
    },
    async saveCollectionsDatasetEditorial (req, res) {

        const results = await collectionsService.saveCollectionsDatasetEditorial(req.body);

        res.json(results);
    },
    async deleteCollectionsDataset (req, res) {
        const results = await collectionsService.deleteCollectionsDataset(req.body);

        res.json(results);
    },
    async updateAttributeConfig (req, res) {
        const results = await collectionsService.updateAttributeConfig(req.body);

        res.json(results);
    },
    async updateStyleConfig (req, res) {
        const results = await collectionsService.updateStyleConfig(req.body);

        res.json(results);
    },
    async updateStyleSld (req, res) {
        const results = await collectionsService.updateStyleSld(req.body);

        res.json(results);
    },
    async getAttributesDb (req, res) {
        const results = await collectionsService.getAttributesDb(req.body);

        res.json(results);
    },
    async getAttributesService (req, res) {
        const results = await collectionsService.getAttributesService(req.body.url, req.body.version, req.body.type, req.body.layername, req.body.externalService);

        res.json(results);
    },
    async getDBSchemas (req, res) {
        const results = await collectionsService.getDBSchemas(req.query);

        res.json(results);
    },
    async getDBTables (req, res) {
        const results = await collectionsService.getDBTables(req.query);

        res.json(results);
    },
    async addDBSchema (req, res) {
        const results = await collectionsService.addDBSchema(req.body);

        res.json(results);
    },
    async addDBTable (req, res) {
        const results = await collectionsService.addDBTable(req.body);

        res.json(results);
    },
    async generateBbox (req, res) {
        const results = await collectionsService.generateBbox(req.body);

        res.json(results);
    },
    async getServiceCollections (req, res) {
        const results = await ogcapiService.getServiceCollections(req.body);

        res.json(results);
    },
    async updateNumberFeatures (req, res) {
        const results = await collectionsService.updateNumberFeatures(req.body);

        res.json(results);
    },
    async getCollectionsChangeLog (req, res) {
        const results = await collectionsService.getCollectionsChangeLog(req.query);

        res.json(results);
    }
});
