module.exports = ({geoserverService}) => ({
    async restartWorkspace (req, res) {
        const results = await geoserverService.restartWorkspace(req.body);

        res.json(results);
    }
});
