const Services = require("./services.js");

module.exports = () => {
    const genericConfigurator = require("../generic_components/configurator.js");

    genericConfigurator._createService = (data) => {
        if (data.service.type === "wfs" || data.service.type === "wms") {
            return new Services.GeoserverService(data);
        }
        else {
            throw new Error(`type ${data.service.type} is not implemented yet`);
        }
    }

    return genericConfigurator;
};
