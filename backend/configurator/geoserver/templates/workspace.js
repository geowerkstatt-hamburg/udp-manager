const moment = require("moment");

/**
 * Configure the workspace attributes.
 * @param {Object} data The data from config-files.
 * @param {Object} hexValues The hex values for geoserver files.
 * @returns {Object} The configuration for workspace as json.
 */
function configureWorkspace (data, hexValues) {
    const config = {
        id: hexValues.workspace,
        name: data.service.name,
        isolated: "false",
        dateCreated: moment().format("YYYY-MM-DD h:mm:ss.SSS") + " UTC"
    };

    return {"workspace": config};
}

module.exports = {configureWorkspace};
