const moment = require("moment");

/**
 * Configure the style xml attributes.
 * @param {Object} data The data from config-files.
 * @param {String} collectionName The name of the collection.
 * @param {Object} hexValues The hex values for geoserver files.
 * @returns {Object} The configuration for style xml as json.
 */
function configureStyleXML (data, collectionName, hexValues) {
    const config = {
        id: hexValues[collectionName].style,
        name: collectionName,
        workspace: {
            id: hexValues.workspace
        },
        format: "sld",
        languageVersion: {
            version: "1.0.0"
        },
        filename: collectionName + ".sld",
        dateCreated: moment().format("YYYY-MM-DD h:mm:ss.SSS") + " UTC"
    };

    return {"style": config};
}

/**
 * Configure the style sld attributes.
 * @param {Object} data The data from config-files.
 * @param {String} collectionName The name of the collection.
 * @returns {Object} The configuration for style sld as json.
 */
function configureStyleSLD (data, collectionName) {
    const config = {
        StyledLayerDescriptor: {
            "@version": "1.0.0",
            "@xsi:schemaLocation": "http://www.opengis.net/sld StyledLayerDescriptor.xsd",
            "@xmlns": "http://www.opengis.net/sld",
            "@xmlns:ogc": "http://www.opengis.net/ogc",
            "@xmlns:xlink": "http://www.w3.org/1999/xlink",
            "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
            NamedLayer: {
                Name: collectionName,
                UserStyle: {
                    FeatureTypeStyle: createSLDStyle(data.collections[collectionName].style)
                }
            }
        }
    };

    return {"style": config};
}

/**
 * Creates the SLD style.
 * @param {Object} styles The style attributes.
 * @returns {Object} The style sld.
 */
function createSLDStyle(styles) {
    const rules = {
        Rule: []
    };

    styles.forEach(style => {
        const mapStyle = {
            "point": createSLDPointStyle(style),
            "line": createSLDLineStyle(style),
            "polygon": createSLDPolygonStyle(style)
        },
        rule = {
            Name: style.rule_name,
            Title: style.title
        }

        if (style.minscaledenominator !== null || style.maxscaledenominator !== null) {
            Object.assign(rule, createSLDMinMaxDenominator(style));
        }

        if (style.filter_attribute !== null) {
            Object.assign(rule, createSLDFilter(style));
        }

        Object.assign(rule, mapStyle[style.geom_type]);
        rules.Rule.push(rule)
    });

    return rules;
}

/**
 * Creates the SLD min and max denominator.
 * @param {Object} style The style attributes.
 * @returns {Object} The min and max denominator sld.
 */
function createSLDMinMaxDenominator (style) {
    const minMaxDenominatorConfig = {};

    if (style.minscaledenominator) {
        minMaxDenominatorConfig.MinScaleDenominator = style.minscaledenominator;
    }

    if (style.maxscaledenominator) {
        minMaxDenominatorConfig.MaxScaleDenominator = style.maxscaledenominator
    }

    return minMaxDenominatorConfig;
}

/**
 * Creates the SLD filter.
 * @param {Object} style The style attributes.
 * @returns {Object} The filter sld.
 */
function createSLDFilter(style) {
    const filterConfig = {
        filter: {}
    };

    filterConfig.filter[style.filter_condition] = {
        PropertyName: style.filter_attribute.includes(":") ? style.filter_attribute.split(":")[1] : style.filter_attribute,
        Literal: style.filter_literal
    };

    return filterConfig;
}

/**
 * Creates the SLD point style.
 * @param {Object} style The style attributes.
 * @returns {Object} The point style sld.
 */
function createSLDPointStyle (style) {
    const pointWellknonnames = ["circle", "square", "triangle", "star", "cross", "x"];
    const pointConfig = {
            PointSymbolizer: {
                Graphic: {
                    Mark: {
                        WellKnownName: style.point_wellknownname
                    },
                    Size: style.size,
                    Rotation: style.icon_rotation
                }
            }
        };

    if (pointWellknonnames.includes(style.point_wellknownname)) {
        Object.assign(pointConfig.PointSymbolizer.Graphic.Mark, creatreSLDFillStyle(style));
        Object.assign(pointConfig.PointSymbolizer.Graphic.Mark, creatreSLDStrokeStyle(style));
    }
    else if (style.point_wellknownname === "externalGraphic") {
        Object.assign(pointConfig.PointSymbolizer, createSldExternalGraphicStyle(style));
    }

    return pointConfig;
}

/**
 * Creates the SLD line style.
 * @param {Object} style The style attributes.
 * @returns {Object} The line style sld.
 */
function createSLDLineStyle (style) {
    const lineConfig = {
        LineSymbolizer: {}
    };

    Object.assign(lineConfig.LineSymbolizer, creatreSLDStrokeStyle(style));

    return lineConfig;
}

/**
 * Creates the SLD polygon style.
 * @param {Object} style The style attributes.
 * @returns {Object} The polygon style sld.
 */
function createSLDPolygonStyle (style) {
    const polygonConfig = {
        PolygonSymbolizer: {}
    };

    if (style.fill_type === "einfarbig") {
        Object.assign(polygonConfig.PolygonSymbolizer, creatreSLDFillStyle(style));
    }
    else if (style.fill_type === "gemustert" || style.fill_type === "Externes Icon") {
        Object.assign(polygonConfig.PolygonSymbolizer, createSldSGraphicFillStyle(style));
    }

    Object.assign(polygonConfig.PolygonSymbolizer, creatreSLDStrokeStyle(style));

    return polygonConfig;
}

/**
 * Creates the SLD external graphic style.
 * @param {Object} style The style attributes.
 * @returns {Object} The external graphic style sld.
 */
function createSldExternalGraphicStyle (style) {
    const externalGraphicConfig = {
        Graphic: {
            ExternalGraphic: {
                OnlineResource: {
                    "@xlink:type": "simple",
                    "@xlink:href": `./symbole/${style.file_name_exticon}`
                },
                Format: "image/png"
            },
            Size: style.icon_size,
            Rotation: style.icon_rotation
        }
    };

    return externalGraphicConfig;
}

/**
 * Creates the SLD graphic fill style.
 * @param {Object} style The style attributes.
 * @returns {Object} The graphic fill style sld.
 */
function createSldSGraphicFillStyle (style) {
    const graphicFillConfig = {
        Fill: {
            GraphicFill: {
                Graphic: {
                    ExternalGraphic: {
                        OnlineResource: {
                            "@xlink:type": "simple",
                            "@xlink:href": style.file_name_exticon !== null ? `./symbole/${style.file_name_exticon}` : `./symbole/${style.rule_name}_polygon_canvas.png`
                        },
                        Format: style.file_content_type_exticon || "image/png"
                    },
                    Size: style.icon_size,
                    Rotation: style.icon_rotation
                }
            }
        }
    };

    return graphicFillConfig;
}

/**
 * Creates the SLD Fill style.
 * @param {Object} style The style attributes.
 * @returns {Object} The fill style sld.
 */
function creatreSLDFillStyle (style) {
    const fillConfig = {
        Fill: {
            CssParameter: []
        }
    };

    if (style.fill_color_dynamic === true) {
        fillConfig.Fill.CssParameter = [
            {
                "@name": "fill",
                "ogc:PropertyName": style.fill_color_dynamic_attribute
            }
        ];
    }
    else {
        fillConfig.Fill.CssParameter = [
            {
                "@name": "fill",
                "#": "#" + style.fill_color_hex
            },
            {
                "@name": "fill-opacity",
                "#": style.fill_opacity
            }
        ];
    }

    return fillConfig;
}

/**
 * Creates the SLD stroke style.
 * @param {Object} style The style attributes.
 * @returns {Object} The stroke style sld.
 */
function creatreSLDStrokeStyle (style) {
    const strokeConfig = {
        Stroke: {
            CssParameter: []
        }
    };

    if (style.stroke_color_dynamic === true) {
        strokeConfig.Stroke.CssParameter = [
            {
                "@name": "stroke",
                "ogc:PropertyName": style.stroke_color_dynamic_attribute
            }
        ];
    }
    else {
        strokeConfig.Stroke.CssParameter = [
            {
                "@name": "stroke",
                "#": "#" + style.stroke_color_hex
            },
            {
                "@name": "stroke-opacity",
                "#": style.stroke_opacity
            },
            {
                "@name": "stroke-width",
                "#": style.stroke_width || style.stroke_width_line
            },
            {
                "@name": "stroke-linejoin",
                "#": "round"
            }
        ];

        if (style.pattern_dash_array && style.pattern_dash_array !== null) {
            strokeConfig.Stroke.CssParameter.push(
                {
                    "@name": "stroke-dasharray",
                    "#": style.pattern_dash_array
                }
            );
        }
    }

    return strokeConfig;
}

module.exports = {configureStyleXML, configureStyleSLD};
