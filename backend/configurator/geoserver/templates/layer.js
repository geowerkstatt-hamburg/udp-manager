const moment = require("moment");

/**
 * Configure the layer attributes.
 * @param {Object} data The data from config-files.
 * @param {String} collectionName The name of the collection.
 * @param {Object} hexValues The hex values for geoserver files.
 * @returns {Object} The configuration for layer as json.
 */
function configureLayer (data, collectionName, hexValues) {
    const config = {
        name: collectionName,
        id: hexValues[collectionName].layer,
        type: "VECTOR",
        defaultStyle: {
            id: hexValues[collectionName].style
        },
        resource: {
            "@class": "featureType",
            id: hexValues[collectionName].featureType
        },
        attribution: {
            logoWidth: 0,
            logoHeight: 0
        },
        dateCreated: moment().format("YYYY-MM-DD h:mm:ss.SSS") + " UTC"
    };

    if (data.collections[collectionName].style === null) {
        delete config.defaultStyle;
    }

    return {"layer": config};
}

module.exports = {configureLayer};
