/**
 * Configure the namespace attributes.
 * @param {Object} data The data from config-files.
 * @param {Object} hexValues The hex values for geoserver files.
 * @returns {Object} The configuration for namespace as json.
 */
function configureNamespace (data, hexValues) {
    const config = {
        id: hexValues.namespace,
        prefix: data.service.name,
        uri: data.service.name,
        isolated: "false"
    };

    return {"namespace": config};
}

module.exports = {configureNamespace};
