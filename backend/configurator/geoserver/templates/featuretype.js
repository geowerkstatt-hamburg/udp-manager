const proj4 = require("proj4");

/**
 * Configure the featureType attributes.
 * @param {Object} data The data from config-files.
 * @param {String} collectionName The name of the collection.
 * @param {Object} hexValues The hex values for geoserver files.
 * @returns {Object} The configuration for featureType as json.
 */
function configureFeaturetype (data, collectionName, hexValues) {
    registerProjections()

    const bbox = data.collections[collectionName].custom_bbox;
    const dataCrs = data.collections[collectionName].crs;
    const targetSrs = data.dataset.srs;
    const bbox_4326 = transformBbox(bbox, targetSrs, "EPSG:4326");

    const config = {
        id: hexValues[collectionName].featureType,
        name: collectionName,
        nativeName: data.collections[collectionName].db_table,
        namespace: {
            id: hexValues.namespace
        },
        title: collectionName,
        keywords: {
            string: [
                "features",
                data.collections[collectionName].db_table
            ]
        },
        nativeCRS: getNativeCRS(dataCrs),
        srs: targetSrs,
        nativeBoundingBox: {
            minx: bbox.xmin,
            maxx: bbox.xmax,
            miny: bbox.ymin,
            maxy: bbox.ymax,
            crs: {
                "@class": "projected",
                "#": targetSrs,
            }
        },
        latLonBoundingBox: {
            minx: bbox_4326.xmin,
            maxx: bbox_4326.xmax,
            miny: bbox_4326.ymin,
            maxy: bbox_4326.ymax,
            crs: "EPSG:4326"
        },
        projectionPolicy: dataCrs === targetSrs ? "FORCE_DECLARED" : "REPROJECT_TO_DECLARED",
        enabled: true,
        store: {
            "@class": "dataStore",
            id: hexValues[collectionName].dataStore
        },
        serviceConfiguration: "false",
        simpleConversionEnabled: "false",
        internationalTitle: "",
        internationalAbstract: "",
        maxFeatures: 0,
        numDecimals: 0,
        padWithZeros: "false",
        forcedDecimal: "false",
        overridingServiceSRS: "false",
        skipNumberMatched: "false",
        circularArcPresent: "false"
    };

    return {"featureType": config};
}

/**
 * Transform the bbox to target crs.
 * @param {String[]} bbox The bbox in dat crs.
 * @param {String} sourceCrs The source crs.
 * @param {String} targetCrs The target crs.
 * @returns
 */
function transformBbox (bbox, sourceCrs, targetCrs) {
    const xmin = parseFloat(bbox.xmin, 10);
    const ymin = parseFloat(bbox.ymin, 10);
    const xmax = parseFloat(bbox.xmax, 10);
    const ymax = parseFloat(bbox.ymax, 10);
    const sourceProjection = proj4.defs(sourceCrs);
    const targetProjection = proj4.defs(targetCrs);
    const transformedBboxMin = proj4(sourceProjection, targetProjection, [xmin, ymin]);
    const transformedBboxMax = proj4(sourceProjection, targetProjection, [xmax, ymax]);

    return {
        xmin: transformedBboxMin[0],
        ymin: transformedBboxMin[1],
        xmax: transformedBboxMax[0],
        ymax: transformedBboxMax[1]
    };

}

/**
 * Register the projections to proj4.
 * @returns {void}
 */
function registerProjections() {
    const namedProjections = [
        ["EPSG:3857", "+proj=merc +a=6378137 +b=6378137 +lat_ts=0 +lon_0=0 +x_0=0 +y_0=0 +k=1 +units=m +nadgrids=@null +wktext +no_defs +type=crs"],
        ["EPSG:4326", "+proj=longlat +datum=WGS84 +no_defs +type=crs"],
        ["EPSG:25832", "+proj=utm +zone=32 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs"],
        ["EPSG:25833", "+proj=utm +zone=33 +ellps=GRS80 +towgs84=0,0,0,0,0,0,0 +units=m +no_defs +type=crs"]
    ];

    proj4.defs(namedProjections);
}

/**
 * Returns the native crs.
 * @param {String} epsgcode The EPSG Code.
 * @returns {Object} The native crs.
 */
function getNativeCRS (epsgcode) {
    if (epsgcode === "CRS:84") {
        return {
            "#":  "GEOGCS[\&quot\;WGS 84\&quot\;, \n"
              + "  DATUM[&quot;WGS84\&quot\;, \n"
              + "    SPHEROID[\&quot\;WGS 84\&quot\;, 6378137.0, 298.257223563]], \n"
              + "  PRIMEM[\&quot\;Greenwich\&quot\;, 0.0], \n"
              + "  UNIT[\&quot\;degree\&quot\;, 0.017453292519943295], \n"
              + "  AXIS[\&quot\;Geodetic longitude\&quot\;, EAST], \n"
              + "  AXIS[\&quot\;Geodetic latitude\&quot\;, NORTH], \n"
              + "AUTHORITY[\&quot\;Web Map Service CRS\&quot\;,\&quot\;84\&quot\;]]"
        };
    }
    else if (epsgcode === "EPSG:3857") {
        return {
            "@class": "projected",
            "#":  "PROJCS[\&quot\;WGS 84 / Pseudo-Mercator\&quot\;, \n"
              + "GEOGCS[&quot;WGS 84&quot;, \n"
              + "  DATUM[&quot;World Geodetic System 1984\&quot\;, \n"
              + "    SPHEROID[\&quot\;WGS 84\&quot\;, 6378137.0, 298.257223563, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;7030\&quot\;]], \n"
              + "    AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;6326\&quot\;]], \n"
              + "  PRIMEM[\&quot\;Greenwich\&quot\;, 0.0, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;8901\&quot\;]], \n"
              + "  UNIT[\&quot\;degree\&quot\;, 0.017453292519943295], \n"
              + "  AXIS[\&quot\;Geodetic longitude\&quot\;, EAST], \n"
              + "  AXIS[\&quot\;Geodetic latitude\&quot\;, NORTH], \n"
              + "  AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;4326\&quot\;]], \n"
              + "PROJECTION[\&quot\;Popular Visualisation Pseudo Mercator\&quot\;, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;1024\&quot\;]], \n"
              + "PARAMETER[\&quot\;semi_minor\&quot\;, 6378137.0], \n"
              + "PARAMETER[\&quot\;latitude_of_origin\&quot\;, 0.0], \n"
              + "PARAMETER[\&quot\;central_meridian\&quot\;, 0.0], \n"
              + "PARAMETER[\&quot\;scale_factor\&quot\;, 1.0], \n"
              + "PARAMETER[\&quot\;false_easting\&quot\;, 0.0], \n"
              + "PARAMETER[\&quot\;false_northing\&quot\;, 0.0], \n"
              + "UNIT[\&quot\;m\&quot\;, 1.0], \n"
              + "AXIS[\&quot\;Easting\&quot\;, EAST], \n"
              + "AXIS[\&quot\;Northing\&quot\;, NORTH], \n"
              + "AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;3857\&quot\;]]"
        };
    }
    else if (epsgcode === "EPSG:4326") {
        return {
            "#":  "GEOGCS[\&quot\;WGS 84\&quot\;, \n"
              + "  DATUM[&quot;World Geodetic System 1984\&quot\;, \n"
              + "    SPHEROID[\&quot\;WGS 84\&quot\;, 6378137.0, 298.257223563, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;7030\&quot\;]], \n"
              + "    AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;6326\&quot\;]], \n"
              + "  PRIMEM[\&quot\;Greenwich\&quot\;, 0.0, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;8901\&quot\;]], \n"
              + "  UNIT[\&quot\;degree\&quot\;, 0.017453292519943295], \n"
              + "  AXIS[\&quot\;Geodetic longitude\&quot\;, EAST], \n"
              + "  AXIS[\&quot\;Geodetic latitude\&quot\;, NORTH], \n"
              + "AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;4326\&quot\;]]"
        };
    }
    else if (epsgcode === "EPSG:25832") {
        return {
            "@class": "projected",
            "#":  "PROJCS[\&quot\;ETRS89 / UTM zone 32N\&quot\;, \n"
              + "GEOGCS[&quot;ETRS89&quot;, \n"
              + "  DATUM[&quot;European Terrestrial Reference System 1989\&quot\;, \n"
              + "    SPHEROID[\&quot\;GRS 1980\&quot\;, 6378137.0, 298.257222101, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;7019\&quot\;]], \n"
              + "    TOWGS84[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], \n"
              + "    AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;6258\&quot\;]], \n"
              + "  PRIMEM[\&quot\;Greenwich\&quot\;, 0.0, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;8901\&quot\;]], \n"
              + "  UNIT[\&quot\;degree\&quot\;, 0.017453292519943295], \n"
              + "  AXIS[\&quot\;Geodetic longitude\&quot\;, EAST], \n"
              + "  AXIS[\&quot\;Geodetic latitude\&quot\;, NORTH], \n"
              + "  AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;4258\&quot\;]], \n"
              + "PROJECTION[\&quot\;Transverse_Mercator\&quot\;, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;9807\&quot\;]], \n"
              + "PARAMETER[\&quot\;central_meridian\&quot\;, 9.0], \n"
              + "PARAMETER[\&quot\;latitude_of_origin\&quot\;, 0.0], \n"
              + "PARAMETER[\&quot\;scale_factor\&quot\;, 0.9996], \n"
              + "PARAMETER[\&quot\;false_easting\&quot\;, 500000.0], \n"
              + "PARAMETER[\&quot\;false_northing\&quot\;, 0.0], \n"
              + "UNIT[\&quot\;m\&quot\;, 1.0], \n"
              + "AXIS[\&quot\;Easting\&quot\;, EAST], \n"
              + "AXIS[\&quot\;Northing\&quot\;, NORTH], \n"
              + "AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;25832\&quot\;]]"
        };
    }
    else if (epsgcode === "EPSG:25833") {
        return {
            "@class": "projected",
            "#":  "PROJCS[\&quot\;ETRS89 / UTM zone 33N\&quot\;, \n"
              + "GEOGCS[&quot;ETRS89&quot;, \n"
              + "  DATUM[&quot;European Terrestrial Reference System 1989\&quot\;, \n"
              + "    SPHEROID[\&quot\;GRS 1980\&quot\;, 6378137.0, 298.257222101, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;7019\&quot\;]], \n"
              + "    TOWGS84[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0], \n"
              + "    AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;6258\&quot\;]], \n"
              + "  PRIMEM[\&quot\;Greenwich\&quot\;, 0.0, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;8901\&quot\;]], \n"
              + "  UNIT[\&quot\;degree\&quot\;, 0.017453292519943295], \n"
              + "  AXIS[\&quot\;Geodetic longitude\&quot\;, EAST], \n"
              + "  AXIS[\&quot\;Geodetic latitude\&quot\;, NORTH], \n"
              + "  AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;4258\&quot\;]], \n"
              + "PROJECTION[\&quot\;Transverse_Mercator\&quot\;, AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;9807\&quot\;]], \n"
              + "PARAMETER[\&quot\;central_meridian\&quot\;, 15.0], \n"
              + "PARAMETER[\&quot\;latitude_of_origin\&quot\;, 0.0], \n"
              + "PARAMETER[\&quot\;scale_factor\&quot\;, 0.9996], \n"
              + "PARAMETER[\&quot\;false_easting\&quot\;, 500000.0], \n"
              + "PARAMETER[\&quot\;false_northing\&quot\;, 0.0], \n"
              + "UNIT[\&quot\;m\&quot\;, 1.0], \n"
              + "AXIS[\&quot\;Easting\&quot\;, EAST], \n"
              + "AXIS[\&quot\;Northing\&quot\;, NORTH], \n"
              + "AUTHORITY[\&quot\;EPSG\&quot\;,\&quot\;25833\&quot\;]]"
        };
    }

    console.log("The " + epsgcode + " is not yet implemented!");

    return {};
}

module.exports = {configureFeaturetype};
