const moment = require("moment");
/**
 * Configure the dataStore attributes.
 * @param {Object} data The data from config-files.
 * @param {String} collectionName The name of the collection.
 * @param {Object} hexValues The hex values for geoserver files.
 * @returns {Object} The configuration for dataStore as json.
 */
function configureDatastore (data, collectionName, hexValues) {
    const config = {
        id: hexValues[collectionName].dataStore,
        name: `${data.connection.name.replaceAll("/", "_")}-${data.collections[collectionName].db_schema}`,
        type: "PostGIS (JNDI)",
        enabled: true,
        workspace: {
            id: hexValues.workspace
        },
        connectionParameters: {
            entry: [
                {
                    "@key": "schema",
                    "#": data.collections[collectionName].db_schema
                },
                {
                    "@key": "Estimated extends",
                    "#": "true"
                },
                {
                    "@key": "fetch size",
                    "#": "1000"
                },
                {
                    "@key": "encode functions",
                    "#": "true"
                },
                {
                    "@key": "Expose primary keys",
                    "#": "false"
                },
                {
                    "@key": "Support on the fly geometry simplification",
                    "#": "true"
                },
                {
                    "@key": "Batch insert size",
                    "#": "1"
                },
                {
                    "@key": "preparedStatements",
                    "#": "false"
                },
                {
                    "@key": "Method used to simplify geometries",
                    "#": "FAST"
                },
                {
                    "@key": "jndiReferenceName",
                    "#": "java:comp/env/" + data.connection.name
                },
                {
                    "@key": "dbtype",
                    "#": "postgis"
                },
                {
                    "@key": "namespace",
                    "#": data.service.name
                },
                {
                    "@key": "Loose bbox",
                    "#": "true"
                }
            ]
        },
        __default: false,
        dateCreated: moment().format("YYYY-MM-DD h:mm:ss.SSS") + " UTC",
        disableOnConnFailure: "false"
    };

    return {"dataStore": config};
}

module.exports = {configureDatastore};
