const {jsonToXml, decodeCanvasDataUrl} = require("../generic_components/parser.js"),
    datastore = require("./templates/datastore.js"),
    featuretype = require("./templates/featuretype.js"),
    layer = require("./templates/layer.js"),
    namespace = require("./templates/namespace.js"),
    style = require("./templates/style.js"),
    workspace = require("./templates/workspace.js");

const GeoserverService = function (data) {
    const genService = require("../generic_components/service.js")(data);

    /**
     * Creates the configuration files for Geoserver services.
     * @returns {Object} The configuration as xml.
     */
    genService.createConfigs = function () {
        const hexValues = this.craeteHexValues(data);
        const jsonConfigs = this.createJsonConfigs(data, hexValues);
        const configs = this.convertJsonToXML(jsonConfigs);

        return configs;
    };

    /**
     * Create hex values for geoserver files.
     * @param {Object} data The data attributes.
     * @returns {Object} The hex values for geoserver files.
     */
    genService.craeteHexValues = function (data) {
        const basicHex = `InfoImpl-${this.stringToHex("GeoServer")}:${this.stringToHex(data.service.name)}:-`;
        const hexValues = {
            namespace: `Namespace${basicHex}${this.stringToHex("namespace")}`,
            workspace: `Workspace${basicHex}${this.stringToHex("workspace")}`,
        };

        this.collectionNames().forEach(collectionName => {
            hexValues[collectionName] = {};
            hexValues[collectionName].dataStore = `DataStore${basicHex}${this.stringToHex(data.collections[collectionName].db_schema)}`,
            hexValues[collectionName].layer = `Layer${basicHex}${this.stringToHex(collectionName)}`;
            hexValues[collectionName].featureType = `FeatureType${basicHex}${this.stringToHex(collectionName)}`;
            hexValues[collectionName].style = `Style${basicHex}${this.stringToHex(collectionName)}`;
        });

        return hexValues;
    };

    /**
     * Create a hex value of a string.
     * @param {String} string The string to convert.
     * @param {Number} length The length of the result string.
     * @returns {String} The hex value.
     */
    genService.stringToHex = function (string) {
        let result = "";

        for (let i = 0; i < string.length; i++) {
            result += string.charCodeAt(i).toString(16);
        }

        return result;
    };

    /**
     * Creates the json configs.
     * @param {Object} data The data attributes.
     * @param {Object} data.connection The connection attributes.
     * @param {Object} data.dataset The dataset attributes.
     * @param {Object[]} data.collections The collections.
     * @param {Object} hexValues The hex values for geoserver files.
     * @returns {Obeject} The json configs.
     */
    genService.createJsonConfigs = function (data, hexValues) {
        const serviceName = data.service.name;
        const jsonConfigs = {};
        const dataConnectionName = data.connection.name.replaceAll("/", "_");

        jsonConfigs[data.service.name + "/namespace.xml"] = namespace.configureNamespace(data, hexValues);
        jsonConfigs[data.service.name + "/workspace.xml"] = workspace.configureWorkspace(data, hexValues);

        this.collectionNames().forEach(collectionName => {
            const datastoreName = `${dataConnectionName}-${data.collections[collectionName].db_schema}`;

            jsonConfigs[`${data.service.name}/${datastoreName}/datastore.xml`] = datastore.configureDatastore(data, collectionName, hexValues);
            jsonConfigs[`${serviceName}/${datastoreName}/${collectionName}/featuretype.xml`] = featuretype.configureFeaturetype(data, collectionName, hexValues);
            jsonConfigs[`${serviceName}/${datastoreName}/${collectionName}/layer.xml`] = layer.configureLayer(data, collectionName, hexValues);
            Object.assign(jsonConfigs, this.createStyleJsonConfigs(data, hexValues, serviceName, collectionName));
        });

        return jsonConfigs;
    };

    /**
     * Creates the style json configs.
     * @param {Object} data The data attributes.
     * @param {Object[]} data.collections The collections.
     * @param {Object} hexValues The hex values for geoserver files.
     * @param {String} serviceName The service name.
     * @param {String} collectionName The collection name.
     * @returns {Obeject} The style json configs.
     */
    genService.createStyleJsonConfigs = function (data, hexValues, serviceName, collectionName) {
        const styleJsonConfigs =  {};
        const styleRules = data.collections[collectionName].style;

        if (styleRules !== null) {
            styleJsonConfigs[`${serviceName}/styles/${collectionName}.xml`] = style.configureStyleXML(data, collectionName, hexValues);
            styleJsonConfigs[`${serviceName}/styles/${collectionName}.sld`] = style.configureStyleSLD(data, collectionName);

            styleRules.forEach(rule => {
                if (rule.geom_type === "polygon" && rule.fill_type === "gemustert") {
                    styleJsonConfigs[`${serviceName}/styles/symbole/${rule.rule_name}_polygon_canvas.png`] = decodeCanvasDataUrl(rule.data_url);
                }
                else if (rule.geom_type === "point" && rule.point_wellknownname === "externalGraphic") {
                    styleJsonConfigs[`${serviceName}/styles/symbole/${rule.file_name_exticon}`] = decodeCanvasDataUrl(rule.data_url_exticon);
                }
            });
        }

        return styleJsonConfigs;
    };

    /**
     * Converts json configs to xml configs.
     * @param {Object} jsonConfigs The json configs.
     * @returns {Object} The xml configs.
     */
    genService.convertJsonToXML = function (jsonConfigs) {
        const xmlConfigs = {};

        Object.keys(jsonConfigs).forEach(key => {
            if (key.toLowerCase().endsWith(".png")) {
                xmlConfigs[key] = jsonConfigs[key];
            }
            else {
                xmlConfigs[key] = jsonToXml(jsonConfigs[key]);
            }
        });

        return xmlConfigs;
    };

    return genService;
};

module.exports = {GeoserverService};
