const deegreeConfigurator = require("./deegree/configurator.js");
const geoserverConfigurator = require("./geoserver/configurator.js");
const ldproxyConfigurator = require("./ldproxy/configurator.js");
const mapserverConfigurator = require("./mapserver/configurator.js");

module.exports = (software) => {
    if (software === "deegree") {
        return deegreeConfigurator();
    }
    if (software === "GeoServer") {
        return geoserverConfigurator();
    }
    if (software === "ldproxy") {
        return ldproxyConfigurator();
    }
    if (software === "MapServer") {
        return mapserverConfigurator();
    }
};
