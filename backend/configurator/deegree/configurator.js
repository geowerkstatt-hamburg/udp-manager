const {deegreeWFS, deegreeWMS} = require("./services.js");

module.exports = () => {
    const genericConfigurator = require("../generic_components/configurator.js");

    genericConfigurator._createService = (data) => {
        if (data.service.type === "wfs" || data.service.type === "wfs-t") {
            return deegreeWFS(data);
        }
        else if (data.service.type === "wms") {
            return deegreeWMS(data);
        }
        else {
            throw new Error(`type ${data.service.type} is not implemented yet`);
        }
    };

    return genericConfigurator;
};
