const {jsonToXml, decodeCanvasDataUrl} = require("../generic_components/parser.js");
const wfs = require("./templates/wfs.js");
const wms = require("./templates/wms.js");
const metadata = require("./templates/metadata.js");
const jdbc = require("./templates/jdbc.js");
const datasources = require("./templates/datasources.js");
const styles = require("./templates/styles.js");

const DeegreeService = function (data) {
    const genService = require("../generic_components/service.js")(data);

    genService.configBlueprint = {
        service: undefined,
        metadata: undefined,
        jdbc: undefined,
        datasources: undefined,
        layers: undefined,
        themes: undefined,
        styles: undefined
    };

    const templates = {
        "wfs": wfs,
        "wms": wms
    };

    genService.template = templates[data.service.type];

    genService.jdbcData = function () {
        return this.connection;
    };

    genService.metaData = function () {
        // resulting metadata file displays each collection as individual "dataset" with it's own namespace
        // not to be confused with the actual dataset
        const datasets = this.collectionNames().map((collectionName) => {
            const {title, alternative_name, namespace} = this.collections[collectionName];

            return {
                collectionName: alternative_name ? alternative_name : collectionName,
                namespace: namespace.namespace,
                namespacePrefix: namespace.namespacePrefix,
                title: title
            };
        });

        return {
            serviceType: this.service.type,
            datasets: datasets,
            title: this.service.title,
            abstract: this.service.description,
            fees: this.dataset.fees,
            accessConstraints: this.dataset.access_constraints,
            inspire: true, // always add extended inspire capabilities as recommended by GDI-DE;
            provider: this.provider.providername,
            position: this.provider.positionname,
            mail: this.provider.electronicmailaddress,
            address: this.provider.deliverypoint,
            city: this.provider.city,
            administrativeArea: this.provider.administrativearea,
            postalCode: this.provider.postalcode,
            country: this.provider.country,
            authority: this.provider.authorityname,
            onlineResource: this.provider.authorityurl,
            language: this.software.inspireSettings.languageCode,
            datasetMetadataId: this.dataset.md_id,
            serviceMetadataId: this.service.md_id,
            datasetResourceId: this.dataset.rs_id,
            catalogueUrl: this.dataset.csw_url
        };
    };

    genService.datasourceData = function (collectionName) {
        const collection = this.collections[collectionName];

        return {
            namespace: collection.namespace.namespace,
            namespacePrefix: collection.namespace.namespacePrefix,
            attributeMap: collection.attributeMap,
            jdbcConnId: this.connection.name,
            featureTypeName: collection.alternative_name || collectionName,
            table: `${collection.db_schema}.${collection.db_table}`,
            featureIdName: collection.primaryKey,
            featureIdType: this.mapDatatype(collection.attributeMap[collection.primaryKey]?.dtype), //may be undefined for complex collections
            geom: collection.mainGeom,
            geomPath: collection.mainGeom,
            srid: this.dataset.srs.split(":")[1],
            epsg: this.dataset.srs
        };
    };

    genService.datasourceID = function (collectionName) {
        return `${this.dataset.shortname}/${collectionName}`;
    };

    genService.createConfigs = function () {
        const configs = {};
        let relPath;

        if (this.configBlueprint.service === true) {
            const serviceConfig = this.template.configureService(this.serviceData());

            relPath = `services/${this.service.name}.xml`;
            configs[relPath] = jsonToXml(serviceConfig);
        }

        if (this.configBlueprint.metadata === true) {
            const metadataConfig = metadata.configureMetadata(this.metaData());

            relPath = `services/${this.service.name}_metadata.xml`;
            configs[relPath] = jsonToXml(metadataConfig);
        }

        if (this.configBlueprint.jdbc === true) {
            const jdbcConfig = jdbc.configureDataSourceConnectionProvider(this.jdbcData());

            relPath = `jdbc/${this.connection.name}.xml`;
            configs[relPath] = jsonToXml(jdbcConfig);
        }

        if (this.configBlueprint.datasources === true) {
            for (let i = 0; i < this.collectionNames().length; i++) {
                const collectionName = this.collectionNames()[i];
                const datasourceData = this.datasourceData(collectionName);
                const config = datasources.configureSQLFeatureStore(datasourceData);

                relPath = `datasources/feature/${this.datasourceID(collectionName)}.xml`;
                configs[relPath] = jsonToXml(config);
            }
        }

        if (this.configBlueprint.layers === true) {
            for (let i = 0; i < this.collectionNames().length; i++) {
                const collectionName = this.collectionNames()[i];
                const layerData = this.layerData(collectionName);
                const config = this.template.configureLayer(layerData);
                const legendDataUrl = layerData.legendDataUrl;
                const legendFileName = layerData.legendFileName;

                relPath = `layers/${this.dataset.shortname}/${collectionName}.xml`;
                configs[relPath] = jsonToXml(config);

                if (legendDataUrl) {
                    relPath = `styles/${this.dataset.shortname}/symbole/${legendFileName}`;
                    configs[relPath] = decodeCanvasDataUrl(legendDataUrl);
                }
            }
        }

        if (this.configBlueprint.styles === true) {
            for (let i = 0; i < this.collectionNames().length; i++) {
                const collectionName = this.collectionNames()[i];
                const {style, use_style_sld, style_sld, style_sld_icons} = this.collections[collectionName];

                if (use_style_sld) {
                    if (style_sld) {
                        relPath = `styles/${this.dataset.shortname}/${collectionName}.xml`;
                        configs[relPath] = style_sld;

                        if (style_sld_icons) {
                            for (const style_sld_icon of style_sld_icons) {
                                relPath = `styles/${this.dataset.shortname}/symbole/${style_sld_icon.file_name}`;
                                configs[relPath] = decodeCanvasDataUrl(style_sld_icon.data_url);
                            }
                        }
                    }
                }
                else {
                    if (style) {
                        const styleData = this.styleData(collectionName);
                        const config = styles.configureFeatureTypeStyle(styleData);

                        relPath = `styles/${this.dataset.shortname}/${collectionName}.xml`;
                        configs[relPath] = jsonToXml(config);

                        for (let j = 0; j < style.length; j++) {
                            const rule = style[j];
                            const usesExternalIcon = Boolean(rule.data_url_exticon);
                            const usesCustomPattern = (rule.geom_type === "polygon") && (rule.fill_type === "gemustert");

                            let dataUrl;

                            if (usesExternalIcon) {
                                relPath = `styles/${this.dataset.shortname}/symbole/${rule.file_name_exticon}`;
                                dataUrl = rule.data_url_exticon;
                            }
                            else if (usesCustomPattern) {
                                relPath = `styles/${this.dataset.shortname}/symbole/${rule.rule_name}_polygon_canvas.png`;
                                dataUrl = rule.data_url;
                            }
                            else {
                                continue;
                            }
                            configs[relPath] = decodeCanvasDataUrl(dataUrl);
                        }
                    }
                }
            }
        }

        if (this.configBlueprint.themes === true) {
            const themeConfig = this.template.configureTheme(this.themeData());

            relPath = `themes/${this.dataset.shortname}.xml`;
            configs[relPath] = jsonToXml(themeConfig);
        }

        return configs;
    };

    return genService;
};

function deegreeWMS (data) {
    const deegreeService = DeegreeService(data);
    
    deegreeService.configBlueprint = {
        service: true,
        metadata: true,
        jdbc: true,
        datasources: true,
        layers: true,
        themes: true,
        styles: true
    };

    deegreeService.serviceData = function () {
        return {
            clickRadius: this.service.clickradius,
            themeId: this.dataset.shortname
        };
    },

    deegreeService.layerData = function (collectionName) {
        const collection = this.collections[collectionName];
        const hasStyle = (collection.style || collection.style_sld) ? true : false;
        const supportedCRS = this.software.supportedApiTypes.wms.availableProjections.join(" ");

        return {
            featureStoreId: `${this.dataset.shortname}/${collectionName}`,
            isGroup: collection.group_object,
            layerName: collection.alternative_name || collectionName,
            layerTitle: collection.title,
            layerCRS: this.dataset.srs,
            envelopeLowerCorner: undefined,
            envelopeUpperCorner: undefined,
            supportedCRS: supportedCRS,
            hasStyle: hasStyle,
            styleStoreId: `${this.dataset.shortname}/${collectionName}`,
            styleName: collection.alternative_name || collectionName,
            legendDataUrl: collection.legend_data_url,
            legendContentType: collection.legend_content_type,
            legendFileName: collection.legend_file_name,
            maxFeatures: this.service.maxfeatures,
            custom_bbox: collection.custom_bbox
        };
    },

    deegreeService.styleData = function (collectionName) {
        const {style, alternative_name} = this.collections[collectionName];

        return {
            featureTypeName: collectionName,
            alternativeFeatureTypeName: alternative_name,
            styleRules: style
        };
    },

    deegreeService.themeData = function () {
        return {
            layers: this.collections,
            groupThemeConfig: this.service.wms_theme_config,
            layerStoreFolder: this.dataset.shortname,
            themeId: this.dataset.shortname,
            serviceTitle: this.service.title,
            supportedCRS: this.software.supportedApiTypes.wms.availableProjections.join(" ")
        };
    }

    return deegreeService;
};

function deegreeWFS (data) {
    const deegreeService = DeegreeService(data);
    
    deegreeService.configBlueprint = {
        service: true,
        metadata: true,
        jdbc: true,
        datasources: true,
        layers: false,
        themes: false,
        styles: false
    };

    deegreeService.serviceData = function () {
        return {
            featureStores: this.collections,
            featureStoreIds: this.collectionNames(),
            shortname: this.dataset.shortname,
            enableTransactions: this.dataset.transactional,
            supportedCRS: this.software.supportedApiTypes.wfs.availableProjections,
            queryMaxFeatures: this.service.maxfeatures,
            additionalOutputFormats: this.service.additionalOutputFormats
        };
    };

    return deegreeService;
};

module.exports = {deegreeWFS, deegreeWMS};