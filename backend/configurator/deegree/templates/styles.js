function configureFeatureTypeStyle(data) {
    const {styleRules, featureTypeName, alternativeFeatureTypeName} = data
    const symbolizerMap = {
        "polygon": _configurePolygonSymbolizer,
        "line": _configureLineSymbolizer,
        "point": _configurePointSymbolizer
    };
    const parsedRules = styleRules.map((rule) => {
        const mainProps = {
            "Name": rule.rule_name,
            "Description": {"Title": rule.title},
            "ogc:Filter": rule.filter_attribute ? _configureFilter(rule) : null,
            "MinScaleDenominator": rule.minscaledenominator,
            "MaxScaleDenominator": rule.maxscaledenominator
        };
        const symbolizer = rule.text_symbolizer ? _configureTextSymbolizer : symbolizerMap[rule.geom_type];
        const symbols = symbolizer(rule);

        return {mainProps, symbols};
    });
    const config = {
        "@xmlns": "http://www.opengis.net/se",
        "@xmlns:de.hh.up": "https://registry.gdi-de.org/id/de.hh.up",
        "@xmlns:xlink": "http://www.w3.org/1999/xlink",
        "@xmlns:ogc": "http://www.opengis.net/ogc",
        "@xmlns:sed": "http://www.deegree.org/se",
        "@xmlns:deegreeogc": "http://www.deegree.org/ogc",
        "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "@xsi:schemaLocation": "http://www.opengis.net/se http://schemas.opengis.net/se/1.1.0/FeatureStyle.xsd http://www.deegree.org/se http://schemas.deegree.org/se/1.1.0/Symbolizer-deegree.xsd",
        "Name": alternativeFeatureTypeName || featureTypeName,
        "Rule": parsedRules
    };

    return {"FeatureTypeStyle": config};
}


function _configureFilter (rule) {
    const config = {"ogc:PropertyName": rule.filter_attribute};

    if (rule.filter_condition === "PropertyIsBetween") {
        config["ogc:LowerBoundary"] = {"ogc:Literal": rule.filter_literal_min};
        config["ogc:UpperBoundary"] = {"ogc:Literal": rule.filter_literal_max};
    }
    else if (rule.filter_condition === "PropertyIsLike") {
        config["@wildCard"] = "*";
        config["@singleChar"] = "#";
        config["@escapeChar"] = "!";
        config["ogc:Literal"] = rule.filter_literal;
    }
    else if (rule.filter_condition === "PropertyIsNull") {
        // do nothing, literal value is not required in this case
    }
    else {
        config["ogc:Literal"] = rule.filter_literal;
    }

    const filterKey = `ogc:${rule.filter_condition}`;

    return {[filterKey]: config};
}


function _configurePolygonSymbolizer (rule) {
    let fill = {};

    if (rule.fill_color_dynamic) {
        fill["SvgParameter"] = [
            {"@name": "fill", "ogc:PropertyName": rule.fill_color_dynamic_attribute},
            {"@name": "fill-opacity", "#": rule.fill_opacity}
        ];
    }
    else if (!rule.fill_color_dynamic && rule.fill_type === "Externes Icon") {
        fill["GraphicFill"] = {
            "Graphic": {
                "ExternalGraphic": {
                    "OnlineResource": {
                        "@xmlns:xlink": "http://www.w3.org/1999/xlink",
                        "@xlink:type": "simple",
                        "@xlink:href": `./symbole/${rule.file_name_exticon}`
                    },
                    "Format": rule.file_content_type_exticon
                },
                "Size": rule.icon_size,
                "Rotation": rule.icon_rotation
            }
        };
    }
    else if (!rule.fill_color_dynamic && rule.fill_type === "gemustert") {
        fill["GraphicFill"] = {
            "Graphic": {
                "ExternalGraphic": {
                    "OnlineResource": {
                        "@xmlns:xlink": "http://www.w3.org/1999/xlink",
                        "@xlink:type": "simple",
                        "@xlink:href": `./symbole/${rule.rule_name}_polygon_canvas.png`
                    },
                    "Format": "image/png"
                },
                "Size": "100"
            }
        };
    }
    else {
        fill = _svgParams({"fill": rule.fill_color_hex, "fill-opacity": rule.fill_opacity});
    }

    let stroke = {};

    if (rule.stroke_color_dynamic) {
        stroke["SvgParameter"] = {"@name": "stroke", "ogc:PropertyName": rule.stroke_color_dynamic_attribute};
    }
    else {
        stroke = _svgParams({
            "stroke": rule.stroke_color_hex,
            "stroke-opacity": rule.stroke_opacity,
            "stroke-width": rule.stroke_width,
            "stroke-linejoin": "round"
        });
    }

    const config = {
        "Geometry": {
            "ogc:PropertyName": rule.geom_name
        },
        "Fill": fill,
        "Stroke": stroke
    };

    return {"PolygonSymbolizer": config};
}


function _configurePointSymbolizer (rule) {
    let fill = {};

    rule.fill_color_dynamic ?
        fill["SvgParameter"] = {
            "@name": "fill", "ogc:PropertyName": rule.fill_color_dynamic_attribute
        } :
        fill = _svgParams({
            "fill": rule.fill_color_hex, "fill-opacity": rule.fill_opacity
        });

    let stroke = {};

    rule.strokecolor_dynamic ?
        stroke["SvgParameter"] = {
            "@name": "stroke", "ogc:PropertyName": rule.stroke_color_dynamic_attribute
        } :
        stroke = _svgParams({
            "stroke": rule.stroke_color_hex,
            "stroke-width": rule.stroke_width,
            "stroke-opacity": rule.stroke_opacity
        });

    const regularGraphic = {
        "Mark": {
            "WellKnownName": rule.point_wellknownname,
            "Fill": fill,
            "Stroke": stroke
        },
        "Size": rule.size,
        "Rotation": rule.icon_rotation
    };

    const externalGraphic = {
        "ExternalGraphic": {
            "OnlineResource": {
                "@xmlns:xlink": "http://www.w3.org/1999/xlink",
                "@xlink:type": "simple",
                "@xlink:href": `./symbole/${rule.file_name_exticon}`
            },
            "Format": rule.file_content_type_exticon
        },
        "Size": rule.size,
        "Rotation": rule.icon_rotation
    };

    const config = {"Geometry": {"ogc:PropertyName": rule.geom_name}};
    const usesExternalGraphic = rule.point_wellknownname === "externalGraphic";

    config["Graphic"] = usesExternalGraphic ? externalGraphic : regularGraphic;

    return {"PointSymbolizer": config};
}


function _configureLineSymbolizer (rule) {
    let stroke = {};
    const stroke_is_dashed = rule.pattern_style === "gestrichelt";

    rule.stroke_color_dynamic ?
        stroke["SvgParameter"] = {
            "@name": "stroke", "ogc:PropertyName": rule.stroke_color_dynamic_attribute
        } :
        stroke = _svgParams({
            "stroke": rule.stroke_color_hex,
            "stroke-width": rule.stroke_width_line,
            "stroke-linecap": "edged",
            "stroke-linejoin": "round",
            "stroke-opacity": rule.stroke_opacity,
            "stroke-dasharray": stroke_is_dashed && `${rule.pattern_dash_array} ${rule.pattern_dash_array}` // short circuit evaluates last expression if first is true
        });

    const config = {
        "Geometry": {"ogc:PropertyName": rule.geom_name},
        "Stroke": stroke};

    return {"LineSymbolizer": config};
}

function _labelPlacement (rule) {
    let labelPlacement = {};

    switch (rule.label_placement) {
        case "Punkt":
            if (rule.anchor_point_x === 0 && rule.anchor_point_y === 0 && rule.displacement_x === 0 && rule.displacement_y === 0) {
                labelPlacement = undefined;
            }
            else {
                labelPlacement["PointPlacement"] = {
                    "AnchorPoint": {
                        "AnchorPointX": rule.anchor_point_x,
                        "AnchorPointY": rule.anchor_point_y
                    },
                    "Displacement": {
                        "DisplacementX": rule.displacement_x,
                        "DisplacementY": rule.displacement_y
                    },
                    "Rotation": rule.rotation
                };
            }
            break;
        case "Linie":
            labelPlacement["LinePlacement"] = {
                "IsRepeated": rule.is_repeated,
                "InitialGap": rule.initial_gap,
                "IsAligned": rule.is_aligned
            };
            break;
        default:
            labelPlacement = undefined;
    }

    return labelPlacement;
}

function _halo (rule) {
    let halo;

    if (rule.halo_radius > 0) {
        halo = {};
        halo["Radius"] = rule.halo_radius;
        halo["Fill"] = _svgParams({"fill": rule.halo_color_hex, "fill-opacity": rule.halo_opacity});
    }

    return halo;
}


function _configureTextSymbolizer (rule) {
    const config = {
        "Geometry": {"ogc:PropertyName": rule.geom_name},
        "Label": {"ogc:PropertyName": `${rule.label_attribute}`},
        "Font": _svgParams({
            "font-family": rule.label_font,
            "font-size": rule.label_font_size,
            "font-style": rule.label_font_style,
            "font-weight": rule.label_font_weight
        }),
        "LabelPlacement": _labelPlacement(rule),
        "Halo": _halo(rule),
        "Fill": _svgParams({
            "fill": rule.fill_color_hex,
            "fill-opacity": rule.fill_opacity
        })
    };

    return {"TextSymbolizer": config};
}


function _svgParams (params) {
    // helper parsing an object of params for xml templating of SVGParameters
    const keys = Object.keys(params).filter(key => params[key] || params[key] === 0); // keep only filled values
    const parsedParams = keys.map((key) => {
        return {"@name": key, "#": params[key]};
    });
    const result = keys ? {"SvgParameter": parsedParams} : undefined;

    return result;
}

module.exports = {configureFeatureTypeStyle};
