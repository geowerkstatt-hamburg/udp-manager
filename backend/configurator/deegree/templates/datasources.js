
function configureSQLFeatureStore (data) {
    const {attributeMap, featureIdName, geom} = data;

    let attributes = Object.keys(attributeMap);

    attributes = attributes.filter(attr => ![featureIdName, geom].includes(attr));
    attributes = attributes.map((key) => {
        return {
            "@mapping": key,
            "@path": `${data.namespacePrefix}:${attributeMap[key].title}`
        };
    });

    let geometry;

    if (geom) {
        geometry = {
            "@mapping": geom,
            "@path": `${data.namespacePrefix}:${data.geomPath}`,
            "StorageCRS": {"@srid": data.srid, "#": data.epsg}
        };
    }

    const config = {
        "@xmlns": "http://www.deegree.org/datasource/feature/sql",
        [`@xmlns:${data.namespacePrefix}`]: data.namespace,
        "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "@configVersion": "3.4.0",
        "@xsi:schemaLocation": "http://www.deegree.org/datasource/feature/sql http://schemas.deegree.org/datasource/feature/sql/3.4.0/sql.xsd",
        "JDBCConnId": data.jdbcConnId,
        "FeatureTypeMapping": {
            "@name": `${data.namespacePrefix}:${data.featureTypeName}`,
            "@table": data.table,
            "FIDMapping": {
                "Column": {"@name": featureIdName, "@type": data.featureIdType}
            },
            "Primitive": attributes,
            "Geometry": geometry
        }
    };

    return {"SQLFeatureStore": config};
}

module.exports = {configureSQLFeatureStore};
