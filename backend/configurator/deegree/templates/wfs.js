function configureService (data) {

    const featureStoreIds = data.featureStoreIds.map((id) => {
        return `${data.shortname}/${id}`;
    });
    const config = {
        "@xmlns": "http://www.deegree.org/services/wfs",
        "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "@configVersion": "3.4.0",
        "@xsi:schemaLocation": "http://www.deegree.org/services/wfs http://schemas.deegree.org/services/wfs/3.4.0/wfs_configuration.xsd",
        "SupportedVersions": {
            "Version": ["1.1.0", "2.0.0"]
        },
        "FeatureStoreId": featureStoreIds,
        "EnableTransactions": data.enableTransactions,
        "QueryCRS": data.supportedCRS,
        "QueryMaxFeatures": data.queryMaxFeatures,
        "GMLFormat": [
            {
                "@gmlVersion": "GML_31",
                "MimeType": [
                    "application/gml+xml; version=3.1",
                    "text/xml; subtype=gml/3.1.1",
                    "text/xml; subtype=\"gml/3.1.1\""
                ]
            },
            {
                "@gmlVersion": "GML_32",
                "MimeType": [
                    "application/gml+xml; version=3.2",
                    "text/xml; subtype=gml/3.2.1",
                    "text/xml; subtype=\"gml/3.2.1\""
                ]
            }
        ]
    };

    if (data.additionalOutputFormats) {
        for (const format of data.additionalOutputFormats) {
            if (format === "csv") {
                config.CsvFormat = {
                    "@delimiter": ";",
                    "MimeType": "text/csv",
                    "ExtraColumns": {
                        "Identifier": "_fid",
                        "CoordinateReferenceSystem": "coordinate_reference_system"
                    }
                };
            }
            if (format === "geojson") {
                config.GeoJSONFormat = {
                    "@allowOtherCrsThanWGS84": "true",
                    "MimeType": "application/geo+json"
                };
            }
        }
    }

    return {"deegreeWFS": config};
}

module.exports = {configureService};
