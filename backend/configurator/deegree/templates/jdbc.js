function configureDataSourceConnectionProvider (data) {
    const config = {
        "@configVersion": "3.4.0",
        "@xmlns": "http://www.deegree.org/connectionprovider/datasource",
        "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "@xsi:schemaLocation": "http://www.deegree.org/connectionprovider/datasource http://schemas.deegree.org/jdbc/datasource/3.4.0/datasource.xsd"
    };

    if (data.name) {
        config.DataSource = {
            "@javaClass": "org.deegree.db.datasource.JndiLookup",
            "@factoryMethod": "lookup",
            "Argument": {"@value": `java:comp/env/${data.name}`, "@javaClass": "java.lang.String"}
        };
    }
    else {
        config.DataSource = {"@javaClass": "org.apache.commons.dbcp2.BasicDataSource"};
        const placeholder = "PLACEHOLDER"; // insert placeholders to avoid leaking secrets
        const properties = {
            "driverClassName": "org.postgresql.Driver",
            "url": `jdbc:postgresql://${placeholder}:${placeholder}/${placeholder}`,
            "username": placeholder,
            "password": placeholder,
            "defaultReadOnly": "false",
            "defaultAutoCommit": "false",
            "maxActive": "10",
            "maxIdle": "20"
        };
        const propertyList = Object.keys(properties).map((key) => {
            return {
                "@name": key,
                "@value": properties[key]
            };
        });

        config.Property = propertyList;
    }

    return {"DataSourceConnectionProvider": config};
}

module.exports = {configureDataSourceConnectionProvider};
