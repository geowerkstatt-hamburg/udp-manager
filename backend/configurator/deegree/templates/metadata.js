function configureMetadata (data) {
    const {serviceType} = data;
    const serviceIdentification = {
        "Title": data.title,
        "Abstract": {"$": data.abstract},
        "Fees": {"$": data.fees},
        "AccessConstraints": {"$": data.accessConstraints}
    };
    const serviceProvider = {
        "ProviderName": data.provider,
        "ProviderSite": {},
        "ServiceContact": {
            "IndividualName": {},
            "PositionName": data.position,
            "Phone": {},
            "Facsimile": {},
            "ElectronicMailAddress": data.mail,
            "Address": {
                "DeliveryPoint": data.address,
                "City": data.city,
                "AdministrativeArea": data.administrativeArea,
                "PostalCode": data.postalCode,
                "Country": data.country
            },
            "OnlineResource": data.onlineResource,
            "HoursOfService": {},
            "ContactInstructions": {},
            "Role": "PointOfContact"
        }
    };

    const externalMetadataSetId = (data.authority && serviceType === "wms") ?
        {"@authority": data.authority, "#": data.datasetResourceId} :
        undefined;

    const datasetList = data.datasets.map((dataset) => {
        const {collectionName, namespace, namespacePrefix, title} = dataset;

        if (data.datasetMetadataId) {
            if (serviceType === "wfs" || serviceType === "wfs-t") {
                return {
                    "Name": {[`@xmlns:${namespacePrefix}`]: namespace, "#": `${namespacePrefix}:${collectionName}`},
                    "Title": title,
                    "MetadataSetId": data.datasetMetadataId,
                    "ExternalMetadataSetId": externalMetadataSetId
                };
            }
            else {
                return {
                    "Name": {[`@xmlns:${namespacePrefix}`]: namespace, "#": `${namespacePrefix}:${collectionName}`},
                    "Title": title,
                    "MetadataSetId": data.datasetMetadataId,
                    "ExternalMetadataSetId": externalMetadataSetId
                };
            }
        }
        else {
            if (serviceType === "wfs" || serviceType === "wfs-t") {
                return {
                    "Name": {[`@xmlns:${namespacePrefix}`]: namespace, "#": `${namespacePrefix}:${collectionName}`},
                    "Title": title
                };
            }
            else {
                return {
                    "Name": {[`@xmlns:${namespacePrefix}`]: namespace, "#": `${namespacePrefix}:${collectionName}`},
                    "Title": title
                };
            }
        }
    });

    const config = {
        "@xmlns": "http://www.deegree.org/services/metadata",
        "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "@configVersion": "3.4.0",
        "@xsi:schemaLocation": "http://www.deegree.org/services/metadata http://schemas.deegree.org/services/metadata/3.4.0/metadata.xsd",
        "ServiceIdentification": serviceIdentification,
        "ServiceProvider": serviceProvider,
        "DatasetMetadata": {
            "MetadataUrlTemplate": data.catalogueUrl + "?Service=CSW&amp;Request=GetRecordById&amp;Version=2.0.2&amp;id=${metadataSetId}&amp;outputSchema=http://www.isotc211.org/2005/gmd&amp;elementSetName=full",
            "ExternalMetadataAuthority": (serviceType === "wms") ? {"@name": data.authority, "#": data.onlineResource} : undefined,
            "Dataset": datasetList
        }
    };

    if (data.inspire && data.serviceMetadataId && ["wms", "wfs", "wfs-t"].includes(serviceType)) {
        const inspireExtensions = {
            "wms": _extendWMSCapabilities,
            "wfs": _extendWFSCapabilities,
            "wfs-t": _extendWFSCapabilities
        };
        const _extend = inspireExtensions[serviceType];

        config.ExtendedCapabilities = _extend(data);
    }

    return {"deegreeServicesMetadata": config};
}


function _extendWFSCapabilities (data) {
    return {
        "@protocolVersions": "2.0.0",
        "ows:ExtendedCapabilities": {
            "@xmlns:inspire_dls": "http://inspire.ec.europa.eu/schemas/inspire_dls/1.0",
            "@xmlns:inspire_common": "http://inspire.ec.europa.eu/schemas/common/1.0",
            "@xmlns:ows": "http://www.opengis.net/ows/1.1",
            "@xsi:schemaLocation": "http://inspire.ec.europa.eu/schemas/common/1.0 http://inspire.ec.europa.eu/schemas/common/1.0/common.xsd http://inspire.ec.europa.eu/schemas/inspire_dls/1.0 http://inspire.ec.europa.eu/schemas/inspire_dls/1.0/inspire_dls.xsd",
            "inspire_common:MetadataUrl": {
                "inspire_common:URL": `${data.catalogueUrl}?Service=CSW&amp;Request=GetRecordById&amp;Version=2.0.2&amp;id=${data.serviceMetadataId}&amp;outputSchema=http://www.isotc211.org/2005/gmd&amp;elementSetName=full`,
                "inspire_common:MediaType": "application/vnd.ogc.csw.GetRecordByIdResponse_xml"
            },
            "inspire_common:SupportedLanguages": {"inspire_common:DefaultLanguage": {"inspire_common:Language": data.language}},
            "inspire_common:ResponseLanguage": {"inspire_common:DefaultLanguage": {"inspire_common:Language": data.language}},
            "inspire_dls:SpatialDataSetIdentifier": data.datasets.map((_) => {
                return {"inspire_common:Code": data.datasetResourceId};
            })
        }
    };
}


function _extendWMSCapabilities (data) {
    return {
        "@protocolVersions": "1.3.0",
        "inspire_vs:ExtendedCapabilities": {
            "@xmlns:inspire_vs": "http://inspire.ec.europa.eu/schemas/inspire_vs/1.0",
            "@xmlns:inspire_common": "http://inspire.ec.europa.eu/schemas/common/1.0",
            "@xsi:schemaLocation": "http://inspire.ec.europa.eu/schemas/common/1.0 http://inspire.ec.europa.eu/schemas/common/1.0/common.xsd http://inspire.ec.europa.eu/schemas/inspire_vs/1.0 http://inspire.ec.europa.eu/schemas/inspire_vs/1.0/inspire_vs.xsd",
            "inspire_common:MetadataUrl": {
                "inspire_common:URL": `${data.catalogueUrl}?Service=CSW&amp;Request=GetRecordById&amp;Version=2.0.2&amp;id=${data.serviceMetadataId}&amp;outputSchema=http://www.isotc211.org/2005/gmd&amp;elementSetName=full`,
                "inspire_common:MediaType": "application/vnd.ogc.csw.GetRecordByIdResponse_xml"
            },
            "inspire_common:SupportedLanguages": {"inspire_common:DefaultLanguage": {"inspire_common:Language": data.language}},
            "inspire_common:ResponseLanguage": {"inspire_common:Language": data.language}
        }
    };
}


module.exports = {configureMetadata};

