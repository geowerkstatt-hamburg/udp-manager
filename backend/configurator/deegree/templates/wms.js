
function configureService (data) {
    const config = {
        "@xmlns": "http://www.deegree.org/services/wms",
        "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "@configVersion": "3.4.0",
        "@xsi:schemaLocation": "http://www.deegree.org/services/wms http://schemas.deegree.org/services/wms/3.4.0/wms_configuration.xsd",
        "ServiceConfiguration": {
            "DefaultLayerOptions": {
                "FeatureInfoRadius": data.clickRadius
            },
            "ThemeId": data.themeId
        },
        "FeatureInfoFormats": {
            "GetFeatureInfoFormat": {
                "File": "html_einfach.gfi",
                "Format": "text/html"
            }
        }
    };

    return {"deegreeWMS": config};
}


function configureLayer (data) {
    const featureLayer = {
        "l:Name": data.layerName,
        "d:Title": data.layerTitle,
        "d:Abstract": data.layerTitle
    };

    if (data.custom_bbox) {
        const {custom_bbox} = data;

        featureLayer["s:Envelope"] = {
            "@crs": data.layerCRS,
            "s:LowerCorner": `${custom_bbox.xmin} ${custom_bbox.ymin}`,
            "s:UpperCorner": `${custom_bbox.xmax} ${custom_bbox.ymax}`
        };
    }

    featureLayer["s:CRS"] = data.supportedCRS;

    if (data.hasStyle) {
        featureLayer["l:StyleRef"] = {"l:StyleStoreId": data.styleStoreId};
    }

    if (data.legendDataUrl) {
        featureLayer["l:StyleRef"]["l:Style"] = {
            "l:StyleName": data.styleName,
            "l:LayerNameRef": data.layerName,
            "l:StyleNameRef": data.styleName,
            "l:LegendGraphic": {
                "@outputGetLegendGraphicUrl": "true",
                "#": `./symbole/${data.legendFileName}`
            }
        };
    }

    featureLayer["l:LayerOptions"] = {"l:MaxFeatures": data.maxFeatures};

    const config = {
        "@xmlns": "http://www.deegree.org/layers/feature",
        "@xmlns:d": "http://www.deegree.org/metadata/description",
        "@xmlns:l": "http://www.deegree.org/layers/base",
        "@xmlns:s": "http://www.deegree.org/metadata/spatial",
        "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "@configVersion": "3.4.0",
        "@xsi:schemaLocation": "http://www.deegree.org/layers/feature http://schemas.deegree.org/layers/feature/3.4.0/feature.xsd",
        "FeatureStoreId": data.featureStoreId,
        "FeatureLayer": featureLayer

    };

    return {"FeatureLayers": config};
}


function configureTheme (data) {
    const {layers, layerStoreFolder, groupThemeConfig} = data;
    const layerStoreIds = Object.keys(layers)
        .filter(store => layers[store].group_object === false)
        .map((store) => {
            return `${layerStoreFolder}/${store}`;
        });

    let themes = [];

    if (groupThemeConfig) {
        themes = _parseGroupThemes(groupThemeConfig, layerStoreFolder);
    }
    else {
        themes = Object.keys(layers).map((theme) => {
            const layer = layers[theme];
            const layerName = layer.alternativeName || theme;

            return {
                "Identifier": layerName,
                "d:Title": layer.title,
                "Layer": {"@layerStore": `${layerStoreFolder}/${theme}`, "#": layerName}
            };
        });
    }

    const config = {
        "@xmlns": "http://www.deegree.org/themes/standard",
        "@xmlns:d": "http://www.deegree.org/metadata/description",
        "@xmlns:s": "http://www.deegree.org/metadata/spatial",
        "@xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        "@configVersion": "3.4.0",
        "@xsi:schemaLocation": "http://www.deegree.org/themes/standard http://schemas.deegree.org/themes/3.4.0/themes.xsd",
        "LayerStoreId": layerStoreIds,
        "Theme": {
            "Identifier": data.themeId,
            "d:Title": data.serviceTitle,
            "d:Abstract": data.serviceTitle,
            "s:CRS": data.supportedCRS,
            "Theme": themes
        }
    };

    return {"Themes": config};
}


function _parseGroupThemes (groupThemeConfig, layerStoreFolder) {

    const {children} = groupThemeConfig;
    const keysToRemove = ["id", "type", "leaf", "parentId", "lastParentId", "children", "name", "title"];

    for (const obj of children) {
        // derive new keys and values from existing entries
        obj["Identifier"] = obj["name"];
        obj["d:Title"] = obj["title"];
        if (obj.leaf) {
            const key = obj["Identifier"];

            obj["Layer"] = {"@layerStore": `${layerStoreFolder}/${key}`, "#": key};
        }

        // if object has children, apply function recursively
        if (obj.children) {
            obj["Theme"] = obj["children"];
            _parseGroupThemes(obj, layerStoreFolder);
        }

        // delete all keys not needed in final output
        keysToRemove.forEach(e => delete obj[e]);

    }

    return children;
}


module.exports = {configureService, configureTheme, configureLayer};
