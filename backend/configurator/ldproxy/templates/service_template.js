function configureService (data) {
    const csw_url = data.dataset.csw_url;
    const show_doc_url = data.dataset.show_doc_url;

    // To be extended in the future to add additional APIs
    // Alternatively desired APIs can be enabled for all datasets via defaults in LDproxy
    const api = [
        {
            "buildingBlock": "COLLECTIONS",
            "additionalLinks": [
            {
                "rel": "describedby",
                "type": "text/html",
                "title": "Metadaten zum Datensatz",
                "href": `${show_doc_url}${data.dataset.md_id}`
            },
            {
                "rel": "describedby",
                "type": "application/xml",
                "title": "Metadaten zum Datensatz (XML)",
                "href": `${csw_url}?Service=CSW&Request=GetRecordById&Version=2.0.2&id=${data.dataset.md_id}&outputSchema=http://www.isotc211.org/2005/gmd&elementSetName=full`
            }
            ]
        }
    ];
    
    // Interate through all collections
    let collections = Object.keys(data.collections).map((collection) => {
        // Define empty lists for queryables and sortables
        let properties = [];

        // Iterate through attribute map and keys
        const attributeMap = data.collections[collection].attributeMap;

        Object.keys(attributeMap).map((key) => {
            const property_title= attributeMap[key].title.split(":").pop();

            // Add field names that match criteria to queryables list
            if(attributeMap[key].dtype.type === "STRING") {
                properties.push(property_title);
            } 
        })

        properties = {
            properties
        };

        const collection_title = data.collections[collection].title;
        const gid = data.collections[collection].primaryKey;
        const table = data.collections[collection].db_table;
        const schema = data.collections[collection].db_schema;
        
        if (table === null || schema === null || gid === null){
            return null;
        }
        else {
            return {
                [collection] : {
                    "id" : collection,
                    "label": collection_title,
                    "enabled": true,
                    "api": [
                        {
                        "buildingBlock": "TEXT_SEARCH",
                        "enabled": true,
                        ...properties
                        }         
                    ]
                }
            };
        }
    });

    collections = Object.assign({},...collections);

    // BEGIN MD DESCRIPTION SHORTENER
    // This section handles the shortening of the MD description and is mostly enherited from Andreas"s script

    const _note_text = `\r\n[...]\r\nEine vollständige Beschreibung des Datensatzes befindet sich <a href="${show_doc_url}${data.dataset.md_id}">hier</a>.`;
    const _EXCLUDE_WORDS = ["ca.", "bzw.","(z.B.", "(z. B.","z.B.","z. B.", "usw.", "bspw.", "(bspw."];
    const _EXCLUDE_ENUMS = [];
    const hard_cutoff_strings = ["Anmerkungen zu den Echtzeitdienst:", "Weitere Informationen zum Echtzeitdienst:"];

    for (let i = 0; i <= 100; i++) {
        _EXCLUDE_ENUMS.push(`${i}.`);
    }

    const exclude = _EXCLUDE_WORDS.concat(_EXCLUDE_ENUMS);
    const max_words = 250;

    // split text at all whitespace which is not newline or tab
    const delims_regex = /[^\S\n\t]+/;

    if (data.dataset.description === null) {
        data.dataset.description = "Dies ist ein Platzhalter da noch keine Datensatzbeschreibung existiert";
    }

    const words = data.dataset.description?.split(delims_regex);
    let contains_cutoff = false

    // Cut off at given string
    hard_cutoff_strings.forEach((string) => {
        if ( data.dataset.description?.includes(string)) {
            const hard_break_ix =  data.dataset.description?.indexOf(string);

            data.dataset.description = (data.dataset.description?.slice(0,hard_break_ix)).concat(_note_text);
            contains_cutoff = true;
        }
    });

    // Shorten description
    if (words?.length > max_words && contains_cutoff === false) {
        // get list of all words with a period, exclude with slashes
        const period_words = [];

        words.forEach((word) => {
            if (word.includes(".")) {
                period_words.push(word);
            }
        });      

        // get array indices
        const period_ix = [];

        period_words.forEach((l) => {
            if (exclude.includes(l) != true) {
                period_ix.push(words.indexOf(l));
            }
        });
      
        // get the largest index of words ending in period 
        const words_under_max = [];

        period_ix.forEach((x) => {
            if (x <= max_words) {
                words_under_max.push(x);
            }
        });

        const last_word_index = words_under_max.slice(-1)[0];
        const new_description = words.slice(0,last_word_index+1).join(" ");

        data.dataset.description = new_description.concat(_note_text);
    }
    else{
        data.dataset.description = data.dataset.description;
    }
    // END MD DESCRIPTION SHORTENER

    const config = {
        "id": data.service.name,
        "entityStorageVersion": 2,
        "label": data.service.title.replaceAll("OAF ",""),
        "description" : data.dataset.description,
        "enabled" : true,
        "serviceType": "OGC_API",
        "externalDocs" : {
          "url": `${show_doc_url}${data.dataset.md_id}`,
          "description": "Metadaten zum Datensatz"
        },
        "metadata": {
          "contactName": data.provider.providername,
          "contactUrl": data.provider.onlineresource,
          "contactEmail": data.provider.electronicmailaddress,
          "contactPhone": null,
          "licenseName": data.dataset.fees_name,
          "licenseUrl": data.dataset.fees_url,
          "attribution": data.dataset.fees_quelle
          //"keywords" : [null]  
        },
        "api" : api,
        collections
      }

    return config;
}

module.exports = {configureService};