// The template works by first gathering all the required sections of the config into objects (nativeCrs, connectionInfo, types)
// And at the end put together into a config object that is then returned
function configureProvider (data) {
    const nativeCrs = {
        "code" : 25832,
        "forceAxisOrder" : "NONE"
    };

    // Convert plaintext PWs to env variables (env variables cannot contain the symbol "/")
    const pw = data.connection.name.replaceAll("/", "_");

    // Convert hardcoded host names to env_vars
    // split from first "-" to seperatee from test-, qs-, and prod-
    const host_var = data.connection.host.replaceAll(".", "_").split("-");

    const connectionInfo = {
        "connectorType": "SLICK",
        "dialect": "PGIS",
        "database": data.connection.database,
        "host": "${" + host_var[1] + "}",
        "user": data.connection.db_user,
        "password": "${" + pw + "}"
    };
    
    // get only keys and iterate over collection names
    let types = Object.keys(data.collections).map((collection) => {
        const collection_title = data.collections[collection].title;
        const collection_description = data.collections[collection].description;
        const attributeMap = data.collections[collection].attributeMap;
        const gid = data.collections[collection].primaryKey;
        const geom_name = data.collections[collection].mainGeom;
        const table = data.collections[collection].db_table;
        const schema = data.collections[collection].db_schema;

        // Generate sourcePath string: requires a sortKey and primartyKey to override defaults: 
        // https://docs.ldproxy.net/providers/feature/10-sql.html#source-path-defaults
        const sourcePath = `/${schema}.${table}{sortKey=${gid}}{primaryKey=${gid}}`;

        // Get attributes (aka properties) from collections
        const attributeMapArray = Object.keys(attributeMap).map((key) => {
            // Use portal name if available, if not default to API name
            // defined here: backend\utils\configuration_data_gen.util.js
            const property_title= attributeMap[key].title;
            const property_label= attributeMap[key].label;

            // the object "key" corresponds with the database name of the attribute/property
            const type = attributeMap[key].dtype;
            const description = attributeMap[key].description;
            const unit = attributeMap[key].unit;

            // bring all the sections together for the types object
            return {
                [property_title] : {
                    "sourcePath" : key,
                    "label" : property_label,
                    "description": description,
                    "unit": unit,
                    ...type
                }
            }
        });           

        // Geometry and GID tags need to be handled slightly differently to normal properties
        // These sections could certainly be refactored to be less redundant

        // Convert list of types to an object
        const attributeMapObj = Object.assign({},...attributeMapArray);
    
        // Get geom
        // Default to null for cases where there is no geom
        let geom_tag = null;

        if (attributeMap[geom_name]) {
            // Split pop to avoid namespaces
            const geom_title= attributeMap[geom_name].title;
            const geom_label= attributeMap[geom_name].label;
            geom_tag = {
                [geom_title] : {
                    "sourcePath" : geom_name,
                    // This may be overenginereed: I believe a simple hard-coded "GEOMETRY" is perfectly fine here
                    "type" : attributeMap[geom_name].dtype.type,
                    "label" : geom_label,
                    "description": attributeMap[geom_name].description,
                    "unit": attributeMap[geom_name].unit,
                    "role" :  "PRIMARY_GEOMETRY",
                    "geometryType" : attributeMap[geom_name].dtype.geometryType
                    // To solve UDH-872 addition tags will be needed here
                    // https://lgv-hamburg.atlassian.net/browse/UDH-872
                    //      constraints:
                    //          required: true
                }
            };
        } 

        let gid_tag = null;

        if (data.collections[collection].primaryKey != null) {
            try {
                const gid_title= attributeMap[gid].title;
                const gid_label= attributeMap[gid].label;
                gid_tag = {[gid_title] : {
                    "sourcePath" : gid,
                    "label" : gid_label,   
                    "description": attributeMap[gid].description,
                    "unit": attributeMap[gid].unit,            
                    ...attributeMap[gid].dtype,
                    "role" : "ID"
                    }
                };
            }
            catch (error) {
                console.log("Error!", collection, error);
            }
        }
        else {
            //console.log(collection,"Contains no GID!!")
        }

        if (table === null || schema === null || gid === null) {
            //console.log(collection, "Contains no schema or table info")
            return null;
        }
        else {
            // Return types object (contains all info for our collections)
            return {
                [collection] : {
                    "label": collection_title,
                    "description": collection_description,
                    "sourcePath" : sourcePath,
                    "type" : "OBJECT",
                    "properties" : {
                        ...attributeMapObj,
                        ...gid_tag,
                        ...geom_tag
                    }
                }
            };
        }
    });

    //Convert list of types to an object
    types = Object.assign({},...types);

    // Gather all elements for our config file
    const config = {
        "id" : data.service.name,
        "entityStorageVersion": 2,
        "providerType": "FEATURE",
        "featureProviderType": "SQL",
        nativeCrs,
        connectionInfo,
        types
    };

    return config;
}

module.exports = {configureProvider};
