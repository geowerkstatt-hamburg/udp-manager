const provider = require("./templates/provider_template.js");
const service = require("./templates/service_template.js");
const YAML = require("yaml");

function ldproxyOAF (data) {
    const genService = require("../generic_components/service.js")(data);

    genService.createConfigs = function () {
        // initiate result object holding all configuration files by filepath
        const configs = {};
        let serviceConfig = {};
        let providerConfig = {};
        const providerPath = `store/entities/providers/${this.service.name}.yaml`;
        const servicePath = `store/entities/services/${this.service.name}.yaml`;

        providerConfig = provider.configureProvider(this);
        configs[providerPath] = YAML.stringify(providerConfig);
        serviceConfig = service.configureService(this);
        configs[servicePath] = YAML.stringify(serviceConfig);

        return configs;
    };

    return genService;
}

module.exports = {ldproxyOAF};
