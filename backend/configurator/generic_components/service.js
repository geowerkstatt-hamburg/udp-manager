module.exports = (data) => ({
    service: data.service,
    software: data.software,
    dataset: data.dataset,
    collections: data.collections,
    connection: data.connection,
    provider: data.provider,

    collectionNames: function () {
        const names = [];

        for (const collection of Object.values(this.collections)) {
            if (!collection.group_object) {
                names.push(collection.name);
            }
        }

        return names;
    },

    collectionGroupNames: function () {
        const groups = [];

        for (const collection of Object.values(this.collections)) {
            if (collection.group_object) {
                groups.push(collection.name);
            }
        }

        return groups;
    },

    mapDatatype: function (dtype) {
        return this.software.dataTypeMap[dtype];
    },

    createConfigs: function () {
        throw new Error("Abstract method, has to be implemented!");
    }
});
