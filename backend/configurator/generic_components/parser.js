// import { create } from 'xmlbuilder2';
const xmlbuilder2 = require("xmlbuilder2");

function jsonToXml (json) {
    const xml = xmlbuilder2.create().ele(json);

    return xml.end({prettyPrint: true});
}

function decodeCanvasDataUrl(dataUrlString) {
    // expects a base64 encoded data url
    // documentation: https://developer.mozilla.org/en-US/docs/Web/API/HTMLCanvasElement/toDataURL
    return new Buffer.from(dataUrlString.split("base64,")[1], "base64");
}

module.exports = {jsonToXml, decodeCanvasDataUrl};
