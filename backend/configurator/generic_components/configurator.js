const fs = require("fs");
const path_module = require("path");
const Logger = require("../../utils/logger.util");

module.exports = {
    storeService: function (data) {
        const service = this._createService(data);
        const configs = service.createConfigs();
        let basePath = `${data.outPath}/configs/dev/${data.service.software}/${data.service.id}`;

        if (data.service.folder) {
            basePath = `${data.outPath}/configs/dev/${data.service.software}/${data.service.id}/${data.service.folder}`;
        }

        const file_paths = [];

        Object.entries(configs).forEach(([path, config]) => {
            const skipFile = data.configsEdited.find((conf) => conf.uri === path);

            if (!skipFile) {
                const fpath = `${basePath}/${path}`;
                const dirname = path_module.dirname(fpath);

                fs.mkdirSync(dirname, {recursive: true}, (err) => {
                    if (err) {
                        throw err;
                    }
                });
                fs.writeFileSync(fpath, config);
                file_paths.push(fpath);
                Logger.debug(`written file ${fpath}`);
            }
        });
        return file_paths;
    },

    _createService: function (data) {
        // abstract methods: https://medium.com/@yuribett/javascript-abstract-method-with-es6-5dbea4b00027
        throw new Error("Abstract factory method, has to be implemented!");
    }
};
