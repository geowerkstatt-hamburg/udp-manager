
const {decodeCanvasDataUrl} = require("../generic_components/parser.js");
// const mapfile_template = require("./templates/mapfile_template.txt");
const mapfile_data = require("./templates/mapfile_template.js");
const fs = require("fs");
const _ = require("underscore");
const path = require("path");

function mapserverWMS(data) {
    const genService = require("../generic_components/service.js")(data);

    genService.createConfigs = function () {
        // initiate result object holding all configuration files by filepath
        const configs = {};
        const mapfilePath = `${this.service.name}/${this.service.name}.map`;

        // get the number of the collections and their names
        const collection_num = Object.keys(this.collections).length;
        const collection_name = Object.keys(this.collections);

        // get group theme config
        const group_config = this.service.wms_theme_config;

        // fill the mapfile template
        const read_template_file = fs.readFileSync(path.join(__dirname, "../../../backend/configurator/mapserver/templates/mapfile_template.txt"), "utf8");
        const template = _.template(read_template_file);
        let template_mapfile = template(mapfile_data.configureMapfile(this));

        // append the layers to the mapfile
        for (let i = 0; i < collection_num; i++) {
            const layer_data = mapfile_data.configureMapfileLayer(this, collection_name[i]);

            if (!layer_data.isGroup) {
                const read_template_layer = fs.readFileSync(path.join(__dirname, "../../../backend/configurator/mapserver/templates/mapfile_layer_template.txt"), "utf8");
                const template_layer = _.template(read_template_layer);

                layer_data.group_name = null;

                if (group_config) {
                    const group_layer = group_config.children[0].children.find((layer) => layer.name === layer_data.layer_name);

                    if (group_layer) {
                        layer_data.group_name = group_config.children[0].name;
                    }
                }

                const append_content_template_layer = template_layer(layer_data);
                const legendDataUrl = layer_data.legendDataUrl;
                const legendFileName = layer_data.legendFileName;

                if (legendDataUrl) {
                    const relPath = `${this.service.name}/legends/${legendFileName}`;

                    configs[relPath] = decodeCanvasDataUrl(legendDataUrl);
                }

                template_mapfile += append_content_template_layer;
            }
        }

        configs[mapfilePath] = template_mapfile + "END # Map File";

        return configs;
    };

    return genService;
}

module.exports = {mapserverWMS};
