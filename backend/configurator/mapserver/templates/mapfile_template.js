function configureMapfile (data) {
    const template_data = {
        mapfile_name: data.service.name,
        shapepath: data.software.dev.softwareSpecificParameter.shapePath,
        projection: "init=" + data.dataset.srs,
        extent: data.dataset.extent,
        wms_title: data.service.title,
        wms_abstract: data.service.description,
        wms_srs: data.software.supportedApiTypes.wms.availableProjections.join(" "),
        wms_accessconstraints: data.service.access_constraints,
        address: data.provider.deliverypoint,
        city: data.provider.city,
        administrativeArea: data.provider.administrativearea,
        postalCode: data.provider.postalcode,
        country: data.provider.country,
        mail: data.provider.electronicmailaddress,
        organization: data.provider.providername,
        fees: data.service.fees,
        wms_inspire_metadataurl_href: data.dataset.csw_url + "?REQUEST=GetRecordById&SERVICE=CSW&VERSION=2.0.2&id=" + data.service.md_id + "&iplug=/ingrid-group:ige-iplug-HH&elementSetName=full"
    };

    return template_data;
}

function configureMapfileLayer (data, collection_name) {
    const template_data = {
        isGroup: data.collections[collection_name].group_object,
        layer_name: collection_name,
        layer_wms_title: data.collections[collection_name].title,
        layer_wms_abstract: data.collections[collection_name].description,
        wms_srs: data.software.supportedApiTypes.wms.availableProjections.join(" "),
        layer_wms_metadataurl_href: data.dataset.csw_url + "?REQUEST=GetRecordById&SERVICE=CSW&VERSION=2.0.2&id=" + data.dataset.md_id + "&iplug=/ingrid-group:ige-iplug-HH&elementSetName=full",
        authority: data.provider.authorityname,
        homepage: data.provider.authorityurl,
        rs_id: data.dataset.rs_id,
        tileindex: data.collections[collection_name].tileindex,
        extent: data.dataset.extent,
        projection: "init=" + data.dataset.srs,
        layerLegendUrl: undefined,
        legendDataUrl: data.collections[collection_name].legend_data_url,
        legendContentType: data.collections[collection_name].legend_content_type,
        legendFileName: data.collections[collection_name].legend_file_name
    };

    return template_data;
}

module.exports = {configureMapfile, configureMapfileLayer};
