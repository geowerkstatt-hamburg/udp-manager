const {mapserverWMS} = require("./services.js");

module.exports = () => {
    const genericConfigurator = require("../generic_components/configurator.js");

    genericConfigurator._createService = (data) => {
        if (data.service.type === "wms" || data.service.type === "wms-time") {
            return mapserverWMS(data);
        }
        else {
            throw new Error(`type ${data.service.type} is not implemented yet`);
        }
    };

    return genericConfigurator;
};
