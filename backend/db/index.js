const pgPromise = require("pg-promise"); // pg-promise core library
const config = require("../config.js").getConfig(); // db connection details
const {diagnostics} = require("./diagnostics");
const {UDPMRepo, ExtInstanceRepo} = require("./repos");
const connection_db = config.database.udp_manager;

// pg-promise initialization options:
const initOptionsUDPMRepo = {
    extend (obj) {
        obj.exec = new UDPMRepo(obj, pgp_udpm);
    }
};

// Initializing the library:
const pgp_udpm = pgPromise(initOptionsUDPMRepo);

// Creating the database instance:
const db_udpm = pgp_udpm(connection_db);

// Initializing optional diagnostics:
if (process.env.DB_DIAGNOSTICS === "true") {
    diagnostics(initOptionsUDPMRepo);
}

// pg-promise initialization options:
const initOptionsExtInstanceRepo = {
    extend (obj) {
        obj.exec = new ExtInstanceRepo(obj, pgp_ext);
    }
};

// Initializing the library:
const pgp_ext = pgPromise(initOptionsExtInstanceRepo);

// Creating dummy db connection:
const db_ext = pgp_ext({
    host: "localhost",
    port: 5432,
    database: "udp_manager",
    user: "postgres",
    password: "Postgres",
    max: 1,
    idleTimeoutMillis: 300000000,
    connectionTimeoutMillis: 20000000
});


module.exports = {db_udpm, db_ext};
