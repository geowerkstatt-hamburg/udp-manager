const os = require("os");
const monitor = require("pg-monitor");
const Logger = require("../utils/logger.util");
const env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

// source: https://github.com/vitaly-t/pg-promise-demo

monitor.setTheme("matrix"); // changing the default theme;

monitor.setLog((msg, info) => {

    // Only "error" events will be logged in prod environemnt

    if (info.event === "error") {
        let logText = os.EOL + msg;

        if (info.time) {
            // If it is a new error being reported,
            // and not an additional error line;
            logText = os.EOL + logText; // add another line break in front;
        }
        Logger.error(logText);
    }

    if (env !== "dev") {
        // If it is not a dev environment:
        info.display = false; // display nothing;
    }

});

function diagnostics (options) {
    if (env === "dev") {
        // In DEV environment, attach to events:
        monitor.attach(options, ["query", "task", "transact", "error"]);
    }
    else {
        // In PROD environment, attach only to error events:
        monitor.attach(options, ["error"]);
    }
}

module.exports = {diagnostics};
