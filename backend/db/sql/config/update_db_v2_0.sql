SET search_path = config, public;
DELETE FROM db_version;
INSERT INTO db_version (version, implementation_date) VALUES ('2.0', now());

CREATE TABLE app
(
    ldap json,
    jira json,
    mail json,
    capabilities_metadata json,
    test_bbox json,
    service_types json,
    modules json,
    server json,
    client_config json,
    git json,
    software json,
	defaultbbox text,
	available_projections json,
    layer_attributes_excludes json
)
;

CREATE TABLE fme_server
(
    id serial,
    name text,
    host text,
    port integer,
    database text,
    schema text,
    password text,
    db_user text,
    CONSTRAINT pk_config_fme_server PRIMARY KEY (id)
)
;

CREATE TABLE webdav
(
    id serial,
    base_url text,
    directory text,
    full_qualified_domain text,
    username text,
    password text,
    proxy boolean,
    name text,
    CONSTRAINT pk_config_webdav PRIMARY KEY (id)
)
;

INSERT INTO app(ldap, jira, mail, capabilities_metadata, test_bbox, service_types, modules, server, client_config, git, software, defaultbbox, available_projections, layer_attributes_excludes) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}','', '[]', '[]');

ALTER TABLE elastic DROP COLUMN server;

SET search_path = data, public;
CREATE TABLE datasets_version_log
(
    dataset_id integer,
    editor text,
    "timestamp" timestamp without time zone,
    data json
)
;

ALTER TABLE layers DROP COLUMN status;
ALTER TABLE layers DROP COLUMN restriction_level;
ALTER TABLE layers ADD COLUMN json_intranet_prod json;
ALTER TABLE layers ADD COLUMN json_internet_prod json;
ALTER TABLE layers ADD COLUMN json_intranet_es_prod json;
ALTER TABLE layers ADD COLUMN json_internet_es_prod json;

ALTER TABLE services ADD COLUMN status_dev boolean DEFAULT true;
ALTER TABLE services ADD COLUMN status_stage boolean DEFAULT false;
ALTER TABLE services ADD COLUMN status_prod boolean DEFAULT false;
ALTER TABLE services ADD COLUMN server_prod text;
ALTER TABLE services ADD COLUMN folder_prod text;
ALTER TABLE services ADD COLUMN url_ext_prod text;
ALTER TABLE services ADD COLUMN url_int_prod text;
ALTER TABLE services ADD COLUMN disable_config_module boolean DEFAULT false;
ALTER TABLE services ADD COLUMN disable_config_management boolean DEFAULT false;
UPDATE services SET type = 'STA' WHERE type = 'SensorThings';
UPDATE services SET status_dev = true WHERE status = 'dev' OR status = 'test';
UPDATE services SET status_dev = false, status_stage = true, status_prod = true WHERE status = 'stage';
UPDATE services SET status_dev = false, status_stage = false, status_prod = true WHERE external = true;

UPDATE services SET url_ext_prod = url_ext WHERE external = true;

ALTER TABLE services DROP COLUMN status;

ALTER TABLE datasets ADD COLUMN publish_date timestamp without time zone;

UPDATE datasets SET status = 'veröffentlicht' WHERE id IN (
SELECT sl.dataset_id FROM services s 
LEFT JOIN service_links sl ON s.id=sl.service_id
WHERE s.status_prod = true AND s.status_dev = false
);

UPDATE datasets SET status = 'in Bearbeitung' WHERE id IN (
SELECT sl.dataset_id FROM services s 
LEFT JOIN service_links sl ON s.id=sl.service_id
WHERE s.status_dev = true 
);

UPDATE datasets SET status = 'veröffentlicht' WHERE external = true;

UPDATE datasets SET status = 'in Bearbeitung' WHERE status = 'dev';

ALTER TABLE collections ADD COLUMN store_type text;

CREATE TABLE source_db_connections
(
	id serial NOT NULL,
	name text NOT NULL,
	dbms text NOT NULL,
	host text NULL,
	port integer NULL,
	database text NULL,
	password text NULL,
	db_user text NULL
)
;

ALTER TABLE source_db_connections ADD CONSTRAINT pk_source_db_connections
	PRIMARY KEY (id)
;

ALTER TABLE source_db_connections 
  ADD CONSTRAINT unique_source_db_connection_name UNIQUE (name)
;

ALTER TABLE collections ADD COLUMN source_data_type text;
ALTER TABLE collections ADD COLUMN source_data_location text;
ALTER TABLE collections ADD COLUMN source_data_name text;
ALTER TABLE collections ADD COLUMN source_data_table text;
ALTER TABLE collections ADD COLUMN source_data_schema text;
ALTER TABLE collections ADD COLUMN source_data_query text;
ALTER TABLE collections ADD COLUMN source_db_connection_id integer;

ALTER TABLE collections
    ADD CONSTRAINT fk_collections_source_db_connection_id FOREIGN KEY (source_db_connection_id)
    REFERENCES source_db_connections (id) MATCH SIMPLE
    ON UPDATE NO ACTION
    ON DELETE NO ACTION;

UPDATE portals SET url = replace(url, host, '');

ALTER TABLE datasets ADD COLUMN md_contact_mail text;