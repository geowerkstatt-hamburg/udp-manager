SELECT id, name, description, to_char(last_run, 'YYYY-MM-DD HH24:MI'::text) AS last_run, message, status, schedule, enabled, execute_on_startup
	FROM config.scheduled_tasks WHERE name = ${task_name};