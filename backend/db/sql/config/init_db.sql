CREATE SCHEMA IF NOT EXISTS config;

SET search_path = config, public;

CREATE TABLE db_version
(
	version text NULL,
	implementation_date timestamp without time zone NULL
)
;

CREATE TABLE app
(
    ldap json,
    jira json,
    mail json,
    capabilities_metadata json,
    test_bbox json,
    service_types json,
    modules json,
    server json,
    client_config json,
    git json,
    software json,
	defaultbbox text,
	available_projections json,
	layer_attributes_excludes json
)
;

CREATE TABLE fme_server
(
    id serial,
    name text,
    host text,
    port integer,
    database text,
    schema text,
    password text,
    db_user text,
    CONSTRAINT pk_config_fme_server PRIMARY KEY (id)
)
;

CREATE TABLE webdav
(
    id serial,
    base_url text,
    directory text,
    full_qualified_domain text,
    username text,
    password text,
    proxy boolean,
    name text,
    CONSTRAINT pk_config_webdav PRIMARY KEY (id)
)
;

CREATE TABLE ext_instances
(
	id serial NOT NULL,
	name text NULL,
	host text NULL,
	port integer NULL,
	database text NULL,
	password text NULL,
	db_user text NULL
)
;

CREATE TABLE scheduled_tasks
(
	id serial NOT NULL,
	name text NULL,
	description text NULL,
	last_run timestamp without time zone NULL,
	message text NULL,
	status text NULL,
	schedule text NULL,
	enabled boolean NULL,
	execute_on_startup boolean NULL,
	is_public boolean NULL
)
;

CREATE TABLE elastic
(
    settings json,
    mappings json,
    indexes json
)
;

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE ext_instances ADD CONSTRAINT pk_config_ext_instances
	PRIMARY KEY (id)
;

ALTER TABLE scheduled_tasks ADD CONSTRAINT pk_config_scheduled_tasks
	PRIMARY KEY (id)
;

INSERT INTO db_version (version, implementation_date) VALUES ('2.0', now());

INSERT INTO app(ldap, jira, mail, capabilities_metadata, test_bbox, service_types, modules, server, client_config, git, software, defaultbbox, available_projections, layer_attributes_excludes) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}', '{}','', '[]', '[]');

INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('update_datasets_metadata', 'Aktualisiert sämtliche Datensatzmetadaten', null, '', 'Noch nicht gelaufen','0 22 * * *', false, false, true);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('update_services_metadata', 'Aktualisiert sämtliche Dienstmetadaten', null, '', 'Noch nicht gelaufen','0 22 * * *', false, false, true);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('generate_layer_json', 'Generiert sämtliche Layer JSON Objekte in der Datenbank neu', null, '', 'Noch nicht gelaufen','0 22 * * *', false, false, true);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('portal_scraper', 'Importiert alle Masterportal Instanzen und verknüpft die dort eingebundenen Layer', null, '', 'Noch nicht gelaufen','0 22 * * *', false, false, true);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('prepare_dev_workspaces', 'Erstellt die dev Workspaces, so wie in der Schnittstellenkonfiguration angegeben', null, '', 'Noch nicht gelaufen','0 22 * * *', false, false, true);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('prepare_stage_workspaces', 'Erstellt die stage Workspaces, so wie in der Schnittstellenkonfiguration angegeben', null, '', 'Noch nicht gelaufen','0 22 * * *', false, false, true);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('prepare_prod_workspaces', 'Erstellt die prod Workspaces, so wie in der Schnittstellenkonfiguration angegeben', null, '', 'Noch nicht gelaufen','0 22 * * *', false, false, true);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('git_clean', 'Führt git checkout/reset/pull und git clean aus', null, '', 'Noch nicht gelaufen','0 22 * * *', false, true, true);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('git_push', 'Führt git push aus', null, '', 'Noch nicht gelaufen','*/1 * * * *', true, true, false);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('git_clone', 'Führt git clone aus', null, '', 'Noch nicht gelaufen','*/1 * * * *', false, false, true);
INSERT INTO scheduled_tasks(name, description, last_run, message, status, schedule, enabled, execute_on_startup, is_public) VALUES ('initialize_elastic_indexes', 'Initialisiert sämtliche Elasticsearch Indizes neu', null, '', 'Noch nicht gelaufen','0 22 * * *', false, false, true);

INSERT INTO elastic(settings, mappings, indexes) VALUES ('{"number_of_shards":1,"analysis":{"filter":{"autocomplete_filter":{"type":"edge_ngram","min_gram":3,"max_gram":20}},"analyzer":{"my_custom_analyzer":{"type":"custom","tokenizer":"my_tokenizer","filter":["lowercase","german_normalization","word_delimiter","autocomplete_filter"]},"autocomplete_search":{"tokenizer":"lowercase"}},"tokenizer":{"my_tokenizer":{"type":"char_group","tokenize_on_chars":["whitespace","-","\n","[","]",".",",",":",";","/","|"]}}}}', '{"properties":{"id":{"type":"text"},"name":{"type":"text","analyzer":"my_custom_analyzer","search_analyzer":"autocomplete_search"},"url":{"type":"text"},"typ":{"type":"text"},"featureType":{"type":"text"},"layers":{"type":"text"},"format":{"index":false,"type":"text"},"version":{"index":false,"type":"text"},"featureNS":{"index":false,"type":"text"},"singleTile":{"index":false,"type":"boolean"},"transparent":{"index":false,"type":"boolean"},"tilesize":{"index":false,"type":"integer"},"gutter":{"index":false,"type":"integer"},"transparency":{"index":false,"type":"integer"},"minScale":{"index":false,"type":"text"},"maxScale":{"index":false,"type":"text"},"gfiTheme":{"enabled":false},"gfiComplex":{"index":false,"type":"text"},"infoFormat":{"index":false,"type":"text"},"layerAttribution":{"index":false,"type":"text"},"legendURL":{"index":false,"type":"text"},"cache":{"index":false,"type":"boolean"},"featureCount":{"index":false,"type":"integer"},"typeName":{"index":false,"type":"text"},"datasets":{"properties":{"md_id":{"type":"text","fields":{"keyword":{"type":"keyword"}}},"csw_url":{"type":"text"},"show_doc_url":{"type":"text"},"rs_id":{"type":"text"},"md_name":{"type":"text","analyzer":"my_custom_analyzer","search_analyzer":"autocomplete_search"},"bbox":{"index":false,"type":"text"},"keywords":{"type":"text","analyzer":"my_custom_analyzer","search_analyzer":"autocomplete_search"},"description":{"type":"text","analyzer":"my_custom_analyzer","search_analyzer":"autocomplete_search"},"kategorie_opendata":{"type":"text"},"kategorie_inspire":{"type":"text"},"kategorie_organisation":{"type":"text"}}},"gfiAttributes":{"type":"object","enabled":false}}}', null);

CREATE SCHEMA IF NOT EXISTS data;

SET search_path = data, public;

/* Create Tables */

CREATE TABLE change_log
(
    id serial NOT NULL,
    dataset_id integer NOT NULL,
    entity text,
    username text,
    change_date timestamp without time zone,
    change_message text
)
;

CREATE TABLE collections
(
	id serial NOT NULL,
	dataset_id integer NOT NULL,
	name text NULL,
	title text NULL,
	title_alt text NULL,
	attribution text NULL,
	additional_categories text NULL,
	legend_url_intranet text NULL,
	legend_url_internet text NULL,
	transparency integer NULL   DEFAULT 0,
	scale_min text NULL   DEFAULT 0,
	scale_max text NULL   DEFAULT 1000000,
	gfi_disabled boolean NULL   DEFAULT false,
	gfi_theme text NULL   DEFAULT 'default',
	gfi_theme_params text NULL,
	attribute_config json NULL,
	gfi_as_new_window boolean NULL   DEFAULT false,
	gfi_window_specs text NULL,
	service_url_visible boolean NULL   DEFAULT true,
	store_type text NULL,
	db_schema text NULL,
	db_table text NULL,
	style json NULL,
	namespace text NULL,
	additional_info text NULL,
	description text NULL,
	group_object boolean NULL   DEFAULT false,
	legend_data_url text NULL,
	legend_content_type text NULL,
	legend_file_name text NULL,
	custom_bbox text NULL,
	api_constraint text NULL,
	complex_scheme boolean NULL DEFAULT false,
	tileindex text NULL,
	use_style_sld boolean DEFAULT false,
    style_sld text NULL,
	style_sld_icons json NULL,
	alternative_name text NULL,
	number_features integer NULL,
	source_data_type text NULL,
	source_data_location text NULL,
	source_data_name text NULL,
	source_data_table text NULL,
	source_data_schema text NULL,
	source_data_query text NULL,
	source_db_connection_id integer NULL
)
;

CREATE TABLE comments
(
	id serial NOT NULL,
	username text NULL,
	comment text NULL,
	post_date date NULL,
	dataset_id integer NULL,
	important boolean NULL
)
;

CREATE TABLE contact_links
(
	contact_id integer NOT NULL,
	dataset_id integer NOT NULL
)
;

CREATE TABLE contacts
(
	id serial NOT NULL,
	name text NULL,
	given_name text NULL,
	surname text NULL,
	company text NULL,
	ad_account text NULL,
	email text NULL,
	tel text NULL,
	objectguid text NULL
)
;

CREATE TABLE datasets
(
	id serial NOT NULL,
	metadata_catalog_id integer NULL,
	title text NULL,
	md_id text NULL,
	rs_id text NULL,
	shortname text NULL,
	description text NULL,
	access_constraints text NULL,
	bbox text NULL,
	cat_hmbtg text NULL,
	cat_inspire text NULL,
	cat_opendata text NULL,
	cat_org text NULL,
	fees text NULL,
	fees_json json NULL,
	filter_keywords text[] NULL,
	keywords text[] NULL,
	file_store text NULL,
	store_type text NULL,
	datasource text NULL,
	last_edit_date timestamp without time zone NULL,
	last_edited_by text NULL,
	create_date timestamp without time zone NULL,
	created_by text NULL,
	publish_date timestamp without time zone NULL,
	responsible_party text NULL,
	db_connection_r_id integer NULL,
	db_connection_w_id integer NULL,
	organisation text NULL,
	department text NULL,
	contact_id integer NULL,
	status text NULL,
	federalstate text NULL,
	district text NULL,
	additional_info text NULL,
	freigabe_link text NULL,
	restriction_level text NULL   DEFAULT 'intranet',
	srs text NULL,
	inspire boolean NULL   DEFAULT FALSE,
	external boolean NULL   DEFAULT FALSE,
	no_catalogue_link boolean NULL   DEFAULT FALSE,
	project_reference text NULL,
	portal_bound boolean NULL,
	portal_bound_name text NULL,
	allowed_groups json NULL,
	md_contact_mail text NULL
)
;

CREATE TABLE db_connections
(
	id serial NOT NULL,
	name text NOT NULL,
	host text NULL,
	port integer NULL,
	database text NULL,
	password text NULL,
	db_user text NULL,
	use_as_default_r boolean NULL   DEFAULT FALSE,
	use_as_default_w boolean NULL   DEFAULT FALSE
)
;

CREATE TABLE source_db_connections
(
	id serial NOT NULL,
	name text NOT NULL,
	dbms text NOT NULL,
	host text NULL,
	port integer NULL,
	database text NULL,
	password text NULL,
	db_user text NULL
)
;

CREATE TABLE etl_processes
(
	id serial NOT NULL,
	type text NULL,
	name text NULL,
	description text NULL,
	folder text NULL,
	host text NULL,
	interval_custom text NULL,
	last_run timestamp without time zone NULL,
	dataset_id integer NULL,
	mapped_collections json NULL,
	interval_number integer NULL,
	interval_unit text NULL,
	repo text NULL,
	service_definition text NULL,
	doclink text NULL
)
;

CREATE TABLE jira_tickets
(
	id serial NOT NULL,
	dataset_id integer NULL,
	key text NULL,
	creator text NULL,
	summary text NULL,
	assignee text NULL,
	project text NULL,
	create_date timestamp without time zone NULL
)
;

CREATE TABLE layers
(
	id serial NOT NULL,
	collection_id integer NOT NULL,
	service_id integer NOT NULL,
	dataset_id integer NOT NULL,
	json_intranet json NULL,
	json_internet json NULL,
	json_intranet_es json NULL,
	json_internet_es json NULL,
	json_intranet_prod json NULL,
	json_internet_prod json NULL,
	json_intranet_es_prod json NULL,
	json_internet_es_prod json NULL,
	delete_request boolean NULL   DEFAULT false,
	delete_request_comment text NULL,
	st_attributes json NULL
)
;

CREATE TABLE metadata_catalogs
(
	id serial NOT NULL,
	name text NULL,
	csw_url text NULL,
	show_doc_url text NULL,
	proxy boolean NULL,
	srs_metadata integer NULL,
	use_in_internet_json boolean NULL   DEFAULT false
)
;

CREATE TABLE portal_layer
(
	layer_id integer NOT NULL,
	portal_id integer NOT NULL
)
;

CREATE TABLE portals
(
	id serial NOT NULL,
	url text NULL,
	title text NULL,
	update_mode text NULL   DEFAULT 'manual',
	host text NULL
)
;

CREATE TABLE service_config_edited
(
	id serial NOT NULL,
	service_id integer NULL,
	uri text NULL
)
;

CREATE TABLE service_links
(
	dataset_id integer NOT NULL,
	service_id integer NOT NULL
)
;

CREATE TABLE service_test_results
(
	id serial NOT NULL,
	service_id integer NULL,
	text text NULL,
	checked boolean NULL   DEFAULT false
)
;

CREATE TABLE services
(
	id serial NOT NULL,
	check_status text NULL,
	name text NULL,
	title text NULL,
	type text NULL,
	software text NULL,
	server text NULL,
	folder text NULL,
	url_ext text NULL,
	url_int text NULL,
	url_sec text NULL,
	security_type text NULL,
	md_id text NULL,
	version text NULL,
	qa boolean NULL,
	description text NULL,
	fees text NULL,
	fees_json json NULL,
	access_constraints text NULL,
	allowed_groups json NULL,
	metadata_catalog_id integer NULL,
	external boolean NULL,
	keywords text[] NULL,
	clickradius integer NULL,
	maxfeatures integer NULL,
	additional_info text NULL,
	wms_theme_config json NULL,
	status_dev boolean NULL DEFAULT true,
	status_stage boolean NULL DEFAULT false,
	status_prod boolean NULL DEFAULT false,
	server_prod text,
    folder_prod text,
    url_ext_prod text,
    url_int_prod text,
    disable_config_module boolean DEFAULT false,
    disable_config_management boolean DEFAULT false
)
;

CREATE TABLE datasets_version_log
(
    dataset_id integer,
    editor text,
    "timestamp" timestamp without time zone,
    data json
)
;

CREATE TABLE visits
(
    service_id integer NULL,
    title text NULL,
    name_ext text NULL,
    visits_intranet integer NULL,
    visits_internet integer NULL,
    visits_total integer NULL,
    month text NULL,
    hist boolean DEFAULT false
)
;

/* Create Primary Keys, Indexes, Uniques, Checks */

ALTER TABLE change_log ADD CONSTRAINT pk_change_log
	PRIMARY KEY (id)
;

ALTER TABLE collections ADD CONSTRAINT pk_collections
	PRIMARY KEY (id)
;

CREATE INDEX ixfk_collections_services ON collections (dataset_id ASC)
;

ALTER TABLE comments ADD CONSTRAINT pk_comments
	PRIMARY KEY (id)
;

CREATE INDEX ixfk_comments_datasets ON comments (dataset_id ASC)
;

ALTER TABLE contact_links ADD CONSTRAINT pk_contact_links
	PRIMARY KEY (contact_id,dataset_id)
;

CREATE INDEX ixfk_contact_links_contacts ON contact_links (contact_id ASC)
;

CREATE INDEX ixfk_contact_links_datasets ON contact_links (dataset_id ASC)
;

ALTER TABLE contacts ADD CONSTRAINT pk_contacts
	PRIMARY KEY (id)
;

ALTER TABLE datasets ADD CONSTRAINT pk_datasets
	PRIMARY KEY (id)
;

ALTER TABLE datasets 
  ADD CONSTRAINT unique_datasets_shortname UNIQUE (shortname)
;

CREATE INDEX ixfk_collections_source_db_connections ON collections (source_db_connection_id ASC)
;

CREATE INDEX ixfk_datasets_db_connections_r ON datasets (db_connection_r_id ASC)
;

CREATE INDEX ixfk_datasets_db_connections_w ON datasets (db_connection_w_id ASC)
;

CREATE INDEX ixfk_datasets_metadata_catalogs ON datasets (metadata_catalog_id ASC)
;

ALTER TABLE db_connections ADD CONSTRAINT pk_db_connections
	PRIMARY KEY (id)
;

ALTER TABLE db_connections 
  ADD CONSTRAINT unique_db_connection_name UNIQUE (name)
;

ALTER TABLE source_db_connections ADD CONSTRAINT pk_source_db_connections
	PRIMARY KEY (id)
;

ALTER TABLE source_db_connections 
  ADD CONSTRAINT unique_source_db_connection_name UNIQUE (name)
;

ALTER TABLE etl_processes ADD CONSTRAINT pk_etl_processes
	PRIMARY KEY (id)
;

CREATE INDEX ixfk_etl_processes_datasets ON etl_processes (dataset_id ASC)
;

ALTER TABLE jira_tickets ADD CONSTRAINT pk_jira_tickets
	PRIMARY KEY (id)
;

CREATE INDEX ixfk_jira_tickets_datasets ON jira_tickets (dataset_id ASC)
;

ALTER TABLE layers ADD CONSTRAINT pk_layers
	PRIMARY KEY (id)
;

CREATE INDEX ixfk_layers ON layers (collection_id ASC)
;

CREATE INDEX ixfk_layers_datasets ON layers (dataset_id ASC)
;

CREATE INDEX ixfk_layers_services ON layers (service_id ASC)
;

ALTER TABLE metadata_catalogs ADD CONSTRAINT pk_metadata_catalogs
	PRIMARY KEY (id)
;

ALTER TABLE portal_layer ADD CONSTRAINT pk_portal_layer
	PRIMARY KEY (portal_id,layer_id)
;

CREATE INDEX ixfk_portal_layer_layers ON portal_layer (layer_id ASC)
;

CREATE INDEX ixfk_portal_layer_portals ON portal_layer (portal_id ASC)
;

ALTER TABLE portals ADD CONSTRAINT pk_portals
	PRIMARY KEY (id)
;

ALTER TABLE service_config_edited ADD CONSTRAINT pk_service_config_edited
	PRIMARY KEY (id)
;

CREATE INDEX ixfk_service_config_edited_services ON service_config_edited (service_id ASC)
;

ALTER TABLE service_links ADD CONSTRAINT pk_service_links
	PRIMARY KEY (dataset_id,service_id)
;

CREATE INDEX ixfk_service_links_datasets ON service_links (dataset_id ASC)
;

CREATE INDEX ixfk_service_links_services ON service_links (service_id ASC)
;

ALTER TABLE service_test_results ADD CONSTRAINT pk_service_test_results
	PRIMARY KEY (id)
;

CREATE INDEX ixfk_service_test_results_services ON service_test_results (service_id ASC)
;

ALTER TABLE services ADD CONSTRAINT pk_services
	PRIMARY KEY (id)
;

CREATE INDEX ixfk_services_metadata_catalogs ON services (metadata_catalog_id ASC)
;

/* Create Foreign Key Constraints */

ALTER TABLE collections ADD CONSTRAINT fk_collections_services
	FOREIGN KEY (dataset_id) REFERENCES datasets (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE collections ADD CONSTRAINT fk_collections_source_db_connection_id
	FOREIGN KEY (source_db_connection_id) REFERENCES source_db_connections (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE comments ADD CONSTRAINT fk_comments_datasets
	FOREIGN KEY (dataset_id) REFERENCES datasets (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE contact_links ADD CONSTRAINT fk_contact_links_contacts
	FOREIGN KEY (contact_id) REFERENCES contacts (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE contact_links ADD CONSTRAINT fk_contact_links_datasets
	FOREIGN KEY (dataset_id) REFERENCES datasets (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE datasets ADD CONSTRAINT fk_datasets_db_connections_r
	FOREIGN KEY (db_connection_r_id) REFERENCES db_connections (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE datasets ADD CONSTRAINT fk_datasets_db_connections_w
	FOREIGN KEY (db_connection_w_id) REFERENCES db_connections (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE datasets ADD CONSTRAINT fk_datasets_metadata_catalogs
	FOREIGN KEY (metadata_catalog_id) REFERENCES metadata_catalogs (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE etl_processes ADD CONSTRAINT fk_etl_processes_datasets
	FOREIGN KEY (dataset_id) REFERENCES datasets (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE jira_tickets ADD CONSTRAINT fk_jira_tickets_datasets
	FOREIGN KEY (dataset_id) REFERENCES datasets (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE layers ADD CONSTRAINT fk_layers_collections
	FOREIGN KEY (collection_id) REFERENCES collections (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE layers ADD CONSTRAINT fk_layers_datasets
	FOREIGN KEY (dataset_id) REFERENCES datasets (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE layers ADD CONSTRAINT fk_layers_services
	FOREIGN KEY (service_id) REFERENCES services (id) ON DELETE No Action ON UPDATE No Action
;

ALTER TABLE portal_layer ADD CONSTRAINT fk_portal_layer_portals
	FOREIGN KEY (portal_id) REFERENCES portals (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE service_config_edited ADD CONSTRAINT fk_service_config_edited_services
	FOREIGN KEY (service_id) REFERENCES services (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE service_links ADD CONSTRAINT fk_service_links_datasets
	FOREIGN KEY (dataset_id) REFERENCES datasets (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE service_links ADD CONSTRAINT fk_service_links_services
	FOREIGN KEY (service_id) REFERENCES services (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE service_test_results ADD CONSTRAINT fk_service_test_results_services
	FOREIGN KEY (service_id) REFERENCES services (id) ON DELETE Cascade ON UPDATE No Action
;

ALTER TABLE services ADD CONSTRAINT fk_services_metadata_catalogs
	FOREIGN KEY (metadata_catalog_id) REFERENCES metadata_catalogs (id) ON DELETE No Action ON UPDATE No Action
;