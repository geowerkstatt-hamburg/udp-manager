SET search_path = config, public;
DELETE FROM db_version;
INSERT INTO db_version (version, implementation_date) VALUES ('1.2', now());

SET search_path = data, public;
ALTER TABLE IF EXISTS jira_tickets
    ADD COLUMN create_date timestamp without time zone;