INSERT INTO config.webdav(
	name, base_url, directory, full_qualified_domain, username, password, proxy)
	VALUES (${name}, ${base_url}, ${directory}, ${full_qualified_domain}, ${username}, ${password}, ${proxy});