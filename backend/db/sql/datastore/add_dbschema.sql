CREATE SCHEMA IF NOT EXISTS ${schema:raw};
GRANT ALL ON SCHEMA ${schema:raw} TO ${user_w:raw};
GRANT USAGE ON SCHEMA ${schema:raw} TO ${user_r:raw};