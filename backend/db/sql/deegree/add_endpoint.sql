INSERT INTO saii_endpoint (
    endpoint_name, 
    proxy_url, 
    endpoint_id
)
VALUES (
    ${endpoint_name}, 
    ${proxy_url}, 
    nextval('saii_endpoint_seq')
)
RETURNING saii_endpoint.endpoint_id;