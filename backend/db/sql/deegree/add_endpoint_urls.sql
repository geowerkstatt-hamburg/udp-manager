BEGIN TRANSACTION;
INSERT INTO  saii_endpoint_url (
    endpoint_id, 
    endpoint_url, 
    id
)
VALUES (
    ${endpoint_id},
    ${endpoint_url}, 
    nextval('saii_endpoint_url_seq')
);
INSERT INTO saii_endpoint_url (
    endpoint_id, 
    endpoint_url, 
    id
)
VALUES (
    ${endpoint_id},
    ${url_int}, 
    nextval('saii_endpoint_url_seq')
)
RETURNING saii_endpoint_url.endpoint_id;
END TRANSACTION;