UPDATE saii_endpoint SET endpoint_name = ${endpoint_name}, proxy_url = ${proxy_url} WHERE endpoint_id = ${endpoint_id};
DELETE FROM saii_endpoint_url WHERE endpoint_id = ${endpoint_id};
INSERT INTO  saii_endpoint_url (
    endpoint_id, 
    endpoint_url, 
    id
)
VALUES (
    ${endpoint_id},
    ${proxy_url}, 
    nextval('saii_endpoint_url_seq')
);
INSERT INTO saii_endpoint_url (
    endpoint_id, 
    endpoint_url, 
    id
)
VALUES (
    ${endpoint_id},
    ${url_sec}, 
    nextval('saii_endpoint_url_seq')
);
