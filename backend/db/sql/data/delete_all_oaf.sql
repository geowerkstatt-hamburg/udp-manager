DELETE FROM data.layers as l
USING data.services as s
WHERE l.service_id = s.id and s.type = 'OAF';

DELETE FROM data.services WHERE type = 'OAF';
