UPDATE data.datasets SET 
    responsible_party=NULLIF(${responsible_party}, '')::text,
    filter_keywords=NULLIF(${filter_keywords},'{}')::text[],
    keywords=NULLIF(${keywords},'{}')::text[],
    datasource=NULLIF(${datasource}, '')::text,
    last_edit_date=${last_edit_date},
    last_edited_by=${last_edited_by},
    federalstate=NULLIF(${federalstate}, '')::text,
    district=NULLIF(${district}, '')::text,
    organisation=NULLIF(${organisation}, '')::text,
    department=NULLIF(${department}, '')::text,
    additional_info=NULLIF(${additional_info}, '')::text,
    freigabe_link=NULLIF(${freigabe_link}, '')::text,
    project_reference=NULLIF(${project_reference}, '')::text,
    portal_bound=${portal_bound},
    portal_bound_name=NULLIF(${portal_bound_name}, '')::text
    WHERE id=${id};