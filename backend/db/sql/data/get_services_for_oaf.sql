select s.id as service_id from data.services s
join data.service_links sl on sl.service_id = s.id
join data.datasets d on d.id = sl.dataset_id
where type = 'OAF' 
--and s.id = 29829
-- Select only with a listed db connection
and db_connection_r_id is not null
group by s.id
--limit 1
