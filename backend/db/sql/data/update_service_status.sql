UPDATE data.services SET
status_dev = ${status_dev},
status_stage = ${status_stage},
status_prod = ${status_prod},
folder = ${folder},
url_int = ${url_int},
url_ext = ${url_ext},
server = ${server}
WHERE id = ${id};