SELECT pl.layer_id AS id, pl.portal_id, c.name, c.title, l.collection_id, l.dataset_id, p.update_mode FROM data.portal_layer pl 
JOIN data.layers l ON pl.layer_id = l.id
JOIN data.collections c ON l.collection_id = c.id
JOIN data.portals p ON pl.portal_id = p.id
WHERE pl.portal_id = ${portal_id};