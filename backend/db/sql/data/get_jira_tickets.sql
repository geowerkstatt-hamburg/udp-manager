SELECT
    dataset_id,
    key,
    creator,
    summary,
    assignee,
    project,
    to_char(create_date, 'DD.MM.YYYY'::text) AS create_date
FROM data.jira_tickets 
WHERE dataset_id = ${dataset_id}::integer;