UPDATE data.services SET
status_dev = ${status_dev},
status_stage = ${status_stage},
status_prod = ${status_prod},
folder_prod = ${folder_prod},
url_int_prod = ${url_int_prod},
url_ext_prod = ${url_ext_prod},
server_prod = ${server_prod}
WHERE id = ${id};