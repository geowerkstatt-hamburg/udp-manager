UPDATE data.datasets SET 
    title=NULLIF(${title}, '')::text,
    md_id=NULLIF(${md_id}, '')::text,
    rs_id=NULLIF(${rs_id}, '')::text,
    description=NULLIF(${description}, '')::text,
    access_constraints=NULLIF(${access_constraints}, '')::text,
    bbox=NULLIF(${bbox}, '')::text,
    cat_hmbtg=NULLIF(${cat_hmbtg}, '')::text,
    cat_inspire=NULLIF(${cat_inspire}, '')::text,
    cat_opendata=NULLIF(${cat_opendata}, '')::text,
    cat_org=NULLIF(${cat_org}, '')::text,
    fees=NULLIF(${fees}, '')::text,
    fees_json=NULLIF(${fees_json}, '')::json,
    keywords=NULLIF(${keywords},'{}')::text[],
    md_contact_mail=NULLIF(${md_contact_mail}, '')::text
WHERE id=${dataset_id};