SELECT s.*, s.fees_json::text, sl.dataset_id, (SELECT count(*) FROM data.service_links WHERE service_id = s.id) AS ds_count FROM data.services s 
LEFT JOIN data.service_links sl ON s.id=sl.service_id
WHERE sl.dataset_id = ${dataset_id}::integer;