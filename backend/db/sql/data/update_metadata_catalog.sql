UPDATE data.metadata_catalogs SET 
    name=NULLIF(${name}, '')::text,
    csw_url=NULLIF(${csw_url}, '')::text,
    show_doc_url=NULLIF(${show_doc_url}, '')::text,
    proxy=${proxy},
    use_in_internet_json=${use_in_internet_json},
    srs_metadata=${srs_metadata}
WHERE id=${id};