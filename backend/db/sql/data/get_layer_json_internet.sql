SELECT array_to_json(array_agg(l.json_internet)) AS json_array FROM data.layers l
LEFT JOIN data.datasets d ON l.dataset_id=d.id
LEFT JOIN data.services s ON l.service_id=s.id
WHERE d.restriction_level='internet' AND (s.status_dev=true OR s.status_stage=true OR s.external=true) AND l.json_internet IS NOT null;