SELECT json_build_object(
	'dataset', (SELECT row_to_json(d) FROM data.datasets d WHERE id = ${dataset_id}),
	'service', (SELECT row_to_json(s) FROM data.services s WHERE id = ${service_id}),
	'collections', (SELECT json_agg(row_to_json(c)) FROM data.collections c WHERE dataset_id = ${dataset_id}),
	'layers', (SELECT json_agg(row_to_json(l)) FROM data.layers l WHERE service_id = ${service_id}),
	'db_connections', (SELECT json_agg(row_to_json(dbc)) FROM data.db_connections dbc),
	'metadata_catalogs', (SELECT json_agg(row_to_json(mdc)) FROM data.metadata_catalogs mdc)
) AS data;