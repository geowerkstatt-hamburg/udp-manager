DELETE FROM data.layers WHERE dataset_id=${id};
DELETE FROM data.collections WHERE dataset_id=${id};
DELETE FROM data.services WHERE id IN (SELECT service_id AS id FROM data.service_links WHERE dataset_id=${id}) AND id NOT IN (SELECT service_id AS id FROM data.service_links WHERE dataset_id != ${id});
DELETE FROM data.service_links WHERE dataset_id=${id};
DELETE FROM data.jira_tickets WHERE dataset_id=${id};
DELETE FROM data.datasets WHERE id=${id};