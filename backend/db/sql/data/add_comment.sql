INSERT INTO data.comments(
	username, comment, post_date, dataset_id, important)
	VALUES (${username}, ${comment}, ${post_date}, ${dataset_id}, ${important})
    RETURNING id;