INSERT INTO data.jira_tickets (
    dataset_id,
    key,
    creator,
    summary,
    assignee,
    project,
    create_date
    )
    VALUES (
        ${dataset_id},
        NULLIF(${key}, '')::text,
        NULLIF(${creator}, '')::text,
        NULLIF(${summary}, '')::text,
        NULLIF(${assignee}, '')::text,
        NULLIF(${project}, '')::text,
        ${create_date}
    );