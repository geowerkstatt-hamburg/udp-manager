UPDATE data.comments
	SET username=${username},
	comment=${comment},
	post_date=${post_date},
	dataset_id=${dataset_id},
	important=${important}
WHERE id = ${id};