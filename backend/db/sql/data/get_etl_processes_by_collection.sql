SELECT id, type, name, folder AS repository, host
FROM   data.etl_processes r, json_array_elements(r.mapped_collections) obj
WHERE  obj->>'id' = ${collection_id};