INSERT INTO data.datasets_version_log(
	dataset_id, editor, "timestamp", data)
	VALUES (${dataset_id}, ${last_edited_by}, NOW(), (
    SELECT json_build_object(
            'dataset', (SELECT row_to_json(d) FROM data.datasets d WHERE id = ${dataset_id}),
            'services', (SELECT json_agg(row_to_json(s)) FROM data.services s WHERE id IN (SELECT service_id AS id FROM data.service_links WHERE dataset_id = ${dataset_id})),
            'collections', (SELECT json_agg(row_to_json(c)) FROM data.collections c WHERE dataset_id = ${dataset_id}),
            'layers', (SELECT json_agg(row_to_json(l)) FROM data.layers l WHERE dataset_id = ${dataset_id})
        ) 
    ));