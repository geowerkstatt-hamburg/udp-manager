SELECT v.*, s.software FROM data.visits v 
LEFT JOIN data.services s ON v.service_id = s.id
WHERE v.month = ${month} AND s.software = ${software} ORDER BY visits_total DESC LIMIT 10;