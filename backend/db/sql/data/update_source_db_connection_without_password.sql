UPDATE data.source_db_connections SET 
name=NULLIF(${name}, '')::text,
dbms=NULLIF(${dbms}, '')::text,
host=NULLIF(${host}, '')::text,
port=${port},
database=NULLIF(${database}, '')::text,
db_user=NULLIF(${user}, '')::text
WHERE id=${id};