UPDATE data.etl_processes
	SET type=NULLIF(${type}, '')::text,
	name=NULLIF(${name}, '')::text,
	description=NULLIF(${description}, '')::text,
	folder=NULLIF(${folder}, '')::text,
	host=NULLIF(${host}, '')::text,
	interval_custom=NULLIF(${interval_custom}, '')::text,
	interval_number=${interval_number},
	interval_unit=NULLIF(${interval_unit}, '')::text,
	last_run=NULLIF(${last_run}, '')::timestamp without time zone,
	mapped_collections=NULLIF(${mapped_collections}, '')::json,
	repo=NULLIF(${repo}, '')::text,
	service_definition=NULLIF(${service_definition}, '')::text,
	doclink=NULLIF(${doclink}, '')::text
	WHERE id = ${id};