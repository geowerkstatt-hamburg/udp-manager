INSERT INTO data.db_connections (
    name, host, port, database, db_user, password, use_as_default_r, use_as_default_w)
    VALUES (NULLIF(${name}, '')::text,
    NULLIF(${host}, '')::text,
    ${port},
    NULLIF(${database}, '')::text,
    NULLIF(${user}, '')::text,
    NULLIF(${password}, '')::text,
    ${use_as_default_r},
    ${use_as_default_w})
    RETURNING id;