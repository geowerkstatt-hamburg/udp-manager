WITH n_staged_apis_count AS (
	SELECT 
		l.dataset_id,
		SUM(CASE WHEN s.status_stage = true THEN 1 else 0 end) AS n_staged_apis
	FROM data.service_links l
	LEFT JOIN data.services s ON s.id = l.service_id
	GROUP BY l.dataset_id
)
SELECT 
	c.dataset_id,
	d.last_edited_by,
	d.last_edit_date::text
FROM n_staged_apis_count c
LEFT JOIN data.datasets d on d.id = c.dataset_id
WHERE c.n_staged_apis > 0; 