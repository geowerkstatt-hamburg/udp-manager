INSERT INTO data.etl_processes(
	type, name, description, folder, host, interval_custom, interval_number, interval_unit, last_run, dataset_id, mapped_collections, repo, service_definition, doclink)
	VALUES (NULLIF(${type}, '')::text,
	NULLIF(${name}, '')::text,
	NULLIF(${description}, '')::text,
	NULLIF(${folder}, '')::text,
	NULLIF(${host}, '')::text,
	NULLIF(${interval_custom}, '')::text,
	${interval_number},
	NULLIF(${interval_unit}, '')::text,
	NULLIF(${last_run}, '')::timestamp without time zone,
	${dataset_id}::integer,
	NULLIF(${mapped_collections}, '')::json,
	NULLIF(${repo}, '')::text,
	NULLIF(${service_definition}, '')::text,
	NULLIF(${doclink}, '')::text
	) RETURNING id;