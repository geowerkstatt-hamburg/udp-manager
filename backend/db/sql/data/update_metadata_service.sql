UPDATE data.services SET
    title=NULLIF(${title}, '')::text,
    description=NULLIF(${description}, '')::text,
    fees=NULLIF(${fees}, '')::text,
    fees_json=NULLIF(${fees_json}, '')::json,
    access_constraints=NULLIF(${access_constraints}, '')::text
WHERE id = ${service_id}