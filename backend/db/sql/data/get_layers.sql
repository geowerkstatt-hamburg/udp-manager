SELECT layers.id, collection.name AS name, collection.title AS title, service.type AS service_type, service.title AS service_title, layers.collection_id, layers.dataset_id, layers.service_id FROM data.layers 
LEFT JOIN data.collections collection ON layers.collection_id = collection.id
LEFT JOIN data.services service ON layers.service_id = service.id