UPDATE data.collections
SET title_alt = NULLIF(${title_alt}, '')::text,
    attribution = NULLIF(${attribution}, '')::text,
    additional_categories = NULLIF(${additional_categories}, '')::text,
    legend_url_intranet = NULLIF(${legend_url_intranet}, '')::text,
    legend_url_internet = NULLIF(${legend_url_internet}, '')::text,
    transparency = ${transparency},
    scale_min = NULLIF(${scale_min}, '')::text,
    scale_max = NULLIF(${scale_max}, '')::text,
    gfi_disabled = ${gfi_disabled},
    gfi_theme = NULLIF(${gfi_theme}, '')::text,
    gfi_theme_params = NULLIF(${gfi_theme_params}, '')::text,
    gfi_as_new_window = ${gfi_as_new_window},
    gfi_window_specs = NULLIF(${gfi_window_specs}, '')::text,
    service_url_visible = ${service_url_visible},
    additional_info = NULLIF(${additional_info}, '')::text,
    number_features = ${number_features}
WHERE id = ${id};