-- find all linked services of current dataset_id and change their status to prod
WITH linked_services AS (
	SELECT 
        * 
    FROM data.service_links
	WHERE dataset_id = ${dataset_id}
)
UPDATE data.services s SET
    status_prod = true
WHERE s.id IN (SELECT service_id FROM linked_services);

-- update dataset details of dataset_id
UPDATE data.datasets d SET 
    status = 'prod',
    last_edit_date = ${last_edit_date},
    last_edited_by = ${last_edited_by}
WHERE d.id = ${dataset_id};