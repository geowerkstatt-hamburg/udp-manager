SELECT DISTINCT ON (s.id) s.id,
    s.name,
    s.title,
	s.status_dev,
	s.status_stage,
	s.status_prod,
	s.check_status,
	s.type,
	s.software,
	s.version,
	s.url_int,
	s.url_ext,
	s.url_int_prod,
	s.url_ext_prod,
	s.software,
	s.folder,
	s.name,
	s.server,
	(SELECT title FROM data.datasets WHERE id = sl.dataset_id) AS dataset_title,
	(SELECT json_agg(t) FROM (
		SELECT id, title, responsible_party FROM data.datasets 
		LEFT JOIN data.service_links sls ON s.id = sls.service_id
		WHERE id = sls.dataset_id
	) as t) AS datasets,
	(SELECT count(*) FROM data.service_links WHERE service_id = s.id) AS ds_count
   FROM data.services s
     LEFT JOIN data.service_links sl ON s.id = sl.service_id;