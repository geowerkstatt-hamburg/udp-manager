UPDATE data.contacts
	SET NULLIF(name=${name}, '')::text,
	NULLIF(given_name=${given_name}, '')::text,
	NULLIF(surname=${surname}, '')::text,
	NULLIF(company=${company}, '')::text,
	NULLIF(ad_account=${ad_account}, '')::text,
	NULLIF(email=${email}, '')::text,
	NULLIF(tel=${tel}, '')::text,
	NULLIF(objectguid=${objectguid}, '')::text
	WHERE id=${contact_id};