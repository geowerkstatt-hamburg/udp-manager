INSERT INTO data.contacts(
	name, given_name, surname, company, ad_account, email, tel, objectguid)
	VALUES (${name}, 
	NULLIF(${given_name}, '')::text,
	NULLIF(${surname}, '')::text,
	NULLIF(${company}, '')::text,
	NULLIF(${ad_account}, '')::text,
	NULLIF(${email}, '')::text,
	NULLIF(${tel}, '')::text,
	NULLIF(${objectguid}, '')::text)
    RETURNING id;