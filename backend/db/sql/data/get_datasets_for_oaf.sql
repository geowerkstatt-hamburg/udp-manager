SELECT 
s.security_type,
d.id AS dataset_id, 
d.md_id, 
d.title, 
d.shortname,
d.fees_json,
d.fees,
d.description,
d.access_constraints,
s.status_dev as service_status_dev,
s.status_stage as service_status_stage,
s.status_prod as service_status_prod,
d.status,
d.restriction_level,
d.metadata_catalog_id
FROM data.datasets as d
join data.service_links as sl on d.id = sl.dataset_id
join data.services  as s on sl.service_id = s.id

WHERE shortname IS NOT NULL and shortname not like 'change_it_%'
and s.type = 'WFS' and security_type not in ('AD SSO', 'Basic Auth') and s.status_stage = true

order by md_id
--limit 5