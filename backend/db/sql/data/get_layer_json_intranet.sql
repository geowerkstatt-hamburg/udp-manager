SELECT array_to_json(array_agg(l.json_intranet)) AS json_array FROM data.layers l
LEFT JOIN data.services s ON l.service_id=s.id
WHERE (s.status_dev=true OR s.status_stage=true OR s.external=true) AND l.json_intranet IS NOT null;