INSERT INTO data.source_db_connections (
    name, dbms, host, port, database, db_user, password)
    VALUES (NULLIF(${name}, '')::text,
    NULLIF(${dbms}, '')::text,
    NULLIF(${host}, '')::text,
    ${port},
    NULLIF(${database}, '')::text,
    NULLIF(${user}, '')::text,
    NULLIF(${password}, '')::text)
    RETURNING id;