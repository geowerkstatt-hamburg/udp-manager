SELECT DISTINCT ON (dataset_id) 
dataset_id,
editor,
to_char(timestamp, 'YYYY-MM-DD HH24:MI'::text) AS timestamp,
data
FROM data.datasets_version_log
WHERE dataset_id = ${dataset_id}::integer
ORDER BY dataset_id, timestamp DESC;