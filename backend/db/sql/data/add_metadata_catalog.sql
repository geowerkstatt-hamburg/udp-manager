INSERT INTO data.metadata_catalogs (name, csw_url, show_doc_url, proxy, use_in_internet_json, srs_metadata) VALUES (
    NULLIF(${name}, '')::text,
    NULLIF(${csw_url}, '')::text,
    NULLIF(${show_doc_url}, '')::text,
    ${proxy},
    ${use_in_internet_json},
    ${srs_metadata}
);