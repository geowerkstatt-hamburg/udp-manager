UPDATE data.datasets SET last_edited_by = ${last_edited_by}, last_edit_date = ${last_edit_date} WHERE id = ${dataset_id};
INSERT INTO data.change_log(
	dataset_id, entity, username, change_date, change_message)
	VALUES (${dataset_id}, ${entity}, ${last_edited_by}, ${last_edit_date}, ${change_message});