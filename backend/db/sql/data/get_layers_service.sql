SELECT 
    layers.id,
    layers.st_attributes,
    collection.name AS name,
    collection.alternative_name AS alternative_name,
    collection.title AS title,
    collection.transparency AS transparency,
    collection.namespace AS namespace,
    service.type AS service_type,
    service.title AS service_title,
    layers.collection_id,
    layers.dataset_id,
    layers.service_id,
    layers.delete_request,
    layers.delete_request_comment
FROM data.layers 
LEFT JOIN data.collections collection ON layers.collection_id = collection.id
LEFT JOIN data.services service ON layers.service_id = service.id
WHERE service_id=${service_id};