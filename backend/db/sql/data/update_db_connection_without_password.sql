UPDATE data.db_connections SET 
name=NULLIF(${name}, '')::text,
host=NULLIF(${host}, '')::text,
port=${port},
database=NULLIF(${database}, '')::text,
db_user=NULLIF(${user}, '')::text,
use_as_default_r=${use_as_default_r},
use_as_default_w=${use_as_default_w}
WHERE id=${id};