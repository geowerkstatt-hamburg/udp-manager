SELECT d.* FROM data.contact_links
LEFT JOIN data.datasets d ON contact_links.dataset_id = d.id
LEFT JOIN data.contacts c ON contact_links.contact_id = c.id
WHERE c.objectguid = ${objectguid};