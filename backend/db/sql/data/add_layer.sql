INSERT INTO data.layers(
	collection_id, service_id, dataset_id, st_attributes)
	VALUES (
        ${collection_id},
        ${service_id},
        ${dataset_id},
        ${st_attributes}
    )
    RETURNING id;