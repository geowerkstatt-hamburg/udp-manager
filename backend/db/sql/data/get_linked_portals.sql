SELECT portal.id AS id, portal_layer.layer_id, portal.url AS url, portal.title, portal.host, portal.update_mode FROM data.portal_layer 
LEFT JOIN data.portals portal ON portal_layer.portal_id = portal.id
WHERE portal_layer.layer_id=${layer_id}::integer;