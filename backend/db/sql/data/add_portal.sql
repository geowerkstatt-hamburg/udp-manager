INSERT INTO data.portals(
	url, host, title, update_mode)
	VALUES (${url}, ${host}, ${title}, ${update_mode})
RETURNING id;