UPDATE data.collections
SET dataset_id = ${dataset_id},
    name = NULLIF(${name}, '')::text,
    title = NULLIF(${title}, '')::text,
    title_alt = NULLIF(${title_alt}, '')::text,
    description = NULLIF(${description}, '')::text,
    attribution = NULLIF(${attribution}, '')::text,
    additional_categories = NULLIF(${additional_categories}, '')::text,
    legend_url_intranet = NULLIF(${legend_url_intranet}, '')::text,
    legend_url_internet = NULLIF(${legend_url_internet}, '')::text,
    transparency = ${transparency},
    scale_min = NULLIF(${scale_min}, '')::text,
    scale_max = NULLIF(${scale_max}, '')::text,
    gfi_disabled = ${gfi_disabled},
    gfi_theme = NULLIF(${gfi_theme}, '')::text,
    gfi_theme_params = NULLIF(${gfi_theme_params}, '')::text,
    gfi_as_new_window = ${gfi_as_new_window},
    gfi_window_specs = NULLIF(${gfi_window_specs}, '')::text,
    service_url_visible = ${service_url_visible},
    namespace = NULLIF(${namespace}, '')::text,
    store_type = NULLIF(${store_type}, '')::text,
    db_schema = NULLIF(${db_schema}, '')::text,
    db_table = NULLIF(${db_table}, '')::text,
    additional_info = NULLIF(${additional_info}, '')::text,
    group_object = ${group_object},
    legend_data_url = NULLIF(${legend_data_url}, '')::text,
    legend_content_type = NULLIF(${legend_content_type}, '')::text,
    legend_file_name = NULLIF(${legend_file_name}, '')::text,
    custom_bbox = NULLIF(${custom_bbox}, '')::text,
    api_constraint = NULLIF(${api_constraint}, '')::text,
    complex_scheme = ${complex_scheme},
    tileindex = NULLIF(${tileindex}, '')::text,
    use_style_sld = ${use_style_sld},
    alternative_name = NULLIF(${alternative_name}, '')::text,
    number_features = ${number_features},
    source_data_type = NULLIF(${source_data_type}, '')::text,
    source_data_location = NULLIF(${source_data_location}, '')::text,
    source_data_name = NULLIF(${source_data_name}, '')::text,
    source_data_table = NULLIF(${source_data_table}, '')::text,
    source_data_schema = NULLIF(${source_data_schema}, '')::text,
    source_data_query = NULLIF(${source_data_query}, '')::text,
    source_db_connection_id = ${source_db_connection_id}
WHERE id = ${id};