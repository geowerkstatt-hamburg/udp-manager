SELECT l.id, l.st_attributes, to_json(c.*) AS collection_params, to_json (s.*) AS service_params, to_json (d.*) AS dataset_params, to_json (mc.*) AS metadata_params
FROM data.layers l
LEFT JOIN data.collections c ON l.collection_id = c.id
LEFT JOIN data.services s ON l.service_id = s.id
LEFT JOIN data.datasets d ON l.dataset_id = d.id
LEFT JOIN data.metadata_catalogs mc ON d.metadata_catalog_id = mc.id
WHERE l.id=${id};