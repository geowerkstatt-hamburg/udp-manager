SELECT array_to_json(array_agg(l.json_internet_prod)) AS json_array FROM data.layers l
LEFT JOIN data.datasets d ON l.dataset_id=d.id
LEFT JOIN data.services s ON l.service_id=s.id
WHERE d.restriction_level='internet' AND (s.status_prod=true) AND l.json_internet IS NOT null;