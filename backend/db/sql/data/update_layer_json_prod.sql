UPDATE data.layers SET 
    json_intranet_prod = ${json_intranet},
    json_internet_prod = ${json_internet},
    json_intranet_es_prod = ${json_intranet_es},
    json_internet_es_prod = ${json_internet_es}
WHERE id=${id}