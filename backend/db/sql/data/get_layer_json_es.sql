SELECT l.id, l.json_intranet_es, l.json_internet_es, l.json_intranet_es_prod, l.json_internet_es_prod, s.status_dev, s.status_stage, s.status_prod, s.external, array_to_string(d.filter_keywords, '|') AS keywords, d.restriction_level  
FROM data.layers l 
LEFT JOIN data.datasets d ON l.dataset_id=d.id
LEFT JOIN data.services s ON l.service_id=s.id;