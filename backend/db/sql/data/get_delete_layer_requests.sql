SELECT 
	l.id AS layer_id,
	d.responsible_party
FROM 
	data.layers AS l
INNER JOIN data.datasets AS d
	ON d.id = l.dataset_id
WHERE 
	delete_request IS true;