const {QueryFile} = require("pg-promise");
const {join: joinPath} = require("path");

const queryFiles = {};

function sql (file) {
    const fullPath = joinPath(__dirname, file);

    const options = {
        minify: true
    };

    const qf = new QueryFile(fullPath, options);

    if (qf.error) {
        console.error(qf.error);
    }
    return qf;
}

module.exports = (folder, file) => {
    const path = `${folder}/${file}.sql`;

    if (!queryFiles[path]) {
        queryFiles[path] = sql(path);
    }

    return queryFiles[path];
};
