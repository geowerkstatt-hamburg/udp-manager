// const {config: sql_config, data: sql_data} = require("../sql");
const sql = require("../sql");
const Logger = require("../../utils/logger.util");
const config = require("../../config.js").getConfig();

class UDPMRepository {
    constructor (db, pgp) {
        this.db = db;
        this.pgp = pgp;
    }

    async getDBVersion () {
        return this.db.any(sql("config", "get_db_version"));
    }

    async getSchemaList () {
        return this.db.any(sql("config", "get_schema_list"));
    }

    async clearDb () {
        return this.db.any(sql("config", "clear_db"));
    }

    async initDb () {
        return this.db.any(sql("config", "init_db"));
    }

    async addDefaultConfig(params) {
        return this.db.any(sql("config", "add_default_config"), params);
    }

    async updateDb (version) {
        return this.db.any(sql("config", "update_db_v" + version));
    }

    async getScheduledTasks () {
        return this.db.any(sql("config", "get_scheduled_tasks"));
    }

    async getScheduledTaskByName (params) {
        return this.db.one(sql("config", "get_scheduled_task_by_name"), params);
    }

    async getPublicScheduledTasks () {
        return this.db.any(sql("config", "get_public_scheduled_tasks"));
    }

    async updateScheduledTask (params) {
        return this.db.any(sql("config", "update_scheduled_task"), params);
    }

    async updateScheduledTaskStatus (params) {
        return this.db.any(sql("config", "update_scheduled_task_status"), params);
    }

    async getExtInstances () {
        return this.db.any(sql("config", "get_ext_instances"));
    }

    async getExtInstanceById (params) {
        return this.db.one(sql("config", "get_ext_instance_by_id"), params);
    }

    async addExtInstance (params) {
        return this.db.any(sql("config", "add_ext_instance"), params);
    }

    async updateExtInstance (params) {
        return this.db.any(sql("config", "update_ext_instance"), params);
    }

    async deleteExtInstance (params) {
        return this.db.any(sql("config", "delete_ext_instance"), params);
    }

    async getElasticConfig () {
        return this.db.one(sql("config", "get_elastic_config"));
    }

    async updateElasticConfig (params) {
        return this.db.any(sql("config", "update_elastic_config"), params);
    }

    async getFmeServerConnections () {
        return this.db.any(sql("config", "get_fme_server"));
    }

    async addFmeServerConnection (params) {
        return this.db.any(sql("config", "add_fme_server"), params);
    }

    async deleteFmeServerConnection (params) {
        return this.db.any(sql("config", "delete_fme_server"), params);
    }

    async updateFmeServerConnection (params) {
        return this.db.any(sql("config", "update_fme_server"), params);
    }

    async updateFmeServerConnectionWoPassword (params) {
        return this.db.any(sql("config", "update_fme_server_without_password"), params);
    }

    async getWebDavConnections () {
        return this.db.any(sql("config", "get_webdav"));
    }

    async deleteWebDavConnection (params) {
        return this.db.any(sql("config", "delete_webdav"),params);
    }

    async addWebDavConnection (params) {
        return this.db.any(sql("config", "add_webdav"), params);
    }

    async updateWebDavConnection (params) {
        return this.db.any(sql("config", "update_webdav"), params);
    }

    async updateWebDavConnectionWoPassword (params) {
        return this.db.any(sql("config", "update_webdav_without_password"), params);
    }

    async getAppConfig () {
        return this.db.one(sql("config", "get_app_config"));
    }

    async updateAppConfig (params) {
        return this.db.any(sql("config", "update_app_config"), params);
    }

    async getMetadataCatalogs () {
        return this.db.any(sql("data", "get_metadata_catalogs"));
    }

    async getMetadataCatalog (params) {
        return this.db.any(sql("data", "get_metadata_catalog"), params);
    }

    async addMetadataCatalog (params) {
        return this.db.any(sql("data", "add_metadata_catalog"), params);
    }

    async updateMetadataCatalog (params) {
        return this.db.any(sql("data", "update_metadata_catalog"), params);
    }

    async deleteMetadataCatalog (params) {
        return this.db.any(sql("data", "delete_metadata_catalog"), params);
    }

    async getMetadataCatalogExtRepl () {
        return this.db.any(sql("data", "get_metadata_catalog_ext_repl"));
    }

    async setAllMetadataCatalogsExtFalse () {
        return this.db.any(sql("data", "set_all_metadata_catalogs_ext_false"));
    }

    async getDbConnections () {
        return this.db.any(sql("data", "get_db_connections"));
    }

    async getDbConnectionById (params) {
        return this.db.one(sql("data", "get_db_connection_by_id"), params);
    }

    async getDbConnectionByName (params) {
        return this.db.one(sql("data", "get_db_connection_by_name"), params);
    }

    async addDbConnection (params) {
        return this.db.one(sql("data", "add_db_connection"), params);
    }

    async updateDbConnection (params) {
        return this.db.any(sql("data", "update_db_connection"), params);
    }

    async updateDbConnectionWoPassword (params) {
        return this.db.any(sql("data", "update_db_connection_without_password"), params);
    }

    async deleteDbConnection (params) {
        return this.db.any(sql("data", "delete_db_connection"), params);
    }

    async getDatasetsByDbConnectionId (params) {
        return this.db.any(sql("data", "get_datasets_by_db_connection_id"), params);
    }

    async getSourceDbConnections () {
        return this.db.any(sql("data", "get_source_db_connections"));
    }

    async getSourceDbConnectionById (params) {
        return this.db.one(sql("data", "get_source_db_connection_by_id"), params);
    }

    async getSourceDbConnectionByName (params) {
        return this.db.one(sql("data", "get_source_db_connection_by_name"), params);
    }

    async addSourceDbConnection (params) {
        return this.db.one(sql("data", "add_source_db_connection"), params);
    }

    async updateSourceDbConnection (params) {
        return this.db.any(sql("data", "update_source_db_connection"), params);
    }

    async updateSourceDbConnectionWoPassword (params) {
        return this.db.any(sql("data", "update_source_db_connection_without_password"), params);
    }

    async deleteSourceDbConnection (params) {
        return this.db.any(sql("data", "delete_source_db_connection"), params);
    }

    async getCollectionsBySourceDbConnectionId (params) {
        return this.db.any(sql("data", "get_collections_by_source_db_connection_id"), params);
    }

    async getServices () {
        return this.db.any(sql("data", "get_services"));
    }

    async getCollections () {
        return this.db.any(sql("data", "get_collections"));
    }

    async getCollection (params) {
        return this.db.one(sql("data", "get_collection"), params);
    }

    async getLayers () {
        return this.db.any(sql("data", "get_layers"));
    }

    async getLayersService (params) {
        return this.db.any(sql("data", "get_layers_service"), params);
    }

    async getLayersPortal (params) {
        return this.db.any(sql("data", "get_layers_portal"), params);
    }

    async getLayersDataset (params) {
        return this.db.any(sql("data", "get_layers_dataset"), params);
    }

    async getLayersCollection (params) {
        return this.db.any(sql("data", "get_layers_collection"), params);
    }

    async getDatasets () {
        return this.db.any(sql("data", "get_datasets"));
    }

    async getDatasetsByUser (params) {
        return this.db.any(sql("data", "get_datasets_by_user"), params);
    }

    async getDatasetsByObjectGuid (params) {
        return this.db.any(sql("data", "get_datasets_by_objectguid"), params);
    }

    async getDatasetById (params) {
        return this.db.one(sql("data", "get_dataset_by_id"), params);
    }

    async getDatasetByShortname (params) {
        return this.db.one(sql("data", "get_dataset_by_shortname"), params);
    }

    async getCollectionsDataset (params) {
        return this.db.any(sql("data", "get_collections_dataset"), params);
    }

    async addCollectionsDataset (params) {
        return this.db.any(sql("data", "add_collections_dataset"), params);
    }

    async updateCollectionsDataset (params) {
        return this.db.any(sql("data", "update_collections_dataset"), params);
    }

    async updateCollectionsDatasetEditorial (params) {
        return this.db.any(sql("data", "update_collections_dataset_editorial"), params);
    }

    async deleteCollectionsDataset (params) {
        return this.db.any(sql("data", "delete_collections_dataset"), params);
    }

    async updateAttributeConfig (params) {
        return this.db.any(sql("data", "update_attribute_config"), params);
    }

    async updateStyleConfig (params) {
        return this.db.any(sql("data", "update_style_config"), params);
    }

    async updateStyleSld (params) {
        return this.db.any(sql("data", "update_style_sld"), params);
    }

    async updateAttributeConfigAllCollections (params) {
        return this.db.any(sql("data", "update_attribute_config_all_collections"), params);
    }

    async updateNumberFeatures (params) {
        return this.db.any(sql("data", "update_number_features"), params);
    }

    async getLayerJson (params) {
        if (params.env === "prod") {
            if (params.query === "all") {
                return this.db.any(sql("data", "get_layer_json_prod_all"), params);
            }
            else if (params.query === "intranet") {
                return this.db.any(sql("data", "get_layer_json_prod_intranet"), params);
            }
            else if (params.query === "internet") {
                return this.db.any(sql("data", "get_layer_json_prod_internet"), params);
            }
            else {
                return this.db.any(sql("data", "get_layer_prod_json"), params);
            }
        }
        else {
            if (params.query === "all") {
                return this.db.any(sql("data", "get_layer_json_all"), params);
            }
            else if (params.query === "intranet") {
                return this.db.any(sql("data", "get_layer_json_intranet"), params);
            }
            else if (params.query === "internet") {
                return this.db.any(sql("data", "get_layer_json_internet"), params);
            }
            else {
                return this.db.any(sql("data", "get_layer_json"), params);
            }
        }
    }

    async getLayerJsonES () {
        return this.db.any(sql("data", "get_layer_json_es"));
    }

    async getServicesDataset (params) {
        return this.db.any(sql("data", "get_services_dataset"), params);
    }

    async getDistinctServicesDataset (params) {
        return this.db.any(sql("data", "get_distinct_services_dataset"), params);
    }

    async getServicesRestrictionLevel () {
        return this.db.any(sql("data", "get_services_restriction_level"));
    }

    async getService (params) {
        return this.db.any(sql("data", "get_service"), params);
    }

    async addDataset (params) {
        return this.db.any(sql("data", "add_dataset"), params);
    }

    async updateDataset (params) {
        return this.db.any(sql("data", "update_dataset"), params);
    }

    async updateDatasetEditorial (params) {
        return this.db.any(sql("data", "update_dataset_editorial"), params);
    }

    async updateDatasetStatus (params) {
        return this.db.any(sql("data", "update_dataset_status"), params);
    }

    async deleteDataset (params) {
        return this.db.any(sql("data", "delete_dataset"), params);
    }

    async deleteDatasetComplete (params) {
        return this.db.any(sql("data", "delete_dataset_complete"), params);
    }

    async deleteDatasetNoServices (params) {
        return this.db.any(sql("data", "delete_dataset_no_services"), params);
    }

    async addService (params) {
        return this.db.any(sql("data", "add_service"), params);
    }

    async updateService (params) {
        return this.db.any(sql("data", "update_service"), params);
    }

    async deleteService (params) {
        return this.db.any(sql("data", "delete_service"), params);
    }

    async deleteNotLinkedServices () {
        return this.db.any(sql("data", "delete_services_not_linked"));
    }

    async updateWmsThemeConfig (params) {
        return this.db.any(sql("data", "update_wms_theme_config"), params);
    }

    async addLayer (params) {
        return this.db.any(sql("data", "add_layer"), params);
    }

    async updateLayer (params) {
        return this.db.any(sql("data", "update_layer"), params);
    }

    async getLinkedPortals (params) {
        return this.db.any(sql("data", "get_linked_portals"), params);
    }

    async deleteLayer (params) {
        return this.db.any(sql("data", "delete_layer"), params);
    }

    async addLayerDeleteRequest (params) {
        return this.db.any(sql("data", "add_layer_delete_request"), params);
    }

    async deleteLayerDeleteRequest (params) {
        return this.db.any(sql("data", "delete_layer_delete_request"), params);
    }

    async getCompleteLayerParams (params) {
        return this.db.any(sql("data", "get_complete_layer_params"), params);
    }

    async getCompleteLayerAllParams () {
        return this.db.any(sql("data", "get_complete_layer_all_params"));
    }

    async getCompleteLayerCollectionParams (params) {
        return this.db.any(sql("data", "get_complete_layer_collection_params"), params);
    }

    async getCompleteLayerServiceParams (params) {
        return this.db.any(sql("data", "get_complete_layer_service_params"), params);
    }

    async getCompleteLayerDatasetParams (params) {
        return this.db.any(sql("data", "get_complete_layer_dataset_params"), params);
    }

    async updateLayerJson (params) {
        return this.db.any(sql("data", "update_layer_json"), params);
    }

    async updateLayerJsonProd (params) {
        return this.db.any(sql("data", "update_layer_json_prod"), params);
    }

    async deleteLayerJsonProd (params) {
        return this.db.any(sql("data", "delete_layer_json_prod"), params);
    }

    async deletePortalsNotIncluded (params) {
        return this.db.any(sql("data", "delete_portals_not_included"), params);
    }

    async getPortalByUrl (params) {
        return this.db.any(sql("data", "get_portal_by_url"), params);
    }

    async deleteAllPortalLinks (params) {
        return this.db.any(sql("data", "delete_all_portal_links"), params);
    }

    async updatePortal (params) {
        return this.db.any(sql("data", "update_portal"), params);
    }

    async addPortal (params) {
        return this.db.any(sql("data", "add_portal"), params);
    }

    async deletePortal (params) {
        return this.db.any(sql("data", "delete_portal"), params);
    }

    async getPortals (params) {
        return this.db.any(sql("data", "get_portals"), params);
    }

    async addPortalLayerLink (params) {
        return this.db.any(sql("data", "add_portal_layer_link"), params);
    }

    async deletePortalLayerLink (params) {
        return this.db.any(sql("data", "delete_portal_layer_link"), params);
    }

    async updatePortalLinks (params) {
        const table = new this.pgp.helpers.TableName({table: "portal_layer", schema: "data"});
        const cs = new this.pgp.helpers.ColumnSet(["portal_id", "layer_id"], {table});
        const query = this.pgp.helpers.insert(params, cs);

        await this.db.none(query).catch(error => {
            Logger.error(error);
            return {error: error.message, code: error.code};
        });
    }

    async getDeleteLayerRequests () {
        return this.db.any(sql("data", "get_delete_layer_requests"));
    }

    async getComments (params) {
        return this.db.any(sql("data", "get_comments"), params);
    }

    async deleteComment (params) {
        return this.db.any(sql("data", "delete_comment"), params);
    }

    async addComment (params) {
        return this.db.any(sql("data", "add_comment"), params);
    }

    async updateComment (params) {
        return this.db.any(sql("data", "update_comment"), params);
    }

    async getContacts (params) {
        return this.db.any(sql("data", "get_contacts"), params);
    }

    async deleteContact (params) {
        return this.db.any(sql("data", "delete_contact"), params);
    }

    async addContact (params) {
        return this.db.any(sql("data", "add_contact"), params);
    }

    async updateContact (params) {
        return this.db.any(sql("data", "update_contact"), params);
    }

    async addContactLink (params) {
        return this.db.any(sql("data", "add_contact_link"), params);
    }

    async deleteContactLinks (params) {
        return this.db.any(sql("data", "delete_contact_links"), params);
    }

    async getDistinctDatasetKeywords () {
        return this.db.any(sql("data", "get_distinct_dataset_keywords"));
    }

    async getDistinctDatasetFilterKeywords () {
        return this.db.any(sql("data", "get_distinct_dataset_filter_keywords"));
    }

    async getDistinctServiceKeywords () {
        return this.db.any(sql("data", "get_distinct_service_keywords"));
    }

    async updateDatasetAllowedGroups (params) {
        return this.db.any(sql("data", "update_dataset_allowed_groups"), params);
    }

    async updateServiceAllowedGroups (params) {
        return this.db.any(sql("data", "update_service_allowed_groups"), params);
    }

    async addServiceLink (params) {
        return this.db.any(sql("data", "add_service_link"), params);
    }

    async getServiceLinks (params) {
        return this.db.any(sql("data", "get_service_links"), params);
    }

    async deleteServiceLink (params) {
        return this.db.any(sql("data", "delete_service_link"), params);
    }

    async getDatasetJiraTickets (params) {
        return this.db.any(sql("data", "get_jira_tickets"), params);
    }

    async addDatasetJiraTicket (params) {
        return this.db.any(sql("data", "add_jira_ticket"), params);
    }

    async deleteDatasetJiraTicket (params) {
        return this.db.any(sql("data", "delete_jira_ticket"), params);
    }

    async getLinkedJiraTickets (params) {
        return this.db.any(sql("data", "get_linked_jira_tickets"), params);
    }

    async checkIfCollectionIdExists (params) {
        return this.db.any(sql("data", "check_if_collection_id_exists"), params);
    }

    async checkIfServiceIdExists (params) {
        return this.db.any(sql("data", "check_if_service_id_exists"), params);
    }

    async checkIfLayerIdExists (params) {
        return this.db.any(sql("data", "check_if_layer_id_exists"), params);
    }

    async setLayerIdSeq () {
        return this.db.any(sql("data", "set_layer_id_seq"));
    }

    async setServiceIdSeq () {
        return this.db.any(sql("data", "set_service_id_seq"));
    }

    async setCollectionIdSeq () {
        return this.db.any(sql("data", "set_collection_id_seq"));
    }

    async setDbConnectionsIdSeq () {
        return this.db.any(sql("data", "set_db_connections_id_seq"));
    }

    async setMetadataCatalogsIdSeq () {
        return this.db.any(sql("data", "set_metadata_catalogs_id_seq"));
    }

    async setEtlProcessesIdSeq () {
        return this.db.any(sql("data", "set_etl_processes_id_seq"));
    }

    /**
     * import dataset with dependend ressources (collections, services, layers, keywords, comments)
     * @param {Object} dataset - all dataset parameters from external instance
     * @param {boolean} new_id - true, if new dataset id, collection ids, service ids and layer ids should be generated
     * @returns {integer} - dataset id
     */
    async importDataset (dataset, new_id) {
        const result = this.db.tx(async t => {
            const db_conenction_cs = new this.pgp.helpers.ColumnSet(["name", "host", "port", "database", "password", "db_user"]);

            if (dataset.db_connection_r_obj && dataset.db_connection_r_insert) {
                const db_conenction_query = this.pgp.helpers.insert(dataset.db_connection_r_obj, db_conenction_cs, {table: "db_connections", schema: "data"});

                await t.none(db_conenction_query);
            }

            if (dataset.db_connection_w_obj && dataset.db_connection_w_insert) {
                const db_conenction_query = this.pgp.helpers.insert(dataset.db_connection_w_obj, db_conenction_cs, {table: "db_connections", schema: "data"});

                await t.none(db_conenction_query);
            }

            if (dataset.metadata_catalog_obj && dataset.metadata_catalog_insert) {
                const metadata_catalog_cs = new this.pgp.helpers.ColumnSet(["name", "csw_url", "show_doc_url", "proxy", "srs_metadata", "use_in_internet_json"]);
                const metadata_catalog_query = this.pgp.helpers.insert(dataset.metadata_catalog_obj, metadata_catalog_cs, {table: "metadata_catalogs", schema: "data"});

                await t.none(metadata_catalog_query);
            }

            if (!config.clientConfig.datasetImportDev) {
                dataset.status = "prod";
            }

            if (dataset.allowed_groups) {
                dataset.allowed_groups = JSON.stringify(dataset.allowed_groups);
            }

            const datasets_column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'datasets' AND TABLE_SCHEMA = 'data';");
            const datasets_columns = [];

            // add id column to array, if new new ids should be generated
            if (!new_id) {
                datasets_columns.push("id");
            }

            // add all remaining columns to array
            datasets_column_names.forEach(c => {
                if (c.column_name !== "id") {
                    datasets_columns.push(c.column_name);
                }
            });

            const dataset_query = this.pgp.helpers.insert(dataset, datasets_columns, {table: "datasets", schema: "data"});

            // if new ids should be generated, get new dataset id from insert query
            if (new_id) {
                const insert_result = await t.one(dataset_query + "RETURNING id");

                dataset.id = insert_result.id;
            }
            else {
                await t.none(dataset_query);
            }

            if (dataset.etl_processes.length > 0) {
                const etl_processes_column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'etl_processes' AND TABLE_SCHEMA = 'data';");
                const etl_processes_columns = [];

                etl_processes_column_names.forEach(e => {
                    etl_processes_columns.push(e.column_name);
                });

                const etl_processes_cs = new this.pgp.helpers.ColumnSet(etl_processes_columns);

                // insert etl processes, if new ids should be generated, get next free etl_process id from sequence
                for (const etl_process of dataset.etl_processes) {
                    if (new_id) {
                        const new_etl_process_id = await t.one("SELECT nextval('data.etl_processes_id_seq'::regclass) AS etl_processes_id;");

                        etl_process.id = parseInt(new_etl_process_id.etl_processes_id);
                        etl_process.dataset_id = dataset.id;
                    }

                    if (etl_process.mapped_collections) {
                        etl_process.mapped_collections = JSON.stringify(etl_process.mapped_collections);
                    }

                    const etl_processes_query = this.pgp.helpers.insert(etl_process, etl_processes_cs, {table: "etl_processes", schema: "data"});

                    await t.none(etl_processes_query);
                }
            }
            if (dataset.comments.length > 0) {
                dataset.comments.forEach(c => {
                    c.dataset_id = dataset.id;
                });
                const comments_cs = new this.pgp.helpers.ColumnSet(["username", "comment", "post_date", "dataset_id", "important"]);
                const comments_query = this.pgp.helpers.insert(dataset.comments, comments_cs, {table: "comments", schema: "data"});

                await t.none(comments_query);
            }
            if (dataset.collections.length > 0) {
                const collections_column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'collections' AND TABLE_SCHEMA = 'data';");
                const collections_columns = [];

                collections_column_names.forEach(c => {
                    collections_columns.push(c.column_name);
                });

                const collections_cs = new this.pgp.helpers.ColumnSet(collections_columns);

                // insert collections, if new ids should be generated, get next free collection id from sequence
                for (const collection of dataset.collections) {
                    if (new_id) {
                        const new_collection_id = await t.one("SELECT nextval('data.collections_id_seq'::regclass) AS collection_id;");

                        // set new collection id for connected layers
                        for (const layer of dataset.layers) {
                            if (layer.collection_id === collection.id) {
                                layer.collection_id = parseInt(new_collection_id.collection_id);
                            }
                        }

                        collection.id = parseInt(new_collection_id.collection_id);
                        collection.dataset_id = dataset.id;
                    }

                    if (collection.attribute_config) {
                        collection.attribute_config = JSON.stringify(collection.attribute_config);
                    }
                    if (collection.style) {
                        collection.style = JSON.stringify(collection.style);
                    }
                    if (collection.style_sld_icons) {
                        collection.style_sld_icons = JSON.stringify(collection.style_sld_icons);
                    }

                    const collections_query = this.pgp.helpers.insert(collection, collections_cs, {table: "collections", schema: "data"});

                    await t.none(collections_query);
                }
            }
            if (dataset.services.length > 0) {
                const services_column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'services' AND TABLE_SCHEMA = 'data';");
                const services_columns = [];

                services_column_names.forEach(c => {
                    services_columns.push(c.column_name);
                });

                const services_cs = new this.pgp.helpers.ColumnSet(services_columns);
                const service_links_cs = new this.pgp.helpers.ColumnSet(["dataset_id", "service_id"]);

                // insert services, if new ids should be generated, get next free service id from sequence
                for (const service of dataset.services) {
                    if (service.allowed_groups) {
                        service.allowed_groups = JSON.stringify(service.allowed_groups);
                    }
                    if (!service.ignore) {
                        if (new_id) {
                            const new_service_id = await t.one("SELECT nextval('data.services_id_seq'::regclass) AS service_id;");

                            // set new service id for connected layers
                            for (const layer of dataset.layers) {
                                if (layer.service_id === service.id) {
                                    layer.service_id = parseInt(new_service_id.service_id);
                                }
                            }

                            service.id = parseInt(new_service_id.service_id);
                            service.dataset_id = dataset.id;
                        }

                        const services_query = this.pgp.helpers.insert(service, services_cs, {table: "services", schema: "data"});

                        await t.none(services_query);

                        const service_links_query = this.pgp.helpers.insert({dataset_id: service.dataset_id, service_id: service.id}, service_links_cs, {table: "service_links", schema: "data"});

                        await t.none(service_links_query);
                    }
                    else {
                        service.dataset_id = dataset.id;

                        const service_links_query = this.pgp.helpers.insert({dataset_id: service.dataset_id, service_id: service.id}, service_links_cs, {table: "service_links", schema: "data"});

                        await t.none(service_links_query);
                    }
                }
            }
            if (dataset.layers.length > 0) {
                const layers_column_names = await t.many("SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'layers' AND TABLE_SCHEMA = 'data';");
                const layers_columns = [];

                layers_column_names.forEach(c => {
                    layers_columns.push(c.column_name);
                });

                const layers_cs = new this.pgp.helpers.ColumnSet(layers_columns);

                // insert layers, if new ids should be generated, get next free layer id from sequence
                for (const layer of dataset.layers) {
                    if (new_id) {
                        const new_layer_id = await t.one("SELECT nextval('data.layers_id_seq'::regclass) AS layer_id;");

                        layer.id = parseInt(new_layer_id.layer_id);
                        layer.dataset_id = dataset.id;
                    }

                    const layers_query = this.pgp.helpers.insert(layer, layers_cs, {table: "layers", schema: "data"});

                    await t.none(layers_query);
                }
            }

            return dataset.id;
        }).then(data => {
            return data;
        }).catch(error => {
            Logger.error(error);
        });

        return result;
    }

    async getLinkedMetadataDatasets () {
        return this.db.any(sql("data", "get_linked_metadata_datasets"));
    }

    async updateMetadataDataset (params) {
        return this.db.any(sql("data", "update_metadata_dataset"), params);
    }

    async getLinkedMetadataServices () {
        return this.db.any(sql("data", "get_linked_metadata_services"));
    }

    async updateMetadataService (params) {
        return this.db.any(sql("data", "update_metadata_service"), params);
    }

    async getEtlProcesses (params) {
        return this.db.any(sql("data", "get_etl_processes"), params);
    }

    async getEtlProcessesByCollection (params) {
        return this.db.any(sql("data", "get_etl_processes_by_collection"), params);
    }

    async getEtlProcessById (params) {
        return this.db.one(sql("data", "get_etl_processes_by_id"), params);
    }

    async addEtlProcess (params) {
        return this.db.any(sql("data", "add_etl_process"), params);
    }

    async updateEtlProcess (params) {
        return this.db.any(sql("data", "update_etl_process"), params);
    }

    async deleteEtlProcess (params) {
        return this.db.any(sql("data", "delete_etl_process"), params);
    }

    async getConfigDataFromDb (params) {
        return this.db.one(sql("data", "get_config_data"), params);
    }

    async getDatasetsWithStagedAPIs (params) {
        return this.db.any(sql("data", "get_datasets_with_staged_apis"), params);
    }

    async updateDatasetAfterBulkImport (params) {
        return this.db.any(sql("data", "update_dataset_after_bulk_import"), params);
    }

    async getDatasetsForOaf (params) {
        return this.db.any(sql("data", "get_datasets_for_oaf"), params);
    }

    async getServicesForOaf (params) {
        return this.db.any(sql("data", "get_services_for_oaf"), params);
    }

    async deleteAllOaf (params) {
        return this.db.any(sql("data", "delete_all_oaf"), params);
    }

    async updateServiceStatus (params) {
        return this.db.none(sql("data", "update_service_status"), params);
    }

    async updateServiceStatusProd (params) {
        return this.db.none(sql("data", "update_service_status_prod"), params);
    }

    async updateDatasetsVersionLog (params) {
        return this.db.none(sql("data", "update_datasets_version_log"), params);
    }

    async addServiceConfigEdited (params) {
        return this.db.none(sql("data", "add_service_config_edited"), params);
    }

    async getServiceConfigsEdited (params) {
        return this.db.any(sql("data", "get_service_configs_edited"), params);
    }

    async getServiceConfigEdited (params) {
        return this.db.any(sql("data", "get_service_config_edited"), params);
    }

    async deleteServiceConfigEdited (params) {
        return this.db.any(sql("data", "delete_service_config_edited"), params);
    }

    async getDistinctMonth (params) {
        return this.db.any(sql("data", "get_distinct_month"), params);
    }

    async getVisits (params) {
        return this.db.any(sql("data", "get_visits"), params);
    }

    async getVisitsTop (params) {
        if (params.software === "Alle") {
            return this.db.any(sql("data", "get_visits_top"), params);
        }
        else {
            return this.db.any(sql("data", "get_visits_top_by_software"), params);
        }       
    }

    async getVisitsTotal () {
        return this.db.any(sql("data", "get_visits_total"));
    }

    async getSoftwareStats () {
        return this.db.any(sql("data", "get_software_stats"));
    }

    async getServiceStats () {
        return this.db.any(sql("data", "get_service_stats"));
    }

    async getLayerStats () {
        return this.db.any(sql("data", "get_layer_stats"));
    }

    async addChangeLog (params) {
        return this.db.any(sql("data", "add_change_log"), params);
    }

    async getChangeLog (params) {
        return this.db.any(sql("data", "get_change_log"), params);
    }

    async deleteServiceConfigEditedByUri (params) {
        return this.db.none(sql("data", "delete_service_config_edited_by_uri"), params);
    }

    async getDatasetVersionLogById (params) {
        return this.db.any(sql("data", "get_dataset_version_log_by_id"), params);
    }
}

module.exports = UDPMRepository;
