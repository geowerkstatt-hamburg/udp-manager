const sql = require("../sql");

class ExtInstanceRepository {
    constructor (db, pgp) {
        this.db = db;
        this.pgp = pgp;
    }

    /**
     * close existing db connection und establish new one with given parameters
     * @param {Object} connection_params - needed parameter to establish db connection
     * @returns {void}
     */
    async dbConnect (connection_params) {
        this.db.$pool.end();
        this.db = this.pgp(connection_params);
    }

    /**
     * search for datasets by id or name
     * @param {String} searchStr - id or (part of) name attribute
     * @returns {Object} - search results
     */
    async searchDataset (searchStr) {
        // if searchString is a Number, search by dataset id
        if (!isNaN(searchStr) && Number.isInteger(parseFloat(searchStr))) {
            const searchDatasetID = parseFloat(searchStr);
            const query = "SELECT DISTINCT ON (d.id, s.status_dev) d.*, s.status_dev AS service_status_dev, s.status_stage AS service_status_stage, s.status_prod AS service_status_prod FROM data.datasets d JOIN data.service_links AS sl ON d.id = sl.dataset_id JOIN data.services s ON sl.service_id = s.id WHERE d.id=$1";

            return this.db.any(query, [searchDatasetID]);
        }
        // if searchString is a string, search by dataset name
        else {
            const query = "SELECT DISTINCT ON (d.id, s.status_dev) d.*, s.status_dev AS service_status_dev, s.status_stage AS service_status_stage, s.status_prod AS service_status_prod FROM data.datasets d JOIN data.service_links AS sl ON d.id = sl.dataset_id JOIN data.services s ON sl.service_id = s.id WHERE d.title ILIKE '%$1:value%'";

            return this.db.any(query, [searchStr]);
        }
    }

    /**
     * get all relevant dataset attributes
     * @param {Integer} id - dataset ID
     * @returns {Object} - dataset and all depending resources
     */
    async getDatasetData (id) {
        const result = this.db.task("get-dataset-data", async t => {
            const dataset = await t.one("SELECT * FROM data.datasets WHERE id=$1", [id]);
            const services_query = "SELECT s.*, sl.dataset_id FROM data.services s LEFT JOIN data.service_links sl ON s.id=sl.service_id WHERE sl.dataset_id = $1";
            const layers_query = "SELECT * FROM data.layers WHERE dataset_id = $1";

            // Get all dataset data
            dataset.comments = await t.any("SELECT * FROM data.comments WHERE dataset_id = $1", [id]);
            dataset.collections = await t.any("SELECT * FROM data.collections WHERE dataset_id = $1", [id]);
            dataset.services = await t.any(services_query, [id]);

            for (const service of dataset.services) {
                const linked_datasets = await t.any("SELECT dataset_id FROM data.service_links WHERE service_id = $1", [service.id]);

                if (linked_datasets.length > 1) {
                    service.ignore = true;
                }
                else {
                    service.ignore = false;
                }
            }

            dataset.layers = await t.any(layers_query, [id]);
            dataset.etl_processes = await t.any("SELECT * FROM data.etl_processes WHERE dataset_id = $1", [id]);

            if (dataset.db_connection_r_id) {
                dataset.db_connection_r_obj = await t.one("SELECT * FROM data.db_connections WHERE id = $1", [dataset.db_connection_r_id]);
            }
            if (dataset.db_connection_w_id) {
                dataset.db_connection_w_obj = await t.one("SELECT * FROM data.db_connections WHERE id = $1", [dataset.db_connection_w_id]);
            }
            if (dataset.metadata_catalog_id) {
                dataset.metadata_catalog_obj = await t.one("SELECT * FROM data.metadata_catalogs WHERE id = $1", [dataset.metadata_catalog_id]);
            }

            return dataset;
        }).then(data => {
            return data;
        }).catch(error => {
            return {error: true, message: error};
        });

        return result;
    }

    async getDBSchemas (params) {
        return this.db.any(sql("datastore", "get_dbschemas"), params);
    }

    async getDBTables (params) {
        return this.db.any(sql("datastore", "get_dbtables"), params);
    }

    /**
     * Get all column names and their datatype for given db table. Also get the geometry type for geometry columns
     * @param {String} db_schema - Database schema name
     * @param {String} db_table - Database table name
     * @returns {Object} - Object with an array of column names and data types for each column of the DB table and an array of column names and geometry types for each geometry column.
     */
    async getAttributes (db_schema, db_table) {
        const query_datatype = await this.db.any("SELECT column_name, udt_name FROM information_schema.columns WHERE table_schema = $1 AND table_name = $2", [db_schema, db_table]);
        const query_geomtype = await this.db.any("SELECT f_geometry_column, type FROM geometry_columns WHERE f_table_schema = $1 AND f_table_name = $2", [db_schema, db_table]);

        return {query_datatype, query_geomtype};
    }

    /**
     * Get all the geometries of data
     * @param {String} db_schema - Database schema name
     * @param {String} db_table - Database table name
     * @param {String} crs - The target crs as EPSG-Code.
     * @returns {Object} - The geometries as geojson
     */
    async getGeometries (db_schema, db_table, crs) {
        const query_geomColumn = await this.db.any("SELECT f_geometry_column FROM geometry_columns WHERE f_table_schema = $1 AND f_table_name = $2", [db_schema, db_table]);
        const srid = await this.db.any(`SELECT Find_SRID('${db_schema}', '${db_table}', '${query_geomColumn[0].f_geometry_column}')`, [db_schema, db_table]);
        const sourceCrs = srid[0].find_srid;
        const targetCrs = crs.slice(5);
        const geom = query_geomColumn[0].f_geometry_column;
        let query_geomAsGeoJSON;

        query_geomAsGeoJSON = await this.db.any(`SELECT ST_AsGeoJSON(ST_Transform(ST_SetSRID(ST_EstimatedExtent('${db_schema}', '${db_table}', '${geom}'), ${sourceCrs}), ${targetCrs}))`, [db_schema, db_table]);

        if (query_geomAsGeoJSON[0].st_asgeojson === null) {
            query_geomAsGeoJSON = await this.db.any(`SELECT ST_AsGeoJSON(ST_Transform(ST_SetSRID(ST_Extent(${geom}),${sourceCrs}),${targetCrs})) FROM ${db_schema}.${db_table}`, [db_schema, db_table]);
        }

        return query_geomAsGeoJSON;
    }

    /**
     * Retuns the crs as epsg code.
     * @param {Object} db_dataCollection The database collection
     * @returns {String} The databse epsg code.
     */
    async getCrs (db_dataCollection) {
        const query_geomColumn = await this.db.any("SELECT f_geometry_column FROM geometry_columns WHERE f_table_schema = $1 AND f_table_name = $2", [db_dataCollection.db_schema, db_dataCollection.db_table]);
        const srid = await this.db.any(`SELECT Find_SRID('${db_dataCollection.db_schema}', '${db_dataCollection.db_table}', '${query_geomColumn[0].f_geometry_column}')`, [db_dataCollection.db_schema, db_dataCollection.db_table]);
        const epsgcode = "EPSG:" + srid[0].find_srid;

        return epsgcode;
    }


    /**
     * Get all attribute column names with their data type and explicit geom type for each collection
     * @param {String} db_schema - Database schema name
     * @param {String} db_table - Database table name
     * @returns {Object} - Array with attributes and their data type and explicit geom type for each collection.
     */
    async getAttributesForMapping (db_schema, db_table) {
        return this.db.any("SELECT i.column_name, i.udt_name AS data_type, gc.type AS geom_type FROM (SELECT column_name, udt_name FROM information_schema.columns WHERE table_schema = $1 AND table_name = $2) AS i LEFT JOIN (SELECT f_geometry_column, type FROM geometry_columns WHERE f_table_schema = $1 AND f_table_name = $2) AS gc ON i.column_name = gc.f_geometry_column;", [db_schema, db_table]);
    }

    async getFmeRepositories () {
        return this.db.any(sql("fmeserver", "get_repositories"));
    }

    async getFmeWorkspaces (params) {
        return this.db.any(sql("fmeserver", "get_workspaces"), params);
    }

    async getFmeWorkspaceDetails (params) {
        return this.db.one(sql("fmeserver", "get_workspacedetails"), params);
    }

    async getFmeAutomations (params) {
        return this.db.any(sql("fmeserver", "get_automations"), params);
    }

    async addDBSchema (params) {
        return this.db.none(sql("datastore", "add_dbschema"), params);
    }

    async addDBTable (params) {
        let sqlCmd = "CREATE TABLE IF NOT EXISTS " + params.schema + "." + params.table + "(";

        params.data_schema.forEach(attr => {
            if (attr.pk && (attr.attr_datatype === "number" || attr.attr_datatype === "integer")) {
                sqlCmd += attr.attr_name_db + " serial,";
            }
            else if (attr.attr_datatype === "number") {
                sqlCmd += attr.attr_name_db + " integer,";
            }
            else if (attr.attr_datatype === "text") {
                sqlCmd += attr.attr_name_db + " text,";
            }
            else if (attr.attr_datatype === "geometry [point]") {
                sqlCmd += attr.attr_name_db + " geometry(MultiPoint," + params.srs + "),";
            }
            else if (attr.attr_datatype === "geometry [line]") {
                sqlCmd += attr.attr_name_db + " geometry(MultiLineString," + params.srs + "),";
            }
            else if (attr.attr_datatype === "geometry [polygon]") {
                sqlCmd += attr.attr_name_db + " geometry(MultiPolygon," + params.srs + "),";
            }
            else {
                sqlCmd += attr.attr_name_db + " " + attr.attr_datatype + ",";
            }

            if (attr.pk) {
                sqlCmd += "CONSTRAINT " + params.table + "_pkey PRIMARY KEY (" + attr.attr_name_db + "),";
            }
        });

        sqlCmd = sqlCmd.slice(0, -1);
        sqlCmd += ");";

        sqlCmd += "GRANT UPDATE, INSERT, SELECT, DELETE, TRIGGER, TRUNCATE ON TABLE " + params.schema + "." + params.table + " TO " + params.user_w + ";";
        sqlCmd += "GRANT SELECT ON TABLE " + params.schema + "." + params.table + " TO " + params.user_r + ";";

        return this.db.none(sqlCmd);
    }
}

module.exports = ExtInstanceRepository;
