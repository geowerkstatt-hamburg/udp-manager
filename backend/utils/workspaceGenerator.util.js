const gitUtil = require("../utils/git.util");
const fs = require("fs").promises;
const path = require("path");
const dataCache = require("../utils/data_cache.util");
const Logger = require("../utils/logger.util");
const moment = require("moment");

module.exports = {
    execute: async function (env, servicesRestrictionLevel) {
        const baseDir = process.env.CONFIG_OUT_PATH;
        const config = require("../config.js").getConfig();

        try {
            await fs.rm(path.join(baseDir, "/workspaces/" + env + "/"), {recursive: true, force: true});

            // create list of software directories to iterate over
            const software_dirs = (await fs.readdir(path.join(baseDir, "/configs/" + env), {withFileTypes: true})).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);

            // if software has option seperateDevWorkspace === false, add software folder even if it has no configs under stage
            for (const softwareConf in config.software) {
                if (!config.software[softwareConf].seperateDevWorkspace && env === "stage") {
                    try {
                        await fs.access(path.join(baseDir, "/configs/dev/" + softwareConf + "/"));

                        software_dirs.push(softwareConf);
                    }
                    catch (err) {
                        Logger.debug("no configs found");
                    }
                }
            }

            for (const software_dir of software_dirs) {
                Logger.debug("seperateDevWorkspace: " + config.software[software_dir].seperateDevWorkspace);

                const configEnvs = [];

                // check if config folder exists and add to list to iterate over
                if ((config.software[software_dir].seperateDevWorkspace && env === "dev") || env === "stage" || env === "prod") {
                    try {
                        await fs.access(path.join(baseDir, "/configs/" + env + "/" + software_dir + "/"));

                        configEnvs.push(env);
                    }
                    catch (err) {
                        Logger.debug("no configs found");
                    }
                }
                if (!config.software[software_dir].seperateDevWorkspace && env === "stage") {
                    try {
                        await fs.access(path.join(baseDir, "/configs/dev/" + software_dir + "/"));

                        configEnvs.push("dev");
                    }
                    catch (err) {
                        Logger.debug("no configs found");
                    }
                }

                // for software configs with seperateDevWorkspace=false, workspace will not be generated
                for (const configEnv of configEnvs) {
                    Logger.debug("Prepare " + software_dir + " workspace with " + configEnv + " configs");

                    const id_dirs = (await fs.readdir(path.join(baseDir, "/configs/" + configEnv + "/" + software_dir), {withFileTypes: true})).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);

                    for (const id_dir of id_dirs) {
                        const fileListSrc = await this._getFilesRecursive(path.join(baseDir, "/configs/" + configEnv + "/" + software_dir + "/" + id_dir), []);
                        const workspace_dirs = (await fs.readdir(path.join(baseDir, "/configs/" + configEnv + "/" + software_dir + "/" + id_dir), {withFileTypes: true})).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);

                        for (const fileSrc of fileListSrc) {
                            const src = path.normalize(path.join(baseDir, "/configs/" + configEnv + "/" + software_dir + "/" + id_dir));
                            const dest = path.normalize(path.join(baseDir, "/workspaces/" + env + "/" + software_dir));
                            const fileDest = fileSrc.replace(src, dest);

                            // configs with present internalExternalWorkspace Element are organized in an internal and external workspaces. Internal workspace is a combination of external+internal folder
                            if (config.software[software_dir].internalExternalWorkspace && env !== "dev") {
                                const idFolder = servicesRestrictionLevel.find((element) => element.id === parseInt(id_dir));

                                if (idFolder) {
                                    const restrictionLevel = servicesRestrictionLevel.find((element) => element.id === parseInt(id_dir)).restriction_level;

                                    if (restrictionLevel === "intranet" || restrictionLevel === "internet") {
                                        const srcExt = path.normalize(path.join(baseDir, "/configs/" + configEnv + "/" + software_dir + "/" + id_dir + "/" + workspace_dirs[0]));
                                        const destExt = path.normalize(path.join(baseDir, "/workspaces/" + env + "/" + software_dir + "/" + config.software[software_dir].internalExternalWorkspace.internal));
                                        const fileDestExt = fileSrc.replace(srcExt, destExt);

                                        await fs.cp(fileSrc, fileDestExt, {recursive: true, force: true});
                                    }
                                    if (restrictionLevel === "internet") {
                                        const srcExt = path.normalize(path.join(baseDir, "/configs/" + configEnv + "/" + software_dir + "/" + id_dir + "/" + workspace_dirs[0]));
                                        const destExt = path.normalize(path.join(baseDir, "/workspaces/" + env + "/" + software_dir + "/" + config.software[software_dir].internalExternalWorkspace.external));
                                        const fileDestExt = fileSrc.replace(srcExt, destExt);

                                        await fs.cp(fileSrc, fileDestExt, {recursive: true, force: true});
                                    }
                                }
                            }
                            else {
                                await fs.cp(fileSrc, fileDest, {recursive: true, force: true});
                            }
                        }
                    }
                }
            }

            // iterate over all static_files subdirectories and copy all file to workspaces
            const static_software_dirs = (await fs.readdir(path.join(baseDir, "/static_files/" + env), {withFileTypes: true})).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);

            for (const static_software_dir of static_software_dirs) {
                const static_workspace_dirs = (await fs.readdir(path.join(baseDir, "/static_files/" + env + "/" + static_software_dir), {withFileTypes: true})).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);
                const static_workspace_files = (await fs.readdir(path.join(baseDir, "/static_files/" + env + "/" + static_software_dir), {withFileTypes: true})).filter(dirent => dirent.isFile()).map(dirent => dirent.name);

                // software configs with seperateDevWorkspace=false are directly copied to stage workspace
                if ((config.software[static_software_dir].seperateDevWorkspace && env === "dev") || env === "stage" || env === "prod") {
                    for (const static_workspace_file of static_workspace_files) {
                        const srcFile = path.join(baseDir, "/static_files/" + env + "/" + static_software_dir + "/" + static_workspace_file);
                        const destFile = path.join(baseDir, "/workspaces/" + env + "/" + static_software_dir + "/" + static_workspace_file);

                        await fs.cp(srcFile, destFile, {force: true});
                    }

                    for (const static_workspace_dir of static_workspace_dirs) {
                        const srcDir = path.join(baseDir, "/static_files/" + env + "/" + static_software_dir + "/" + static_workspace_dir);
                        const destDir = path.join(baseDir, "/workspaces/" + env + "/" + static_software_dir + "/" + static_workspace_dir);

                        let destExists = true;

                        try {
                            await fs.access(destDir);
                        }
                        catch (err) {
                            destExists = false;
                        }

                        if (destExists || static_software_dir === "MapServer") {
                            const fileListSrc = await this._getFilesRecursive(srcDir, []);

                            for (const fileSrc of fileListSrc) {
                                const src = path.normalize(srcDir);
                                const dest = path.normalize(destDir);
                                const fileDest = fileSrc.replace(src, dest);

                                await fs.cp(fileSrc, fileDest, {recursive: true, force: true});
                            }
                        }
                    }
                }
            }

            // iterate over all workspaces to write time.stamp file
            const workspace_software_dirs = (await fs.readdir(path.join(baseDir, "/workspaces/" + env), {withFileTypes: true})).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);

            for (const workspace_software_dir of workspace_software_dirs) {
                if (config.software[workspace_software_dir][env].instances.length === 0) {
                    const destDir = path.join(baseDir, "/workspaces/" + env + "/" + workspace_software_dir);

                    await fs.writeFile(path.join(destDir, "/time.stamp"), "Workspace zuletzt aktualisiert: " + moment().format("YYYY-MM-DD HH:mm:ss"));
                }
                else {
                    const workspace_dirs = (await fs.readdir(path.join(baseDir, "/workspaces/" + env + "/" + workspace_software_dir), {withFileTypes: true})).filter(dirent => dirent.isDirectory()).map(dirent => dirent.name);

                    for (const workspace_dir of workspace_dirs) {
                        const destDir = path.join(baseDir, "/workspaces/" + env + "/" + workspace_software_dir + "/" + workspace_dir);

                        await fs.writeFile(path.join(destDir, "/time.stamp"), "Workspace zuletzt aktualisiert: " + moment().format("YYYY-MM-DD HH:mm:ss"));
                    }
                }
            }

            if (config.modules.git) {
                // commit new workspaces
                await gitUtil.addCommit("new workspace configurations");
                await gitUtil.push(config.git.branch);
                dataCache.setChange(env, false);
            }

            return {status: "success"};
        }
        catch (error) {
            Logger.debug(error);

            return {status: "error", error: error};
        }
    },

    _getFilesRecursive: async function (dir, fileList) {
        try {
            const files = await fs.readdir(dir, {withFileTypes: true});
            const me = this;

            let list = fileList || [];

            for (const file of files) {
                if (file.isDirectory()) {
                    list = await me._getFilesRecursive(dir + "/" + file.name, list);
                }
                else {
                    list.push(path.normalize(path.join(dir, "/", file.name)));
                }
            }

            return list;
        }
        catch (error) {
            Logger.debug(error);

            return fileList;
        }
    }
};
