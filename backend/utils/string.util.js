module.exports = {
    removeTrailingWhitespacesInObject: (obj) => {
        for (const key of Object.keys(obj)) {
            const value = obj[key];

            if (typeof value === "string") {
                obj[key] = value.trim();
            }
        }

        return obj;
    }
};
