const _ = require("underscore");
const fs = require("fs").promises;
const path = require("path");
const jsonDiff = require("json-diff");
const Logger = require("./logger.util");

module.exports = {
    async generateAttrDiffLog (params) {
        const attr_diff = [];
        let old_attr_config = null;

        if (params.datasetVersionLog[0].data.collections.find(c => c.id === params.selected_collection_id)) {
            old_attr_config = params.datasetVersionLog[0].data.collections.find(c => c.id === params.selected_collection_id).attribute_config;
        }

        const diff = jsonDiff.diff(old_attr_config, params.selected_collection_attr_config, {full: false, color: true});

        if (!old_attr_config || old_attr_config.length === 0) {
            return null;
        }
        else if (!diff || diff.length === 0) {
            return {status: "no change detected", attr_diff: "<br>Die Attribute haben sich nicht geändert!", timestamp: params.datasetVersionLog[0].timestamp, attr_diff_json: null};
        }
        else {
            diff.forEach((entry, index) => {
                if (entry[0] === " ") {
                    attr_diff.push({[`Attribut ${index + 1}`]: "keine Änderungen"});
                }
                else if (entry[0] === "-") {
                    attr_diff.push({[`Attribut ${index + 1}`]: "Attribut gelöscht"});
                }
                else if (entry[0] === "+") {
                    attr_diff.push({[`Attribut ${index + 1}`]: "Attribut neu hinzugefügt"});
                }
                else {
                    const renamed_items = this.renameAttrKeys(entry[1]);

                    if (Object.keys(renamed_items).length !== 0) {
                        attr_diff.push({[`Attribut ${index + 1}`]: renamed_items});
                    }
                    else {
                        attr_diff.push({[`Attribut ${index + 1}`]: "keine Änderung"});
                    }
                }
            });

            const template = await this.prepareDiffObj(attr_diff);

            return {status: "change detected", attr_diff: template, timestamp: params.datasetVersionLog[0].timestamp, editor: params.datasetVersionLog[0].editor, attr_diff_json: attr_diff};
        }
    },

    async generateCollectionDiffLog (params) {
        const collection_diff = [];
        const old_collections = params.datasetVersionLog[0].data.collections;
        const diff = jsonDiff.diff(old_collections, params.collections, {full: true, color: true});

        if (!old_collections) {
            return null;
        }
        else if (!diff || diff.length === 0) {
            return {status: "no change detected", attr_diff: "<br>Die Collections haben sich nicht geändert!", timestamp: params.datasetVersionLog[0].timestamp, collections_diff_json: null};
        }
        else {
            // loop through diff array and generate a new array with renamed keys
            diff.forEach((entry) => {
                if (entry.length === 2) {
                    if (entry[0] === " ") {
                        // if the first element is a space, then the collection has no changes
                        const collectionID = entry[1].id;
                        const collectionTitle = entry[1].title;

                        collection_diff.push({[`${collectionTitle} (${collectionID})`]: "keine Änderungen"});
                    }
                    else if (entry[0] === "-") {
                        // if the first element is a minus sign, then the collection has been deleted
                        const collectionID = entry[1].id;
                        const collectionTitle = entry[1].title;

                        collection_diff.push({[`${collectionTitle} (${collectionID})`]: "Collection gelöscht"});
                    }
                    else if (entry[0] === "+") {
                        // if the first element is a plus sign, then the collection has been added
                        const collectionID = entry[1].id;
                        const collectionTitle = entry[1].title;

                        collection_diff.push({[`${collectionTitle} (${collectionID})`]: "Collection neu hinzugefügt"});
                    }
                    else {
                        // if the first element is ~, then collection attributes have been changed
                        const collectionID = entry[1].id;
                        let collectionTitle = entry[1].title;

                        if (typeof entry[1].title === "object") {
                            // if the title is an object, then the title has changed
                            collectionTitle = entry[1].title.__new;
                        }

                        // rename the collection keys to GUI labels
                        const renamed_items = this.renameCollectionKeys(entry[1]);

                        if (Object.keys(renamed_items).length !== 0) {
                            // if the renamed items are not empty, then add the renamed items to the collection_diff array
                            collection_diff.push({[`${collectionTitle} (${collectionID})`]: renamed_items});
                        }
                        else {
                            // if the renamed items are empty, then add a no change message to the collection_diff array
                            collection_diff.push({[`${collectionTitle} (${collectionID})`]: "keine Änderung"});
                        }
                    }
                }
            });

            const template = await this.prepareDiffObj(collection_diff);

            return {status: "change detected", collection_diff: template, timestamp: params.datasetVersionLog[0].timestamp, editor: params.datasetVersionLog[0].editor, collection_diff_json: collection_diff};
        }
    },

    renameAttrKeys: function (obj) {
        let newKey;
        const newKeysArray = [];
        const attr_map = {
            attr_name_source: "Name (Quelle)",
            attr_name_db: "Name (DB)",
            attr_name_service: "Name (API)",
            attr_name_masterportal: "Label",
            attr_datatype: "Datentyp",
            pk: "Primärschlüssel",
            primary_geom: "Primäre Geometrie",
            primary_date: "Primärer Zeitindex",
            rule_mandatory: "Pflicht",
            rule_filter_vis: "Filter für Visualisierung",
            rule_filter_data: "Filter für Daten",
            rule_order: "Attributreihenfolge",
            rule_date: "Datum",
            rule_georef: "Georeferenzierung",
            rule_note: "Anmerkungen",
            unit: "Einheit",
            description: "Beschreibung"
        };

        // iterate over the object keys
        // if the key is found in the attribute map, then rename the key
        Object.keys(obj).map(key => {
            if (attr_map[key]) {
                newKey = attr_map[key];

                // create a new object with the renamed key and the original value
                newKeysArray.push({[newKey]: obj[key]});
            }

            // return the new array of objects
            return newKeysArray;
        });

        return Object.assign({}, ...newKeysArray);
    },

    renameCollectionKeys: function (obj) {
        let newKey;
        const newKeysArray = [];
        const attr_map = {
            group_object: "Gruppenobjekt",
            name: "Name (technisch)",
            alternative_name: "Name (alternativ/extern)",
            title: "Titel",
            api_constraint: "API-Zugriff",
            description: "Beschreibung",
            legend_file_name: "Legendengrafik",
            legend_url_intranet: "Legenden URL (Intranet)",
            legend_url_internet: " Legenden URL (Internet)",
            transparency: "Transparenz",
            scale_min: "Min-Scale",
            scale_max: "Max-Scale",
            custom_bbox: "BBox (benutzerdefiniert)",
            namespace: "Namepspace",
            complex_scheme: "Komplexes Datenmodell",
            store_type: "Datentyp",
            tileindex: "Tileindex",
            db_schema: "Schema",
            db_table: "Tabelle",
            number_features: "Anzahl Features",
            additional_info: "zusätzliche Informationen",
            title_alt: "Name (alternativ/extern)",
            attribution: "Attribution",
            additional_categories: "zusätzliche Kategorien",
            service_url_visible: "Service URL sichtbar",
            gfi_theme: "GFI Theme",
            gfi_theme_params: "Theme Parameter (JSON)",
            gfi_as_new_window: "GFI als neues Fenster",
            gfi_window_specs: "GFI Fenster Parameter",
            gfi_disabled: "GFI deaktivieren",
            attribute_config: "Attributkonfiguration",
            style: "Stylekonfiguration",
            use_style_sld: "Manuelle Stylekonfiguration verwenden",
            style_sld: "Manuelle Stylekonfiguration",
            style_sld_icons: "Icons für Manuelle Stylekonfiguration",
            source_data_type: "Quelldatentyp",
            source_data_location: "Ablageort",
            source_data_name: "Dateiname",
            source_data_table: "Tabelle",
            source_data_schema: "Schema",
            source_data_query: "Filter",
            source_db_connection_id: "Datenbankverbindungs-ID"
        };

        // iterate over the object keys, if the value is an object, then it may be necessary to rename the key
        // the rename map is defined above and contains the keys that should be renamed
        Object.keys(obj).map(key => {
            if (obj[key]) {
                if (typeof obj[key] === "object") {
                    // if the key exists in the rename map, then rename it
                    if (attr_map[key]) {
                        newKey = attr_map[key];

                        // if the new key is one of the following, then the value is an array of objects and the
                        // value should be set to "editiert"
                        if (newKey === "Attributkonfiguration" ||
                            newKey === "Legendengrafik" ||
                            newKey === "Stylekonfiguration" ||
                            newKey === "Manuelle Stylekonfiguration" ||
                            newKey === "Icons für Manuelle Stylekonfiguration") {
                            if (Array.isArray(obj[key][0])) {
                                newKeysArray.push({[newKey]: "editiert"});
                            }
                        }
                        else if (newKey) {
                            // otherwise, just add the new key-value pair to the array
                            newKeysArray.push({[newKey]: obj[key]});
                        }
                    }
                }
            }

            return newKeysArray;
        });

        return Object.assign({}, ...newKeysArray);
    },

    prepareDiffObj: async function (diff) {
        const read_template_file = await fs.readFile(path.join(__dirname, "templates/diff_obj_html.txt"), {encoding: "utf8"});
        const template_data = {
            diff: diff
        };

        try {
            const template = _.template(read_template_file);
            const template_log = template(template_data).replace(/\s\s+/g, " ").trim();

            return template_log;
        }
        catch (e) {
            Logger.error(e);

            return "";
        }
    }
};
