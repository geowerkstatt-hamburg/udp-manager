const tunnel = require("tunnel");
const https = require("https");

module.exports = {
    getHttpsAgent: function (proxy) {
        if (proxy) {
            return tunnel.httpsOverHttp({
                proxy: proxy,
                rejectUnauthorized: false
            });
        }
        else {
            return new https.Agent({
                rejectUnauthorized: false
            });
        }
    }
};
