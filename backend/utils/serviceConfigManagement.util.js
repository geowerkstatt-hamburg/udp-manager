const Logger = require("./logger.util.js");
const path = require("path");
const fs = require("fs").promises;
const dataCache = require("./data_cache.util.js");
const gitUtil = require("./git.util.js");
const configuratorFactory = require("../configurator");
const moment = require("moment");

module.exports = {
    /**
     * Generates the configuration files for given services
     * First step - generate data object for the config generation process
     * Second step - generate the configuration files and upload them
     * @param {Object} params - service id, service title
     * @returns {Object} - status: success
     */
    async generateServiceConfig (params) {
        try {
            const me = this;
            const config = require("../config.js").getConfig();
            let workspaceEnv = "dev";

            // software configs with seperateDevWorkspace=false are directly copied to stage workspace
            if (!config.software[params.software].seperateDevWorkspace) {
                workspaceEnv = "stage";
            }

            // check if files must be moved to dev before generating
            let moveCommit = false;

            if (params.configDataObject.service.folder !== params.targetWorkspace || params.status_dev === false) {
                moveCommit = true;
            }

            // build src and dest paths
            const srcBase = path.join(process.env.CONFIG_OUT_PATH, "/configs/stage/" + params.software + "/" + params.service_id);
            let src = srcBase;
            const destBase = path.join(process.env.CONFIG_OUT_PATH, "/configs/dev/" + params.software + "/" + params.service_id);
            let dest = destBase;
            let destWS = path.join(process.env.CONFIG_OUT_PATH, "/workspaces/" + workspaceEnv + "/" + params.software);

            if (params.srcWorkspace && params.targetWorkspace) {
                src = path.join(src, "/" + params.srcWorkspace);
                dest = path.join(dest, "/" + params.targetWorkspace);
                destWS = path.join(destWS, "/" + params.targetWorkspace);
            }

            let srcExists = true;
            let file_list = [];
            const files_to_delete = [];

            // check if src exists and generate lists of files to delete that are not manually edited and files to move
            try {
                await fs.access(src);
            }
            catch (err) {
                srcExists = false;
            }

            if (srcExists) {
                file_list = await me._getFilesRecursive(src, []);

                for (const file of file_list) {
                    const do_not_delete = params.configDataObject.configsEdited.find(conf => conf.uri === file.replace(src, "").replace(/\\/g, "/").substring(1));

                    if (!do_not_delete) {
                        files_to_delete.push(file);
                    }
                }
            }

            try {
                params.configDataObject.service.folder = params.targetWorkspace;

                if (params.configDataObject.service.type === "wfs-t") {
                    params.configDataObject.service.type = "wfs";
                }

                if (params.configDataObject.service.type === "wms-time") {
                    params.configDataObject.service.type = "wms";
                }

                const configurator = configuratorFactory(params.configDataObject.service.software);

                let commit_message = "add service config to dev - " + params.title + " [" + params.service_id + "]";

                // create new service configs in dev folder
                if (!moveCommit) {
                    if (params.configDataObject.configsEdited.length === 0) {
                        await fs.rm(destBase, {recursive: true, force: true});
                    }
                    else {
                        for (const file of files_to_delete) {
                            await fs.rm(file, {force: true});
                        }
                    }
                }
                // move files from stage to dev and then generate new configs
                else {
                    commit_message = "move service config to dev - " + params.title + " [" + params.service_id + "]";

                    if (params.configDataObject.configsEdited.length === 0) {
                        await fs.rm(destBase, {recursive: true, force: true});

                        if (params.status_stage === true) {
                            await fs.rm(srcBase, {recursive: true, force: true});
                        }
                    }
                    else {
                        await fs.rm(destBase, {recursive: true, force: true});

                        for (const file of files_to_delete) {
                            await fs.rm(file, {force: true});
                        }

                        await fs.cp(src, dest, {recursive: true});
                    }
                }

                await configurator.storeService(params.configDataObject);

                await fs.cp(dest, destWS, {recursive: true, force: true});

                await fs.writeFile(path.join(destWS, "/time.stamp"), "Workspace zuletzt aktualisiert: " + moment().format("YYYY-MM-DD HH:mm:ss"));

                if (config.modules.git) {
                    const gitStatus = await gitUtil.addCommit(commit_message);

                    dataCache.incrementCommitCount();
                    if (params.status_stage) {
                        dataCache.setChange("stage", true);
                    }
                    dataCache.setChange("dev", true);

                    if (moveCommit) {
                        dataCache.setChange("stage", true);
                    }

                    return gitStatus;
                }
                else {
                    return {status: "success"};
                }
            }
            catch (error) {
                Logger.error("services.service - generateConfig:");
                Logger.error(error);
                return {error: true, message: error.message};
            }
        }
        catch (error) {
            Logger.error("services.service - generateConfig - DataObject:");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    deployServiceConfig: async function (params) {
        const config = require("../config.js").getConfig();

        try {
            let workspaceEnv = params.dest;

            // software configs with seperateDevWorkspace=false are directly copied to stage workspace
            if (!config.software[params.software].seperateDevWorkspace && params.dest === "dev") {
                workspaceEnv = "stage";
            }

            const srcBase = path.join(process.env.CONFIG_OUT_PATH, "/configs/" + params.src + "/" + params.software + "/" + params.service_id);
            let src = srcBase;
            const destBase = path.join(process.env.CONFIG_OUT_PATH, "/configs/" + params.dest + "/" + params.software + "/" + params.service_id);
            let dest = destBase;
            let destWS = path.join(process.env.CONFIG_OUT_PATH, "/workspaces/" + workspaceEnv + "/" + params.software);

            if (params.srcWorkspace && params.targetWorkspace) {
                src = path.join(src, "/" + params.srcWorkspace);
                dest = path.join(dest, "/" + params.targetWorkspace);
                destWS = path.join(destWS, "/" + params.targetWorkspace);
            }

            src = path.normalize(src);
            dest = path.normalize(dest);

            let srcExists = true;

            // check if src folder exists
            try {
                await fs.access(src);
            }
            catch (err) {
                srcExists = false;
            }

            if (srcExists) {
                // try delete dest folder first
                try {
                    await fs.rm(destBase, {recursive: true, force: true});
                }
                catch (err) {
                    Logger.debug("dest folder does not exist");
                }

                await fs.cp(src, dest, {recursive: true, force: true});

                if (config.software[params.software].internalExternalWorkspace && params.dest !== "dev") {
                    const idFolder = params.servicesRestrictionLevel.find((element) => element.id === parseInt(params.service_id));

                    if (idFolder) {
                        const restrictionLevel = params.servicesRestrictionLevel.find((element) => element.id === parseInt(params.service_id)).restriction_level;

                        if (restrictionLevel === "intranet" || restrictionLevel === "internet") {
                            const destWSinternal = path.join(process.env.CONFIG_OUT_PATH, "/workspaces/" + workspaceEnv + "/" + params.software) + "/" + config.software[params.software].internalExternalWorkspace.internal;

                            await fs.cp(src, destWSinternal, {recursive: true, force: true});
                            await fs.writeFile(path.join(destWSinternal, "/time.stamp"), "zuletzt aktualisiert: " + moment().format("YYYY-MM-DD HH:mm:ss"));
                        }
                        if (restrictionLevel === "internet") {
                            const destWSexternal = path.join(process.env.CONFIG_OUT_PATH, "/workspaces/" + workspaceEnv + "/" + params.software) + "/" + config.software[params.software].internalExternalWorkspace.external;

                            await fs.cp(src, destWSexternal, {recursive: true, force: true});
                            await fs.writeFile(path.join(destWSexternal, "/time.stamp"), "zuletzt aktualisiert: " + moment().format("YYYY-MM-DD HH:mm:ss"));
                        }

                    }
                }
                else {
                    await fs.cp(src, destWS, {recursive: true, force: true});
                    await fs.writeFile(path.join(destWS, "/time.stamp"), "Workspace zuletzt aktualisiert: " + moment().format("YYYY-MM-DD HH:mm:ss"));
                }

                if (config.modules.git) {
                    let gitStatus = {};

                    if (params.dest === "prod") {
                        const commit_message = "copy service config to prod - " + params.title + " [" + params.service_id + "]";

                        gitStatus = await gitUtil.addCommit(commit_message);

                        dataCache.incrementCommitCount();
                        dataCache.setChange("prod", true);

                        gitStatus.serviceStatus = {status_dev: false, status_stage: true, status_prod: true, folder: params.targetWorkspace, url_int: params.newUrlInt, url_ext: params.newUrlExt, id: params.service_id, server: params.server};
                    }
                    else {
                        await fs.rm(srcBase, {recursive: true, force: true});

                        const commit_message = "move service config to " + params.dest + " - " + params.title + " [" + params.service_id + "]";

                        gitStatus = await gitUtil.addCommit(commit_message);

                        dataCache.incrementCommitCount();
                        dataCache.setChange(params.src, true);
                        dataCache.setChange(params.dest, true);

                        gitStatus.serviceStatus = {status_dev: false, status_stage: true, status_prod: params.status_prod, folder: params.targetWorkspace, url_int: params.newUrlInt, url_ext: params.newUrlExt, id: params.service_id, server: params.server};
                    }

                    return gitStatus;
                }
                else {
                    const status = {status: "success"};

                    if (params.dest !== "prod") {
                        await fs.rm(srcBase, {recursive: true, force: true});

                        status.serviceStatus = {status_dev: false, status_stage: true, status_prod: params.status_prod, folder: params.targetWorkspace, url_int: params.newUrlInt, url_ext: params.newUrlExt, id: params.service_id, server: params.server};
                    }
                    else {
                        status.serviceStatus = {status_dev: false, status_stage: true, status_prod: true, folder: params.targetWorkspace, url_int: params.newUrlInt, url_ext: params.newUrlExt, id: params.service_id, server: params.server};
                    }

                    return status;
                }
            }
            else {
                return {status: "error", message: "could not find src folder"};
            }
        }
        catch (error) {
            Logger.error("services.service - deployServiceConfig");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    unDeployServiceConfig: async function (params) {
        const config = require("../config.js").getConfig();
        const configPath = path.join(process.env.CONFIG_OUT_PATH, "/configs/prod/" + params.software + "/" + params.service_id);

        try {
            await fs.rm(configPath, {recursive: true, force: true});

            if (config.modules.git) {
                const commit_message = "delete service config from prod - " + params.title + " [" + params.service_id + "]";
                const gitStatus = await gitUtil.addCommit(commit_message);

                dataCache.incrementCommitCount();
                dataCache.setChange("prod", true);

                gitStatus.serviceStatus = {status_dev: false, status_stage: true, status_prod: false, id: params.service_id};

                return gitStatus;
            }
            else {
                return {status: "success", serviceStatus: {status_dev: false, status_stage: true, status_prod: false, id: params.service_id}};
            }
        }
        catch (error) {
            Logger.error("services.service - unDeployServiceConfig");
            Logger.error(error);
            return {error: true, message: error.message};
        }
    },

    _getFilesRecursive: async function (dir, fileList) {
        const files = await fs.readdir(dir);
        const me = this;

        let list = fileList || [];

        for (let i = 0; i < files.length; i++) {
            const fileStat = await fs.stat(dir + "/" + files[i]);

            if (fileStat.isDirectory()) {
                list = await me._getFilesRecursive(dir + "/" + files[i], list);
            }
            else {
                list.push(path.normalize(path.join(dir, "/", files[i])));
            }
        }

        return list;
    }
};
