const winston = require("winston");
const env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";
const transports = [];

const displayStack = winston.format((info) => {
    info.message = info.stack ? info.stack : info.message;
    return info;
});

const logFormat = winston.format.printf(({level, message, timestamp}) => {
    return `${timestamp} -- ${level}: ${message}`;
});

if (env !== "dev") {
    transports.push(
        new winston.transports.Console({
            format: winston.format.combine(
                winston.format.timestamp(),
                logFormat
            )
        })
    );
}
else {
    transports.push(
        new winston.transports.Console({
            format: winston.format.combine(
                displayStack(),
                winston.format.cli()
            )
        })
    );
}

const LoggerInstance = winston.createLogger({
    level: process.env.LOG_LEVEL,
    levels: winston.config.npm.levels,
    format: winston.format.combine(
        winston.format.timestamp({
            format: "YYYY-MM-DD HH:mm:ss"
        }),
        winston.format.errors({stack: true}),
        winston.format.splat(),
        winston.format.json()
    ),
    transports
});

module.exports = LoggerInstance;
