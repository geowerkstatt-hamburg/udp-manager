const Logger = require("./logger.util");
// global defined id array, for simple id management
let ids_array = [];

module.exports = {
    /**
     * Scan Masterportal config.json and extract all layer-IDs
     * @param {*} webdav_conf - webdav config object from UDPM config.js
     * @param {*} portal - Masterportal folder object
     * @param {JSON} portal_config - Masterportal config.json content
     * @returns {JSON} - Object with title, url and Array including all found layer-IDs für given Masterportal instance
     */
    getApp: function (webdav_conf, portal, portal_config) {
        let ids = [];
        let title = "";

        if (portal_config.hasOwnProperty("Portalconfig")) {
            if (portal_config.Portalconfig.hasOwnProperty("portalTitle")) {
                if (portal_config.Portalconfig.portalTitle.title) {
                    title = portal_config.Portalconfig.portalTitle.title;
                }
            }
        }
        else if (portal_config.hasOwnProperty("portalConfig")) {
            if (portal_config.portalConfig.mainMenu.hasOwnProperty("title")) {
                if (portal_config.portalConfig.mainMenu.title.text) {
                    title = portal_config.portalConfig.mainMenu.title.text;
                }
            }
        }
        if (title.trim() === "") {
            title = portal.basename;
        }

        if (portal_config.hasOwnProperty("Themenconfig")) {
            // Masterportal 2 config.json
            // reset global id array
            ids_array = [];
            for (const key in portal_config.Themenconfig) {
                ids = ids.concat(this.getIds(portal_config.Themenconfig[key]));
            }
            // Eliminate double IDs by building new Set
            ids = [...new Set(ids)];

            if (ids.length > 0) {
                return {title: title, host: webdav_conf.full_qualified_domain, url: "/" + portal.basename, ids: ids, update_mode: "auto"};
            }
            else {
                Logger.debug("No layer ids found: " + portal.basename, webdav_conf.base_url + "/" + portal.basename);
                return {};
            }
        }
        else if (portal_config.hasOwnProperty("layerConfig")) {
            // Masterportal 3 config.json
            // reset global id array
            ids_array = [];

            for (const element of portal_config.layerConfig.baselayer.elements) {
                this.getIdElements(element);
            }

            for (const element of portal_config.layerConfig.subjectlayer.elements) {
                this.getIdElements(element);
            }

            // Eliminate double IDs by building new Set
            ids = [...new Set(ids_array)];

            if (ids.length > 0) {
                return {title: title, host: webdav_conf.full_qualified_domain, url: "/" + portal.basename, ids: ids, update_mode: "auto"};
            }
            else {
                Logger.debug("No layer ids found: " + portal.basename, webdav_conf.base_url + "/" + portal.basename);
                return {};
            }
        }
        else {
            Logger.debug("No Themenconfig or layerConfig found: " + portal.basename, webdav_conf.base_url + "/" + portal.basename);
            return {};
        }
    },

    /**
     * Recursively extracts and stores layer IDs from a given layer element.
     *
     * @param {Object} layerElement - The layer element to extract IDs from.
     * @return {void}
     */
    getIdElements: function (layerElement) {
        if (layerElement.type === "folder") {
            for (const element of layerElement.elements) {
                this.getIdElements(element);
            }
        }
        else {
            if (Array.isArray(layerElement.id)) {
                ids_array.concat(layerElement.id);
            }
            else {
                ids_array.push(layerElement.id);
            }
        }
    },

    /**
     * get all layer-IDs from top level themeconfig
     * @param {JSON} themeconfig - themeconfig Object from Masterportal config.json
     * @returns {Array} - array with all found layer-IDs
     */
    getIds: function (themeconfig) {
        let ids = [];

        if (themeconfig.hasOwnProperty("Layer")) {
            for (const layer of themeconfig.Layer) {
                ids = ids.concat(this.getIdsHelper(layer));
            }
        }
        if (themeconfig.hasOwnProperty("Ordner")) {
            this.getIdArrayHelper(themeconfig.Ordner, 0);

            ids = ids.concat(ids_array);
        }
        return ids;
    },

    /**
     * Get all layer-IDs from themeconfig ID array, recursive call for nested objects
     * @param {Array} folder_array - Array with layer-IDs
     * @param {Integer} index - index of array element to use
     * @returns {void}
     */
    getIdArrayHelper: function (folder_array, index) {
        if (index < folder_array.length) {
            if (folder_array[index].hasOwnProperty("Ordner")) {
                this.getIdArrayHelper(folder_array[index].Ordner, 0);
            }
            if (folder_array[index].hasOwnProperty("Layer")) {
                ids_array = ids_array.concat(this.getIdsHelper(folder_array[index].Layer));
            }
            this.getIdArrayHelper(folder_array, index + 1);
        }
    },

    /**
     * get layer-IDs from complex themeconfig
     * @param {JSON} id_obj - could be object or array
     * @returns {Array} - array with all found layer-IDs
     */
    getIdsHelper: function (id_obj) {
        let ids = [];

        if (Array.isArray(id_obj)) {
            for (const id of id_obj) {
                if (id.hasOwnProperty("id")) {
                    ids = ids.concat(this.addId(id.id));
                }
                else if (id.hasOwnProperty("layerId")) {
                    ids = ids.concat(this.addId(id.layerId));
                }
                else {
                    ids = ids.concat(this.addId(id));
                }

                if (id.hasOwnProperty("children")) {
                    ids = ids.concat(this.getIdsHelper(id.children));
                }
            }
        }
        else {
            if (id_obj.hasOwnProperty("id")) {
                if (id_obj.id.hasOwnProperty("id")) {
                    ids = ids.concat(this.addId(id_obj.id.id));
                    if (id_obj.id.id.hasOwnProperty("children")) {
                        ids = ids.concat(this.getIdsHelper(id_obj.id));
                    }
                }
                else if (id_obj.id.hasOwnProperty("layerId")) {
                    ids = ids.concat(this.addId(id_obj.id.layerId));
                }
                else {
                    if (Array.isArray(id_obj.id)) {
                        ids = ids.concat(this.getIdsHelper(id_obj.id));
                    }
                    else if (id_obj.id.hasOwnProperty("layerId")) {
                        ids = ids.concat(this.addId(id_obj.id.layerId));
                    }
                    else {
                        ids = ids.concat(this.addId(id_obj.id));
                    }
                    if (id_obj.hasOwnProperty("children")) {
                        ids = ids.concat(this.getIdsHelper(id_obj.children));
                    }
                }
            }
            else if (id_obj.hasOwnProperty("layerId")) {
                ids = ids.concat(this.addId(id_obj.layerId));
            }
        }
        return ids;
    },

    /**
     * check if given string is a integer
     * @param {String} str - possible number string
     * @returns {boolean} - returns true if given str is a integer
     */
    isInt: function (str) {
        if (typeof str !== "string") {
            return false;
        }
        return !isNaN(str) && Number.isInteger(parseFloat(str));
    },

    /**
     * extract layer-IDs from given structure
     * @param {Object} id - Integer, Array or JSON
     * @returns {Array} - Array with all found layer-IDs
     */
    addId: function (id) {
        const ids = [];

        if (this.isInt(id)) {
            ids.push(parseFloat(id));
        }
        else if (id.hasOwnProperty("layerId")) {
            ids.push(parseFloat(id.layerId));
        }
        else if (Array.isArray(id)) {
            for (let i = 0; i < id.length; i++) {
                if (Number.isInteger(parseFloat(id[i]))) {
                    ids.push(parseFloat(id[i]));
                }
                else if (id[i].hasOwnProperty("id")) {
                    ids.push(parseFloat(id[i].id));
                }
                else if (id[i].hasOwnProperty("layerId")) {
                    ids.push(parseFloat(id[i].layerId));
                }
            }
        }

        return ids;
    }
};
