
function _findAttributesByType (attributeMap, typeName) {
    const result = Object.keys(attributeMap).filter((k) => {
        const attribute = attributeMap[k];
        let dtype = attribute.dtype;

        if (typeof dtype === "object") {
            dtype = dtype.type; // temporary fix for https://www.jira.geoportal-hamburg.de/browse/UDH-819
        }
        dtype = dtype.toLowerCase();
        if (dtype.startsWith(typeName)) {
            return k;
        }
        else {
            return null;
        }
    });

    return result;
}

function _validateCollectionAttributes (collection) {
    const {attributeMap} = collection;
    const geometries = _findAttributesByType(attributeMap, "geometry");
    const dates = _findAttributesByType(attributeMap, "date");

    if (geometries.length > 0 && collection.mainGeom === null) {
        throw new Error(`<br> Collection '${collection.name}' enthält min. eine Geometrie - Bitte primäre Geometrie markieren!`);
    }

    if (dates.length > 0 && collection.primary_date === null) {
        throw new Error(`<br> Collection '${collection.name}' enthält min. ein Datum - Bitte primäres Datum markieren!`);
    }

    // throw error, if attributes from the attribute map do not occur in the database
    if (collection.missing_attr.length > 0) {
        let missing_attr_str = "";
        let explanation = "";

        for (const element of collection.missing_attr) {
            missing_attr_str += "Collection Id: " + collection.id + ", Attribute: " + Object.values(element) + ";";
        }

        if (collection.missing_attr[0].length === collection.attribute_config.length) {
            explanation = "<br><br> Wahrscheinlich fehlt eine lesende Berechtigung für den verwendeten DB Benutzer!";
        }

        throw new Error(`<br> Die Attributtabelle bei folgenden Collections beinhaltet Attribute, die nicht in der Datenbank existieren: <br> ${missing_attr_str} ${explanation}`);
    }

    // throw error, if data types are missing for the mapping in the software config
    if (collection.missing_datatypes.length > 0) {
        const dtypes_str = collection.missing_datatypes.join(", ");

        throw new Error(`Im Konfigurator nicht vorgesehene Postgres/PostGIS Datentypen :  + ${dtypes_str}`);
    }
}

function _validateCollectionSchemeTable (collection) {
    const collectionIsParsable = Boolean(collection.attribute_config && collection.db_schema && collection.db_table);

    if (!collectionIsParsable) {
        throw new Error(`Leere Attributtabelle, fehlender Schema- und/oder Tabellenname bei Collection ${collection.name}.`);
    }
}

function _validateCollectionStoreType (dataset, collection) {
    if (dataset.store_type === "Beides" && !collection.store_type) {
        throw new Error(`Fehlende Datentyp Deklaration bei Collection ${collection.name}.`);
    }
}

module.exports = {
    validate: function (data) {
        const {dataset, collections, service, connection} = data;

        if (!["wms", "wms-time", "wfs", "wfs-t", "oaf"].includes(service.type)) {
            throw new Error(`Schnittstellen vom Typ ${service.type} werden vom Konfigurator nicht unterstützt.`);
        }

        if (Object.keys(connection).length === 0) {
            throw new Error("Keine Datenbankverbindung vorhanden.");
        }

        for (const collectionName in collections) {
            const collection = collections[collectionName];

            if (!collection.group_object && !collection.complex_scheme) {
                _validateCollectionSchemeTable(collection);
                _validateCollectionAttributes(collection);
                _validateCollectionStoreType(dataset, collection);
            }
        }
    }
};
