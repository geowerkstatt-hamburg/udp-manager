const _ = require("underscore");
const xmldom = require("xmldom");
const DOMParser = xmldom.DOMParser;
const xpath = require("xpath");

module.exports = {
    /**
     * parse WFS Capabilities document and extract usefull attributes (title, fees, FeatureTypes...)
     * @param {String} capabilities - WFS Capabilities document as String
     * @param {boolean} check_version - true if it is intended to check if the service response with configured Version. ("true" used in update Layers workflow, "false" in check service workflow)
     * @param {String} version - WFS Version from DM database
     * @returns {Object} - JSON with all Capabilities Informations
     */
    parseWFSCapabilities: function (capabilities, check_version, version) {
        const capabilities_doc = new DOMParser().parseFromString(capabilities, "text/xml");
        const namespace_v2 = {"wfs": "http://www.opengis.net/wfs/2.0", "xlink": "http://www.w3.org/1999/xlink", "ows": "http://www.opengis.net/ows/1.1", "inspire_dls": "http://inspire.ec.europa.eu/schemas/inspire_dls/1.0"};

        let version_check = null;
        let namespace = {"wfs": "http://www.opengis.net/wfs", "xlink": "http://www.w3.org/1999/xlink", "ows": "http://www.opengis.net/ows", "inspire_dls": "http://inspire.ec.europa.eu/schemas/inspire_dls/1.0"};
        let crs_srs = "SRS";

        if (version === "2.0.0") {
            namespace = namespace_v2;
            crs_srs = "CRS";
        }

        const select = xpath.useNamespaces(namespace);

        if (check_version) {
            version_check = this._checkVersion(capabilities_doc, select, version);
        }
        // if version from DM datbase is 1.1.0 and capabilities version is 2.0.0 are different, use v2.0.0 namespaces. Importend for service checker module
        else if (version === "1.1.0" && this._getVersion(capabilities_doc, select) === "2.0.0") {
            namespace = namespace_v2;
            select = xpath.useNamespaces(namespace);
        }

        const featureTypes = select("//wfs:FeatureType", capabilities_doc);
        const title_cap = select("string(//ows:Title)", capabilities_doc);
        const fees_cap = select("string(//ows:Fees)", capabilities_doc);
        const accessconstraints_cap = select("string(//ows:AccessConstraints)", capabilities_doc);
        const collection_list = [];
        const epsg_list = [];
        const ext_cap_wfs = select("//inspire_dls:ExtendedCapabilities", capabilities_doc);
        const ext_cap_wfs_2 = select("//ows:ExtendedCapabilities", capabilities_doc);

        let inspire_ext_cap = false;

        if (ext_cap_wfs.length > 0 || ext_cap_wfs_2.length > 0) {
            inspire_ext_cap = true;
        }

        epsg_list.push(select("string(//wfs:Default" + crs_srs + ")", capabilities_doc));
        const otherCRS = select("//wfs:Other" + crs_srs, capabilities_doc);

        for (let i = 0; i < otherCRS.length; i++) {
            epsg_list.push(otherCRS[i].childNodes[0].data);
        }

        for (let i = 0; i < featureTypes.length; i++) {
            let group_layer = true;

            if (select("wfs:FeatureType", featureTypes[i]).length === 0) {
                group_layer = false;
            }

            let name = select("string(wfs:Name)", featureTypes[i]);
            let title = select("string(wfs:Title)", featureTypes[i]);

            const authority_url = false;
            const identifier = false;
            const metadata_url_wfs = select("string(wfs:MetadataURL)", featureTypes[i]);

            let metadata_uuid = "";

            if (metadata_url_wfs) {
                if (metadata_url_wfs.indexOf("id=") > -1 && metadata_url_wfs.indexOf("&") > -1) {
                    metadata_uuid = metadata_url_wfs.split("id=")[1].split("&")[0];
                }
            }

            const namespace_abk = name.split(":")[0];
            const namespace_url = namespace_abk + "=" + select("wfs:Name", featureTypes[i])[0]._nsMap[namespace_abk];

            if (namespace_url === undefined) {
                namespace_url = namespace_abk + "=" + select("/wfs:WFS_Capabilities", capabilities_doc)[0]._nsMap[namespace_abk];
            }

            if (name.includes(":")) {
                name = name.split(":")[1];
            }

            if (title.includes(":")) {
                title = title.split(":")[1];
            }

            collection_list.push({
                name: name,
                title: title,
                metadata_uuid: metadata_uuid,
                authority_url: authority_url,
                identifier: identifier,
                group_layer: group_layer,
                namespace: namespace_url ? namespace_url : "",
                output_format_wfs: "XML",
                scale_max: "2500000",
                scale_min: "0"
            });
        }

        return {
            title: title_cap,
            fees: fees_cap,
            accessconstraints_cap: accessconstraints_cap,
            inspire_ext_cap: inspire_ext_cap,
            collection_list: collection_list,
            epsg_list: _.uniq(epsg_list),
            version_check: version_check
        };
    },

    /**
     * parse WMS Capabilities document and extract usefull attributes (title, fees, FeatureTypes...)
     * @param {String} capabilities - WMS Capabilities document as String
     * @param {boolean} check_version - true if it is intended to check if the service response with configured Version. ("true" used in update Layers workflow, "false" in check service workflow)
     * @param {String} version - WMS Version from DM database
     * @returns {Object} - JSON with all Capabilities Informations
     */
    parseWMSCapabilities: function (capabilities, check_version, version) {
        const capabilities_doc = new DOMParser().parseFromString(capabilities, "text/xml");
        const namespace = {"wms": "http://www.opengis.net/wms", "inspire_vs": "http://inspire.ec.europa.eu/schemas/inspire_vs/1.0", "xlink": "http://www.w3.org/1999/xlink"};
        const select = xpath.useNamespaces(namespace);
        const config = require("../config.js").getConfig();

        let ns_prefix = "wms:";
        let crs_srs = "CRS";
        let version_check = null;

        if (check_version) {
            version_check = this._checkVersion(capabilities_doc, select, version);
        }

        if (version === "1.1.1") {
            ns_prefix = "";
            crs_srs = "SRS";
        }

        const featureTypes = select("//" + ns_prefix + "Layer", capabilities_doc);
        const title_cap = select("string(//" + ns_prefix + "Title)", capabilities_doc);
        const fees_cap = select("string(//" + ns_prefix + "Fees)", capabilities_doc);
        const accessconstraints_cap = select("string(//" + ns_prefix + "AccessConstraints)", capabilities_doc);
        const collection_list = [];
        const epsg_list = [];
        const ext_cap_wms = select("//inspire_vs:ExtendedCapabilities", capabilities_doc);

        let inspire_ext_cap = false;

        if (ext_cap_wms.length > 0) {
            inspire_ext_cap = true;
        }

        const boundingBoxes = select("//" + ns_prefix + "BoundingBox", capabilities_doc);

        for (let i = 0; i < boundingBoxes.length; i++) {
            epsg_list.push(select("string(@" + crs_srs + ")", boundingBoxes[i]));
        }

        for (let i = 0; i < featureTypes.length; i++) {
            let group_layer = true;

            if (select(ns_prefix + "Layer", featureTypes[i]).length === 0) {
                group_layer = false;
            }

            const name = select("string(" + ns_prefix + "Name)", featureTypes[i]);
            const title = select("string(" + ns_prefix + "Title)", featureTypes[i]);

            let metadata_uuid = "";
            let authority_url = false;
            let identifier = false;
            let scale_min = "0";
            let scale_max = "2500000";

            const metadata_url_wms = select("string(" + ns_prefix + "MetadataURL/" + ns_prefix + "OnlineResource/@xlink:href)", featureTypes[i]);
            const authority_url_el = select("//" + ns_prefix + "AuthorityURL", featureTypes[i]);
            const identifier_el = select("//" + ns_prefix + "Identifier", featureTypes[i]);

            if (authority_url_el.length > 0) {
                authority_url = true;
            }

            if (identifier_el.length > 0) {
                identifier = true;
            }

            if (metadata_url_wms) {
                if (metadata_url_wms.indexOf("id=") > -1 && metadata_url_wms.indexOf("&") > -1) {
                    metadata_uuid = metadata_url_wms.split("id=")[1].split("&")[0];
                }
            }

            if (version === "1.1.1") {
                if (select("string(ScaleHint)", featureTypes[i]).length !== 0) {
                    const scaleHint_min = select("string(ScaleHint/@min)", featureTypes[i]);
                    const scaleHint_max = select("string(ScaleHint/@max)", featureTypes[i]);

                    scale_min = parseFloat(scaleHint_min) * 2386.335;

                    if (config.applyDpiFactor) {
                        scale_min = parseInt(parseFloat(scale_min) * (96 / (25.4 / 0.28)));
                    }
                    if (scaleHint_max === "Infinity") {
                        scale_max = "2500000";
                    }
                    else {
                        scale_max = parseFloat(scaleHint_max) * 2386.335;
                        if (config.applyDpiFactor) {
                            scale_max = parseInt(parseFloat(scale_max) * (96 / (25.4 / 0.28)));
                        }
                    }
                }
            }
            else {
                if (select("string(wms:MinScaleDenominator)", featureTypes[i]).length !== 0) {
                    scale_min = select("string(wms:MinScaleDenominator)", featureTypes[i]);
                    if (scale_min.indexOf("e+") > 0) {
                        scale_min = String(Number(scale_min.replace("e+", "E")));
                    }
                    if (config.applyDpiFactor) {
                        scale_min = String(parseInt(parseFloat(scale_min) * (96 / (25.4 / 0.28))));
                    }
                }
                if (select("string(wms:MaxScaleDenominator)", featureTypes[i]).length !== 0) {
                    scale_max = select("string(wms:MaxScaleDenominator)", featureTypes[i]);
                    if (scale_max.indexOf("e+") > 0) {
                        scale_max = String(Number(scale_max.replace("e+", "E")));
                    }
                    if (config.applyDpiFactor) {
                        scale_max = String(parseInt(parseFloat(scale_max) * (96 / (25.4 / 0.28))));
                    }
                }
            }

            collection_list.push({
                name: name,
                title: title,
                metadata_uuid: metadata_uuid,
                authority_url: authority_url,
                identifier: identifier,
                group_layer: group_layer,
                namespace: "",
                output_format_wms: "image/png",
                scale_max: scale_max.replace(new RegExp(",", "g"), "."),
                scale_min: scale_min.replace(new RegExp(",", "g"), ".")
            });
        }

        return {
            title: title_cap,
            fees: fees_cap,
            access_constraints: accessconstraints_cap,
            inspire_ext_cap: inspire_ext_cap,
            collection_list: collection_list,
            epsg_list: _.uniq(epsg_list),
            version_check: version_check
        };
    },

    /**
     * parse WMTS Capabilities document and extract usefull attributes (title, fees, FeatureTypes...)
     * @param {String} capabilities - WMTS Capabilities document as String
     * @param {boolean} check_version - true if it is intended to check if the service response with configured Version. ("true" used in update Layers workflow)
     * @param {String} version - WMTS Version from DM database
     * @returns {Object} - JSON with all Capabilities Informations
     */
    parseWMTSCapabilities: function (capabilities, check_version, version) {
        const capabilities_doc = new DOMParser().parseFromString(capabilities, "text/xml");
        const namespace = {"wmts": "http://www.opengis.net/wmts/1.0", "ows": "http://www.opengis.net/ows/1.1"};
        const select = xpath.useNamespaces(namespace);

        let version_check = null;

        if (check_version) {
            version_check = this._checkVersion(capabilities_doc, select, version);
        }

        const featureTypes = select("//wmts:Layer", capabilities_doc);
        const title_cap = select("string(//ows:Title)", capabilities_doc);
        const fees_cap = select("string(//ows:Fees)", capabilities_doc);
        const accessconstraints_cap = select("string(//ows:AccessConstraints)", capabilities_doc);
        const inspire_ext_cap = false;
        const collection_list = [];

        for (let i = 0; i < featureTypes.length; i++) {
            const name = select("string(ows:Identifier)", featureTypes[i]);
            const title = select("string(ows:Title)", featureTypes[i]);
            const scale_min = "0";
            const scale_max = "2500000";

            collection_list.push({
                name: name,
                title: title,
                metadata_uuid: "",
                authority_url: "",
                identifier: "",
                group_layer: "",
                namespace: "",
                output_format: "image/png",
                scale_max: scale_max,
                scale_min: scale_min
            });
        }
        return {
            title: title_cap,
            fees: fees_cap,
            access_constraints: accessconstraints_cap,
            inspire_ext_cap: inspire_ext_cap,
            collection_list: collection_list,
            epsg_list: [],
            version_check: version_check
        };
    },

    /**
     * extraxt string value from version attribute
     * @param {Object} capabilities_doc - parsed XML Capabilities document
     * @param {Object} select - XPath Query Object
     * @returns {String} - service version
     */
    _getVersion: function (capabilities_doc, select) {
        return select("string(/*/@version)", capabilities_doc);
    },

    /**
     * checks if version from Capabilities matches version from DM database
     * @param {Object} capabilities_doc - parsed XML Capabilities document
     * @param {Object} select - XPath Query Object
     * @param {String} version - version from DM database
     * @returns {boolean} - true if version matches
     */
    _checkVersion: function (capabilities_doc, select, version) {
        var cap_version = this._getVersion(capabilities_doc, select);

        if (version === cap_version) {
            return true;
        }
        else {
            return false;
        }
    }
};
