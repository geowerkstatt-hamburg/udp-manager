const xmldom = require("xmldom");
const DOMParser = xmldom.DOMParser;
const xpath = require("xpath");

module.exports = {
    _getNamespaces: function () {
        return {"xsd": "http://www.w3.org/2001/XMLSchema", "l": "http://www.deegree.org/layers/base", "tilelayer": "http://www.deegree.org/layers/tile"};
    },

    parseServiceWFS: function (text) {
        const doc = new DOMParser().parseFromString(text, "text/xml");
        const featureStoreIds = [];

        for (let i = 0; i < doc.getElementsByTagName("FeatureStoreId").length; i++) {
            featureStoreIds.push(doc.getElementsByTagName("FeatureStoreId")[i].childNodes[0].nodeValue);
        }

        return {featureStoreIds: featureStoreIds};
    },

    parseServiceWMS: function (text) {
        const doc = new DOMParser().parseFromString(text, "text/xml");
        const themeId = doc.getElementsByTagName("ThemeId")[0].childNodes[0].nodeValue;

        return {themeId: themeId};
    },

    parseTheme: function (text) {
        const doc = new DOMParser().parseFromString(text, "text/xml");
        const layerStoreIds = [];

        for (let i = 0; i < doc.getElementsByTagName("LayerStoreId").length; i++) {
            layerStoreIds.push(doc.getElementsByTagName("LayerStoreId")[i].childNodes[0].nodeValue);
        }

        return {layerStoreIds: layerStoreIds};
    },

    parseLayer: function (text) {
        const doc = new DOMParser().parseFromString(text, "text/xml");
        const select = xpath.useNamespaces(this._getNamespaces());

        if (text.indexOf("RemoteWMSLayers") > -1) {
            const remoteWMSId = doc.getElementsByTagName("RemoteWMSId")[0].childNodes[0].nodeValue;
            const layername = doc.getElementsByTagName("Name")[0].childNodes[0].nodeValue;

            return {type: "RemoteWMSLayer", name: layername, layerStoreId: null, datasource: remoteWMSId, sqlFeatureStore: false, remoteOWS: true, tileStore: false, styles: [], jdbcId: "", tileMatrixSet: "", appschema: []};
        }
        else if (text.indexOf("TileLayers") > -1) {
            const layername = select("//l:Name/text()", doc)[0].nodeValue;
            const tileStoreId = select("//tilelayer:TileDataSet/@tileStoreId", doc)[0].value;

            return {type: "TileLayer", name: layername, layerStoreId: null, datasource: tileStoreId, sqlFeatureStore: false, remoteOWS: false, tileStore: true, styles: [], jdbcId: "", tileMatrixSet: "", appschema: []};
        }
        else if (text.indexOf("CoverageLayers") > -1) {
            const layername = select("//l:Name/text()", doc)[0].nodeValue;
            const coverageStoreId = doc.getElementsByTagName("CoverageStoreId")[0].childNodes[0].nodeValue;

            return {type: "CoverageLayer", name: layername, layerStoreId: null, datasource: coverageStoreId, sqlFeatureStore: false, remoteOWS: false, tileStore: true, styles: [], jdbcId: "", tileMatrixSet: "", appschema: []};
        }
        else {
            const layername = select("//l:Name/text()", doc)[0].nodeValue;
            const featureStoreId = doc.getElementsByTagName("FeatureStoreId")[0].childNodes[0].nodeValue;
            const styles = [];
            const styleStoreIdRoot = select("//l:StyleStoreId/text()", doc);
            const styleLength = styleStoreIdRoot.length;

            for (let i = 0; i < styleLength; i++) {
                styles.push({styleId: styleStoreIdRoot[i].nodeValue});
            }

            return {type: "FeatureLayer", name: layername, layerStoreId: null, datasource: featureStoreId, sqlFeatureStore: true, remoteOWS: false, tileStore: false, styles: styles, jdbcId: "", tileMatrixSet: "", appschema: []};
        }
    },

    parseFeatureStore: function (text) {
        const doc = new DOMParser().parseFromString(text, "text/xml");

        const jdbcId = doc.getElementsByTagName("JDBCConnId")[0].childNodes[0].nodeValue;

        return {jdbcId: jdbcId};
    },

    parseWFSFeatureStore: function (text, featureStoreId, featureTypes, featureStoreRaw, featureStoreUri) {
        const doc = new DOMParser().parseFromString(text, "text/xml");
        const appschemas = [];

        const jdbcId = doc.getElementsByTagName("JDBCConnId")[0].childNodes[0].nodeValue;

        if (doc.getElementsByTagName("GMLSchema")[0]) {
            var gmlSchema = doc.getElementsByTagName("GMLSchema")[0].childNodes[0].nodeValue;

            if (gmlSchema.indexOf("http") > -1) {
                appschemas.push(gmlSchema);
            }
            else {
                appschemas.push(gmlSchema.split("appschemas/")[1]);
            }
        }
        if (text.indexOf("BLOBMapping") > 0) {
            featureTypes.push({type: "FeatureType", name: "blob", layer: "", datasource: featureStoreId, sqlFeatureStore: true, remoteOWS: false, tileStore: false, style: "", jdbcId: jdbcId, appschema: appschemas, featureStoreRaw: featureStoreRaw, featureStoreUri: featureStoreUri});
        }
        else {
            for (var i = 0; i < doc.getElementsByTagName("FeatureTypeMapping").length; i++) {
                var featureName = doc.getElementsByTagName("FeatureTypeMapping")[i].getAttribute("name");

                featureTypes.push({type: "FeatureType", name: featureName, layer: "", datasource: featureStoreId, sqlFeatureStore: true, remoteOWS: false, tileStore: false, style: "", jdbcId: jdbcId, appschema: appschemas, featureStoreRaw: featureStoreRaw, featureStoreUri: featureStoreUri});
            }
        }

        return featureTypes;
    }
};
