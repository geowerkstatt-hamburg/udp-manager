/**
 * returns array of intersecting datasets for two input arrays
 * @param {Array} a - first array to intersect
 * @param {Array} b - seceond array to intersect
 * @returns {Array} - Array with intersecting objects
 */
module.exports = (a, b) => {
    const results = [];

    for (let i = 0; i < a.length; i++) {
        var existsInB = false;

        for (let j = 0; j < b.length; j++) {
            if (a[i].name === b[j].name) {
                existsInB = true;
                break;
            }
        }

        if (existsInB) {
            results.push(a[i]);
        }
    }

    return results;
};
