module.exports = {
    /**
     * Setting required default values und return layer json objects
     * 
     * @param {JSON} layer_params - all params (layer, collection, service, dataset) that are necessary to generate die layer json object
     * @param {JSON} metadata_catalog_repl - JSON Array with 1 or 0 Objects of metadata catalogs to replace urls in internet json objects
     * @returns {JSON} - JSON with 4 layer json objects (intranet/intranet_es/internet/internet_es)
     */
    generateLayerJson: function (layer_params, metadata_catalog_repl, prod) {
        const {layerAttributesExcludes} = require("../config.js").getConfig();

        // Setting default values
        const scale_min = layer_params.collection_params.scale_min ? layer_params.collection_params.scale_min : "0";
        const scale_max = layer_params.collection_params.scale_max ? layer_params.collection_params.scale_max : "2500000";
        const layerattribution = layer_params.collection_params.attribution ? layer_params.collection_params.attribution : "nicht vorhanden";
        const gfi_format = layer_params.st_attributes.gfi_format ? layer_params.st_attributes.gfi_format : "text/xml";
        const cache = layer_params.service_params.software === "GWC" ? true : false;
        const title = layer_params.collection_params.title_alt ? layer_params.collection_params.title_alt : layer_params.collection_params.title;
        const rootel = layer_params.st_attributes.rootel ? layer_params.st_attributes.rootel : "Things";
        const related_wms_layers = layer_params.st_attributes.related_wms_layers ? layer_params.st_attributes.related_wms_layers.split(",") : [];
        const singletile = layer_params.st_attributes.single_tile ? layer_params.st_attributes.single_tile : false;
        const tilesize = layer_params.st_attributes.tile_size ? layer_params.st_attributes.tile_size : 512;
        const transparency = layer_params.collection_params.transparency ? layer_params.collection_params.transparency : 0;
        const transparent = layer_params.st_attributes.transparent ? layer_params.st_attributes.transparent : true;
        const gfi_as_new_window = layer_params.collection_params.gfi_as_new_window ? layer_params.collection_params.gfi_as_new_window : false;
        const service_url_is_visible = layer_params.collection_params.service_url_visible;
        const gfi_complex = layer_params.collection_params.gfi_complex ? layer_params.collection_params.gfi_complex : false;
        const gutter = layer_params.st_attributes.gutter ? layer_params.st_attributes.gutter : 0;
        const featurecount = layer_params.st_attributes.feature_count ? layer_params.st_attributes.feature_count : 5;
        const notsupportedfor3d = layer_params.st_attributes.notsupportedfor3d ? layer_params.st_attributes.notsupportedfor3d : false;
        const epsg = layer_params.st_attributes.epsg ? layer_params.st_attributes.epsg : 'EPSG:4326';
        const load_things_only_in_current_extent = layer_params.st_attributes.load_things_only_in_current_extent ? layer_params.st_attributes.load_things_only_in_current_extent : true;
        const visibility = layer_params.st_attributes.visibility ? layer_params.st_attributes.visibility : false;
        const request_vertex_normals = layer_params.st_attributes.request_vertex_normals ? layer_params.st_attributes.request_vertex_normals : false;
        
        let legend_url_intranet = layer_params.collection_params.legend_url_intranet ? layer_params.collection_params.legend_url_intranet : "";
        let legend_url_internet = layer_params.collection_params.legend_url_internet ? layer_params.collection_params.legend_url_internet : "";
        let url = layer_params.service_params.url_ext;

        if (prod) {
            url = layer_params.service_params.url_ext_prod;
            if (legend_url_intranet !== "") {
                const legend_url_intranet_new = legend_url_intranet.replace("qs-geodienste", "geodienste");
                legend_url_intranet = legend_url_intranet_new;
            }
            if (legend_url_internet !== "") {
                const legend_url_internet_new = legend_url_internet.replace("qs-geodienste", "geodienste");
                legend_url_internet = legend_url_internet_new;
            }
        }

        if (layer_params.service_params.external) {
            url = layer_params.service_params.url_ext_prod;
        }
        
        if (layer_params.service_params.type === "GeoJSON") {
            if (layer_params.collection_params.alternative_name) {
                url = url + "/" + layer_params.collection_params.alternative_name
            }
            else {
                url = url + "/" + layer_params.collection_params.name
            }
        }

        let time_series = layer_params.st_attributes.time_series ? layer_params.st_attributes.time_series : false;
        let type = layer_params.service_params.type;

        if (layer_params.service_params.type === "WFS-T") {
            type = "WFS";
        }

        if (layer_params.service_params.type === "WMS-Time") {
            type = "WMS";
        }

        if (layer_params.service_params.type === "STA") {
            type = "SensorThings";
        }
        

        let namespace = layer_params.collection_params.namespace ? layer_params.collection_params.namespace : "nicht vorhanden";
        let output_format = layer_params.st_attributes.output_format ? layer_params.st_attributes.output_format : null;

        if (!output_format) {
            if (layer_params.service_params.type === "WMS" || layer_params.service_params.type === "WMS-Time" || layer_params.service_params.type === "Oblique") {
                output_format = "image/png";
            }
            else {
                output_format = "XML";
            }
        }

        let gfi_config = "showAll";
        const attribute_config = layer_params.collection_params.attribute_config;        

        if (Array.isArray(attribute_config)) {
            if (attribute_config.length > 0) {
                // if no attribute is to be ignored in the portal, it is checked whether the attribute names of the service and portal are the same
                const attr_portal_names = attribute_config.map(v => v.attr_name_masterportal);
                const filter = attr_portal_names.filter((name) => name !== null);
                
                // check if all portal names are null
                if (filter.length > 0) {
                    gfi_config = {};
                    // link attribute service name with attribute portal name for gfi_config and outfilters the ignored attributes
                    for (let i = 0; i < attribute_config.length; i++) {
                        if (attribute_config[i].attr_name_service === "ignore" || attribute_config[i].attr_name_service === null || attribute_config[i].attr_name_masterportal === "ignore" || attribute_config[i].attr_name_masterportal === null || attribute_config[i].attr_name_masterportal_ignore || layerAttributesExcludes.includes(attribute_config[i].attr_name_service)) {
                            continue;
                        }
                        else {
                            gfi_config[attribute_config[i].attr_name_service] = attribute_config[i].attr_name_masterportal;
                        }
                    }
                }
            }
        }

        // check if the gfi_config object is empty, ergo all portal names have been set to ignore and the whole GFI should be ignored
        if ((Object.keys(gfi_config).length === 0 && gfi_config.constructor === Object) || layer_params.collection_params.gfi_disabled) {
            gfi_config = "ignore";
        }

        let gfi_theme = layer_params.collection_params.gfi_theme ? layer_params.collection_params.gfi_theme : "default";

        if (this.isJsonString(layer_params.collection_params.gfi_theme_params)) {
            gfi_theme = {
                name: layer_params.collection_params.gfi_theme,
                params: JSON.parse(layer_params.collection_params.gfi_theme_params)
            };

            // if (gfi_theme.name === "default" && !layer_params.collection_params.gfi_beautifykeys) {
            //     gfi_theme.params.beautifyKeys = layer_params.collection_params.gfi_beautifykeys;
            // }
        }

        // if (!layer_params.collection_params.gfi_theme_params && layer_params.collection_params.gfi_theme) {
        //     if (layer_params.collection_params.gfi_theme === "default" && layer_params.collection_params.gfi_beautifykeys) {
        //         gfi_theme = {
        //             name: layer_params.collection_params.gfi_theme,
        //             params: {
        //                 beautifyKeys: layer_params.collection_params.gfi_beautifykeys
        //             }
        //         };
        //     }
        // }

        let mouse_hover_field = null;

        if (layer_params.st_attributes.mouse_hover_field) {
            if (layer_params.st_attributes.mouse_hover_field.split(",").length > 1) {
                mouse_hover_field = layer_params.st_attributes.mouse_hover_field.split(",");
            }
        }

        if (layer_params.collection_params.namespace) {
            if (layer_params.collection_params.namespace.indexOf("=")) {
                namespace = layer_params.collection_params.namespace.split("=")[1];
            }
        }
        if (layer_params.collection_params.alternative_name !== null) {
            layer_name = layer_params.collection_params.alternative_name
        }
        else {
            layer_name = layer_params.collection_params.name
        }

        // prepare Layer JSON data Object
        const json_data = {
            id: layer_params.id,
            title: title,
            url: url,
            type: type,
            layer_name: layer_name,
            output_format: output_format,
            version: layer_params.service_params.version,
            singletile: singletile,
            transparent: transparent,
            transparency: transparency,
            service_url_is_visible: service_url_is_visible,
            tilesize: tilesize,
            gutter: gutter,
            namespace: namespace,
            scale_min: scale_min,
            scale_max: scale_max,
            gfi_format: gfi_format,
            gfi_config: gfi_config,
            gfi_theme: gfi_theme,
            gfi_complex: gfi_complex,
            gfi_as_new_window: gfi_as_new_window,
            gfi_window_specs: layer_params.collection_params.gfi_window_specs,
            layerattribution: layerattribution,
            legend_url: legend_url_intranet,
            cache: cache,
            featurecount: featurecount,
            request_vertex_normals: request_vertex_normals,
            maximum_screen_space_error: layer_params.st_attributes.maximum_screen_space_error,
            rootel: rootel,
            filter: layer_params.st_attributes.filter,
            expand: layer_params.st_attributes.expand,
            related_wms_layers: related_wms_layers,
            style_id: layer_params.st_attributes.style_id,
            cluster_distance: layer_params.st_attributes.cluster_distance,
            load_things_only_in_current_extent: load_things_only_in_current_extent,
            mouse_hover_field: mouse_hover_field,
            epsg: epsg,
            hidelevels: layer_params.st_attributes.hidelevels,
            minzoom: layer_params.st_attributes.minzoom,
            projection: layer_params.st_attributes.projection,
            terrainurl: layer_params.st_attributes.terrainurl,
            resolution: layer_params.st_attributes.resolution,
            extent: layer_params.st_attributes.extent,
            origin: layer_params.st_attributes.origin,
            resolutions: layer_params.st_attributes.resolutions,
            visibility: visibility,
            vtstyles: layer_params.st_attributes.vtstyles,
            notsupportedfor3d: notsupportedfor3d,
            limit: layer_params.st_attributes.limit,
            time_series: time_series
        };
        
        // copy layer JSON data Object 4 times for every scope
        const json_intranet_data = JSON.parse(JSON.stringify(json_data));
        const json_internet_data = JSON.parse(JSON.stringify(json_data));
        const json_intranet_data_es = JSON.parse(JSON.stringify(json_data));
        const json_internet_data_es = JSON.parse(JSON.stringify(json_data));

        // generate layer JSON Objects if metadata params are linked
        json_internet_data.legend_url = legend_url_internet;
        json_internet_data_es.legend_url = legend_url_internet;
        if (layer_params.dataset_params.md_id) {
            json_intranet_data.datasets = [this.generateDatasetJsonString(layer_params.dataset_params, layer_params.metadata_params, false)];
            json_internet_data.datasets = [this.generateDatasetJsonStringInternet(layer_params.dataset_params, layer_params.metadata_params, metadata_catalog_repl, false)];
            json_intranet_data_es.datasets = [this.generateDatasetJsonString(layer_params.dataset_params, layer_params.metadata_params, true)];
            json_internet_data_es.datasets = [this.generateDatasetJsonStringInternet(layer_params.dataset_params, layer_params.metadata_params, metadata_catalog_repl, true)];
        }
        else {
            json_intranet_data.datasets = [];
            json_internet_data.datasets = [];
            json_intranet_data_es.datasets = [];
            json_internet_data_es.datasets = [];
        }

        return {
            id: layer_params.id,
            json_intranet: this.jsonTemplates(type, json_intranet_data).replace(/&quot;/g, "\""),
            json_internet: this.jsonTemplates(type, json_internet_data).replace(/&quot;/g, "\""),
            json_intranet_es: this.jsonTemplates(type, json_intranet_data_es).replace(/&quot;/g, "\""),
            json_internet_es: this.jsonTemplates(type, json_internet_data_es).replace(/&quot;/g, "\"")
        };
    },
    
    /**
     * generate dataset object (metadata coupling information) for specific layer (Intranet scope)
     * 
     * @param {JSON} dataset - all layer specific dataset params (metadata coupling)
     * @param {JSON} metadata_params - params of coupled metadata catalog
     * @param {boolean} es_param - true if layer json object ist meant for ElasticSearch index
     * @returns {JSON} - Object with layer metadata params
     */
    generateDatasetJsonString: function (dataset, metadata_params, es_param) {
        let kategorie_opendata = [];
        let kategorie_organisation = "";
        let kategorie_inspire = ["kein INSPIRE-Thema"];

        if (dataset.cat_opendata) {
            kategorie_opendata = dataset.cat_opendata.split("|");
        }
        if (dataset.cat_org) {
            kategorie_organisation = dataset.cat_org;
        }
        if (dataset.cat_inspire) {
            kategorie_inspire = dataset.cat_inspire.split("|");
        }

        if (!es_param) {
            return {
                md_id: dataset.md_id,
                csw_url: metadata_params ? metadata_params.csw_url : null,
                show_doc_url: metadata_params ? metadata_params.show_doc_url : null,
                rs_id: dataset.rs_id,
                md_name: dataset.title,
                bbox: dataset.bbox,
                kategorie_opendata: kategorie_opendata,
                kategorie_inspire: kategorie_inspire,
                kategorie_organisation: kategorie_organisation
            }
        }
        else {
            return {
                md_id: dataset.md_id,
                csw_url: metadata_params ? metadata_params.csw_url : null,
                show_doc_url: metadata_params ? metadata_params.show_doc_url : null,
                rs_id: dataset.rs_id,
                md_name: dataset.title,
                keywords: dataset.keywords,
                description: dataset.description,
                bbox: dataset.bbox,
                kategorie_opendata: kategorie_opendata,
                kategorie_inspire: kategorie_inspire,
                kategorie_organisation: kategorie_organisation
            }
        } 
    },

    /**
     * generate dataset object (metadata coupling information) for specific layer (Internet scope)
     * 
     * @param {JSON} dataset - all layer specific dataset params (metadata coupling)
     * @param {JSON} metadata_params - params of coupled metadata catalog
     * @param {JSON} metadata_catalog_repl - if the metadata catalog params should be replaced with other informations, this object contains the params
     * @param {boolean} es_param - true if layer json object ist meant for ElasticSearch index
     * @returns {JSON} - Object with layer metadata params
     */
    generateDatasetJsonStringInternet: function (dataset, metadata_params, metadata_catalog_repl, es_param) {
        let kategorie_opendata = [];
        let csw_url = metadata_params ? metadata_params.csw_url : null;
        let show_doc_url = metadata_params ? metadata_params.show_doc_url : null;
        let kategorie_organisation = "";
        let kategorie_inspire = ["kein INSPIRE-Thema"];

        if (dataset.cat_opendata) {
            kategorie_opendata = dataset.cat_opendata.split("|");
        }
        if (dataset.cat_org) {
            kategorie_organisation = dataset.cat_org;
        }
        if (dataset.cat_inspire) {
            kategorie_inspire = dataset.cat_inspire.split("|");;
        }

        if (metadata_catalog_repl.length > 0 && !dataset.external) {
            csw_url = metadata_catalog_repl[0].csw_url;
            show_doc_url = metadata_catalog_repl[0].show_doc_url;
        }

        if (!es_param) {
            return {
                md_id: dataset.md_id,
                csw_url: csw_url,
                show_doc_url: show_doc_url,
                rs_id: dataset.rs_id,
                md_name: dataset.title,
                bbox: dataset.bbox,
                kategorie_opendata: kategorie_opendata,
                kategorie_inspire: kategorie_inspire,
                kategorie_organisation: kategorie_organisation
            }
        }
        else {
            return {
                md_id: dataset.md_id,
                csw_url: csw_url,
                show_doc_url: show_doc_url,
                rs_id: dataset.rs_id,
                md_name: dataset.title,
                keywords: dataset.keywords,
                description: dataset.description,
                bbox: dataset.bbox,
                kategorie_opendata: kategorie_opendata,
                kategorie_inspire: kategorie_inspire,
                kategorie_organisation: kategorie_organisation
            }
        }
    },

    /**
     * returns JSON Object for specific service Type
     * 
     * @param {String} type - service type [WMS, WMTS, WFS, GeoJSON, Elastic, Terrain3D, TileSet3D, STA, Oblique, VectorTiles]
     * @param {JSON} params - values for JSON template
     * @returns {JSON} - filled out JSON template
     */
    jsonTemplates: function (type, params) {
        const reg = /(?<=}).*?,/g;
        const vtStyles = params.vtstyles ? params.vtstyles.split(reg).map(vtStyles => JSON.parse(vtStyles)) : null;
        const extent = params.extent ? params.extent.split(",").map(Number) : null;
        const origin = params.origin ? params.origin.split(",").map(Number) : null;
        const resolutions = params.resolutions ? params.resolutions.split(",").map(Number) : true;

        const templates = {
            WMS: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typ: params.type,
                layers: params.layer_name,
                format: params.output_format,
                version: params.version,
                singleTile: params.singletile,
                transparent: params.transparent,
                transparency: params.transparency,
                urlIsVisible: params.service_url_is_visible,
                tilesize: params.tilesize,
                gutter: params.gutter,
                minScale: params.scale_min,
                maxScale: params.scale_max,
                infoFormat: params.gfi_format,
                gfiAttributes: params.gfi_config,
                gfiTheme: params.gfi_theme,
                gfiComplex: params.gfi_complex,
                layerAttribution: params.layerattribution,
                legendURL: params.legend_url,
                cache: params.cache,
                featureCount: params.featurecount,
                datasets: params.datasets,
                notSupportedFor3DNeu: params.notsupportedfor3d,
                time: params.time_series
            },
            WMTS: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typ: params.type,
                layers: params.layer_name,
                format: params.output_format,
                version: params.version,
                singleTile: params.singletile,
                transparent: params.transparent,
                transparency: params.transparency,
                urlIsVisible: params.service_url_is_visible,
                tilesize: params.tilesize,
                gutter: params.gutter,
                minScale: params.scale_min,
                maxScale: params.scale_max,
                infoFormat: params.gfi_format,
                gfiAttributes: params.gfi_config,
                gfiTheme: params.gfi_theme,
                gfiComplex: params.gfi_complex,
                layerAttribution: params.layerattribution,
                legendURL: params.legend_url,
                cache: params.cache,
                featureCount: params.featurecount,
                datasets: params.datasets
            },
            WFS: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typ: params.type,
                featureType: params.layer_name,
                outputFormat: params.output_format,
                version: params.version,
                featureNS: params.namespace,
                gfiAttributes: params.gfi_config,
                gfiTheme: params.gfi_theme,
                gfiComplex: params.gfi_complex,
                layerAttribution: params.layerattribution,
                legendURL: params.legend_url,
                datasets: params.datasets,
                urlIsVisible: params.service_url_is_visible
            },
            GeoJSON: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typ: params.type,
                format: params.output_format,
                version: params.version,
                minScale: params.scale_min,
                maxScale: params.scale_max,
                gfiAttributes: params.gfi_config,
                gfiTheme: params.gfi_theme,
                gfiComplex: params.gfi_complex,
                layerAttribution: params.layerattribution,
                legendURL: params.legend_url,
                datasets: params.datasets,
                urlIsVisible: params.service_url_is_visible
            },
            Elastic: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typeName: params.layer_name,
                typ: params.type,
                version: params.version,
                minScale: params.scale_min,
                maxScale: params.scale_max,
                gfiAttributes: params.gfi_config,
                gfiTheme: params.gfi_theme,
                gfiComplex: params.gfi_complex,
                layerAttribution: params.layerattribution,
                legendURL: params.legend_url,
                datasets: params.datasets,
                urlIsVisible: params.service_url_is_visible
            },
            Terrain3D: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typ: params.type,
                layerAttribution: params.layerattribution,
                legendURL: params.legend_url,
                cesiumTerrainProviderOptions: {
                    requestVertexNormals: params.request_vertex_normals
                },
                datasets: params.datasets,
                urlIsVisible: params.service_url_is_visible
            },
            TileSet3D: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typ: params.type,
                gfiAttributes: params.gfi_config,
                layerAttribution: params.layerattribution,
                legendURL: params.legend_url,
                cesium3DTilesetOptions: {
                    maximumScreenSpaceError: params.maximum_screen_space_error
                },
                datasets: params.datasets,
                urlIsVisible: params.service_url_is_visible
            },
            SensorThings: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typ: params.type,
                version: params.version,
                gfiAttributes: params.gfi_config,
                layerAttribution: params.layerattribution,
                legendURL: params.legend_url,
                datasets: params.datasets,
                urlParameter: {
                    root: params.rootel,
                    filter: params.filter,
                    expand: params.expand
                },
                related_wms_layers: params.related_wms_layers,
                epsg: params.epsg,
                gfiTheme: params.gfi_theme,
                styleId: params.style_id,
                clusterDistance: params.cluster_distance,
                loadThingsOnlyInCurrentExtent: params.load_things_only_in_current_extent,
                mouseHoverField: params.mouse_hover_field,
                urlIsVisible: params.service_url_is_visible
            },
            Oblique: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typ: params.type,
                resolution: params.resolution,
                hideLevels: params.hidelevels,
                minZoom: params.minzoom,
                terrainUrl: params.terrainurl,
                projection: params.projection,
                gfiAttributes: params.gfi_config,
                layerAttribution: params.layerattribution,
                legendURL: params.legend_url,
                datasets: params.datasets,
                urlIsVisible: params.service_url_is_visible
            },
            VectorTiles: {
                id: String(params.id),
                name: params.title,
                url: params.url,
                typ: params.type,
                epsg: params.epsg,
                extent: extent,
                origin: origin,
                resolutions: resolutions,
                tileSize: params.tilesize,
                transparency: params.transparency,
                visibility: params.visibility,
                minScale: params.scale_min,
                maxScale: params.scale_max,
                legendURL: params.legend_url,
                gfiAttributes: params.gfi_config,
                gfiTheme: params.gfi_theme,
                vtStyles: vtStyles
            },
            OAF: {
                id: String(params.id),
                typ: params.type,
                name: params.title,
                url: params.url,
                collection: params.layer_name,
                limit: params.limit,
                gfiAttributes: params.gfi_config,
                gfiTheme: params.gfi_theme,
                datasets: params.datasets
            }
        };
        const template_data = templates[type];

        if (params.gfi_as_new_window) {
            template_data.gfiAsNewWindow = {
                name: "_blank",
                specs: params.gfi_window_specs
            };
        }

        if (!params.gfi_complex) {
            delete template_data.gfiComplex;
        }
        if (!params.gfi_format || params.gfi_format === "" || params.gfi_config === "ignore") {
            delete template_data.infoFormat;
        }
        if (!params.cluster_distance) {
            delete template_data.clusterDistance;
        }
        if (!params.related_wms_layers || params.related_wms_layers.length === 0) {
            delete template_data.related_wms_layers;
        }
        if (!params.mouse_hover_field || params.mouse_hover_field.length === 0) {
            delete template_data.mouseHoverField;
        }
        if (!params.style_id || params.style_id === "") {
            delete template_data.styleId;
        }
        if (params.limit <= 0 || !params.limit) {
            delete template_data.limit
        }
        if (!params.time_series) {
            delete template_data.time
        }

        return JSON.stringify(template_data);
    },

    /**
     * helper function to evaluate, wether given String is a JSON object
     * 
     * @param {String} str - input String to test 
     * @returns {boolean} - true if String is JSON
     */
    isJsonString: function (str) {
        if (str) {
            try {
                JSON.parse(str);
                return true;
            }
            catch (e) {
                return false;
            }
        }
        else {
            return false;
        }
    }
};
