const cryptor = require("../utils/cryptor.util");

function _prepareDataset (data, config) {
    for (const catalog in data.metadata_catalogs) {
        if (data.dataset.restriction_level === "internet") {
            if (data.metadata_catalogs[catalog].use_in_internet_json) {
                data.dataset.csw_url = data.metadata_catalogs[catalog].csw_url;
                data.dataset.show_doc_url = data.metadata_catalogs[catalog].show_doc_url;
            }
        }
        else {
            if (data.metadata_catalogs[catalog].id === data.dataset.metadata_catalog_id) {
                data.dataset.csw_url = data.metadata_catalogs[catalog].csw_url;
                data.dataset.show_doc_url = data.metadata_catalogs[catalog].show_doc_url;
            }
        }
    }

    data.dataset.fees_url = data.dataset.fees_json?.url || null; // optional chaining to safely test for existence
    data.dataset.fees_name = data.dataset.fees_json?.name || null;
    data.dataset.fees_quelle = data.dataset.fees_json?.quelle || null;
    data.dataset.transactional = data.service.type.toLowerCase() === "wfs-t";
    data.dataset.extent = config.defaultBbox.replace(/,/gi, " ");

    return data.dataset;
}

function _prepareCollections (data, db_data_types, config) {
    // filter collections based on api_constraint of API Type before proceeding
    const {visualizationApis, vectorApis, vectorSoftwares, rasterSoftwares} = config;
    const apiType = data.service.type;
    const software = data.service.software;
    const isInVisualizationApis = visualizationApis.some(item => item.toLowerCase() === apiType.toLowerCase());
    const isInVectorApis = vectorApis.some(item => item.toLowerCase() === apiType.toLowerCase());
    const isInVectorSoftwares = vectorSoftwares.some(item => item.toLowerCase() === software.toLowerCase());
    const isInRasterSoftwares = rasterSoftwares.some(item => item.toLowerCase() === software.toLowerCase());
    let exclusionCriterionApiType;
    let inclusionCriterionSoftware;

    if (isInVisualizationApis) {
        exclusionCriterionApiType = "only_download";
    }
    else if (isInVectorApis) {
        exclusionCriterionApiType = "only_visual";
    }
    else {
        throw new Error(`API type ${apiType} not defined as visualization or vector API`);
    }

    if (isInVectorSoftwares) {
        inclusionCriterionSoftware = "Vektor";
    }
    else if (isInRasterSoftwares) {
        inclusionCriterionSoftware = "Raster";
    }
    else {
        throw new Error(`Software ${software} not defined as raster or vector software`);
    }

    data.collections = data.collections.filter((collection) => {
        if ((collection.api_constraint !== exclusionCriterionApiType) && ((data.dataset.store_type === "Beides" && collection.store_type === inclusionCriterionSoftware) || data.dataset.store_type === inclusionCriterionSoftware)) {
            const hasLayer = data.layers.some((layer) => layer.collection_id === collection.id);    
            
            if (hasLayer) {
                return collection;
            }
        }
        else {
            return null;
        }
    });

    // prepare collections and keep track of missing datatypes
    const collections = {};

    for (const collection of data.collections) {
        // short reference to collection name
        const name = collection.name;

        // map simple attributes
        collections[name] = collection;

        collections[name].attributeMap = {};
        collections[name].missing_datatypes = [];
        collections[name].missing_attr = [];

        // assign optional custom bbox / envelope
        let bbox = null;

        if (collection.custom_bbox) {
            const bbox_values = collection.custom_bbox.split(",");
            const bbox_keys = ["xmin", "ymin", "xmax", "ymax"];

            bbox = Object.assign(...bbox_keys.map((k, v)=>({[k]: bbox_values[v]})));
        }

        collections[name].custom_bbox = bbox;

        if (collection.namespace) {
            const splitNamespace = collection.namespace.split("=");

            collections[name].namespace = {
                "namespacePrefix": splitNamespace[0],
                "namespace": splitNamespace[1]
            };
        }

        // skip remaining collection parsing steps under certain conditions
        const skipIteration =
            data.dataset.store_type === "Raster" ||
            (data.dataset.store_type === "Beides" && collection.store_type === "Raster") ||
            collection.group_object ||
            collection.complex_scheme ||
            data.dataset.db_connection_r_id === null ||
            collection.attribute_config === null ||
            collection.db_schema === null ||
            collection.db_table === null;

        if (!skipIteration) {
            // Filter all source attributes that shall be ignored in db
            collection.attribute_config = collection.attribute_config.filter((a) => {
                const attr_name_db = a.attr_name_db.toLowerCase().trim();

                if (attr_name_db !== "ignore") {
                    return a;
                }
                else {
                    return null;
                }
            });

            // Check remaining db attributes that are missing in the database
            const attr_map_names = collection.attribute_config.map((x) => x.attr_name_db);
            const attr_db_names = db_data_types[collection.id].map((y) => y.column_name);
            const not_matching_attr = attr_map_names.filter(element => !attr_db_names.includes(element));

            if (not_matching_attr.length > 0) {
                collections[name].missing_attr.push(not_matching_attr);
            }

            // Now, also filter all attributes that shall be ignored by service
            collection.attribute_config = collection.attribute_config.filter((a) => {
                const attr_name_service = a.attr_name_service.toLowerCase().trim();

                if (attr_name_service !== "ignore") {
                    return a;
                }
                else {
                    return null;
                }
            });

            // Check if remaining attribute names in db and service are unique
            const dbAttributes = [];

            collection.attribute_config.forEach(a => dbAttributes.push(a.attr_name_db));
            const dbAttributesAreUnique = dbAttributes.length === new Set(dbAttributes).size;

            const serviceAttributes = [];

            collection.attribute_config.forEach(a => serviceAttributes.push(a.attr_name_service));
            const serviceAttributesAreUnique = (serviceAttributes.length === new Set(serviceAttributes).size);

            if ((dbAttributesAreUnique === false) || (serviceAttributesAreUnique === false)) {
                throw new Error(`Eingabefehler in Collection '${name}': Doppelte Attributnamen in Name (DB) oder Name (API) vorhanden.`);
            }

            // assign primary key, main geom and primary date and fill if present in attribute_config
            collections[name].primaryKey = null;
            collections[name].mainGeom = null;
            collections[name].primary_date = null;

            if (collection.attribute_config && Object.entries(collection.attribute_config).length > 0) {
                collection.attribute_config.forEach(attr_column => {

                    if (Object.keys(attr_column).includes("pk") && attr_column.pk === true) {
                        const collection_pk = attr_column.attr_name_db;

                        collections[name].primaryKey = collection_pk;
                    }
                    if (Object.keys(attr_column).includes("primary_geom") && attr_column.primary_geom === true) {
                        const collection_primary_geom = attr_column.attr_name_db;

                        collections[name].mainGeom = collection_primary_geom;
                    }
                    if (Object.keys(attr_column).includes("primary_date") && attr_column.primary_date === true) {
                        const collection_primary_date = attr_column.attr_name_db;

                        collections[name].primary_date = collection_primary_date;
                    }
                });
            }

            const collection_db_data_types = db_data_types[collection.id];

            // parse attribute mapping
            collection.attribute_config.forEach(attribute => {
                const attribute_name_matching = collection_db_data_types.filter((el) => el.column_name === attribute.attr_name_db);

                if (attribute_name_matching.length > 0) {
                    collections[name].attributeMap[attribute.attr_name_db] = {};
                    collections[name].attributeMap[attribute.attr_name_db].title = attribute.attr_name_service;

                    // If Portal Name is set to ignore, the service name will be used as a title
                    // Otherwise the portal name will be used as a title
                    if (attribute.attr_name_masterportal === "ignore" || !attribute.attr_name_masterportal || attribute.attr_name_masterportal.trim().startsWith("{")) {
                        collections[name].attributeMap[attribute.attr_name_db].label = attribute.attr_name_service;
                    }
                    else {
                        collections[name].attributeMap[attribute.attr_name_db].label = attribute.attr_name_masterportal;
                    }

                    collections[name].attributeMap[attribute.attr_name_db].description = attribute.description;
                    collections[name].attributeMap[attribute.attr_name_db].unit = attribute.unit;

                    // map data types between database and list for specific type
                    if (attribute.attr_datatype.indexOf("geometry") > -1) {
                        if (config.software[data.service.software].dataTypeMap[attribute_name_matching[0].geom_type]) {
                            collections[name].attributeMap[attribute.attr_name_db].dtype = config.software[data.service.software].dataTypeMap[attribute_name_matching[0].geom_type];
                        }
                        else {
                            if (collections[name].missing_datatypes.indexOf(attribute_name_matching[0].geom_type) === -1) {
                                collections[name].missing_datatypes.push(attribute_name_matching[0].geom_type);
                            }
                        }
                    }
                    else {
                        if (config.software[data.service.software].dataTypeMap[attribute_name_matching[0].data_type]) {
                            collections[name].attributeMap[attribute.attr_name_db].dtype = config.software[data.service.software].dataTypeMap[attribute_name_matching[0].data_type];
                        }
                        else {
                            if (collections[name].missing_datatypes.indexOf(attribute_name_matching[0].data_type) === -1) {
                                collections[name].missing_datatypes.push(attribute_name_matching[0].data_type);
                            }
                        }
                    }
                }
            });
        }
    }

    return Object.fromEntries(
        Object.entries(collections).sort(([a], [b]) => a.localeCompare(b))
    );
}

function _prepareService (data, config) {
    data.service.id = String(data.service.id);
    data.service.type = data.service.type.toLowerCase();
    data.service.clickradius = data.service.clickradius ? data.service.clickradius : 10;
    data.service.maxfeatures = data.service.maxfeatures ? data.service.maxfeatures : 1000000;
    data.service.additionalOutputFormats = config.software[data.service.software].dev.softwareSpecificParameter.additionalOutputFormats;

    return data.service;
}

function _prepareDbConnection (data) {
    let connection = {};

    if (data.db_connections) {
        for (const db_connection of data.db_connections) {
            if (data.dataset.transactional) {
                if (db_connection.id === data.dataset.db_connection_w_id) {
                    connection = db_connection;
                }
            }
            else {
                if (db_connection.id === data.dataset.db_connection_r_id) {
                    connection = db_connection;
                }
            }
        }
    
        connection.password = !connection.password ? null : cryptor.decrypt(connection.password);
    }
    
    return connection;
}

module.exports = (data, db_data_types) => {
    const config = require("../config.js").getConfig();

    return {
        dataset: _prepareDataset(data, config),
        collections: _prepareCollections(data, db_data_types, config),
        service: _prepareService(data, config),
        connection: _prepareDbConnection(data),
        provider: config.capabilitiesMetadata,
        software: config.software[data.service.software],
        outPath: process.env.CONFIG_OUT_PATH
    };
};
