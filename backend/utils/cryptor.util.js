const crypto = require("crypto");
const key = process.env.SECRET;

module.exports = {
    /**
     * Encrypt text with "aes-256-cbc" algorithm. Given key und random iv (initialization vector) is used for encryption
     * key must be a String 32 characters
     * @param {String} text - readable text to be encrypted
     * @returns {String} - String combined from iv (initialization vector) and encrypted String
     */
    encrypt: (text) => {
        const iv = crypto.randomBytes(16),
            cipher = crypto.createCipheriv("aes-256-cbc", Buffer.from(key), iv);

        let encrypted = cipher.update(text);

        encrypted = Buffer.concat([encrypted, cipher.final()]);

        return iv.toString("hex") + "__" + encrypted.toString("hex");
    },

    /**
     * Decrypt text with "aes-256-cbc" algorithm. Given key and iv (part of input String) is used.
     * @param {String} text - encrypted text. Must must be a comination of iv and encrypted String, seperated by "__"
     * @returns {String} - readable text
     */
    decrypt: (text) => {
        const iv = Buffer.from(text.split("__")[0], "hex"),
            encryptedText = Buffer.from(text.split("__")[1], "hex"),
            decipher = crypto.createDecipheriv("aes-256-cbc", Buffer.from(key), iv);

        let decrypted = decipher.update(encryptedText);

        decrypted = Buffer.concat([decrypted, decipher.final()]);

        return decrypted.toString();
    }
};
