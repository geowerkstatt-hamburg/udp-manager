const xmldom = require("xmldom");
const DOMParser = xmldom.DOMParser;
const xpath = require("xpath");
const Logger = require("../utils/logger.util");

module.exports = {
    _getNamespaces: function () {
        return {"csw": "http://www.opengis.net/cat/csw/2.0.2", "gmd": "http://www.isotc211.org/2005/gmd", "gco": "http://www.isotc211.org/2005/gco", "srv": "http://www.isotc211.org/2005/srv", "gml": "http://www.opengis.net/gml", "gmx": "http://www.isotc211.org/2005/gmx", "gts": "http://www.isotc211.org/2005/gts", "xsi": "http://www.w3.org/2001/XMLSchema-instance"};
    },

    _parseXMLdoc: function (md) {
        const metadata_response_doc = new DOMParser().parseFromString(md, "text/xml");
        const select = xpath.useNamespaces(this._getNamespaces());

        return select("//gmd:MD_Metadata", metadata_response_doc)[0];
    },

    _getFees: function (md_doc) {
        const select = xpath.useNamespaces(this._getNamespaces());
        const MD_LegalConstraints = select("//gmd:MD_LegalConstraints", md_doc);
        let fees = "";

        try {
            for (let i = 0; i < MD_LegalConstraints.length; i++) {
                const codeListValue_fees = select("gmd:useConstraints/gmd:MD_RestrictionCode/@codeListValue", MD_LegalConstraints[i]);

                for (let j = 0; j < codeListValue_fees.length; j++) {
                    if (codeListValue_fees[j].nodeValue.indexOf("otherRestrictions") > -1) {
                        const other_constraints = select("gmd:otherConstraints", MD_LegalConstraints[i]);

                        for (let k = 0; k < other_constraints.length; k++) {
                            let fees_str = select("string(gmx:Anchor)", other_constraints[k]);

                            if (fees_str === "") {
                                fees_str = select("string(gco:CharacterString)", other_constraints[k]);
                            }
                            if (fees_str !== "" && fees_str.indexOf("{") === -1) {
                                fees += fees_str + ", ";
                            }
                        }
                    }
                }
            }

            return fees.substring(0, fees.length - 2);
        }
        catch (error) {
            Logger.debug("metadata_parser.util - _getFees");
            Logger.debug(error);
        }
    },

    _getFeesJSON: function (md_doc) {
        const select = xpath.useNamespaces(this._getNamespaces());
        const MD_LegalConstraints = select("//gmd:MD_LegalConstraints", md_doc);
        let fees = "";

        for (let i = 0; i < MD_LegalConstraints.length; i++) {
            const codeListValue_fees = select("gmd:useConstraints/gmd:MD_RestrictionCode/@codeListValue", MD_LegalConstraints[i]);

            for (let j = 0; j < codeListValue_fees.length; j++) {
                if (codeListValue_fees[j].nodeValue.indexOf("otherRestrictions") > -1) {
                    const other_constraints = select("gmd:otherConstraints", MD_LegalConstraints[i]);

                    for (let k = 0; k < other_constraints.length; k++) {
                        let fees_str = select("string(gmx:Anchor)", other_constraints[k]);

                        if (fees_str === "") {
                            fees_str = select("string(gco:CharacterString)", other_constraints[k]);
                        }
                        if (fees_str !== "" && fees_str.indexOf("{") > -1) {
                            fees = fees_str;
                        }
                    }
                }
            }
        }

        return fees;
    },

    _getAccessConstraints: function (md_doc) {
        const select = xpath.useNamespaces(this._getNamespaces());
        const MD_LegalConstraints = select("//gmd:MD_LegalConstraints", md_doc);
        let access_constraints = "";

        try {
            for (let i = 0; i < MD_LegalConstraints.length; i++) {
                const codeListValue_ac = select("gmd:accessConstraints/gmd:MD_RestrictionCode/@codeListValue", MD_LegalConstraints[i]);

                for (let j = 0; j < codeListValue_ac.length; j++) {
                    if (codeListValue_ac[j].nodeValue.indexOf("otherRestrictions") > -1) {
                        let acc_str = select("string(gmd:otherConstraints/gmx:Anchor)", MD_LegalConstraints[i]);

                        if (acc_str === "") {
                            acc_str = select("string(gmd:otherConstraints/gco:CharacterString)", MD_LegalConstraints[i]);
                        }
                        if (acc_str !== "") {
                            access_constraints += acc_str + ", ";
                        }
                    }
                }
            }

            return access_constraints.substring(0, access_constraints.length - 2);
        }
        catch (error) {
            Logger.debug("metadata_parser.util - _getAccessConstraints");
            Logger.debug(error);
        }
    },

    _getKeywords: function (md_doc) {
        const select = xpath.useNamespaces(this._getNamespaces());
        const keyword_elements = select("//gmd:keyword", md_doc);
        let keywords = "";

        try {
            for (let i = 0; i < keyword_elements.length; i++) {
                const keyword = select("string(gco:CharacterString)", keyword_elements[i]);

                keywords += keyword + ",";
            }

            return keywords.substring(0, keywords.length - 1);
        }
        catch (error) {
            Logger.debug("metadata_parser.util - _getKeywords");
            Logger.debug(error);
        }
    },

    _getContactInfo: function (md_doc) {
        const select = xpath.useNamespaces(this._getNamespaces());
        const contact_element = select("//gmd:pointOfContact", md_doc);
        const md_contact_mail_obj = {};
        let md_contact_mail = "";

        try {
            for (let i = 0; i < contact_element.length; i++) {
                const mail = select("string(gmd:CI_ResponsibleParty/gmd:contactInfo/gmd:CI_Contact/gmd:address/gmd:CI_Address/gmd:electronicMailAddress/gco:CharacterString)", contact_element[i]);
                const role_code = select("string(gmd:CI_ResponsibleParty/gmd:role/gmd:CI_RoleCode/@codeListValue)", contact_element[i]);

                md_contact_mail_obj[role_code] = mail;
            }

            if (md_contact_mail_obj.hasOwnProperty("pointOfContact")) {
                md_contact_mail += md_contact_mail_obj.pointOfContact;
            }
            else if (md_contact_mail_obj.hasOwnProperty("publisher")) {
                md_contact_mail += md_contact_mail_obj.publisher;
            }

            return md_contact_mail;
        }
        catch (error) {
            Logger.debug("metadata_parser.util - _getContactInfo");
            Logger.debug(error);
        }
    },

    parseServiceMD: function (servicemd, parsedoc) {
        let md_metadata = servicemd;

        if (parsedoc) {
            md_metadata = this._parseXMLdoc(servicemd);
        }

        const select = xpath.useNamespaces(this._getNamespaces());
        const md_id = select("string(//gmd:fileIdentifier/gco:CharacterString)", md_metadata);
        const service_url = select("string(//srv:SV_OperationMetadata//srv:connectPoint//gmd:CI_OnlineResource//gmd:linkage//gmd:URL)", md_metadata);
        const title = select("string(//gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString)", md_metadata);

        const service_metainfo = {
            title: title.replace("-in Arbeit", "").trim(),
            description: select("string(//gmd:identificationInfo/srv:SV_ServiceIdentification/gmd:abstract/gco:CharacterString)", md_metadata).replace(/\r\n\r\n/g, "\r\n").replace(/\r\n\r\n/g, "\r\n"),
            service_url: service_url.replace("?", ""),
            md_id: md_id,
            fees: this._getFees(md_metadata),
            fees_json: this._getFeesJSON(md_metadata),
            access_constraints: this._getAccessConstraints(md_metadata),
            keywords: this._getKeywords(md_metadata)
        };

        return service_metainfo;
    },

    getCoupledRessources: function (servicemd) {
        const md_metadata = this._parseXMLdoc(servicemd);
        const select = xpath.useNamespaces(this._getNamespaces());
        const md_ids = select("//srv:operatesOn/@uuidref", md_metadata);
        const id_links = select("//gmd:MD_DigitalTransferOptions/gmd:onLine/gmd:CI_OnlineResource/gmd:description", md_metadata);
        const dataset_ids = [];

        for (let i = 0; i < md_ids.length; i++) {
            dataset_ids.push({
                md_id: md_ids[i].nodeValue
            });
        }

        for (let i = 0; i < id_links.length; i++) {
            const id_link = select("string(gco:CharacterString)", id_links[i]);

            if (id_link.indexOf("#**#") > 0) {
                const md_id = id_link.split("#**#")[1];

                dataset_ids.push({
                    md_uuid: md_id
                });
            }
        }

        return dataset_ids;
    },

    getServiceMDID: function (servicemd) {
        const md_metadata = this._parseXMLdoc(servicemd);
        const uuids = [];

        if (md_metadata) {
            const select = xpath.useNamespaces(this._getNamespaces());
            const uuids_doc = select("//gmd:fileIdentifier/gco:CharacterString", md_metadata);

            for (let i = 0; i < uuids_doc.length; i++) {
                uuids.push(uuids_doc[i].firstChild.data);
            }
        }
        return uuids;
    },

    parseDatasetMD: function (datasetmd, parsedoc) {
        const config = require("../config.js").getConfig();
        let md_metadata = datasetmd;

        if (parsedoc) {
            md_metadata = this._parseXMLdoc(datasetmd);
        }
        const select = xpath.useNamespaces(this._getNamespaces());
        const descriptiveKeywords = select("//gmd:descriptiveKeywords", md_metadata);
        const CI_ResponsibleParty = select("//gmd:CI_ResponsibleParty", md_metadata);
        const bbox_min_x = select("string(//gmd:EX_GeographicBoundingBox/gmd:westBoundLongitude/gco:Decimal)", md_metadata);
        const bbox_min_y = select("string(//gmd:EX_GeographicBoundingBox/gmd:southBoundLatitude/gco:Decimal)", md_metadata);
        const bbox_max_x = select("string(//gmd:EX_GeographicBoundingBox/gmd:eastBoundLongitude/gco:Decimal)", md_metadata);
        const bbox_max_y = select("string(//gmd:EX_GeographicBoundingBox/gmd:northBoundLatitude/gco:Decimal)", md_metadata);
        const hierarchyLevelEl = select("//gmd:hierarchyLevel", md_metadata);

        let cat_opendata = "";
        let cat_inspire = "";
        let cat_hmbtg = "";
        let cat_org = "";
        let hierarchyLevel = select("string(gmd:MD_ScopeCode)", hierarchyLevelEl[0]);

        try {
            if (hierarchyLevel === "") {
                hierarchyLevel = select("string(gmd:MD_ScopeCode/@codeListValue)", hierarchyLevelEl[0]);
            }
        }
        catch (error) {
            Logger.debug("metadata_parser.util - parseDatasetMD - if hirachy");
            Logger.debug(error);
        }

        for (let i = 0; i < CI_ResponsibleParty.length; i++) {
            const role = select("string(gmd:role/gmd:CI_RoleCode/@codeListValue)", CI_ResponsibleParty[i]);
            const organisationName = select("string(gmd:organisationName/gco:CharacterString)", CI_ResponsibleParty[i]);

            if (role === "publisher") {
                cat_org = organisationName;
            }

            if (i === CI_ResponsibleParty.length - 1 && cat_org === "") {
                cat_org = organisationName;
            }
        }

        // for (let i = 0; i < config.cat_org.length; i++) {
        //     if (cat_org.indexOf(config.cat_org[i]) > -1) {
        //         cat_org = config.cat_org[i];
        //         break;
        //     }
        // }

        for (let i = 0; i < descriptiveKeywords.length; i++) {
            const thesaurus = select("string(gmd:MD_Keywords/gmd:thesaurusName/gmd:CI_Citation/gmd:title/gco:CharacterString)", descriptiveKeywords[i]);
            const keywords = select("gmd:MD_Keywords/gmd:keyword", descriptiveKeywords[i]);

            for (let j = 0; j < keywords.length; j++) {
                const keyword = select("string(gco:CharacterString)", keywords[j]);

                if (config.openDataCat[keyword]) {
                    cat_opendata += config.openDataCat[keyword] + "|";
                }
                else if (thesaurus === "GEMET - INSPIRE themes, version 1.0") {
                    cat_inspire += keyword + "|";
                }
                else if (thesaurus === "HmbTG-Informationsgegenstand") {
                    cat_hmbtg += keyword + "|";
                }
            }
        }

        if (cat_inspire.substring(0, cat_inspire.length - 1) === "Kein INSPIRE-Thema") {
            cat_inspire = "nicht INSPIRE-identifiziert";
        }
        else {
            cat_inspire = cat_inspire.substring(0, cat_inspire.length - 1);
        }

        if (cat_opendata.substring(0, cat_opendata.length - 1) === "") {
            cat_opendata = "Sonstiges";
        }
        else {
            cat_opendata = cat_opendata.substring(0, cat_opendata.length - 1);
        }

        const keywords = this._getKeywords(md_metadata);

        const inspire = keywords.includes("inspireidentifiziert");

        const dataset_params = {
            title: select("string(//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:title/gco:CharacterString)", md_metadata),
            description: select("string(//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:abstract/gco:CharacterString)", md_metadata),
            keywords: keywords,
            rs_id: select("string(//gmd:identificationInfo/gmd:MD_DataIdentification/gmd:citation/gmd:CI_Citation/gmd:identifier/gmd:MD_Identifier/gmd:code/gco:CharacterString)", md_metadata),
            md_id: select("string(//gmd:fileIdentifier/gco:CharacterString)", md_metadata),
            cat_opendata: cat_opendata,
            cat_inspire: cat_inspire,
            cat_hmbtg: cat_hmbtg.substring(0, cat_hmbtg.length - 1),
            cat_org: cat_org,
            bbox: bbox_min_x + " " + bbox_min_y + "," + bbox_max_x + " " + bbox_max_y,
            fees: this._getFees(md_metadata),
            fees_json: this._getFeesJSON(md_metadata),
            access_constraints: this._getAccessConstraints(md_metadata),
            hierarchyLevel: hierarchyLevel,
            inspire: inspire,
            md_contact_mail: this._getContactInfo(md_metadata)
        };

        return dataset_params;
    },

    parseServiceMDArray: function (servicemdlist) {
        const servicemdstring = [];

        if (servicemdlist.indexOf("<gmd:MD_Metadata") > 0) {
            const servicemdlistsplit = servicemdlist.split("<gmd:MD_Metadata");

            for (let i = 1; i < servicemdlistsplit.length; i++) {
                if (i === servicemdlistsplit.length - 1) {
                    servicemdstring.push("<gmd:MD_Metadata" + servicemdlistsplit[i].split("</gmd:MD_Metadata>")[0] + "</gmd:MD_Metadata>");
                }
                else {
                    servicemdstring.push("<gmd:MD_Metadata" + servicemdlistsplit[i]);
                }
            }
        }

        const servicemdarray = [];

        for (let i = 0; i < servicemdstring.length; i++) {
            const metadata_response_doc = new DOMParser().parseFromString(servicemdstring[i], "text/xml");
            const select = xpath.useNamespaces(this._getNamespaces());
            const hierarchyLevelEl = select("//gmd:hierarchyLevel", metadata_response_doc);

            let hierarchyLevel = select("string(gmd:MD_ScopeCode)", hierarchyLevelEl[0]);

            if (hierarchyLevel === "") {
                hierarchyLevel = select("string(gmd:MD_ScopeCode/@codeListValue)", hierarchyLevelEl[0]);
            }
            if (hierarchyLevel === "service") {
                servicemdarray.push(this.parseServiceMD(metadata_response_doc, false));
            }
        }

        return servicemdarray;
    },

    parseDatasetMDArray: function (datasetmdlist) {
        const datasetmdstring = [];

        try {
            if (datasetmdlist.indexOf("<gmd:MD_Metadata") > 0) {
                const datasetmdlistsplit = datasetmdlist.split("<gmd:MD_Metadata");

                for (let i = 1; i < datasetmdlistsplit.length; i++) {
                    if (i === datasetmdlistsplit.length - 1) {
                        datasetmdstring.push("<gmd:MD_Metadata" + datasetmdlistsplit[i].split("</gmd:MD_Metadata>")[0] + "</gmd:MD_Metadata>");
                    }
                    else {
                        datasetmdstring.push("<gmd:MD_Metadata" + datasetmdlistsplit[i]);
                    }
                }
            }
        }
        catch (error) {
            Logger.debug("metadata_parser.util - parseDatasetMDArray if");
            Logger.debug(error);
        }

        const datasetmdarray = [];

        try {
            for (let i = 0; i < datasetmdstring.length; i++) {
                const metadata_response_doc = new DOMParser().parseFromString(datasetmdstring[i], "text/xml");
                const select = xpath.useNamespaces(this._getNamespaces());
                const hierarchyLevelEl = select("//gmd:hierarchyLevel", metadata_response_doc);

                let hierarchyLevel = select("string(gmd:MD_ScopeCode)", hierarchyLevelEl[0]);

                if (hierarchyLevel === "") {
                    hierarchyLevel = select("string(gmd:MD_ScopeCode/@codeListValue)", hierarchyLevelEl[0]);
                }
                if (hierarchyLevel !== "service") {
                    datasetmdarray.push(this.parseDatasetMD(metadata_response_doc, false));
                }
            }

            return datasetmdarray;
        }
        catch (error) {
            Logger.debug("metadata_parser.util - parseDatasetMDArray for");
            Logger.debug(error);
            return datasetmdarray;
        }
    }
};
