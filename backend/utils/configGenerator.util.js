const config = require("../config.js");
const dataCache = require("./data_cache.util.js");

module.exports = {
    prepare: function (config_db, config_webdav, config_fmeserver) {
        const confClone = JSON.parse(JSON.stringify(config_db));

        // prepare client config
        const clientConfig = confClone.client_config;

        clientConfig.version = dataCache.getVersion();
        clientConfig.softwareConfig = confClone.software;
        clientConfig.modules = confClone.modules;
        clientConfig.server = confClone.server;
        clientConfig.testBbox = confClone.test_bbox;
        clientConfig.serviceTypes = confClone.service_types;
        clientConfig.defaultBbox = confClone.defaultbbox;
        clientConfig.layerAttributesExcludes = confClone.layer_attributes_excludes;

        if (confClone.modules.jiraTickets && confClone.jira) {
            clientConfig.jira = confClone.jira.browseUrl;
            clientConfig.jiraProjects = confClone.jira.projects;
            clientConfig.jiraLabels = confClone.jira.labels;
        }

        clientConfig.authMethod = process.env.AUTH_METHOD;

        if (!process.env.SMTP_HOST || !process.env.SMTP_PORT) {
            clientConfig.mailEnabled = false;
        }
        else {
            clientConfig.mailEnabled = true;
        }

        clientConfig.authMethod = process.env.AUTH_METHOD;

        confClone.git.options_config.push("remote.origin.url=" + process.env.GIT_REPO_URL);

        const visualizationApis = [];
        const vectorApis = [];

        for (const serviceType of confClone.service_types) {
            if (serviceType.category === "visualization") {
                visualizationApis.push(serviceType.name);
            }
            if (serviceType.category === "vector") {
                vectorApis.push(serviceType.name);
            }
        }

        if (process.env.LDAP_ADMIN_GROUP) {
            confClone.ldap.allowed_groups.push({name: process.env.LDAP_ADMIN_GROUP, role: "admin"});
        }

        if (config_webdav) {
            config.setConfig("webdav", config_webdav);
        }
        if (config_fmeserver) {
            const fmeServerNames = [];

            for (const fmeServer of config_fmeserver) {
                fmeServerNames.push(fmeServer.name);
            }

            config.setConfig("database.fmeServer", config_fmeserver);
            clientConfig.fmeServer = fmeServerNames;
        }

        config.setConfig("ldap", confClone.ldap);
        config.setConfig("jira", confClone.jira);
        config.setConfig("mail", confClone.mail);
        config.setConfig("capabilitiesMetadata", confClone.capabilities_metadata);
        config.setConfig("visualizationApis", visualizationApis);
        config.setConfig("vectorApis", vectorApis);
        config.setConfig("modules", confClone.modules);
        config.setConfig("git", confClone.git);
        config.setConfig("server", confClone.server);
        config.setConfig("software", confClone.software);
        config.setConfig("clientConfig", clientConfig);
        config.setConfig("defaultBbox", confClone.defaultbbox);
        config.setConfig("testBbox", confClone.test_bbox);
        config.setConfig("availableProjections", confClone.available_projections);
        config.setConfig("serviceTypes", confClone.service_types);
        config.setConfig("layerAttributesExcludes", confClone.layer_attributes_excludes);
    }
};
