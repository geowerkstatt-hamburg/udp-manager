const config = require("../config.js").getConfig();
const Logger = require("../utils/logger.util");
const simpleGit = require("simple-git");

let git;

if (config.modules.git) {
    // init git module
    const options = {
        config: config.git.options_config,
        binary: "git",
        maxConcurrentProcesses: 6,
        trimmed: false
    };

    if (config.git.ssh) {
        const GIT_SSH_COMMAND = `ssh -o UserKnownHostsFile=${config.git.sshKnownHosts} -o StrictHostKeyChecking=no -i ${config.git.sshKey}`;

        git = simpleGit(process.env.CONFIG_OUT_PATH, options).env("GIT_SSH_COMMAND", GIT_SSH_COMMAND);
    }
    else {
        git = simpleGit(process.env.CONFIG_OUT_PATH, options);
    }

    try {
        git.raw(["config", "--system", "--add", "safe.directory", process.env.CONFIG_OUT_PATH]);
    }
    catch (error) {
        Logger.error(error);
    }
}

module.exports = {
    /**
     * clean git repository from files that are not under version control
     * @returns {JSON} - status
     */
    clean: async function () {
        try {
            await git.clean(simpleGit.CleanOptions.FORCE);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error"};
        }
    },

    /**
     * checkout, reset and pull git branch. is called before every commit
     * @param {String} gitBranch - name of brach to checkout
     * @returns {JSON} - status
     */
    checkout: async function (gitBranch) {
        try {
            await git.checkout(gitBranch);
            await git.reset("hard", ["origin/" + gitBranch]);
            await git.pull();

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error"};
        }
    },

    /**
     * add and commit files
     * @param {String} commit_message - commit message
     * @returns {JSON} - status
     */
    addCommit: async function (commit_message) {
        try {
            //await git.add(files_to_commit);
            await git.raw("add", "--all");
            await git.commit(commit_message);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error"};
        }
    },


    /**
     * add new files and remove old
     * @param {Array} files_to_commit - Array of String with path strings
     * @param {Array} files_to_delete - Array of String with path strings
     * @param {String} commit_message - commit message
     * @returns {JSON} - status
     */
    mvCommit: async function (files_to_commit, files_to_delete, commit_message) {
        try {
            await git.add(files_to_commit);
            await git.rm(files_to_delete);
            await git.commit(commit_message);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error"};
        }
    },

    /**
     * remove files
     * @param {Array} conf_path - path string of the configs
     * @param {String} commit_message - commit message
     * @returns {JSON} - status
     */
    delCommit: async function (conf_path, commit_message) {
        try {
            await git.raw("rm", "--force", "-r", conf_path);
            await git.commit(commit_message);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error"};
        }
    },

    /**
     * remove directory
     * @param {Array} directory - directory to delete
     * @param {String} commit_message - commit message
     * @returns {JSON} - status
     */
    delDirCommit: async function (directory, commit_message) {
        try {
            await git.raw("rm", "-r", directory);
            await git.commit(commit_message);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error"};
        }
    },

    /**
     * execute git push
     * @param {String} gitBranch - name of branch
     * @returns {JSON} - status
     */
    push: async function (gitBranch) {
        try {
            await git.pull();
            await git.push("origin", gitBranch);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error"};
        }
    },

    /**
     * execute git clone
     * @returns {JSON} - status
     */
    clone: async function () {
        try {
            await git.clone(process.env.GIT_REPO_URL, process.env.CONFIG_OUT_PATH);

            return {status: "success"};
        }
        catch (error) {
            Logger.error(error);
            return {status: "error"};
        }
    }
};
