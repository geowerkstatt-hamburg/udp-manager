const Logger = require("./logger.util");
const ldap = require("ldapjs");
let client;

const attributes = {
    user: ["objectGUID;binary", "dn", "mail", "sn", "givenName", "sAMAccountName", "company", "telephoneNumber"],
    group: ["cn", "sAMAccountName", "memberOf"]
};

const mockDataUser = [
    {
        "dn": "CN=Gamdschie Samweis,OU=LGV,OU=Users,OU=KundenFHH,OU=Dataport,DC=fhhnet,DC=stadt,DC=hamburg,DC=de",
        "sn": "Gamdschie",
        "telephoneNumber": "+49 40 428 26-1234",
        "givenName": "Samweis",
        "company": "LGV",
        "objectGUID": "11111111-1111-1111-1111-111111111111",
        "objectGUID_byte": "\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11",
        "sAMAccountName": "GamdSam",
        "mail": "samweis.gamdschie@gv.hamburg.de",
        "groups": [
            {
                "name": "RES-FS-LGV-Kundenportal-r"
            }
        ]
    },
    {
        "dn": "CN=Tuk Peregrin,OU=LGV,OU=Users,OU=KundenFHH,OU=Dataport,DC=fhhnet,DC=stadt,DC=hamburg,DC=de",
        "sn": "Tuk",
        "telephoneNumber": "+49 40 428 26-1234",
        "givenName": "Peregrin",
        "company": "LGV",
        "objectGUID": "11111111-1111-1111-1111-111111111111",
        "objectGUID_byte": "\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11",
        "sAMAccountName": "TukPer",
        "mail": "peregrin.tuk@gv.hamburg.de",
        "groups": [
            {
                "name": "RES-FS-LGV-Kundenportal-r"
            }
        ]
    },
    {
        "dn": "CN=Brandybock Meriadoc,OU=LGV,OU=Users,OU=KundenFHH,OU=Dataport,DC=fhhnet,DC=stadt,DC=hamburg,DC=de",
        "sn": "Brandybock",
        "telephoneNumber": "+49 40 428 26-1234",
        "givenName": "Meriadoc",
        "company": "LGV",
        "objectGUID": "11111111-1111-1111-1111-111111111111",
        "objectGUID_byte": "\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11",
        "sAMAccountName": "BrandMer",
        "mail": "meriadoc.brandybock@gv.hamburg.de",
        "groups": [
            {
                "name": "RES-FS-LGV-Kundenportal-r"
            }
        ]
    },
    {
        "dn": "CN=Beutlin Frodo,OU=LGV,OU=Users,OU=KundenFHH,OU=Dataport,DC=fhhnet,DC=stadt,DC=hamburg,DC=de",
        "sn": "Beutlin",
        "telephoneNumber": "+49 40 428 26-1234",
        "givenName": "Frodo",
        "company": "LGV",
        "objectGUID": "11111111-1111-1111-1111-111111111111",
        "objectGUID_byte": "\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11\\11",
        "sAMAccountName": "BeutFro",
        "mail": "frodo.beutlin@gv.hamburg.de",
        "groups": [
            {
                "name": "RES-FS-LGV-Kundenportal-c"
            }
        ]
    }
];

// create new ldapjs client
if (process.env.AUTH_METHOD === "ldap") {
    if (process.env.LDAP_URL) {
        client = ldap.createClient(
            {
                url: process.env.LDAP_URL,
                tlsOptions: {
                    rejectUnauthorized: false
                },
                reconnect: true
            }
        );

        client.on("error", (err) => {
            Logger.debug(err.message);
        });
    }
}

module.exports = {
    /**
     * define Filter and execute search for users bei sAMAccountName
     * @param {String} accountname - AD user name
     * @returns {Object} - found user
     */
    findUser: async function (accountname) {
        const searchOptions = {
            scope: "sub",
            filter: "(&(objectCategory=User)(|(sAMAccountName=" + accountname + ")(userPrincipalName=" + accountname + ")))",
            attributes: attributes.user
        };
        const user = await this._search(process.env.LDAP_BASE_DN, searchOptions);

        return user.length > 0 ? user[0] : {};
    },

    /**
     * define Filter and execute search for users bei sAMAccountName
     * @param {String} mail - mail address
     * @returns {Object} - found user
     */
    findUsersByMail: function (mail) {
        if (client) {
            const searchOptions = {
                scope: "sub",
                filter: "(&(objectCategory=User)(mail=" + mail + "))",
                attributes: attributes.user
            };

            return this._search(process.env.LDAP_BASE_DN, searchOptions);
        }
        else {
            const result = mockDataUser.filter((user) => user.mail === mail);

            for (const user of result) {
                delete user.groups;
            }

            return result;
        }

    },

    /**
     * define Filter and execute search for users bei objectGuid
     * @param {String} objectGuid - AD objectGuid
     * @returns {Object} - found user
     */
    findUserByObjectGuid: async function (objectGuid) {
        const filter = new ldap.filters.EqualityFilter({
            attribute: "objectGUID",
            value: Buffer.from(objectGuid, "base64")
        });

        const searchOptions = {
            scope: "sub",
            filter: filter, //"(&(objectCategory=User)(objectGUID=" + objectGuid + "))",
            attributes: attributes.user
        };
        const user = await this._search(process.env.LDAP_BASE_DN, searchOptions);

        return user.length > 0 ? user[0] : {};
    },

    /**
     * Find all groups the supplied user is member of
     * @param {Object} user - ad user object
     * @returns {Array} all found groups
     */
    getGroupMembershipForUser: async function (user) {
        if (client) {
            const groups = await this.getGroupMembershipForDN(user.dn);
            let groups_final = [];

            for (const group of groups) {
                if (group.memberOf) {
                    if (Array.isArray(group.memberOf)) {
                        for (const groupMember of group.memberOf) {
                            const nestedgroups = await this.getGroupMembershipForDN(groupMember);

                            if (nestedgroups.length > 0) {
                                groups_final = groups_final.concat(nestedgroups);
                            }
                            else {
                                const groupDetails = await this.findGroup(groupMember);

                                if (groupDetails.cn) {
                                    groups_final.push(groupDetails);
                                }
                            }
                        }
                    }
                    else {
                        const dnList = group.memberOf.split("CN=");

                        for (const dnItem of dnList) {
                            const dn = "CN=" + dnItem.replace(/,\s*$/, "");
                            const nestedgroups = await this.getGroupMembershipForDN("CN=" + dn);

                            if (nestedgroups.length > 0) {
                                groups_final = groups_final.concat(nestedgroups);
                            }
                            else {
                                const groupDetails = await this.findGroup(dn);

                                if (groupDetails.cn) {
                                    groups_final.push(groupDetails);
                                }
                            }
                        }
                    }
                }
            }

            const grouplist = groups_final.concat(groups).map((ug) => {
                return {name: ug.cn};
            });

            return grouplist;
        }
        else {
            let groups = [];

            const result = mockDataUser.filter((aduser) => aduser.dn === user.dn);

            for (const aduser of result) {
                groups = groups.concat(aduser.groups);
            }

            return groups;
        }
    },

    /**
     * Find group by dn
     * @param {String} dn - distinguishedName
     * @returns {Array} all found groups
     */
    findGroup: async function (dn) {
        const searchOptions = {
            scope: "sub",
            filter: "(&(objectCategory=Group)(distinguishedName=" + dn + "))",
            attributes: attributes.group
        };
        const group = await this._search(process.env.LDAP_BASE_DN, searchOptions);

        return group.length > 0 ? group[0] : {};
    },

    /**
     * Find all groups, where supplied Name ist part of groupname.
     * @param {String} groupname - search phrase
     * @returns {Array} all found groups
     */
    findGroups: async function (groupname) {
        const searchOptions = {
            scope: "sub",
            filter: "(&(objectClass=group)(!(objectClass=computer))(!(objectClass=user))(!(objectClass=person))(cn=*" + groupname + "*))",
            attributes: attributes.group
        };
        const groupsFound = await this._search(process.env.LDAP_BASE_DN, searchOptions);
        const grouplist = groupsFound.map((ug) => {
            return {name: ug.cn};
        });

        return grouplist;
    },

    /**
     * search AD for User by surname/givenname
     * @param {String} surname - surname, must be supplied
     * @param {String} givenname - givenname or "*" Wildcard
     * @returns {Array} - all found users
     */
    findUsers: function (surname, givenname) {
        if (client) {
            const searchOptions = {
                scope: "sub",
                filter: "(&(objectCategory=User)(|(cn=" + surname + " " + givenname + ")))",
                attributes: attributes.user
            };

            return this._search(process.env.LDAP_BASE_DN, searchOptions);
        }
        else {
            const result = mockDataUser.filter((user) => user.sn === surname && user.givenName === givenname);

            for (const user of result) {
                delete user.groups;
            }

            return result;
        }
    },

    /**
     * get all members for given group dn
     * @param {String} dn - distinguishedName
     * @returns {Array} - all found members
     */
    getGroupMembershipForDN: function (dn) {
        const searchOptions = {
            scope: "sub",
            filter: "(member=" + dn + ")",
            attributes: attributes.group
        };

        return this._search(process.env.LDAP_BASE_DN, searchOptions);
    },

    /**
     * execute ldap search
     * @param {String} baseDN - base search path
     * @param {Object} searchOpts - search filter and set of attributes to return
     * @returns {Object} - Promise
     */
    _search: function (baseDN, searchOpts) {
        const me = this;

        return new Promise((resolve, reject) => {
            const results = [];

            // perform bind operation against ldap
            client.bind(process.env.LDAP_USER, process.env.LDAP_PASSWORD, bindErr => {
                if (bindErr) {
                    Logger.error(bindErr);
                    reject(bindErr);
                }
                else {
                    client.search(baseDN, searchOpts, (err, res) => {
                        if (err) {
                            Logger.error(err);
                            reject(err);
                        }

                        res.on("searchEntry", entry => {
                            if (entry.object) {
                                const resEntry = entry.object;

                                if (entry.raw.hasOwnProperty("objectGUID")) {
                                    const objectGUID = me._formatGUID(entry.raw.objectGUID);

                                    resEntry.objectGUID = objectGUID;
                                    resEntry.objectGUID_base64 = entry.raw.objectGUID;
                                }

                                results.push(resEntry);
                            }
                            else if (entry.pojo) {
                                results.push(me._attributeParser(entry.pojo));
                            }
                        });

                        res.on("end", () => {
                            resolve(results);
                        });
                    });
                }
            });
        });
    },

    /**
     * convert ldap entry object attributes to flat attributes object
     * @param {Object} pojo - active directory search result with key-value collection
     * @returns {Object} - flat object with entry attributes
     */
    _attributeParser: function (pojo) {
        const obj = {};

        if (pojo.objectName) {
            obj.dn = pojo.objectName;
        }

        if (pojo.attributes) {
            for (const attr of pojo.attributes) {
                if (attr.type === "objectGUID;binary") {
                    const objectGUID = this._formatGUID(attr.values[0]);

                    obj.objectGUID = objectGUID;
                    obj.objectGUID_base64 = attr.values[0];
                }
                else {
                    obj[attr.type] = attr.values.toString();
                }
            }
        }

        return obj;
    },

    /**
     * Format objectGUI from ByteArray to UUID String
     * @param {ByteArray} objectGUID - User objectGUID as ByteArray
     * @returns {Object} - object with 2 Elements: objectGUI {String} - objectGUI as UUID String and objectGUI_vyte {String} - Byte List with delimiter
     */
    _formatGUID: function (objectGUID) {
        // Quelle: https://github.com/ldapjs/node-ldapjs/issues/297

        const data = Buffer.from(objectGUID, "base64");

        // GUID_FORMAT_D
        let template = "{3}{2}{1}{0}-{5}{4}-{7}{6}-{8}{9}-{10}{11}{12}{13}{14}{15}";

        // check each byte
        for (let i = 0; i < data.length; i++) {
            // get the current character from that byte
            let dataStr = data[i].toString(16);

            dataStr = data[i] >= 16 ? dataStr : "0" + dataStr;

            // insert that character into the template
            template = template.replace(new RegExp("\\{" + i + "\\}", "g"), dataStr);
        }

        return template;
    }
};
