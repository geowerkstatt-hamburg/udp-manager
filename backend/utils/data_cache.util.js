const values = {
    version_history: [
        {id: 0, version: "0.9"},
        {id: 1, version: "1.0"},
        {id: 2, version: "1.1"},
        {id: 3, version: "1.2"},
        {id: 4, version: "1.3"},
        {id: 5, version: "2.0"}
    ],
    dbVersion: "2.0",
    dbEmpty: true,
    dbValid: true,
    scheduledTasks: {},
    commitCount: 0,
    change: {
        dev: false,
        stage: false,
        prod: false,
        test: false
    }
};

module.exports = {
    getVersion: function () {
        return values.version_history[values.version_history.length - 1].version;
    },

    getVersionHistory: function () {
        return values.version_history;
    },

    getVersionMatch: function () {
        return values.dbVersion === values.version_history[values.version_history.length - 1].version;
    },

    setDbVersion: function (dbVersion) {
        values.dbVersion = dbVersion;
    },

    getDbVersion: function () {
        return values.dbVersion;
    },

    setDbEmpty: function (dbEmpty) {
        values.dbEmpty = dbEmpty;
    },

    setDbValid: function (dbValid) {
        values.dbValid = dbValid;
    },

    getDbEmpty: function () {
        return values.dbEmpty;
    },

    getDbValid: function () {
        return values.dbValid;
    },

    getScheduledTasks: function () {
        return values.scheduledTasks;
    },

    setScheduledTasks: function (scheduledTasks) {
        values.scheduledTasks = scheduledTasks;
    },

    getCommitCount () {
        return values.commitCount;
    },

    incrementCommitCount () {
        values.commitCount = values.commitCount + 1;
    },

    setCommitCountZero () {
        values.commitCount = 0;
    },

    getChange (env) {
        return values.change[env];
    },

    setChange (env, value) {
        values.change[env] = value;
    }
};
