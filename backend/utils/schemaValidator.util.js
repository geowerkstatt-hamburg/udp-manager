const Ajv = require("ajv");
const Logger = require("./logger.util");
const ajv = new Ajv();

const schemas = {
    addMetadataCatalog: {
        type: "object",
        properties: {
            name: {type: "string"},
            csw_url: {type: "string"},
            show_doc_url: {type: "string"},
            proxy: {type: "boolean"},
            use_in_internet_json: {type: "boolean"},
            srs_metadata: {type: "integer"}
        },
        required: ["name", "csw_url", "show_doc_url", "proxy", "srs_metadata"],
        additionalProperties: false
    },
    updateMetadataCatalog: {
        type: "object",
        properties: {
            id: {type: "integer"},
            name: {type: "string"},
            csw_url: {type: "string"},
            show_doc_url: {type: "string"},
            proxy: {type: "boolean"},
            use_in_internet_json: {type: "boolean"},
            srs_metadata: {type: "integer"}
        },
        required: ["id", "name", "csw_url", "show_doc_url", "proxy", "srs_metadata"],
        additionalProperties: false
    },
    addExtInstance: {
        type: "object",
        properties: {
            name: {type: "string"},
            host: {type: "string"},
            port: {type: "integer"},
            database: {type: "string"},
            user: {type: "string"},
            password: {type: "string"}
        },
        required: ["name", "host", "port", "database", "user", "password"],
        additionalProperties: false
    },
    updateExtInstance: {
        type: "object",
        properties: {
            id: {type: "integer"},
            name: {type: "string"},
            host: {type: "string"},
            port: {type: "integer"},
            database: {type: "string"},
            user: {type: "string"},
            password: {type: "string"}
        },
        required: ["id", "name", "host", "port", "database", "user", "password"],
        additionalProperties: false
    },
    addDbConnection: {
        type: "object",
        properties: {
            name: {type: "string"},
            host: {type: "string"},
            port: {type: "integer"},
            database: {type: "string"},
            user: {type: "string"},
            password: {type: "string"},
            use_as_default_r: {type: "boolean"},
            use_as_default_w: {type: "boolean"}
        },
        required: ["name", "host", "port", "database", "user", "password", "use_as_default_r", "use_as_default_w"],
        additionalProperties: false
    },
    updateDbConnection: {
        type: "object",
        properties: {
            id: {type: "integer"},
            name: {type: "string"},
            host: {type: "string"},
            port: {type: "integer"},
            database: {type: "string"},
            user: {type: "string"},
            password: {type: "string"},
            use_as_default_r: {type: "boolean"},
            use_as_default_w: {type: "boolean"}
        },
        required: ["id", "name", "host", "port", "database", "user", "password", "use_as_default_r", "use_as_default_w"],
        additionalProperties: false
    }
};

module.exports = {
    validate: function (schema, data) {
        const validate = ajv.compile(schemas[schema]);
        const valid = validate(data);

        if (!valid) {
            Logger.error("JSON Schema validation error - " + schema);
            console.log(validate.errors);
        }
        return valid;
    }
};
