const Logger = require("./logger.util");
const nodemailer = require("nodemailer");
const env = process.env.NODE_ENV ? process.env.NODE_ENV.trim() : "dev";

/**
 * offers functions for E-Mail notification for different workflows.
 * Every functions calls this.sendMail with a String of comma separted E-Mail recipients, a String für Mail Subject
 * and a String containing the actual message. Mail SMTP Server is only called in prod env.
 */
module.exports = {
    onDeleteLayer: async function (layer_id, title, mail_from) {
        const config = require("../config.js").getConfig();
        const subject = "UDP-Manager: Layer (" + layer_id + ") gelöscht";
        const text = "Der produktive Layer \"" + title + "\" mit der ID " + layer_id + " wurde gelöscht.";

        if (env !== "dev") {
            await this.sendMail(config.mail.delete_layer_to, subject, text, mail_from);
        }
        else {
            Logger.debug("--- Mail sent (prod only):");
            Logger.debug(config.mail.delete_layer_to);
            Logger.debug(subject);
            Logger.debug(text);
            Logger.debug(mail_from);
        }
    },

    onDeleteLayerRequest: async function (layer_id, title, comment, linked_portals, mail_from) {
        const config = require("../config.js").getConfig();
        const subject = "UDP-Manager: Layer (" + layer_id + ") zum Löschen markiert";
        let text = "Der Layer \"" + title + "\" mit der ID " + layer_id + " wurde zum Löschen markiert.<br/>";
        let linked_portals_str = "";

        for (let i = 0; i < linked_portals.length; i++) {
            linked_portals_str = linked_portals_str + "\"" + linked_portals[i].title + "\", ";
        }

        text += "<br/>Folgende Portale sind betroffen:<br/>";
        text += linked_portals_str.substring(0, linked_portals_str.length - 2);

        if (comment.length > 0) {
            text += "Kommentar:<br/>";
            text += comment;
        }

        if (env !== "dev") {
            await this.sendMail(config.mail.delete_layer_request_to, subject, text, mail_from);
        }
        else {
            Logger.debug("--- Mail sent (prod only):");
            Logger.debug(config.mail.delete_layer_request_to);
            Logger.debug(subject);
            Logger.debug(text);
            Logger.debug(mail_from);
        }
    },

    onNoMoreLinkedPortals: async function (layer_id, responsible_party) {
        const config = require("../config.js").getConfig();
        const subject = "UDP-Manager: zum löschen vorgemerkter Layer (" + layer_id + ") aus allen Portalen entfernt";
        const text = "Der Layer mit der ID " + layer_id + " wurde aus allen Portalen entfernt und kann nun gelöscht werden.";
        const mail_receiver = config.clientConfig.responsibleParty.find(el => el.name === responsible_party).mail;
        const mail_sender = config.mail.portals_from;

        if (env !== "dev") {
            await this.sendMail(mail_receiver, subject, text, mail_sender);
        }
        else {
            Logger.debug("--- Mail sent (prod only):");
            Logger.debug(mail_receiver);
            Logger.debug(subject);
            Logger.debug(text);
            Logger.debug(mail_sender);
        }
    },

    onCheckerStatus: async function (check_results) {
        const config = require("../config.js").getConfig();
        const subject = "UDP-Manager: Prüfbericht";
        const text = "Zusammenfassung der letzten Prüfung: Für " + check_results.num_cancelled + " Dienste konnte die Prüfung nicht durchgeführt werden. " + check_results.num_check + " Dienste ohne Fehler, " + check_results.num_warn + " Dienste mit Warnungen, " + check_results.num_error + " Dienste mit Fehlern.";

        if (env !== "dev") {
            await this.sendMail(config.mail.service_checker_to, subject, text);
        }
        else {
            Logger.debug("--- Mail sent (prod only):");
            Logger.debug(config.mail.service_checker_to);
            Logger.debug(subject);
            Logger.debug(text);
        }
    },

    // temporarily deactivate this function (send an e-mail when a service is put into production).
    // onProdService: async function (service_id, title, service_type, preview_text, mail_from) {
    //     const subject = "Neuer produktiver Dienst: " + title;
    //     let text = "Der Dienst \"" + title + "\" ist jetzt produktiv verfügbar.";

    //     if (preview_text) {
    //         text += " <br/>" + preview_text;
    //     }

    //     if (env !== "dev") {
    //         await this.sendMail(config.mail.prod_to, subject, text, mail_from);
    //     }
    //     else {
    //         Logger.debug("--- Mail sent (prod only):");
    //         Logger.debug(config.mail.prod_to);
    //         Logger.debug(subject);
    //         Logger.debug(text);
    //         Logger.debug(mail_from);
    //     }
    // },

    onQAService: async function (title, user, responsible_party_mail, mail_from) {
        const subject = "[QS] bitte Dienst prüfen: " + title;
        const text = "Die Bearbeitung des Dienstes " + title + " wurde durch " + user + " abgeschlossen.";

        if (env !== "dev") {
            await this.sendMail(responsible_party_mail, subject, text, mail_from);
        }
        else {
            Logger.debug("--- Mail sent (prod only):");
            Logger.debug(responsible_party_mail);
            Logger.debug(subject);
            Logger.debug(text);
            Logger.debug(mail_from);
        }
    },

    sendMail: async function (mail_to, subject, text, mail_from) {
        if (process.env.SMTP_HOST && process.env.SMTP_PORT && mail_to) {
            const transporter = nodemailer.createTransport({
                host: process.env.SMTP_HOST,
                port: process.env.SMTP_PORT,
                secure: false,
                auth: null,
                tls: {
                    rejectUnauthorized: false
                }
            });
            const mailOptions = {
                from: mail_from,
                to: mail_to,
                subject: subject,
                text: text
            };

            await transporter.sendMail(mailOptions, (error) => {
                if (error) {
                    Logger.debug("Send Mail", error);
                }
            });
        }
    }
};
