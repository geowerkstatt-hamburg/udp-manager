const xpath = require("xpath");
const xmldom = require("xmldom");
const DOMParser = xmldom.DOMParser;

module.exports = {
    parseGFIAttributes: function (gml_schema_response, gfi_url, layername) {
        const config = require("../config.js").getConfig();
        const gml_schema_doc = new DOMParser().parseFromString(gml_schema_response.data, "text/xml");
        const gfi_attributes = [];
        const select = xpath.useNamespaces({"x": "http://www.w3.org/2001/XMLSchema"});
        let elements = select("//x:element/x:complexType/x:complexContent/x:extension/x:sequence/x:element", gml_schema_doc);

        if (gfi_url.indexOf("service=WFS") > -1) {
            elements = select("//x:element[@name='" + layername + "']/x:complexType/x:complexContent/x:extension/x:sequence/x:element", gml_schema_doc);
            if (elements.length === 0) {
                elements = select("//x:complexType[@name='" + layername + "']/x:complexContent/x:extension/x:sequence/x:element", gml_schema_doc);
            }
        }

        for (let i = 0; i < elements.length; i++) {
            const featureName = select("string(@name)", elements[i]);

            let featureType = select("string(@type)", elements[i]);

            if (config.dbDatatypesMapping[featureType]) {
                featureType = config.dbDatatypesMapping[featureType];
            }
            gfi_attributes.push(
                {
                    attr_name_db: featureName,
                    attr_name_service: featureName,
                    attr_name_masterportal: featureName,
                    attr_datatype: featureType
                }
            );
        }

        return gfi_attributes;
    }
};
