const Ajv = require("ajv");
const ajv = new Ajv();

const schemas = {
    server: {
        type: "array",
        minItems: 1,
        items: {
            type: "object",
            properties: {
                name: {type: "string"},
                domain: {type: "string"},
                protocol: {type: "string"},
                lb: {type: "boolean"},
                server: {
                    type: "array",
                    minItems: 0,
                    items: {type: "string"}
                }
            },
            required: ["name", "domain", "protocol", "lb"],
            additionalProperties: false
        }
    },
    capabilities_metadata: {
        type: "object",
        properties: {
            providername: {type: "string"},
            providersite: {type: "string"},
            individualname: {type: "string"},
            positionname: {type: "string"},
            phone: {type: "string"},
            facsimile: {type: "string"},
            electronicmailaddress: {type: "string"},
            deliverypoint: {type: "string"},
            city: {type: "string"},
            administrativearea: {type: "string"},
            postalcode: {type: "string"},
            country: {type: "string"},
            onlineresource: {type: "string"},
            hoursofservice: {type: "string"},
            contactinstructions: {type: "string"},
            metadataurl: {type: "string"},
            authorityname: {type: "string"},
            authorityurl: {type: "string"}
        },
        required: ["providername", "providersite", "individualname", "positionname", "phone", "facsimile", "electronicmailaddress", "deliverypoint", "city", "administrativearea", "postalcode", "country", "onlineresource", "hoursofservice", "contactinstructions", "metadataurl", "authorityname", "authorityurl"],
        additionalProperties: false
    },
    service_types: {
        type: "array",
        minItems: 1,
        items: {
            type: "object",
            properties: {
                name: {type: "string"},
                category: {type: "string"},
                versions: {
                    type: "array",
                    minItems: 1,
                    items: {type: "string"}
                }
            },
            required: ["name", "category", "versions"],
            additionalProperties: false
        }
    },
    test_bbox: {
        type: "array",
        minItems: 1,
        items: {
            type: "object",
            properties: {
                name: {type: "string"},
                srs: {type: "string"},
                bbox: {type: "string"}
            },
            required: ["name", "srs", "bbox"],
            additionalProperties: false
        }
    },
    client_config: {
        type: "object",
        properties: {
            helpLink: {type: "string"},
            user: {
                type: "array",
                minItems: 0,
                items: {type: "string"}
            },
            responsibleParty: {
                type: "array",
                minItems: 0,
                items: {
                    type: "object",
                    properties: {
                        name: {type: "string"},
                        mail: {type: "string"},
                        mail_alt: {type: "string"},
                        inbox_name: {type: "string"},
                        inbox_name_alt: {type: "string"}
                    },
                    required: ["name"],
                    additionalProperties: false
                }
            },
            regions: {
                type: "array",
                minItems: 0,
                items: {
                    type: "object",
                    properties: {
                        landlevel: {type: "string"},
                        arealevel: {
                            type: "array",
                            minItems: 0,
                            items: {type: "string"}
                        }
                    },
                    required: ["landlevel", "arealevel"],
                    additionalProperties: false
                }
            },
            defaultDatasetSrs: {type: "string"},
            defaultCollectionNamespace: {type: "string"},
            mapPreview: {type: "string"},
            mapPreviewProd: {type: "string"},
            mapPreviewBaseLayerId: {type: "string"},
            mapPreviewComplexAppend: {type: "string"},
            securityProxyUrlSso: {
                type: "object",
                properties: {
                    dev: {type: "string"},
                    stage: {type: "string"},
                    prod: {type: "string"}
                },
                additionalProperties: false
            },
            securityProxyUrlAuth: {type: "string"}
        },
        required: [],
        additionalProperties: false
    },
    ldap: {
        type: "object",
        properties: {
            allowed_groups: {
                type: "array",
                minItems: 0,
                items: {
                    type: "object",
                    properties: {
                        name: {type: "string"},
                        role: {type: "string"}
                    },
                    required: ["name", "role"],
                    additionalProperties: false
                }
            },
            allowed_users: {
                type: "array",
                minItems: 0,
                items: {
                    type: "object",
                    properties: {
                        name: {type: "string"},
                        role: {type: "string"}
                    },
                    required: ["name", "role"],
                    additionalProperties: false
                }
            }
        },
        required: ["allowed_groups", "allowed_users"],
        additionalProperties: false
    },
    jira: {
        type: "object",
        properties: {
            url: {type: "string"},
            browseUrl: {type: "string"},
            issueTypeId: {
                type: "object",
                properties: {
                    task: {type: "string"},
                    sub_task: {type: "string"}
                }
            },
            projects: {
                type: "array",
                minItems: 1,
                items: {
                    type: "object",
                    properties: {
                        name: {type: "string"},
                        name_alias: {type: "string"},
                        new_as_subtask: {type: "boolean"},
                        use_in_ticketsearch: {type: "boolean"},
                        story_points_value: {type: "integer"},
                        transition_execution: {type: "boolean"},
                        transition_id: {
                            type: "array",
                            minItems: 1,
                            items: {type: "integer"}
                        },
                        components: {
                            type: "array",
                            minItems: 0,
                            items: {
                                type: "object",
                                properties: {
                                    name: {type: "string"}
                                },
                                required: [],
                                additionalProperties: false
                            }
                        }
                    },
                    required: ["name", "name_alias", "new_as_subtask", "use_in_ticketsearch", "story_points_value", "transition_execution", "transition_id", "components"],
                    additionalProperties: false
                }
            },
            defaultAssignee: {type: "string"},
            labels: {
                type: "array",
                minItems: 1,
                items: {type: "string"}
            },
            proxy: {type: "boolean"}
        },
        required: ["url", "browseUrl", "issueTypeId", "projects", "defaultAssignee", "labels", "proxy"],
        additionalProperties: false
    },
    mail: {
        type: "object",
        properties: {
            portals_from: {type: "string"},
            prod_to: {type: "string"},
            delete_layer_to: {type: "string"},
            delete_layer_request_to: {type: "string"}
        },
        required: ["portals_from", "prod_to", "delete_layer_to", "delete_layer_request_to"],
        additionalProperties: false
    },
    git: {
        type: "object",
        properties: {
            options_config: {
                type: "array",
                minItems: 0,
                items: {type: "string"}
            },
            branch: {type: "string"},
            ssh: {type: "boolean"},
            sshKnownHosts: {type: "string"},
            sshKey: {type: "string"}
        },
        required: ["options_config", "branch", "ssh"],
        additionalProperties: false
    },
    defaultbbox: {type: "string"},
    available_projections: {
        type: "array",
        minItems: 1,
        items: {type: "string"}
    }
};

const softwareSchema = {
    type: "object",
    properties: {
        dev: {
            type: "object",
            properties: {
                instances: {
                    type: "array",
                    minItems: 0,
                    items: {type: "string"}
                },
                urlIntTemplate: {type: "string"},
                urlExtTemplate: {type: "string"},
                defaultServer: {type: "string"},
                softwareSpecificParameter: {type: "object"}
            }
        },
        stage: {
            type: "object",
            properties: {
                instances: {
                    type: "array",
                    minItems: 0,
                    items: {type: "string"}
                },
                urlIntTemplate: {type: "string"},
                urlExtTemplate: {type: "string"},
                defaultServer: {type: "string"},
                softwareSpecificParameter: {type: "object"}
            }
        },
        prod: {
            type: "object",
            properties: {
                instances: {
                    type: "array",
                    minItems: 0,
                    items: {type: "string"}
                },
                urlIntTemplate: {type: "string"},
                urlExtTemplate: {type: "string"},
                defaultServer: {type: "string"},
                softwareSpecificParameter: {type: "object"}
            }
        },
        seperateDevWorkspace: {type: "boolean"},
        internalExternalWorkspace: {
            type: "object",
            properties: {
                internal: {type: "string"},
                external: {type: "string"}
            }
        },
        supportedApiTypes: {type: "object"},
        dataTypeMap: {type: "object"},
        inspireSettings: {
            type: "object",
            properties: {
                languageCode: {type: "string"}
            }
        }
    },
    required: ["dev", "stage", "prod", "supportedApiTypes"],
    additionalProperties: false
};

module.exports = {
    validate: function (data) {
        for (const software in data.software) {
            const validateSoftware = ajv.compile(softwareSchema);
            const validSoftware = validateSoftware(data.software[software]);

            if (!validSoftware) {
                this._throwError(validateSoftware, software);
            }
        }

        for (const schema in schemas) {
            const validate = ajv.compile(schemas[schema]);
            const valid = validate(data[schema]);

            if (!valid) {
                this._throwError(validate, schema);
            }
        }
    },

    _throwError: function (validate, schema) {
        // CAUTION: ajv only returns the first error by default: https://ajv.js.org/options.html#allerrors
        // Changing this option may introduce vulnerabilities, see https://stackoverflow.com/q/72520664
        const messagePrefix = `Validierung der '${schema}' Konfiguration fehlgeschlagen!<br>`;
        let message = "";
        const error = validate.errors[0];
        const errorType = error.keyword;

        switch (errorType) {
            case "required":
                message = `Benötigtes Attribut nicht vorhanden: ${error.params.missingProperty}`;
                break;
            case "additionalProperties":
                message = `Unerwartetes Attribut vorhanden: ${error.params.additionalProperty}`;
                break;
            case "type":
                message = `Attribut fehlerhaft: ${error.instancePath.substring(1)}.<br>Fehlermeldung: "${error.message}"`;
                break;
            default:
                for (const key in error) {
                    const value = JSON.stringify(error[key]);

                    message += `\n  "${key}": ${value}`;
                }
        }

        const parsedMessage = messagePrefix + message;

        throw new Error(parsedMessage);
    }
};
