![UDP-Manager](./doc/images/logo_udpm.png)

The UDP Manager is a browser-based application for managing datasets and APIs and provides a range of central functions for the operation of an urban data platform (UDP) or spatial data infrastructure (SDI).

The frontend is based on the GPL version of [ExtJS](https://www.sencha.com/legal/gpl/). The data is stored in a PostgreSQL database and the backend is based on a NodeJS express application. UDP-Manager is open source software released under the [GPL v3 License](http://www.gnu.org/licenses/gpl.html).

UDP-Manager is a project by [Geowerkstatt Hamburg](https://www.hamburg.de/geowerkstatt/).

**New major release v2! Many changes (including breaking changes) in all components! See [Changelog](./CHANGELOG.md)**

## Main features
* Central overview of all information on datasets and their access options (release, authorizations, data structure, interfaces)
* Coupling of dataset and service metadata
* Define data schema and determine cartographic design
* Create api configuration for the deegree, GeoServer, ldproxy and MapServer software
* Automatic versioning of the api configurations in a git repository
* Generate the [Masterportal](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/) layer JSON objects
* Management of Elasticsearch search indexes for the Masterportal theme search


## Documentation
* [Download](https://bitbucket.org/geowerkstatt-hamburg/udp-manager/downloads/)
* [Setup](./doc/en/setup.md)
* [Migration](./doc/en/migration.md)
* [User Documentation (german)](https://geoportal-hamburg.de/udp_manager_docs/de/user_doc/)
* [Changelog](./CHANGELOG.md)

## Contact
[LGV Hamburg - UDP IT-Infrastruktur](mailto:geobasisinfrastruktur@gv.hamburg.de)