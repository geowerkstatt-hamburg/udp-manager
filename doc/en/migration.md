# Migration

How-To migrate data from Dienstemanager to UDP-Manager:

- UDP-Manager database must be initialized and empty (see [setup](setup.md))
- edit /migration_tool/config_template.js und rename file to /migration_tool/config.js
- set Dienstemanager source DB parameter in /migration_tool/config.js
- set UDP-Manager target DB parameter in /migration_tool/config.js
- start migration with "npm run migrate"

**Attention:**

There are some new entities and attributes that can not automatically be set in the migration process. Manual revision might be necessary!

For datasets without a **shortname**, a new **shortname** is generated according to the pattern **change_it_[int]**.

For services without dataset metadata coupling, a new dataset object will be generated. If a direct matching by service title is not possible, two different datasets can be generated for WMS and WFS that actually serve the same data.