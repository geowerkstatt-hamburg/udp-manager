# Setup
Description how to set up a UDP-Manager instance.

## System requirements

- NodeJS 18 LTS or higher
- PostgreSQL 13 or higher
- Optional: LDAP (Active Directory) for authentication
- Optional: Elasticsearch 8.1 or higher

## Installation

- Download latest stable version:

[https://bitbucket.org/geowerkstatt-hamburg/udp-manager/downloads/UDP-Manager\_latest\_stable.zip](https://bitbucket.org/geowerkstatt-hamburg/udp-manager/downloads/UDP-Manager_latest_stable.zip)

- unzip file
- run _npm install_ in application folder
- a NodeJS Process Manager (e.g. PM2) is recommended for productive environment.
- create PostgreSQL database and a db user.
- rename _/.env.template_ to _/.env_ and edit:
  
  - `LOG_LEVEL`: Setting the log level. If set to "info", errors and warnings will be logged. With "debug", further messages on processing steps are written to the log
  - `NODE_PORT`: Defines the local port under which the NodeJS application is available
  - `NODE_HOST`: Binds and listens on a specific host name
  - `SECRET`: Key used to encrypt passwords stored in the database. Must be a string with 32 characters!
  - `API_TOKEN`: Any String. All requests to /backend routes must have the header "token: {API_TOKEN}". If set to "*" token check is disabled.
  - `CONFIG_OUT_PATH`: Absolut path where service configurations will be stored
  - `AUTH_METHOD`: Set Authentication Method. One of "local", "ldap" or "keycloak" must be set
  - `KC_ISSUER_URL`: OPTIONAL - Only necessary when AUTH_METHOD=keycloak. URL of Keycloak realm
  - `KC_CLIENT_ID`: OPTIONAL - Only necessary when AUTH_METHOD=keycloak. Keycloak Client ID
  - `KC_CLIENT_SECRET`: OPTIONAL - Only necessary when AUTH_METHOD=keycloak. Keycloak Client secret
  - `KC_REDIRECT_URL`: OPTIONAL - Only necessary when AUTH_METHOD=keycloak. http(s)://{{host}}:{{port}}/auth/callback
  - `KC_POST_LOGOUT_REDIRECT_URL`: OPTIONAL - Only necessary when AUTH_METHOD=keycloak. http(s)://{{host}}:{{port}}/logout/callback
  - `LDAP_URL`: OPTIONAL - Only necessary when AUTH_METHOD=ldap. 
  - `LDAP_BASE_DN`: OPTIONAL - Only necessary when AUTH_METHOD=ldap. 
  - `LDAP_USER`: OPTIONAL - Only necessary when AUTH_METHOD=ldap. 
  - `LDAP_PASSWORD`: OPTIONAL - Only necessary when AUTH_METHOD=ldap. 
  - `LDAP_ADMIN_GROUP`: OPTIONAL - Only necessary when AUTH_METHOD=ldap. Name of the AD group that will authorized for the UDP Manager as admin role
  - `DEV_AD_USER`: OPTIONAL - only necessary for local dev environment
  - `PROXY_HOST`: proxy host name
  - `PROXY_PORT`: proxy port
  - `DB_HOST`: database host name
  - `DB_PORT`: database port
  - `DB_NAME`: database name
  - `DB_USER`: database user
  - `DB_PASSWORD`: database password
  - `DB_DIAGNOSTICS`: true / false, if true all database sql statements are logged
  - `SMTP_HOST`: OPTIONAL - Hostname of the SMTP server
  - `SMTP_PORT`: OPTIONAL - Port of the SMTP server
  - `GIT_REPO_URL`: OPTIONAL - git repository url
  - `JIRA_USERNAME`: OPTIONAL
  - `JIRA_PASSWORD`: OPTIONAL

- Optional: configure preview portal: [https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/setup.md](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/setup.md)

- Optional: Configure access restriction and reverse proxy (see next chapter)
- start UDP-Manager (_npm run prod_)

Note: the corresponding start script (in the _scripts_ element) in the package.json file is designed for a Windows environment. For operation under Linux, the environment variables must be set with "export" instead of "set".

- open UDP-Manager in any browser
- if the database is empty, you will be asked to create the schema, press "Initialisieren"
- edit app configuration. See [config documentation](https://bitbucket.org/geowerkstatt-hamburg/udp-manager/src/main/doc/en/config.md)

## keycloak setup
TBD


## Apache http webserver configuration (AD / ldap authentication)

This describes how an access restriction can be set up in an Apache web server 2.4 using an existing LDAP (e.g. Microsoft Active Directory) and a reverse proxy.


### access restriction

The following Apache modules are required:

_mod_ldap_, _mod_authn_core_, _mod_authz_user_, _mod_headers_, _mod_rewrite_, _auth_ntlm_module_


The following section must be added to the Apache configuration:

    <Location /udp-manager/app>
        AuthName "UDP-Manager"
        AuthType SSPI
        NTLMAuth On
        NTLMAuthoritative On
        NTLMOfferBasic On
        <RequireAll>
            <RequireAny>
                Require valid-user
            </RequireAny>
            <RequireNone>
                Require user "ANONYMOUS LOGON"
                Require user "NT-AUTORITÄT\ANONYMOUS-ANMELDUNG"
            </RequireNone>
        </RequireAll>
        RequestHeader set remoteuser expr=%{REMOTE_USER}
    </Location>

### Reverse-Proxy

By default, the NodeJS application runs on port 9020. If additional static content is delivered via Apache, the UDP-Manager can also be made available via port 80/443 via a reverse proxy.

The following Apache modules are required:

_proxy_, _proxy_wstunnel_module_, _rewrite_module_, _proxy_http_module_

The following section must be added to the Apache configuration:

    RewriteEngine On
    RewriteCond %{REQUEST_URI} ^/udpm_ws [NC]
    RewriteCond %{QUERY_STRING} transport=websocket [NC]
    RewriteRule /(.\*) ws://localhost:9020/$1 [P,L]

    ProxyPass /udpm_ws http://localhost:9020/udpm_ws keepalive=On
    ProxyPassReverse /udpm_ws http://localhost:9020/udpm_ws
    ProxyPass /udp-manager http://localhost:9020 timeout=1200 keepalive=On ttl=1200 connectiontimeout=1200 retry=1200
    ProxyPassReverse /udp-manager http://localhost:9020

## Setup local dev environment
**_ATTENTION:_** due to senchas new license policy, ExtJS and CMD are no longer available as free download from the Sencha Homepage
.
The ExtJS SDK ist required. It can be downloaded [here](https://github.com/tremez/extjs-gpl).
The content must then be moved to ./frontend/ext.

Install NodeJS modules:
```
npm install
```

Start app:
```
npm start
```

Sencha CMD is required to build the frontend.
```
npm install -g @sencha/cmd
```

Start local dev build
```
sencha app watch
```
Production build
```
sencha app build production
```

The output paths for the builds can be configured in ./frontend/app.json (default is ""../../[prod/test/dev]_build")

### Build and run Docker container

To build the docker container run:
```
docker build --build-arg http_proxy=[proxy_url] --build-arg https_proxy=[proxy_url] -t udpmanager:[version] .
```
If you are behind a company proxy the build-args can be set.


To start the container run:
```
docker run --env-file ./.env --name udpmanager -d -p 3000:3000 -v ./api-configs:/usr/src/api-configs udpmanager:[version]
```
**_ATTENTION:_**  NODE_HOST env parameter must be set to '0.0.0.0'. ldap AUTH_METHOD is not supported in docker context.

To run udp-manager with postgresql container, use docker compose:
```
docker compose --env-file ./.env up
```

before running, you have to set services/app/volumes mount source.