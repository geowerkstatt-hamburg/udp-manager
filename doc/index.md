![UDP-Manager](./images/logo_udpm.png)

Der UDP-Manager ist eine browserbasierte Anwendung zur Verwaltung von Datensätzen und APIs und stellt eine Reihe zentraler Funktionen für den Betrieb einer urbanen Datenplattform (UDP) oder Geodateninfrastruktur (GDI) zur Verfügung.

Das Frontend basiert auf der GPL-Version des JavaScript Frameworks ExtJS. Die Daten werden in einer PostgreSQL Datenbank abgelegt und das Backend baut auf einer NodeJS express Anwendung auf. Der UDP-Manager ist eine Open-Source-Software, die unter der [GPL v3 License](http://www.gnu.org/licenses/gpl.html) veröffentlicht ist.

Dies ist ein Projekt der [Geowerkstatt Hamburg](https://www.hamburg.de/geowerkstatt/).

## Hauptfunktionen

* Zentrale Übersicht sämtlicher Informationen zu Datensätzen und deren Bereitstellung (Freigabe, Berechtigungen, Datenstruktur, Schnittstellen)
* Datenschema definieren und Kartographische Ausgestaltung (Styling) festlegen
* Schnittstellenkonfigurationen für die Software deegree, GeoServer, ldproxy und MapServer erzeugen
* Kopplung von Datensatz- und Schnittstellenmetadaten
* Automatische Versionierung der Schnittstellenkonfigurationen in einem git Repository
* Generieren der [Masterportal](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/) Layer JSON Objekte
* Verwaltung von Elasticsearch Suchindizes für die Masterportal Themensuche

## Verweise

[Repository](https://bitbucket.org/geowerkstatt-hamburg/udp-manager/src/main/)

[Installation](https://bitbucket.org/geowerkstatt-hamburg/udp-manager/src/main/doc/en/setup.md)

[Migration](https://bitbucket.org/geowerkstatt-hamburg/udp-manager/src/main/doc/en/migration.md)

[Downloads](https://bitbucket.org/geowerkstatt-hamburg/udp-manager/downloads/)

[Changelog](https://bitbucket.org/geowerkstatt-hamburg/udp-manager/src/main/CHANGELOG.md)

## Kontakt
[LGV Hamburg - UDP IT-Infrastruktur](mailto:geobasisinfrastruktur@gv.hamburg.de)
