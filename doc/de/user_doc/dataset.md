# Datensatz erfassen
Der Datensatz ist das führende Objekt, mit dem alle weiteren Entitäten verknüpft sind. Zu Beginn der Erfassung steht entsprechend die Erzeugung eines neuen Datensatzobjektes.

Über den (+) Button unterhalb der Datensatzliste kann ein neues Datensatzobjekt angelegt werden.

Im oberen Bereich des Datensatz-Details-Formulars sind einige vom System vergebene Informationen zu sehen, die nicht manuell bearbeitet werden können. Die Felder werden nach dem speichern automatisch befüllt:

* **Status**: Angabe des Status, in dem sich der Datensatz befindet. In folgenden Status kann ein Datensatz vorliegen:
    * _In Bearbeitung_: Neu angelegte Datensätze, bzw. alle Datensätze die keine produktive Schnittstelle besitzen. Alle Formulare können bearbeitet werden.
    * _veröffentlicht - in Bearbeitung_: Datensätze mit produktiven Schnittstellen, die sich in der Überarbeitung befinden. Verknüpfte Schnittstellen liegen im Status "prod" und im Status "dev" vor.
    * _vorveröffentlicht_: Alle Schnittstellen des Datensatzes befinden sich im Status "stage".
    * _veröffentlicht - vorveröffentlicht_: Die Überarbeitung dieses Datensatzes ist abgeschlossen. Die Schnittstellen wurden aus "dev" in den Status "stage" verschoben, aber noch nicht neu veröffentlicht.
    * _veröffentlicht_: Es gibt keine aktive Bearbeitung, sämtliche Schnittstellen liegen im Status “prod” sowie “stage” vor.

* **Veröffentlichung**: Datum an dem zum ersten mal die Schnittstellen des Datensatzes veröffentlicht, sprich in den Status "prod" verschoben wurden.
* **Erstellt am**: Datum an dem der Datensatzeintrag erstellt wurde.
* **von**: Name der Person, die den Datensatzeintrag angelegt hatte.
* **Zuletzt bearbeitet**: Datum an dem der Datensatzeintrag oder verknüpfte Elemente zuletzt bearbeitet wurden.
* **von**: Name der Person, die den Datensatzeintrag zuletzt bearbeitet hatte.

Im _Datensatz-Details Bereich_ können nun die Informationen in die entsprechenden Felder eingetragen werden. Folgende Attribute müssen befüllt sein:

<a style="color:red; font-size:1em">*</a> Pflichtfeld <a style="color:grey; font-size:1em">\*</a> Standardwert

* **Titel**<a style="color:red; font-size:1em">*</a>: Titel des Datensatzes, analog zum Eintrag im Metadatenkatalog.
* **Kurzname**<a style="color:red; font-size:1em">*</a>: Anhand dieser Angabe werden später weitere Attribute abgeleitet (Schema, Endpunktname). Der Kurzname muss folgende formelle Kriterien erfüllen:
    * Eindeutigkeit
    * Kleinschreibung
    * Mehrere Wörter mittels Unterstrichen verknüpfen (Schlangenschreibweise)
    * Keine Umlaute
    * Keine Sonderzeichen (außer Unterstrich)
    * Keine Zahlen zu Beginn des Kurznamens
    * Max. 60 Zeichen
* **Verantwortliche Stelle**<a style="color:red; font-size:1em">*</a>: Auswahl der einer Verantwortlichen Stelle aus dem Dropdown-Menü. Die vorgegebenen Werte werden in der Konfiguration hinterlegt.
* **Freigabeebene**<a style="color:red; font-size:1em">*</a>: Über das Dop-Down kann _intranet_ (Veröffentlichung nur im Intranet vorgesehen) oder _internet_ (Veröffentlichung auch im Internet) ausgewählt werden. Ist in der Softwarekonfiguration das Attribut internalExternalWorkspace vergeben, wird automatisch abhängig der Freigabeebene ein interner und externer Workspace erzeugt und befüllt.

Zum Abschluss muss der _Speichern_ Button betätigt werden.

Folgende weitere Felder können befüllt werden:
**Externer Datensatz**:
Wenn angehakt kennzeichnet diese Checkbox einen Datensatz, der zwar im UDP-Manager beschrieben ist, die Daten und Schnittstellen aber in externen Systemen bereit gestellt werden.

**Projekt / Kostensammler**:
Name eines Projektes, zu welchem der Datensatz zugeordnet werden kann.

**Filter-Schlagwort**:
Möglichkeit diesem Datensätze Schlagworte zuzuordnen, über die gefiltert werden kann. Das ist z.B. relevant wenn gefilterte Elasticsearch Indizes angelegt werden sollen.

**Regionale Zugehörigkeit**:
Angabe zur Länder- und Kreisebene, die dem Datensatz zugeordnet werden können.

**Datenquelle/Fachsystem**:
Informationen zum Quell-/ oder Fachsystem der Daten.

**Koordinatenreferenzsystem**<a style="color:grey; font-size:1em">*</a>:
EPSG Code (in der Form EPSG:{ID}), in dem die Daten vorliegen.

**Datentyp**<a style="color:grey; font-size:1em">*</a>:
Auswahl zwischen Vektor, Raster oder Beides. Bei Auswahl von "Beides" kann für jede Collection einzeln der Datentyp festgelegt werden.

**DB-Verbindung (lesend/schreibend)**<a style="color:grey; font-size:1em">*</a>:
Wird der Datentyp _Vektor_ ausgewählt, werden diese beiden Felder sichtbar. Aus den Drop-Downs kann nun eine zuvor in der Konfiguration definierten Datenbankverbindung (siehe **[Datenbankverbindungen konfigurieren](./management)**) ausgewählt werden.

**Speicherort**:
Wird der Datentyp _Raster_ ausgewählt, wird das Feld Speicherort sichtbar. Hier kann der Speicherort der Rasterdaten angegeben werden.

## Datenfreigabe
In diesem Bereich können weitere Details für die Freigabe der Daten eingetragen werden. Im Feld _Link zur Freigabe_ kann eine URL auf z.B. ein PDF Dokument hinterlegt werden. In den Feldern zur _Datenproduzierende Stelle_ können weitere Details zum Datenlieferanten eingetragen werden.

Unter _Berechtigungen_ können Personen, die als Kontakt (siehe **[Kontakte verwalten](../management)**) erfasst wurden mit dem Datensatz verknüpft werden. Über _AD Gruppen berechtigen_ kann das Active Directory nach Gruppennamen durchsucht und gefundene Gruppen ebenfalls mit dem Datensatz verknüpft werden.

Wird die Checkbox _Portalgebunden_ aktiviert kann über das Drop-Down _Name des Portals_ ein zuvor erfasstes Portal (siehe **[Portale verwalten](../portal)**) ausgewählt werden.

## Metadaten
Im Bereich _Metadaten_ kann die Kopplung zu einem Datensatz-Metadatensatz eingetragen werden. Nach Auswahl des Quellkatalogs kann über den Suchknopf (Lupe-Symbol) der gewählte Metadatenkatalog nach diesem Datensatz durchsucht werden. Dafür wird der Wert aus dem Feld _Titel_, oder wenn befüllt die _Metadaten UUID_ verwendet. In einem sich neu öffnenden Fenster werden die Suchergebnisse angezeigt. Nach Auswahl eines Ergebnisses aus der Metadatensuche werden die Felder _Beschreibung_, _Schlagworte_, _Zugangsbeschränkungen_, _Nutzungsbedingungen_, _Nutzungsbedingungen (JSON)_, _Kategorien_, _Ansprechpartner E-Mail_ sowie _INSPIRE-identifiziert_ automatisch befüllt.

## zusätzliche Informationen
Freitextfeld um sonstige Informationen zu diesem Datensatz zu dokumentieren.

HINWEIS: Es werden keine Schnittstellen importiert, die mit weiteren Datensätzen verknüpft sind!