# Verwaltung

## Konfigurationen
Siehe [Konfiguration](../configuration)

## Geplante Aufgaben
Unter diesem Reiter unter "Verwaltung" können Geplante Aufgaben (Scheduled Tasks) konfiguriert werden. Durch Doppelklick in die Spalte „Zeitplan" kann ein neues cron-Pattern eingegeben werden. Durch das setzen des Häkchens in der Spalte „aktiv" wird die entsprechende Aufgabe aktiviert. Über einen Klick in die Spalte „starten" kann die entsprechende Aufgabe direkt ausgeführt werden.

Änderungen in der Tabelle können über „speichern" gespeichert werden. Der aktuelle Status der Aufgaben kann über das Aktualisieren-Symbol neu geladen werden.

### Layer JSON Objekte neu generieren (generate_layer_json)
Für sämtliche Layer die mit Schnittstellen im Status dev oder stage verknüpft sind werden die Masterportal JSON Objekte neu generiert.

### git Repository aufräumen (git clean)
Es werden nacheinander folgende git Kommandos ausgeführt:
- git push (alle offene Commits ans Repository senden)
- git checkout/reset hard/pull (der letzte Stand aus dem Repository wird abgerufen und alle lokalen Änderungen verworfen)
- git clean (es werden alle nicht getrackten Objekte entfernt)

Es wird empfohlen diesen Task einmal pro Tag (nachts) auszuführen.

### git Repository klonen (git_clone)
Um ein Remote Repository in das lokale File-System des UDP-Managers zu klonen, kann dieser Task ausgeführt werden. Das Ausführen ist nur notwendig, wenn noch kein lokales Repository vorhanden ist.

### Elasticsearch Indizes (neu-)anlegen und befüllen (initialize_elastic_indexes)
Es werden alle konfigurierten Indizes auf allen Servern gelöscht, neu mit den hinterlegten Settings und Mappings angelegt und mit den Layer JSON Objekten befüllt.

### dev Dienstkonfigurationen zu Workspaces zusammen stellen (prepare_dev_workspaces)
Alle Dienstkonfigurationen mit Status _dev_ werden in die jeweiligen Workspace Ordner zusammen kopiert und im Unterordner /workspaces/dev abgelegt.

### stage Dienstkonfigurationen zu Workspaces zusammen stellen (prepare_stage_workspaces)
Alle Dienstkonfigurationen mit Status _stage_ werden in die jeweiligen Workspace Ordner zusammen kopiert und im Unterordner /workspaces/stage abgelegt.

### prod Dienstkonfigurationen zu Workspaces zusammen stellen (prepare_prod_workspaces)
Alle Dienstkonfigurationen mit Status _prod_ werden in die jeweiligen Workspace Ordner zusammen kopiert und im Unterordner /workspaces/prod abgelegt
s
### Datensatz-Metadaten aktualisieren (update_dataset_metadata)
Bei Datensätzen mit aktiver Metadatenkopplung auf einen externen Metadatenkatalog werden die Parameter "Beschreibung", "Schlagworte", "Zugangsbeschränkungen", "Nutzungsbedingungen", "Kategorien" und "INSPIRE-identifiziert" mit den Angaben aus dem Metadatensatz aktualisiert.

### Dienstmetadaten aktualisieren (update_service_metadata)
Bei Schnittstellen mit aktiver Metadatenkopplung auf einen externen Metadatenkatalog werden die Parameter "Beschreibung", "Schlagworte", "Zugangsbeschränkungen" und "Nutzungsbedingungen" und mit den Angaben aus dem Metadatensatz aktualisiert.

### LayerIDs aus Masterportalinstanzen auslesen (portal_scraper)
Diese geplante Aufgabe verwendet WebDAV, um auf die Server zuzugreifen, auf denen die Masterportalinstanzen abgelegt sind. Hierbei wird die Konfiguration der Masterportalinstanzen mit den enthaltenen LayerIDs ausgelesen und das Layer-Portal Mapping in der UDP-Manager Datenbank abgelegt. Daher muss die config.js ein Element webdav enthalten, in dem mindestens eine WebDAV-Verbindung hinterlegt ist. HINWEIS: für diesen Prozess ist es notwendig, dass eine config.json der Masterportalinstanz beiliegt.

## Kontakte verwalten

Es können Kontakte erstellt (_Neu_) oder aus einem LDAP (Active-Directory) importiert werden (_Neu aus LDAP_). In der Liste können vorhandene Kontakte ausgewählt und im Formular auf der rechten Seite bearbeitet oder gelöscht werden.