# Schnittstellen erfassen

Über den (+) Button unterhalb der Schnittstellenliste kann ein neues Schnittstellenobjekt angelegt werden.

Nach Auswahl von _Typ_ und _Software_ werden die restlichen Felder mit den in der _Software-Konfiguration_ definierten Vorgaben befüllt. Im Standardfall sind keine weiteren Anpassungen notwendig.

Zum Abschluss muss der _Speichern_ Button betätigt werden.

<a style="color:red; font-size:1em">*</a> Pflichtfeld <a style="color:grey; font-size:1em">\*</a> Standardwert

**QA Prüfung**: Über das Setzen des Häkchens kann kenntlich gemacht werden, dass sich die Schnittstelle in der Qualitätsabnahme befindet und geprüft wird. Ist das Häkchen gesetzt und wird das Formular gespeichert, wird automatisch eine E-Mail an die Verantwortliche Stelle des Datensatzes verschickt.

**Typ**<a style="color:red; font-size:1em">*</a>: Auswahl des Schnittstellentyps über ein Dropdown-Menü.

**Externe Schnittstelle**: Wenn angehakt kennzeichnet diese Checkbox eine Schnittstelle, die von einem externen Anbieter konfiguriert wurde. Alle weiteren Felder zur internen Konfiguration einer Schnittstelle werden deaktiviert.

**Software**<span style="color:red; font-size:1em">*</span>: Software, die für die Schnittstellenkonfiguration verwendet werden soll. Auswahl über ein Dropdown-Menü.

**Endpoint-Name**<span style="color:red; font-size:1em">*</span><span style="color:grey; font-size:1em">\*</span>: Name des Endpunktes der Schnittstelle. Setzt sich aus dem Schnittstellen-Typ und dem Kurznamen des Datensatzes zusammen.

**Titel**<span style="color:red; font-size:1em">*</span><span style="color:grey; font-size:1em">\*</span>: Titel der Schnittstelle. Setzt sich aus dem Schnittstellen-Typ und dem Titel des Datensatzes zusammen.

**Version**<a style="color:red; font-size:1em">*</a>: Version des zugrundeliegenden Schnittstellentyps. Auswahl über ein Dropdown-Menü. Kann bei nicht OGC Schnittstellen auch frei eingetragen werden.

**Server**<span style="color:red; font-size:1em">*</span><span style="color:grey; font-size:1em">\*</span>: Name des Servers, auf dem die Schnittstelle erstellt wurde. Auswahl über ein Dropdown-Menü.

**Instanz / Ordner**<span style="color:red; font-size:1em">*</span><span style="color:grey; font-size:1em">\*</span>: Name der Instanz oder des Ordners, in der/dem die Schnittstellenkonfigurationen liegen.

**Interne URL**<span style="color:grey; font-size:1em">*</span>: Interne Server-URL, über die die Schnittstelle intern aufgerufen werden kann. Setzt sich aus den vorher eingetragenen Informationen zusammen.

**Externe URL**<span style="color:red; font-size:1em">*</span><span style="color:grey; font-size:1em">\*</span>: Externe Server-URL, über die die Schnittstelle extern aufgerufen werden kann. Für den Pfad der URL wird der Endpoint-Name herangezogen.

**Klickradius (Px)**: Gibt an in welchem Radius um ein Objekt eine GetFeatureInfo Abfrage ausgelöst wird. Nur bei WMS Schnittstellen einstellbar.

**Max. Features**: Maximale Anzahl von Features, die bei einen Aufruf zurückgegeben werden. Nur für WMS/WFS/WFS-T Schnittstellen einstellbar.

**zusätzliche Informationen**: Freitextfeld um sonstige Informationen zu der Schnittstelle zu dokumentieren.

## Spezialeinstellungen
**Automatische Config-Generierung deaktivieren**: Über die Checkbox kann für die gewählte Schnittstelle das automatische Generieren der Konfigurationsdateien deaktiviert werden. Im Veröffentlichungsworkflow werden dann nur die manuell hinterlegten / bearbeiteten Dateien verschoben.

**Konfigurations-Management deaktivieren**: Über die Checkbox kann die komplette Verwaltung von Konfigurationsdateien für diese Schnittstelle deaktiviert werden. Es wird dann nur der Status der jeweiligen Schnittstelle im Veröffentlichungsworkflow angepasst.

## WMS Themenkonfiguration
Nur sichtbar für Schnittstellentyp "WMS" / "WMS-Time" und die Software "deegree", "GeoServer" und "MapServer". Im linken Bereich werden sämtliche, noch nicht zugeordnete WMS Layer angezeigt. Diese können via Drag-and-Drop in den rechten Bereich verschoben werden. Dabei kann eine beliebige Reihenfolge gewählt werden und Contentlayer in Gruppenlayern organisiert werden. Diese Struktur wird in den WMS Capabilities abgebildet. Werden keine Layer im rechten Bereich zugeordnet, werden sämtliche verfügbaren Layer nach der Reihenfolge wie unter _Collections_ angegeben im WMS angelegt.

## geschützte Schnittstellen konfigurieren
Funktion kann über das Hinzufügen von _serviceSecurity_ in _modules_ in der _config.js_ aktiviert werden.

Über das Drop-Down Menü _Absicherung_ können folgende Arten der Absicherung ausgewählt werden (Liste kann über das Objekt _securityType_ in der config.js erweitert werden):
- Basic Auth
- AD SSO
- IP Schutz
- Referer-Filter

Zu beachten ist hierbei, dass bei Auswahl AD SSO weitere Einstellungsmöglichkeiten existieren, wobei alle anderen Optionen rein informativ sind und weitere Angaben unter z.B. den Kommentaren abgelegt werden müssen

**AD SSO**
Wird aus dem Drop-Down "AD SSO" gewählt, werden Eingabemöglichkeiten für die Berechtigung von AD-Gruppen sowie für die Abgesicherte URL (wird automatisch aus _externe URL_ befüllt) eingeblendet. Nach Klick auf _AD Gruppen berechtigen wird ein Fenster geöffnet, in dem im Suchschlitz am oberen linken Rand nach einer AD Gruppe gesucht werden kann. Suchergebnisse können über _+_ in die Liste der berechtigten Gruppen hinzugefügt werden. Berechtigte Gruppen können über _-_ auch wieder aus der Liste entfernt werden.

_Hinweis:_ Der UDP-Manager selbst bietet keine Absicherungsschicht, sondern nur eine Eingabemöglichkeit zur Abbildung der Berechtigung.

## Schnittstellen-Metadaten koppeln

Im Bereich _Schnittstellen_ kann unter dem Punkt _Metadatenkopplung_ die Verknüpfung zum Schnittstellen-Metadatensatz gespeichert werden. Nach Auswahl des Quellkatalogs kann über den Knopf mit Lupe-Symbol der Metadatenkatalog nach diese Schnittstelle durchsucht werden. Dafür wird der Wert aus dem Feld "Titel", oder wenn befüllt die Metadaten-UUID verwendet. In einem sich neu öffnenden Fenster werden die Suchergebnisse angezeigt. Nach Auswahl eines Ergebnisses aus der Metadatensuche werden die Felder "Beschreibung", "Schlagworte", "Zugangsbeschränkungen" und "Nutzungsbedingungen" automatisch befüllt.

## Layer
Unter diesem Tab sind alle Layer, die dieser Schnittstelle zugeordnet sind aufgelistet und editierbar. Die selben Layer können auch im Bereich (Collections)[../collection/#Layer konfigurieren] bearbeitet werden.

## Konfiguration
Unter diesem Tab können die für diese Schnittstelle generierten Konfigurationsdateien betrachtet und editiert werden.

HINWEIS: Das manuelle Anpassen von einzelnen Konfigurationsdateien sollte nur im Ausnahmefall durchgeführt werden!

### Workspace neu laden/validieren
_Workspace neu laden_: Die deegree und ldproxy Instanz muss neu initialisiert werden, damit neue Schnittstellenkonfigurationen oder Änderungen sichtbar werden. Dafür muss der Workspace neu geladen werden. Diese Verfügbarkeit dieser Funktion kann in Softwareeinstellungen konfiguriert werden.

_Workspace validieren_: Die Konfigurationsdateien in einem Workspace können hiermit validiert werden. Als Ergebnis wird eine Auflistung von Fehlern, die in den Konfigurationsdateien bestehen, ausgegeben. Nur für deegree Schnittstellen verfügbar!

## Statistik

Bei aktiviertem Besucherstatistik-Modul (siehe **[modules](../../config/#modules)**) kann unter diesem Tab eine grafische Darstellung der Requestzahlen nach Monat für diese Schnittstelle eingesehen werden.

## Veröffentlichungsworkflow
Befindet sich der Datensatz im Bearbeitungsmodus, kann über den Button "Konfigurationsdateien generieren" die Schnittstellenkonfigurationen erzeugt werden. Es werden dabei immer alle Schnittstelle des Datensatzes auf einmal behandelt! Es können nicht einzelne Schnittstellen konfiguriert und veröffentlicht werden. Zur Zeit implementiert ist das für die Software deegree, GeoServer, ldproxy sowie MapServer. Voraussetzung hierfür ist die vollständige Konfiguration der Collections, inklusive Attributtabelle und, für WMS, eine Style-Konfiguration. Die MapServer Templates sind zur Zeit nur für die Erzeugung von Raster WMS vorbereitet. In diesem Fall ist das Ausfüllen der Attributtabelle sowie die Stylekonfiguration keine Voraussetzung für die WMS Config-Erzeugung.

Die zusammen gestellten Workspaces werden im konfigurierten Ordner unter /workspaces getrennt nach Umgebung abgelegt.
