# Changelog

Bei Anpassungen an einer Entität (Datensatz, Collection, Layer, Schnittstelle, Jira, ETL Prozess, Kommentar) wird die Information welches Objekt angepasst wurde, wann die Änderung statt fand und wer die Änderung durchgeführt hat in einem Datensatz-spezifischem Changelog eingetragen. Dies geschieht immer automatisch mit Betätigung des Speichern-Buttons auf den entsprechenden Formularseiten. Es werden keine Details (welche Werte sich wie geändert haben) zu dem jeweiligen Speichervorgang abgelegt.

Das Changelog ist über den entsprechenden Tab nach Auswahl eines Datensatzes erreichbar.