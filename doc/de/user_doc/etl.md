# ETL Prozesse erfassen

Für jeden Datensatz können unter dem Tab _ETL Prozesse_ Referenzen auf z.B. FME Workspaces hinterlegt werden, die für die Aktualisierung dieses Datensatzes relevant sind.
Der Standardfall wäre, dass es pro Datensatz einen ETL Prozess gibt, es kann aber auch mehrere Prozesse geben, die einzelne Tabellen (Collections) Aktualisieren. Für diesen Fall können pro ETL Prozess auch Verknüpfungen auf Collections dieses Datensatzes eingetragen werden.

* Typ: Auswahl zwischen _FME Workspace_, _FME Automation_, _Container Service_ und _Sonstiges_.
* Host: Eingabe, bzw. Auswahl (für FME) des Hosts, auf dem der Prozess läuft.
* Ordner: Eingabemöglichkeit für eine Ablagestruktur. Nur für _Container Service_ und _Sonstiges_.
* Repository: Referenz auf ein Repository. Bei _FME Workspaces_ ist hier ein FME Server Repository gemeint und kann über das Drop-Down ausgewählt werden. Die Liste der Repositories wird automatisch aus der FME Server Datenbank geladen. Voraussetzung ist die entsprechende Konfiguration in der config.js ( siehe **[Datenbank](../../config/#database)**)).
* Service Definition: Relevant für _Container Service_ und _Sonstiges_. Name eines HELM Charts oder dc.yaml.
* Dokumentation Datenquelle: Relevant für _Container Service_ und _Sonstiges_. Link zu einer API-Dokumentation
* Name: Bezeichnung des ETL Prozesses
* Intervall: Eingabemöglichkeit in welchem Rhythmus der Prozess ausgeführt wird.
* Letzte Ausführung: Datum der letzten Ausführung.
* Beschreibung: Freitextfeld.
* Collection Verknüpfen: Über das Drop-Down kann eine Collection des Datensatzes, oder über _Alle verknüpfen_ direkt sämtliche Collections mit diesem ETL Prozess verknüpft werden. Die verknüpften Collections werden in der darunter liegenden Tabelle angezeigt.