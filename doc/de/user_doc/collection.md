# Collections erfassen

Ein Datensatz kann in beliebig viele (1-n) _Collections_ untergliedert werden. Aus diesen Sub-Datensätzen werden die schnittstellenspezifischen Layer (WMS Layer, WFS FeatureType, ...) abgeleitet.

Über den (+) Button unterhalb der Collection-Liste kann ein neues Collection-Objekt angelegt werden.

Im _Collection-Details_ Bereich können nun die Informationen in die entsprechenden Felder eingetragen werden. Folgende Attribute müssen befüllt sein:

<a style="color:red; font-size:1em">*</a> Pflichtfeld <a style="color:grey; font-size:1em">\*</a> Standardwert

- **Name (technisch)**<a style="color:red; font-size:1em">*</a>: technischer Name, der Layername, und ggf. der Tabellenname werden hiervon abgeleitet. Darf keine Leer- oder Sonderzeichen (außer _ ) enthalten, nicht mit einer Ziffer oder _ beginnen, nicht länger als 60 Zeichen sein und keine Umlaute oder ß enthalten.
- **Name (alternativ/extern)**: Bei externen Datensätze oder für Collections mit speziellen Anforderungen (z.B. INSPIRE) kann die Namenskonventionen des technischen Namens hiermit übersteuert werden. Wird dann für den Layernamen verwendet. Der Tabellenname bleibt davon unberührt.
- **Titel**<a style="color:red; font-size:1em">*</a>: Titel der Collection.
- **API-Zugriff**<a style="color:red; font-size:1em">*</a>: Steuert, über welche Schnittstellen eine Collection bereit gestellt werden soll. Zur Auswahl stehen:
    - Visualisierung & Download
    - Nur Visualisierung (_WMS-Schnittstelle_)
    - Nur Download (_WFS- und OAF-Schnittstelle_)


Nun muss zum Abschluss der _Speichern_ Button betätigt werden.

Das ID Feld wird nach dem Speichern automatisch befüllt und kann manuell nicht editiert werden.

Folgende Standardwerte werden gesetzt, wenn die entsprechenden Felder nicht ausgefüllt sind:

- **Transparenz**<a style="color:grey; font-size:1em">*</a>: 0
- **Min-Scale**<a style="color:grey; font-size:1em">*</a>: 0
- **Max-Scale**<a style="color:grey; font-size:1em">*</a>: 2500000
- **GFI Thema**<a style="color:grey; font-size:1em">*</a>: default
- **Namespace**<a style="color:grey; font-size:1em">*</a>: Wert aus config.js

Folgende weitere Informationen können gesetzt werden:

**Gruppenobjekt**:
Zum Aufbau verschachtelter Themenbäume in den Capabilities (ausschließlich WMS), kann eine Collection über die Checkbox _Gruppenobjekt_ als Gruppenobjekt definiert werden. Gruppenobjekte haben keine eigene Datenquelle und auch keine eigene Darstellungsvorschrift. Es können aber Attribute für die Anzeige in Masterportalen konfiguriert werden. **ACHTUNG**: Wird eine Collection nachträglich als Gruppenobjekt gekennzeichnet, müssen verknüpfte WMS Layer manuell als Gruppenlayer markiert werden!

**Beschreibung**: Fließtext zur weiteren inhaltlichen Beschreibung der Collection. Darstellung über OAF Schnittstelle.

**Legende (Datei)**:
Über den Upload-Button können Legendengrafiken hochgeladen werden. Diese Legenden werden im Konfiguratormodul bei der Erzeugung von WMS Schnittstellen berücksichtigt.

**Legenden-URL Internet**:
URL auf eine externe Legende, die im Internet abgelegt ist

**Legenden-URL Intranet**:
URL auf eine externe Legende, die im Intranet abgelegt ist

**Transparenz (%)**:
Der Layer wird mit der gesetzten Prozentzahl in den Masterportalen transparent dargestellt.

**Min-Scale**:
Minimaler Maßstabsbereich, in dem die Karte sichtbar ist

**Max-Scale**:
Maximaler Maßstabsbereich, in dem die Karte sichtbar ist

**BBox (benutzerdefiniert)**:
Gibt die collectionspezifische räumliche Ausdehnung im Format _xmin, ymin, xmax, ymax_ an. Kann über einen Button aus den Daten generiert werden. Deegree: Wenn gesetzt, wird diese Bounding Box als Envelope in der deegree Konfiguration des Layers verwendet. Geoserver: Muss gesetzt/generiert werden, da diese in der Geoserver Konfiguration verwendet werden.

**Namespace**:
Bezeichnung und URI eines Namensraums. Muss im Format _Bezeichnung=URI_ angegeben werden.

**Komplexes Datenmodell**:
Auswahl zwischen ja/nein, ob den Daten der Collection ein komplexes Datenmodell zugrunde liegt.

**Schema**<a style="color:grey; font-size:1em">*</a>:
Datenbankschema, das die Tabellen mit den Daten für diese Collection enthält. Wenn eine Datenbankverbindung am Datensatz verknüpft wurde, wird die Liste aller verfügbaren Schemas abgerufen und zur Auswahl angeboten. Wird ein noch nicht vergebener Name eingegeben, kann diese Schema über _+_ in der Datenbank hinzugefügt werden. Vorausgesetzt, dass am Datensatz auch eine schreibende Datenbankverbindung verknüpft wurde.

**Tabelle**<a style="color:grey; font-size:1em">*</a>:
Datenbanktabelle oder View, die die Daten für diese Collection enthält. Wenn eine Datenbankverbindung am Datensatz verknüpft wurde und ein Schema ausgewählt ist, wird die Liste aller verfügbaren Tabellen abgerufen und zur Auswahl angeboten. Wird ein noch nicht vergebener Name eingegeben, kann diese Tabelle über _+_ in der Datenbank hinzugefügt werden. Voraussetzung ist die Konfiguration der Attribute, sowie dass am Datensatz auch eine schreibende Datenbankverbindung verknüpft wurde.

**Anzahl Features**: Anzahl der Objekte, die zu dieser Collection gehören.

**Quelldaten**
In dieser Sektion können Informationen für die Anbindung eines Datensatzes aus einem Fachverfahren für die Datenintegration hinterlegt werden.

_Quelldatentyp_: Aus dem Drop-Down Menü kann der Typ der Quelldaten gewählt werden oder alternativ frei eingegeben werden.

_DB-Verbindung_: Wurde als Quelldatentyp eine Datenbank ausgewählt, kann hier eine Quelldatenbankverbindung verknüpft werden.

_Ablageort_: Pfad, z.B. Netzwerklaufwerk, in dem dateibasierte Quelldaten abgelegt sind.

_Dateiname_: Name der dateibasierten Quelldaten.

_Schema_: Angabe eines Datenbankschemas aus der Quelldatenbank

_Tabelle_: Angabe einer Datenbanktabelle oder eines Tabellenblattes einer Exceltabelle.

_Filter_: Hier können Filter für die Abfrage dieser Quelldaten hinterlegt werden (z.B. "WHERE kategorie = 'haus'")

**zusätzliche Informationen**: Zusätzliche Informationen, die als Fließtext an die Collection angehängt werden können. Darstellung nur im UDP Manager.

**Spezialeinstellungen**

_Alternativer Titel_:

Es kann ein alternativer Titel vergeben werden, wenn der aus den Capabilities übersteuert werden soll. Der Titel des Layers im Themenbaum des Masterportals wird damit überschrieben.
Dieser alternative Titel wird in die Layer-JSON eingetragen. Er hat keine Auswirkungen auf die Konfigurationsdateien der Dienste.

_Layer-Attribution_:

Es kann ein Text eingegeben werden, der im Masterportal rechts unten eingeblendet wird, wenn der Layer sichtbar ist.

_Zusätzliche Kategorien_:

Hier können zusätzliche Kategorien eingetragen werden, die nicht in den Datensatz-Metadaten vorhanden sind.

_Service URL sichtbar_<a style="color:grey; font-size:1em">*</a>:
Wenn gewählt, wird über den Info-Button im Master-Portal Themenbaum die URL des Dienstes angezeigt.


**GFI-Konfiguration**

_GFI Theme_<a style="color:grey; font-size:1em">*</a>

Soll das GFI im Portal in einer anderen Form angezeigt werden, kann hier auf das Template verwiesen werden

_Theme Parameter (JSON)_

Hier kann ein JSON Objekt eingefügt werden, das zur Konfiguration des gewählten GFI Themes benutzt wird.

_GFI als Fenster öffnen_

Die GFI Informationen werden nicht im Master Portal dargestellt, sondern als neues Browser Fenster geöffnet.

_GFI deaktivieren_

Ist dieser Haken gesetzt, wird im Portal für diesen Layer kein GFI abgefragt.

## Attribute konfigurieren
_Allgemeiner Hinweis:
Die Attributtabelle wird für mehrere Aufgaben herangezogen:_

* _Automatischen Anlegen von Datenbank-Tabellen_
* _Automatische API Konfiguration_
* _GFI Konfiguration - Attributmapping von Schnittstelle in Masterportal_
* _Schemaprüfung bei der automatischen Datenaktualisierung_

Der Attribut-Editor ist in einen Tabellenbereich und einen Formularbereich aufgeteilt. Über das Mittelmenü kann zwischen der Ansicht des Formularbereiches und eines [Änderungslogs](#anderungslog) gewechselt werden.

In der Tabelle werden die wichtigsten Attribute abgebildet. Im unteren Teil befinden sich die Bedienelemente:
<img src="../../../images/button_add.png" width="20" height="20" /> Eine neue Zeile der Tabelle hinzufügen. Eine Begrenzung der Anzahl von Zeilen gibt es nicht.
<img src="../../../images/button_delete.png" width="20" height="20" /> Ausgewählte Zeile löschen.
<img src="../../../images/button_csv.png" width="20" height="20" /> Gesamte Tabelle im CSV-Format abspeichern.
<img src="../../../images/button_more.png" width="20" height="20" /> Weitere Funktionen können aufgerufen werden. Es besteht die Möglichkeit Attribute über eine schon vorhandene Schnittstelle oder Datenbanktabelle einzutragen. Nur die Spalten _Name (DB)_, _Name (API)_ und _Label_ werden dabei gefüllt. Dabei wird die zuvor vorhandene Attributtabelle komplett überschrieben. Relevant für externe Dienste.
Ein Umsortieren der Attribute ist über einfaches Drag & Drop in der Tabelle möglich.

Im Formular können die einzelnen Attribute bearbeitet werden. Folgende Attribute gibt es:

_Name (Quelle)_: Attributname aus einem Quellsystem.

_Name (DB)_: Name der Attributspalte in der Datenbank des Zielsystems.

_Name (API)_: Attributname für die Schnittstellenkonfiguration.

_Label_: Anzeigename in einem Masterportal / im GetFeatureInfo Request. Attributtitel im OGC API Feature.

Mit den beiden Spalten _Name (API)_ und _Label_ wird die GFI Konfiguration gesetzt. Das Mapping dieser beiden Spalten wird automatisch in die Layer-JSON überführt.

_Datentyp_: Auswahl des Datentyps für das Attribut. Wird für die Erstellung der Datenbank und die Schnittstellenkonfiguration genutzt. Die Liste der Datentypen ist vereinfacht und wird für die beschriebenen Automatismen umgeschrieben.
Eine Datentyp muss für ein Attribut immer gesetzt werden.

_Primärschlüssel (PK)_: ja/nein - Attribut ist Primärschlüssel. Ein Attribut muss immer als Primärschlüssel markiert sein.

_Primäre Geometrie_: ja/nein - Geometrieattribut als primäre Geometrie. Muss auch gesetzt werden, wenn nur ein Geometrieattribut vorhanden ist.

_Primärer Zeitindex_: ja/nein - Attribut als primärer Zeitindex. Muss auch gesetzt werden, wenn nur ein Zeitattribut vorhanden ist.


**Regeln zur Datenintegration**
Die Felder, die hier zusammengefasst sind, beziehen sich auf das Anlegen neuer Tabellen bei neuen Daten. Außerdem werden sie bei der automatischen Datenaktualisierung einbezogen.

- _Pflicht_: ja/nein - beschreibt, ob ein Attributwert vorhanden sein muss, oder nicht.

- _Filter für Visualisierung_: Werte, die für das Attribut eingetragen werden dürfen.

- _Filter für Daten_: mehrere Werte sind jeweils mit | zu trennen.

- _Attributreihenfolge_: Sollten mehrere Attribute aus der Quelle zu einem Attribut im Zielsystem zusammengefasst werden, kann hier die Reihenfolge der Attribute abgebildet werden.
Für die automatischen Datenaktualisierung ist eine Vorauswahl der gängigen Attribute, die zusammengefasst werden, hinterlegt.

- _Datum_: Auswahl für das Format eines Datumattributes.

- _Georeferenzierung_: Attribute, die zur Georeferenzierung genutzt werden sollen. Vorauswahl mit gängigen Attributen hinterlegt.

_Einheit_: Einheit des Attributes.

_Beschreibung_: Nähere Beschreibung des Attributs, z. B. bei unverständlichen Attributnamen.

Über den Button _Speichern_ wird die Attributtabelle abgespeichert. Soll die Attributtabelle für alle Collections übernommen werden, kann vor dem Speichern ein Häkchen bei _Konfiguration für alle Collections übernehmen_ gesetzt werden.


**Spezifikationen zum Ausfüllen des Formulars**
_Attribute ignorieren_: Attribute aus einem Quellsystem, die nicht ins Zielsystem übernommen werden sollen, können über eine Checkbox im Formularbereich als _**ignore**_ markiert werden. Die Spalten _Name (DB)_, _Name (API)_ und _Label_ können einzeln markiert werden. Wobei die Auswahl für die Spalte _Name (DB)_ sich auf die beiden anderen Checkboxes fortpflanzt.
Ist die Checkbox bei _Label_ gesetzt, wird dieses Attribut für eine GetFeatureInfo-Abfrage (GFI) ignoriert.

_Zusammengesetzte Attribute_: Das Zusammenführen von Attributen aus einem Quellsystem in ein Zielsystem wird wie folgt in der Tabelle festgehalten:
Für die Quellattribute, die ignoriert werden sollen, wird die Spalte _Name (DB)_ mit _**ignore**_ markiert. Anschließend wird eine neue Zeile angelegt, in der die Spalte _Name (Quelle)_ leer bleibt. Die nachfolgenden Namensspalten werden mit einem beliebigen Namen für das neue zusammengesetzte Attribut befüllt. In der Spalte _Attributreihenfolge_ kann nun für die Datenintegration festgehalten werden, auf welche Quellattribute sich das neu zusammengesetzte Attribut bezieht und in welcher Reihenfolge es zusammengesetzt werden soll.
Um das Ganze für die automatische Datenaktualisierung kenntlich zu machen, ist es dazu noch notwendig die Spalte _Attributreihenfolge_ auch für die Quellattribute, die zusammengesetzt werden sollen, zu befüllen. Hierzu ist die Spalte mit einem Dropdown ausgestattet. Dieses beinhaltet bisher folgende Auswahlmöglichkeiten:

- Adresse_Str
- Adresse_Hausnr
- Adresse_Zusatz
- Ort_PLZ
- Ort

## Style bearbeiten

Hierzu gibt es zwei Optionen: über _Style konfigurieren_ öffnet sich ein Style-Editor, über den ein Style generisch definiert werden kann. Dieser Style wird beim Generieren der Schnittstellenkonfigurationen in z.B. ein SLD XML übersetzt. Über _Style SLD bearbeiten_ kann in einem Dateieditor direkt ein SLD XML, bzw. ein deegree FeatureTypeStyle eingefügt werden. Dieser Inhalt wird beim Generieren der Schnittstellenkonfiguration 1 zu 1 übernommen.

![Styling Workflow](../../images/collection_styling_workflow.png)

In beiden Fällen werden Style-Vorschriften und externe Grafiken zunächst in der UDP-Manager Datenbank gespeichert. Der Haken bei _Manuelle SLD Stylekonfiguration verwenden_ entscheidet, welche Variante zum Einsatz kommt.

Theoretisch erlaubt der UDP-Manager eine dritte Option:
Konfigurationsdateien nach Erzeugung manuell bearbeiten.

Das soll jedoch lediglich eine "Notlösung" - für Schnittstellen-Einstellungen sein,
die zu speziell sind, um eine Implementierung im UDP-Manager zu rechtfertigen.
**Bei SLDs entfällt diese Notlösung grundsätzlich!**
Die Möglichkeit, SLDs und externe Grafiken direkt in der Datenbank zu bearbeiten, bietet dieselben Freiheiten. Sie vermeidet jedoch die Folgeprobleme manueller Nachbearbeitung! Zur Veranschaulichung:

![Hinweis zur manuellen Bearbeitung von Styles](../../images/do_not_edit_style_in_repo.png)

### Details zu _Style SLD bearbeiten_

Im linken Bereich des Fensters können für den Style notwendige Icons oder Fonts (als ttf) über den Button _Upload_ hochgeladen werden. Zur Referenzierung hochgeladener Dateien in der SLD gilt folgender Pfad: `./symbole/[Dateiname.Endung]`. Nach Klick auf _Speichern_ wird der Style und die hochgeladenen Icons in der UDP-Manager Datenbank abgelegt.

Wird eine funktionierende SLD Vorlage für die weitere manuelle Bearbeitung des Styles benötigt, kann wie folgt vorgegangen werden:

* Zunächst via _Style konfigurieren_ einen "Dummy-Style" erstellen. So nahe am gewünschten Resultat wie möglich.
* Nun die Konfiguration zum WMS generieren.
* Für jede Collection, die eine manuelle SLD erfordert:
    * SLD via Schnittstellen Tab unter _Konfiguration_ öffnen.
    * Inhalt kopieren - nicht editieren! (siehe Einleitung zum Style Kapitel!)
    * Zur Collection navigieren, _Style SLD bearbeiten_ klicken und Inhalt einfügen.
    * Hier editieren und bei Bedarf externe Grafiken hochladen.
    * Haken nicht vergessen: _Manuelle SLD Stylekonfiguration verwenden_

### Details zu _Style konfigurieren_ (Style-Editor)
_Hinweis: Voraussetzung für die Aktivierung des Style-Editors ist, dass in der Attributtabelle mindestens ein Geometrie-Attribut konfiguriert wurde._

Nach öffnen des Style-Editors muss zunächst das Geometrieattribut gewählt werden, auf das sich die neue Regel beziehen soll. Es kann nun eine neue Regel für diesen Geometrietyp angelegt werden (_Regel hinzufügen_) oder eine Beschriftung (TextSymbolizer) hinzugefügt werden (_Beschriftung hinzufügen_).

Anschließend kann die kartographische Darstellung im rechten Formularfeld definiert werden. Um die gewünschten Style-Regeln für die gewählte Collection abzuspeichern muss rechts unten auf _speichern_ geklickt werden.

**Punkt**
Nach Angabe eines Titels muss entweder ein Standardsymbol (_Kreis, Dreieck, Quadrat, Stern_) oder _Externes Icon_ aus dem Drop-Down gewählt werden.

Für alle Symbolarten können die Parameter _Größe_ (default 10px), _Rotation_ (default 0°), _MinScaleDenominator_ (optional) und _MaxScaleDenominator_ (optional) definiert werden.
Für Standardsymbole kann zusätzlich die Umringfarbe (Auswahlfenster nach Klick auf Farbbalken) mit Deckkraft (1 volle Deckkraft, 0 Transparent) sowie die Breite des Umrings (default 3px) gewählt werden.
Alternativ zur Farbauswahl, kann über _Farbwert aus Attribut_ auch ein Attribut gewählt werden das den Farbwert enthält. Gleiches gilt für die Definition der Füllfarbe.

Bei externen Icons kann über _Browse Icon_ eine Datei im lokalen Dateisystem ausgewählt und hochgeladen werden.

**Linie**
Nach Angabe eines Titels kann die Strichfarbe (Auswahlfenster nach Klick auf Farbbalken) ausgewählt werden. Alternativ zur Farbauswahl, kann über _Farbwert aus Attribut_ auch ein Attribut gewählt werden das den Farbwert enthält.
Des Weiteren kann die _Strichbreite_ (in px), die _Deckkraft_ (1 volle Deckkraft, 0 Transparent), _MinScaleDenominator_ (optional) und _MaxScaleDenominator_ (optional) definiert werden.

Unter _Stil_ kann eine durchgezogene Linie oder eine gestrichelte Linie gewählt werden. Bei Auswahl von _gestrichelte Linie_ kann im Feld _Muster Strichbreite_ die Breite der Einzelstriche (in px) angegeben werden.

**Polygon**
Nach Angabe eines Titels muss zunächst die Art der Flächenfüllung über das Drop-Down _Füllung_ angegeben werden.

Für sämtliche Füllarten können folgende Angaben bearbeitet werden: die Umringfarbe (Auswahlfenster nach Klick auf Farbbalken) mit Deckkraft (1 volle Deckkraft, 0 Transparent) sowie die Breite des Umrings (default 3px) gewählt werden. Alternativ zur Farbauswahl, kann über _Farbwert aus Attribut_ auch ein Attribut gewählt werden das den Farbwert enthält. Außerdem _MinScaleDenominator_ (optional) und _MaxScaleDenominator_ (optional).

Bei einfarbiger Füllung kann wie oben beschrieben eine Farbe gewählt werden. Für gemusterte Füllung kann außerdem die Art der Füllung (_Stil_, _Muster Strichbreite_ sowie _Musterdichte_) definiert werden.

Bei externen Icons kann über _Browse Icon_ eine Datei im lokalen Dateisystem ausgewählt und hochgeladen werden.

**Text**
Nach Angabe eines Titels muss zunächst das Attribut gewählt werden, das den Text für die Beschriftung enthält.
Die genaue stilistische Ausprägung der Schrift wird über die Felder _Font_, _Fontstil_, _Fontdicke_, _Schriftgröße_, _Schriftfarbe_ sowie _Deckkraft_ festgelegt.

Im Bereich _Halo definieren_ kann ein Textschatten über _Radius_, _Farbe_ und _Deckkraft_ konfiguriert werden

Über das Feld _Ausrichtung an_ kann gewählt werden, ob das Label sich an einem Punkt oder an einer Linie ausrichten soll.

Bei Auswahl von _Punkt_ werden Beschriftungen an einzelnen Punkten platziert. Bei Linien liegt dieser Punkt in der Mitte des sichtbaren Teils der Linie. Bei Polygonen ist der Punkt der Schwerpunkt des sichtbaren Teils des Polygons. Die Position der Beschriftung relativ zum Beschriftungspunkt kann durch die folgenden Felder gesteuert werden:

_Ankerpunkt_: Platzierung der Beschriftung relativ zum Beschriftungspunkt.
_Versatz_ (in px): Versetzt die Beschriftung vom Ankerpunkt.
_Rotation_ (in °): Rotiert die Beschriftung im Uhrzeigersinn.

Bei der Auswahl von _Linie_ werden Beschriftungen entlang von Linien gesetzt. Die Art der Platzierung kann über folgende Attribute gesteuert werden:

_Wiederholend_: Soll die Beschriftung wiederholend angezeigt werden.
_Anfangsabstand_ (in px): Abstand, ab dem die ersten Beschriftung erscheinen soll.
_Ausgerichtet_: Beschriftung folgt der Liniengeometrie.

Der Maßstabsbereich, in dem die Beschriftung angezeigt werden soll kann über die Felder _MinScaleDenominator_ (optional) und _MaxScaleDenominator_ (optional) gesteuert werden.

**Filter**
Für jede Regel kann ein Filter definiert werden, über den bestimmt werden kann, wann die jeweilige Regel gelten soll. Es können gängige Bedingungen über das Drop-Down _Bedingung_ gewählt werden. Im Drop-Down _Attribut_ muss das Attribut gewählt werden, über das gefiltert werden soll. im Feld _Literal_ (min,max) wird der Filterwert angegeben.

Konfigurierte Regeln können in der Liste per Drag&Drop umsortiert werden.

## Collection importieren
Es gibt die Möglichkeit eine oder mehrere Collections aus einer vorhandenen Schnittstelle (WMS, WMTS oder WFS) zu importieren. Die zugrunde liegende Funktionalität ist das Abrufen der Capabilities der Schnittstelle und daraus das Extrahieren der nötigen Informationen. Wird eine Collection importiert, wird dabei gleichzeitig ein Layerelement hinzugefügt.
In der Konfigurationsdatei (config.js) kann über An- und Ausschalten des Moduls "externalCollectionImport" die Art des Imports gesteuert werden. Es folgt eine kurze Übersicht zu den zwei Varianten:

Allgemeiner Import: _externalCollectionImport = false_

* Der Import einer Collection aus einer Schnittstelle ist für jeden Datensatz möglich.
* Die Auswahl der Schnittstelle erfolgt automatisch, je nachdem welche vorhanden ist. Sind WMS und WFS vorhanden, wird eine Kombination der beiden Informationen genutzt.
* Titel und Name der Collection werden mit den zurückgegebenen Informationen befüllt. Dabei wird die Namenskonvention des Namens nicht berücksichtigt und muss ggf. manuell angepasst werden.
* Der Alternative Name der Collection wird nicht befüllt.

Import für externe Datensätze: _externalCollectionImport = true_

* Der Import einer Collection aus einer Schnittstelle ist nur für als "extern" markierte Datensätze möglich.
* Die Auswahl einer Schnittstelle findet über ein Dropdown statt.
* Der Collection Titel bekommt einen Präfix, je nach ausgewähltem Schnittstellentyp - wird in der Layer JSON wieder entfernt.
* Der Alternative Name der Collection wird mit dem Namen aus der Schnittstelle gefüllt - wird dann in der Layer JSON genutzt.
* Der Technische Name der Collection lautet _extern\_\[Collection ID]_.
* Der API Zugriff wird gesetzt, je nachdem über welchen Schnittstellentyp die Collection importiert wurde. Nach dem Import ist der API Zugriff nicht mehr editierbar.
* Der Namespace einer importierten externen Collection wird nicht gesetzt.

## Layer konfigurieren
Unter "Collections" -> "Layer" können über den _+_ Button unterhalb der Tabelle für jede am Datensatz vorhandene Schnittstelle für die gewählte Collection je ein Layerobjekt erzeugt werden. In der Layer-Tabelle werden die Titel der zugehörigen Schnittstelle angezeigt. Unter _Layer-Details_ können schnittstellenspezifische Layereigenschaften konfiguriert werden.

**Layer-ID**:
Die eindeutige ID des Layers, wird in der Datenbank automatisch generiert.

**Name**:
Der Layer-Name wird vom Collection-Namen abgeleitet und kann nicht editiert werden.

**Schnittstelle**:
Anzeige der mit diesem Layer verbundenen Schnittstelle.

**WMS Einstellungen**

_Gruppenlayer_<a style="color:grey; font-size:1em">*</a>: Angabe, ob der Layer als Gruppenlayer konfiguriert werden soll.

_Gutter (Px)_<a style="color:grey; font-size:1em">*</a>:
Ganze Zahl, mit der die Darstellung von an Kachelgrenzen liegenden Symbolen verbessert werden kann.

_Kachelgröße (Px)_<a style="color:grey; font-size:1em">*</a>:
Größe, in der die WMS Bildkacheln abgerufen werden.

_Singletile_<a style="color:grey; font-size:1em">*</a>:
Wenn angehakt, wird der WMS nicht gekachelt abgerufen, sondern in einem Bild.

_Transparent_<a style="color:grey; font-size:1em">*</a>:
Wenn angehakt, wird der Bildhintergrund transparent dargestellt, Voraussetzung ist das Bildformat PNG.

_Ausgabeformat_<a style="color:grey; font-size:1em">*</a>:
Bildformat des getMap Response. Standard ist image/png.

_GFI Format_:
Definition, welches Ausgabeformat bei einem GFI Request zurück gegeben werden soll. Hier kann jedes durch den WMS unterstütze Format eingegeben werden.

_Feature Count_<a style="color:grey; font-size:1em">*</a>:
Anzahl der Objekte, die durch einen GFI Request zurück gegeben werden sollen.

_Kein 3D Support_<a style="color:grey; font-size:1em">*</a>:
Anhaken, wenn Layer nicht in 3D Anwendungen benutzbar ist.

_Zeitreihe (WMS-Time)_<a style="color:grey; font-size:1em">*</a>:
Angabe, ob es sich um einen Layer mit Zeitreihe handelt.

**WFS Einstellungen**

_Ausgabeformat_<a style="color:grey; font-size:1em">*</a>:
Es kann zwischen XML und GeoJSON gewählt werden. Standard ist _XML_.

**STA Einstellungen**

_URL Param. – Root_<a style="color:grey; font-size:1em">*</a>: Auswahl zwischen Datastreams und Things.

_URL Param. – Filter_: Definition des Filters zur Abfrage der STA.

_URL Param. – Expand_: Definition der Ausdehnung und Sortierung.

_EPSG_<a style="color:grey; font-size:1em">*</a>: Default ist EPSG:4326.

_Zu ersetzende LayerIDs_: In Master Portalen mit automatisch generiertem Themenbaum werden die hier referenzierten Layer Objekte nicht angezeigt.

_Style ID_: Referenz auf eine Style ID, für eine Darstellungsvorschrift aus der Master Portal style.json.

_ClusterDistance (Px)_: Objekte mit einer Entfernung von weniger als dem hier definierten Wert werden im Master Portal geclustert angezeigt. Default ist 50.

_Nur Objekte in aktuellem Ausschnitt laden_<a style="color:grey; font-size:1em">*</a>: Wenn gewählt, werden aus der STA nur Objekte der aktuell gewählten räumlichen Ausdehnung geladen. Default ist true.

**3D Einstellungen**

_requestVertexNormals:_ für Typ Terrain3D

_maxScreenSpaceError_: für Typ TileSet3D

**Oblique Einstellungen**

_hideLevels_: verbergen der gewählten Zoomstufen.

_minZoom_: niedrigste Zoomstufe.

_resolution_: Definition der Bodenauflösung in Pixel.

_projection_: Projektion des Quell-Datensatzes.

_terrainUrl_: Url auf den Gelände Datensatz.

**VectorTiles Einstellungen**

_extent:_ Erforderlich, um das GridSet des VTCs zu definieren. Wird es nicht festgelegt, wird die Ausdehnung des Koordinatenreferenzsystems des Portals verwendet.

_origin:_ Erforderlich, um das GridSet des VTCs zu definieren. Wird es nicht festgelegt, wird die linke obere Ecke des Koordinatenreferenzsystems des Portals verwendet.

_resolutions:_ Erforderlich, um das GridSet des VTCs zu definieren. Wird es nicht festgelegt, werden die Auflösungen des Portals verwendet. Fehlende Zoomstufen werden nur extrapoliert, wenn die Auflösungen explizit angegeben sind. Daher dürfen nur Auflösungen angegeben werden, für die Kacheln existieren.

_vtStyles:_ Beschreibt die verfügbaren Styles, die mit dem styleVT-Tool verwendet werden können.

_visibility:_ Gibt an, ob der Layer zu Beginn aktiviert ist. Default ist false.

**OAF Einstellungen**
_Limit:_ Integer, der die Anzahl der maximal zurück gegebenen Objekte pro Request definiert.

### Testrequests
Für den ausgewählten Layer können eine Reihe von Testrequests über das Drop-Down am oberen linken Rand des _Layer-Details_ Formular ausgeführt werden. Die Request-URL wird vor dem Ausführen in einem Fenster angezeigt und kann dort kopiert oder editiert werden. Die auswählbaren Kartenausschnitte können in der config.js definiert werden (siehe **[Test BBOX](../../configuration/app/#test-bboxes)**).

### Layer löschen / Löschvermerk
Ein Layer mit mindestens einer Portalkopplung (siehe folgendes Kapitel) kann nicht direkt gelöscht werden. Zunächst müssen alle Kopplungen gelöst werden. Zur Kommunikation dieser Aufgabe kann über _Löschvermerk_ ein entsprechender Hinweis erstellt werden. Bei Konfigurierter Mailadresse für _delete\_layer\_request\_to_ in der config.js (siehe **[Mail](../../configuration/app/#mail)**) wird dafür automatisch an die hinterlegte Mailadresse eine Mail verschickt.

Wenn keine gekoppelten Portale mehr vorhanden sind wird der _Löschen_ Button aktiviert.

### Portale
In der Tabelle _gekoppelte Portale_ werden sämtliche Portale aufgelistet, in denen der gewählte Layer enthalten ist. Die Kopplungen können mit Hilfe der geplanten Aufgabe [LayerIDs aus Masterportalinstanzen auslesen](../management/#layerids-aus-masterportalinstanzen-auslesen-portal_scraper) automatisch aus Masterportalinstanzen ausgelesen werden. Manuell verknüpfte Portale können aus der Tabelle direkt gelöscht werden.

### JSON
Im Reiter JSON wird das Masterportal Layer-JSON Objekt für den gewählten Layer angezeigt. Dieses Objekt wird automatisch bei jedem Speichervorgang aktualisiert.

Über die UDP-Manager API können diese JSON Objekte abgerufen und direkt in Masterportal-Instanzen eingebunden werden. (siehe **[Layer JSON Objekte holen](../../api/getlayerjson)**)

Über die Buttons _JSON neu erzeugen_ bzw. _JSON neu erzeugen (ganze Collection)_ kann für diesen Layer bzw. alle Layer der Collection das JSON Objekt neu generiert werden.

## Änderungslog

Auf Collectionebene und für die Attributkonfiguration wird ein Änderungslog bereit gestellt. Dabei wird der aktuelle Stand mit einem alten abgespeicherten Stand verglichen und die Differenzen ausgegeben. Grundlage des alten Standes ist das Versionslog, welches bei jeder Verschiebung der Schnittstellen in den Status _prod_ in der Datenbank abgespeichert wird.
Auf Collectionebene werden die Änderungen der einzelnen Parameter dargestellt und, ob die Attributkonfiguration und/oder der Style editiert wurden. Das Änderungslog der Attributkonfiguration gibt detailliert die Änderungen der Informationen für jedes einzelne Attribut einer Collection wieder. Die Form im UDP Manager sieht folgendermaßen aus:

<img src="../../../images/aenderungslog.png" width="80%" height="100%" />

Abgerufen werden können die Änderungslogs jeweiles über den dazugehörigen Reiter im Mittelmenü. Außerdem gibt das Icon <img src="../../../images/icon_change.png" width="20" height="20" /> in der jeweiligen Tabelle einen Hinweis darauf, dass diese Collection bzw. dieses Attribut geändert wurde.

Das Änderungslog wird über einen Hintergrundprozess immer dann automatisch generiert, sobald ein neuer Datensatz in der Liste der _Datensatz-Details_ ausgewählt wird. Um das Änderungslog für die Attributkonfiguration erneut generieren zu lassen, reicht es aus in der Liste unter _Collection-Details_ eine Collection auszuwählen.