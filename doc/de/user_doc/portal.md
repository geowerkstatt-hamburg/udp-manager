# Portale verwalten

Unter dem Menüpunkt _Portale_ der Hauptnavigation, werden alle Portalinstanzen (auf Masterportal Basis) aufgelistet, die Layer aus dem UDP-Manager enthalten. Diese Liste kann automatisch durch die geplante Aufgabe (**[LayerIDs aus Masterportalinstanzen auslesen (portal_scraper)](../management/#layerids-aus-masterportalinstanzen-auslesen-portal_scraper)**) aktualisiert werden. (HINWEIS: für diesen Prozess ist es notwendig, dass eine config.json der Masterportalinstanz beiliegt.)

In der Linken Tabelle werden sämtliche Portale untergliedert nach Host angezeigt. Nach Klick auf einen Eintrag werden in der rechten Tabelle alle Layer angezeigt, die in diesem Portal eingebunden sind. Über einen Doppelklick auf einen Eintrag in der Layertabelle gelangt man direkt auf die Layer-Details Seite des gewählten Layers.

Über den _+_ Button unterhalb der Portaltabelle kann eine Portalinstanz manuell hinzugefügt werden. Diese manuell angelegten Portale werden nicht durch den automatischen Prozess überschrieben. Über den Link Button unterhalb der Layertabelle können Layer mit diesem Portal verknüpft werden. Diese Funktion ist nur bei manuell angelegten Portalen aktiv.
