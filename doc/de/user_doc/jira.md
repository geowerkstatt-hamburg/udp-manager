# JIRA Tickets verknüpfen und erzeugen

Hier können dem jeweiligen Datensatz Jira Tickets zugeordnet und neue Tickets erstellt werden.

HINWEIS: Funktioniert nur mit JIRA Cloud und der API v1000.x.x

_Jira Ticket zuordnen_

Dafür kann im Suchfeld (links) nach einem bestimmten Jira Ticket gesucht werden. Es kann nach Ticket-Key, Autor, Bearbeiter oder Zusammenfassung gesucht werden. Mit einem Klick auf den Button „Jira Tickets Suchen" werden die gefilterten Jira Tickets in einem neuen Fenster angezeigt. Bleibt das Suchfeld leer, werden alle verfügbaren Tickets geladen. Mit einem Klick auf + wird das Ticket dem Dienst zugeordnet und erscheint in der Tabelle.

In der Konfiguration (config.js) kann mithilfe des Parameters „use\_in\_ticketsearch" festgelegt werden, welche Jira Projekte der Suche zu Grunde liegen. Es besteht die Möglichkeit mehrere Projekte zu durchsuchen.

_Jira Ticket erstellen_

Mithilfe der Jira API kann ein Ticket direkt aus dem UDP-Manager automatisiert in einem Jira Projekt angelegt werden. Dem Ticket liegt ein allgemeines Template – je nachdem, welche Ticketart ausgewählt wurde – zugrunde. Die Templates werden dazu benutzt, um den Inhalt des Beschreibungsbereiches des Jira-Tickets zu füllen. Für jede Ticketart muss ein eigenes Template erstellt werden, welches sich individuell anpassen lässt. Die Templates müssen im Textformat (.txt) vorliegen und in folgender Form abgespeichert werden:

- Neue\_Daten\_template
- Daten\_Aktualisierung\_template
- Loeschen\_template

Dies ist wichtig, damit das jeweilige Template der Ticketart zugewiesen werden kann. Die Templates müssen im Ordner „…\backend\services\templates" abgelegt werden. Hier befindet sich auch eine Vorlage für ein solches Template (_Vorlage\_JiraTicket\_Template.txt)_. In der Vorlage sind alle Angaben aufgelistet und kurz beschrieben, die automatisch über manuelle Eingaben oder aus dem UDP-Manager eingetragen werden. Darüber hinaus sind die Templates frei gestaltbar.

Im Folgenden werden die manuellen Eingaben und vorher definierten Informationen aus dem UDP-Manager beschrieben.

Der Button „Jira Ticket anlegen" auf der rechten Seite ermöglicht das Anlegen eines Jira Tickets. Bei Klick auf den Button öffnet sich ein neues Fenster, in dem einige Angaben gemacht werden müssen, um das Jira Ticket zu erstellen.

1. Zunächst muss angegeben werden, in welchem Jira Projekt ein neues Ticket angelegt werden soll. Welche Jira Projekte in der Auswahl enthalten sein sollen, kann in der Konfiguration (config.js - jira) konfiguriert werden. Mit dem Parameter _name\_alias_ kann ein Alias als Anzeigename vergeben werden. Außerdem kann über den Parameter _new\_as\_subtask_ bestimmt werden, ob das neue Ticket als Sub-Task oder als Task erstellt werden soll. Ist der Parameter als true eingestellt, erscheint nach Auswahl des jeweiligen Projektes eine neue Auswahl „Ticketverknüpfung" und das neue Jira Ticket wird als Sub-Task erstellt.

    - Bei „Ticketverknüpfung" wird das Ticket ausgewählt, an das das neue Ticket als Sub-Task angehängt werden soll. Zur Auswahl stehen alle Tickets, die über die Suche (siehe oben) der Tabelle hinzugefügt wurden. Sie werden gefiltert angezeigt, passend zum ausgewählten Jira-Projekt.

    - Unabhängig davon, ob ein Ticket als Sub-Task oder Task erstellt wird, muss im nächsten Schritt ein Label aus einer Drop-Down-Liste ausgewählt werden. Welche Label zur Verfügung stehen, kann in der Konfigurationsdatei (config.js - jira) bestimmt werden.

2. Danach muss die Ticketart ausgewählt werden. Damit wird festgelegt, welche Aufgabe hinter dem Ticket steht. Drei Möglichkeiten stehen zur Auswahl: _Neue Daten_, _Daten Aktualisierung_, _Löschen_. Zusammen mit der Datensatz ID bildet die ausgewählte Ticketart die Zusammenfassung des neuen Tickets.

3. Je nachdem welche Ticketart ausgewählt wird, können weitere Einstellungen vorgenommen werden:

    - Für alle drei Ticketarten (_Neue Daten_, _Daten Aktualisierung_, _Löschen_) erscheint eine Auswahl mit allen Schnittstellen, die für den Datensatz konfiguriert sind.Standardmäßig sind alle Checkboxen mit einem Haken versehen. Sollen nicht alle vorhandenen Schnittstellen erstellt, geändert oder gelöscht werden, können einzelne Checkboxen abgewählt werden.

    - Für die Ticketart _Neue Daten_ erscheint die Auswahl, ob ein FME Scheduler oder eine FME Automation aktiviert werden soll. Wird eins der beiden gewählt, wird der Beschreibung des Tickets automatisch der Prozessname und wo dieser zu finden ist aus dem Reiter ETL Prozesse hinzugefügt.

    - Für die Ticketart _Daten Aktualisierung_ erscheint eine Auswahl, welche Komponenten bei der Änderung angepasst werden sollen. Dabei können mehrere Checkboxen angehakt werden, die sich in mehrere Themen – Daten, FME und Absicherung – unterteilen. Die ausgewählten Komponenten werden mit in die Beschreibung des Tickets aufgenommen.  
    Wird die Checkbox „Database" angehakt, öffnet sich ein neues Fenster, in dem alle zum Datensatz zugehörige Collections abgebildet sind. Hier kann ausgewählt werden, welche Collections von einer Änderung betroffen sind, in dem man die Checkboxen an- oder abwählt. Im Beschreibungstext des Jira Tickets werden daraufhin nur die ausgewählten Collections mit ihrem Speicherort in der Datenbank (DB - Schema - Tabelle) aufgelistet.  
    Wird bei FME Schedule oder Automation angehakt, wird auch hier der Beschreibung des Tickets automatisch der Prozessname und wo dieser zu finden ist aus dem Reiter ETL Prozesse hinzugefügt.  
    Gibt es Anpassungen an der Absicherung einer Schnittstelle, kann dies über das Anwählen der Checkbox Absicherung angegeben werden. Nähere Informationen zur Änderung der Anpassungen müssen in das Anmerkungstextfeld eingetragen werden.

    - Für die Ticketart _Neue Daten_ und _Löschen_ wird automatisch erkannt, ob eine Absicherung für eine Schnittstelle hinterlegt ist und mit im Beschreibungstext aufgelistet. Auch hier müssen nähere Informationen allerdings im Anmerkungstextfeld angegeben werden.

4. Als nächstes kann ein Fälligkeitsdatum bestimmt werden. Als Standardwert ist das heutige Datum plus 7 Tage eingestellt. Dies kann je nach Dringlichkeit nach vorne oder hinten verschoben werden.

5. Zum Abschluss können in einem Textfeld weitere Anmerkungen als Freitext beschrieben werden. Diese Angaben werden ebenfalls der Beschreibung des Tickets hinzugefügt.

6. Neben den o.g. Eingaben über die GUI, werden noch weitere Felder des Jira Tickets standardmäßig gefüllt. Dabei handelt es sich um den Assignee/Bearbeiter des Tickets sowie das Feld „Story Points". Einstellungen dazu können in der Konfiguration (config.js - jira) getätigt werden.

7. Über den Button „Abschicken" wird das Ticket erstellt. Hierbei wird dem Ticket, wenn es als Sub-Task erstellt wurde, automatisch die Priorität des Haupttickets zugewiesen. Ein Ticket, das als Task erstellt wird, erhält standardmäßig eine mittlere Priorität. Wenn das Ticket erfolgreich angelegt wurde, schließt sich das Fenster zur Ticketerstellung automatisch.

8. Nachdem das Ticket erfolgreich angelegt wurde, kann das Ticket in eine beliebige Spalte des Jira Boards verschoben werden. Ein Jira Ticket wird standardmäßig im Backlog angelegt und kann mit einem neuen eigenständigen Request an die API verschoben werden. Diese Funktionalität ist in der Konfigurationsdatei (config.js - jira) für jedes dort eingetragene Jira Projekt individuell konfigurierbar. Mit dem Parameter _transition\_execution_kann die Funktionalität an- und ausgeschaltet werden. Im Parameter _transition\_id_ werden die IDs der Spalten des Jira Boards in einem Array hinterlegt. Hierbei sind 1 bis n Einträge möglich. Die Zahl der IDs ist abhängig vom Workflow des Jira Projekts. Dieser Workflow bestimmt, in welche Boardspalten ein Ticket aus der aktuellen Spalte verschoben werden darf. So kann es sein, dass mehrere Verschiebungen benötigen werden, um das angelegte Ticket aus dem Backlog in die gewünschte Spalte zu verschieben.