# Benutzerdokumentation

Der UDP-Manager fungiert in einer Urban Data Platform als _Single Point of Truth_. Hier werden sämtliche Informationen gesammelt, die notwendig sind um Datensätze zu beschreiben, zu gliedern, zu aktualisieren und über Schnittstellen bereitzustellen.

Zentrales Objekt im UDP-Manager ist der Datensatz. Am Datensatz werden alle übergreifenden Informationen wie Metadaten (Ansprechpartner, Lizenz, ...) oder die Freigaberegeln geführt. Ein Datensatz kann in _n_ Collections (Sub-Datensätze) unterteilt werden. Auf Datenbankebene würde ein Datensatz über ein Schema abgebildet und eine Collection über eine Tabelle, bzw. einen Datenbank-View. Eine Collection enthält konkrete Informationen zu den Daten selbst: die Definition des Datenschemas (Attribute, Datentypen,...), in welchem Datenbank-Schema / in welcher Datenbank-Tabelle die Daten abgelegt sind und über welche kartographische Ausgestaltung die Daten präsentiert werden sollen.

Jeder Datensatz kann über verschiedene Schnittstellen (WMS, WFS, OAF, ...) in den jeweiligen unterschiedlichen Ausgabeformaten oder Datentypen bereit gestellt werden. Dafür wird, ausgehend von einer Collection, ein schnittstellenspezifischer Layer definiert. An einem Layer werden Parameter hinterlegt, die notwendig sind, um die Collection über die verknüpfte Schnittstelle in Clients wie dem Masterportal einzubinden.

![vereinfachte Darstellung des Datenmodells](../../images/datamodel_simple.png)

Zur einheitlichen Benennung zusammen gehörender Entitäten (Datensatz, Schema, Schnittstellen-URL) wurde das Feld _Kurzname_ (_shortname_) auf Datensatzebene eingeführt. Aus dem Kurznamen eines Datensatzes werden beim Anlegen neuer Objekte folgende Felder abgeleitet:

- Collection: Datenbank-Schema
- Schnittstelle: Endpoint-Name, somit auch die interne und externe URL

Sobald alle relevanten Informationen im UDP-Manager hinterlegt sind, können automatisiert Schnittstellenkonfigurationen erzeugt werden. Zur Zeit werden deegree (WMS/WFS), GeoServer (WMS/WFS), ldproxy (OAF) und MapServer (Raster WMS) unterstützt. Durch den modularen Aufbau können Konfiguratormodule für weitere Schnittstellenimplementierung integriert werden.

## Abgrenzung
Der UDP-Manager bietet keine Benutzerverwaltung. Er wurde so entwickelt, dass ein bestehendes Active-Directory als Authentifizierung- und Autorisierungsschicht oder eine Keycloak Instanz als OpenID Connect Provider genutzt werden kann. Es wird auch keine Zugriffssteuerung für Schnittstellen angeboten.

Es ist auch keine Kartenviewer-Komponente enthalten. Es ist vorgesehen, dass eine externe Masterportalinstanz als Vorschauportal genutzt werden kann.

## Standardworkflow: Datensatz über Schnittstellen veröffentlichen, Konfigurationen automatisch generieren
Der folgende Beispielworkflow soll einen Überblick über die Funktionsweise des UDP-Managers bieten.

### Anlegen eines neuen Datensatzes

Der Datensatz ist das Hauptobjekt, dem alle anderen Informationen angehängt werden. Zu Beginn muss also ein neues Datensatzobjekt angelegt werden.

* _+_ Button unterhalb der Datensatztabelle drücken. Es wird ein neues, leeres Formular geöffnet. Ungespeicherte Objekte sind an der ID _0_ zu erkennen. Default Werte sind bereits vorausgefüllt (_Koordinatenreferenzsystem_, _Datentyp_, _DB-Verbindungen_).
* Zuerst müssen die Pflichtfelder _Titel_, _Kurzname_, _Verantwortliche Stelle_ und _Freigabeebene_ gefüllt werden.
* HINWEIS: Standardmäßig ist der Datentyp "Vektor" gewählt. Die Auswahl der DB-Verbindungen ist dabei wichtig für spätere Bearbeitungsschritte. Diese müssen im Vorfeld unter dem Reiter _Verwaltung_ konfiguriert werden. Wird "Beides" als Datentyp gewählt, kann später für jede Collection getrennt der Datentyp festgelegt werden. Dabei können nicht pro Collection unterschiedliche DB-Verbindungen gewählt werden.
* Anschließend wird empfohlen über _Speichern_ rechts unterhalb des Datensatzforumlars den Datensatz zwischenzuspeichern. Nach erfolgreichem Speichern wird die neu vergebene Datensatz ID sichtbar. Die Felder mit Informationen zu Bearbeiter und Bearbeitungsdatum werden automatisch befüllt.
* Im Bereich "Metadaten" kann ein Quellkatalog aus dem Drop-Down ausgewählt werden (muss vorher genau wie DB-Verbindungen unter _Verwaltung_ konfiguriert werden)
* Falls zur Hand, kann die UUID des Metadatensatzes in das entsprechende Feld eingetragen werden
* Über den Button mit Lupensymbol kann jetzt der gewählte Katalog nach dem Datensatztitel bzw. der UUID durchsucht werden
* Es öffnet sich ein neues Fenster mit den Suchergebnisse. Hier kann der korrekte Metadatensatz ausgewählt werden
* Über _OK_ werden die Informationen zu _Beschreibung_, _Schlagworte_, _Zugangsbeschränkungen_ und _Nutzungsbedingungen_ aus den Metadaten ausgelesen und in die entsprechende Felder im Datensatz Formular eingetragen.
* Durch einen Klick auf _Speichern_ wird der aktuelle Stand abgespeichert.

### Anlegen von Collections

Als Collection wird eine Datensatzuntergliederung verstanden, die die eigentlichen Daten beschreibt und im Standardfall bei Vektordaten durch eine Datenbanktabelle repräsentiert wird.

* _+_ Button unterhalb der Collections-Tabelle drücken. Default Werte sind bereits vorausgefüllt (_Transparenz_, _Min-Scale_, _Max-Scale_, _Namespace_, _Service URL sichtbar_, _GFI Theme_).
* Zuerst müssen die Pflichtfelder _Name (technisch)_, _Titel_ und _API-Zugriff_ gefüllt werden.
* Die Felder _Schema_ und _Tabelle_ werden automatisch ausgefüllt. Das Schema ist gleich dem Datensatz Kurznamen. Der Tabellenname ist gleich dem Collection-Namen.
* Anschließend wird empfohlen über _Speichern_ rechts unterhalb des Collection-Formulars die Collection zwischenzuspeichern. Nach erfolgreichem Speichern wird die neu vergebene Collection ID im Formular sichtbar.

### Attributkonfiguration

Für die weiteren Bearbeitungsschritte ist es Notwendig, das Datenschema der Collection zu definieren. Es sollte immer mindestens eine ID Spalte als Primary Key (PK) sowie eine Geometriespalte als Hauptgeometrie eingetragen werden.

* _Attribute konfigurieren_ Button im Collection-Details Formular drücken.
* Es öffnet sich ein neues Fenster, in dem die Attribute in eine Tabelle eingetragen werden können.
* _Zeile hinzufügen_ drücken.
* ID (Beispiel)
    * "id" in der Spalte _Name (Quelle)_ eintragen. Die Spalten _Name (DB)_, _Name (API)_ sowie _Name (Portal)_ werden automatisch ausgefüllt.
    * Unter _Datentyp_ "number" wählen".
    * Häkchen in der Checkbox _PK_ (Primary Key) setzen.
* Geometrie (Beispiel)
    * "geom" in der Spalte _Name (Quelle)_ eintragen. Die Spalten _Name (DB)_, _Name (API)_ sowie _Name (Portal)_ werden automatisch ausgefüllt.
    * Datentyp wählen (z.B. "geometry [point]").
    * Häkchen in der Checkbox _primäre Geometrie_ setzen.
* Weitere Attribute (Beispiel)
    * "name" in der Spalte _Name (Quelle)_ eintragen. Die Spalten _Name (DB)_, _Name (API)_ sowie _Name (Portal)_ werden automatisch ausgefüllt.
    * Unter _Datentyp_ "text" wählen".
* Durch einen Klick auf _Speichern_ wird die Attributkonfiguration abgespeichert.

### Datenbankschema und Tabelle anlegen

Wenn am Datensatz eine schreibende DB-Verbindung eingetragen ist und die Attributkonfiguration durchgeführt wurde, können Schema und Tabelle über den UDP-Manager angelegt werden.

* hinter dem Feld _Schema_ ist der _+_ Button aktiv, wenn eine schreibende DB-Verbindung am Datensatz existiert und das eingetragene Schema noch nicht in der Datenbank vorhanden ist. Die Liste aller Schemas wird bei Klick auf eine Collection in der Tabelle geladen und kann über das Drop-Down durchsucht werden.
* Durch das Drücken des _+_ Buttons hinter Schema wird das Schema in der Datenbank angelegt.
* hinter dem Feld _Tabelle_ ist der _+_ Button aktiv, wenn eine schreibende DB-Verbindung am Datensatz existiert, ein existierendes Schema gewählt ist und die Attributkonfiguration durchgeführt wurde.
* Durch das Drücken des _+_ Buttons hinter Tabelle öffnet sich eine neues Fenster zur Feindefinition des Datenbankschemas.
* Im neu geöffneten Fenster werden alle eingetragenen Attribute noch einmal aufgelistet. In der Spalte _Datentyp_ kann jetzt über das Drop-Down der korrekte Datentyp ausgewählt werden. Wichtig ist dies vor allem für numerische Datentypen und Datumsfelder.
* Durch drücken von _Tabelle anlegen_ wird in der Datenbank eine Tabelle nach definiertem Schema angelegt.

### Stylekonfiguration

Um die Daten über eine Schnittstelle wie WMS darstellen zu lassen, ist es notwendig die kartographische Darstellungsvorschrift zu definieren.

* _Style konfigurieren_ Button drücken. Es öffnet sich ein neues Fenster, in dem neue Style-Regeln angelegt und konfiguriert werden können.
* Über das Drop-Down _Geometrieattribut wählen_ muss definiert werden, auf welches Geometrieattribut der Collection sich die Style- oder Beschriftungsregel beziehen soll. Erst dann werden die übrigen Buttons aktiv.
* Nach drücken des _regel hinzufügen_ Buttons erscheint ein neuer Eintrag in der Tabelle. Hier kann über einen Doppelklick in der Spalte _Bezeichnung_ der Name (technisch) der Regel angepasst werden.
* Im Formularfeld _Titel_ kann zusätzlich zur Bezeichnung ein Titel in Langform angegeben werden.
* In den weiteren Formularfeldern kann die kartographische Ausgestaltung wie Farbe, Strichbreite oder Symbol ausgewählt werden.
* Im Bereich _Filter definieren_ kann eine Bedingung hinzugefügt werden, wann diese Style-Regel gelten soll. Zur Zeit kann nur eine Filterbedingung pro Regel eingegeben werden.
* Über den Button _Beschriftung hinzufügen_ kann eine spezielle Style-Regel hinzugefügt werden, mit der Objektbeschriftungen definiert werden können. Die Handhabung ist analog zur allgemeinen Style-Regel.
* Durch einen Klick auf _Speichern_ wird die Stylekonfiguration abgespeichert.

### Anlegen von Schnittstellen

Unter dem Reiter _Schnittstellen_ können Zugriffsmöglichkeiten für Datensätze beschrieben und konfiguriert werden.

* _+_ Button unterhalb der Schnittstellen-Tabelle drücken. Es wird ein neues, leeres Formular geöffnet.
* Zunächst müssen aus den Drop-Downs _Typ_ und _Software_ die gewünschten Werte ausgewählt werden.
* alle weiteren Pflichtfelder werden anschließend mit default Werten befüllt. Manuelle Anpassungen sollten in den meisten Fällen nicht notwendig sein.
* URLs können nur über den Editierbutton (Stiftsymbol) manuell bearbeitet werden.
* Bei externen Schnittstellen (Haken setzen in der entsprechenden Checkbox) muss nur die externe URL eingefügt werden (Feld ist dann immer aktiv).
* Anschließend wird empfohlen über _Speichern_ rechts unterhalb des Schnittstellen-Formulars die Schnittstelle zwischenzuspeichern. Nach erfolgreichem Speichern wird die neu vergebene Schnittstellen ID sichtbar.
* OPTIONAL: Metadaten koppeln / Eintragen
* Im Bereich Metadaten kann ein Quellkatalog aus dem Drop-Down ausgewählt werden (muss vorher eingetragen werden).
* Falls zur Hand, kann die UUID des Metadatensatzes in das entsprechende Feld eingetragen werden.
* Über den Button mit dem Lupensymbol kann jetzt der gewählte Katalog nach dem Schnittstellentitel bzw. der UUID durchsucht werden.
* Es öffnet sich ein neues Fenster mit den Suchergebnisse. Hier kann der korrekte Metadatensatz ausgewählt werden.
* Über _OK_ werden die Informationen zu _Beschreibung_, _Schlagworte_, _Zugangsbeschränkungen_ und _Nutzungsbedingungen_ aus den Metadaten ausgelesen und in die entsprechende Felder im Schnittstellen Formular eingetragen.
* Durch einen Klick auf _Speichern_ wird der aktuelle Stand abgespeichert.

### Layerkonfiguration

Sobald Collections und Schnittstellen hinzugefügt wurden, werden automatisch Layerobjekte für jede Collection und jede Schnittstelle erzeugt.
Diese können unter dem Reiter "Collections" und dann im Unterreiter "Layer" eingesehen und editiert werden.
Alle wichtigen Layer-Parameter werden mit default-Werten befüllt. Im Standardfall ist keine weitere manuelle Bearbeitung notwendig.

### Schnittstellenkonfiguration generieren und veröffentlichen
Der Workflow zur Verwaltung der Schnittstellenkonfigurationen ist an GitOps angelehnt. Als "Single-Point-Of-Truth" fungiert der Ordner (im besten Falls als git Repository) mit allen Konfigurationsdateien, bzw. der UDP-Manager der diesen Ordner bestückt. Der Gesamtprozess mit dreigliedrigem System (dev/stage/prod) ist in folgender Grafik abgebildet:

![Workflow Schnittstellenkonfiguration generieren und verteilen](../../images/configs.png)

1. Über einen Klick auf _Config Generieren_ werden alle für die jeweilige Schnittstelle benötigten Konfigurationsdateien erzeugt und im konfigurierten Ordner unter /configs/dev abgelegt. Dies ist nur im Bearbeitungsmodus möglich!
2. Nach Abschluss der Bearbeitung kann die Schnittstelle über die Funktion _verschieben nach_ in die Stage verschoben werden. Nach Klick auf den Button muss zunächst der Zielworkspace ausgewählt werden. Innerhalb des konfigurierten Ordners werden nun alle Konfigurationsdateien der gewählten Schnittstelle nach /configs/stage verschoben. Der _Status_ der Schnittstelle sowie _Instanz/Ordner_ werden automatisch angepasst.
3. Die Verschiebung nach Prod funktioniert prinzipiell genau wie unter 2. beschrieben, nur das die Konfigurationen nach /configs/prod kopiert werden, also auch in der Stage liegen bleiben. Der Workspace kann nicht angepasst werden.
4. Konfigurationen die im Status _Prod_ vorliegen können über die Funktion _Veröffentlichung zurücknehmen_ aus der Prod wieder gelöscht werden.

Ausgehend von den generierten Schnittstellenkonfigurationen können über die geplanten Aufgaben _(dev/stage/prod) Schnittstellenkonfigurationen zu Workspaces zusammen stellen_ die Workspace Ordner regelmäßig neu zusammen gestellt werden. Der Rhythmus kann für jede Umgebung (dev/stage/prod) getrennt definiert werden. In folgender Grafik ist dieser Prozess abgebildet.

![Workflow Workspace Generierung](../../images/workspaces.png)

### Schnittstellenkonfiguration - Ordnerstruktur

Der Ordner, indem alle Konfigurationsdateien abgelegt werden wird im _.env_-File im Attribut _CONFIG_OUT_PATH_ definiert. In diesem Ordner werden zwei Unterordner generiert:

- configs: in diesem Ordner werden sämtliche Schnittstellenkonfiguration sortiert nach Umgebung, Software und Schnittstellen-ID in eigenen Ordnern abgelegt. Dies ist notwendig, um einzelne Schnittstellen unabhängig ändern oder löschen zu können. Die Konfigurationen werden direkt nach Klick auf _Config Generieren_, in der Struktur wie in folgender Grafik zu sehen, erzeugt.

![Unterordnerstruktur von /configs](../../images/folder_configs.png)

- workspaces: in diesem Ordner werden aus den einzelnen Schnittstellenkonfigurationsordnern die Workspaces gegliedert nach Umgebung und Software zusammen kopiert. Die Struktur ist in folgender Grafik zu sehen. Das Generieren der Workspaces geschieht asynchron über die Tasks _(dev/stage/prod) Schnittstellenkonfigurationen zu Workspaces zusammen stellen_ (siehe **[geplante Aufgaben](../management/#Geplante Aufgaben)**). Die interne Struktur der Workspaces ist je nach verwendeter Software unterschiedlich. Für MapServer WMS wird eine Unterordnerstruktur erzeugt, in der Legendengrafiken, Sub-Mapfiles und GFI-Templates in jeweils eigenen Unterordnern abgelegt werden.

![Unterordnerstruktur von /workspaces](../../images/folder_workspaces.png)

Zusätzlich kann manuell ein Unterordner _static_files_ angelegt werden, der statische Ressourcen enthält, die bei der Generierung der Workspace-Ordner mit einbezogen werden. Die Ordnerstruktur ist in folgender Grafik dargestellt.

![Unterordnerstruktur von /static_files](../../images/folder_static_files.png)

### Verwendung von git
Es wird empfohlen die generierten Konfigurationen in einem git Repository zu versionieren. Im _.env_-File kann die Repository-URL konfiguriert werden. Weitere Parameter können in der UDP-Manager Konfiguration angepasst werden (siehe **[git](../configuration/#git)**).

Im konfigurierten Ausgabeordner muss hierfür zunächst das Repository initial geklont werden. Der UDP-Manager wird dann für jede Operation in diesem lokalen Repository commits erstellen und mit dem Remote Repository synchronisieren. Hierfür wird pro Minute geprüft ob offene Commits vorliegen und wenn ja ein `git push` ausgeführt

Über die geplante Aufgabe _git Repository aufräumen_ kann in regelmäßigen Abständen der Ordner von Artefakten bereinigt werden.

Dieses git Repository kann nun als Ausgangspunkt für eine Deployment-Pipeline genutzt werden, um die Konfigurationsdateien auf die Systeme, die die Schnittstellen bereit stellen sollen zu übertragen.

### Exemplarische GitOps-Pipeline zur Verwaltung von Schnittstellenkonfigurationen: Beispiel UDP Hamburg

Als Anregung zur eigenen Adaption und zwecks interner Doku folgt eine detaillierte Beschreibung der Implementierung in der UDP Hamburg:

* Der UDP-Manager schreibt Schnittstellenkonfigurationen und Style-Assets in ein lokales Git-Repository. Das zugehörige Remote-Repository liegt in einer internen Gitlab-Instanz.
* Aktuell differenzieren wir drei Umgebungen: dev, stage und prod. Im Repository sind diese bewusst nicht als Git-Branches, sondern als Unterordner repräsentiert. Die Vorteile werden in [diesem Artikel](https://codefresh.io/blog/stop-using-branches-deploying-different-gitops-environments/) anschaulich dargelegt.
* Neu erstellte oder modifizierte Schnittstellenkonfigurationen werden nach festem Pfadschema wie oben beschrieben abgelegt:
    * Als Workspaces bezeichnen wir unterschiedliche Instanzen einer Schnittstellensoftware. Diese können im UDP-Manager vorgegeben werden, um die Verteilung einzelner Schnittstellen auf mehrere Instanzen zu steuern.
* Nach erfolgreichem Schreibvorgang wird der betroffene Workspace im Repository automatisch neu zusammen kopiert.
    * Hier landen alle Schnittstellenkonfigurationen, die dem Workspace zugewiesen sind. Die Dateistruktur innerhalb eines Workspaces ist softwarespezifisch. Sie wird daher im Konfigurator Modul implementiert.
* Im selben Schritt werden Workspaces auch um statische Daten ergänzt. Etwa um Software- oder Workspace -übergreifende Konfigurationen.
* Soll eine Schnittstelle z.B. von _dev_ nach _stage_ rücken, werden alle zugehörigen Dateien zunächst innerhalb von `/configs` verschoben
    * Auch dieser Vorgang triggert ein erneutes Zusammenkopieren betroffener Workspaces
* Die beschriebenen Dateiänderungen werden zu einem Commit gebündelt und auf das Remote-Repository gepusht. Das löst folgende Schritte aus:
    * Mittels [Gitlab-Runner](https://docs.gitlab.com/runner/) startet das zentrale Repository einen Gitlab Job. Dieser triggert einen Git-Pull auf allen Repository-Klonen. Somit werden alle Konfigurationen permanent auf allen Zielservern synchronisiert.
    * Pro Zielserver ist letztlich nur ein Repository-Ausschnitt relevant. Welcher Workspace auf welchen Zielserver gehört wird ebenfalls im Repository definiert. Dazu startet der Gitlab Job nach erfolgtem Pull einen separaten Kopierprozess. Dabei werden Dateien nur noch innerhalb des Zielservers bewegt. Die Orchestrierung der Kopierjobs erfolgt mit Ansible.
    * Quell- und Zielpfade sollten auf eine Spiegelung von Dateien ausgelegt sein. Dies ist ein bequemer Weg, um im Repository gelöschte Dateien automatisch von den Zielservern zu entfernen.
* Wichtige Hinweise zum Löschen von Schnittstellen:
    * Entfernt werden zunächst nur Dateien unterhalb von `configs/[Umgebung]/[Schnittstellensoftware]/`
    * Das Zusammenkopieren von Workspaces, das in Folge jeder Änderung getriggert wird, überschreibt den betroffenen Workspace - gelöschte Schnittstellenkonfigurationen bleiben also vorerst im Workspace liegen.
    * Eine Lösung wäre, Workspaces nicht zu überschreiben sondern erst zu löschen und neu anzulegen. Umfasst der Workspace viele Konfigurationsdateien, kann das jedoch sehr lange dauern. In unserem Fall war die User-Experience dadurch stark beeinträchtigt.
    * Daher haben wir einen geplanten Prozess im UDP-Manager angelegt: In der Nacht werden alle Workspaces im Repository einmalig gelöscht und neu erzeugt. Durch den anschließenden Git-Pull und die Spiegelung wird das Löschen wie gehabt auf die Zielserver propagiert. Da es in unserem Regelbetrieb keinen Bedarf gibt, produktive Schnittstellen unmittelbar zu entfernen, reicht die nächtliche Bereinigung völlig aus.

**Lessons Learned**

* Verschiedene Umgebungen ohne Git-Branches zu verwalten hat sich schnell bewährt. Merge- bzw. Schreibkonflikte treten im beschriebenen Setup nicht auf.
* Die Pipeline wird nur bei Änderungen getriggert und schreibt lediglich geänderte Dateien. Das ist zwar elegant, erhöht aber auch die Prozesskomplexität. Zuvor hatten wir einen simplen Batchprozess im Einsatz. Mit entscheidenden Nachteilen:
    * Zumindest in der Testumgebung müssen Änderungen schnell wirksam werden, um eine flüssige Bearbeitung zu ermöglichen. Ein Ausführungsintervall von z.B. 5 Minuten war für unsere User nicht zufriedenstellend.
    * Kürzere Intervalle führten zu einem technischen Flaschenhals. In unserem Fall bedingt durch obligatorische Virenscans auf den Zielservern. Diese müssen bei jeder Timestamp-Änderung von Dateien anspringen. Weil der Batchprozess auch unveränderte Dateien immer wieder neu kopiert, muss der Virenscan stets alle Dateien durchlaufen. Weil das sehr ressourcenintensiv ist, werden ansonsten adäquat dimensionierte Server schnell überlastet.

## Datensätze Überarbeiten
Ein Datensatz dessen Schnittstellen produktiv veröffentlicht wurden befindet sich im Status "veröffentlicht". Es können nun im Formular Datensatz-Details nur noch redaktionelle Inhalte bearbeitet werden (Felder mit nicht fettgedrucktem Label). Für alle anderen Felder muss zuerst der Bearbeitungsmodus für diesen Datensatz gestartet werden. Hierfür muss der Knopf "Datensatz aktualisieren" in der Titelleiste gedrückt werden. Dies hat zur Folge, das sämtliche Schnittstellen aus dem Status _stage_ in _dev_ verschoben werden und der Datensatz-Status auf "veröffentlicht - in Bearbeitung" wechselt. Nun können sämtliche Felder bearbeitet werden. Zum Abschluss mus der oben beschriebene Veröffentlichungsworkflow durchlaufen werden.

Auf Collection-Ebene gibt es ebenfalls einige Felder, die auch ohne aktivem Bearbeitungsmodus editiert werden können. Dabei handelt es sich um alle Attribute, die für die Masterportal-Layerkonfiguration genutzt werden. Dazu zählt auch das Layer-Details Formular.

## Verwaltung von externen Schnittstellen
TODO

## Verwalten von Schnittstellen, ohne Konfigurationsverwaltung
TODO