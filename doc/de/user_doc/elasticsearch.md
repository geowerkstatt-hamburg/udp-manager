# Elasticsearch
Zur Verbesserung der Layersuche in Masterportalen kann ein Elasticsearch Suchindex eingebunden werden. Elasticsearch basiert auf Lucene. Das in Java geschriebene Programm speichert hierbei Dokumente in einem NoSQL-Format und ermöglicht somit auf einfache Weise die Umsetzung nach den Kriterien der Hochverfügbarkeit und Lastverteilung.

Ein Index in Elasticsearch setzt sich aus einzelnen Dokumenten im JSON-Format zusammen, welche mittels PUT/POST-Request an diesen gesendet werden. Um die Inhalte dieser Dokumente einem einheitlichen Satz an Attributen und Datentypen zuordnen zu können, empfiehlt es sich eine Datendefinition („Mapping") zu erstellen.

Die im UDP-Manager konfigurierten Indizes werden automatisch beim Speichern der Layer-/Schnittstellendetails aktualisiert. Weitere Informationen zur Konfiguration von Elasticsearch Indizes können unter /Verwaltung/Elasticsearch abgerufen werden.

## Mapping
Prinzipiell kann Elasticsearch ohne Schemas arbeiten, sodass kein explizites Mapping definiert werden muss. Für diesen Fall leitet Elasticsearch beim Indizieren von Dokumenten den Datentyp automatisch vom Inhalt ab. Der Vorteil eines explizit definierten Mappings ist jedoch, dass dadurch dem Server zusätzliche Informationen übergeben werden, wie die importierten Dokumente strukturiert sein müssen und wie sie zu durchsuchen sind. Zudem lassen sich durch ein Mapping fehlerhafte Datensätze beim Import erkennen.

Für eine Indexierung der Layerobjekte werden folgende Einstellungen und Mappings empfohlen und sind als Standardwerte hinterlegt:

```json
{
   "settings" : {
      "number_of_shards" : 1,
      "analysis": {
         "analyzer": {
            "autocomplete": {
               "tokenizer": "standard",
               "filter": [
                  "lowercase",
                  "german_normalization",
                  "word_delimiter",
                  "autocomplete_filter"
               ]
            }
         },
         "filter": {
            "autocomplete_filter": {
               "type": "edge_ngram",
               "min_gram": 3,
               "max_gram": 20
            }
         }
      }
   },
   "mappings" : {
      "properties": {
         "id": { "type": "text" },
         "name": { "type": "text", "analyzer": "autocomplete", "boost": 4},
         "layername": { "type": "text", "analyzer": "autocomplete", "boost": 2},
         "url": { "type": "text" },
         "typ": { "type": "text" },
         "featureType": {"type": "text"},
         "layers": { "type": "text" },
         "format": { "index": false, "type": "text" },
         "version": { "index": false, "type": "text" },
         "featureNS": { "index": false, "type": "text" },
         "singleTile": { "index": false, "type": "boolean" },
         "transparent": { "index": false, "type": "boolean" },
         "tilesize": { "index": false, "type": "integer" },
         "gutter": { "index": false, "type": "integer" },
         "transparency": { "index": false, "type": "integer" },
         "minScale": { "index": false, "type": "text" },
         "maxScale": { "index": false, "type": "text" },
         "gfiTheme": { "index": false, "type": "text" },
         "gfiComplex": { "index": false, "type": "text" },
         "infoFormat": { "index": false, "type": "text" },
         "layerAttribution": { "index": false, "type": "text" },
         "legendURL": { "index": false, "type": "text" },
         "cache": { "index": false, "type": "boolean" },
         "featureCount": { "index": false, "type": "integer" },
         "typeName": { "index": false, "type": "text" },
         "datasets": {
            "properties": {
               "md_id": {
                  "type": "text",
                  "fields": {
                     "keyword": {
                           "type": "keyword"
                     }
                  }
               },
               "csw_url": { "type": "text" },
               "show_doc_url": { "type": "text" },
               "rs_id": { "type": "text" },
               "md_name": { "type": "text", "analyzer": "autocomplete" },
               "bbox": { "index": false, "type": "text" },
               "keywords": { "type": "text" },
               "description": { "type": "text" },
               "kategorie_opendata": { "type": "text" },
               "kategorie_inspire": { "type": "text" },
               "kategorie_organisation": { "type": "text" }
            }
         },
         "gfiAttributes": {
            "enabled": false
         }
      }
   }
}
```