[zurück zur Übersicht](../api.md)

# Attributkonfiguration aktualisieren

Es die Attributkonfiguration einer Collection aktualisiert.

**URL** : `/backend/updateattributeconfig`

**Methode** : `POST`

**Content-Type**: `application/json`

**Datenschema**

Das an den Request angehängte Objekt muss die ID der Collection (collection_id) sowie die Attributkonfiguration als JSON Array enthalten.

```json
{
    "collection_id": [Integer],
    "attribute_config": [
        {
            "attr_name_source": "[String]",
            "attr_name_db": "[String]",
            "attr_name_service": "[String]",
            "attr_name_masterportal": "[String]",
            "attr_datatype": "[String]",
            "specifications": "[String]",
            "description": "[String]",
            "pk": [Boolean],
            "primary_geom": [Boolean],
            "primary_date": [Boolean],
            "unit": "[String]",
            "rule_mandatory": [Boolean],
            "rule_filter_vis": "[String]",
            "rule_filter_data": "[String]",
            "rule_order": "[String]",
            "rule_date": "[String]",
            "rule_georef": "[String]",
            "rule_note": "[String]",
        }
    ],
}
```

**Beispieldaten** Alle Attribute müssen gesetzt sein.

```json
{
    "collection_id": 23,
    "attribute_config": [
        {
            "attr_name_source": "Name",
            "attr_name_db": "name",
            "attr_name_service": "name",
            "attr_name_masterportal": null,
            "attr_datatype": "text",
            "specifications": null,
            "description": null,
            "pk": false,
            "primary_geom": false,
            "primary_date": false,
            "unit": "",
            "rule_mandatory": false,
            "rule_filter_vis": null,
            "rule_filter_data": null,
            "rule_order": null,
            "rule_date": null,
            "rule_georef": null,
            "rule_note": null,
        }
    ]
}
```

## Antwort bei Erfolg

**Code** : `200 OK`

**Rückgabe** Beispiel

```json
{
    "status": "success",
    "id": 1
}
```