[zurück zur Übersicht](../api.md)

# Layer JSON Objekte holen

Es wird eine Liste sämtlicher Masterportal Layer JSON Objekte zurückgegeben.

**URL** : `/backend/getetlprocesses[?id=:layer_id][?scope=:scope][?env=:env]`

**URL Parameter (optional)** : `layer_id=[Integer]` wobei layer_id die ID eines Layers als Integer ist. `scope=[String]` kann entweder _intranet_ oder _internet_ sein. Bei _intranet_ werden alle Layerobjekte von intern freigegebenen Layer zurück gegeben. Bei _internet_ werden alle Layerobjekte von extern freigegebenen Layer zurück gegeben. `env=[String]` kann entweder _stage_ oder _prod_ sein. Es wird die LayerJSON der entsprechenden Umgebung zurück gegeben, wobei bei _stage_ auch _dev_-Layer inkludiert sind.

**Methode** : `GET`

## Antwort bei Erfolg

**Code** : `200 OK`

**Beispiel für eine Rückgabe**

```json
[
  {
    "id": "123",
    "name": "Layername",
    "url": "https://host.de/wms",
    "typ": "WMS",
    "layers": "layer",
    "format": "image/png",
    "version": "1.3.0",
    "singleTile": false,
    "transparent": true,
    "transparency": 0,
    "urlIsVisible": true,
    "tilesize": 512,
    "gutter": 20,
    "minScale": "0",
    "maxScale": "2500000",
    "infoFormat": "text/xml",
    "gfiAttributes": {
      "attr_1": "Attribut 1"
    },
    "gfiTheme": "default",
    "layerAttribution": "nicht vorhanden",
    "legendURL": "",
    "cache": false,
    "featureCount": 5,
    "datasets": [
      {
        "md_id": "UUID",
        "show_doc_url": "https://metaver.de/trefferanzeige?docuuid=",
        "rs_id": "RSID",
        "bbox": "461468.96892897453,5916367.229806512,587010.9095989474,5980347.755797674",
        "kategorie_opendata": [
          "Wissenschaft und Technologie",
          "Regionen und Städte",
          "Bevölkerung und Gesellschaft"
        ],
        "kategorie_inspire": [
          "Versorgungswirtschaft und staatliche Dienste"
        ],
        "kategorie_organisation": "Amt"
      }
    ],
    "notSupportedFor3DNeu": false
  }
]
```