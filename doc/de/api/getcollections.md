[zurück zur Übersicht](../api.md)

# Collections holen

Es wird eine Liste sämtlicher Collections pro Datensatz mit allen Attributen zurückgegeben.

**URL** : `/backend/getcollectionsdataset?dataset_id=:dataset_id]`

**URL Parameter (verpflichtend)** : `dataset_id=[Integer]` wobei dataset_id die ID eines Datensatzes als Integer ist.

**Methode** : `GET`

## Antwort bei Erfolg

**Code** : `200 OK`

**Beispiel für eine Rückgabe**

```json
{
    "results": [
        {
            "id": 172,
            "dataset_id": 50,
            "name": "strecken",
            "title": "Titel der Collection",
            "title_alt": "alternativer Titel",
            "attribution": "Attribution",
            "additional_categories": "cat4|cat5",
            "legend_url_intranet": "https://legende_intern.de",
            "legend_url_internet": "https://legende_extern.de",
            "transparency": 0,
            "scale_min": "0",
            "scale_max": "2500000",
            "gfi_disabled": false,
            "gfi_theme": "default",
            "gfi_theme_params": "GFI Parameter",
            "attribute_config": [
                {
                    "attr_name_source": "name",
                    "attr_name_db": "name",
                    "attr_name_service": "Name",
                    "attr_name_masterportal": "name",
                    "attr_datatype": "text",
                    "description": "Bezeichnung",
                    "pk": false,
                    "primary_geom": false,
                    "primary_date": false,
                    "unit": null,
                    "rule_mandatory": true,
                    "rule_filter_vis": null,
                    "rule_filter_data": null,
                    "rule_order": null,
                    "rule_date": null,
                    "rule_georef": null,
                    "rule_note": null
                }
            ],
            "gfi_as_new_window": false,
            "gfi_window_specs": "GFI Fenster Parameter",
            "service_url_visible": true,
            "db_schema": "dbschema",
            "db_table": "dbtable",
            "style": [
                {
                    "rule_name": "line_style",
                    "title": "line_style",
                    "stroke_color_dynamic": false,
                    "stroke_color_dynamic_attribute": "app:visual",
                    "stroke_color_hex": "dc40c2",
                    "stroke_width_line": 8,
                    "stroke_opacity": 1,
                    "pattern_style": "gestrichelt",
                    "pattern_dash_array": 5,
                    "filter_attribute": "",
                    "filter_condition": "",
                    "filter_literal": "",
                    "filter_literal_min": "",
                    "filter_literal_max": "",
                    "geom_name": "geom",
                    "geom_type": "line",
                    "id": 5563
                }
            ],
            "namespace": "ns=http://www.namespace.org/ns",
            "additional_info": "zusätzliche Infos",
            "description": "Beschreibung",
            "group_object": false,
            "legend_data_url": "data:image/png;base64,[...]",
            "legend_content_type": "image/png",
            "legend_file_name": "Legende",
            "custom_bbox": "1,2,3,4",
            "api_constraint": "visual_download",
            "complex_scheme": false,
            "tileindex": null,
            "use_style_sld": false,
            "style_sld": null,
            "style_sld_icons": null,
            "number_features": 120,
            "alternative_name": null,
            "store_type": null,
            "attr_log": {
                "status": "no change detected",
                "attr_diff": "<br>Die Attribute haben sich nicht geändert!",
                "timestamp": "2024-10-25 12:00",
                "attr_diff_json": null
            }
        }
    ]
}
```