[zurück zur Übersicht](../api.md)

# ETL Prozessinformationen aktualisieren

Es wird das Datum, wann der ETL Prozess zuletzt gelaufen ist aktualisiert.

**URL** : `/backend/updateetlsummary`

**Methode** : `POST`

**Content-Type**: `application/json`

**Datenschema**

Das an den Request angehängte Objekt muss die ID des ETL-Prozesses (id) und das Datum, wann der Prozess gelaufen ist (last_run) enthalten.

```json
{
    "id": [Integer],
    "last_run": "[DateTime String - YYYY-MM-DD HH:mm:ss]"
}
```

**Beispieldaten** Alle Attribute müssen gesetzt sein.

```json
{
    "id": 1,
    "last_run": "2023-07-10 12:00:00"
}
```

## Antwort bei Erfolg

**Code** : `200 OK`

**Rückgabe** Beispiel

```json
{
    "status": "success",
    "id": 1
}
```