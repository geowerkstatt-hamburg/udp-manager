[zurück zur Übersicht](../api.md)

# Datensätze holen

Es wird eine Liste sämtlicher Datensätze mit allen Attributen zurückgegeben. Optional können alle Datensätze, für die ein spezieller User berechtigt ist, geholt werden.

**URL** : `/backend/getdatasets[?objectguid=:objectguid][?user=:user]`

**URL Parameter (optional)** : `objectguid=[String]` wobei objectguid die objectGUID eines AD Accounts in Form eines UUID-Strings ist. `user=[String]` wobei user der sAMAccountName eines AD Accounts in Form eines Strings ist.

**Methode** : `GET`

## Antwort bei Erfolg

**Code** : `200 OK`

**Beispiel für eine Rückgabe**

```json
[
    {
        "id": 50,
        "metadata_catalog_id": 1,
        "title": "Datensatztitel",
        "md_id": "4E67DF32-AAC0-4410-A215-4110F8D50BBD",
        "rs_id": "https://registry.gdi-de.org/id/de.hh/e085712c-1cce-49b6-a4c3-c28e58a76da4",
        "shortname": "kurzname",
        "responsible_party": "Team1",
        "description": "Beschreibung",
        "access_constraints": "Zugangsbeschränkungen",
        "bbox": "8.420551 53.394985,10.326304 53.964153",
        "cat_hmbtg": "cat1|cat2|cat3",
        "cat_inspire": "cat1|cat2|cat3",
        "cat_opendata": "cat1|cat2|cat3",
        "cat_org": "Organisation",
        "fees": "Datenlizenz Deutschland Namensnennung 2.0, Quellenvermerk: Freie und Hansestadt Hamburg, Behörde für Umwelt, Klima, Energie und Agrarwirtschaft (BUKEA)",
        "fees_json": "{\"id\":\"dl-by-de/2.0\",\"name\":\"Datenlizenz Deutschland Namensnennung 2.0\",\"url\":\"https://www.govdata.de/dl-de/by-2-0\",\"quelle\":\"Freie und Hansestadt Hamburg, Behörde für Umwelt, Klima, Energie und Agrarwirtschaft (BUKEA)\"}",
        "filter_keywords": [
            "filter_keyword1",
            "filter_keyword2",
        ],
        "keywords": [
            "keyword1",
            "keyword2",
        ],
        "db_connection_r_id": 1,
        "db_connection_w_id": 2,
        "file_store": "Laufwerk",
        "store_type": "Vektor",
        "datasource": "Fachsystem",
        "last_edit_date": "2023-01-01 10:00",
        "last_edited_by": "Max Mustermann",
        "create_date": "2023-01-01 10:00",
        "created_by": "Max Mustermann",
        "publish_date": null,
        "status": "in Bearbeitung",
        "federalstate": "Bundesland",
        "district": "Landkreis",
        "organisation": "Organisation",
        "department": "Abteilung",
        "additional_info": "zusätzliche Informationen",
        "freigabe_link": "https://freigabe/freigabe.pdf",
        "inspire": false,
        "external": false,
        "srs": "EPSG:25832",
        "restriction_level": "internet",
        "no_catalogue_link": false,
        "contacts": [
            {
                "id": 2,
                "name": "Muster, Max",
                "given_name": "Max",
                "surname": "Muster",
                "company": "LGV",
                "ad_account": "MusterMax",
                "email": "max.mustermann@company.de",
                "tel": "+49 10 123 11-1111",
                "objectguid": "21d7788d-1a97-4292-8969-afefd225bcf3"
            }
        ],
        "project_reference": "Projektrefenz",
        "portal_bound": false,
        "portal_bound_name": "Portalname",
        "allowed_groups": [
            "Gruppenname"
        ]
    }
]
```