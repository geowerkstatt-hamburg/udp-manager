[zurück zur Übersicht](../api.md)

# Anzahl Features aktualisieren

Es wird die Anzahl der Features für eine Collections aktualisiert

**URL** : `/backend/updatenumberfeatures`

**Methode** : `POST`

**Content-Type**: `application/json`

**Datenschema**

Das an den Request angehängte Objekt muss die ID der Collection (id) sowie die Anzahl der Features (number_features) enthalten.

```json
{
    "id": [Integer],
    "number_features": [Integer]
}
```

**Beispieldaten** Alle Attribute müssen gesetzt sein.

```json
{
    "id": 1,
    "number_features": 100,
}
```

## Antwort bei Erfolg

**Code** : `200 OK`

**Rückgabe** Beispiel

```json
{
    "status": "success"
}
```