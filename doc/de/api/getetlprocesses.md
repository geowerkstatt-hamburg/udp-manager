[zurück zur Übersicht](../api.md)

# ETL Prozesse holen

Es wird eine Liste sämtlicher ETL Prozesse, die mit einer Collection oder einem Datensatz verknüpft sind, mit den für die Verbindung relevanten Attributen zurückgegeben.

**URL** : `/backend/getetlprocesses[?collection_id=:collection_id][?dataset_id=:dataset_id]`

**URL Parameter (verpflichtend)** : `collection_id=[Integer]` oder `dataset_id=[Integer]` wobei collection_id die ID einer Collection als Integer und dataset_id die ID eines Datensatzes als Integer ist.

**Methode** : `GET`

## Antwort bei Erfolg

**Code** : `200 OK`

**Beispiel für eine Rückgabe bei übergabe einer Collection-ID**

```json
[
    {
        "id": 619,
        "type": "FME Workspace",
        "name": "workspace.fmw",
        "repository": "Repository",
        "host": "FME Server"
    }
]
```

**Beispiel für eine Rückgabe bei übergabe einer Datensatz-ID**

```json
[
    {
        "id": 619,
        "type": "FME Workspace",
        "name": "workspace.fmw",
        "description": "Description",
        "folder": "FME Repository",
        "host": "FME Server",
        "interval_custom": null,
        "interval_number": null,
        "interval_unit": null,
        "last_run": "2023-08-15 13:25",
        "dataset_id": 876,
        "mapped_collections": [
            {
                "id": 1,
                "name": "collection_name"
            }
        ],
        "repo": null,
        "service_definition": null,
        "doclink": null
    }
]
```