[zurück zur Übersicht](../api.md)

# Datenbankverbindung holen

Es wird eine Datenbankverbindung mit allen Attributen (außer Passwort) zurückgegeben. Als Filterparameter muss die Verbindungs-ID angegeben werden.

**URL** : `/backend/getdbconnection[?id=:id]`

**URL Parameter (benötigt)** : `id=[Integer]` wobei id die Verbindungs-ID einer Datenbankverbindung als Integer ist.

**Methode** : `GET`

## Antwort bei Erfolg

**Code** : `200 OK`

**Beispiel für eine Rückgabe**

```json

{
    "id": 1,
    "name": "connection_name",
    "host": "db.host.de",
    "port": 5432,
    "database": "db",
    "user": "user",
    "use_as_default_r": true,
    "use_as_default_w": false
}
```