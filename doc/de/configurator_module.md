# Modul zur softwareunabhängigen Konfiguration von Geo-APIs

Das Modul strukturiert generische Konfigurationsobjekte vor.
Diese sind um ein ebenso generisches Datenobjekt herum strukturiert.
Um softwarespezifische Konfigurationsdatein mittels UDP-Manager zu erzeugen,
muss das Modul um entsprechende Implementierungen erweitert werden.
Zum Release werden Deegree (WFS+WMS), GeoServer (WFS+WMS) sowie ldproxy (OAF) unterstützt.

# Beziehungen und Eigenschaften konfigurierbarer Entitäten

### 1 Datensatz : 1 API Endpoint

Alle wesentlichen Informationen, die zur Konfiguration von WFS, WMS, OAF, VectorTiles usw.
benötigt werden, gehen vom Datensatz aus. Darunter auch sämtliche Metadaten.

Daran anknüpfend haben wir folgende Regel aufgestellt: "1 Datensatz = 1 Endpoint".
Jeder Schnittstellentyp (WFS, WMS, OAF etc.) gilt als Endpoint. In jeder Serverlösung zur Bereitstellung solcher APIs wird letztlich auf Ebene einzelner Datensätze gesteuert, weleche Daten wie verfügbar sein sollen.

Konzeptionell benötigt also jeder Datensatz für jeden Schnittstellentyp ein eigenes "Konfigurationsbündel".
Im exemplarischen Fall von Deegree braucht es zur Konfiguration von Datensatz X über den Schnittstellentyp WFS
bis zu einem Dutzend XML Dateien, die aufeinender verweisen und in einer bestimmten Dateihierarchie angeordnet werden müssen.
Nun ließe sich ein Datensatz beispielsweise als WFS und WMS mittels Deegree oder GeoServer veröffentlichen, während die Bereitstellung als OAF
mittels ldproxy erfolgt. Ein und derselbe Schnittstellentyp zu ein und demselben Datensatz (z.B. "OAF Schulen Hamburg") kann nicht parallel über zwei verschiedene Softwarelösungen (deegree OAF und ldproxy OAF) verwaltet werden.

Mit wachsender Zahl an Datensätzen und parallel betriebenen API steigt die Zahl und Komplexität benötigter "Konfigurationsbündel" rapide an. Die konsequente 1:1 Beziehung zwischen Datensatz und softwarespezifischem Schnittstellentyp vereinfacht die konzeptionelle und technischen Administration von Schnittsstellen.

### N Datensätze : 1 Datensatz-API

Auch diese Variante lässt sich im Datenmodell des UDP-Managers abbilden, auch wenn sie eine Ausnahme bleiben soll.
Im Kontext der UDP Hamburg ist dies etwa nötig, um mehrere Datensätze über einen STA Endpunkt zu konfigurieren.
Das Konfigurator-Modul selbst unterstützt die N:1 Konstellation derzeit nicht.
In solchen Fällen müssen Konfigurationsdateien manuell angelegt werden.

### 1 Datensatz: N Collections

Datensätze können nach belibigen Merkmalen untergliedert werden.
Solche Untereinheiten werden hier mit dem generischen Begriff "Collections" subsummiert.
Der Datensatz "Hamburg Schulen" könnte sich etwa in die Collections "Berufsschulen, Gymnasien, Sondersculen" etc.
gliedern. Oder in eine Collection mit allen Schulstandorten (inkl. Attribut "Schulart") und eine separate Collection
mit Relationen zwischen den Schulen (z.B. Kooperationsstrukturen). Diese Gliederung obliegt den Dateneigentümern.

### 1 Collection: 1 Datensatz-API-Untereinheit

Im Regelfall werden einzelne Collections auch als einzelne Tabellen oder Views im relationalen Datenbankmodell abgebildet.
Eine Collection kann aber auch eine hierarchsich strukturierte Datei referenzieren (z.B. als GML oder GeoJSON)
In jedem Fall haben Collections stets eine eigene Schemadefinition und eine distinkte Datenquelle.

Sofern sich eine Datensatz-API untergliedern lässt, orientiert sie sich an den Collections.
N Collections vom Datensatz "Hamburger Schulen" werden...

* ...zu N FeatureTypes im WFS

* ...zu N Layern im WMS

* ...zu N Layern im Vector Tile Cache

* ...zu N Collections im OAF

* usw.

Auch hier zieht sich die 1:1 Logik konsequent durch.


# Ein generisches Datenobjekt mit allen Attributen für die Konfiguration

Hier kommen alle Informationen zur Veröffentlichung eines Datensatzes über dedizierte Schnittstellen zusammen.
Das Objekt ist als einfach erweiterbares JSON Literal angelegt, mit folgenden Unterobjekten:

```
const genericConfigData = {

    dataset: {...},
    collections: {...},
    service: {...},
    catalogue: {...},
    connection: {...}
    software: {...},
    provider: {...},

}
```

**TODO: Vollständig ausgefülltes Beispiel verlinken**

### Aus der Datenbank: dataset / catalogue / connection / collections / service
Diese Objekte entsprechen den jeweiligen Entitäten im Datenmodell des UDP-Managers.
Für jeden Datensatz wir in der GUI ausgewählt, welche Collections veröffentlicht werden sollen.
und über welche Schnittstelle (=service) das geschieht. Das Connection Objekt legt die zugehörige Datenbankverbindung fest.
Mit Catalogue ist der Metadatenkatalog gemeint, der die Datensatz- und Schnittstellenbeschreibung enthält.
Hier sind auch die Parameter hinterlegt, die zur Interaktion mit der jeweiligen CSW Schnittstelle benötigt werden.

### Aus softwarespezifischen JSON-Configs im Konfigurator-Modul: software
Globale Einstellungen zur Software, mit der APIs bereitgestellt werden, liegen als individuelle JSON Dateien im Konfigurator-Modul.
Hier lassen sich u.a. folgende Einstellungen unterbringen:

* Mapping von Posgres Datentypen zu softwareseitig abstrahierten Datentypen

* unterstützte Projektionen

* schnittstellenspezifische Konfig-Defaults

### Aus der globalen config.js: provider und Mapping "Schnittstellentyp : Software"
Metadaten zum Bereitsteller (provider) der Schnittstellen sind in der globalen config.js
einstellbar. Siehe dazu die separate `config.md` Dokumentation.

Wichtig: Hier wird auch definiert, welcher Schnittstellentyp durch welche Software veröffentlich werden soll.
Beispielsweise WFS+WMS via Deegree und OAF via ldproxy.


# Ein Design-Pattern: Factory-Method
Im Submodul `./backend/configurator/generic_components` sind die generischen Objekte enthalten, von denen sich die softwarepsezifischen Implementierungen ableiten sollen:

Die Klasse *ServiceConfigurator* aus `configurator.js` agiert als "Factory" nach dem Factory-Method Pattern.
Sie ist für das Erzeugen konkreter API-Konfigurationen zuständig. Die Methode zur Erzeugung ist jedoch als abstrakte Methode hinterlegt. Softwarespezifische Konfiguratoren (z.B. der DeegreeConfigurator) müssen vom ServiceConfigurator erben und besagte Methode implementieren.

Das Erzeugnis der Methode ist ein *Service* Objekt, ebenfalls als abstrakte Klasse angelegt (siehe `service.js`).
Instanziiert wird es mit dem generischen Datenobjekt zur Service/API Konfiguration. Die abstrake *Service* Klasse ist abhängig von dessen Struktur. Ihr Konstruktor zerlegt das Datenobjekt in die übergreifenden Bestandteile.
Auch werden hier diverse Getter und sonstige Methoden platziert, die den Zugriff auf häufig benötigte Attribute
im Datenobjekt vereinfachen (z.B. Service.collectionNames). Softwarespezifische Implementierungen eines Service (z.b. ein WebFeatureService in Deegree) erben und erweitern die Attribute und Methoden.
Dabei müssen sie auch die abstrakte Methode `Service.createConfigs()` implementieren.

Das Parsen gängiger Dateiformate zur API-Konfiguration (XML, JSON, YAML) wird ebenfalls als generische Komponente
verstanden. Entsprechende Funktionen können zentral unter `parser.js` abgelegt werden.

Der Entrypoint für Clients vom Konfigurator-Modul ist die `./backend/configurator/index.js`
Sie enthält lediglich die Klasse *ConfiguratorFactory*. Dabei handelt es sich um eine sogenannte "Simple Factory".
Abhängig von der spezifizierten Software im generischen Datenobjekt "entscheidet" die Klasse, welcher Konfigurator instanziert werden muss. So kann die gesamte konfigurationsspezifische Logik vom Client abstrahiert werden. Sie reduziert sich auf zwei Zeilen:

```
// instanziiere den passenden Konfigurator
const configurator = ConfiguratorFactory.create(data.software.name);

// speichere den service mittels Konfigurator-übergreifendem Interface .storeService()
// das Service Objekt und seine Subklassen werden ausschließlich innerhalb der Konfiguratoren verarbeitet.
configurator.storeService(data);
```

Eine Illustration vom Zusammenspiel abstrakter und konreter Klassen nach beschriebenen Design-Pattern:
![Illustration](../images/generic_api_configurator.png "Generischer API Konfigurator")
