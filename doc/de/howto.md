# How-To UDP-Manager

In den folgenden Abschnitten wird der komplette Workflow, vom anlegen eines Datensatzes bis zur Konfiguration von Schnittstellen beschrieben und die genauen Arbeitsschritte in der Oberfläche in Videos dargestellt.

## Anlegen eines Datensatzes

Der Datensatz ist das Hauptobjekt, dem alle anderen Informationen angehängt werden. Als erstes muss also ein Datensatzobjekt angelegt werden.

![type:video](/udp_manager_videos/create_dataset.mp4){: style='width: 100%'}

* _+_ Button unterhalb der Datensatztabelle drücken.
* Es wird ein neues, leeres Formular geöffnet. Ungespeicherte Objekte sind an der ID _0_ zu erkennen.
* Default Werte sind bereits vorausgefüllt (_Koordinatenreferenzsystem_, _Datentyp_, _DB-Verbindungen_).
* HINWEIS: Die Auswahl der DB-Verbindungen ist wichtig für spätere Bearbeitungsschritte. Diese müssen im Vorfeld unter dem Reiter _Verwaltung_ konfiguriert werden.
* Zuerst müssen die Pflichtfelder _Titel_, _Kurzname_, _Verantwortliche Stelle_ und _Freigabeebene_ gefüllt werden.
* Anschließend wird empfohlen über _Speichern_ rechts unterhalb des Datensatzforumlars den Datensatz zwischenzuspeichern.
* Nach erfolgreichem Speichern wird die neu vergebene Datensatz ID im Formular sichtbar. Die Felder mit Informationen zu Bearbeiter und Bearbeitungsdatum werden automatisch befüllt.
* OPTIONAL: Metadaten koppeln / Eintragen
* Im Bereich Metadaten kann ein Quellkatalog aus dem Drop-Down ausgewählt werden (muss vorher genau wie DB-Verbindungen unter _Verwaltung_ konfiguriert werden)
* Falls zur Hand, kann die UUID des Metadatensatzes in das entsprechende Feld eingetragen werden
* Über den Button mit Lupensymbol kann jetzt der gewählte Katalog nach dem Datensatztitel bzw. der UUID durchsucht werden
* Es öffnet sich ein neues Fenster mit den Suchergebnisse. Hier kann der korrekte Metadatensatz ausgewählt werden
* Über _OK_ werden die Informationen zu _Beschreibung_, _Schlagworte_, _Zugangsbeschränkungen_ und _Nutzungsbedingungen_ aus den Metadaten ausgelesen und in die entsprechende Felder im Datensatz Formular eingetragen.
* Durch einen Klick auf _Speichern_ wird der aktuelle Stand abgespeichert.

## Anlegen von Collections

Als Collection wird eine Datensatzuntergliederung verstanden, die die eigentlichen Daten beschreibt und im Standardfall durch eine Datenbanktabelle repräsentiert wird.

![type:video](/udp_manager_videos/create_collection.mp4){: style='width: 100%'}

* _+_ Button unterhalb der Collections-Tabelle drücken.
* Es wird ein neues, leeres Formular geöffnet. Ungespeicherte Objekte sind an der ID _0_ zu erkennen.
* Default Werte sind bereits vorausgefüllt (_Transparenz_, _Min-Scale_, _Max-Scale_, _Namespace_, _Service URL sichtbar_, _GFI Theme_).
* Zuerst müssen die Pflichtfelder _Name (technisch)_, _Titel_ und _API-Zugriff_ gefüllt werden.
* Die Felder _Schema_ und _Tabelle_ werden automatisch ausgefüllt. Das Schema ist gleich dem Datensatz Kurznamen. Der Tabellenname ist gleich dem Collection-Namen.
* Anschließend wird empfohlen über _Speichern_ rechts unterhalb des Collection-Formulars die Collection zwischenzuspeichern.
* Nach erfolgreichem Speichern wird die neu vergebene Collection ID im Formular sichtbar.

## Attributkonfiguration

Für die weiteren Bearbeitungsschritte ist es Notwendig, das Datenschema der Collection zu definieren. Es sollte immer mindestens eine ID Spalte als Primary Key (PK) sowie eine Geometriespalte als Hauptgeometrie eingetragen werden.

![type:video](/udp_manager_videos/attribute_config.mp4){: style='width: 100%'}

* _Attribute konfigurieren_ Button im Collection-Details Formular drücken.
* Es öffnet sich ein neues Fenster, in dem die Attribute in eine Tabelle eingetragen werden können.
* _Zeile hinzufügen_ drücken.
* ID (Beispiel)
    * "id" in der Spalte _Name (Quelle)_ eintragen. Die Spalten _Name (DB)_, _Name (API)_ sowie _Name (Portal)_ werden automatisch ausgefüllt.
    * Unter _Datentyp_ "number" wählen".
    * Häkchen in der Checkbox _PK_ (Primary Key) setzen.
* Geometrie (Beispiel)
    * "geom" in der Spalte _Name (Quelle)_ eintragen. Die Spalten _Name (DB)_, _Name (API)_ sowie _Name (Portal)_ werden automatisch ausgefüllt.
    * Datentyp wählen (z.B. "geometry [point]").
    * Häkchen in der Checkbox _primäre Geometrie_ setzen.
* Weitere Attribute (Beispiel)
    * "name" in der Spalte _Name (Quelle)_ eintragen. Die Spalten _Name (DB)_, _Name (API)_ sowie _Name (Portal)_ werden automatisch ausgefüllt.
    * Unter _Datentyp_ "text" wählen".
* Durch einen Klick auf _Speichern_ wird die Attributkonfiguration abgespeichert.

## Datenbankschema und Tabelle anlegen

Wenn am Datensatz eine schreibende DB-Verbindung eingetragen ist und die Attributkonfiguration durchgeführt wurde, können Schema und Tabelle über den UDP-Manager angelegt werden.

![type:video](/udp_manager_videos/create_schema_table.mp4){: style='width: 100%'}

* hinter dem Feld _Schema_ ist der _+_ Button aktiv, wenn eine schreibende DB-Verbindung am Datensatz existiert und das eingetragene Schema noch nicht in der Datenbank vorhanden ist. Die Liste aller Schemas wird bei Klick auf eine Collection in der Tabelle geladen und kann über das Drop-Down durchsucht werden.
* Durch das Drücken des _+_ Buttons hinter Schema wird das Schema in der Datenbank angelegt.
* hinter dem Feld _Tabelle_ ist der _+_ Button aktiv, wenn eine schreibende DB-Verbindung am Datensatz existiert, ein existierendes Schema gewählt ist und die Attributkonfiguration durchgeführt wurde.
* Durch das Drücken des _+_ Buttons hinter Tabelle öffnet sich eine neues Fenster zur Feindefinition des Datenbankschemas.
* Im neu geöffneten Fenster werden alle eingetragenen Attribute noch einmal aufgelistet. In der Spalte _Datentyp_ kann jetzt über das Drop-Down der korrekte Datentyp ausgewählt werden. Wichtig ist dies vor allem für numerische Datentypen und Datumsfelder.
* Durch drücken von _Tabelle anlegen_ wird in der Datenbank eine Tabelle nach definiertem Schema angelegt.

## Stylekonfiguration

Um die Daten über eine Schnittstelle wie WMS darstellen zu lassen, ist es notwendig die kartographische Darstellungsvorschrift zu definieren.

![type:video](/udp_manager_videos/create_style.mp4){: style='width: 100%'}

* _Style konfigurieren_ Button drücken. Es öffnet sich ein neues Fenster, in dem neue Style-Regeln angelegt und konfiguriert werden können.
* Über das Drop-Down _Geometrieattribut wählen_ muss definiert werden, auf welches Geometrieattribut der Collection sich die Style- oder Beschriftungsregel beziehen soll. Erst dann werden die übrigen Buttons aktiv.
* Nach drücken des _regel hinzufügen_ Buttons erscheint ein neuer Eintrag in der Tabelle. Hier kann über einen Doppelklick in der Spalte _Bezeichnung_ der Name (technisch) der Regel angepasst werden.
* Im Formularfeld _Titel_ kann zusätzlich zur Bezeichnung ein Titel in Langform angegeben werden.
* In den weiteren Formularfeldern kann die kartographische Ausgestaltung wie Farbe, Strichbreite oder Symbol ausgewählt werden.
* Im Bereich _Filter definieren_ kann eine Bedingung hinzugefügt werden, wann diese Style-Regel gelten soll. Zur Zeit kann nur eine Filterbedingung pro Regel eingegeben werden.
* Über den Button _Beschriftung hinzufügen_ kann eine spezielle Style-Regel hinzugefügt werden, mit der Objektbeschriftungen definiert werden können. Die Handhabung ist analog zur allgemeinen Style-Regel.
* Durch einen Klick auf _Speichern_ wird die Stylekonfiguration abgespeichert.

## Anlegen von Schnittstellen

Unter dem Reiter _Schnittstellen_ können Zugriffsmöglichkeiten für Datensätze beschrieben und konfiguriert werden.

![type:video](/udp_manager_videos/create_service.mp4){: style='width: 100%'}

* _+_ Button unterhalb der Schnittstellen-Tabelle drücken.
* Es wird ein neues, leeres Formular geöffnet. Ungespeicherte Objekte sind an der ID _0_ zu erkennen.
* Zunächst müssen aus den Drop-Downs _Typ_ und _Software_ die gewünschten Werte ausgewählt werden.
* alle weiteren Pflichtfelder werden anschließend mit default Werten befüllt. Manuelle Anpassungen sollten in den meisten Fällen nicht notwendig sein.
* URLs können nur über den Editierbutton (Stiftsymbol) manuell bearbeitet werden.
* Bei externen Schnittstellen (Haken setzen in der entsprechenden Checkbox) muss nur die externe URL eingefügt werden (Feld ist dann immer aktiv).
* Anschließend wird empfohlen über _Speichern_ rechts unterhalb des Schnittstellen-Formulars die Schnittstelle zwischenzuspeichern.
* Nach erfolgreichem Speichern wird die neu vergebene Schnittstellen ID im Formular sichtbar.
* OPTIONAL: Metadaten koppeln / Eintragen
* Im Bereich Metadaten kann ein Quellkatalog aus dem Drop-Down ausgewählt werden (muss vorher eingetragen werden).
* Falls zur Hand, kann die UUID des Metadatensatzes in das entsprechende Feld eingetragen werden.
* Über den Button mit dem Lupensymbol kann jetzt der gewählte Katalog nach dem Schnittstellentitel bzw. der UUID durchsucht werden.
* Es öffnet sich ein neues Fenster mit den Suchergebnisse. Hier kann der korrekte Metadatensatz ausgewählt werden.
* Über _OK_ werden die Informationen zu _Beschreibung_, _Schlagworte_, _Zugangsbeschränkungen_ und _Nutzungsbedingungen_ aus den Metadaten ausgelesen und in die entsprechende Felder im Schnittstellen Formular eingetragen.
* Durch einen Klick auf _Speichern_ wird der aktuelle Stand abgespeichert.

## Layerkonfiguration

Sobald Collections und Schnittstellen hinzugefügt wurden, werden automatisch Layerobjekte für jede Collection und jede Schnittstelle erzeugt.
Diese können unter dem Reiter "Collections" und dann im Unterreiter "Layer" eingesehen und editiert werden.
Alle wichtigen Layer-Parameter werden mit default-Werten befüllt. Im Standardfall ist keine weitere manuelle Bearbeitung notwendig.

![type:video](/udp_manager_videos/show_layer.mp4){: style='width: 100%'}

## Erzeugen der Schnittstellenkonfiguration

Nachdem alle Informationen zu Datensatz, Collections und Schnittstellen eingetragen wurden, können die Schnittstellenkonfigurationen erzeugt werden.

![type:video](/udp_manager_videos/create_service_conf.mp4){: style='width: 100%'}

* Zuerst muss zum Reiter "Schnittstellen" und dort in den Unterreiter "Konfiguration" gewechselt werden.
* Konfigurationen können nur für Schnittstellen im Status _dev_ oder _stage_ erstellt werden. Liegt noch keine Konfiguration vor, wird die Meldung "Fehler: Konfiguration konnte nicht geladen werden!" angezeigt.
* Auf den Button _Config Generieren_ drücken, um die Schnittstellenkonfiguration zu erzeugen.
* Es öffnet sich ein neues Fenster, in dem der "dev"-Workspace ausgewählt werden muss, in den die Konfiguration generiert wird.
* Mit Klick auf _Auswählen_ wird die Generierung der Konfiguration gestartet.
* HINWEIS: Falls sich die Schnittstelle im Status _stage_ befand, wird der Status automatisch auf _dev_ geändert.
* Nach erfolgreichem Generieren können die einzelnen Konfiguration über die neu erschienen Schaltflächen geöffnet und auch manuell editiert werden.
* Alle Dateien werden im Backend ins Filesystem geschrieben und, falls konfiguriert, automatisch in ein git-Repository commited.
* HINWEIS: Das manuelle Editieren der Konfigurationsdateien sollte auf das Nötigste beschränkt werden!
* Bei deegree Diensten kann der "dev"-Workspace über die Buttons _Workspace validieren_ geprüft und über _Workspace neu laden_ neu Initialisiert werden. 