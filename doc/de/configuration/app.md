# UDP-Manager Konfiguration

## Server
Liste der Server, die pro Schnittstelle ausgewählt werden können.

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|name|ja|String||
|domain|ja|String||
|protocol|ja|String|http oder https|
|lb|ja|Boolean|true, wenn dieser Server eine Loadbalancergruppe repräsentiert|
|server|nein|String[]|wenn lb=true werden in diesen Array die einzelnen Server hinter dem Loadbalancer eingetragen|

**Beispiel**

```json
[
    {
        name: "name", domain: ".de", protocol: "http", lb: true, server: ["server1", "server2"]
    }
]
```
***

## Schnittstellen Typen
Typen von Schnittstellen, die im UDP-Manager verwaltet werden können.

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|name|ja|String|Schnittstellenbezeichnung|
|category|ja|String|Entweder "visualization", "vector" oder "misc". "vector" wird hier als Synonym für Downloadschnittstellen verstanden|
|versions|ja|String[]|Liste von für diesen Schnittstellen verfügbaren Versionen|

**Beispiel**

```json
[
    {name: "WMS", category: "visualization", versions: ["1.1.1", "1.3.0"]},
    {name: "WFS", category: "vector", versions: ["1.1.0", "2.0.0"]},
    {name: "WPS", category: "misc", versions: ["1.0.0"]}
]
```
***

## Test BBoxes
Bounding boxes, die beim Aufruf von Testrequests ausgewählt werden können.

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|name|ja|String||
|srs|ja|String||
|bbox|ja|String[]||

**Beispiel**

```json
[
    {
        name: "St.Pauli", srs: "EPSG:25832", bbox: ["562394,5932816,564194,5934616"]
    }
]
```
***

## Default BBOX
Bounding Box, die standardmäßig in Schnittstellenkonfigurationen verwendet wird.

**Beispiel**

```
548315 5916868 588060 5955212
```
***

## Unterstützte Projektionen
Komma-separierte Liste von EPSG-Codes

**Beispiel**

```
EPSG:25832,EPSG:25833,EPSG:4326,EPSG:4258,EPSG:31467,EPSG:3857,EPSG:3044,EPSG:3034,EPSG:3035
```
***

## Attribute in Layer-JSON ignorieren
String-Array von Attributnamen / Name (API), die bei der Generierung der Layer-JSON Objekte unter "gfiAttributes" ignoriert werden sollen.

**Beispiel**

```
["geom", "the_geom", "geometry"]
```
***

## Client Konfiguration
Konfigurationsparameter, die sich auf das Frontend beziehen.

|Name|Pflichtfeld|Datentyp|Beschreibung|Beispiel|
|----|--------|----|-----------|-------|
|helpLink|nein|String|||
|user|nein|String[]|Array-Liste mit den Namen der Editoren, die verwendet werden, wenn keine AD-Verbindung konfiguriert ist|`["Hans Dampf"]`|
|responsibleParty|ja|**[responsibleParty](#responsibleparty)**[]|Array-Liste mit dem Namen und der E-Mail-Adresse der verantwortlichen Stellen||
|regions|nein|**[regions](#regions)**[]|Array-Liste der Regionen, die im Formular für die Datensatzdetails ausgewählt und verknüpft werden können||
|defaultDatasetSrs|ja|String|Standard-EPSG-Code, wenn nicht anders im Datensatz angegeben|`EPSG:25832`|
|defaultCollectionNamespace|ja|String|Standard-Namespace, der in FeatureType-Konfigurationen verwendet werden soll|`de.hh.up=https://registry.gdi-de.org/id/de.hh.up`|
|mapPreview|ja|String|URI zu einer Masterportal-Instanz, die zur Vorschau für dev und stage Schnittstellen verwendet werden soll|`/UDPPreview`|
|mapPreviewProd|ja|String|URI zu einer Masterportal-Instanz, die zur Vorschau für prod Schnittstellen verwendet werden soll|`/UDPPreviewProd`|
|mapPreviewBaseLayerId|ja|Integer|Layer ID, die standardmäßig in den Vorschauportalinstanzen als Hintergrundkarte verwendet werden soll||
|securityProxyUrlSso|nein|**[securityProxyUrlSso](#securityProxyUrlSso)**|||
|securityProxyUrlAuth|nein|String|Standard URL-Prefix für Basic-Auth geschützte Schnittstellen||

### responsibleParty
Parameter der Verantwortlichen Stelle. 

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|name|ja|String|Wird im Dropdown-Menü in der Detailansicht des Datensatzes angeboten|
|mail|ja|String|Für den automatischen Versand von E-Mails|
|mail_alt|nein|String|Für den automatischen Versand von E-Mails - Alternative beim Löschvermerk|
|inbox_name|ja|String|Wird als Absender der E-Mail verwendet|
|inbox_name_alt|nein|String|Wird als Absender der E-Mail verwendet - Alternative beim Löschvermerk|

**Beispiel**

```json
[
    {name: "Team 1", mail: "mail@mail.de", mail_alt: "alternative_mail@mail.de", inbox_name: "inbox Team 1", inbox_name_alt: "inbox Team Alternative"},
    {name: "Team 2", mail: "mail@mail.de", inbox_name: "inbox Team 2"}
]
```
***

### regions
Parameters für die Definition von Regionen. 

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|landlevel|ja|String|Name der übergeordneten Region, z.B. Bundesland|
|arealevel|ja|String[]|Liste von Sub-Regionen, z.B. Landkreisen|

**Beispiel**

```json
{
    "landlevel": "Niedersachsen",
    "arealevel": [
        "alle Kreise",
        "mehrere Kreise",
        "Cuxhaven",
        "Harburg",
        "Heidekreis",
        "Lüchow-Dannenberg",
        "Lüneburg",
        "Rotenburg/Wümme",
        "Stade",
        "Uelzen"
    ]
}
```
***

### securityProxyUrlSso
URL Prefixe getrennt für dev, stage und prod bei AD geschützten Schnittstellen.

**Beispiel**

```json
{
    "dev": "https://server.de/dev_secure_services",
    "stage": "https://server.de/qs_secure_services",
    "prod": "https://server.de/prod_secure_services"
}
```
***

## Capabilities Metadata
Metadatenattribute auf Schnittstellenebene, die in den Capabilitites von OGC Diensten verwendet werden.

```json
{
    providername: "LGV Hamburg",
    providersite: "",
    individualname: "",
    positionname: "Geobasisinfrastruktur",
    phone: "",
    facsimile: "",
    electronicmailaddress: "udp-hilfe@gv.hamburg.de",
    deliverypoint: "Neuenfelder Straße 19",
    city: "Hamburg",
    administrativearea: "HH",
    postalcode: "21109",
    country: "Germany",
    onlineresource: "http://www.geoinfo.hamburg.de",
    hoursofservice: "",
    contactinstructions: "",
    metadataurl: "https://metaver.de/csw",
    authorityname: "LGV",
    authorityurl: "http://www.hamburg.de/bsu/landesbetrieb-geoinformation-und-vermessung/"
}
```
***

## LDAP
Definition der Berechtigungen (Gruppen / einzelne Benutzer) für den Zugriff auf den UDP-Manager

Dieses Feld ist nur editierbar, wenn in der env Variable _AUTH\_METHOD_ "ldap" definiert wurde.

Rolle _admin_: Vollzugriff
Rolle _reader_: Lesender Zugriff

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|allowed_groups|ja|Object[]|Name einer AD-Gruppen, die für den UDP-Manager autorisiert werden sollen sowie die zugeordnete Rolle|
|allowed_users|ja|Object[]|Name eines spezifischen AD-Benutzers, der für den UDP-Manager autorisiert werden sollen, sowie die zugeordnete Rolle|

**Beispiel**

```json
{
    allowed_groups: [
        {name: "gruppe_1", role: "admin"},
        {name: "gruppe2", role: "reader"}
    ],
    allowed_users: [
        {name: "MusterMax", role: "admin"}
    ]
}
```
***

## JIRA
JIRA API Verbindungsparameter. Optional, wird nur benötigt, wenn der Haken bei "JIRA Modul aktivieren" gesetzt ist.

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|url|ja|String|URL der JIRA REST API|
|browse_url|ja|String|URL für die HTML Ansicht eines Issues in JIRA|
|issueTypeId|ja|Object|Name und IDs von Issue-Typen|
|projects|ja|**[projects](#Projektparameter)**[]|Parameter für die verschiedenen Jira-Projekte, die integriert werden sollen|
|defaultAssignee|ja|String|Name eines Benutzers, der der Standard-Zuweisungsempfänger ist. Kann ein leerer String sein|
|labels|ja|String[]|Schlüsselwörter|
|proxy|ja|Boolean|true, wenn ein Proxy für die Verbindung benutzt werden soll|

#### Projektparameter
|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|name|ja|String|Schlüsselwort eines JIRA Projekts|
|name_alias|ja|String|Anzeigename|
|new_as_subtask|ja|Boolean|true, wenn das Issue als Sub-Task angelegt werden soll|
|use_in_ticketsearch|ja|Boolean|true, wenn das Issue suchbar sein soll|
|story_points_value|ja|Integer|Standardwert für die Story Points eines Issues|
|transition_execution|ja|Boolean|true, wenn ein neues Issue in ein definiertes Kanban-Board übertragen werden soll|
|transition_id|ja|Integer[]|Ids der Kanban-Board Spalten|
|components|ja|Objects[]|Namen von Komponenten|

**Beispiel**

```json
{
    url: "https://www.jira-host.de/rest/api/2",
    browse_url: "https://www.jira-host.de/browse",
    issueTypeId: {
        taks: "10000",
        sub_task: "20000"
    }
    projects: [
        {
            name: "PROJECT",
            name_alias: "Projektname (PROJECT)",
            new_as_subtask: true,
            use_in_ticketsearch: true,
            story_points_value: 2,
            transition_execution: false,
            transition_id: [1, 2],
            components: [{name: "Testzweck"}]
        }
    ],
    defaultAssignee: "",
    labels: ["Label_1", "Label_2"],
    proxy: true
}
```
***

## Mail
E-Mail Empfänger, an die bei bestimmten Aktionen automatisch Mails verschickt werden sollen.

Dieses Feld ist nur editierbar, wenn die env Variablen _SMTP\_HOST_ und _SMTP\_PORT_ gesetzt sind.

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|portals_from|ja|String|Absendeadresse|
|prod_to|ja|String|Empfänger, wenn eine Schnittstelle produktiv gesetzt wird|
|delete_layer_to|ja|String|Empfänger, wenn ein Layer gelöscht wurde|
|delete_layer_request_to|ja|String|Empfänger, wenn ein Layer Löschvermerk eingetragen wurde|

**Beispiel**

```json
{
    portals_from: "\"Team 1\" <team@mail.de>",
    prod_to: "person1@mail.de, person2@mail.de",
    delete_layer_to: "team2@mail.de",
    delete_layer_request_to: "team2@mail.de"
}
```
***

## Module
Über die entsprechenden Checkboxen können einzelne Module für den UDP-Manager aktiviert / deaktiviert werden

### git
Wenn der Haken bei "git Modul aktivieren" gesetzt wurde können weitere Konfigurationsparameter eingegeben werden.

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|options_config|ja|String[]|Array, in dem git-config Parameter hinzugefügt werden können|
|branch|ja|String|Name des verwendeten Branches|
|ssh|ja|Boolean|true, wenn eine SSH Verbindung genutzt werden soll|
|sshKnownHosts|nein|String|Pfad zur known_hosts Datei|
|sshKey|nein|String|Pfad zur private-key Datei|

**Beispiel**

```json
{
    options_config: [
        "http.proxy=http://proxy.de:80",
        "user.email=name@mail.de",
        "user.name=UserName",
    ],
    branch: "main",
    ssh: true,
    sshKnownHosts: "C:/User/.ssh/known_hosts",
    sshKey: "C:/User/.ssh/id_rsa"
}
```
***

### geschützte Schnittstellen
Ist dieser Haken gesetzt, können pro Schnittstelle geschützte URLs für die Verfahren "Basic Auth" und "AD SSO" gesetzt werden.
Bei "AD SSO" können zusätzlich AD Gruppen, die für den Zugriff auf diese Schnittstelle berechtigt sind, ausgewählt werden.

### Zugriffsstatistiken
Ist dieser Haken gesetzt, wird im Hauptmenü unter dem Punkt "Statistiken" der Tab "Zugriffe" und im Schnittstellen Tab der Punkt "Statistiken" aktiviert. Hier können Werte für monatliche Zugriffe auf Schnittstellen eingesehen werden.
Diese Zahlen speisen sich aus der Datenbanktabelle "data.visits", die durch einen externen Prozess befüllt werden muss.

### Elasticsearch
Ist dieser Haken gesetzt, werden die Informationen aus der Elasticsearch Konfiguration genutzt um Layer JSON Objekte in die konfigurierten Indexe zu schreiben.

### externe Collection-Import
Ist dieser Haken gesetzt, wird im Collections-Tab die Funktion zum Import von Collections aus den Capabilities von an diesem Datensatz konfigurierten WMS / WFS aktiviert.