# Quelldatenbankverbindungen
In diesem Abschnitt können Datenbanksysteme eingetragen werden, aus denen Datensätze für die Bereitstellung über Schnittstellen repliziert werden. Der Workflow würde grob wie folgt aussehen: Quelldatenbank --> ETL-Prozess --> Datenbank --> Schnittstelle --> Client. Quelldatenbankverbindungen können pro Collection unter "Quelldaten" verknüpft werden.
In der Tabelle sind alle eingetragenen Quelldatenbankverbindungen aufgelistet.

**Bezeichnung**: Name der Verbindung, der in den Auswahlmenüs im Datensatzformular auftaucht. Für deegree und Geoserver Konfigurationen wird dieser Wert auch als Name der referenzierten JNDI Ressource verwendet.

**DBMS**: Angabe des Datenbankmangementsystems, z.B. PostgreSQL oder Oracle

**Host**: Datenbankhost

**Port**: Datenbankport (Standard bei PostgreSQL ist 5432)

**Datenbankname**: Name der Datenbank.

**DB-User**: Datenbankbenutzer zur Authentifizierung.

**Passwort**: Passwort zur Authentifizierung. Wird als Hash in der DB abgelegt. Schlüssel zur (De)Codierung wird über den ENV Parameter _SECRET_ definiert.