# Metadatenkataloge
Um die Funktion zur Kopplung von Metadaten nutzen zu können, muss zumindest ein Metadatenkatalog eingetragen werden.

**Bezeichnung**: Name des Katalogs, der auch in den Auswahlmenüs im Datensatz und Schnittstellenformular erscheint.

**CSW URL**: URL des CSW Endpunktes des Katalogs. Die URL muss ohne "?" und GET Parameter eingetragen werden. Beispiel: https://metaver.de/csw

**Show Doc URL**: URL für eine HTML Repräsentanz eines Metadatensatzes. Die URL muss so eingetragen werden, dass am Ende die UUID des Metadatensatzes angehängt werden kann. Beispiel: https://metaver.de/trefferanzeige?cmd=doShowDocument&docuuid=

**Metadaten SRS**: EPSG Code der Projektion, die in der Bounding Box in Metadatensätzen verwendet wird.

**Proxy**: Haken setzen, wenn für Zugriffe auf den Metadatenkatalog der in der _.env_ eingetragene Proxy verwendet werden muss.

**In Internet Layer JSON verwenden**: Bei der Verwendung von mehreren Katalog-Instanzen, kann über diesen Haken gesteuert werden, dass in der Internet service.json die CSW URL des eigentlich gekoppelten Katalogs mit der URL dieses Katalogs überschrieben wird.