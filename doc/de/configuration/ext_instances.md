# Externe UDP-Manager Instanzen
Um die Funktion zum Import von Datensätzen aus externen UDP-Manager Instanzen nutzen zu können, muss mindestens eine UDP-Manager Instanz (Datenbankverbindungsparameter) hier angelegt werden.

**Bezeichnung**: Name der Verbindung, der in den Auswahlmenüs im Datensatzformular auftaucht.

**Host**: Datenbankhost

**Port**: Datenbankport (Standard bei PostgreSQL ist 5432)

**Datenbankname**: Name der Datenbank.

**DB\_User**: Datenbankbenutzer zur Authentifizierung.

**Passwort**: Passwort zur Authentifizierung. Wird als Hash in der DB abgelegt. Schlüssel zur (De)Codierung wird über den ENV Parameter _SECRET_ definiert.