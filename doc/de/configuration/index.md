# Anwendungskonfiguration

Um den UDP-Managers nutzen zu können, müssen die folgenden Bereiche (vor allem _Anwendung_ und _Software_, aber auch_Metadatenkataloge_ und _Datenbankverbindungen_) bearbeitet werden. Diese können über den Hauptmenüpunkt _Verwaltung_ und dort unter _Konfigurationen_ erreicht werden.