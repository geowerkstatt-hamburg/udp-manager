# FME Flow Datenbank
PostgreSQL Verbindungsparameter auf eine FME Flow Datenbank. Wenn mindestens ein Objekt eingetragen wurde, können pro Datensatz im Tab _ETL Prozesse_ FME ETL Prozesse dieser Server Instanz verknüpft werden.

**Bezeichnung**: Name der Verbindung, der in den Auswahlmenüs im Datensatzformular auftaucht.

**Host**: Datenbankhost

**Port**: Datenbankport (Standard bei PostgreSQL ist 5432)

**Datenbankname**: Name der Datenbank.

**Schema**: Dankbankschema.

**DB\**User**: Datenbankbenutzer zur Authentifizierung.

**Passwort**: Passwort zur Authentifizierung. Wird als Hash in der DB abgelegt. Schlüssel zur (De)Codierung wird über den ENV Parameter _SECRET_ definiert.