# Elasticsearch
In dieser Sektion können verschiedene Such-Indizes mit individuellen Filtern konfiguriert werden. Wenn mindestens ein Index eingerichtet ist, werden automatisch Layer-JSON Objekte im Index hinzugefügt. Dieser Index kann in der Masterportal Themensuche eingebunden werden.

**Settings bearbeiten**: Es öffnet sich ein Bearbeitungsfenster, in dem die Index Settings angepasst werden können. Standardwerte sind hinterlegt.

**Mappings bearbeiten**: Es öffnet sich ein Bearbeitungsfenster, in dem die Attribut-Mappings angepasst werden können. Es sind Standardwerte hinterlegt, die in der Regel nicht angepasst werden sollten.

Im Bereich **Indizes** können neue Indizes angelegt und bestehende bearbeitet oder gelöscht werden.

**Neu**: Es wird ein neues Element in die Tabelle eingefügt.

**Löschen**: Der selektierte Index wird nach Bestätigung aus der Liste entfernt.

**Name**: Die Bezeichnung des Index, unter dem er aufgerufen werden kann. Da der Name Teil der URL ist, sollten keine Leer-, oder Sonderzeichen sowie keine Großbuchstaben verwendet werden.

**Server**: In dieses Textfeld kann eine durch Kommata getrennte Liste von Server-URLs eingetragen werden, über die Elasticsearch Installationen erreichbar sind. (Bsp.: "http://server1.de:9200,http://server2.de:9200")

**Nur produktive Layer**: Wenn angehakt, werden für diesen Index nur Layer berücksichtigt, die mit einer Schnittstelle im Status "prod" verknüpft sind.

**Inklusive interne Layer**: Wenn angehakt, werden für diesen Index auch Layer berücksichtigt, bei denen im zugehörigen Datensatz die Freigabeebene "intranet" gesetzt ist.

**Nur Datensätze mit diesen Schlagworten**: Komma-separierte Liste an Filterschlagworten, die im Datensatz vergeben sein MÜSSEN, damit die zugehörigen Layer für diesen Index berücksichtigt werden.

**Datensätze mit diesen Schlagworten ausschließen**: Komma-separierte Liste an Filterschlagworten. Wenn diese in einem Datensatz vergeben sind, werden die zugehörigen Layer nicht für diesen Index berücksichtigt.

WICHTIG:

Sämtliche Anpassungen werden erst nach Klick auf _Speichern_ gültig.