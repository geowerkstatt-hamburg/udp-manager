# Software

Die auswählbare Software im Schnittstellen-Tab kann hier definiert werden. Einige Parameter sind essentiell für die Benutzung des Configurator-Moduls.

Über den Button _neu_ kann ein neuer Eintrag angelegt werden. Die vorhandene Beispielkonfiguration muss angepasst und im Feld _Bezeichnung_ ein Softwarename vergeben werden. Zum Abschluss muss _Speichern_ betätigt werden.

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|dev|ja|**[dev-Parameter](#Umgebungsparameter)**|Object mit Parametern die umgebungsspezifisch sind|
|stage|ja|**[stage-Parameter](#Umgebungsparameter)**|Object mit Parametern die umgebungsspezifisch sind|
|prod|ja|**[prod-Parameter](#Umgebungsparameter)**|Object mit Parametern die umgebungsspezifisch sind|
|seperateDevWorkspace|ja|Boolean|wenn true, werden configs im Status _dev_ auch in einen Workspace im Status _dev_ zusammengestellt|
|internalExternalWorkspace|nein|Object|mit dem keys _internal_ und _external_ können zwei getrennte Workspaces definiert werden, in denen je nach Freigabeebene des Datensatzes (internet/intranet) die verknüpften Schnittstellen nur im _internal_ oder auch im _external_ Workspace zusammen gestellt werden|
|supportedApiTypes|ja|Object|Jeder Schlüssel dieses Objektes steht für einen unterstützen Schnittstellen-Typ. Pro Schnittstelle kann unter _availableProjections_ ein String[] mit einer Liste von EPSG Codes hinterlegt werden|
|dataTypeMap|ja|Object|Mapping von Datenbank-Datentypen auf von der Software unterstütze Datentypen|
|inspireSettings|nein|Object|In diesem Objekt können INSPIRE spezifische Parameter konfiguriert werden. Aktuell wird nur für WMS und WFS der Parameter _languageCode_ benötigt|

**Beispiel**

```json
{
    "dev": {
        "instances": [
            "fachdaten_dev"
        ],
        "urlIntTemplate": "{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
        "urlExtTemplate": "https://qs-geodienste.hamburg.de/{{service_name}}",
        "defaultServer": "lgvfds-container-stage",
        "softwareSpecificParameter": {
            "apiKey": null,
            "workspaceReloadAllowed": true,
            "additionalOutputFormats": [
                "csv",
                "geojson"
            ]
        }
    },
    "stage": {
        "instances": [
            "fachdaten_a_b"
        ],
        "urlIntTemplate": "{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
        "urlExtTemplate": "https://qs-geodienste.hamburg.de/{{service_name}}",
        "defaultServer": "wfalgqa004",
        "softwareSpecificParameter": {
            "apiKey": null,
            "workspaceReloadAllowed": true,
            "additionalOutputFormats": [
                "csv",
                "geojson"
            ]
        }
    },
    "prod": {
        "instances": [
            "fachdaten_a_b"
        ],
        "urlIntTemplate": "{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
        "urlExtTemplate": "https://geodienste.hamburg.de/{{service_name}}",
        "defaultServer": "lgvfds03",
        "softwareSpecificParameter": {
            "apiKey": null,
            "workspaceReloadAllowed": true,
            "additionalOutputFormats": [
                "csv",
                "geojson"
            ]
        }
    },
    "seperateDevWorkspace": true,
    "supportedApiTypes": {
        "wms": {
            "availableProjections": [
                "CRS:84",
                "EPSG:25832",
                "EPSG:25833",
                "EPSG:4326",
                "EPSG:4258",
                "EPSG:31467",
                "EPSG:3857",
                "EPSG:3044",
                "EPSG:3034",
                "EPSG:3035"
            ]
        },
        "wmstime": {
            "availableProjections": [
                "CRS:84",
                "EPSG:25832",
                "EPSG:25833",
                "EPSG:4326",
                "EPSG:4258",
                "EPSG:31467",
                "EPSG:3857",
                "EPSG:3044",
                "EPSG:3034",
                "EPSG:3035"
            ]
        },
        "wfs": {},
        "wfst": {},
        "wps": {},
        "wmts": {}
    },
    "dataTypeMap": {
        "smallint": "number",
        "integer": "number",
        "int": "number",
        "int2": "number",
        "int4": "number",
        "int8": "number",
        "float": "number",
        "float2": "number",
        "float4": "number",
        "float8": "number",
        "varchar": "text",
        "char": "text",
        "string": "text",
        "time": "date",
        "timestamp": "date",
        "timestampz": "date",
        "interval": "date",
        "bool": "boolean",
        "geometry_point": "geometry [point]",
        "geometry_line": "geometry [line]",
        "geometry_multiline": "geometry [line]",
        "geometry_polygon": "geometry [polygon]",
        "geometry_multipolygon": "geometry [polygon]",
        "text": "text",
        "MULTIPOLYGON": "geometry [polygon]",
        "LINESTRING": "geometry [line]",
        "timestamptz": "date",
        "bpchar": "text",
        "POINT": "geometry [point]",
        "POLYGON": "geometry [polygon]",
        "MULTILINESTRING": "geometry [line]",
        "MULTIPOINT": "geometry [point]",
        "uuid": "text",
        "date": "date",
        "numeric": "number"
    },
    "inspireSettings": {
        "languageCode": "ger"
    }
}
```

## Umgebungsparameter

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|instances|ja|String[]|Liste an in dieser Umgebung verfügbaren Workspaces / Instanzen|
|urlIntTemplate|ja|String|Template um automatisch die interne URL generieren zu können|
|urlExtTemplate|ja|String|Template um automatisch die externe URL generieren zu können|
|defaultServer|ja|String|Server, der standardmäßig für diese Umgebung gesetzt werden soll|
|softwareSpecificParameter|ja|**[softwareSpecificParameter](#Softwarespezifische-Parameter)**|Softwarespezifische Parameter|


**Beispiel Deegree**

```json
{
    "instances": [
        "fachdaten_dev"
    ],
    "urlIntTemplate": "{{protocol}}://{{server}}{{domain}}/{{folder}}/services/{{service_name}}",
    "urlExtTemplate": "https://qs-geodienste.hamburg.de/{{service_name}}",
    "defaultServer": "lgvfds-container-stage",
    "softwareSpecificParameter": {
        "apiKey": null,
        "workspaceReloadAllowed": true,
        "additionalOutputFormats": [
            "csv",
            "geojson"
        ]
    }
}
```

** Beispiel Geoserver**

```json
{
    "instances": [],
    "urlIntTemplate": "{{protocol}}://{{server}}{{domain}}/geoserver/{{service_name}}",
    "urlExtTemplate": "{{protocol}}://{{server}}{{domain}}/geoserver/{{service_name}}",
    "defaultServer": "server1",
    "softwareSpecificParameter": {
        "basicAuth" : {
            "username": "admin",
            "password": "goserver"
        },
        "webappContext": "geoserver_123",
        "workspaceReloadAllowed": true
    }
```

### Softwarespezifische-Parameter

|Name|Pflichtfeld|Datentyp|Beschreibung|
|----|--------|----|-----------|
|apiKey|nein|String|Bietet die Software eine API an, kann hier der API-Key definiert werden (aktuell relevant für deegree)|
|basicAuth|nein|**[basicAuth](#basicAuth-Parameter)**|Bietet die Software eine API an, können hier die Credentials angegeben werden. (aktuell relevant für GeoServer)|
|webappContext|nein|String|Wenn die API nicht über den Standard-Context erreichbar ist kann hier der Context angegeben werden. (aktuell relevant für GeoServer)|
|workspaceReloadAllowed|nein|Boolean|Wenn true, kann in dieser Umgebung einer Workspace Reload ausgeführt werden. Aktuell relevant für deegree und ldproxy|
|additionalOutputFormats|nein|String[]|Liste an Output-Formaten die zusätzlich angeboten werden sollen. Zur Zeit nur für deegree Relevant. Hier kann _csv_ und _geojson_ hinzugefügt werden|
|shapePath|nein|String|Pfad, auf die Shape-Indexdatei. Relevant für MapServer Raster-WMS|