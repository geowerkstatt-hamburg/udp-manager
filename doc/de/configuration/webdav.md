# WebDAV
Liste von WebDAV Verbindungsobjekten. Diese werden verwendet, um automatisch Layer-IDs aus Masterportal-Konfigurationen auszulesen.
Diese Informationen werden dafür verwendet um unter dem Hauptmenüeintrag _Portale_ die Liste alle Masterportalinstanzen zu pflegen und mit den eingebundenen Layern zu verknüpfen.

**Bezeichnung**: Name dieser WebDAV Verbindung.

**Basis-URL**: Beispiel: https://server.de

**Verzeichnis**: Name des Verzeichnisses, der als WebDAV Zugangspunkt freigegeben wurde.

**Vollqualifizierte Domain**: Beispiel: https://www.portale.de, kann identisch mit _Basis-URL_ sein.

**Benutzername**: Anmeldename

**Passwort**: Passwort fie Anmeldung

**Proxy**: Haken setzen, wenn für Zugriffe ein Proxy verwendet werden muss.
