# Datenbankverbindungen
Um automatisch neue Schnittstellenkonfigurationen erzeugen und Collections aus Datenbanktabellen importieren zu können, muss mindestens eine Datenbankverbindung hier angelegt werden. Es ist nur möglich PostgreSQL Verbindungen zu konfigurieren.
In der Tabelle sind alle eingetragenen Datenbankverbindungen aufgelistet. Die Verbindung, die als _Default Lesend_ gesetzt ist, wird in der Tabelle mit einem Seiten-Symbol markiert. Die Verbindung, die als _Default Schreibend_ gesetzt ist, wird in der Tabelle mit einem Stift-Symbol markiert.

Es ist nicht vorgesehen Parameter für stage und produktiv Datenbanken getrennt zu hinterlegen. Für den Workflow ist nur die Verbindung relevant, die bei der Entwicklung neuer Schnittstellen benötigt wird. Es sollten auch keine Verbindungsparameter in den Konfigurationsdateien enthalten sein, sondern immer über JNDI Ressourcen oder Umgebungsvariablen auf den Servern direkt konfiguriert und in den Schnittstellenkonfigurationen nur referenziert werden.

**Bezeichnung**: Name der Verbindung, der in den Auswahlmenüs im Datensatzformular auftaucht. Für deegree und Geoserver Konfigurationen wird dieser Wert auch als Name der referenzierten JNDI Ressource verwendet.

**Host**: Datenbankhost

**Port**: Datenbankport (Standard bei PostgreSQL ist 5432)

**Datenbankname**: Name der Datenbank.

**DB-User**: Datenbankbenutzer zur Authentifizierung.

**Passwort**: Passwort zur Authentifizierung. Wird als Hash in der DB abgelegt. Schlüssel zur (De)Codierung wird über den ENV Parameter _SECRET_ definiert.

**Default Lesend**: Wenn gesetzt, wird die Verbindung automatisch beim anlegen neuer Datensätze als lesende Verbindung eingetragen.

**Default Schreibend**: Wenn gesetzt, wird die Verbindung automatisch beim anlegen neuer Datensätze als schreibende Verbindung eingetragen.