# API

Beschreibung der UDP-Manager Backend-Routen.

Für sämtliche API Zugriffe muss im Request Header ein Authentifizierungs-Token mitgesendet werden:

```json
    {
        token: "[API_TOKEN]"
    }
```

Ausgenommen ist die Route _/getlayerjson_!

Der Token wird im _.env_ File über den Parameter `API_TOKEN` festgelegt.

## Datensatz

* [hole Datensätze](api/getdatasets.md) : `GET /backend/getdatasets`
* [hole Datensatz](api/getdatasets.md) : `GET /backend/getdataset`

## Collection

* [hole Collections pro Datensatz](api/getcollections.md) : `GET /backend/getcollectionsdataset`
* [Attributkonfiguration aktualisieren](api/updateattributeconfig.md) : `POST /backend/updateattributeconfig`
* [Anzahl Features aktualisieren](api/updatenumberfeatures.md) : `POST /backend/updatenumberfeatures`

## ETL Prozess

* [hole ETL Prozesse pro Collection oder pro Datensatz](api/getetlprocesses.md) : `GET /backend/getetlprocesses`
* [ETL Prozessinformationen aktualisieren](api/updateetlsummary.md) : `POST /backend/updateetlsummary`

## Layer JSON

* [hole Layer JSON Objekte](api/getlayerjson.md) : `GET /backend/getlayerjson`

## Datenbankverbindung

* [hole Datenbankverbindung](api/getdbconnection.md) : `GET /backend/getdbconnection`