# Changelog UDP-Manager
 All important changes in this project are stored in this file.

## Unreleased - in development
### __Breaking Changes__
    - new application config schema. Introduction of an .env file for basic config params. All other configurations are stored directly in the database and can be edited via the UDP-Manager gui.
    - new generate config und deploy workflow and gui controls. All services coupled to one dataset are treated together.
    - new dataset status values
    - elastic config parameter server now on index level

### Added
    - editorial attributes can be changed on dataset level without starting the edit mode
    - layer attributes on collection level can now be changed without starting edit mode
    - View and edit layers ordered by service under the service details tab
    - OpenID Connect Authentication integration
    - service counter in global services list
    - ldproxy: labels and description added to templates
    - new PropertyIsLike Style Filter Rule
    - new data source params could be set at collection level
    - datasets can now be consist of raster and vector data
    - Attribute and Collection Change Log
    - Masterportal 3.0 config.json support for layer ID parser.
    - GeoServer: WMS and WFS will now be generated as geoserver services.
    - switch between dev/stage and productive layer json elements
    - time stamp file will be added to workspaces

### Changed
    - new dataset details form
    - new Attribute Configuration Editor
    - Portal URI is shown in grid

### Deprecated

### Removed
    - dataset import from external UDP-Manager instance
    - deegree.pro integration for basic auth secured services

### Fixed


## v1.3 - 2024-05-31
### __Breaking Changes__

### Added
    - service counter in global services list
    - ldproxy: labels and description added to templates
    - WMS-Time as service type
    - Style editor: rules can be reorganized using drag & drop
    - new external url for oaf services in dev

### Changed
    - update deegree config templates. update csv outputFormat mime type
    - update mapserver config templates. add wms_feature_info_mime_type
    - change csv format for wfs service

### Deprecated

### Removed

### Fixed
    - wrong service config shown after new service is created
    - Service Editor Preview not rerendered
    - legend upload to use same file twice
    - directly access service via url param
    - add new dataset when search filter is active


## v1.2 - 2024-03-27
### __Breaking Changes__

### Added

### Changed
    - save button size and color
    - toolbar layout, delete and copy buttons look and feel
    - config_software: new parameter defaultServer
    - JIRA Cloud Support for API v1000.824.0

### Deprecated
    - config_software: defaultDevServer replaced by defaultServer

### Removed
    - hit tolerance layer setting

### Fixed
    - old service config is not properly deleted when generating new configs or deploying configs to other environments
    - stage workspaces with seperateDevWorkspace=false are missing dev configs
    - dataset, collection, layer, service search filter
    - delete service config folder with large number of files
    - style editor with large number of rules
    - dockerfile git installation
    - add database schema and table buttons enabled in newly created collections
    - missing permission at created database schema and table
    - masterportal layer id extractor if title is undefined

## v1.1 - 2024-01-18

### Added
    - Dockerfile and docker-compose.yml
    - Check on startup if outPath folder exists. Initially clone repository if configured

### Changed
    -  When importing datasets, datasets with coupled dev services will be ignored
    -  search dataset or service by id returns unique result
    -  Jira ticket templates reworked & selection of collections whose data needs to be updated added to Jira ticket creation

### Fixed
    - Generating the internal url for GeoWebCache
    - When delete dataset, services that are coupled with other datasets are deleted too
    - Polygon style form, pattern style unintentional set to null
    - Wrong metadata url in deegree metadata template
    - init database without ldap config
    - action column trigger function even if no action should be available
    - portal scraper can not parse layer-IDs from config.json out of layerId Elements
    - after generating or deploying service configs, old unused configs are not deleted
    - metadata catalog urls are overwritten in layer json for layers with external dataset reference
    - dataset keyword filter not working
    - the tile size attribute value is not used for the layer json
    - if ldap is not configured, db init and update pages are not loaded