const config = require("./config.js");
const database = require("./database.js");

async function start () {

    function isJsonString (str) {
        try {
            JSON.parse(str);
        }
        catch (e) {
            return false;
        }
        return true;
    }

    function uniqueDatasetIds (arr) {
        const ids = [];

        arr.forEach((obj) => {
            ids.push(obj.dataset_md_id);
        });

        return [...new Set(ids)];
    }

    const statusMap = {
        "Produktiv": "veröffentlicht",
        "In Bearbeitung": "in Bearbeitung",
        "Test": "in Bearbeitung"
    };

    let shortnameCounter = 1;

    const dm_metadata_catalogs = await database.getMetadataCatalogs();

    for (let i = 0; i < dm_metadata_catalogs.length; i++) {
        dm_metadata_catalogs[i].id = dm_metadata_catalogs[i].config_metadata_catalog_id;
        dm_metadata_catalogs[i].use_in_internet_json = false;
        delete dm_metadata_catalogs[i].config_metadata_catalog_id;
    }

    await database.insertMetadataCatalogs(dm_metadata_catalogs);

    console.log("INSERT metadata_catalogs COMPLETE");

    const dm_dataset = await database.getDatasets();

    for (let i = 0; i < dm_dataset.length; i++) {
        dm_dataset[i].title = dm_dataset[i].dataset_name;
        delete dm_dataset[i].dataset_name;
        dm_dataset[i].shortname = dm_dataset[i].dataset_shortname ? dm_dataset[i].dataset_shortname : "change_it_" + shortnameCounter;
        delete dm_dataset[i].dataset_shortname;
        dm_dataset[i].rs_id = dm_dataset[i].rs_id ? dm_dataset[i].rs_id : null;
        dm_dataset[i].cat_opendata = dm_dataset[i].cat_opendata ? dm_dataset[i].cat_opendata : null;
        dm_dataset[i].cat_inspire = dm_dataset[i].cat_inspire ? dm_dataset[i].cat_inspire : null;
        dm_dataset[i].cat_hmbtg = dm_dataset[i].cat_hmbtg ? dm_dataset[i].cat_hmbtg : null;
        dm_dataset[i].cat_org = dm_dataset[i].cat_org ? dm_dataset[i].cat_org : null;
        dm_dataset[i].bbox = dm_dataset[i].bbox ? dm_dataset[i].bbox : null;
        dm_dataset[i].fees = dm_dataset[i].fees ? dm_dataset[i].fees : null;
        dm_dataset[i].description = dm_dataset[i].description ? dm_dataset[i].description : null;
        dm_dataset[i].access_constraints = dm_dataset[i].access_constraints ? dm_dataset[i].access_constraints : null;
        // if (!dm_dataset[i].config_metadata_catalog_id) {
        //     dm_dataset[i].md_id = "";
        // }
        dm_dataset[i].metadata_catalog_id = dm_dataset[i].config_metadata_catalog_id;
        dm_dataset[i].keywords = dm_dataset[i].keywords.split("|");
        dm_dataset[i].srs = config.defaultDatasetSrs;
        dm_dataset[i].status = statusMap[dm_dataset[i].status];

        if (!dm_dataset[i].dataset_shortname) {
            shortnameCounter++;
        }
    }

    await database.insertDatasets(dm_dataset);

    console.log("INSERT datasets COMPLETE");

    const dm_services = await database.getServices();

    console.log("INPUT services: " + dm_services.length);

    for (let i = 0; i < dm_services.length; i++) {
        const id = await database.getDatasetId(dm_services[i]);
        const keywords = await database.getServiceKeywords(dm_services[i]);
        const service_keywords = [];
        const dataset_filter_keywords = [];

        for (let j = 0; j < keywords.length; j++) {
            if (keywords[j].text === "mrh" || keywords[j].text === "only_mrh" || keywords[j].text === "saga") {
                dataset_filter_keywords.push(keywords[j].text);
            }
            else {
                service_keywords.push(keywords[j].text);
            }
        }

        dm_services[i].id = dm_services[i].service_id;
        delete dm_services[i].service_id;
        dm_services[i].check_status = dm_services[i].service_checked_status;
        dm_services[i].status_dev = dm_services[i].status !== "Produktiv";
        dm_services[i].status_stage = dm_services[i].status === "Produktiv";
        dm_services[i].status_prod = dm_services[i].status === "Produktiv";
        dm_services[i].create_date = dm_services[i].last_edit_date;
        delete dm_services[i].service_checked_status;
        dm_services[i].name = dm_services[i].service_name;
        delete dm_services[i].service_name;
        dm_services[i].md_id = dm_services[i].service_md_id;
        delete dm_services[i].service_md_id;
        dm_services[i].update_interval = dm_services[i].update_number + " " + dm_services[i].update_unit;
        dm_services[i].keywords = service_keywords;
        dm_services[i].filter_keywords = dataset_filter_keywords;
        dm_services[i].metadata_catalog_id = dm_services[i].config_metadata_catalog_id;
        dm_services[i].restriction_level = dm_services[i].only_intranet ? "intranet" : "internet";
        dm_services[i].allowed_groups = JSON.stringify(dm_services[i].allowed_groups);
        dm_services[i].software = dm_services[i].software === "3dcitydb" ? "VCS" : dm_services[i].software;
        dm_services[i].store_type = dm_services[i].software === "MapServer" ? "Raster" : "Vektor";
        dm_services[i].folder = dm_services[i].software === "MapServer" ? null : dm_services[i].folder;
        dm_services[i].server_prod = dm_services[i].status === "Produktiv" ? dm_services[i].server : null;
        dm_services[i].folder_prod = dm_services[i].status === "Produktiv" ? dm_services[i].folder : null;
        dm_services[i].url_int_prod = dm_services[i].status === "Produktiv" ? dm_services[i].url_int : null;
        dm_services[i].url_ext_prod = dm_services[i].status === "Produktiv" ? dm_services[i].url_ext : null;

        if (dm_services[i].title.indexOf("WFS-T") > -1 || dm_services[i].title.indexOf("WFST") > -1) {
            dm_services[i].type = "WFS-T";
        }
        else {
            dm_services[i].type = dm_services[i].service_type;
        }
        delete dm_services[i].service_type;

        let etl_process = null;

        if (dm_services[i].update_process_type && dm_services[i].update_process_name) {
            etl_process = {
                type: dm_services[i].update_process_type.indexOf("FME") > -1 ? "FME Workspace" : "Sonstiges",
                name: dm_services[i].update_process_name,
                interval_custom: dm_services[i].update_interval,
                folder: dm_services[i].update_process_location,
                description: dm_services[i].update_process_description,
                interval_number: dm_services[i].update_number === "" || dm_services[i].update_number === null ? null : parseInt(dm_services[i].update_number),
                interval_unit: dm_services[i].update_unit
            };
        }

        if (id.length > 0) {
            const dataset_id = id[0].id;

            dm_services[i].dataset_id = dataset_id;

            await database.updateDatasetFromService(dm_services[i]);

            if (etl_process) {
                etl_process.dataset_id = dataset_id;
                await database.insertEtlProcesses(etl_process);
            }
        }
        else {
            const title_new = dm_services[i].title.replace("WMS ", "").replace("WFS ", "").replace("WFS-T ", "").replace("WFST ", "").replace("WMTS ", "").trim() + " *ANPASSEN*";
            const new_id = await database.getNewDatasetId(title_new);

            if (new_id.length === 0) {
                const dataset = {
                    title: title_new,
                    shortname: "change_it_" + shortnameCounter,
                    datasource: dm_services[i].datasource,
                    create_date: dm_services[i].last_edit_date,
                    last_edit_date: dm_services[i].last_edit_date,
                    last_edited_by: dm_services[i].last_edited_by,
                    created_by: dm_services[i].editor,
                    responsible_party: dm_services[i].responsible_party,
                    status: statusMap[dm_services[i].status],
                    srs: config.defaultDatasetSrs,
                    restriction_level: dm_services[i].only_intranet ? "intranet" : "internet",
                    external: dm_services[i].external
                };

                if (dataset_filter_keywords.length > 0) {
                    dataset.filter_keywords = dataset_filter_keywords;
                }
                else {
                    dataset.filter_keywords = {};
                }

                const dataset_id = await database.insertNewDataset(dataset);

                shortnameCounter++;

                dm_services[i].dataset_id = dataset_id[0].id;

                if (etl_process) {
                    etl_process.dataset_id = dataset_id[0].id;
                    await database.insertEtlProcesses(etl_process);
                }
            }
            else {
                dm_services[i].dataset_id = new_id[0].id;
            }
        }

        await database.insertService(dm_services[i]);
        await database.insertServiceLink(dm_services[i]);
    }

    await database.deleteDuplicateETL();

    console.log("INSERT services COMPLETE");

    const dm_freigaben = await database.getFreigaben();

    console.log("INPUT freigaben: " + dm_freigaben.length);

    for (let i = 0; i < dm_freigaben.length; i++) {
        const id = await database.getDatasetIdServices(dm_freigaben[i]);

        if (id.length > 0) {
            dm_freigaben[i].id = id[0].id;

            await database.updateFreigabeLink(dm_freigaben[i]);
        }
    }

    console.log("INSERT freigaben COMPLETE");

    //await database.insertServices(dm_services);

    const dm_wms_layers = await database.getLayersWMS();
    const layer_names = [];

    console.log("INPUT WMS layers: " + dm_wms_layers.length);

    for (let i = 0; i < dm_wms_layers.length; i++) {
        dm_wms_layers[i].title = dm_wms_layers[i].title ? dm_wms_layers[i].title : dm_wms_layers[i].layer_name;
        dm_wms_layers[i].name = dm_wms_layers[i].layer_name;
        delete dm_wms_layers[i].layer_name;
        dm_wms_layers[i].attribution = dm_wms_layers[i].layerattribution;
        delete dm_wms_layers[i].layerattribution;
        dm_wms_layers[i].gfi_as_new_window = dm_wms_layers[i].gfi_asnewwindow;
        delete dm_wms_layers[i].gfi_asnewwindow;
        dm_wms_layers[i].gfi_window_specs = dm_wms_layers[i].gfi_windowspecs;
        delete dm_wms_layers[i].gfi_windowspecs;
        dm_wms_layers[i].service_url_visible = dm_wms_layers[i].service_url_is_visible;
        delete dm_wms_layers[i].service_url_is_visible;
        dm_wms_layers[i].date_attr = dm_wms_layers[i].datetime_column;
        delete dm_wms_layers[i].datetime_column;

        if (dm_wms_layers[i].gfi_config === "ignore") {
            dm_wms_layers[i].gfi_disabled = true;
        }
        else {
            dm_wms_layers[i].gfi_disabled = false;
        }

        if (isJsonString(dm_wms_layers[i].gfi_config)) {
            dm_wms_layers[i].attribute_config_tmp = [];
            const gfi_config = JSON.parse(dm_wms_layers[i].gfi_config);

            for (const key in gfi_config) {
                dm_wms_layers[i].attribute_config_tmp.push({
                    attr_name_source: key,
                    attr_name_db: key,
                    attr_name_service: key,
                    attr_name_masterportal: gfi_config[key],
                    description: ""
                });
            }

            dm_wms_layers[i].attribute_config = JSON.stringify(dm_wms_layers[i].attribute_config_tmp);
            delete dm_wms_layers[i].attribute_config_tmp;
        }
        else {
            dm_wms_layers[i].attribute_config = null;
        }

        // was ist mit gfi_beautifykeys?

        const id = await database.getDatasetIdByMdId(dm_wms_layers[i]);

        if (id.length > 0) {
            dm_wms_layers[i].dataset_id = id[0].id;

            const service_link = await database.getServiceLink({dataset_id: dm_wms_layers[i].dataset_id, service_id: dm_wms_layers[i].service_id});

            if (service_link.length === 0) {
                const service = dm_services.find((obj) => obj.id === dm_wms_layers[i].service_id);

                service.dataset_id = dm_wms_layers[i].dataset_id;

                await database.insertServiceLink({dataset_id: dm_wms_layers[i].dataset_id, id: dm_wms_layers[i].service_id});
                await database.updateDatasetFromService(service);
            }
        }
        else if (!dm_wms_layers[i].dataset_md_id) {
            const service_layers = dm_wms_layers.filter((obj) => obj.service_id === dm_wms_layers[i].service_id);
            const service_layers_not_coupled = service_layers.filter((obj) => obj.dataset_md_id === null || obj.dataset_md_id === "");
            const service_layers_coupled = service_layers.filter((obj) => obj.dataset_md_id !== null && obj.dataset_md_id !== "");
            const dataset_mdids = uniqueDatasetIds(service_layers_coupled);

            if (service_layers.length > service_layers_not_coupled.length && dataset_mdids.length === 1) {
                const alt_id = await database.getDatasetIdByMdId({dataset_md_id: dataset_mdids[0]});

                dm_wms_layers[i].dataset_id = alt_id[0].id;
            }
            else if (service_layers.length > service_layers_not_coupled.length && dataset_mdids.length > 1) {
                const dataset = {
                    title: dm_wms_layers[i].title + " *ANPASSEN*",
                    shortname: "change_it_" + shortnameCounter,
                    datasource: dm_wms_layers[i].datasource,
                    create_date: dm_wms_layers[i].last_edit_date,
                    last_edit_date: dm_wms_layers[i].last_edit_date,
                    last_edited_by: dm_wms_layers[i].last_edited_by,
                    created_by: dm_wms_layers[i].editor,
                    responsible_party: dm_wms_layers[i].responsible_party,
                    status: statusMap[dm_wms_layers[i].status],
                    srs: config.defaultDatasetSrs,
                    restriction_level: dm_wms_layers[i].only_intranet ? "intranet" : "internet",
                    external: dm_wms_layers[i].external,
                    filter_keywords: {}
                };

                const dataset_id = await database.insertNewDataset(dataset);

                shortnameCounter++;

                dm_wms_layers[i].dataset_id = dataset_id[0].id;

                await database.insertServiceLink({dataset_id: dm_wms_layers[i].dataset_id, id: dm_wms_layers[i].service_id});
            }
            else if (service_layers_not_coupled.length === service_layers.length) {
                const alt_id = await database.getDatasetIdServices(dm_wms_layers[i]);

                if (alt_id[0]) {
                    dm_wms_layers[i].dataset_id = alt_id[0].id;
                }
            }
        }
    }

    for (let i = 0; i < dm_wms_layers.length; i++) {
        if (dm_wms_layers[i].dataset_id) {
            const collection_id = await database.insertCollection(dm_wms_layers[i]);

            const layer = {
                id: dm_wms_layers[i].layers_id,
                collection_id: collection_id[0].id,
                service_id: dm_wms_layers[i].service_id,
                dataset_id: dm_wms_layers[i].dataset_id,
                st_attributes: JSON.stringify({
                    gutter: dm_wms_layers[i].gutter,
                    tile_size: dm_wms_layers[i].tilesize,
                    single_tile: dm_wms_layers[i].singletile,
                    transparent: dm_wms_layers[i].transparent,
                    output_format: dm_wms_layers[i].output_format,
                    gfi_format: dm_wms_layers[i].gfi_format,
                    feature_count: dm_wms_layers[i].featurecount,
                    notsupportedin3d: dm_wms_layers[i].notsupportedfor3d,
                    time_series: dm_wms_layers[i].time_series
                })
            };

            await database.insertLayer(layer);

            layer_names.push(dm_wms_layers[i].name + dm_wms_layers[i].dataset_id);
        }
        else {
            console.log("Not inserted: Layer_id: " + dm_wms_layers[i].layers_id + " -  service_id: " + dm_wms_layers[i].service_id);
            //neuer Datensatz bzw. neuen Datensatz von Service verwenden
        }
    }

    console.log("INSERT WMS layers COMPLETE");

    const dm_layers = await database.getLayers();

    console.log("INPUT layers: " + dm_layers.length);

    for (let i = 0; i < dm_layers.length; i++) {
        let name = dm_layers[i].layer_name;
        let title = dm_layers[i].title ? dm_layers[i].title : dm_layers[i].layer_name;
        let ns_name = "ns";

        if (name.indexOf(":") > 0) {
            name = dm_layers[i].layer_name.split(":")[1];
            ns_name = dm_layers[i].layer_name.split(":")[0];
        }
        if (title.indexOf(":") > 0) {
            title = dm_layers[i].title.split(":")[1];
        }

        const namespace = ns_name + "=" + dm_layers[i].namespace;

        dm_layers[i].title = title;
        dm_layers[i].name = name;
        dm_layers[i].namespace = namespace;
        delete dm_layers[i].layer_name;
        dm_layers[i].attribution = dm_layers[i].layerattribution;
        delete dm_layers[i].layerattribution;
        dm_layers[i].gfi_as_new_window = dm_layers[i].gfi_asnewwindow;
        delete dm_layers[i].gfi_asnewwindow;
        dm_layers[i].gfi_window_specs = dm_layers[i].gfi_windowspecs;
        delete dm_layers[i].gfi_windowspecs;
        dm_layers[i].service_url_visible = dm_layers[i].service_url_is_visible;
        delete dm_layers[i].service_url_is_visible;
        dm_layers[i].date_attr = dm_layers[i].datetime_column;
        delete dm_layers[i].datetime_column;

        if (dm_layers[i].gfi_config === "ignore") {
            dm_layers[i].gfi_disabled = true;
        }
        else {
            dm_layers[i].gfi_disabled = false;
        }

        if (isJsonString(dm_layers[i].gfi_config)) {
            dm_layers[i].attribute_config_tmp = [];
            const gfi_config = JSON.parse(dm_layers[i].gfi_config);

            for (const key in gfi_config) {
                dm_layers[i].attribute_config_tmp.push({
                    attr_name_source: key,
                    attr_name_db: key,
                    attr_name_service: key,
                    attr_name_masterportal: gfi_config[key],
                    description: ""
                });
            }

            dm_layers[i].attribute_config = JSON.stringify(dm_layers[i].attribute_config_tmp);
            delete dm_layers[i].attribute_config_tmp;
        }
        else {
            dm_layers[i].attribute_config = null;
        }

        const id = await database.getDatasetIdByMdId(dm_layers[i]);

        if (id.length > 0) {
            dm_layers[i].dataset_id = id[0].id;

            const service_link = await database.getServiceLink({dataset_id: dm_layers[i].dataset_id, service_id: dm_layers[i].service_id});

            if (service_link.length === 0) {
                const service = dm_services.find((obj) => obj.id === dm_layers[i].service_id);

                service.dataset_id = dm_layers[i].dataset_id;

                await database.insertServiceLink({dataset_id: dm_layers[i].dataset_id, id: dm_layers[i].service_id});
                await database.updateDatasetFromService(service);
            }
        }

        else if (!dm_layers[i].dataset_md_id) {
            const service_layers = dm_layers.filter((obj) => obj.service_id === dm_layers[i].service_id);
            const service_layers_not_coupled = service_layers.filter((obj) => obj.dataset_md_id === null || obj.dataset_md_id === "");
            const service_layers_coupled = service_layers.filter((obj) => obj.dataset_md_id !== null && obj.dataset_md_id !== "");
            const dataset_mdids = uniqueDatasetIds(service_layers_coupled);

            if (service_layers.length > service_layers_not_coupled.length && dataset_mdids.length === 1) {
                const alt_id = await database.getDatasetIdByMdId({dataset_md_id: dataset_mdids[0]});

                dm_layers[i].dataset_id = alt_id[0].id;
            }
            else if (service_layers.length > service_layers_not_coupled.length && dataset_mdids.length > 1) {
                const dataset = {
                    title: dm_layers[i].title + " *ANPASSEN*",
                    shortname: "change_it_" + shortnameCounter,
                    datasource: dm_layers[i].datasource,
                    create_date: dm_layers[i].last_edit_date,
                    last_edit_date: dm_layers[i].last_edit_date,
                    last_edited_by: dm_layers[i].last_edited_by,
                    created_by: dm_layers[i].editor,
                    responsible_party: dm_layers[i].responsible_party,
                    status: statusMap[dm_layers[i].status],
                    srs: config.defaultDatasetSrs,
                    restriction_level: dm_layers[i].only_intranet ? "intranet" : "internet",
                    external: dm_layers[i].external,
                    filter_keywords: {}
                };

                const dataset_id = await database.insertNewDataset(dataset);

                shortnameCounter++;

                dm_layers[i].dataset_id = dataset_id[0].id;

                await database.insertServiceLink({dataset_id: dm_layers[i].dataset_id, id: dm_layers[i].service_id});
            }
            else if (service_layers_not_coupled.length === service_layers.length) {
                const alt_id = await database.getDatasetIdServices(dm_layers[i]);

                if (alt_id[0]) {
                    dm_layers[i].dataset_id = alt_id[0].id;
                }
            }
        }
    }

    for (let i = 0; i < dm_layers.length; i++) {
        if (dm_layers[i].dataset_id) {
            let collection_id = [{id: 0}];

            if (!layer_names.includes(dm_layers[i].name + dm_layers[i].dataset_id)) {
                collection_id = await database.insertCollection(dm_layers[i]);
            }
            else {
                collection_id = await database.getCollectionIdByDatasetID(dm_layers[i]);
            }

            const layer = {
                id: dm_layers[i].layers_id,
                collection_id: collection_id[0].id,
                service_id: dm_layers[i].service_id,
                dataset_id: dm_layers[i].dataset_id
            };

            if (dm_layers[i].service_type === "WFS" || dm_layers[i].service_type === "WFS-T") {
                layer.st_attributes = JSON.stringify({
                    output_format: dm_layers[i].output_format
                });

                await database.updateCollectionNamespace({collection_id: collection_id[0].id, namespace: dm_layers[i].namespace});
            }
            else if (dm_layers[i].service_type === "OAF") {
                layer.st_attributes = JSON.stringify({
                    //TBD
                });
            }
            else if (dm_layers[i].service_type === "Terrain3D") {
                layer.st_attributes = JSON.stringify({
                    request_vertex_normals: dm_layers[i].request_vertex_normals
                });
            }
            else if (dm_layers[i].service_type === "TileSet3D") {
                layer.st_attributes = JSON.stringify({
                    maximum_screen_space_error: dm_layers[i].maximum_screen_space_error
                });
            }
            else if (dm_layers[i].service_type === "Oblique") {
                layer.st_attributes = JSON.stringify({
                    resolution: dm_layers[i].resolution,
                    minzoom: dm_layers[i].minzoom,
                    hidelevels: dm_layers[i].hidelevels,
                    projection: dm_layers[i].projection,
                    terrainurl: dm_layers[i].terrainurl
                });
            }
            else if (dm_layers[i].service_type === "SensorThings") {
                layer.st_attributes = JSON.stringify({
                    epsg: dm_layers[i].epsg,
                    rootel: dm_layers[i].rootel,
                    expand: dm_layers[i].expand,
                    filter: dm_layers[i].filter,
                    related_wms_layers: dm_layers[i].related_wms_layers,
                    style_id: dm_layers[i].style_id,
                    cluster_distance: dm_layers[i].cluster_distance,
                    load_things_only_in_current_extent: dm_layers[i].load_things_only_in_current_extent,
                    mouse_hover_field: dm_layers[i].mouse_hover_field,
                    sta_test_url: dm_layers[i].sta_test_url
                });

                dm_layers[i].service_type === "STA";
            }
            else if (dm_layers[i].service_type === "VectorTiles") {
                layer.st_attributes = JSON.stringify({
                    extent: dm_layers[i].extent,
                    origin: dm_layers[i].origin,
                    resolutions: dm_layers[i].resolutions,
                    visibility: dm_layers[i].visibility,
                    vtstyles: dm_layers[i].vtstyles
                });
            }
            else {
                layer.st_attributes = "{}";
            }

            if (layer.id && collection_id[0].id !== 0) {
                await database.insertLayer(layer);
                layer_names.push(dm_layers[i].layer_name + dm_layers[i].dataset_id);
            }
            else {
                console.log("no collection id");
            }
        }
        else {
            console.log("Not inserted: Layer_id: " + dm_wms_layers[i].layers_id + " -  service_id: " + dm_wms_layers[i].service_id);
        }
    }

    console.log("INSERT layers COMPLETE");

    const dm_comments = await database.getComments();

    for (let i = 0; i < dm_comments.length; i++) {
        const dataset_id = await database.getDatasetIdServices(dm_comments[i]);

        if (dataset_id.length > 0) {
            dm_comments[i].dataset_id = dataset_id[0].id;
            dm_comments[i].username = dm_comments[i].author;
            delete dm_comments[i].author;
            dm_comments[i].comment = dm_comments[i].text;
            delete dm_comments[i].text;
            await database.insertComment(dm_comments[i]);
        }
    }

    console.log("INSERT comments COMPLETE");

    const dm_delete_requests = await database.getLayerDeleteRequest();

    for (let i = 0; i < dm_delete_requests.length; i++) {
        await database.updateLayerDeleteRequest(dm_delete_requests[i]);
    }

    console.log("INSERT delete_requests COMPLETE");

    const dm_jira_tickets = await database.getJiraTickets();

    for (let i = 0; i < dm_jira_tickets.length; i++) {
        const dataset_id = await database.getDatasetIdServices(dm_jira_tickets[i]);

        if (dataset_id.length > 0) {
            dm_jira_tickets[i].dataset_id = dataset_id[0].id;
            await database.insertJiraTicket(dm_jira_tickets[i]);
        }
    }

    console.log("INSERT jira_tickets COMPLETE");

    await database.setCollectionsIdSeq();
    await database.setServicesIdSeq();
    await database.setDatasetsIdSeq();
    await database.setLayersIdSeq();
    await database.setMetadataCatalogsIdSeq();

    console.log("sequences set");
}

start();
