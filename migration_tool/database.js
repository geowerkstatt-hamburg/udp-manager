const pgpromise = require("pg-promise")();
const config = require("./config.js");
const Logger = require("../backend/utils/logger.util");

const connection_source = config.database.dm_source;
const connection_target = config.database.udpm_target;

const db_source = pgpromise(connection_source);
const db_target = pgpromise(connection_target);

module.exports = {
    dbsourceQuery: async function (query, params) {
        var result = db_source.any(query, params)
            .then(data => {
                return data;
            })
            .catch(error => {
                Logger.debug(error);
                Logger.debug(query);
            });

        return result;
    },

    dbtargetQuery: async function (query, params) {
        var result = db_target.any(query, params)
            .then(data => {
                return data;
            })
            .catch(error => {
                Logger.debug(error);
                Logger.debug(query);
            });

        return result;
    },

    getMetadataCatalogs: async function () {
        return this.dbsourceQuery("SELECT * FROM config_metadata_catalogs LIMIT 1");
    },

    insertMetadataCatalogs: async function (params) {
        const table = new pgpromise.helpers.TableName({table: "metadata_catalogs", schema: "data"});
        const cs = new pgpromise.helpers.ColumnSet(["id", "name", "csw_url", "show_doc_url", "proxy", "srs_metadata", "use_in_internet_json"], {table});
        const query = pgpromise.helpers.insert(params, cs);

        await db_target.none(query);
    },

    getDatasets: async function () {
        return this.dbsourceQuery("SELECT * FROM datasets");
    },

    insertDatasets: async function (params) {
        const table = new pgpromise.helpers.TableName({table: "datasets", schema: "data"});
        const cs = new pgpromise.helpers.ColumnSet(["title", "md_id", "rs_id", "cat_opendata", "cat_inspire", "cat_hmbtg", "cat_org", "bbox", "fees", "description", "keywords", "access_constraints", "metadata_catalog_id", "shortname", "srs", "status"], {table});
        const query = pgpromise.helpers.insert(params, cs);

        await db_target.none(query);
    },

    getServiceKeywords: async function (params) {
        return this.dbsourceQuery("SELECT * FROM keywords WHERE service_id=${service_id}", params);
    },

    getFreigaben: async function () {
        return this.dbsourceQuery("SELECT a.service_id, b.link FROM freigaben_links a LEFT JOIN freigaben b ON a.freigabe_id=b.freigabe_id");
    },

    getServices: async function () {
        return this.dbsourceQuery("SELECT DISTINCT ON (s.service_id) s.*, ll.md_id AS dataset_md_id FROM services s LEFT JOIN layer_links ll ON s.service_id = ll.service_id WHERE s.deleted IS false ORDER BY s.service_id");
    },

    getDatasetId: async function (params) {
        return this.dbtargetQuery("SELECT id FROM data.datasets WHERE md_id = ${dataset_md_id}", params);
    },

    getNewDatasetId: async function (title) {
        return this.dbtargetQuery("SELECT id FROM data.datasets WHERE title = ${title}", {title: title});
    },

    getDatasetIdServices: async function (params) {
        return this.dbtargetQuery("SELECT dataset_id AS id FROM data.service_links WHERE service_id = ${service_id}", params);
    },

    getServiceLink: async function (params) {
        return this.dbtargetQuery("SELECT * FROM data.service_links WHERE dataset_id = ${dataset_id} AND service_id = ${service_id}", params);
    },

    getDatasetIdByMdId: async function (params) {
        return this.dbtargetQuery("SELECT id FROM data.datasets WHERE md_id = ${dataset_md_id}", params);
    },

    updateDatasetFromService: async function (params) {
        return this.dbtargetQuery("UPDATE data.datasets SET responsible_party=NULLIF(${responsible_party}, '')::text, status=NULLIF(${status}, '')::text, datasource=NULLIF(${datasource}, '')::text, created_by=${editor}, last_edited_by=${last_edited_by}, last_edit_date=${last_edit_date}, create_date=${create_date}, filter_keywords=array_cat(filter_keywords, ${filter_keywords}), inspire=${inspire_relevant}, restriction_level=NULLIF(${restriction_level}, '')::text, external=${external}, store_type=${store_type} WHERE id = ${dataset_id}", params);
    },

    updateFreigabeLink: async function (params) {
        return this.dbtargetQuery("UPDATE data.datasets SET freigabe_link = ${link} WHERE id=${id};", params);
    },

    insertServices: async function (params) {
        const table = new pgpromise.helpers.TableName({table: "services", schema: "data"});
        const cs = new pgpromise.helpers.ColumnSet(["id", "dataset_id", "check_status", "name", "title", "status", "type", "software", "server", "folder", "url_ext", "url_int", "url_sec", "security_type", "md_id", "version", "qa", "description", "fees", "access_constraints", "allowed_groups", "metadata_catalog_id", "external"], {table});
        const query = pgpromise.helpers.insert(params, cs);

        await db_target.none(query);
    },

    insertServiceLinks: async function (params) {
        const table = new pgpromise.helpers.TableName({table: "service_links", schema: "data"});
        const cs = new pgpromise.helpers.ColumnSet(["dataset_id", "service_id"], {table});
        const query = pgpromise.helpers.insert(params, cs);

        await db_target.none(query);
    },

    insertEtlProcesses: async function (params) {
        const table = new pgpromise.helpers.TableName({table: "etl_processes", schema: "data"});
        const cs = new pgpromise.helpers.ColumnSet(["name", "description", "folder", "interval_custom", "interval_number", "interval_unit", "type", "dataset_id"], {table});
        const query = pgpromise.helpers.insert(params, cs);

        await db_target.none(query);
    },

    insertComment: async function (params) {
        return this.dbtargetQuery("INSERT INTO data.comments(username, comment, post_date, dataset_id) VALUES (NULLIF(${username}, '')::text, NULLIF(${comment}, '')::text, ${post_date}, ${dataset_id});", params);
    },

    insertJiraTicket: async function (params) {
        return this.dbtargetQuery("INSERT INTO data.jira_tickets(dataset_id, key, creator, summary, assignee, project) VALUES (${dataset_id}, NULLIF(${key}, '')::text, NULLIF(${creator}, '')::text, NULLIF(${summary}, '')::text, NULLIF(${assignee}, '')::text, NULLIF(${project}, '')::text);", params);
    },

    insertService: async function (params) {
        return this.dbtargetQuery("INSERT INTO data.services(id, check_status, name, title, status_dev, status_stage, status_prod, type, software, server, folder, url_ext, url_int, url_sec, security_type, md_id, version, qa, description, fees, access_constraints, allowed_groups, metadata_catalog_id, external) VALUES (${id}, NULLIF(${check_status}, '')::text, NULLIF(${name}, '')::text, NULLIF(${title}, '')::text, ${status_dev}, ${status_stage}, ${status_prod}, NULLIF(${type}, '')::text, NULLIF(${software}, '')::text, NULLIF(${server}, '')::text, NULLIF(${folder}, '')::text, NULLIF(${url_ext}, '')::text, NULLIF(${url_int}, '')::text, NULLIF(${url_sec}, '')::text, NULLIF(${security_type}, '')::text, NULLIF(${md_id}, '')::text, NULLIF(${version}, '')::text, ${qa}, NULLIF(${description}, '')::text, NULLIF(${fees}, '')::text, NULLIF(${access_constraints}, '')::text, ${allowed_groups}, ${metadata_catalog_id}, ${external});", params);
    },

    insertServiceLink: async function (params) {
        return this.dbtargetQuery("INSERT INTO data.service_links(dataset_id, service_id) VALUES (${dataset_id}, ${id});", params);
    },

    getLayersWMS: async function () {
        return this.dbsourceQuery("SELECT DISTINCT ON (layers.layer_id) ll.md_id AS dataset_md_id, layers.layer_id AS layers_id, layers.*, ows.*, type3d.*, oblique.*, sensorthings.*, vectortiles.*, services.service_type, services.status, services.only_intranet, services.datasource, services.last_edit_date, services.last_edited_by, services.editor, services.responsible_party, services.external FROM layers LEFT JOIN layers_type_ows ows ON layers.layer_id = ows.layer_id LEFT JOIN layers_type_3d type3d ON layers.layer_id = type3d.layer_id LEFT JOIN layers_type_oblique oblique ON layers.layer_id = oblique.layer_id LEFT JOIN layers_type_sensorthings sensorthings ON layers.layer_id = sensorthings.layer_id LEFT JOIN layers_type_vectortiles vectortiles ON layers.layer_id = vectortiles.layer_id LEFT JOIN services services ON layers.service_id = services.service_id LEFT JOIN layer_links ll ON layers.layer_id = ll.layer_id WHERE layers.deleted = false AND services.service_type = 'WMS';");
    },

    getLayers: async function () {
        return this.dbsourceQuery("SELECT DISTINCT ON (layers.layer_id) ll.md_id AS dataset_md_id, layers.layer_id AS layers_id, layers.*, ows.*, type3d.*, oblique.*, sensorthings.*, vectortiles.*, services.service_type, services.status, services.only_intranet, services.datasource, services.last_edit_date, services.last_edited_by, services.editor, services.responsible_party, services.external FROM layers LEFT JOIN layers_type_ows ows ON layers.layer_id = ows.layer_id LEFT JOIN layers_type_3d type3d ON layers.layer_id = type3d.layer_id LEFT JOIN layers_type_oblique oblique ON layers.layer_id = oblique.layer_id LEFT JOIN layers_type_sensorthings sensorthings ON layers.layer_id = sensorthings.layer_id LEFT JOIN layers_type_vectortiles vectortiles ON layers.layer_id = vectortiles.layer_id LEFT JOIN services services ON layers.service_id = services.service_id LEFT JOIN layer_links ll ON layers.layer_id = ll.layer_id WHERE layers.deleted = false AND services.service_type != 'WMS';");
    },

    insertCollection: async function (params) {
        return this.dbtargetQuery("INSERT INTO data.collections (dataset_id, name, title, title_alt, attribution, additional_categories, legend_url_intranet, legend_url_internet, transparency, scale_min, scale_max, gfi_disabled, gfi_theme, gfi_theme_params, attribute_config, gfi_as_new_window, gfi_window_specs, service_url_visible, namespace) VALUES (${dataset_id}, NULLIF(${name}, '')::text, NULLIF(${title}, '')::text, NULLIF(${title_alt}, '')::text, NULLIF(${attribution}, '')::text, NULLIF(${additional_categories}, '')::text, NULLIF(${legend_url_intranet}, '')::text, NULLIF(${legend_url_internet}, '')::text, ${transparency}, NULLIF(${scale_min}, '')::text, NULLIF(${scale_max}, '')::text, ${gfi_disabled}, NULLIF(${gfi_theme}, '')::text, NULLIF(${gfi_theme_params}, '')::text, NULLIF(${attribute_config}, '')::json, ${gfi_as_new_window}, NULLIF(${gfi_window_specs}, '')::text, ${service_url_visible}, NULLIF(${namespace}, '')::text) RETURNING id;", params);
    },

    insertLayer: async function (params) {
        return this.dbtargetQuery("INSERT INTO data.layers(id, collection_id, service_id, dataset_id, st_attributes) VALUES (${id}, ${collection_id}, ${service_id}, ${dataset_id}, ${st_attributes});", params);
    },

    insertNewDataset: async function (params) {
        return this.dbtargetQuery("INSERT INTO data.datasets(title, shortname, datasource, last_edit_date, last_edited_by, create_date, created_by, responsible_party, status, restriction_level, external, srs, filter_keywords) VALUES (NULLIF(${title}, '')::text, NULLIF(${shortname}, '')::text, NULLIF(${datasource}, '')::text, ${last_edit_date}, ${last_edited_by}, ${create_date}, ${created_by}, NULLIF(${responsible_party}, '')::text, NULLIF(${status}, '')::text, NULLIF(${restriction_level}, '')::text, ${external}, NULLIF(${srs}, '')::text, NULLIF(${filter_keywords},'{}')::text[]) RETURNING id;", params);
    },

    getCollectionIdByName: async function (params) {
        return this.dbtargetQuery("SELECT id FROM data.collections WHERE name = ${name}", params);
    },

    getCollectionIdByDatasetID: async function (params) {
        return this.dbtargetQuery("SELECT id FROM data.collections WHERE dataset_id = ${dataset_id} AND name = ${name}", params);
    },

    getComments: async function () {
        return this.dbsourceQuery("SELECT * FROM comments");
    },

    getLayerDeleteRequest: async function () {
        return this.dbsourceQuery("SELECT * FROM layer_delete_requests");
    },

    updateLayerDeleteRequest: async function (params) {
        return this.dbtargetQuery("UPDATE data.layers SET delete_request=true, delete_request_comment = ${comment} WHERE id = ${layer_id}", params);
    },

    getJiraTickets: async function () {
        return this.dbsourceQuery("SELECT * FROM service_jira_tickets");
    },

    setCollectionsIdSeq: function () {
        return this.dbtargetQuery("SELECT setval('data.collections_id_seq', (SELECT MAX(id) FROM data.collections), true);");
    },

    setServicesIdSeq: function () {
        return this.dbtargetQuery("SELECT setval('data.services_id_seq', (SELECT MAX(id) FROM data.services), true);");
    },

    setDatasetsIdSeq: function () {
        return this.dbtargetQuery("SELECT setval('data.datasets_id_seq', (SELECT MAX(id) FROM data.datasets), true);");
    },

    setLayersIdSeq: function () {
        return this.dbtargetQuery("SELECT setval('data.layers_id_seq', (SELECT MAX(id) FROM data.layers), true);");
    },

    setMetadataCatalogsIdSeq: function () {
        return this.dbtargetQuery("SELECT setval('data.metadata_catalogs_id_seq', (SELECT MAX(id) FROM data.metadata_catalogs), true);");
    },

    updateCollectionNamespace: function (params) {
        return this.dbtargetQuery("UPDATE data.collections SET namespace=${namespace} WHERE id=${collection_id};", params);
    },

    deleteDuplicateETL: function () {
        return this.dbtargetQuery("DELETE FROM data.etl_processes a USING data.etl_processes b WHERE a.id < b.id AND a.name = b.name AND a.type = b.type AND a.folder = b.folder;");
    }
};
