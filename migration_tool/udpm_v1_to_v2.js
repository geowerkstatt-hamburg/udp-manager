const database = require("./database_v2.js");

async function start () {
    const collections = await database.getCollections();

    console.log("migrate attribute config for " + collections.length + " collections");

    for (const collection of collections) {
        if (Array.isArray(collection.attribute_config)) {
            for (const attr_conf of collection.attribute_config) {
                if (attr_conf.attr_name_db === "ignore") {
                    attr_conf.attr_name_db_ignore = true;
                }
                else {
                    attr_conf.attr_name_db_ignore = false;
                }
                if (attr_conf.attr_name_service === "ignore") {
                    attr_conf.attr_name_service_ignore = true;
                }
                else {
                    attr_conf.attr_name_service_ignore = false;
                }
                if (attr_conf.attr_name_masterportal === "ignore") {
                    attr_conf.attr_name_masterportal_ignore = true;
                }
                else {
                    attr_conf.attr_name_masterportal_ignore = false;
                }
                if ((attr_conf.attr_name_service === "geom" || attr_conf.attr_name_service === "the_geom") && (!attr_conf.attr_name_masterportal || attr_conf.attr_name_masterportal === "ignore")) {
                    attr_conf.attr_name_masterportal_ignore = true;
                    attr_conf.attr_name_masterportal = "Geometrie";
                }
                if (attr_conf.attr_name_service === "gid" && (!attr_conf.attr_name_masterportal || attr_conf.attr_name_masterportal === "ignore")) {
                    attr_conf.attr_name_masterportal_ignore = true;
                    attr_conf.attr_name_masterportal = "Identifikator";
                }
            }

            collection.attribute_config = JSON.stringify(collection.attribute_config);

            await database.updateAttrConfig(collection);
        }
    }
}

start();
