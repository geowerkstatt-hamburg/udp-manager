const config = {
    database: {
        dm_source: {
            host: "host",
            port: 5432,
            database: "db_name",
            user: "user_name",
            password: "password"
        },
        udpm_target: {
            host: "host",
            port: 5432,
            database: "db_name",
            user: "user_name",
            password: "password"
        }
    }
};

module.exports = config;
