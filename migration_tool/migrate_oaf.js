const axios = require("axios");
const {db_udpm} = require("../backend/db/index.js");

async function getdata () {
    // Datasets to be migrated
    // Defined by SQL Query found here:
    // backend\db\sql\data\get_datasets_for_oaf.sql
    const datasets = await db_udpm.exec.getDatasetsForOaf();
    return datasets
}


function migrate (dataset){
    // For given dataset create an OAF service and also layers for all over its collections via REST API

    let service_id = null;
    const api_domain = 'http://localhost:9020'

    async function saveService(dataset){
        console.log('I am dataset', dataset)
        // Define new service body for API put and fill in with dataset object
        let new_service = {
            "id": 0,
            "type": "OAF",
            "check_status": "Geprüft",
            "title": "OAF " + dataset.title,
            "status": dataset.service_status,
            "qa": false,
            "version": "1.1.0",
            "software": "ldproxy",
            "reverseproxy": false,
            "server": "wfalgqa004",
            "external": false,
            "folder": null,
            "name": dataset.shortname,
            "url_int": `http://wfalgqa004.dpaorinp.de/oaf_intranet/datasets/${dataset.shortname}`,
            "url_ext":`https://qs-api.hamburg.de/datasets/v1/${dataset.shortname}`,
            "security_type": null,
            "url_sec": null,
            "clickradius": null,
            "maxfeatures": null,
            "md_id": dataset.md_id,
            "config_metadata_catalog_name": "HMDK",
            "metadata_link": `<a href=\"https://hmdk.metaver.de/trefferanzeige?docuuid=355D0466-445C-45D9-ADCB-C49015D5AB4E\" target=\"_blank\">Metadaten aufrufen</a>`,
            "description": dataset.description,
            "keywords": [],
            "fees": dataset.fees,
            "fees_json": dataset.fees_json,
            "access_constraints": dataset.access_constraints,
            "additional_info": "",
            "metadata_catalog_id": dataset.metadata_catalog_id,
            "last_edit_date": Date().toLocaleString(),
            "last_edited_by": "Grant Helle",
            "dataset_id": dataset.dataset_id
        };
        
        try {
            new_service.dataset_id = dataset.dataset_id
            const response = await axios.post(`${api_domain}/backend/saveservice`,{
            ...new_service
            });
            return response.data.id

        } catch (error){
            console.error(error);
            }
    }

    async function getCollectionsDataset() {
    try {
        var CollectionsDatasetList = []
        const response = await axios.get(`${api_domain}/backend/getcollectionsdataset?dataset_id=${dataset.dataset_id}`);
        
        response.data.forEach((dataset) => {
            if (dataset.group_object == false) {
                CollectionsDatasetList.push(dataset.id)
            } else {
                console.log(dataset.title, 'is group object!')
            }
        })
        
        return CollectionsDatasetList

    } catch (error) {
        console.error(error);
        }
    }

    async function saveLayer(collection_id, service_id, dataset) {

        // new layer template for REST API put call
        let new_layer = {
            "id": 0,
            "collection_id": collection_id,
            "dataset_id": dataset.dataset_id,
            "service_id": service_id,
            "last_edit_date": Date().toLocaleString(),
            "last_edited_by": "Grant Helle",
            "service_type": "OAF",
            "service_security_type": null,
            "service_title": "OAF " + dataset.title,
            "service_url_int": `https://wfalgqa004.dpaorinp.de/oaf/datasets/${dataset.shortname}`,
            "service_url_sec": `https://qs-api.hamburg.de/v1/${dataset.shortname}`,
            "st_attributes": "{\"limit\":null}"
            }

        try {
            await axios.post(`${api_domain}/backend/savelayer`,{
                ...new_layer
                });
            return new_layer

        } catch (error) {
            console.error(error);
            }
        }

    Promise.all([getCollectionsDataset(), saveService(dataset)])
    .then(async function (results) {

        // Retrieve results from API calls
        // Get collections for given dataset
        const CollectionsDatasetList = results[0];

        // Create new service for dataset
        const service = results[1];

        // iterate through collections list and save layer for newly created service

        for (const collection of CollectionsDatasetList) {
            await saveLayer(collection, service, dataset)
            console.log(`Collection saved ${collection} for service ${service} for dataset ${dataset.dataset_id}`)
        }
    });
}

getdata()
.then((datasets) => {
    db_udpm.exec.deleteAllOaf();  
    return datasets
})
.then((datasets) => {
    datasets.forEach(dataset => migrate(dataset));
  });