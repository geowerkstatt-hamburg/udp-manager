const pgPromise = require("pg-promise");
const axios = require("axios");
const config = require("../config.js").getConfig();
const {UDPMRepo} = require("../backend/db/repos");
const https_agent = require("../backend/utils/https_agent.util");

// -----------------------------------------------------
// SCRIPT SETTINGS
// -----------------------------------------------------

// datasets that shall not be (re)imported
const EXCLUDED_DATASET_IDS = [
    925,
    114,
    340,
    408,
    442,
    642,
    476,
    443,
    456,
    268,
    370
]

// SOURCE connection string used to select datasets with min. 1 stage API for import
const SOURCE_UDPMNGR_CONNECTION_STRING = config.database.udp_manager;

// SOURCE connection ID as found in TARGET DB settings (Verwaltung --> Externe UDP-Manager-Instanzen konfigurieren)
// the relevant id value is stored in schema 'config', table 'ext_instances' and field 'id'
const SOURCE_UDPMNGR_CONNECTION_ID = 1;

// TARGET connection string used to update api status after import
const TARGET_UDPMNGR_CONNECTION_STRING = config.database.udp_manager;

// -----------------------------------------------------
// SCRIPT FUNCTIONS
// -----------------------------------------------------

function initializeDatabaseInstance(db_connection) {
    // demo for initializing pg-promise instances: https://github.com/vitaly-t/pg-promise-demo/blob/master/JavaScript/db/index.js 
    const initOptionsUDPMRepo = {
        extend (obj) {
            obj.exec = new UDPMRepo(obj, pgp_instance);
        }
    };
    const pgp_instance = pgPromise(initOptionsUDPMRepo);
    const db_instance = pgp_instance(db_connection);

    return db_instance;
}

function assembleImportRequestBody (datasetId, lastEditedBy, lastEditDate) {

    const formattedEditDate = lastEditDate.slice(0, 16); // remove seconds from timestamp string

    return {
        id: datasetId, // Datensatz ID, integer
        last_edited_by: lastEditedBy, // Benutzer Vor- und Zuname
        last_edit_date: formattedEditDate, // Datum des Imports YYYY-MM-DD HH:mm,
        createNewIds: false, // Boolean, sollte immer false sein
        update: true, // Boolean, true wenn Datensatz schon vorhanden
        connectionId: SOURCE_UDPMNGR_CONNECTION_ID // integer, id der externen UDPMNGR-Instanz aus der importiert werden soll, in DB unter config.ext_instances.id aufgeführt
    };
}

// -----------------------------------------------------
// ENTRYPOINT
// -----------------------------------------------------

async function main (runInDevMode = false) {
    // get dataset ids with at least one api in status stage
    const sourceDataBase = initializeDatabaseInstance(SOURCE_UDPMNGR_CONNECTION_STRING);
    let datasets = await sourceDataBase.exec.getDatasetsWithStagedAPIs();

    // use example dataset
    if (runInDevMode === true) {
        datasets = [{
            dataset_id: 745,
            last_edited_by: 'Skriptus Importus',
            last_edit_date: '2019-03-05 08:41'
        }]
    }

    // define import endoint of target host
    const targetHost = TARGET_UDPMNGR_CONNECTION_STRING.host
    const importDatasetEndpoint = `http://${targetHost}/udp-manager/backend/importdataset`
    
    // prepare axios config for proxy handling
    // bypassing the regular proxy setting and using a separate httpsAgent with an http proxy set is neccessary
    // see axios issue: https://github.com/axios/axios/issues/925 
    const axiosConfig = {
        proxy: false,
        httpsAgent: https_agent.getHttpsAgent(config.proxy)
    };

    // initiate target database
    const targetDataBase = initializeDatabaseInstance(TARGET_UDPMNGR_CONNECTION_STRING);

    // iterate over retrieved dataset ids
    console.log(`Importing datasets from ${SOURCE_UDPMNGR_CONNECTION_STRING.host} to ${targetHost}`);

    const errors = [];

    for (const dataset of datasets) {
        
        const {dataset_id, last_edited_by, last_edit_date} = dataset

        if (EXCLUDED_DATASET_IDS.includes(dataset_id)) {
            console.log(`Skipping: dataset id ${dataset_id} is in list of excluded datasets`)
            continue
        }
        const body = assembleImportRequestBody(dataset_id, last_edited_by, last_edit_date);

        try {
            const response = await axios.post(importDatasetEndpoint, body, axiosConfig);

            response.data.dataset_id = dataset_id;

            if (response.data.status === "error") {
                errors.push(response.data);

                console.log(`ERROR: import of dataset id ${dataset_id} failed`);
            }
            else {
                console.log(`Imported dataset id ${dataset_id} `);
                await targetDataBase.exec.updateDatasetAfterBulkImport(dataset);
            }
        }
        catch (error) {
            console.log(error);
        }
    }

    console.log(errors.length);

    console.dir(errors, {depth: null});
}

main(runInDevMode=false);
