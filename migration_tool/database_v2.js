const pgpromise = require("pg-promise")();
const config = require("./config.js");
const Logger = require("../backend/utils/logger.util");

const connection_source = config.database.udpm_source;
const connection_target = config.database.udpm_target;

const db_source = pgpromise(connection_source);
const db_target = pgpromise(connection_target);

module.exports = {
    dbsourceQuery: async function (query, params) {
        var result = db_source.any(query, params)
            .then(data => {
                return data;
            })
            .catch(error => {
                Logger.debug(error);
                Logger.debug(query);
            });

        return result;
    },

    dbtargetQuery: async function (query, params) {
        var result = db_target.any(query, params)
            .then(data => {
                return data;
            })
            .catch(error => {
                Logger.debug(error);
                Logger.debug(query);
            });

        return result;
    },

    getCollections: async function () {
        return this.dbsourceQuery("SELECT id, attribute_config FROM data.collections");
    },

    updateAttrConfig: async function (collection) {
        return this.dbtargetQuery("UPDATE data.collections SET attribute_config = ${attribute_config} WHERE id = ${id}", collection);
    }
};
